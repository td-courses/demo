<?php
date_default_timezone_set('Etc/UTC');
header('Content-Type: text/html; charset=utf-8');

require_once('PHPMailerAutoload.php');
require_once('class.smtp.php');
require_once('../../includes/classes/class.Mysqli.php');

global $db;
$db = new dbClass();

$source_id  	 	= $_REQUEST['source_id'];
$incomming_call_id	= $_REQUEST['incomming_call_id'];
$hidden_increment	= $_REQUEST['hidden_increment'];

$address 			= $_REQUEST['address'];
$cc_address 		= $_REQUEST['cc_address'];
$bcc_address 		= $_REQUEST['bcc_address'];
$subject 	 		= $_REQUEST['subject'];
$body 	 			= $_REQUEST['body'];
if($body == ''){
    $body = ' ';
}
$attachmet 	 		= $_REQUEST['attachmet'];
$address = explode(",", $_REQUEST['address']);

$mail = new PHPMailer;
//Tell PHPMailer to use SMTP
$mail->isSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 0;
//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';
//Set the hostname of the mail server
$mail->Host = "mail.yandex.ru";
//Set the SMTP port number - likely to be 25, 465 or 587
$mail->Port = 587;
//Whether to use SMTP authentication
$mail->SMTPAuth = true;
//Username to use for SMTP authentication
$mail->Username = "no-reply@my.ge";
//Password to use for SMTP authentication
$mail->Password = 'b7M=9jdM3nmY_TMgjL9wjJ!wuw5C';
//Set who the message is to be sent from
$mail->setFrom('no-reply@my.ge', 'MY.GE');
//Set an alternative reply-to address
$mail->addReplyTo('no-reply@my.ge', 'MY.GE');
//Set who the message is to be sent to

//Attach an image file

if ($bcc_address != '') {
    $mail->AddCC($bcc_address);
}
if ($cc_address != '') {
    $mail->AddBCC($cc_address);
}


$all_address = '';
for($i = 0; $i < count($address); $i++) {
    $add = $address[$i];
    
    $mail->addAddress($add);
    
    if ($all_address == '') {
        $all_address.=$add;
    }else{
        $all_address.=','.$add;
    }
    
}

$mail->Subject = $subject;
$mail->addAttachment($attachmet);
$mail->msgHTML($body);

//send the message, check for errors
if (!$mail->send()) {
    $status = 'false';
    $data 	 = array("status" => $status);
}else{
    $status	= 'true';
    $data 	= array("status" => $status);
    $user	= $_SESSION['USERID'];
    $c_date	= date('Y-m-d H:i:s');
}

if ($status	== 'true') {
    if ($source_id == 1) {
        $type=1;
    }elseif ($source_id == 2){
        $type=2;
    }else {
        $type = 0;
    }
    $db->setQuery("INSERT INTO `sent_mail`
                              (`id`,`user_id`, `date`, `address`, `cc_address`, `bcc_address`, `subject`, `body`, `attachment`, `type`, `status`, `actived`)
                        VALUES
                              ($hidden_increment,'$user', now(), '$all_address', '$cc_address', '$bcc_address', '$subject', '$body', '$attachmet', '$type', '1', '1')");
    
    $db->execQuery();
}

echo json_encode($data);