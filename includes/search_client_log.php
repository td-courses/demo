<?php
require_once 'classes/class.Mysqli.php';
global $db;
$db = new dbClass();

$action                  = $_REQUEST['act'];
$data                    = array();

$client_history_user_id  = $_REQUEST['client_history_user_id'];
$incomming_call_id       = $_REQUEST['incomming_call_id'];
$date_from               = $_REQUEST['date_from'];
$date_to                 = $_REQUEST['date_to'];
$user	                 = $_SESSION['USERID'];
switch ($action) {
    case 'search_transaction':
        
        $Curl = curl_init();
        curl_setopt($Curl, CURLOPT_URL, 'https://www.mymarket.ge/ka/callapp/transactions');
        curl_setopt($Curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($Curl, CURLOPT_POST, 1);
        curl_setopt($Curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($Curl, CURLOPT_POSTFIELDS, array('secret' => 'mGeLNeds6ARgQ4yrC', 'user_id' => $client_history_user_id, 'date_from' => $date_from, 'date_to' => $date_to));
        $Return = curl_exec($Curl);
        curl_close($Curl);
        $Return     = json_decode($Return, true);
        $cound_data = count($Return[Data]);
        
        foreach ($Return[Data] as $key => $value){
            $transaction_id = $value[transaction_id];
            $site_name      = $value[site_name];
            $comment        = $value[comment];
            $name           = $value[name];
            $product_id     = $value[product_id];
            $money_before   = $value[money_before];
            $money_after    = $value[money_after];
            $expense        = $value[expense];
            $quantity       = $value[quantity];
            $insert_date    = $value[insert_date];
            $my_user_id     = $value[user_id];
            
            $db->setQuery("INSERT INTO `my_transaction`
                                      (`id`,`user_id`, `datetime`, `incomming_call_id`, `my_user_id`, `site_name`, `name`, `product_id`, `money_before`, `money_after`, `expense`, `quantity`, `insert_date`, `comment`) 
                                VALUES 
                                      ($transaction_id,'$user', NOW(), '$incomming_call_id', '$my_user_id', '$site_name', '$name', '$product_id', '$money_before', '$money_after', '$expense', '$quantity', '$insert_date', '$comment')
                           ON DUPLICATE KEY UPDATE
                            `my_user_id`   = '$my_user_id',
                            `site_name`    = '$site_name',
                            `name`         = '$name',
                            `product_id`   = '$product_id',
                            `money_before` = '$money_before',
                            `money_after`  = '$money_after',
                            `expense`      = '$expense',
                            `quantity`     = '$quantity',
                            `insert_date`  = '$insert_date',
                            `comment`      = '$comment'");
            $db->execQuery();
        }
        
        $data = array('transaction'=>count($Return[Data]));
        break;
    case 'search_order':
        
        $Curl = curl_init();
        curl_setopt($Curl, CURLOPT_URL, 'https://www.mymarket.ge/ka/callapp/users');
        curl_setopt($Curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($Curl, CURLOPT_POST, 1);
        curl_setopt($Curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($Curl, CURLOPT_POSTFIELDS, array('secret' => 'mGeLNeds6ARgQ4yrC', 'user_id' => $client_history_user_id, 'date_from' => $date_from, 'date_to' => $date_to));
        $Return = curl_exec($Curl);
        curl_close($Curl);
        $Return     = json_decode($Return, true);
//         echo '<pre>';
//             var_dump($Return[Data]);
//         echo '<pre>';
        foreach ($Return[Data][0][Products] as $key => $value){
            foreach ($value AS $val){
                if ($key=='MYAUTO') {
                    $production_id = $val[car_id];
                }else{
                    $production_id      = $val[product_id];
                }
                $title       = $val[title];
                $create_date = $val[create_date];
                $update_date = $val[update_date];
                $end_date    = $val[end_date];
                $block_date  = $val[block_date];
                $vip_to      = $val[vip_to];
                $color_to    = $val[color_to];
                $vip         = $val[vip];
                $prom_color  = $val[prom_color];
                
                $db->setQuery("INSERT IGNORE INTO `my_order`
                                          (`user_id`, `datetime`, `incomming_call_id`, `site`, `my_user_id`, `production_id`, `title`, `create_date`, `update_date`, `end_date`, `block_date`, `vip_to`, `color_to`, `vip`, `prom_color`)
                                    VALUES
                                          ('$user', NOW(), '$incomming_call_id', '$key', '$client_history_user_id', '$production_id', '$title', '$create_date', '$update_date', '$end_date', '$block_date', '$vip_to', '$color_to', '$vip', '$prom_color')");
                $db->execQuery();
            }
        }
       
        $data = array('order'=>count($Return[Data]));
        break;
    default:
        $error = 'Action is Null';
}

echo json_encode($data);
?>