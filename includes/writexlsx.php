<?php
ini_set('memory_limit', '-1');
set_time_limit(0);
require_once('PHPExcel.php');
include('classes/core.php');

$objPHPExcel = new PHPExcel();
$data='';
$action = $_REQUEST['act'];
$file_name='';

if ($action=='create_excel_code') {

    
    $wb5   = '';
    $wb6   = '';
    $wb7   = '';

    $file_name='Campaign_Code_Template.xlsx';

    
    $b = 1;
    $objPHPExcel->getActiveSheet()->setCellValue("A".$b,'Mobile');
    $objPHPExcel->getActiveSheet()->setCellValue("B".$b,'CustomerID');
    $objPHPExcel->getActiveSheet()->setCellValue("C".$b,'CampaignCode');
    $objPHPExcel->getActiveSheet()->setCellValue("D".$b,'PinCode');
    $objPHPExcel->getActiveSheet()->setCellValue("E".$b,'UserName');
    $objPHPExcel->getActiveSheet()->setCellValue("F".$b,'FullName');
    $objPHPExcel->getActiveSheet()->setCellValue("G".$b,'PID');
    $objPHPExcel->getActiveSheet()->setCellValue("H".$b,'SameMobile');
    $objPHPExcel->getActiveSheet()->setCellValue("I".$b,'Language');
    $objPHPExcel->getActiveSheet()->setCellValue("J".$b,'PromeDate');

    
    $a = 3;
    

    
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="nova.xlsx"');
    header('Cache-Control: max-age=0');
}else if ($action=='create_excel') {


    $file_name='outgoing_voice.xlsx';
    $request_id = $_REQUEST['request_id'];

    $qer=create_excel($request_id);

    $b = 1;


   $row_excel = array();
   $column=array();
   $column[0]='A';
   $column[1]='B';
   $column[2]='C';
   $column[3]='D';
   $column[4]='E';
   $column[5]='F';
   $column[6]='G';
   $column[7]='H';
   $column[8]='I';
   $column[9]='J';
   $column[10]='K';
   $column[11]='L';
   $row_excel[0] = "ნომერი";
    $count = 1;
    while ($res = mysql_fetch_assoc($qer)) {
        $row_excel[$count] = $res['name'];
        $count++;
    }
    $b = 1;
    $c=0;


    foreach ($row_excel as $rx){

        $objPHPExcel->getActiveSheet()->setCellValue($column[$c].$b,$rx);

        $c++;
    }

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="DY_Report.xlsx"');
    header('Cache-Control: max-age=0');




}else if ($action=='clientonsite') {

    $start  = $_REQUEST['start'];
    $end    = $_REQUEST['end'];
    $status = $_REQUEST['status'];

    if ($status == 2) {
        $status_filt = "AND client_subscribtion.status=0 and client_subscribtion.disable_date BETWEEN '$start' and '$end'";
    }else{
        $status_filt = "AND client_subscribtion.status=1 and client_subscribtion.start_date BETWEEN '$start' and '$end'";
    }
    
    $res = mysql_query("SELECT      client_subscribtion.id,
                                    client_subscribtion.agreement_number,
                                    cash_machine.dy,
                                    client.rs_id,
                                    client.`name`,
                                    subscription_owner.`name`,
                                    persons.`name`,
                                    position.person_position,
                                    client_subscribtion.start_date,
                                    client_subscribtion.end_date,
                                    CONCAT(sub_duration.`name`, ' თვე') AS `period`,
                                    client_subscribtion.start_date AS `pay_date`,
                                    SUM(client_subscribtion_schedule.price) AS `price`,
                                    SUM(client_subscribtion_schedule.payed) AS `payed`,
                                    0-SUM(IF(client_subscribtion_schedule.`status`=0 AND DATE(client_subscribtion_schedule.end_date) <= CURDATE(),client_subscribtion_schedule.price,0)) AS `balance`,
                                    '' AS `pay_type`,
                                    IF(client_subscribtion.invoice = 1, 'კი', 'არა') AS 'invoice',
                                    CONCAT(cash_machine_address.street_name, ' ', street_number) AS `address`,
                                    region_district.name AS `district`,
                                    client.phone_number AS `phone`,
                                    client.mail AS `email`,
                                    client_subscribtion.disable_date,
                                    onsite_desable_reason.name,
                                    client_subscribtion.comment,
                                    COUNT(DISTINCT client_subscribtion.id) AS `raodenoba`
                        FROM        client_subscribtion
                        JOIN        client ON client.id = client_subscribtion.client_id
                        JOIN	    client_subscribtion_schedule ON client_subscribtion_schedule.client_subscription_id = client_subscribtion.id
                        LEFT JOIN   onsite_desable_reason ON onsite_desable_reason.id = client_subscribtion.disable_reason
                        JOIN	    cash_machine ON client_subscribtion.cash_machine_id = cash_machine.id
                        JOIN        sub_duration ON client_subscribtion.sub_duration_id = sub_duration.id
                        LEFT JOIN   cash_machine_address ON cash_machine.id = cash_machine_address.cash_machine_id
                        LEFT JOIN   region_district ON region_district.id = cash_machine_address.region_district_id
                        LEFT JOIN   subscription_owner ON subscription_owner.id = client_subscribtion.subscription_owner_id
                        LEFT JOIN   users ON users.id = client_subscribtion.agreement_author_id
                        LEFT JOIN   persons ON persons.id = users.person_id
                        LEFT JOIN   position ON position.id = persons.position
                        WHERE      `cash_machine`.`actived` = 1 AND client_subscribtion.subscribtion_id = 2 $status_filt
                        GROUP BY    client_subscribtion.agreement_number");
    
    $b = 1;
    
    $objPHPExcel->getActiveSheet()->setCellValue("A".$b,'ხელშეკრულების N');
    $objPHPExcel->getActiveSheet()->setCellValue("B".$b,'DY');
    $objPHPExcel->getActiveSheet()->setCellValue("C".$b,'სნ');
    $objPHPExcel->getActiveSheet()->setCellValue("D".$b,'დასახელება');
    $objPHPExcel->getActiveSheet()->setCellValue("E".$b,'პროვაიდერი');
    $objPHPExcel->getActiveSheet()->setCellValue("F".$b,'აგენტი');
    $objPHPExcel->getActiveSheet()->setCellValue("G".$b,'თანამდებობა');
    $objPHPExcel->getActiveSheet()->setCellValue("H".$b,'ხელშეკრულების გაფორმების თარიღი');
    $objPHPExcel->getActiveSheet()->setCellValue("I".$b,'ხელშეკრულების დასრულების თარიღი');
    $objPHPExcel->getActiveSheet()->setCellValue("J".$b,'პერიოდი');
    $objPHPExcel->getActiveSheet()->setCellValue("K".$b,'გადახდის თარიღი');
    $objPHPExcel->getActiveSheet()->setCellValue("L".$b,'გადასახდელი თანხა');
    $objPHPExcel->getActiveSheet()->setCellValue("M".$b,'გადახდილი თანხა');
    
    if ($status==1) {
        $objPHPExcel->getActiveSheet()->setCellValue("N".$b,'ბალანსი');
        $objPHPExcel->getActiveSheet()->setCellValue("O".$b,'გადახდის ტიპი');
        $objPHPExcel->getActiveSheet()->setCellValue("P".$b,'ფაქტურა');
        $objPHPExcel->getActiveSheet()->setCellValue("Q".$b,'მისამართი');
        $objPHPExcel->getActiveSheet()->setCellValue("R".$b,'უბანი');
        $objPHPExcel->getActiveSheet()->setCellValue("S".$b,'საკონტაქტო ტელეფონი');
        
        $objPHPExcel->getActiveSheet()->setCellValue("T".$b,'იმეილი');
        $objPHPExcel->getActiveSheet()->setCellValue("U".$b,'კომენტარი');
        $objPHPExcel->getActiveSheet()->setCellValue("V".$b,'რაოდენობა');
    }else{
        $objPHPExcel->getActiveSheet()->setCellValue("N".$b,'მისამართი');
        $objPHPExcel->getActiveSheet()->setCellValue("O".$b,'უბანი');
        $objPHPExcel->getActiveSheet()->setCellValue("P".$b,'საკონტაქტო ტელეფონი');
        
        $objPHPExcel->getActiveSheet()->setCellValue("Q".$b,'იმეილი');
        
        $objPHPExcel->getActiveSheet()->setCellValue("R".$b,'გაუქმების თარიღი');
        $objPHPExcel->getActiveSheet()->setCellValue("S".$b,'გაუქმების მიზეზი');
        $objPHPExcel->getActiveSheet()->setCellValue("T".$b,'კომენტარი');
        $objPHPExcel->getActiveSheet()->setCellValue("U".$b,'რაოდენობა');
    }
    
    
    
    $a = 3;
    
    while ($row=mysql_fetch_array($res)) {
        $objPHPExcel->getActiveSheet()->setCellValue("A".$a,$row[1]);
        $objPHPExcel->getActiveSheet()->setCellValue("B".$a,$row[2]);
        $objPHPExcel->getActiveSheet()->setCellValue("C".$a,$row[3]);
        $objPHPExcel->getActiveSheet()->setCellValue("D".$a,$row[4]);
        $objPHPExcel->getActiveSheet()->setCellValue("E".$a,$row[5]);
        $objPHPExcel->getActiveSheet()->setCellValue("F".$a,$row[6]);
        $objPHPExcel->getActiveSheet()->setCellValue("G".$a,$row[7]);
        $objPHPExcel->getActiveSheet()->setCellValue("H".$a,$row[8]);
        $objPHPExcel->getActiveSheet()->setCellValue("I".$a,$row[9]);
        $objPHPExcel->getActiveSheet()->setCellValue("J".$a,$row[10]);
        $objPHPExcel->getActiveSheet()->setCellValue("K".$a,$row[11]);
        $objPHPExcel->getActiveSheet()->setCellValue("L".$a,$row[12]);
        $objPHPExcel->getActiveSheet()->setCellValue("M".$a,$row[13]);
        if ($status==1) {
            $objPHPExcel->getActiveSheet()->setCellValue("N".$a,$row[14]);
            $objPHPExcel->getActiveSheet()->setCellValue("O".$a,$row[15]);
            $objPHPExcel->getActiveSheet()->setCellValue("P".$a,$row[16]);
            $objPHPExcel->getActiveSheet()->setCellValue("Q".$a,$row[17]);
            $objPHPExcel->getActiveSheet()->setCellValue("R".$a,$row[18]);
            $objPHPExcel->getActiveSheet()->setCellValue("S".$a,$row[19]);
            $objPHPExcel->getActiveSheet()->setCellValue("T".$a,$row[20]);
            $objPHPExcel->getActiveSheet()->setCellValue("U".$a,$row[23]);
            $objPHPExcel->getActiveSheet()->setCellValue("V".$a,$row[24]);
        }else{
            $objPHPExcel->getActiveSheet()->setCellValue("N".$a,$row[17]);
            $objPHPExcel->getActiveSheet()->setCellValue("O".$a,$row[18]);
            $objPHPExcel->getActiveSheet()->setCellValue("P".$a,$row[19]);
            $objPHPExcel->getActiveSheet()->setCellValue("Q".$a,$row[20]);
            $objPHPExcel->getActiveSheet()->setCellValue("R".$a,$row[21]);
            $objPHPExcel->getActiveSheet()->setCellValue("S".$a,$row[22]);
            $objPHPExcel->getActiveSheet()->setCellValue("T".$a,$row[23]);
            $objPHPExcel->getActiveSheet()->setCellValue("U".$a,$row[24]);
        }
            
        $a++;
    }
    
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="CLIENT_Report.xlsx"');
    header('Cache-Control: max-age=0');

}else if ($action=='insurance') {
    
    $start  = $_REQUEST['start'];
    $end    = $_REQUEST['end'];
    $status = $_REQUEST['status'];
    
    if ($status == 2) {
        $status_filt = "AND client_subscribtion.status=0 and client_subscribtion.disable_date BETWEEN '$start' and '$end'";
    }else{
        $status_filt = "AND client_subscribtion.status=1 and client_subscribtion.start_date BETWEEN '$start' and '$end'";
    }
    
    $res = mysql_query("SELECT      client_subscribtion.id,
                                    client_subscribtion.agreement_number,
                                    cash_machine.luq_number,
                                    cash_machine.dy,
                                    client.rs_id,
                                    client.`name`,
                                    subscription_owner.`name`,
                                    client_subscribtion.start_date,
                                    client_subscribtion.end_date,
                                    CONCAT(sub_duration.`name`, ' თვე') AS `period`,
                                    client_subscribtion.start_date AS `pay_date`,
                                    SUM(client_subscribtion_schedule.price) AS `price`,
                                    SUM(client_subscribtion_schedule.payed) AS `payed`,
                                    0-SUM(IF(client_subscribtion_schedule.`status`=0,client_subscribtion_schedule.price,0)) AS `balance`,
                                    '' AS `pay_type`,
                                    cash_machine_address.street_name AS `address`,
                                    region_district.name AS `district`,
                                    client.phone_number AS `phone`,
                                    client.mail AS `email`,
                                    client_subscribtion.disable_date,
                                    onsite_desable_reason.name,
                                    client_subscribtion.comment,
                                    persons.`name`,
                                    position.person_position
                        FROM        client_subscribtion
                        JOIN        client ON client.id = client_subscribtion.client_id
                        JOIN	    client_subscribtion_schedule ON client_subscribtion_schedule.client_subscription_id = client_subscribtion.id
                        LEFT JOIN   onsite_desable_reason ON onsite_desable_reason.id = client_subscribtion.disable_reason
                        JOIN	    cash_machine ON client_subscribtion.cash_machine_id = cash_machine.id
                        JOIN        sub_duration ON client_subscribtion.sub_duration_id = sub_duration.id
                        LEFT JOIN   cash_machine_address ON cash_machine.id = cash_machine_address.cash_machine_id
                        LEFT JOIN   subscription_owner ON subscription_owner.id = client_subscribtion.subscription_owner_id
                        LEFT JOIN   region_district ON region_district.id = cash_machine_address.region_district_id
                        LEFT JOIN   users ON users.id = client_subscribtion.agreement_author_id
                        LEFT JOIN   persons ON persons.id = users.person_id
                        LEFT JOIN   position ON position.id = persons.position
                        WHERE       `cash_machine`.`actived` = 1 AND client_subscribtion.subscribtion_id = 3 $status_filt 
                        GROUP BY    client_subscribtion_schedule.client_subscription_id");
    
    $b = 1;
    
    $objPHPExcel->getActiveSheet()->setCellValue("A".$b,'ხელშეკრულების N');
    $objPHPExcel->getActiveSheet()->setCellValue("B".$b,'ლუქის N');
    $objPHPExcel->getActiveSheet()->setCellValue("C".$b,'DY');
    $objPHPExcel->getActiveSheet()->setCellValue("D".$b,'სნ');
    $objPHPExcel->getActiveSheet()->setCellValue("E".$b,'დასახელება');
    $objPHPExcel->getActiveSheet()->setCellValue("F".$b,'პროვაიდერი');
    $objPHPExcel->getActiveSheet()->setCellValue("G".$b,'ხელშეკრულების გაფორმების თარიღი');
    $objPHPExcel->getActiveSheet()->setCellValue("H".$b,'ხელშეკრულების დასრულების თარიღი');
    $objPHPExcel->getActiveSheet()->setCellValue("I".$b,'პერიოდი');
    $objPHPExcel->getActiveSheet()->setCellValue("J".$b,'გადახდის თარიღი');
    $objPHPExcel->getActiveSheet()->setCellValue("K".$b,'გადასახდელი თანხა');
    $objPHPExcel->getActiveSheet()->setCellValue("L".$b,'გადახდილი თანხა');
    $objPHPExcel->getActiveSheet()->setCellValue("M".$b,'ბალანსი');
    $objPHPExcel->getActiveSheet()->setCellValue("N".$b,'გადახდის ტიპი');
    $objPHPExcel->getActiveSheet()->setCellValue("O".$b,'მისამართი');
    $objPHPExcel->getActiveSheet()->setCellValue("P".$b,'უბანი');
    $objPHPExcel->getActiveSheet()->setCellValue("Q".$b,'საკონტაქტო ტელეფონი');
    $objPHPExcel->getActiveSheet()->setCellValue("R".$b,'იმეილი');
    $objPHPExcel->getActiveSheet()->setCellValue("S".$b,'გაუქმების თარიღი');
    $objPHPExcel->getActiveSheet()->setCellValue("T".$b,'გაუქმების მიზეზი');
    $objPHPExcel->getActiveSheet()->setCellValue("U".$b,'კომენტარი');
    $objPHPExcel->getActiveSheet()->setCellValue("V".$b,'აგენტი');
    $objPHPExcel->getActiveSheet()->setCellValue("W".$b,'თანამდებობა');
    $a = 3;
    
    while ($row=mysql_fetch_array($res)) {
        $objPHPExcel->getActiveSheet()->setCellValue("A".$a,$row[1]);
        $objPHPExcel->getActiveSheet()->setCellValue("B".$a,$row[2]);
        $objPHPExcel->getActiveSheet()->setCellValue("C".$a,$row[3]);
        $objPHPExcel->getActiveSheet()->setCellValue("D".$a,$row[4]);
        $objPHPExcel->getActiveSheet()->setCellValue("E".$a,$row[5]);
        $objPHPExcel->getActiveSheet()->setCellValue("F".$a,$row[6]);
        $objPHPExcel->getActiveSheet()->setCellValue("G".$a,$row[7]);
        $objPHPExcel->getActiveSheet()->setCellValue("H".$a,$row[8]);
        $objPHPExcel->getActiveSheet()->setCellValue("I".$a,$row[9]);
        $objPHPExcel->getActiveSheet()->setCellValue("J".$a,$row[10]);
        $objPHPExcel->getActiveSheet()->setCellValue("K".$a,$row[11]);
        $objPHPExcel->getActiveSheet()->setCellValue("L".$a,$row[12]);
        $objPHPExcel->getActiveSheet()->setCellValue("M".$a,$row[13]);
        $objPHPExcel->getActiveSheet()->setCellValue("N".$a,$row[14]);
        $objPHPExcel->getActiveSheet()->setCellValue("O".$a,$row[15]);
        $objPHPExcel->getActiveSheet()->setCellValue("P".$a,$row[16]);
        $objPHPExcel->getActiveSheet()->setCellValue("Q".$a,$row[17]);
        $objPHPExcel->getActiveSheet()->setCellValue("R".$a,$row[18]);
        $objPHPExcel->getActiveSheet()->setCellValue("S".$a,$row[19]);
        $objPHPExcel->getActiveSheet()->setCellValue("T".$a,$row[20]);
        $objPHPExcel->getActiveSheet()->setCellValue("U".$a,$row[21]);
        $objPHPExcel->getActiveSheet()->setCellValue("V".$a,$row[22]);
        $objPHPExcel->getActiveSheet()->setCellValue("W".$a,$row[23]);
        
        $a++;
    }
    
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="DY_Report.xlsx"');
    header('Cache-Control: max-age=0');
    
    
    
    
}else if ($action=='clieninsurance') {
    
    $start  = $_REQUEST['start'];
    $end    = $_REQUEST['end'];
    $status = $_REQUEST['status'];
    
    if ($status == 2) {
        $status_filt = "AND client_subscribtion.status=0 and client_subscribtion.disable_date BETWEEN '$start' and '$end'";
    }else{
        $status_filt = "AND client_subscribtion.status=1 and client_subscribtion.start_date BETWEEN '$start' and '$end'";
    }
    
    $res = mysql_query("SELECT      client_subscribtion.id,
                                    client_subscribtion.agreement_number,
                                    cash_machine.luq_number,
                                    cash_machine.dy,
                                    client.rs_id,
                                    client.`name`,
                                    subscription_owner.`name`,
                                    client_subscribtion.start_date,
                                    client_subscribtion.end_date,
                                    CONCAT(sub_duration.`name`, ' თვე') AS `period`,
                                    client_subscribtion.start_date AS `pay_date`,
                                    SUM(client_subscribtion_schedule.price) AS `price`,
                                    SUM(client_subscribtion_schedule.payed) AS `payed`,
                                    0-SUM(IF(client_subscribtion_schedule.`status`=0,client_subscribtion_schedule.price,0)) AS `balance`,
                                    '' AS `pay_type`,
                                    cash_machine_address.street_name AS `address`,
                                    region_district.name AS `district`,
                                    client.phone_number AS `phone`,
                                    client.mail AS `email`,
                                    client_subscribtion.disable_date,
                                    onsite_desable_reason.name,
                                    client_subscribtion.comment,
                                    persons.`name`,
                                    position.person_position,
                                    COUNT(DISTINCT client_subscribtion.id) AS `raodenoba`
                        FROM        client_subscribtion
                        JOIN        client ON client.id = client_subscribtion.client_id
                        JOIN	    client_subscribtion_schedule ON client_subscribtion_schedule.client_subscription_id = client_subscribtion.id
                        LEFT JOIN   onsite_desable_reason ON onsite_desable_reason.id = client_subscribtion.disable_reason
                        JOIN	    cash_machine ON client_subscribtion.cash_machine_id = cash_machine.id
                        JOIN        sub_duration ON client_subscribtion.sub_duration_id = sub_duration.id
                        LEFT JOIN   cash_machine_address ON cash_machine.id = cash_machine_address.cash_machine_id
                        LEFT JOIN   subscription_owner ON subscription_owner.id = client_subscribtion.subscription_owner_id
                        LEFT JOIN   region_district ON region_district.id = cash_machine_address.region_district_id
                        LEFT JOIN   users ON users.id = client_subscribtion.agreement_author_id
                        LEFT JOIN   persons ON persons.id = users.person_id
                        LEFT JOIN   position ON position.id = persons.position
                        WHERE       `cash_machine`.`actived` = 1 AND client_subscribtion.subscribtion_id = 3 $status_filt 
                        GROUP BY    client_subscribtion.agreement_number");
    
    $b = 1;
    
    $objPHPExcel->getActiveSheet()->setCellValue("A".$b,'ხელშეკრულების N');
    $objPHPExcel->getActiveSheet()->setCellValue("B".$b,'ლუქის N');
    $objPHPExcel->getActiveSheet()->setCellValue("C".$b,'DY');
    $objPHPExcel->getActiveSheet()->setCellValue("D".$b,'სნ');
    $objPHPExcel->getActiveSheet()->setCellValue("E".$b,'დასახელება');
    $objPHPExcel->getActiveSheet()->setCellValue("F".$b,'პროვაიდერი');
    $objPHPExcel->getActiveSheet()->setCellValue("G".$b,'ხელშეკრულების გაფორმების თარიღი');
    $objPHPExcel->getActiveSheet()->setCellValue("H".$b,'ხელშეკრულების დასრულების თარიღი');
    $objPHPExcel->getActiveSheet()->setCellValue("I".$b,'პერიოდი');
    $objPHPExcel->getActiveSheet()->setCellValue("J".$b,'გადახდის თარიღი');
    $objPHPExcel->getActiveSheet()->setCellValue("K".$b,'გადასახდელი თანხა');
    $objPHPExcel->getActiveSheet()->setCellValue("L".$b,'გადახდილი თანხა');
    $objPHPExcel->getActiveSheet()->setCellValue("M".$b,'ბალანსი');
    $objPHPExcel->getActiveSheet()->setCellValue("N".$b,'გადახდის ტიპი');
    $objPHPExcel->getActiveSheet()->setCellValue("O".$b,'მისამართი');
    $objPHPExcel->getActiveSheet()->setCellValue("P".$b,'უბანი');
    $objPHPExcel->getActiveSheet()->setCellValue("Q".$b,'საკონტაქტო ტელეფონი');
    $objPHPExcel->getActiveSheet()->setCellValue("R".$b,'იმეილი');
    $objPHPExcel->getActiveSheet()->setCellValue("S".$b,'გაუქმების თარიღი');
    $objPHPExcel->getActiveSheet()->setCellValue("T".$b,'გაუქმების მიზეზი');
    $objPHPExcel->getActiveSheet()->setCellValue("U".$b,'კომენტარი');
    $objPHPExcel->getActiveSheet()->setCellValue("V".$b,'აგენტი');
    $objPHPExcel->getActiveSheet()->setCellValue("W".$b,'თანამდებობა');
    $objPHPExcel->getActiveSheet()->setCellValue("X".$b,'რაოდენობა');
    
    $a = 3;
    
    while ($row=mysql_fetch_array($res)) {
        $objPHPExcel->getActiveSheet()->setCellValue("A".$a,$row[1]);
        $objPHPExcel->getActiveSheet()->setCellValue("B".$a,$row[2]);
        $objPHPExcel->getActiveSheet()->setCellValue("C".$a,$row[3]);
        $objPHPExcel->getActiveSheet()->setCellValue("D".$a,$row[4]);
        $objPHPExcel->getActiveSheet()->setCellValue("E".$a,$row[5]);
        $objPHPExcel->getActiveSheet()->setCellValue("F".$a,$row[6]);
        $objPHPExcel->getActiveSheet()->setCellValue("G".$a,$row[7]);
        $objPHPExcel->getActiveSheet()->setCellValue("H".$a,$row[8]);
        $objPHPExcel->getActiveSheet()->setCellValue("I".$a,$row[9]);
        $objPHPExcel->getActiveSheet()->setCellValue("J".$a,$row[10]);
        $objPHPExcel->getActiveSheet()->setCellValue("K".$a,$row[11]);
        $objPHPExcel->getActiveSheet()->setCellValue("L".$a,$row[12]);
        $objPHPExcel->getActiveSheet()->setCellValue("M".$a,$row[13]);
        $objPHPExcel->getActiveSheet()->setCellValue("N".$a,$row[14]);
        $objPHPExcel->getActiveSheet()->setCellValue("O".$a,$row[15]);
        $objPHPExcel->getActiveSheet()->setCellValue("P".$a,$row[16]);
        $objPHPExcel->getActiveSheet()->setCellValue("Q".$a,$row[17]);
        $objPHPExcel->getActiveSheet()->setCellValue("R".$a,$row[18]);
        $objPHPExcel->getActiveSheet()->setCellValue("S".$a,$row[19]);
        $objPHPExcel->getActiveSheet()->setCellValue("T".$a,$row[20]);
        $objPHPExcel->getActiveSheet()->setCellValue("U".$a,$row[21]);
        $objPHPExcel->getActiveSheet()->setCellValue("V".$a,$row[22]);
        $objPHPExcel->getActiveSheet()->setCellValue("W".$a,$row[23]);
        $objPHPExcel->getActiveSheet()->setCellValue("X".$a,$row[24]);
        
        $a++;
    }
    
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="CLIENT_Report.xlsx"');
    header('Cache-Control: max-age=0');
    
}elseif ($action == 'clientloan'){
    
    $start  = $_REQUEST['start'];
    $end    = $_REQUEST['end'];
    $status = $_REQUEST['status'];
    
    if ($status == 2) {
        $status_filt = "cash_machine_loan_finish.status=0 and cash_machine_loan_finish.disable_date BETWEEN '$start' and '$end'";
    }else{
        $status_filt = "cash_machine_loan_finish.status=1 and cash_machine_loan_finish.start_date BETWEEN '$start' and '$end'";
    }
    
    $res = mysql_query("SELECT   cash_machine_loan_finish.id,
                                 cash_machine_loan_finish.agreement_number,
                                 cash_machine.dy,
                				 client.`rs_id`,
                				 client.`name`,
                				 cash_machine_loan_finish.start_date,
                				 cash_machine_loan_finish.end_date,
                				 CONCAT(cash_machine_loan_finish.period, ' თვე'),
                				(SELECT cash_machine_loan_detail_finish.pay_date
                				 FROM   cash_machine_loan_detail_finish 
                				 WHERE  cash_machine_loan_detail_finish.`status` = 0 
                				 AND cash_machine_loan_detail_finish.cash_machine_loan_id = cash_machine_loan_finish.id
                				 ORDER BY cash_machine_loan_detail_finish.pay_date ASC 
                				 LIMIT 1),
                				 SUM(cash_machine_loan_detail_finish.pay_amount) AS `price`,
                				 SUM(cash_machine_loan_detail_finish.payed_amount) AS `payed`,
                                 0-SUM(IF(cash_machine_loan_detail_finish.`status`=0 AND DATE(cash_machine_loan_detail_finish.pay_date) <= CURDATE(),cash_machine_loan_detail_finish.pay_amount,0)) AS `balance`,
                				 '' as pay_type,
                                 IF(cash_machine_loan_finish.invoice = 1,'კი','არა'),
                				 cash_machine_loan_finish.name_surname,
                				 cash_machine_loan_finish.personal_number,
                				 cash_machine_loan_finish.address,
                				 cash_machine_loan_finish.phone,
                				 persons.`name`,
                				 position.person_position
                        				 
                        FROM     cash_machine_loan_finish
                        JOIN     cash_machine_loan_detail_finish ON cash_machine_loan_finish.id = cash_machine_loan_detail_finish.cash_machine_loan_id
                        JOIN     client ON client.id = cash_machine_loan_finish.client_id
                        JOIN     cash_machine ON cash_machine.id = cash_machine_loan_finish.cash_machine_id
                        LEFT JOIN users ON users.id = cash_machine_loan_finish.user_id
                        LEFT JOIN persons ON users.person_id = persons.id
                        LEFT JOIN position ON position.id = persons.position
                        WHERE    $status_filt
                        GROUP BY cash_machine_loan_finish.cash_machine_id");
    
    $b = 1;
    
    $objPHPExcel->getActiveSheet()->setCellValue("A".$b,'ხელშეკრულების N');
    $objPHPExcel->getActiveSheet()->setCellValue("B".$b,'DY');
    $objPHPExcel->getActiveSheet()->setCellValue("C".$b,'სნ');
    $objPHPExcel->getActiveSheet()->setCellValue("D".$b,'დასახელება');
    $objPHPExcel->getActiveSheet()->setCellValue("E".$b,'ხელშეკრულების გაფორმების თარიღი');
    $objPHPExcel->getActiveSheet()->setCellValue("F".$b,'ხელშეკრულების დასრულების თარიღი');
    $objPHPExcel->getActiveSheet()->setCellValue("G".$b,'პერიოდი');
    $objPHPExcel->getActiveSheet()->setCellValue("H".$b,'გადახდის თარიღი');
    $objPHPExcel->getActiveSheet()->setCellValue("I".$b,'გადასახდელი თანხა');
    $objPHPExcel->getActiveSheet()->setCellValue("J".$b,'გადახდილი თანხა');
    $objPHPExcel->getActiveSheet()->setCellValue("K".$b,'ბალანსი');
    $objPHPExcel->getActiveSheet()->setCellValue("L".$b,'გადახდის ტიპი');
    $objPHPExcel->getActiveSheet()->setCellValue("M".$b,'ფაქტურა');
    $objPHPExcel->getActiveSheet()->setCellValue("N".$b,'სახელი, გვარი');
    $objPHPExcel->getActiveSheet()->setCellValue("O".$b,'პ/ნ');
    $objPHPExcel->getActiveSheet()->setCellValue("P".$b,'მისამართი');
    $objPHPExcel->getActiveSheet()->setCellValue("Q".$b,'ტელეფონი');
    $objPHPExcel->getActiveSheet()->setCellValue("R".$b,'აგენტი');
    $objPHPExcel->getActiveSheet()->setCellValue("S".$b,'თანამდებობა');
    
    $a = 3;
    
    while ($row=mysql_fetch_array($res)) {
        $objPHPExcel->getActiveSheet()->setCellValue("A".$a,$row[1]);
        $objPHPExcel->getActiveSheet()->setCellValue("B".$a,$row[2]);
        $objPHPExcel->getActiveSheet()->setCellValue("C".$a,$row[3]);
        $objPHPExcel->getActiveSheet()->setCellValue("D".$a,$row[4]);
        $objPHPExcel->getActiveSheet()->setCellValue("E".$a,$row[5]);
        $objPHPExcel->getActiveSheet()->setCellValue("F".$a,$row[6]);
        $objPHPExcel->getActiveSheet()->setCellValue("G".$a,$row[7]);
        $objPHPExcel->getActiveSheet()->setCellValue("H".$a,$row[8]);
        $objPHPExcel->getActiveSheet()->setCellValue("I".$a,$row[9]);
        $objPHPExcel->getActiveSheet()->setCellValue("J".$a,$row[10]);
        $objPHPExcel->getActiveSheet()->setCellValue("K".$a,$row[11]);
        $objPHPExcel->getActiveSheet()->setCellValue("L".$a,$row[12]);
        $objPHPExcel->getActiveSheet()->setCellValue("M".$a,$row[13]);
        $objPHPExcel->getActiveSheet()->setCellValue("N".$a,$row[14]);
        $objPHPExcel->getActiveSheet()->setCellValue("O".$a,$row[15]);
        $objPHPExcel->getActiveSheet()->setCellValue("P".$a,$row[16]);
        $objPHPExcel->getActiveSheet()->setCellValue("Q".$a,$row[17]);
        $objPHPExcel->getActiveSheet()->setCellValue("R".$a,$row[18]);
        $objPHPExcel->getActiveSheet()->setCellValue("S".$a,$row[19]);
        
        $a++;
    }
    
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="CLIENT_Report.xlsx"');
    header('Cache-Control: max-age=0');
    
}elseif ($action == 'clientloan1'){
    
    $start  = $_REQUEST['start'];
    $end    = $_REQUEST['end'];
    $status = $_REQUEST['status'];
    
    if ($status == 2) {
        $status_filt = "cash_machine_loan_finish.status=0 and cash_machine_loan_finish.disable_date BETWEEN '$start' and '$end'";
    }else{
        $status_filt = "cash_machine_loan_finish.status=1 and cash_machine_loan_finish.start_date BETWEEN '$start' and '$end'";
    }
    
    $res = mysql_query("SELECT     cash_machine_loan_finish.id,
                                   cash_machine_loan_finish.agreement_number,
                                   cash_machine.dy,
                                   client.`rs_id`,
                                   client.`name`,
                                   cash_machine_loan_finish.start_date,
                                   cash_machine_loan_finish.end_date,
                                   CONCAT(cash_machine_loan_finish.period, ' თვე'),
                                  (SELECT   cash_machine_loan_detail_finish.pay_date
                                   FROM     cash_machine_loan_detail_finish
                                   WHERE    cash_machine_loan_detail_finish.`status` = 0
                                   AND      cash_machine_loan_detail_finish.cash_machine_loan_id = cash_machine_loan_finish.id
                                   ORDER BY cash_machine_loan_detail_finish.pay_date ASC
                                   LIMIT 1),
                                   SUM(cash_machine_loan_detail_finish.pay_amount) AS `price`,
                                   SUM(cash_machine_loan_detail_finish.payed_amount) AS `payed`,
                                   0-SUM(IF(cash_machine_loan_detail_finish.`status`=0 AND DATE(cash_machine_loan_detail_finish.pay_date) <= CURDATE(),cash_machine_loan_detail_finish.pay_amount,0)) AS `balance`,
                                   '' as pay_type,
                                   IF(cash_machine_loan_finish.invoice = 1,'კი','არა'),
                                   cash_machine_loan_finish.name_surname,
                                   cash_machine_loan_finish.personal_number,
                                   cash_machine_loan_finish.address,
                                   cash_machine_loan_finish.phone,
                                   persons.`name`,
                                   position.person_position
                         FROM      cash_machine_loan_finish
                         JOIN      cash_machine_loan_detail_finish ON cash_machine_loan_finish.id = cash_machine_loan_detail_finish.cash_machine_loan_id
                         JOIN      client ON client.id = cash_machine_loan_finish.client_id
                         JOIN      cash_machine ON cash_machine.id = cash_machine_loan_finish.cash_machine_id
                         LEFT JOIN users ON users.id = cash_machine_loan_finish.user_id
                         LEFT JOIN persons ON users.person_id = persons.id
                         LEFT JOIN position ON position.id = persons.position
                         WHERE     $status_filt
                         GROUP BY  cash_machine_loan_finish.agreement_number");
    
    $b = 1;
    
    $objPHPExcel->getActiveSheet()->setCellValue("A".$b,'ხელშეკრულების N');
    $objPHPExcel->getActiveSheet()->setCellValue("B".$b,'DY');
    $objPHPExcel->getActiveSheet()->setCellValue("C".$b,'სნ');
    $objPHPExcel->getActiveSheet()->setCellValue("D".$b,'დასახელება');
    $objPHPExcel->getActiveSheet()->setCellValue("E".$b,'ხელშეკრულების გაფორმების თარიღი');
    $objPHPExcel->getActiveSheet()->setCellValue("F".$b,'ხელშეკრულების დასრულების თარიღი');
    $objPHPExcel->getActiveSheet()->setCellValue("G".$b,'პერიოდი');
    $objPHPExcel->getActiveSheet()->setCellValue("H".$b,'გადახდის თარიღი');
    $objPHPExcel->getActiveSheet()->setCellValue("I".$b,'გადასახდელი თანხა');
    $objPHPExcel->getActiveSheet()->setCellValue("J".$b,'გადახდილი თანხა');
    $objPHPExcel->getActiveSheet()->setCellValue("K".$b,'ბალანსი');
    $objPHPExcel->getActiveSheet()->setCellValue("L".$b,'გადახდის ტიპი');
    $objPHPExcel->getActiveSheet()->setCellValue("M".$b,'ფაქტურა');
    $objPHPExcel->getActiveSheet()->setCellValue("N".$b,'სახელი, გვარი');
    $objPHPExcel->getActiveSheet()->setCellValue("O".$b,'პ/ნ');
    $objPHPExcel->getActiveSheet()->setCellValue("P".$b,'მისამართი');
    $objPHPExcel->getActiveSheet()->setCellValue("Q".$b,'ტელეფონი');
    $objPHPExcel->getActiveSheet()->setCellValue("R".$b,'აგენტი');
    $objPHPExcel->getActiveSheet()->setCellValue("S".$b,'თანამდებობა');
    
    $a = 3;
    
    while ($row=mysql_fetch_array($res)) {
        $objPHPExcel->getActiveSheet()->setCellValue("A".$a,$row[1]);
        $objPHPExcel->getActiveSheet()->setCellValue("B".$a,$row[2]);
        $objPHPExcel->getActiveSheet()->setCellValue("C".$a,$row[3]);
        $objPHPExcel->getActiveSheet()->setCellValue("D".$a,$row[4]);
        $objPHPExcel->getActiveSheet()->setCellValue("E".$a,$row[5]);
        $objPHPExcel->getActiveSheet()->setCellValue("F".$a,$row[6]);
        $objPHPExcel->getActiveSheet()->setCellValue("G".$a,$row[7]);
        $objPHPExcel->getActiveSheet()->setCellValue("H".$a,$row[8]);
        $objPHPExcel->getActiveSheet()->setCellValue("I".$a,$row[9]);
        $objPHPExcel->getActiveSheet()->setCellValue("J".$a,$row[10]);
        $objPHPExcel->getActiveSheet()->setCellValue("K".$a,$row[11]);
        $objPHPExcel->getActiveSheet()->setCellValue("L".$a,$row[12]);
        $objPHPExcel->getActiveSheet()->setCellValue("M".$a,$row[13]);
        $objPHPExcel->getActiveSheet()->setCellValue("N".$a,$row[14]);
        $objPHPExcel->getActiveSheet()->setCellValue("O".$a,$row[15]);
        $objPHPExcel->getActiveSheet()->setCellValue("P".$a,$row[16]);
        $objPHPExcel->getActiveSheet()->setCellValue("Q".$a,$row[17]);
        $objPHPExcel->getActiveSheet()->setCellValue("R".$a,$row[18]);
        $objPHPExcel->getActiveSheet()->setCellValue("S".$a,$row[19]);
        
        $a++;
    }
    
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="CLIENT_Report.xlsx"');
    header('Cache-Control: max-age=0');
}elseif ($action == 'production_sale'){
    
    $start  = $_REQUEST['start'];
    $end    = $_REQUEST['end'];
    $status = $_REQUEST['status'];
    
    $res = mysql_query("SELECT      client_production_sale.id,
                                    client_production_sale.id,
                                    client.rs_id,
                                    object.`name`,
                                    persons.`name`,
                                    persons1.`name`,
                                    client_production_sale.datetime,
                                    GROUP_CONCAT(production.`name`),
                                    ROUND(SUM(client_production_sale_detail.quantity * client_production_sale_detail.price),2),
                                    IF(client_production_sale.sale_status = 0 || client_production_sale.sale_status = 2, 'გადახდილი', 'რეალიზაციის შემდგომ'),
                                    client_production_sale.`comment`
                        FROM        `client_production_sale`
                        JOIN        client_production_sale_detail ON client_production_sale.id = client_production_sale_detail.client_production_sale_id
                        JOIN        production ON production.id = client_production_sale_detail.production_id                                
                        JOIN        client ON client.id = client_production_sale.client_id
                        JOIN        object ON object.id = client_production_sale.object_id
                        JOIN        users ON users.id = client_production_sale.user_id
                        JOIN        persons ON persons.id = users.person_id
                        LEFT JOIN   users AS users1 ON users1.id = client_production_sale.sale_consultant_user_id
                        LEFT JOIN   persons AS persons1 ON persons1.id = users1.person_id
                        WHERE       client_production_sale.actived = 1
                        AND         client_production_sale.datetime BETWEEN '$start' AND '$end'
                        GROUP BY    client_production_sale.id");
    
    $b = 1;
    
    $objPHPExcel->getActiveSheet()->setCellValue("A".$b,'N');
    $objPHPExcel->getActiveSheet()->setCellValue("B".$b,'საიდენტიფიკაციო ნომერი');
    $objPHPExcel->getActiveSheet()->setCellValue("C".$b,'ობიექტი');
    $objPHPExcel->getActiveSheet()->setCellValue("D".$b,'თანამშრომელი');
    $objPHPExcel->getActiveSheet()->setCellValue("E".$b,'კონსულტანტი');
    $objPHPExcel->getActiveSheet()->setCellValue("F".$b,'თარიღი');
    $objPHPExcel->getActiveSheet()->setCellValue("G".$b,'პროდუქტი');
    $objPHPExcel->getActiveSheet()->setCellValue("H".$b,'თანხა');
    $objPHPExcel->getActiveSheet()->setCellValue("I".$b,'სტატუსი');
    $objPHPExcel->getActiveSheet()->setCellValue("J".$b,'კომენტარი');
    
    $a = 3;
    
    while ($row=mysql_fetch_array($res)) {
        $objPHPExcel->getActiveSheet()->setCellValue("A".$a,$row[1]);
        $objPHPExcel->getActiveSheet()->setCellValue("B".$a,$row[2]);
        $objPHPExcel->getActiveSheet()->setCellValue("C".$a,$row[3]);
        $objPHPExcel->getActiveSheet()->setCellValue("D".$a,$row[4]);
        $objPHPExcel->getActiveSheet()->setCellValue("E".$a,$row[5]);
        $objPHPExcel->getActiveSheet()->setCellValue("F".$a,$row[6]);
        $objPHPExcel->getActiveSheet()->setCellValue("G".$a,$row[7]);
        $objPHPExcel->getActiveSheet()->setCellValue("H".$a,$row[8]);
        $objPHPExcel->getActiveSheet()->setCellValue("I".$a,$row[9]);
        $objPHPExcel->getActiveSheet()->setCellValue("J".$a,$row[10]);
        
        $a++;
    }
    
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="CLIENT_Report.xlsx"');
    header('Cache-Control: max-age=0');
}elseif ($action == 'inc_ex'){
    $start  = $_REQUEST['start'];
    $end    = $_REQUEST['end'];
    $status = $_REQUEST['status'];
    
    $res = mysql_query("SELECT  client_transaction.id,
                                client_transaction.datetime,
                                transaction_type.name AS transaction_type_name,
                                client.`name`,
                                client_transaction.client_id,
                                client_transaction.pay_source,
                                paybox.`name` AS `regioni`,
                                '','','','',
                                client_transaction.pay_amount
                        FROM `client_transaction`
                        LEFT JOIN transaction_type ON transaction_type.id = client_transaction.transaction_type_id
                        LEFT JOIN client ON client_transaction.client_id = client.rs_id
                        LEFT JOIN cash_machine ON cash_machine.id = client_transaction.cash_machine_id
                        LEFT JOIN pay_method_name ON client_transaction.pay_method = pay_method_name.pay_method
                        LEFT JOIN paybox ON SUBSTR(transaction_id,1,8) = paybox.paybox_id
                        LEFT JOIN users ON users.id  = client_transaction.user_id
                        LEFT JOIN persons ON persons.id = users.person_id
                        WHERE DATE(client_transaction.datetime) >= '$start' 
                              AND DATE(client_transaction.datetime) <= '$end'
                              AND client_transaction.actived = 1
                              AND client_transaction.source_type_id IN (1, 2, 3, 6, 7, 8, 9, 10,11,12,20)
                              AND client_transaction.edit_type_id IN (1, 11)");
    
    $b = 1;
    
    $objPHPExcel->getActiveSheet()->setCellValue("A".$b,' თარიღი');
    $objPHPExcel->getActiveSheet()->setCellValue("B".$b,'პროდუქტის ან მომსახურების ტიპი ');
    $objPHPExcel->getActiveSheet()->setCellValue("C".$b,'კლიენტის სახელი');
    $objPHPExcel->getActiveSheet()->setCellValue("D".$b,'კლიენტის ID');
    $objPHPExcel->getActiveSheet()->setCellValue("E".$b,'გადახდის მეთოდი (თიბისი, ნოვა  ტერმინალი, ლიბერთი ბანკი, ვებ.გვერდი და ასე შემდეგ )');
    $objPHPExcel->getActiveSheet()->setCellValue("F".$b,'ოფისი,რეგიონები');
    $objPHPExcel->getActiveSheet()->setCellValue("G".$b,'თანამშრომელი, რომელმაც გაყიდა პროდუქტი ან სერვისი');
    $objPHPExcel->getActiveSheet()->setCellValue("H".$b,'თანამშრომლის პოზიცია , რომელმაც გაყიდა პროდუქტი ან სერვისი');
    $objPHPExcel->getActiveSheet()->setCellValue("I".$b,'დისტრიბიუტორის სახელი, რომელმაც გაყიდა პროდუქტი ან სერვისი');
    $objPHPExcel->getActiveSheet()->setCellValue("J".$b,'გაყიდვების აგენტი, რომელმაც გაყიდა პროდუქტი ან მომსახურება');
    $objPHPExcel->getActiveSheet()->setCellValue("K".$b,'შემოსავლის ოდენობა');
    
    $a = 3;
    
    while ($row=mysql_fetch_array($res)) {
        $objPHPExcel->getActiveSheet()->setCellValue("A".$a,$row[1]);
        $objPHPExcel->getActiveSheet()->setCellValue("B".$a,$row[2]);
        $objPHPExcel->getActiveSheet()->setCellValue("C".$a,$row[3]);
        $objPHPExcel->getActiveSheet()->setCellValue("D".$a,$row[4]);
        $objPHPExcel->getActiveSheet()->setCellValue("E".$a,$row[5]);
        $objPHPExcel->getActiveSheet()->setCellValue("F".$a,$row[6]);
        $objPHPExcel->getActiveSheet()->setCellValue("G".$a,$row[7]);
        $objPHPExcel->getActiveSheet()->setCellValue("H".$a,$row[8]);
        $objPHPExcel->getActiveSheet()->setCellValue("I".$a,$row[9]);
        $objPHPExcel->getActiveSheet()->setCellValue("J".$a,$row[10]);
        $objPHPExcel->getActiveSheet()->setCellValue("k".$a,$row[11]);
        
        $a++;
    }
    
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="CLIENT_Report.xlsx"');
    header('Cache-Control: max-age=0');
}elseif ($action == 'reserve_excel'){
    $tab  	= $_REQUEST['tab'];
    $where  = "";
    if($tab=='0') {
        $where  = "";
    }else {
        $where  = "AND object.id = $tab";
    }
    
    
    $res = mysql_query("SELECT  	reserve.id,
                                    production.code,
                					production.name AS name,   														
                					production_category.`name`,   	                                                
                					IF(SUM(reserve.quantity - reserve.sold) < production.min_amount,CONCAT('<div style=\"background:red;\">',SUM(reserve.quantity - reserve.sold) + 
                					IFNULL((SELECT SUM(transfer_detail.quantity) 
        									FROM   transfer
        									JOIN   transfer_detail ON transfer_detail.transfer_id = transfer.id 
        									WHERE  transfer.source_id = object.id 
        									AND transfer.`status` = 0
        									AND    transfer_detail.production_id = reserve.production_id),0),'</div>'),CONCAT(SUM(reserve.quantity - reserve.sold)+
                					IFNULL((SELECT SUM(transfer_detail.quantity) 
        									FROM   transfer
        									JOIN   transfer_detail ON transfer_detail.transfer_id = transfer.id 
        									WHERE  transfer.source_id = object.id 
        									AND transfer.`status` = 0
        									AND    transfer_detail.production_id = reserve.production_id),0))) as quantity,
                					IFNULL((SELECT SUM(transfer_detail.quantity) 
        									FROM   transfer
        									JOIN   transfer_detail ON transfer_detail.transfer_id = transfer.id 
        									WHERE  transfer.source_id = object.id 
        									AND    transfer.`status` = 0
        									AND    transfer_detail.production_id = reserve.production_id),0) AS qac_daudast,
                					production_unit.name,
                					ROUND(reserve.price , 2) AS cost,
                					ROUND((reserve.price * (SUM( reserve.quantity - reserve.sold ) + IFNULL(SUM(transfer_detail.quantity),0))) , 2) AS sumCost
                        FROM  		reserve
                        LEFT JOIN	object ON	reserve.object_id = object.id
                        LEFT JOIN production ON	reserve.production_id = production.id
                        LEFT JOIN production_unit ON production.unit = production_unit.id
                        LEFT JOIN production_category ON production_category.id=production.category
                        LEFT JOIN production_category AS PAR ON production_category.parent_id=PAR.id
                        LEFT JOIN production_category AS PAR1 ON PAR.parent_id=PAR1.id
                        LEFT JOIN transfer ON transfer.id = reserve.transfer_id AND transfer.`status` = 0
                        LEFT JOIN transfer_detail ON transfer.id = transfer_detail.transfer_id AND transfer_detail.quantity > 0
                        WHERE	  ROUND((reserve.quantity - reserve.sold + IFNULL((SELECT SUM(transfer_detail.quantity) 
																				   FROM   transfer
																				   JOIN   transfer_detail ON transfer_detail.transfer_id = transfer.id 
																				   WHERE  transfer.source_id = object.id 
																				   AND    transfer.`status` = 0
																				   AND    transfer_detail.production_id = reserve.production_id),0)),2) > 0.00 $where 
                       
                        GROUP BY  reserve.production_id");
    
    $b = 1;
    
    $objPHPExcel->getActiveSheet()->setCellValue("A".$b,'კოდი');
    $objPHPExcel->getActiveSheet()->setCellValue("B".$b,'პროდუქტი ');
    $objPHPExcel->getActiveSheet()->setCellValue("C".$b,'კატეგორია');
    $objPHPExcel->getActiveSheet()->setCellValue("D".$b,'რაოდენობა');
    $objPHPExcel->getActiveSheet()->setCellValue("E".$b,'გაცემული(დაუდასტურებელი)');
    $objPHPExcel->getActiveSheet()->setCellValue("F".$b,'ზომის ერთეული');
    $objPHPExcel->getActiveSheet()->setCellValue("G".$b,'თვითღირებულება');
    $objPHPExcel->getActiveSheet()->setCellValue("H".$b,'ღირებულება');
    
    $a = 3;
    
    while ($row=mysql_fetch_array($res)) {
        $objPHPExcel->getActiveSheet()->setCellValue("A".$a,$row[1]);
        $objPHPExcel->getActiveSheet()->setCellValue("B".$a,$row[2]);
        $objPHPExcel->getActiveSheet()->setCellValue("C".$a,$row[3]);
        $objPHPExcel->getActiveSheet()->setCellValue("D".$a,$row[4]);
        $objPHPExcel->getActiveSheet()->setCellValue("E".$a,$row[5]);
        $objPHPExcel->getActiveSheet()->setCellValue("F".$a,$row[6]);
        $objPHPExcel->getActiveSheet()->setCellValue("G".$a,$row[7]);
        $objPHPExcel->getActiveSheet()->setCellValue("H".$a,$row[8]);
        
        $a++;
    }
    
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="reserve.xlsx"');
    header('Cache-Control: max-age=0');
}else{
    $res = mysql_query("");
    
    $b = 1;
    
        $objPHPExcel->getActiveSheet()->setCellValue("A".$b,'DY კოდი');
        $objPHPExcel->getActiveSheet()->setCellValue("B".$b,'კლიენტის ნომერი');
        $objPHPExcel->getActiveSheet()->setCellValue("C".$b,'კლიენტის სახელი');
        $objPHPExcel->getActiveSheet()->setCellValue("D".$b,'კომპანიის ტიპი');
        $objPHPExcel->getActiveSheet()->setCellValue("E".$b,'სულ აპარატები');
        $objPHPExcel->getActiveSheet()->setCellValue("F".$b,'VIP');
        $objPHPExcel->getActiveSheet()->setCellValue("G".$b,'პასუხისმგებელი პირი');
        $objPHPExcel->getActiveSheet()->setCellValue("H".$b,'აპარატის მოდელი');
        $objPHPExcel->getActiveSheet()->setCellValue("I".$b,'აპარატის მდგომარეობა');
        $objPHPExcel->getActiveSheet()->setCellValue("J".$b,'მეორადის წყარო');
        $objPHPExcel->getActiveSheet()->setCellValue("K".$b,'რეგიონი რეგიონის კოდი');
        $objPHPExcel->getActiveSheet()->setCellValue("L".$b,'ს/ა მისამართი');
        $objPHPExcel->getActiveSheet()->setCellValue("M".$b,'საკონტაქტო პირი');
        $objPHPExcel->getActiveSheet()->setCellValue("N".$b,'ტელ #');
        $objPHPExcel->getActiveSheet()->setCellValue("O".$b,'ელ ფოსტა');
        $objPHPExcel->getActiveSheet()->setCellValue("P".$b,'სტატუსი SIM');
        $objPHPExcel->getActiveSheet()->setCellValue("Q".$b,'გათიშვის მიზეზი');
        $objPHPExcel->getActiveSheet()->setCellValue("R".$b,'გათიშვის თარიღი');
        $objPHPExcel->getActiveSheet()->setCellValue("S".$b,'დავალიანება');
        $objPHPExcel->getActiveSheet()->setCellValue("T".$b,'რეგისტრაციის თარიღი');
        $objPHPExcel->getActiveSheet()->setCellValue("U".$b,'ოპერატორი');
        $objPHPExcel->getActiveSheet()->setCellValue("V".$b,'რეგისტრაციის ადგილი');
        $objPHPExcel->getActiveSheet()->setCellValue("W".$b,'ბოლო გადახდა');
        $objPHPExcel->getActiveSheet()->setCellValue("X".$b,'გადახდილი პერიოდი');
        $objPHPExcel->getActiveSheet()->setCellValue("Y".$b,'გადახდის ვადა იწურება');
        $objPHPExcel->getActiveSheet()->setCellValue("Z".$b,'ბალანსი');
        $objPHPExcel->getActiveSheet()->setCellValue("AA".$b,'კონტრაქტის ვადა იწურება');
        $objPHPExcel->getActiveSheet()->setCellValue("AB".$b,'მოხსნის თარიღი');
        $objPHPExcel->getActiveSheet()->setCellValue("AC".$b,'მოხსნის მიზეზი');
        $objPHPExcel->getActiveSheet()->setCellValue("AD".$b,'ოპერატორი');
        $objPHPExcel->getActiveSheet()->setCellValue("AE".$b,'დერეგისტრაციის ადგილი');
        $objPHPExcel->getActiveSheet()->setCellValue("AF".$b,'განვადების ტიპი');
        $objPHPExcel->getActiveSheet()->setCellValue("AG".$b,'გაფორმების თარიღი');
        $objPHPExcel->getActiveSheet()->setCellValue("AH".$b,'განვადების ვადა');
        $objPHPExcel->getActiveSheet()->setCellValue("AI".$b,'თვიური შესატანი თანხა');
        $objPHPExcel->getActiveSheet()->setCellValue("AJ".$b,'ამოწურვის თარიღი');
        $objPHPExcel->getActiveSheet()->setCellValue("AK".$b,'შემდეგი გადახდის ვადა');
        $objPHPExcel->getActiveSheet()->setCellValue("AL".$b,'შენატანი');
        $objPHPExcel->getActiveSheet()->setCellValue("AM".$b,'ჯამში შეტანილი');
        $objPHPExcel->getActiveSheet()->setCellValue("AN".$b,'დარჩენილი დავალიანება');
        $objPHPExcel->getActiveSheet()->setCellValue("AO".$b,'შენიშვნები');
        $objPHPExcel->getActiveSheet()->setCellValue("AP".$b,'ადგილზე მომსახურება');
        $objPHPExcel->getActiveSheet()->setCellValue("AQ".$b,'პროვაიდერი');
        $objPHPExcel->getActiveSheet()->setCellValue("AR".$b,'გაფორმების თარიღი');
        $objPHPExcel->getActiveSheet()->setCellValue("AS".$b,'სულ აპარატები');
        $objPHPExcel->getActiveSheet()->setCellValue("AT".$b,'აგენტი');
        $objPHPExcel->getActiveSheet()->setCellValue("AU".$b,'თანამდებობა');
        $objPHPExcel->getActiveSheet()->setCellValue("AV".$b,'ოფისი');
        $objPHPExcel->getActiveSheet()->setCellValue("AW".$b,'გადახდის თარიღი');
        $objPHPExcel->getActiveSheet()->setCellValue("AX".$b,'გადასახდელი თანხა');
        $objPHPExcel->getActiveSheet()->setCellValue("AY".$b,'ბალანსი');
        $objPHPExcel->getActiveSheet()->setCellValue("AZ".$b,'გაუქმების თარიღი');
        $objPHPExcel->getActiveSheet()->setCellValue("BA".$b,'გაუქმების მიზეზი');
        $objPHPExcel->getActiveSheet()->setCellValue("BB".$b,'მობილური ოპერატორი');
        $objPHPExcel->getActiveSheet()->setCellValue("BC".$b,'ფისკალის ნომერი');
        $objPHPExcel->getActiveSheet()->setCellValue("BD".$b,'GSM ნომერი');
        $objPHPExcel->getActiveSheet()->setCellValue("BE".$b,'ICC');
        $objPHPExcel->getActiveSheet()->setCellValue("BF".$b,'MSI');
        $objPHPExcel->getActiveSheet()->setCellValue("BG".$b,'RS ID');
        $objPHPExcel->getActiveSheet()->setCellValue("BH".$b,'Import Date');
        $objPHPExcel->getActiveSheet()->setCellValue("BI".$b,'Import ID');
    
      $a = 3;
    while ($row=mysql_fetch_array($res)) {
      $objPHPExcel->getActiveSheet()->setCellValue("A".$a,$row[1]);
      $objPHPExcel->getActiveSheet()->setCellValue("B".$a,$row[2]);
      $objPHPExcel->getActiveSheet()->setCellValue("C".$a,$row[3]);
      $objPHPExcel->getActiveSheet()->setCellValue("D".$a,$row[4]);
      $objPHPExcel->getActiveSheet()->setCellValue("E".$a,$row[5]);
      $objPHPExcel->getActiveSheet()->setCellValue("F".$a,$row[6]);
      $objPHPExcel->getActiveSheet()->setCellValue("G".$a,$row[7]);
      $objPHPExcel->getActiveSheet()->setCellValue("H".$a,$row[8]);
      $objPHPExcel->getActiveSheet()->setCellValue("I".$a,$row[9]);
      $objPHPExcel->getActiveSheet()->setCellValue("J".$a,$row[10]);
      $objPHPExcel->getActiveSheet()->setCellValue("K".$a,$row[11]);
      $objPHPExcel->getActiveSheet()->setCellValue("L".$a,$row[12]);
      $objPHPExcel->getActiveSheet()->setCellValue("M".$a,$row[13]);
      $objPHPExcel->getActiveSheet()->setCellValue("N".$a,$row[14]);
      $objPHPExcel->getActiveSheet()->setCellValue("O".$a,$row[15]);
      $objPHPExcel->getActiveSheet()->setCellValue("P".$a,$row[16]);
      $objPHPExcel->getActiveSheet()->setCellValue("Q".$a,$row[17]);
      $objPHPExcel->getActiveSheet()->setCellValue("R".$a,$row[18]);
      $objPHPExcel->getActiveSheet()->setCellValue("S".$a,$row[19]);
      $objPHPExcel->getActiveSheet()->setCellValue("T".$a,$row[20]);
      $objPHPExcel->getActiveSheet()->setCellValue("U".$a,$row[21]);
      $objPHPExcel->getActiveSheet()->setCellValue("V".$a,$row[22]);
      $objPHPExcel->getActiveSheet()->setCellValue("W".$a,$row[23]);
      $objPHPExcel->getActiveSheet()->setCellValue("X".$a,$row[24]);
      $objPHPExcel->getActiveSheet()->setCellValue("Y".$a,$row[25]);
      $objPHPExcel->getActiveSheet()->setCellValue("Z".$a,$row[26]);
      $objPHPExcel->getActiveSheet()->setCellValue("AA".$a,$row[27]);
      $objPHPExcel->getActiveSheet()->setCellValue("AB".$a,$row[28]);
      $objPHPExcel->getActiveSheet()->setCellValue("AC".$a,$row[29]);
      $objPHPExcel->getActiveSheet()->setCellValue("AD".$a,$row[30]);
      $objPHPExcel->getActiveSheet()->setCellValue("AE".$a,$row[31]);
      $objPHPExcel->getActiveSheet()->setCellValue("AF".$a,$row[32]);
      $objPHPExcel->getActiveSheet()->setCellValue("AG".$a,$row[33]);
      $objPHPExcel->getActiveSheet()->setCellValue("AH".$a,$row[34]);
      $objPHPExcel->getActiveSheet()->setCellValue("AI".$a,$row[35]);
      $objPHPExcel->getActiveSheet()->setCellValue("AJ".$a,$row[36]);
      $objPHPExcel->getActiveSheet()->setCellValue("AK".$a,$row[37]);
      $objPHPExcel->getActiveSheet()->setCellValue("AL".$a,$row[38]);
      $objPHPExcel->getActiveSheet()->setCellValue("AM".$a,$row[39]);
      $objPHPExcel->getActiveSheet()->setCellValue("AN".$a,$row[40]);
      $objPHPExcel->getActiveSheet()->setCellValue("AO".$a,$row[41]);
      $objPHPExcel->getActiveSheet()->setCellValue("AP".$a,$row[42]);
      $objPHPExcel->getActiveSheet()->setCellValue("AQ".$a,$row[43]);
      $objPHPExcel->getActiveSheet()->setCellValue("AR".$a,$row[44]);
      $objPHPExcel->getActiveSheet()->setCellValue("AS".$a,$row[45]);
      $objPHPExcel->getActiveSheet()->setCellValue("AT".$a,$row[46]);
      $objPHPExcel->getActiveSheet()->setCellValue("AU".$a,$row[47]);
      $objPHPExcel->getActiveSheet()->setCellValue("AV".$a,$row[48]);
      $objPHPExcel->getActiveSheet()->setCellValue("AW".$a,$row[49]);
      $objPHPExcel->getActiveSheet()->setCellValue("AX".$a,$row[50]);
      $objPHPExcel->getActiveSheet()->setCellValue("AY".$a,$row[51]);
      $objPHPExcel->getActiveSheet()->setCellValue("AZ".$a,$row[52]);
      $objPHPExcel->getActiveSheet()->setCellValue("BA".$a,$row[53]);
      $objPHPExcel->getActiveSheet()->setCellValue("BB".$a,$row[54]);
      $objPHPExcel->getActiveSheet()->setCellValue("BC".$a,$row[55]);
      $objPHPExcel->getActiveSheet()->setCellValue("BD".$a,$row[56]);
      $objPHPExcel->getActiveSheet()->setCellValue("BE".$a,$row[57]);
      $objPHPExcel->getActiveSheet()->setCellValue("BF".$a,$row[58]);
      $objPHPExcel->getActiveSheet()->setCellValue("BG".$a,$row[59]);
      $objPHPExcel->getActiveSheet()->setCellValue("BH".$a,$row[60]);
      $objPHPExcel->getActiveSheet()->setCellValue("BI".$a,$row[61]);
      
      $a++;
    }
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="irp.xlsx"');
    header('Cache-Control: max-age=0');
}


$writerObject = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$writerObject->save($file_name);

$data['error'] = 'edede';
echo json_encode($data);

function create_excel($request_id)
{
    if (empty($_SESSION['last_id'])) {
        $qer = mysql_query("SELECT name from autocall_request_voice where request_id='$request_id' and actived='1' and voice_type_id <> '1'");

    } else {

        $qer = mysql_query("SELECT name
            from 
            autocall_request_voice
            where request_id= (SELECT MAX(request_id) FROM autocall_request_voice)
            and actived='1' 
            and voice_type_id <> '1'
         ");

    }


    return $qer;

}


?>
