<?php
    require_once 'classes/class.Mysqli.php';
    global $db;
    $db = new dbClass();
    
    
    $message      = htmlspecialchars($_REQUEST['smsText'], ENT_QUOTES);
    $userID       = $_SESSION['USERID'];
    $site         = $_REQUEST['site'];
    $smsAddressee = $_REQUEST['smsAddressee'];
    $name         = $_REQUEST['name'];
    $sms_event_id = $_REQUEST['sms_event_id'];
    $site_array   = explode(",",$site);
    
    ini_set('max_execution_time', 0);
    $send_status = 0;
    foreach ($site_array AS $val){
        $Curl = curl_init();
        curl_setopt($Curl, CURLOPT_URL, 'https://www.mymarket.ge/ka/callapp/sendmessage');
        curl_setopt($Curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($Curl, CURLOPT_POST, 1);
        curl_setopt($Curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($Curl, CURLOPT_POSTFIELDS, array('secret' => 'mGeLNeds6ARgQ4yrC', 'user_id'  => $smsAddressee, 'site'=>$val, 'message' => $message));
        
        $Return = curl_exec($Curl);
        $result = json_decode($Return, true);
       
        curl_close($Curl);
        
        $status = $result[StatusCode];
        
        if ($sms_event_id != '') {
            $db->setQuery("UPDATE `send_site` 
                              SET `user_id`      = '$userID',
                                  `date`         = NOW(),
                                  `site_user_id` = '$smsAddressee',
                                  `my_user_name` = '$name',
                                  `site`         = '$val',
                                  `content`      = '$message',
                                  `status`       = '$status'
                            WHERE `id`           = '$sms_event_id'");
        }else{
            $db->setQuery("INSERT INTO `send_site`
                                      (`user_id`, `date`, `site_user_id`, `my_user_name`, `site`, `content`, `status`, `actived`) 
                                VALUES 
                                      ('$userID', NOW(), '$smsAddressee', '$name', '$val', '$message', '$status', 1)");
        }
        $db->execQuery();
        
        if ($status == 1){
            $send_status = $status;
        }
    }
    
    $data = array('result' => $status);
    
    echo json_encode($data);
?>