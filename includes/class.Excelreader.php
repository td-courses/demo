<?php

class ExcelReader extends PHPExcel_IOFactory{
  
    public $filename;
    public $type;
    public $objReader;
    public $objPHPExcel;
    public $values;
  
    public function ExcelReader($filename){
  
      $this->filename     = $filename;
      $this->type         = PHPExcel_IOFactory::identify($this->filename);
      $this->objReader    = PHPExcel_IOFactory::createReader($this->type);
      $this->objPHPExcel  = $objReader->load($this->filename);
  
    }
  
    public function getValues(){
  
      $this->values = '';
  
      foreach ($this->$objPHPExcel->getWorksheetIterator() as $worksheet) {
        
          foreach($worksheet->toArray() as $row){
        
            $value .= '(NOW(),';
             
            $i = 0; 
            while ($i <= 16) {
        
              $cell = htmlentities($row[$i], ENT_QUOTES);
        
              if ($i == 16) {
                $value .= '\''.$cell.'\'';
              }else if($i == 9 || $i == 10){
                $datetime = dateConvert($cell);
                $value .= '\''.$datetime.'\',';
              }else{
                $value .= '\''.$cell.'\',';
              }
              
              $i++;
            }
        
            $value .= '),';
        
          }
        
          $values .= substr($value, 0, -1);
        
        }
  
    }
  }