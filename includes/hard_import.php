
<?php
require_once 'classes/class.Mysqli.php';
global $db;
$db = new dbClass();
require_once 'excel_reader2.php';

/* ******************************
 *	File Upload aJax actions
 * ******************************
 */

$action = $_REQUEST['act'];
$error	= '';
$data	= array();

switch ($action) {
	case 'upload_file':
		$element		= 'choose_file';
		$file_name		= $_REQUEST['file_name'];
		$type			= $_REQUEST['type'];
		$path			= $_REQUEST['path'];
		$sms_event_id	= $_REQUEST['sms_event_id'];
		$user_id		= $_SESSION['USERID'];
		$path			= $path . $file_name . '.' . $type;
		

		if (! empty ( $_FILES [$element] ['error'] )) {
			
			switch ($_FILES [$element] ['error']) {
				case '1' :
					
					 $error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
					break;
				case '2' :
					$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
					break;
				case '3' :
					$error = 'The uploaded file was only partially uploaded';
					break;
				case '4' :
					
					$error = 'No file was uploaded.';
					break;
				case '6' :
					$error = 'Missing a temporary folder';
					break;
				case '7' :
					$error = 'Failed to write file to disk';
					break;
				case '8' :
					$error = 'File upload stopped by extension';
					break;
				case '999' :
				default :
					$error = 'No error code avaiable';
					
			 }
			 
			} elseif (empty ( $_FILES [$element] ['tmp_name'] ) || $_FILES [$element] ['tmp_name'] == 'none') {
				$error = 'No file was uploaded..';
			} else {
	
			    $filename = $_FILES [$element] ['tmp_name'];
			
				$data = new Spreadsheet_Excel_Reader($filename);
				$r    = $data->rowcount($sheet_index=0); 
				$i    = 0;
				
				while (1!=$r){
					$status=1;
					
					if (strlen($data->val($r,'A'))!='' && strlen($data->val($r,'B')) != '' && (strlen($data->val($r,'C')) != '' || strlen($data->val($r,'D')) != '' || strlen($data->val($r,'E')) != '' || strlen($data->val($r,'F')) != '')){
					    $value.='(\''.$user_id.'\', \''.$sms_event_id.'\', \''.$data->val($r,'A').'\', \''.$data->val($r,'B').'\', \''.$data->val($r,'C').'\', \''.$data->val($r,'D').'\', \''.$data->val($r,'E').'\', \''.$data->val($r,'F').'\', 1, 1),';
					} 
					
					$r--;
				}
				
				$values = substr($value, 0, -1);
				
				$db->setQuery("INSERT INTO  `hard_site_event_detail`
                				           (`user_id`, `sms_event_id`, `site_user_id`, `site`, `text`, `text1`, `text2`, `text3`, `status`, `actived`)
                			         VALUES
                				           $values");
				$res = $db->execQuery();
				
				//if($res){
				    $data = array("status" =>1);
				//}
				    
				if (file_exists($path)){
					unlink($path);
				}
			}
        break;
    default:
        $error = 'Action is Null';
}
$data['error'] = $error;

echo json_encode($data);

?>