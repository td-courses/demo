<?php

    require_once 'classes/class.Mysqli.php';
    global $db;
    
    $db        = new dbClass();
    $user_id   = $_SESSION['USERID'];
    $send_type = $_REQUEST['send_type'];
    
    if ($send_type == 1) {
        
        $task_id           = $_REQUEST['task_id'];
        $task_type         = $_REQUEST['task_type'];
        $time              = $_REQUEST['time'];
        $site              = $_REQUEST['site'];
        $incomming_call_id = $_REQUEST['incomming_call_id'];
        $hidde_outgoing_id = $_REQUEST['hidde_outgoing_id'];
        $status_name       = $_REQUEST['status_name'];
        $task_recipient    = $_REQUEST['task_recipient'];
        $userID            = $_SESSION['USERID'];
        $type              = $_REQUEST['type'];
        
        
        $db->setQuery("SELECT mail from ( 
        
        
                        SELECT   user_info.mail
                        FROM     users_site
                        JOIN     users ON users.id = users_site.user_id
                        JOIN     user_info ON user_info.user_id = users.id
                        WHERE    site_id IN($site) AND NOT ISNULL(user_info.mail) AND users.id != '$task_recipient' AND user_info.send_mail = 1
                        GROUP BY user_info.mail
                        
                        UNION ALL

                        SELECT   user_info.mail
                        FROM     users_site
                        JOIN     users ON users.id = users_site.user_id
                        JOIN     user_info ON user_info.user_id = users.id
                        WHERE    site_id IN($site) AND NOT ISNULL(user_info.mail) AND users.id = '$_SESION[userid]' AND user_info.send_mail = 1
                        GROUP BY user_info.mail      

                        UNION ALL

                        SELECT   user_info.mail
                        FROM     users_site
                        JOIN     users ON users.id = users_site.user_id
                        JOIN     user_info ON user_info.user_id = users.id
                        WHERE    site_id IN($site) AND NOT ISNULL(user_info.mail) AND user_info.position_id = '21' AND user_info.send_mail = 1
                        GROUP BY user_info.mail        

                        UNION ALL

                        SELECT   user_info.mail
                        FROM     users_site
                        JOIN     users ON users.id = users_site.user_id
                        JOIN     user_info ON user_info.user_id = users.id
                        WHERE    site_id IN($site) AND NOT ISNULL(user_info.mail) AND user_info.position_id = '22' AND user_info.send_mail = 1
                        GROUP BY user_info.mail       

                        ) as q  GROUP BY mail      
                        
                        ");

        $check     = $db->getNumRow();
        $mail_info = $db->getResultArray();
        
        if ($type == 1) {
            if($task_type =="1") {
                $subject = 'ახალი დავალება';
                $message = 'დაემატა ახალი დავალება.<br> დავალების სტატუსი:'.$status_name.' <br> დავალების ნომერი:'.$task_id;
            }
            if($task_type =="2"){
                $subject = 'ახალი გამავალი ზარი';
                $message = 'დაემატა ახალი გამავალი ზარი.<br> გამავალი ზარის სტატუსი:'.$status_name.' <br> გამავალი ზარის ნომერი:'.$task_id;
            }
        
        } 
        if ($type == 2){

           $db->setQuery("SELECT collumn, old_value, new_value FROM `logs` WHERE row_id = '$task_id' AND `date` >= '$time' ");
           $logs = $db->getResultArray();
           $cols = '';
           foreach($logs[result] AS $log){
                $old_value = $log['old_value'];
                $new_value = $log['new_value'];
                $row = $log['collumn'];
                
                switch ($row){
                    case 'task_start_date' : $col = "პერიოდიდან";  break;
                    case 'task_end_date' : $col = "პერიოდამდე";  break;
                    case 'task_status_id' : $col = "სტატუსი"; $old_value = get_task_status($old_value); $new_value = get_task_status($new_value);  break;
                    case 'task_status_id_2' : $col = "სტატუსი 2"; $old_value = get_task_status($old_value); $new_value = get_task_status($new_value); break;
                    case 'task_branch_id' : $col = "განყოფილება"; $old_value = get_department($old_value); $new_value = get_department($new_value); break;
                    case 'task_recipient_id' : $col = "დავალების მიმღები"; $old_value = get_user($old_value); $new_value = get_user($new_value); break;
                    case 'task_note' : $col = "შედეგი";  break;
                    case 'task_description' : $col = "კომენტარი";  break;
                    case 'cat_1' : $col = "კატეგორია 1"; $old_value = get_category($old_value); $new_value = get_category($new_value); break;
                    case 'cat_1_1' : $col = "ქვე კატეგორია 1"; $old_value = get_category($old_value); $new_value = get_category($new_value); break;
                    case 'cat_1_1_1' : $col = "ქვე კატეგორია 2"; $old_value = get_category($old_value); $new_value = get_category($new_value); break;
                    case 'sites' : $col = "საიტი";break;
                }

                
                if($old_value != "" )
                    $cols .= " $col "." - ძველი მნიშვნელობა ".$old_value.",";
                if($new_value != "" )
                    $cols .= " $col "." - ახალი მნიშვნელობა ".$new_value.". <br>";
            }
            if ($task_type == "1"){
                $subject = 'დავალების ცვლილება';
                $message = 'შეიცვალა დავალება . შეცვლილი ველ(ებ)ი : '.$cols.' <br> დავალების სტატუსი:'.$status_name.' <br> დავალების ნომერი:'.$task_id;
            }
            if($task_type =="2"){
                $subject = 'გამავალი ზარის ცვლილება';
                $message = 'შეიცვალა გამავალი ზარი . შეცვლილი ველ(ებ)ი : '.$cols.' <br> გამავალი ზარის სტატუსი:'.$status_name.' <br> გამავალი ზარის ნომერი:'.$task_id;
            }
        }
        
        if ($check > 0) {
            foreach ($mail_info[result] AS $mail){
                $address = $mail[mail];
                
                $url     = "https://notifications.my.ge/ka/main/SendMail";
                
                $Curl = curl_init();
                curl_setopt($Curl, CURLOPT_URL, $url);
                curl_setopt($Curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($Curl, CURLOPT_POST, 1);
                curl_setopt($Curl, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($Curl, CURLOPT_POSTFIELDS, array(
                    'Secret'	 => 'mGeLNeds6ARgQ4yrC',
                    'To'		 => $address,
                    'ToName'	 => $address,
                    'From'       => 'no-reply@my.ge',
                    'FromName'   => 'My.ge',
                    'ReplyTo'    => 'no-reply@my.ge',
                    'Subject'	 => $subject,
                    'Message'	 => $message,
                    'IsHTML'     => 1
                ));
                
                $Return = curl_exec($Curl);
                $status = curl_getinfo($Curl, CURLINFO_HTTP_CODE);
                curl_close($Curl);
                //var_export($Return);
                $Return = json_decode($Return, true);
                
                $db->setQuery("INSERT INTO `sent_mail`
                                          (`incomming_call_id`, `outgoing_id`, `user_id`, `date`, `address`, `cc_address`, `bcc_address`, `subject`, `body`, `attachment`, `status`, `actived`)
                                    VALUES
                                          ('$incomming_call_id', '$hidde_outgoing_id', '$user_id', now(), '$address', '', '', '$subject', '$message', '', '$Return[Status]', '1')");
                
                $db->execQuery();
            }
        }
        
        $db->setQuery(" SELECT   user_info.mail
                        FROM     user_info
                        WHERE    NOT ISNULL(user_info.mail) AND user_info.user_id = '$task_recipient'");
        
        $check1     = $db->getNumRow();
        $mail_info1 = $db->getResultArray();
        
        if ($check1>0) {
            foreach ($mail_info1[result] AS $mail){
                $address = $mail[mail];
                
                $url     = "https://notifications.my.ge/ka/main/SendMail";
                
                $Curl = curl_init();
                curl_setopt($Curl, CURLOPT_URL, $url);
                curl_setopt($Curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($Curl, CURLOPT_POST, 1);
                curl_setopt($Curl, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($Curl, CURLOPT_POSTFIELDS, array(
                    'Secret'	 => 'mGeLNeds6ARgQ4yrC',
                    'To'		 => $address,
                    'ToName'	 => $address,
                    'From'       => 'no-reply@my.ge',
                    'FromName'   => 'My.ge',
                    'ReplyTo'    => 'no-reply@my.ge',
                    'Subject'	 => $subject,
                    'Message'	 => $message,
                    'IsHTML'     => 1
                ));
                
                $Return = curl_exec($Curl);
                $status = curl_getinfo($Curl, CURLINFO_HTTP_CODE);
                curl_close($Curl);
                //var_export($Return);
                $Return = json_decode($Return, true);
                
                $db->setQuery("INSERT INTO `sent_mail`
                                          (`incomming_call_id`, `outgoing_id`, `user_id`, `date`, `address`, `cc_address`, `bcc_address`, `subject`, `body`, `attachment`, `status`, `actived`)
                                    VALUES
                                          ('$incomming_call_id', '$hidde_outgoing_id', '$user_id', now(), '$address', '', '', '$subject', '$message', '', '$Return[Status]', '1')");
                
                $db->execQuery();
            }
        }
        
    }else{
        $message           = $_REQUEST['body'];
        $subject           = htmlspecialchars($_REQUEST['subject'], ENT_QUOTES);
        
        $attachments       = $_REQUEST['attachments'];
        $incomming_call_id = $_REQUEST['incomming_call_id'];
        $hidde_outgoing_id = $_REQUEST['hidde_outgoing_id'];
        $address           = $_REQUEST['address'];
        $from_mail_id      = $_REQUEST['from_mail_id'];
        $cc_address        = $_REQUEST['cc_address'];
        $bcc_address       = $_REQUEST['bcc_address'];
        $send_mail_id      = $_REQUEST['send_mail_id'];
        $hidden_increment  = $_REQUEST['hidden_increment'];
        
        $db->setQuery("SELECT `name`,
                        	   rand_name
                       FROM    file
                       WHERE   mail_id = '$hidden_increment' AND actived = 1");
        
        $res_attachment = $db->getResultArray();
        $attach         = "";
        $file_name      = "";
        
        foreach($res_attachment[result] AS $attachment){
            $attach .= "https://crm.my.ge/mailmyge/file_attachment/$attachment[rand_name];";
            $file_name .= $attachment[name].';';
        }
        
        $attach    = substr($attach, 0, -1);
        $file_name = substr($file_name, 0, -1);
        
        
        $db->setQuery(" SELECT signature 
                        FROM   signature 
                        WHERE  account_id = '$from_mail_id' AND actived = 1 LIMIT 1");
        
        $signature = $db->getResultArray();
        $mail_signature = html_entity_decode($signature[result][0][signature]);
        
        $res_reply = $db->setQuery("SELECT `name` 
                                    FROM   `mail_account`
                                    WHERE   id = '$from_mail_id'
                                    AND     actived = 1");
        
        $reply = $db->getResultArray();
        
        $userID  = $_SESSION['USERID'];
        $url     = "https://notifications.my.ge/ka/main/SendMail";
        
        $Curl = curl_init();
        curl_setopt($Curl, CURLOPT_URL, $url);
        curl_setopt($Curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($Curl, CURLOPT_POST, 1);
        curl_setopt($Curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($Curl, CURLOPT_POSTFIELDS, array(
            'Secret'	 => 'mGeLNeds6ARgQ4yrC',
            'To'		 => $address,
            'ToName'	 => $address,
            'From'       => $reply[result][0][name],
            'FromName'   => 'My.ge',
            'ReplyTo'    => $reply[result][0][name],
            'CC'         => $cc_address,
            'BCC'        => $bcc_address,
            'Subject'	 => $subject,
            'Message'	 => urldecode($message.'<br><br><p>'.$mail_signature.'</p>'),
            'Attach'     => $attach,
            'AttachName' => $file_name,
            //'Embedd'     => 'https://www.myauto.ge/templates/assets/img/img/cards.png?v=2',
            'IsHTML'     => 1
        ));
        
        
        $Return = curl_exec($Curl);
        $status = curl_getinfo($Curl, CURLINFO_HTTP_CODE);
        curl_close($Curl);
        //var_export($Return);
        $Return = json_decode($Return, true);
        
        $db->setQuery("SELECT id
                       FROM sent_mail
                       WHERE id = '$hidden_increment'");
        $check = $db->getNumRow();
        
        if ($check != 0) {
            $db->setQuery("UPDATE `sent_mail` 
                              SET `date`        = NOW(),
                                  `address`     = '$address',
                                  `cc_address`  = '$cc_address',
                                  `bcc_address` = '$bcc_address',
                                  `subject`     = '$subject',
                                  `body`        = '$message',
                                  `status`      = '$Return[Status]'
                           WHERE  `id`          = '$hidden_increment'");
        }else{
            $db->setQuery("INSERT INTO `sent_mail`
                                      (`id`, `incomming_call_id`, `outgoing_id`, `user_id`, `date`, `address`, `cc_address`, `bcc_address`, `subject`, `body`, `attachment`, `status`, `actived`)
                               VALUES
                                      ($hidden_increment, '$incomming_call_id', '$hidde_outgoing_id', '$user_id', now(), '$address', '$cc_address', '$bcc_address', '$subject', '$message', '', '$Return[Status]', '1')");
        }
        
        $db->execQuery();
    }
    
    $data = array('status' => $Return[Status]);
    echo json_encode($data);


    function get_task_status($id){
        global $db;
        $db->setQuery("SELECT `name` FROM `task_status` WHERE id = '$id' ");
        $res = $db->getResultArray();

        return $res[result][0]['name'];

    }

    function get_department($id){
        global $db;
        $db->setQuery("SELECT `name` FROM `department` WHERE id = '$id' ");
        $res = $db->getResultArray();

        return $res[result][0]['name'];

    }

    function get_user($id){
        global $db;
        $db->setQuery("SELECT `name` FROM `user_info` WHERE `user_id` = '$id' ");
        $res = $db->getResultArray();

        return $res[result][0]['name'];

    }

    function get_category($id){
        global $db;
        $db->setQuery("SELECT `name` FROM `info_category` WHERE id = '$id' ");
        $res = $db->getResultArray();

        return $res[result][0]['name'];

    }
?>