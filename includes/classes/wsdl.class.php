<?php

ini_set("soap.wsdl_cache_enabled", 0);

class wsdl {
	
    private $client;
    private $requestInfo;
    private $URL;
	private $CallerId;		
	private $CallerHashCode; 
	private $RemoteAddress;	
	private $RemoteUserName;
	private $PageUrl;
	
	/**
	 * კონსტრუქტორი
	 * 
	 * @param unknown $URL
	 * @param unknown $CallerId
	 * @param unknown $CallerHashCode
	 * @param unknown $RemoteAddress
	 * @param unknown $RemoteUserName
	 * @param unknown $PageUrl
	 */ 
	public function wsdl($URL, $CallerId, $CallerHashCode, $RemoteAddress, $RemoteUserName, $PageUrl){
		
	    $this->URL                          = $URL;
	    $this->CallerId                     = $CallerId;                         
		$this->CallerHashCode               = $CallerHashCode;
		$this->RemoteAddress                = $RemoteAddress;
		$this->RemoteUserName               = $RemoteUserName;
		$this->PageUrl                      = $PageUrl;
		$this->client                       = new  SoapClient($URL);
		
		$this->requestInfo				    = new stdClass();
		 
		$this->requestInfo->CallerId 		= $this->CallerId;
		$this->requestInfo->CallerHashCode  = $this->CallerHashCode;
		$this->requestInfo->RemoteAddress   = $this->RemoteAddress;
		$this->requestInfo->RemoteUserName  = $this->RemoteUserName;
		$this->requestInfo->PageUrl         = $this->PageUrl;
		
	}
	
	public function check(){
	    return print_r($this->client->__getFunctions());
	}
	
	/**
	 * მომხმარებლის აუთენთიფიკაცია
	 * 
	 * @param unknown $username
	 * @param unknown $password
	 */
	public function AuthenticateUser($username, $password){
	 
	
	$params 				  = new stdClass();
	$params->requestInfo      = $this->requestInfo;
	$params->username         = $username;
	$params->password         = $password;
	
	
	$results                  = $this->client->AuthenticateUser($params);
	return $results->AuthenticateUserResult;
	
	}
	
	/**
	 * კლიენტის ძიება პინით
	 * 
	 * @param int $clientNumber
	 * @param int $personalNumber
	 */
	public function SearchClient($clientNumber, $personalNumber){
	    
		$params 				  = new stdClass();
		$params->requestInfo      = $this->requestInfo;
		$params->clientNumber     = $clientNumber;
		$params->personalNumber   = $personalNumber;
		
		$results                  = $this->client->SearchClient($params);		
		return $results->SearchClientResult;
		
	}
	
	/**
	 * კლიენტის ძიება ტელეფონით
	 * 
	 * @param unknown $clientNumber
	 * @param unknown $personalNumber
	 */
	public function SearchClientByPhone($clientPhone){
	     
	    $params 				  = new stdClass();
	    $params->requestInfo      = $this->requestInfo;
	    $params->phoneNumber      = $clientPhone;
	
	    $results                  = $this->client->SearchClientByPhone($params);
	    
	    return array($results->SearchClientByPhoneResult->FoundClient, $results->SearchClientByPhoneResult->ClientPublicKey);
	
	}

	/**
	 * SMS -ის გაგზავნა
	 * 
	 * @param unknown $clientPublicKey
	 * @param unknown $message
	 */
	public function SendSms($clientPublicKey, $message){
	     
	    $params						= new stdClass();
	    $params->requestInfo		= $this->requestInfo;
		$params->clientPublicKey	= $clientPublicKey;
		$params->message			= $message;
	
	    $results					= $this->client->SendSms($params);
	    
	    return $results->SendSmsResult;
	
	}
	
	/**
	 * გადამისამრთების გენერირება
	 * 
	 * @param unknown $clientPublicKey
	 */
	public function GenerateRedirect($clientPublicKey){
	
	    $params 				  = new stdClass();
	    $params->requestInfo      = $this->requestInfo;
	    $params->clientPublicKey  = $clientPublicKey;
	
	    $results                  = $this->client->GenerateRedirectRequest($params);
	    return $results->GenerateRedirectRequestResult;
	
	}
		
	/**
	 * იუზერების სია
	 */
	public function GetUserList(){
	
	    $params 				  = new stdClass();
	    $params->requestInfo      = $this->requestInfo;
	
	    $results                  = $this->client->GetUserList($params);
	    return $results->GetUserListResult;
	
	}
	
	/**
	 * ჯგუფების სია
	 */
	public function GetRolesList(){
	
	    $params 				  = new stdClass();
	    $params->requestInfo      = $this->requestInfo;
	    
	    $results                  = $this->client->GetRolesList($params);
	    
	    $count   =  count($results->GetRolesListResult -> ApplicationRole);
	    
	    $roles = array();

	    for($i = 0; $i < $count; $i++){
	        
	        $roles[$i][RoleId]     = $results -> GetRolesListResult -> ApplicationRole[$i] -> RoleId;
	        $roles[$i][RoleName]   = $results -> GetRolesListResult -> ApplicationRole[$i] -> RoleName;
	    
	    }
	    
	    return $roles;
	
	}
	
	public function GetUserInfo($user_id){
	
		$params 				  = new stdClass();
		$params->requestInfo      = $this->requestInfo;
		$params->userId      	  = $user_id;
		
		$results                  = $this->client->GetUserInfo($params);
		
		$user_info = array();
		
		$user_info[Image]     	 = "data:image/png;base64, ".base64_encode($results -> GetUserInfoResult ->  Image);
		$user_info[Comment] 	 = $results -> GetUserInfoResult ->  Comment;
		$user_info[Mobile]  	 = $results -> GetUserInfoResult ->  Mobile;
		$user_info[Phone]   	 = $results -> GetUserInfoResult ->  Phone;
		$user_info[Address] 	 = $results -> GetUserInfoResult ->  Address;
		$user_info[PositionName] = $results -> GetUserInfoResult ->  PositionName;
		$user_info[username] 	 = $results -> GetUserInfoResult ->  username;
		
		return $user_info;
	
	}
	
	public function GetActiveConversations(){
	    
	    $params 				  = new stdClass();
	    $params->requestInfo      = $this->requestInfo;
	    $results = $this->client->GetActiveConversations($params);
	    $activ_conversation = $results->GetActiveConversationsResult;
	    $json      = json_encode($activ_conversation);
	    $resArray  = json_decode($json,TRUE);
	    return $resArray['Conversation'];
	    
	}
	
	public function GetConversationMessages($id){
	    
	    $params 				= new stdClass();
	    $params->requestInfo    = $this->requestInfo;
	    $params->conversationId = $id;
	    $results                = $this->client->GetConversationMessages($params);
	    $conversation_sessages  = $results->GetConversationMessagesResult;
	    $json                   = json_encode($conversation_sessages);
	    $resArray               = json_decode($json,TRUE);
	    
	    return $resArray[ConversationMessage];
	    
	}
	
	public function ReplyMessage($user_id,$message,$chat_id){
	    
	    $params 		     = new stdClass();
	    $params->requestInfo = $this->requestInfo;
	    $params->messageId   = $chat_id;
	    $params->replyText   = $message;
	    $params->userId      = $user_id;
	    
	    $results                = $this->client->ReplyMessage($params);
	    $check = $results->ReplyMessageResponse;
	    if ($check) {
	        $status = 1;
	    }else{
	        $status = 1;
	    }
	    
	    return $status;
	    
	}
	
	public function FinishConversation($user_id,$chat_id){
	    
	    $params 		        = new stdClass();
	    $params->requestInfo    = $this->requestInfo;
	    $params->conversationId = $chat_id;
	    $params->userId         = $user_id;
	    $params->isSpam         = 0;
	    
	    $this->client->FinishConversation($params);
	    
	    return $params;
	}
	
	public function IsSpam($user_id,$chat_id,$isspan){
	    
	    $params 		        = new stdClass();
	    $params->requestInfo    = $this->requestInfo;
	    $params->conversationId = $chat_id;
	    $params->userId         = $user_id;
	    $params->isSpam         = $isspan;
	    
	    $this->client->FinishConversation($params);
	    
	    return $params;
	}
	
}

?>