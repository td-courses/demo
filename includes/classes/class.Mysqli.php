<?php
session_set_cookie_params(60 * 60 * 24 * 7, "/");
session_start();
date_default_timezone_get('Asia/Tbilisi');
//require_once 'config.php';
define("MYSQLHOST",     "localhost");
define("MYSQLPORT",     "3306");
define("MYSQLUSER",     "root");
define("MYSQLPASS",     "rootx");
define("MYSQLDB",       "tdg");
define("MYSQLCHARSET",  "utf8mb4");
error_reporting(E_ERROR | E_PARSE);


class  dbClass
{

    public $mysqli;

    public $host;
    public $port;
    public $user;
    public $password;
    public $db;

    public $charset;

    public $query;

    public $curTable;

    /**CONSTRUCTOR
     * 
     * @param string  $host
     * @param string  $user
     * @param string  $password
     * @param string  $db
     * @param integer $port
     * @param string  $charset
     * @throws Exception
     */
    public function __construct($host = MYSQLHOST, $user = MYSQLUSER, $password = MYSQLPASS, $db = MYSQLDB, $port = MYSQLPORT, $charset = MYSQLCHARSET)
    {

        $this->host     = $host;
        $this->port     = $port;
        $this->user     = $user;
        $this->password = $password;
        $this->db       = $db;

        $this->charset  = $charset;

        if (empty($this->host) || empty($this->user) || empty($this->password) || empty($this->db)) {
            throw new Exception('Empty Parameters');
        }

        $this->mysqli = new mysqli($this->host, $this->user, $this->password, $this->db, $this->port);

        if ($this->mysqli->connect_error) {
            throw new Exception('Connect Error ' . $this->mysqli->connect_errno . ': ' . $this->mysqli->connect_error);
        }

        if ($this->mysqli) {
            $this->mysqli->set_charset($this->charset);
        } else {
            throw new Exception('SET Charset ERROR');
        }
    }

    /**SET QUERY
     * 
     * @param string $query
     */
    function setQuery($query, $table = '')
    {
        if ($table != '') {
            $this->curTable = $table;
        }
        if ($query == '') {
            throw new Exception('MySQLi query empty');
        } else {
            $this->query = $query;
        }
    }

    /**Get Last INSERT Id
     *
     * 
     */
    function getLastId()
    {
        return $this->mysqli->insert_id;
    }

    /**EXECUTE MYSQL QUERY
     * 
     * @throws Exception
     */
    function execQuery()
    {

        $this->mysqli->query($this->query);

        if ($this->mysqli->error) {
            throw new Exception($this->mysqli->error . " ::: Query -> " . $this->query);
        } else {
            return $this->mysqli->info;
        }
    }


    /**GET QUERY RESULT NUMERIC ARRAY
     *
     * @param INT $type
     * @throws Exception
     * @return array $fetchArray
     */
    function getResultArray($type = MYSQLI_ASSOC)
    {

        $fetchArray = array();
        $result     = $this->mysqli->query($this->query);

        if ($result) {
            $start = microtime();
            $count = 0;

            $fetchArray['result'] = NULL;
            while ($row = $result->fetch_array($type)) {
                $fetchArray['result'][] = $row;
                $count++;
            }

            if (!empty($this->curTable)) {
                $request = $this->mysqli->query("SHOW COLUMNS FROM " . $this->curTable);
                if ($request) {
                    $i = 0;
                    while ($r = $request->fetch_array($type)) {

                        if (strpos($r['Type'], 'int') !== false)                            ///
                        {                                                                ///
                            $fetchTableColumns[] = 'number';                            /// MAPPING OF DATA TYPES KENDOUI
                        }                                                                ///
                        else if (strpos($r['Type'], 'varchar') !== false) {                ///
                            $fetchTableColumns[] = 'string';
                        } else if (strpos($r['Type'], 'date') !== false) {
                            $fetchTableColumns[] = 'date';
                        }
                        $i++;
                    }
                } else {
                    $fetchTableColumns = 'FAILED MYSQLI->QUERY';
                }
            } else {
                $fetchTableColumns = 'Empty table name';
            }

            $columns = array();
            $cNum = $result->field_count;

            while ($cRow = $result->fetch_field()) {
                $columns[] = $cRow;
            }



            $fetchArray['data_type'] = $fetchTableColumns;
            $fetchArray['time']  = round(microtime() - $start, 4);
            $fetchArray['count'] = $count;

            for ($n = 0; $n < $cNum; $n++) {
                $fetchArray['columns'][$n] = $columns[$n]->name;
            }


            return $fetchArray;
        } else {
            self::disconnect();
            throw new Exception($this->mysqli->error . " ::: Query -> " . $this->query);
        }
    }

    /**GET JSON FROM ASSOC ARRAY RESULT
     *
     * @param INT $type
     * @return json $array
     */
    function getJson($type = ASSOC)
    {

        if ($type == ASSOC) {
            $array = self::getResultArray(MYSQLI_ASSOC);
        } elseif ($type == NUM) {
            $array = self::getResultArray(MYSQLI_NUM);
        } else {
            throw new Exception('Unknow type!');
        }

        $encodedArray   = json_encode($array, JSON_NUMERIC_CHECK);

        if ($encodedArray) {
            return $encodedArray;
        } else {
            throw new Exception('JSON ecode failed!');
        }
    }

    /**GET CONVERT IN EXCEL FROM ASSOC ARRAY RESULT
     *
     * @param Excel Object
     * @return Attachment
     */
    // ################################################################endregion

    function getExcelExport()
    {
        $ExcelObject = new PHPExcel();

        $data = $this->getResultArray()["result"];
        $columns = $this->getResultArray()["columns"];

        $ExcelObject->setActiveSheetIndex(0);

        $column = 0;

        foreach ($columns as $f) {
            $ExcelObject->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $f);
            $column++;
        }

        $excel_row = 2;

        if ($data !== null) {

            foreach ($data as $row) {

                foreach ($columns as $cn => $c) {
                    $ExcelObject->getActiveSheet()->setCellValueByColumnAndRow($cn, $excel_row, $row[$c]);
                }
                $excel_row++;
            }

            $object_Writer = PHPExcel_IOFactory::createWriter($ExcelObject, 'Excel5');
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="Export_data.xls"');
            $object_Writer->save('php://output');
        }
    }

    // ################################################################

    function getExcelImport($tmp_file)
    {

        $tmp_name = $tmp_file['name'];
        $ext = end((explode(".", $tmp_name)));
        $tmp = $tmp_file['tmp_name'];

        $allowTypes = array('xls', 'xlsx');


        $fileType = pathinfo($tmp_name, PATHINFO_EXTENSION);
        if (in_array($fileType, $allowTypes)) {

            $excelRead = PHPExcel_IOFactory::createReaderForFile($tmp);
            $objectRead = $excelRead->load($tmp);
            $sheet = $objectRead->getSheet(0);
            $lastRow = $sheet->getHighestRow();
            $lastCol = $sheet->getHighestColumn();

            $excelArray =  $sheet->toArray(null, true, true, true);

            unset($excelArray[1]);
            // GET IN ARRAY
            return array_values($excelArray);

            // $lastCol++;



            // foreach ($excelArray as $row) {
            //     for ($col = 'A'; $col != $lastCol; $col++) {

            //         var_dump($row[$col]);
            //     }
            // }
        } else {
            throw new Exception('File Extension is not allowed');
        }
    }


    //get list array
    function getList($count, $hidden, $checkbox = 0)
    {
        $data    = array("aaData" => array());
        $result  = $this->mysqli->query($this->query);

        if ($result) {
            $start = microtime();
            $rowcount = 0;
            while ($aRow = $result->fetch_array(MYSQLI_NUM)) {
                $row = array();

                if ($hidden === "no") {
                    for ($i = 0; $i < $count; $i++) {
                        /* General output */
                        $row[0] = 0;
                        $row[$i + 1] = $aRow[$i];
                        if ($checkbox == 1) {
                            if ($i == ($count - 1)) {
                                $row[] = '<div class="callapp_checkbox">
                                              <input type="checkbox" id="callapp_checkbox_' . $aRow[$hidden] . '" name="check_' . $aRow[$hidden] . '" value="' . $aRow[$hidden] . '" class="check" />
                                              <label for="callapp_checkbox_' . $aRow[$hidden] . '"></label>
                                          </div>';
                            }
                        }
                    }
                    $data['aaData'][] = $row;
                    $rowcount++;
                } else {
                    for ($i = 0; $i < $count; $i++) {
                        /* General output */
                        $row[] = $aRow[$i];
                        if ($checkbox == 1) {
                            if ($i == ($count - 1)) {
                                $row[] = '<div class="callapp_checkbox">
                                              <input type="checkbox" id="callapp_checkbox_' . $aRow[$hidden] . '" name="check_' . $aRow[$hidden] . '" value="' . $aRow[$hidden] . '" class="check" />
                                              <label for="callapp_checkbox_' . $aRow[$hidden] . '"></label>
                                          </div>';
                            }
                        }
                    }
                    $data['aaData'][] = $row;
                    $rowcount++;
                }
            }

            $data['time']  = round(microtime() - $start, 4);
            $data['count'] = $rowcount;

            return $data;
        } else {
            self::disconnect();
            throw new Exception($this->mysqli->error . " ::: Query -> " . $this->query);
        }
    }

    /**GET RESULT ROW NUMBERS
     * 
     * @throws Exception
     * @return integer $row
     */
    function getNumRow()
    {

        $result = $this->mysqli->query($this->query);

        if ($result) {
            $row = $result->num_rows;
            return $row;
        } else {
            throw new Exception($this->mysqli->error . " ::: Query -> " . $this->query);
        }
    }

    /**GET TABLE CURRENT INCREMENT VALUE
     * 
     * @param  string  $table
     * @return integer $increment
     */
    function getIncrement($table)
    {

        $result    = $this->mysqli->query("SHOW TABLE STATUS LIKE '$table'");
        $row       = $result->fetch_assoc();
        $increment = $row['Auto_increment'];

        return $increment;
    }

    /**SET TABLE INCREMENT VALUE
     * 
     * @param  string  $table
     * @return integer $increment
     */
    function setIncrement($table)
    {

        $increment = self::getIncrement($table);
        $next_increment = $increment + 1;
        $this->mysqli->query("ALTER TABLE $table AUTO_INCREMENT=$next_increment");

        return $increment;
    }

    /**GROUP RESULT BY COLUMN PARAMETER
     * 
     * @param  string $column
     * @return array  $groupArray
     */
    function groupBy($column)
    {

        $groupArray = array();

        foreach (self::getFetchArray() as $row) {

            $i   = 0;
            $key = 0;

            foreach ($groupArray as $grouprow) {

                if ($row[$column] == $grouprow[$column]) {
                    $key = $i;
                }

                $i++;
            }

            if ($key > 0) {
                $groupArray[$key]['count'] += 1;
            } else {
                $groupArray[] = $row;
            }
        }

        return $groupArray;
    }

    /**Chosens options
     * @param Int $id - Selected
     * @param String $selects - Options
     * @return Chosen options HTML 
     */

    function getSelect($id, $selects = "name")
    {

        $req = $this->mysqli->query($this->query);

        $data = '<option value="0" >----</option>';


        if ($req) {
            while ($res = $req->fetch_array()) {

                if ($res['id'] == $id) {
                    $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res[$selects] . '</option>';
                } else {
                    $data .= '<option value="' . $res['id'] . '">' . $res[$selects] . '</option>';
                }
            }
        } else {
            throw new Exception($this->mysqli->error . " ::: Query -> " . $query);
        }

        return $data;
    }

    /**
     * disconnect mysql server
     */
    function disconnect()
    {

        $this->mysqli->close();
    }

    function escp($string)
    {
        return $this->mysqli->real_escape_string($string);
    }
}
