<?php
$action = $_REQUEST['act'];

switch ($action) {
    case 'search_phone':
        $phone      = $_REQUEST['phone'];
        $filt_array = array('secret' => 'mGeLNeds6ARgQ4yrC', 'phone'  => $phone);
        break;
    case 'search_mail':
        $s_u_mail   = $_REQUEST['s_u_mail'];
        $filt_array = array('secret' => 'mGeLNeds6ARgQ4yrC', 'email'  => $s_u_mail);
        break;
    case 'search_user_id':
        $s_u_user_id  = $_REQUEST['s_u_user_id'];
        $filt_array   = array('secret' => 'mGeLNeds6ARgQ4yrC', 'user_id'  => $s_u_user_id);
        break;
    case 'search_s_u_pid':
        $s_u_pid    = $_REQUEST['s_u_pid'];
        $filt_array = array('secret' => 'mGeLNeds6ARgQ4yrC', 'id_code'  => $s_u_pid);
        break;
    default:
        $error = 'Action is Null';
}

$Curl = curl_init();
curl_setopt($Curl, CURLOPT_URL, 'https://www.mymarket.ge/ka/callapp/users');
curl_setopt($Curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($Curl, CURLOPT_POST, 1);
curl_setopt($Curl, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($Curl, CURLOPT_POSTFIELDS, $filt_array);

$Return = curl_exec($Curl);

curl_close($Curl);

$Return     = json_decode($Return, true);
$cound_data = count($Return[Data]);
//  echo '<pre>';
//  var_dump($Return);
//  echo '<pre>';

if (count($Return[Data][0][org])>0) {
    $my_user_id      = $Return[Data][0][user_id];
    $my_email        = $Return[Data][0][email];
    $my_user_name    = $Return[Data][0][org][0][type];
    $my_user_surname = $Return[Data][0][org][0][company];
    $my_id_code      = $Return[Data][0][org][0][id_code];
    $my_gender_id    = $Return[Data][0][gender_id];
    $birth_year      = $Return[Data][0][birth_year];
    $my_phone        = $Return[Data][0][phones][0][phone];
    $statusmessage   = $Return[StatusMessage];
}else{
    $my_user_id      = $Return[Data][0][user_id];
    $my_email        = $Return[Data][0][email];
    $my_user_name    = $Return[Data][0][user_name];
    $my_user_surname = $Return[Data][0][user_surname];
    $my_id_code      = '';
    $my_gender_id    = $Return[Data][0][gender_id];
    $birth_year      = $Return[Data][0][birth_year];
    $my_phone        = $Return[Data][0][phones][0][phone];
    $statusmessage   = $Return[StatusMessage];
    
}
if ($my_gender_id == 1) {
    $my_gender_id = 2;
}else if ($my_gender_id == 2) {
    $my_gender_id = 1;
}else{
    $my_gender_id = 0;
}

if ($cound_data == 0) {
    $my_user_id      = '';
    $my_email        = '';
    $my_user_name    = '';
    $my_user_surname = '';
    $my_gender_id    = 0;
    $birth_year      = '';
    $my_phone        = '';
    $my_id_code      = '';
}
$data = array(
    'user_id'      => $my_user_id,
    'email'        => $my_email,
    'user_name'    => $my_user_name,
    'user_surname' => $my_user_surname,
    'id_code'      => $my_id_code,
    'gender_id'    => $my_gender_id,
    'birth_year'   => $birth_year,
    'phone'        => $my_phone,
    'count'        => $cound_data,
    'message'      => $statusmessage
);

echo json_encode($data);
?>