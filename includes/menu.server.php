<?php
include('classes/class.Mysqli.php');
global $db;

$db      = new dbClass();
$nav_id	 = 1;
$user_id = $_SESSION['USERID'];
$error	 = '';
$data	 = '';

$db->setQuery("SELECT 	`menu_detail`.`id`,
                        `menu_detail`.`title`,
                        `menu_detail`.`page_id`,
                        `menu_detail`.`url`,
                        `menu_detail`.`icon`,
                        `menu_detail`.`sub_icon`
                FROM 	`users`
                LEFT JOIN `group` ON `users`.`group_id` = `group`.id
                LEFT JOIN `group_permission` ON `group`.id = `group_permission`.`group_id`
                LEFT JOIN menu_detail ON `group_permission`.`page_id` = `menu_detail`.`page_id`
                WHERE `users`.`id` = $user_id AND `menu_detail`.`menu_id` = $nav_id AND `menu_detail`.`parent` = 0
                ORDER BY `menu_detail`.`position`");

$par_class  = GetParentClass($nav_id);
$categories = array("nav" => array(), "nav_class" => $par_class);
$result     = $db->getResultArray();

foreach ($result[result]  AS $result1){ 
    $id = $result1['id'];
    
    $db->setQuery("SELECT 	`menu_detail`.`id`,
                            `menu_detail`.`title`,
                            `menu_detail`.`page_id`,
                            `menu_detail`.`url`,
                            `menu_detail`.`icon`,
                            `menu_detail`.`sub_icon`
                  FROM 	    `users`
                  LEFT JOIN `group` ON `users`.`group_id` = `group`.id
                  LEFT JOIN `group_permission` ON `group`.id = `group_permission`.`group_id`
                  LEFT JOIN  menu_detail ON `group_permission`.`page_id` = `menu_detail`.`page_id`
                  WHERE 	`users`.`id` = $user_id AND `menu_detail`.`menu_id` = $nav_id AND `menu_detail`.`parent`='$id'
                  ORDER BY  `menu_detail`.`position`");
    
    
    $category 		 = $result1;
    $category["sub"] = array();
    
    $result2 = $db->getResultArray();
    
	
    foreach ($result2[result] AS $sub_result){

        $subcat		   = $sub_result;
        $subcat["sub"] = array();
        $sub_id        = $sub_result[id];
        
        $db->setQuery("	SELECT 	  `menu_detail`.`id`,
                                  `menu_detail`.`title`,
                                  `menu_detail`.`page_id`,
                                  `menu_detail`.`url`,
                                  `menu_detail`.`icon`,
                                  `menu_detail`.`sub_icon`
                        FROM 	  `users` LEFT JOIN `group` ON `users`.`group_id` = `group`.id
                        LEFT JOIN `group_permission` ON `group`.id = `group_permission`.`group_id`
                        LEFT JOIN  menu_detail ON `group_permission`.`page_id` = `menu_detail`.`page_id`
                        WHERE 	  `users`.`id` = $user_id AND `menu_detail`.`menu_id` = $nav_id AND `menu_detail`.parent=$sub_id
                        ORDER BY  `menu_detail`.`position`");
        
        $result3=$db->getResultArray();
        
        foreach ($result3[result] AS $subsub_result){
            $subsubcat = $subsub_result;
            array_push($subcat["sub"], $subsubcat);
        }
        array_push($category["sub"], $subcat);
    }
    array_push($categories["nav"], $category);
}

$data = $categories;
$data['error'] = $error;

echo json_encode($data);

function GetParentClass($nav_id) {
    global $db_nav;
    $db_nav = new dbClass();
    
    $db_nav->setQuery("SELECT `class`
                       FROM   `menu`
                       WHERE  `id` = $nav_id");
    
    $res = $db_nav->getResultArray();
    return $res[0]['class'];
}

?>