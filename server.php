<?php

 ini_set('display_errors', 1);
 ini_set('display_startup_errors', 1);
 error_reporting(E_ALL);

set_time_limit(0);
date_default_timezone_set('Asia/Tbilisi');

include 'includes/classes/class.Mysqli.php';
$mysqli = new dbClass();

global $mysqli;
$esc_message = addslashes($_REQUEST['message']);
$ms          = $_REQUEST['message'];
$source      = $_REQUEST['source'];
$esc_name    = addslashes($_REQUEST['chat_name']);
$esc_mail    = addslashes($_REQUEST['chat_mail']);
$esc_phone   = addslashes($_REQUEST['chat_phone']);
$os = $_REQUEST['os'];
$br = $_REQUEST['browser'];
$user = $_REQUEST['user_name'];
$user_id = $_REQUEST['user_id'];
$chat_name = $_REQUEST['chat_name'];
$idd = $_REQUEST['id'];
//$check = $_REQUEST['check'];
//     $esc_message = $mysqli->real_escape_string($esc_message);
//     $esc_name = $mysqli->real_escape_string($esc_name);
//     $esc_mail = $mysqli->real_escape_string($esc_mail);
//     $esc_phone = $mysqli->real_escape_string($esc_phone);
$chat_id = $_REQUEST['chat_id'];
$ip = get_client_ip();

if($source=='site'){
    $mysqli->setQuery("SELECT  site_chat_account.`name`, 
                               site_chat.sender_id
                        FROM  `site_chat`
                        JOIN   site_chat_account ON site_chat_account.id = site_chat.account_id
                        where  site_chat.id = '$chat_id'");
    $res = $mysqli->getResultArray();

    $web_site = $res['result'][0][name];
    $sender   = $res['result'][0]['sender_id'];

    $mysqli->setQuery("INSERT  site_messages
                          SET `datetime`      = NOW(),
                              `site_chat_id`  = '". $_REQUEST['chat_id']."',
                              `user_id`       = '". $_REQUEST['user_id']."',
                              `text`          = '".$_REQUEST['message']."'");
    $mysqli->execQuery();
    ////////////////----------------------------------------
    $mysqli->setQuery("UPDATE  site_chat
                          SET `last_user_id`  = '".$_REQUEST['user_id']."',
                              `last_datetime` = NOW(),
                              `status`        = '2'
                       WHERE   id = $chat_id");
    $mysqli->execQuery();
    //////////////////--------------------------------------
    ini_set('max_execution_time', 0);

    $Curl = curl_init();
    curl_setopt($Curl, CURLOPT_URL, 'https://www.mymarket.ge/ka/callapp/sendmessage');
    curl_setopt($Curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($Curl, CURLOPT_POST, 1);
    curl_setopt($Curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($Curl, CURLOPT_POSTFIELDS, array('secret' => 'mGeLNeds6ARgQ4yrC', 'user_id'  => $sender, 'site'=>$web_site, 'message' => $ms));

    $Return = curl_exec($Curl);

    curl_close($Curl);

    $Return     = json_decode($Return, true);
    var_dump($Return);
}elseif($source=='fbm'){
    $fb_obj = $_REQUEST['fb_object'];
    $links_array = $fb_obj['arr'];
    $fb_text = str_replace("\n", '', $fb_obj['text_fb']);
    
    $esc_message = $esc_message.' '.$_REQUEST['fb_attachment'];
    $mysqli->query= ("
                insert fb_messages
                SET
                    `datetime`      = NOW(),
                    `fb_chat_id` = '". $_REQUEST['chat_id']."',
                    `user_id`       = '". $_REQUEST['user_id']."',
                    `time`          = NOW(),
                    `text`          = '".$esc_message."'
                ");
    $mysqli->execQuery();
    ////////////////----------------------------------------
    $mysqli->query=("
                UPDATE fb_chat
                SET
                    `last_user_id`  = '".$_REQUEST['user_id']."',
        `last_datetime` = NOW(),
        `status`        = '2'
        WHERE  id=$chat_id
        ");
    $mysqli->execQuery();
    //////////////////--------------------------------------
    $mysqli->setQuery("SELECT sender_id,
        fb_account.`token` AS `token`
        FROM  `fb_chat`
        JOIN   fb_account ON fb_account.id = fb_chat.account_id
        where  fb_chat.id = '$chat_id'");
    $res = $mysqli->getResultArray();

    $sender         = $res['result'][0]['sender_id'];
    $access_token   = $res['result'][0]['token'];
    $url            = "https://graph.facebook.com/v2.6/me/messages?access_token=$access_token";

    $ch     = curl_init($url);
    $esc_message = str_replace("\n", '', $esc_message);

    $jsonData = '{
                "recipient" :   {"id":"'.$sender.'"},
                "message"   :   {
                    "text": "'.$fb_text.'"

                }}';


    var_dump($jsonData);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    $result = curl_exec($ch);
    $errors = curl_error($ch);
    $response = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    foreach ($links_array as $esc_message ){

    if (strpos($esc_message, 'https://crm.my.ge') !== false && (strpos($esc_message, '.doc') !== false || strpos($esc_message, '.doc') !== false  || strpos($esc_message, '.xlsx') !== false || strpos($esc_message, '.pdf') !== false || strpos($esc_message, '.pptx') !== false)) {
        $jsonData = '{
                "recipient" :   {"id":"'.$sender.'"},
                "message"   :   {
                   
                     "attachment":{
                                      "type":"file", 
                                      "payload":{
                                        "is_reusable": false,
                                        "url":"'.$esc_message.'"
                                      }
                                  }

                        
                }}';
    }elseif  (strpos($esc_message, 'https://crm.my.ge') !== false && (strpos($esc_message, '.jpg') !== false || strpos($esc_message, '.png') !== false  || strpos($esc_message, '.gif') !== false || strpos($esc_message, '.JPEG') !== false)){
        $jsonData = '{
                "recipient" :   {"id":"'.$sender.'"},
                "message"   :   {
                   
                     "attachment":{
                                      "type":"image", 
                                      "payload":{
                                        "is_reusable": false,
                                        "url":"'.$esc_message.'"
                                      }
                                  }

                        
                }}';
    }elseif  (strpos($esc_message, 'https://crm.my.ge') !== false && (strpos($esc_message, '.mp4') !== false || strpos($esc_message, '.3GP') !== false  || strpos($esc_message, '.WMV') !== false || strpos($esc_message, '.FLV') !== false)){
        $jsonData = '{
                "recipient" :   {"id":"'.$sender.'"},
                "message"   :   {
                   
                     "attachment":{
                                      "type":"video", 
                                      "payload":{
                                        "is_reusable": false,
                                        "url":"'.$esc_message.'"
                                      }
                                  }

                        
                }}';
    } elseif  (strpos($esc_message, 'https://crm.my.ge') !== false && (strpos($esc_message, '.wav') !== false || strpos($esc_message, '.mp3') !== false  || strpos($esc_message, '.AAC') !== false || strpos($esc_message, '. FLAC') !== false)){
        $jsonData = '{
                "recipient" :   {"id":"'.$sender.'"},
                "message"   :   {
                   
                     "attachment":{
                                      "type":"audio", 
                                      "payload":{
                                        "is_reusable": false,
                                        "url":"'.$esc_message.'"
                                      }
                                  }

                        
                }}';
    }

    var_dump($jsonData);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    $result = curl_exec($ch);
    $errors = curl_error($ch);
    $response = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    // curl_close($ch);
    // var_dump($errors);
    // var_dump($response);
    // die( $result)   ;
    // curl_close($ch);
    // var_dump($errors);
    // var_dump($response);
    // die( $result);
           
}
    

    curl_close($ch);
    var_dump($errors);
    var_dump($response);
    die( $result)   ;
    curl_close($ch);
    var_dump($errors);
    var_dump($response);
    die( $result);
}
// elseif($source=='fbm'  && $check == 1){

//     $mysqli->setQuery("SELECT sender_id,
//                               fb_account.`token` AS `token`
//                        FROM  `fb_chat`
//                        JOIN   fb_account ON fb_account.id = fb_chat.account_id
//                        where  fb_chat.id = '$chat_id'");
//     $res = $mysqli->getResultArray();

//     $sender         = $res['result'][0]['sender_id'];
//     $access_token   = $res['result'][0]['token'];
//     $url            = "https://graph.facebook.com/v3.3/me/message_attachments?access_token=$access_token";

//     $ch     = curl_init($url);

//     $jsonData = '{
//                     "recipient" :   {"id":"'.$sender.'"},
//                     "message":{
//                         "attachment":{
//                           "type":"image",
//                           "payload":{
//                             "is_reusable": true,
//                             "url":"'.$esc_message.'"
//                           }
//                         }
//                       }
//                     }';

//     var_dump($jsonData);
//     curl_setopt($ch, CURLOPT_POST, 1);
//     curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
//     curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
//     $result = curl_exec($ch);
//     $errors = curl_error($ch);
//     $response = curl_getinfo($ch, CURLINFO_HTTP_CODE);
//     curl_close($ch);
//     var_dump($errors);
//     var_dump($response);
//     die( $result)   ;
//     curl_close($ch);
//     var_dump($errors);
//     var_dump($response);
//     die( $result);
// }
elseif($source=='fbm_block'){
    $chat_id = $_REQUEST['chat_id'];
    $mysqli->setQuery(" SELECT sender_id,
                               fb_account.`name` AS `token`
                        FROM  `fb_chat`
                        JOIN   fb_account ON fb_account.id = fb_chat.account_id
                        where  fb_account.id = '$chat_id'");
    $res = $mysqli->getResultArray();

    $access_token   = $res['result'][0]['token'];
    $url            = "https://graph.facebook.com/v3.2/me/blocked";
    $u_id           = $res['result'][0]['sender_id'];
    $ch             = curl_init($url);
    $jsonData       = '{
            "access_token"  :  "'. $access_token .'",
            "uid"           :  "'. $u_id .'"
        }';
    curl_setopt($ch, CURLOPT_POST, 1); //curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    $result = curl_exec($ch);
    $errors = curl_error($ch);
    $response = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    var_dump($errors);
    var_dump($response);
    die( $result)   ;
}elseif($source=='mail'){
    $chat_id    = $_REQUEST['chat_id'];
    $attachment = $_REQUEST['ge'];
    $file_name  = $_REQUEST['file_name'];

    $attachment = substr($attachment, 0, -1);
    $file_name  = substr($file_name, 0, -1);



    $mysqli->setQuery(" SELECT    mail_chat.sender_id,
                			      mail_account.`name` AS mail,
                			      mail_messages.`subject`,
                                  mail_account.`id` AS mail_id
                        FROM     `mail_chat`
                        JOIN      mail_account ON mail_account.id = mail_chat.account_id
                        LEFT JOIN mail_messages ON mail_chat.id = mail_messages.mail_chat_id
                        WHERE     mail_chat.id = '$chat_id'
                        GROUP BY  mail_chat.id");

    $res          = $mysqli->getResultArray();
    $sender       = $res['result'][0]['sender_id'];
    $from         = $res['result'][0]['mail'];
    $subject      = $res['result'][0]['subject'];
    $from_mail_id = $res['result'][0]['mail_id'];

    $mysqli->setQuery("SELECT signature
                       FROM   signature
                       WHERE  account_id = '$from_mail_id' AND actived = 1 LIMIT 1");

    $signature = $mysqli->getResultArray();
    $mail_signature = html_entity_decode($signature[result][0][signature]);
    $url     = "https://notifications.my.ge/ka/main/SendMail";
    //$sender  = 'aleqsandrefocxverashvili@gmail.com';
    $Curl = curl_init();
    curl_setopt($Curl, CURLOPT_URL, $url);
    curl_setopt($Curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($Curl, CURLOPT_POST, 1);
    curl_setopt($Curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($Curl, CURLOPT_POSTFIELDS, array(
        'Secret'	 => 'mGeLNeds6ARgQ4yrC',
        'To'		 => $sender,
        'From'       => $from,
        'FromName'   => 'My.ge',
        'ReplyTo'    => $from,
        'Subject'	 => $subject,
        'Message'	 => urldecode($esc_message.'<br><br><p>'.$mail_signature.'</p>'),
        'Attach'     => $attachment,
        'AttachName' => $file_name,
        'IsHTML'     => 1
    ));

    $Return = curl_exec($Curl);
    $status = curl_getinfo($Curl, CURLINFO_HTTP_CODE);
    curl_close($Curl);

    $Return = json_decode($Return, true);

    if($Return['Status']=='2'){
        $str = str_replace("'","\'",$_REQUEST['mail_text']);
        $mysqli->setQuery("INSERT  mail_messages
                              SET `datetime`     = NOW(),
                                  `mail_chat_id` = '". $_REQUEST['chat_id']."',
                                  `user_id`      = '". $_REQUEST['user_id']."',
                                  `time`         = NOW(),
                                  `text`         = '".$str."' ");
        $mysqli->execQuery();
        $attach_message_id = $mysqli->getLastId();

        $attach = explode(";",$attachment);
        $file_name = explode(";",$file_name);


        if ($attachment != '') {
            $i=0;
            foreach($attach AS $attachm){
                $mysqli->setQuery("INSERT INTO `mail_attachments`
                                              (`messages_id`, `patch`, `name`, `send_mail_id`,check_mesage) 
                                        VALUES 
                                              ($attach_message_id, '$attachm', '$file_name[$i]', 0,1);");
                $mysqli->execQuery();
                $i++;
            }
        }
        ////////////////----------------------------------------
        $mysqli->setQuery("UPDATE  mail_chat
                              SET `last_user_id`  = '".$_REQUEST['user_id']."',
                                  `last_datetime` = NOW(),
                                  `status`        = '2'
                           WHERE   id             = $chat_id");

        $mysqli->execQuery();

        $status = 2;
        $data = array('status'=>'ok');
        //die($status);
    }else{
        $data = array('status'=>'no');
    }
    die(json_encode($data));
}elseif($source=='site_chat'){
    $user_id             = $_REQUEST['user_id'];
    $message             = $_REQUEST['message'];
    $chat_id             = $_REQUEST['chat_id'];
    $browser             = $_REQUEST['browser'];
    $user_name           = $_REQUEST['user_name'];
    $site_chat_mesage_id = $_REQUEST['site_chat_mesage_id'];

    require_once('includes/classes/wsdl.class.php');

    $wsdl = new wsdl('http://192.168.215.30/services/public/callcentersupportservice.asmx?wsdl', 'CallCenter', '2cc4541a13009ba5da9b90a146d10369', '192.168.200.145', 'Lasha', 'index.php');

    $status = $wsdl->ReplyMessage($user_id,$message,$site_chat_mesage_id);

    $site_mesage_values = '';

    //if ($status == 1) {
    $resuslt_messages = $wsdl->GetConversationMessages($chat_id);

    foreach ($resuslt_messages as $message){
        $mesageid = $message['MessageId'];
        $datetime = $message['ShowFrom'];
        $type     = $message['MessageTypeId'];
        $UserId   = $message['UserId'];
        $text     = htmlspecialchars($message['MessageText'], ENT_QUOTES);

        if ($mesageid != '') {
            $site_mesage_values1 .= '(\''.$mesageid.'\',\''.$datetime.'\', \''.$UserId.'\', \''.$chat_id.'\', \''.$type.'\',\''.$text.'\'),';
        }
    }

    $site_mesage_values.= $site_mesage_values1;
    $site_mesage_values = substr($site_mesage_values, 0, -1);

    $mysqli->setQuery("INSERT IGNORE INTO `site_chat_messages`
        (`mesage_id`, `datetime`, `user_id`, `site_chat_id`, `type`, `text`)
        VALUES
        $site_mesage_values");
    $mysqli->execQuery();

    $mysqli->query=("UPDATE  site_chat
        SET `last_user_id`  = '$user_id',
        `last_datetime` = NOW(),
        `status`        = '2'
        WHERE   id             = $chat_id");

    $mysqli->execQuery();
    //}
}else{



    if($chat_id == 0){
        if(strlen($chat_name) == 0){
            $gest_name = "სტუმარი $clientID";
        }else{
            $gest_name = $esc_name;
        }
        $mysqli->setQuery(" INSERT INTO `chat`
            (`join_date`, `answer_date`, `answer_user_id`, `name`, `email`, `phone`, `ip`, `country`, `device`, `browser`, `status`, `socet_id`)
            VALUES
            (NOW(), '', '', '$gest_name', '$esc_mail', '$esc_phone', '$ip', '', '$os', '$br', '1', '$clientID');");
        $mysqli->execQuery();
        $chat_id = $mysqli->getLastId();
        $json = array('chat_id_paste'=>$chat_id);
        echo json_encode($json,JSON_NUMERIC_CHECK);
    }else if($_REQUEST[act] == 'gabrazeba'){
        $myquery = $mysqli->setQuery("DELETE FROM `tabel_copy`");

        echo $mysqli->getJson();
    }else{
        $json = array('chat_id_paste'=>'');
        echo json_encode($json,JSON_NUMERIC_CHECK);
    }

    if($idd == 0){
        $mysqli->setQuery(" UPDATE  `chat`
            SET  `answer_date`    = NOW(),
            `answer_user_id` = '$user_id',
            `status`         = '2',
            `seen`           = 0
            WHERE   `id`='$chat_id' AND status = 1;");
        $mysqli->execQuery();
    }

    if($idd == 1){
        $mysqli->setQuery(" INSERT INTO `chat_details`
            (`chat_id`,`message_client`, `message_operator`, `operator_user_id`, `message_datetime`)
            VALUES
            ('$chat_id', '$esc_message', '', '', NOW());");

        $mysqli->execQuery();
    }else{
        $mysqli->setQuery(" UPDATE  `chat` SET
            `last_person`='1', `seen` = 0
            WHERE   `id`='$chat_id';");
        $mysqli->execQuery();
        $mysqli->setQuery(" INSERT INTO `chat_details`
            (`chat_id`,`message_client`, `message_operator`, `operator_user_id`, `message_datetime`)
            VALUES
            ('$chat_id', '', '$esc_message', '$user_id', NOW());");

        $mysqli->execQuery();

        $mysqli->setQuery("UPDATE `chat` set
            `last_user_id` = '$user_id',
            `last_user_message_date` = NOW(), `seen` = 0
            where chat.`id` = '$chat_id'");

        $mysqli->execQuery();
    }
}
function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

?>
