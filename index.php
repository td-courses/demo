<?php 
	session_start();
	
?>

<?php if ($_SESSION['USERID'] == ''): ?>
	<?php require_once('includes/login.php');?>
<?php else: ?>
	<?php require_once('includes/functions.php');?>

	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>MYGE</title>

			<link rel="shortcut icon" type="image/ico" href="https://www.mymarket.ge/favicon.ico" />
			<link rel="stylesheet" type="text/css" media="screen" href="media/css/reset/reset.css" />
			<!--[if IE]>
				<link rel="stylesheet" type="text/css" media="screen" href="css/reset/ie.css" />
			<![endif]-->

			<!-- font awesome -->
			<script defer src="js/fontawesome-all.js"></script>

			<?php echo GetJs();?>
			<?php echo GetCss();?>

			<script type="text/javascript">
				$(document).ready(function (){
					AjaxSetup();
				});
			</script>
			<style>
				/* Google Material Icons */
				@font-face {
					font-family: 'Material Icons';
					font-style: normal;
					font-weight: 400;
					src: url(media/fonts/material_icons/MaterialIcons-Regular.eot); /* For IE6-8 */
					src: local('Material Icons'),
						local('MaterialIcons-Regular'),
						url(media/fonts/material_icons/flUhRq6tzZclQEJ-Vdg-IuiaDsNc.woff2) format('woff2'),
						url(media/fonts/material_icons/MaterialIcons-Regular.woff) format('woff'),
						url(media/fonts/material_icons/MaterialIcons-Regular.ttf) format('truetype');
				}
				#table_right_menu{
					z-index:1px;
				}

				.material-icons {
					font-family: 'Material Icons';
					font-weight: normal;
					font-style: normal;
					font-size: 24px;  /* Preferred icon size */
					display: inline-block;
					line-height: 1;
					text-transform: none;
					letter-spacing: normal;
					word-wrap: normal;
					white-space: nowrap;
					direction: ltr;
					
					/* Support for all WebKit browsers. */
					-webkit-font-smoothing: antialiased;
					/* Support for Safari and Chrome. */
					text-rendering: optimizeLegibility;
					
					/* Support for Firefox. */
					-moz-osx-font-smoothing: grayscale;
					
					/* Support for IE. */
					font-feature-settings: 'liga';
				}
				#news{
					display: block;
					position: fixed !important;
					bottom: 10px !important;
					right: 10px !important;
					background: #DB3340;
					width: 200px;
					height: 40px;
					border-radius:3px;
					color: #E8B71A;
					text-align:center;
					z-index: 9999;
				}
				#news span{
					display: block;
					padding-top: 7px;
				}
				#news_action{
					display: block;
					position: fixed !important;
					bottom: 55px !important;
					right: 10px !important;
					background: #DB3340;
					width: 200px;
					height: 40px;
					border-radius:3px;
					color: #E8B71A;
					text-align:center;
					z-index: 9999;
				}
				#news_action span{
					display: block;
					padding-top: 7px;
				}

				#tabel_id {
					width: 100%;
				}
				#logout_comment {
					width: 100%; min-height: 50px;
					resize: vertical;
					box-sizing: border-box;
				}
				label[for="logout_comment"] {
					margin-top: 10px;
				}
				#save_id, #undo_id {
					font-family: pvn;
					font-size: 13px;
					line-height: 1.4;
					color: #636e72;
					background: #E6F2F8;
					border: 1px solid #A3D0E4;
				}

				#save_id:hover, #undo_id:hover {
					color: #2d3436;
				}
				.ui-dialog-titlebar-close {
					background: #E6F2F8;
					border: 1px solid #A3D0E4;
					position: relative;
				}

				.ui-dialog-titlebar-close span {
					position: absolute;
					left: 50%;
				}

				.activitie-screen-disabler {
					display: none;
					width: 100%; height: 100%;
					position: fixed;
					top: 0; left: 0;
					z-index: 99999;
					background: rgba(0,0,0, .80);
					user-select: none;
				}
				.activitie-screen-disabler.on {
					display: flex;
					align-items: center;
					justify-content: center;
				}
				.activitie-time-counter {
					text-align: center;
				}
				.activitie-time-counter span {
					font-size: 330px;
					color: #ecf0f1;
				}
				#returnFromActivitie {
					margin-left: 50%;
					transform: translate(-50%);
					background: #3498db;
					font-family: pvn;
					font-size: 16px;
					padding: 9px 17px;
					border-radius: 5px;
					margin-top: 13px;
					color: #ecf0f1;
					border: none;
					cursor: pointer;
					transition: background .1s ease;
				}
				#returnFromActivitie:hover {
					background: #2980b9;
				}
				.activitie-cur-time {
					display: flex;
					width: 200px; height: 50px;
					position: absolute;
					bottom: 0; right: 0;
					align-items: center;
					justify-content: center;
					font-size: 30px;
					color: #f1c40f;
				}
				.livestate-action-circ {
					width: 10px; height: 10px;
					float: left;
					position: relative;
					top: 2px;
					margin-right: 3px;
				}
            </style>
            <script type="text/javascript">
            var logout = true;

                $(document).ready(function (){

					//checker1();
					//checkActivitieStatus();

        		    function checker1(){
        				setTimeout(function(){

                        	$.ajax({
                        		async: true,
            		            url: "checker.php",
            		            type: "POST",
            		            data: "act=get_checker&logout=1",
            		            dataType: "json",
            		            success: function (data) {

									if(data.alertCount > 0){
										var audio = new Audio('sms.mp3');
										audio.play();
									}

            		            	if(logout==true) {

                  				       if(data.logout==1){
              					   			buttons = {
              					   				 "logout1": {
              					   			            text: "არა",
              					   			            id: "logout1"
              					   			       },
              					   					"login": {
              					   			            text: "დიახ",
              					   			            id: "login"
              					   			       }
              						   			};

          	    						if(data.time==0){
          	        						logout=true; window.location.href="index.php?act=logout";
          	        					}
              					   		GetDialog("logout", "auto", "auto",buttons);
              	   							$("#logout").html('თქვენი სამუშაო გრაფიკით განსაზღვრული დრო ამოიწურა!გსურთ სისტემაში დარჩენა?<br> <div style="margin: 5px 0px 0 146px; padding: 5px 10px; display: inline-block;">დარჩენილი დრო </div> <div style="margin: 5px auto; padding: 5px 10px; border: solid 1px; display: inline-block;">'+data.time+'</div><div style="margin: 5px auto; padding: 5px 10px; display: inline-block;">წმ</div>');
                  				    	}else{
              				    		//$("#logout").dialog("close");
              					    	}

              				    	}

                		            if(data.count != 0){
                		            	$("#new_task").attr('data-badge',data.count);
                		            }else{
                 		            	$("#new_task").removeAttr('data-badge');
                 		            }

                		            if(data.no_wiev != 0){
                		            	$("#new_task").attr('data-badge1',data.no_wiev);
                		            }else{
                 		            	$("#new_task").removeAttr('data-badge1');
                 		            }
                 		            
                		            if(data.count != 0 || data.no_wiev != 0){
                		            	$("#new_task").attr('class','badge2');
                		            }else{
                 		            	$("#new_task").removeClass('badge2');
                		            }
                		            
                		        }
            		        });
                           	checker1();
                        }, 10000);


                    }

                    function checker(){
                    	setTimeout(function(){
                    		$.ajax({
                    			async: true,
            		            url: "checker.php",
            		            type: "POST",
            		            data: "act=get_checker",
            		            dataType: "json",
            		            success: function (data) {
          		            	   	$( "#newsmain" ).html(data.count);
            		            }
            		        });
                    		checker();
                        	}, 10000);
					}

					// check activitie status
					function checkActivitieStatus() {

						// define request data
						const data = {
							act: "check_activitie_status"
						}

						// send ajax request
						$.post("server-side/call/incomming.action.php", data, responce => {

							// restruct responce
							const { error, activitieId, activitieStatus, secondsPassed } = responce;

							// check for error
							if(error !== "") {
								alert(error);
							} else {

								// define if activitie counter is on
								const activitieCounterIsOn = $(".activitie-screen-disabler").hasClass("on");

								// check for activitie id
								if(activitieStatus === "on" && !activitieCounterIsOn) {
									$D.activitieOnTimer(secondsPassed);
									$D.chosedActivitieId = activitieId;
								} else if(activitieStatus === "off" && activitieCounterIsOn) {

									// close activite timer
									$D.activitieOffTimer();
								}

								// make check interval
								setTimeout(checkActivitieStatus, 1000);

							}

						});
					}

    				$(document).on("click", "#news", function () {
    					location.href='index.php?pg=13#tab-1'
    				});
    				
    				$(document).on("click", "#news_action", function () {
    					location.href='index.php?pg=47'
    				});

					$(document).on("click", ".logout", function () {

						$.post("server-side/logout.action.php", {act: "logout_save"}, () => {
							window.location.href="index.php?act=logout";
						});

					});


                $(document).on("click", "#logout1", function () {

                	window.location.href="index.php?act=logout";
                });

                $(document).on("click", "#login", function () {
                	logout = false;
                	$("#logout").dialog("close");
				});

				// return from activitie
				$(document).on("click", "#returnFromActivitie", function() {

					// define request data
					const data = {
						act: "change_incall_activitie_status",
						activitieId: $D.chosedActivitieId,
						type: "off"
					};

					// send ajax request
					$.post("server-side/call/incomming.action.php", data, responce => {

						// restruct responce
						const { error, onlineStatus, dndStatus } = responce;

						// check for error
						if(error !== ""){
							alert(error);
						}else{
							$D.activitieOffTimer();
							toggleChatStatus('#chat_status', onlineStatus === 1 ? 2 : 1);

							// toggle dnd
							$.post("AsteriskManager/dndAction.php", {
								action: dndStatus === 1 ? "dndon" : "dndoff"
							});
						}

					});
				});

				// clcik on everything and everywhere
				$(window).click(function() {

					// close choose activitie
					$("#callapp_op_activitie_choose").data("active", false).removeClass("on");
				});

				// desable context menu on screen disabler
				$(document).on("contextmenu", ".activitie-screen-disabler", () => false);

			});

        </script>
		</head>
		<body >

			<div id="npm"></div>
			<!--<div id="loading" style="z-index: 999999;padding: 160px 0;height: 100%;width: 100%;position: fixed;background: #463e29c2;top: 0;">
			    <div class="loading-circle-1">
			    </div>
			    <div class="loading-circle-2">
			    </div>
			</div>-->
			<?php require_once('includes/pages.php'); ?>

			<div id="newsmain">

			</div>
			<!-- jQuery Dialog -->
        	<div id="logout_dialog" title="ქმედება">
        	</div>
        	<!-- jQuery Dialog -->
        	<div id="logout" title="ავტომატური გამოსვლა სისტემიდან">
			</div>

			<!-- activitie screen disabler -->
			<div class="activitie-screen-disabler">
				<div>
					<div class="activitie-time-counter">
						<span>00:00:00</span>
					</div>
					<button id="returnFromActivitie">
						აქტივობიდან დაბრუნება
					</button>
				</div>
				<div class="activitie-cur-time">
					00:00:00
				</div>
			</div>

		</body>
	</html>
<?php endif;?>
