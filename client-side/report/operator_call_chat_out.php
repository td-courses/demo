<head>
<style type="text/css">

.hidden{
	display: none;
}
.high {
    height: 15px;
}
.callapp_head{
	font-family: pvn;
	font-weight: bold;
	font-size: 20px;
	color: #2681DC;
}
.callapp_head_hr{
	border: 1px solid #2681DC;
}
.callapp_refresh{
    padding: 5px;
    border-radius:3px;
    color:#FFF;
    background: #9AAF24;
    float: right;
    font-size: 13px;
    cursor: pointer;
}
.ui-widget-content {
    border: 0px solid #2681dc;
}

#table_right_menu {
	top: 56px;
}

.search_button {
    padding: 2px;
    border-radius: 0px;
    background: #009688;
    cursor: pointer;
	float: left;
}
</style>
	<script type="text/javascript">
		var aJaxURL		      = "server-side/report/operator_call_chat_out.action.php";
		var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
		var tName		      = "example";
		var tName1		      = "report_1";

		
		$(document).ready(function () {
			GetDateTimes("search_start_my");
			GetDateTimes("search_end_my");
			$("#fillter_in").button();
			LoadTable();
			
		});
		

		function LoadTable(){
			var start	= $("#search_start_my").val();
	    	var end		= $("#search_end_my").val();
			         /* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
			GetDataTable(tName, aJaxURL, "get_list",10, "start=" + start + "&end=" + end, 0, "", 1, "desc", [2,3,4,5,6,7,8], change_colum_main);
            SetEvents("", "", "", tName, "add_edit_form", aJaxURL, "type=report1");
			 setTimeout(function(){$('.ColVis, .dataTable_buttons').css('display','none');}, 90);
		}
		function  LoadTable_modal(){
            var start	= $("#search_start_my").val();
            var end		= $("#search_end_my").val();
            var id		= $("#request_report_id").val();

            GetDataTable("report_1", aJaxURL, "get_list_report",11, "start=" + start + "&end=" + end+ "&id="+ id, 0, "", 1, "desc", '', change_colum_main);
            SetEvents("", "", "", tName1, "add_edit_form1", aJaxURL, "type=report2");
            setTimeout(function(){$('.ColVis, .dataTable_buttons').css('display','none');}, 90);
        }

		
		$(document).on("click", "#show_copy_prit_exel", function () {
	        if($(this).attr('myvar') == 0){
	            $('.ColVis,.dataTable_buttons').css('display','block');
	            $(this).css('background','#2681DC');
	            $(this).children('img').attr('src','media/images/icons/select_w.png');
	            $(this).attr('myvar','1');
	        }else{
	        	$('.ColVis,.dataTable_buttons').css('display','none');
	        	$(this).css('background','#0C6A57');
	            $(this).children('img').attr('src','media/images/icons/select.png');
	            $(this).attr('myvar','0');
	        }
	    });
	    
		$(document).on("click", ".callapp_refresh", function () {
        	LoadTable();
	    });
	    
		$(document).on("click", "#fillter_in", function () {
	    	LoadTable();
	    });
        $(document).on("click", "#search_ab_pin", function () {
        	var dLength = [[5, 10, 30, -1], [5, 10, 30, "ყველა"]];
            GetDataTable("table_history", aJaxURL, "get_list_history", 8, "&start_check="+$('#start_check').val()+"&end_check="+$('#end_check').val()+"&phone="+$("#phone").val()+"&s_u_user_id="+$("#s_u_user_id").val(), 0, dLength, 2, "desc", '', "<'F'lip>");

            $("#table_history_length").css('top','0px');
        });
        
        function LoadDialog(form) {
            if(form=="add_edit_form"){
                var button_modal = {

                    "cancel": {
                        text: "დახურვა",
                        id: "cancel-dialog",
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                }
                GetDialog("add_edit_form", 1000, "auto", button_modal, "center top");
                LoadTable_modal();
            }else {
                var button_modal = {

                    "cancel": {
                        text: "დახურვა",
                        id: "cancel-dialog",
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                }
                var dLength = [[5, 10, 30, -1], [5, 10, 30, "ყველა"]];
                GetDialog("add_edit_form1", 1050, "auto", button_modal, "center top");
                $("#search_ab_pin").button();
                var dLength = [[5, 10, 30, -1], [5, 10, 30, "ყველა"]];
                GetDataTable("table_history", aJaxURL, "get_list_history", 8, "&start_check="+$('#start_check').val()+"&end_check="+$('#end_check').val()+"&phone="+$("#phone").val()+"&s_u_user_id="+$("#s_u_user_id").val(), 0, dLength, 2, "desc", '', "<'F'lip>");

                $("#table_history_length").css('top','0px');
                $("#incomming_cat_1, #incomming_cat_1_1, #incomming_cat_1_1_1, #my_site, #source_id, #inc_status, #s_u_status, #client_sex").chosen({ search_contains: true });
           }


        }
        function show_right_side(id){
	        $("#right_side fieldset").hide();
	        $("#" + id).show();
	        //$(".add-edit-form-class").css("width", "1260");
	        //$('#add-edit-form').dialog({ position: 'left top' });
	        hide_right_side();

	        var str = $("."+id).children('img').attr('src');
			str = str.substring(0, str.length - 4);
			$("#side_menu span").children('img').css('border-bottom','');
	        $("."+id).children('img').css('filter','brightness(0.1)');
	        $("."+id).children('img').css('border-bottom','2px solid #333');
	    }

	    function hide_right_side(){
			$(".info").children('img').css('filter','brightness(1.1)');
			$(".record").children('img').css('filter','brightness(1.1)');
	        $(".call_resume").children('img').css('filter','brightness(1.1)');
	        $("#record fieldset").show();
	    }

        function listen(file){
        	$('#auau').each(function(){
        	    this.pause(); // Stop playing
        	    this.currentTime = 0; // Reset time
        	});
            var url = 'http://pbx.my.ge/records/' + file;
            $("#auau source").attr('src',url);
            $("#auau").load();
        }
	</script>
</head>

<body>
<div class="callapp_head" style="padding: 13px 5px 0 13px;">მომართვები<span class="callapp_refresh"><img alt="refresh" src="media/images/icons/refresh.png" height="14" width="14">   განახლება</span><hr class="callapp_head_hr"></div>
	<div id="tabs" ">
   		
		<table id="table_right_menu" >
            <tr>
				<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4; background-color: #0C6A57; color: white" id="show_copy_prit_exel" myvar="0">
                	Excel/Print
                </td>
            </tr>
        </table>

        	<div id="button_area" style="margin-top: 50px;">
            	<div class="left" style="width: 250px;">
            		<label for="search_start_my" class="left" style="margin: 5px 0 0 9px;">დასაწყისი</label>
            		<input style="width: 150px; margin-left: 5px; height: 15px;" value="<?php echo date('Y-m-d')." 00:00";?>" type="text" name="search_start_my" id="search_start_my" class="inpt left"/>
            	</div>
            	<div class="right" style="">
            		<button style="margin-left: 15px;" id="fillter_in">ფილტრი</button>
        		</div>
            	<div class="right" style="">
            		<label for="search_end_my" class="left" style="margin: 5px 0 0 9px;">დასასრული</label>
            		<input style="width: 150px; margin-left: 5px; height: 15px;" value="<?php echo  date('Y-m-d')." 23:59"; ?>" type="text" name="search_end_my" id="search_end_my" class="inpt right" />
        		</div>
        		
        	</div>		
            <table class="display" id="example" style="width: 100%;">
                <thead style="width: 10px;">
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width: 20%;">ოპერატორი</th>
                        <th style="width: 10%;">ყველა მომართვა</th>
                        <th style="width: 10%;">ნაპასუხები ზარი</th>
                        <th style="width: 10%;">ნაპასუხები ჩატი</th>
                        <th style="width: 10%;">გამავალი ზარი</th>
                        <th style="width: 10%;">ფეისბუქ ჩატი</th>
                        <th style="width: 10%;">მეილი</th>
                        <th style="width: 10%;">საიტის ჩატი</th>
                        <th style="width: 10%;">მანუალური</th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                        	<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
                        </th>
                        <th>
                        	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                        </th>                            
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                        </th>
                        <th>
                            <input type="text" name="search_phone" value="ფილტრი" class="search_init"/>
                        </th>
                        <th>
                            <input type="text" name="search_phone" value="ფილტრი" class="search_init"/>
                        </th>
                        <th>
                            <input type="text" name="search_phone" value="ფილტრი" class="search_init"/>
                        </th>
                        <th>
                            <input type="text" name="search_phone" value="ფილტრი" class="search_init"/>
                        </th>
                        <th>
                            <input type="text" name="search_phone" value="ფილტრი" class="search_init"/>
                        </th>
                        <th>
                            <input type="text" name="search_phone" value="ფილტრი" class="search_init"/>
                        </th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>&nbsp;</th>
                        <th>ჯამი:<br>სულ ჯამი:</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                    </tr>
                </tfoot>
            </table>
		</div>
         <!-- jQuery Dialog -->
	<div id="audio_dialog" title="ჩანაწერი">
	</div>
    <div id="add_edit_form"  class="form-dialog"  title="მომართვები"></div>
    <div id="add_edit_form1"  class="form-dialog"  title="მომართვები"></div>
</body>