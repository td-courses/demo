<head>
<meta http-equiv="refresh" content="300">
<style type="text/css">

    .callapp_head{
        font-family: pvn;
        font-weight: bold;
        font-size: 20px;
        color: #2681DC;
    }
    .callapp_head_hr{
        border: 1px solid #2681DC;
    }
    .callapp_refresh{
        padding: 5px;
        border-radius:3px;
        color:#FFF;
        background: #9AAF24;
        float: right;
        font-size: 13px;
        cursor: pointer;
    }
    .ui-widget-content {
        border: none;
    }

</style>
<script type="text/javascript">

        // define global object
        const $G = {
            aJaxURL: "server-side/report/sms_report.action.php"
        }
        
        // document ready
		$(document).ready(function () { 
			GetDate("start_time");
			GetDate("end_time");
            // load main table
			loadMainTable();
			$("#show_report").button();
		});
        
        // define main table loader function
		function loadMainTable(){
			start_time = $('#start_time').val();
			end_time   = $('#end_time').val();
			GetDataTable("example", $G.aJaxURL, "get_list", 7, "start_time="+start_time + "&end_time=" + end_time, 0, "", 1, "desc", "", "<'dataTable_buttons'T><'F'Cfipl>");
			setTimeout(function(){$('.ColVis, .dataTable_buttons').css('display','none');}, 90);
        }
        
		
		$(document).on("click", "#show_report", () => loadMainTable());
        // click on refresh button
        $(document).on("click", ".callapp_refresh", () => loadMainTable());

	
</script>
</head>

<body>
<div class="callapp_head" style="padding: 15px 0 0 15px;">SMS კომუნიკაცია<span class="callapp_refresh"><img alt="refresh" src="media/images/icons/refresh.png" height="14" width="14">განახლება</span><hr class="callapp_head_hr"></div>
<div id="tabs" style="width: 99%;">
<div id="button_area">
	<div class="left" style="width: 200px;">
		<label for="search_start" class="left" style="margin: 6px 0 0 9px; font-size: 12px;">დასაწყისი</label>
		<input type="text" name="search_start" id="start_time" value="" class="inpt right" style="width: 110px; height: 16px;"/>
	</div>
	<div class="right" style="width: 210px;">
		<label for="search_end" class="left" style="margin: 6px 0 0 9px; font-size: 12px;">დასასრული</label>
		<input type="text" name="search_end" id="end_time" value="" class="inpt right" style="width: 110px; height: 16px;"/>
	</div>	
</div>
<input style="margin-left: 15px;" id="show_report" name="show_report" type="submit" value="რეპორტების ჩვენება">	
	
<table class="display" id="example" style="width: 100%;">
    <thead>
        <tr id="datatable_header">
            <th>ID</th>
            <th style="width: 12%;">თარიღი</th>
            <th style="width: 12%;">ავტორი</th>
            <th style="width: 12%;">ადრესატი</th>
            <th style="width: 10%;">User Id</th>
            <th style="width: 44%;">ტექსტი</th>
            <th style="width: 10%;">სტატუსი</th>
        </tr>
    </thead>
    <thead>
        <tr class="search_header">
            <th>
                <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 80%"/>
            </th>
            <th>
                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80%" />
            </th>
            <th>
                <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 80%"/>
            </th>
            <th>
                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80%" />
            </th>
            <th>
                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80%" />
            </th>
            <th>
                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80%" />
            </th>
        </tr>
    </thead>
</table>

</body>
	