<head>
<style type="text/css">
    caption{
        margin: 0;
    	padding: 0;
    	background: #f3f3f3;
    	height: 40px;
    	line-height: 40px;
    	text-indent: 2px;
    	font-family: "Trebuchet MS", Trebuchet, Arial, sans-serif;
    	font-size: 140%;
    	font-weight: bold;
    	color: #000;
    	text-align: left;
    	letter-spacing: 1px;
    	border-top: dashed 1px #c2c2c2;
    	border-bottom: dashed 1px #c2c2c2;
    }
    div, caption, td, th, h2, h3, h4 {
    	font-size: 12px;
    	font-family: verdana,sans-serif;
    	voice-family: "\"}\"";
    	voice-family: inherit;
    	color: #333;
    }
    tbody {
    	display: table-row-group;
    	vertical-align: middle;
    	border-color: inherit;
    }
    tbody tr {
    	background: #dfedf3;
    	font-size: 110%;
    }
    tr {
    	display: table-row;
    	vertical-align: inherit;
    	border-color: inherit;
    }
    tbody tr th, tbody tr td {
    	padding: 5px;
    	border: solid 1px #326e87;
    	text-align: center;
    	vertical-align:middle;
    }
    thead tr th {
    	height: 32px;
    	aline-height: 32px;
    	text-align: center;
    	vertical-align:middle;
    	color: #1c5d79;
    	background: #CBDFEE;
    	border-left: solid 1px #326e87;
    	border-right: solid 1px #326e87;
    	border-collapse: collapse;
    }
    table.sortable a.sortheader {
    	text-decoration: none;
    	display: block;
    	color: #1c5d79;
    	xcolor: #000000;
    	font-weight: bold;
    }
    a{
    	cursor: pointer;
    }
    .tdstyle{
    	text-align: left;
    	vertical-align:middle;
    }
    .callapp_head{
    	font-family: pvn;
    	font-weight: bold;
    	font-size: 20px;
    	color: #2681DC;
    }
    .callapp_head_hr{
    	border: 1px solid #2681DC;
    }
    .ui-widget-content {
        border: 0px solid #2681dc;
    }
</style>
<script src="js/highcharts.js"></script>
<script src="js/exporting.js"></script>
<script type="text/javascript">
		var aJaxURL		      = "server-side/report/technical_chat.action.php";
		var aJaxURL1	      = "server-side/report/sales_statistics_chat.action.php";	
		var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
		var tName		      = "example0";
		var tbName		      = "tabs";
		var fName		      = "add-edit-form";
		var file_name 	      = '';
		var rand_file 	      = '';
		
		$(document).ready(function () {   
			GetTabs(tbName);   	
			GetDateTimes("start_time");
			GetDateTimes("end_time");
			$("#show_report").button({});
		});

		$(document).on("tabsactivate", "#tabs", function() {
        	var tab = GetSelectedTab(tbName);
        	if (tab == 0) {
        		drawFirstLevel();
        		drawFirstLevel1();
        		$(this).css('height','700px');
        	}else if(tab == 1){
        		getData();
       			getData11();
       			getData1();
       			$(this).css('height','1600px');
            }else if(tab == 2){
            	getData5();
            	$(this).css('height','800px');
            }else if(tab == 3){
            	getData4();
            	getData8();
            	getData2();
            	getData9();
            	getData3();
            	getData10();
            	$(this).css('height','1700px');
            }
        });

		function getData(){
			 var options = {
			        chart: {
			            renderTo: 'chart_container',
			            margin: [ 50, 50, 100, 80]
			        },
			        title: {
			            text: 'ნაპასუხები ჩატები ოპერატორების მიხედვით',
			            x: -20 
			        },
			       
			        xAxis: {
			            categories: [],
			            labels: {
			            	 rotation: -45,
			            	align: 'right'
			            }
			        },
			        yAxis: {
			            title: {
			                text: 'ჩატები'
			            },
			            plotLines: [{
			                value: 0,
			                width: 1,
			                color: '#808080'
			            }]
			        },
			        tooltip: {
			        	//valueSuffix: ' áƒªáƒ�áƒšáƒ˜'
			                
			        },
			        legend: {
		                layout: 'vertical',
		                align: 'left',
		                verticalAlign: 'top',
		                borderWidth: 0
			        },
			        series: []
			    }

			 var i=0;
			
				agent	= '';
				queuet = '';
			
				var optionss = $('#myform_List_Queue_to option');
				var values = $.map(optionss ,function(option) {
					if(queuet != ""){
						queuet+=",";
						
					}
					queuet+="'"+option.value+"'";
				});
			
			var optionss = $('#myform_List_Agent_to option');
			var values = $.map(optionss ,function(option) {
				if(agent != ''){
					agent+=',';
					
				}
				agent+="'"+option.value+"'";
			});
			
			start_time = $('#start_time').val();
			end_time = $('#end_time').val();

			    $.getJSON("server-side/report/sales_statistics_chat.action.php?start="+start_time + "&end=" + end_time + "&agent=" + agent + "&queuet=" + queuet, function(json) {
				    
			    	options.xAxis.categories = json[1]['agent'];
			    	options.tooltip.valueSuffix = json[1]['unit'];
			    	options.series[0] = {};
			    	options.series[0].name = json[1]['name'];
			    	options.series[0].data = json[1]['call_count'];
			    	options.series[0].type = "column";
			    	
			        chart = new Highcharts.Chart(options);
			        
			    });
		}
		function getData1(){
			 var options = {
			        chart: {
			            renderTo: 'chart_container1',
			            margin: [ 50, 50, 100, 80]
			        },
			        title: {
			            text: 'კავშირის გაწყვეტის მიზეზეი',
			            x: -20 
			        },
			        subtitle: {
			            text: '',
			            x: -20
			        },
			        xAxis: {
			            categories: [],
			            labels: {
			            	 rotation: -45,
			            	align: 'right'
			            }
			        },
			        yAxis: {
			            title: {
			                text: 'ჩატები'
			            },
			            plotLines: [{
			                value: 0,
			                width: 1,
			                color: '#808080'
			            }]
			        },
			        tooltip: {
			        	//valueSuffix: ' áƒªáƒ�áƒšáƒ˜'
			                
			        },
			        legend: {
		                layout: 'vertical',
		                align: 'left',
		                verticalAlign: 'top',
		                borderWidth: 0
			        },
			        series: []
			    }

			 var i=0;
			
				agent	= '';
				queuet = '';
			
				var optionss = $('#myform_List_Queue_to option');
				var values = $.map(optionss ,function(option) {
					if(queuet != ""){
						queuet+=",";
						
					}
					queuet+="'"+option.value+"'";
				});
			
			var optionss = $('#myform_List_Agent_to option');
			var values = $.map(optionss ,function(option) {
				if(agent != ''){
					agent+=',';
					
				}
				agent+="'"+option.value+"'";
			});
			
			start_time = $('#start_time').val();
			end_time = $('#end_time').val();

			    $.getJSON("server-side/report/sales_statistics_chat.action.php?start="+start_time + "&end=" + end_time + "&agent=" + agent + "&queuet=" + queuet, function(json) {
				    
			    	options.xAxis.categories = json[0]['cause'];
			    	options.tooltip.valueSuffix = json[0]['unit'];
			    	options.series[0] = {};
			    	options.series[0].name = json[0]['name'];
			    	options.series[0].data = json[0]['quantity'];
			    	options.series[0].type = "column";
			    	
			        chart = new Highcharts.Chart(options);
			        
			    });
		}
		function getData2(){
			 var options = {
			        chart: {
			            renderTo: 'chart_container2',
			            margin: [ 50, 50, 100, 80]
			        },
			        title: {
			            text: 'ნაპასუხები ჩატები საათების მიხედვით',
			            x: -20 
			        },
			        subtitle: {
			            text: '',
			            x: -20
			        },
			        xAxis: {
			            categories: [],
			            labels: {
			            	 rotation: -45,
			            	align: 'right'
			            }
			        },
			        yAxis: {
			            title: {
			                text: 'ჩატები'
			            },
			            plotLines: [{
			                value: 0,
			                width: 1,
			                color: '#808080'
			            }]
			        },
			        tooltip: {
			        	//valueSuffix: ' áƒªáƒ�áƒšáƒ˜'
			                
			        },
			        legend: {
		                layout: 'vertical',
		                align: 'left',
		                verticalAlign: 'top',
		                borderWidth: 0
			        },
			        series: []
			    }

			 var i=0;
			
				agent	= '';
				queuet = '';
			
				var optionss = $('#myform_List_Queue_to option');
				var values = $.map(optionss ,function(option) {
					if(queuet != ""){
						queuet+=",";
						
					}
					queuet+="'"+option.value+"'";
				});
			
			var optionss = $('#myform_List_Agent_to option');
			var values = $.map(optionss ,function(option) {
				if(agent != ''){
					agent+=',';
					
				}
				agent+="'"+option.value+"'";
			});
			
			start_time = $('#start_time').val();
			end_time = $('#end_time').val();

			    $.getJSON("server-side/report/sales_statistics_chat.action.php?start="+start_time + "&end=" + end_time + "&agent=" + agent + "&queuet=" + queuet, function(json) {
				    
			    	options.xAxis.categories = json[2]['datetime'];
			    	options.tooltip.valueSuffix = json[2]['unit'];
			    	options.series[0] = {};
			    	options.series[0].name = json[2]['name'];
			    	options.series[0].data = json[2]['answer_count'];
			    	options.series[0].type = "column";
			    	
			        chart = new Highcharts.Chart(options);
			        
			    });
		}
		function getData3(){
			 var options = {
			        chart: {
			            renderTo: 'chart_container3',
			            margin: [ 50, 50, 100, 80]
			        },
			        title: {
			            text: 'ნაპასუხები ჩატები კვირის დღეების მიხედვით',
			            x: -20 
			        },
			        subtitle: {
			            text: '',
			            x: -20
			        },
			        xAxis: {
			            categories: [],
			            labels: {
			            	 rotation: -45,
			            	align: 'right'
			            }
			        },
			        yAxis: {
			            title: {
			                text: 'ჩატები'
			            },
			            plotLines: [{
			                value: 0,
			                width: 1,
			                color: '#808080'
			            }]
			        },
			        tooltip: {
			        	//valueSuffix: ' áƒªáƒ�áƒšáƒ˜'
			                
			        },
			        legend: {
		                layout: 'vertical',
		                align: 'left',
		                verticalAlign: 'top',
		                borderWidth: 0
			        },
			        series: []
			    }

			 var i=0;
			
				agent	= '';
				queuet = '';
			
				var optionss = $('#myform_List_Queue_to option');
				var values = $.map(optionss ,function(option) {
					if(queuet != ""){
						queuet+=",";
						
					}
					queuet+="'"+option.value+"'";
				});
			
			var optionss = $('#myform_List_Agent_to option');
			var values = $.map(optionss ,function(option) {
				if(agent != ''){
					agent+=',';
					
				}
				agent+="'"+option.value+"'";
			});
			
			start_time = $('#start_time').val();
			end_time = $('#end_time').val();

			    $.getJSON("server-side/report/sales_statistics_chat.action.php?start="+start_time + "&end=" + end_time + "&agent=" + agent + "&queuet=" + queuet, function(json) {
				    
			    	options.xAxis.categories = json[3]['datetime1'];
			    	options.tooltip.valueSuffix = json[3]['unit'];
			    	options.series[0] = {};
			    	options.series[0].name = json[3]['name'];
			    	options.series[0].data = json[3]['answer_count1'];
			    	options.series[0].type = "column";
			    	
			        chart = new Highcharts.Chart(options);
			        
			    });
		}




		function getData4(){
			 var options = {
			        chart: {
			            renderTo: 'chart_container4',
			            margin: [ 50, 50, 100, 80]
			        },
			        title: {
			            text: 'ნაპასუხები ჩატები დღეების მიხედვით',
			            x: -20 
			        },
			        subtitle: {
			            text: '',
			            x: -20
			        },
			        xAxis: {
			            categories: [],
			            labels: {
			            	 rotation: -45,
			            	align: 'right'
			            }
			        },
			        yAxis: {
			            title: {
			                text: 'ჩატები'
			            },
			            plotLines: [{
			                value: 0,
			                width: 1,
			                color: '#808080'
			            }]
			        },
			        tooltip: {
			        	//valueSuffix: ' áƒªáƒ�áƒšáƒ˜'
			                
			        },
			        legend: {
		                layout: 'vertical',
		                align: 'left',
		                verticalAlign: 'top',
		                borderWidth: 0
			        },
			        series: []
			    }

			 var i=0;
			
				agent	= '';
				queuet = '';
			
				var optionss = $('#myform_List_Queue_to option');
				var values = $.map(optionss ,function(option) {
					if(queuet != ""){
						queuet+=",";
						
					}
					queuet+="'"+option.value+"'";
				});
			
			var optionss = $('#myform_List_Agent_to option');
			var values = $.map(optionss ,function(option) {
				if(agent != ''){
					agent+=',';
					
				}
				agent+="'"+option.value+"'";
			});
			
			start_time = $('#start_time').val();
			end_time = $('#end_time').val();

			    $.getJSON("server-side/report/sales_statistics_chat.action.php?start="+start_time + "&end=" + end_time + "&agent=" + agent + "&queuet=" + queuet, function(json) {
				    
			    	options.xAxis.categories = json[4]['datetime2'];
			    	options.tooltip.valueSuffix = json[4]['unit'];
			    	options.series[0] = {};
			    	options.series[0].name = json[4]['name'];
			    	options.series[0].data = json[4]['answer_count2'];
			    	options.series[0].type = "column";
			    	
			        chart = new Highcharts.Chart(options);
			        
			    });
		}

		function getData5(){
			 var options = {
			        chart: {
			            renderTo: 'chart_container5',
			            margin: [ 50, 50, 100, 80]
			        },
			        title: {
			            text: 'კავშირის გაწყვეტის მიზეზი',
			            x: -20 
			        },
			        subtitle: {
			            text: '',
			            x: -20
			        },
			        xAxis: {
			            categories: [],
			            labels: {
			            	 rotation: -45,
			            	align: 'right'
			            }
			        },
			        yAxis: {
			            title: {
			                text: 'ჩატები'
			            },
			            plotLines: [{
			                value: 0,
			                width: 1,
			                color: '#808080'
			            }]
			        },
			        tooltip: {
			        	//valueSuffix: ' áƒªáƒ�áƒšáƒ˜'
			                
			        },
			        legend: {
		                layout: 'vertical',
		                align: 'left',
		                verticalAlign: 'top',
		                borderWidth: 0
			        },
			        series: []
			    }

			 var i=0;
			
				agent	= '';
				queuet = '';
			
				var optionss = $('#myform_List_Queue_to option');
				var values = $.map(optionss ,function(option) {
					if(queuet != ""){
						queuet+=",";
						
					}
					queuet+="'"+option.value+"'";
				});
			
			var optionss = $('#myform_List_Agent_to option');
			var values = $.map(optionss ,function(option) {
				if(agent != ''){
					agent+=',';
					
				}
				agent+="'"+option.value+"'";
			});
			
			start_time = $('#start_time').val();
			end_time = $('#end_time').val();

			    $.getJSON("server-side/report/sales_statistics_chat.action.php?start="+start_time + "&end=" + end_time + "&agent=" + agent + "&queuet=" + queuet, function(json) {
				    
			    	options.xAxis.categories = json[5]['cause1'];
			    	options.tooltip.valueSuffix = json[5]['unit'];
			    	options.series[0] = {};
			    	options.series[0].name = json[5]['name'];
			    	options.series[0].data = json[5]['answer_count3'];
			    	options.series[0].type = "column";
			    	
			        chart = new Highcharts.Chart(options);
			        
			    });
		}


		 function getData8(){
			 var options = {
			        chart: {
			            renderTo: 'chart_container8',
			            margin: [ 50, 50, 100, 80]
			        },
			        title: {
			            text: 'უპასუხო ჩატები დღეების მიხედვით',
			            x: -20 
			        },
			        subtitle: {
			            text: '',
			            x: -20
			        },
			        xAxis: {
			            categories: [],
			            labels: {
			            	 rotation: -45,
			            	align: 'right'
			            }
			        },
			        yAxis: {
			            title: {
			                text: 'ჩატები'
			            },
			            plotLines: [{
			                value: 0,
			                width: 1,
			                color: '#808080'
			            }]
			        },
			        tooltip: {
			        	//valueSuffix: ' áƒªáƒ�áƒšáƒ˜'
			                
			        },
			        legend: {
		                layout: 'vertical',
		                align: 'left',
		                verticalAlign: 'top',
		                borderWidth: 0
			        },
			        series: []
			    }

			 var i=0;
			
				agent	= '';
				queuet = '';
			
				var optionss = $('#myform_List_Queue_to option');
				var values = $.map(optionss ,function(option) {
					if(queuet != ""){
						queuet+=",";
						
					}
					queuet+="'"+option.value+"'";
				});
			
			var optionss = $('#myform_List_Agent_to option');
			var values = $.map(optionss ,function(option) {
				if(agent != ''){
					agent+=',';
					
				}
				agent+="'"+option.value+"'";
			});
			
			start_time = $('#start_time').val();
			end_time = $('#end_time').val();

			    $.getJSON("server-side/report/sales_statistics_chat.action.php?start="+start_time + "&end=" + end_time + "&agent=" + agent + "&queuet=" + queuet, function(json) {
				    
			    	options.xAxis.categories = json[8]['times'];
			    	options.tooltip.valueSuffix = json[8]['unit'];
			    	options.series[0] = {};
			    	options.series[0].name = json[8]['name'];
			    	options.series[0].data = json[8]['unanswer_call'];
			    	options.series[0].type = "column";
			    	
			        chart = new Highcharts.Chart(options);
			        
			    });
		}



		 function getData9(){
			 var options = {
			        chart: {
			            renderTo: 'chart_container9',
			            margin: [ 50, 50, 100, 80]
			        },
			        title: {
			            text: 'უპასუხო ჩატები საათების  მიხედვით',
			            x: -20 
			        },
			        subtitle: {
			            text: '',
			            x: -20
			        },
			        xAxis: {
			            categories: [],
			            labels: {
			            	 rotation: -45,
			            	align: 'right'
			            }
			        },
			        yAxis: {
			            title: {
			                text: 'ჩატები'
			            },
			            plotLines: [{
			                value: 0,
			                width: 1,
			                color: '#808080'
			            }]
			        },
			        tooltip: {
			        	//valueSuffix: ' áƒªáƒ�áƒšáƒ˜'
			                
			        },
			        legend: {
		                layout: 'vertical',
		                align: 'left',
		                verticalAlign: 'top',
		                borderWidth: 0
			        },
			        series: []
			    }

			 var i=0;
			
				agent	= '';
				queuet = '';
			
				var optionss = $('#myform_List_Queue_to option');
				var values = $.map(optionss ,function(option) {
					if(queuet != ""){
						queuet+=",";
						
					}
					queuet+="'"+option.value+"'";
				});
			
			var optionss = $('#myform_List_Agent_to option');
			var values = $.map(optionss ,function(option) {
				if(agent != ''){
					agent+=',';
					
				}
				agent+="'"+option.value+"'";
			});
			
			start_time = $('#start_time').val();
			end_time = $('#end_time').val();

			    $.getJSON("server-side/report/sales_statistics_chat.action.php?start="+start_time + "&end=" + end_time + "&agent=" + agent + "&queuet=" + queuet, function(json) {
				    
			    	options.xAxis.categories = json[9]['times2'];
			    	options.tooltip.valueSuffix = json[9]['unit'];
			    	options.series[0] = {};
			    	options.series[0].name = json[9]['name'];
			    	options.series[0].data = json[9]['unanswer_count1'];
			    	options.series[0].type = "column";
			    	
			        chart = new Highcharts.Chart(options);
			        
			    });
		}

		 function getData10(){
			 var options = {
			        chart: {
			            renderTo: 'chart_container10',
			            margin: [ 50, 50, 100, 80]
			        },
			        title: {
			            text: 'უპასუხო ჩატები კვირის დღეების მიხედვით',
			            x: -20 
			        },
			        subtitle: {
			            text: '',
			            x: -20
			        },
			        xAxis: {
			            categories: [],
			            labels: {
			            	 rotation: -45,
			            	align: 'right'
			            }
			        },
			        yAxis: {
			            title: {
			                text: 'ჩატები'
			            },
			            plotLines: [{
			                value: 0,
			                width: 1,
			                color: '#808080'
			            }]
			        },
			        tooltip: {
			        	//valueSuffix: ' áƒªáƒ�áƒšáƒ˜'
			                
			        },
			        legend: {
		                layout: 'vertical',
		                align: 'left',
		                verticalAlign: 'top',
		                borderWidth: 0
			        },
			        series: []
			    }

			 var i=0;
			
				agent	= '';
				queuet = '';
			
				var optionss = $('#myform_List_Queue_to option');
				var values = $.map(optionss ,function(option) {
					if(queuet != ""){
						queuet+=",";
						
					}
					queuet+="'"+option.value+"'";
				});
			
			var optionss = $('#myform_List_Agent_to option');
			var values = $.map(optionss ,function(option) {
				if(agent != ''){
					agent+=',';
					
				}
				agent+="'"+option.value+"'";
			});
			
			start_time = $('#start_time').val();
			end_time = $('#end_time').val();

			    $.getJSON("server-side/report/sales_statistics_chat.action.php?start="+start_time + "&end=" + end_time + "&agent=" + agent + "&queuet=" + queuet, function(json) {
				    
			    	options.xAxis.categories = json[10]['date1'];
			    	options.tooltip.valueSuffix = json[10]['unit'];
			    	options.series[0] = {};
			    	options.series[0].name = json[10]['name'];
			    	options.series[0].data = json[10]['unanswer_count2'];
			    	options.series[0].type = "column";
			    	
			        chart = new Highcharts.Chart(options);
			        
			    });
			    
		}

		 function drawFirstLevel(){
			    var options = {
			                  chart: {
			                      renderTo: 'chart_container0',
			                      plotBackgroundColor: null,
			                      plotBorderWidth: null,
			                      plotShadow: null,
			                  },
			                  colors: ['#538DD5', '#FA3A3A'],
			                  title: {
			                      text: 'ტექნიკური ინფორმაცია'
			                  },
			                  tooltip: {
			                      formatter: function() {
			                          return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
			                      }
			                  },
			                  plotOptions: {
			                   pie: {
			                          allowPointSelect: true,
			                          cursor: 'pointer',
			                          dataLabels: {
			                              enabled: true,
			                              color: '#000000',
			                              connectorColor: '#FA3A3A',
			                              formatter: function() {
			                                  return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
			                              }
			                          },
			                          point: {
			                              events: {
			                                  click: function() {                   
			                                  }
			                              }
			                          }
			                      }
			                  },
			                  series: [{
			                      type: 'pie',
			                      name: 'კატეგორიები',
			                     // color: '#FA3A3A',
			                      data: []
			                  }]
			              }
			    var i=0;
			    
			    agent = '';
			    queuet = '';
			   
			    var optionss = $('#myform_List_Queue_to option');
			    var values = $.map(optionss ,function(option) {
			     if(queuet != ""){
			      queuet+=",";
			      
			     }
			     queuet+="'"+option.value+"'";
			    });
			   
			   var optionss = $('#myform_List_Agent_to option');
			   var values = $.map(optionss ,function(option) {
			    if(agent != ''){
			     agent+=',';
			     
			    }
			    agent+="'"+option.value+"'";
			   });
			   
			   start_time = $('#start_time').val();
			   end_time = $('#end_time').val();
			              $.getJSON("server-side/report/cs_chat.action.php?start="+start_time + "&end=" + end_time + "&agent=" + agent + "&queuet=" + queuet, function(json) {
			                  options.series[0].data = json;
			                  chart = new Highcharts.Chart(options);
			              });
			   }
		 function drawFirstLevel1(){
			    var options = {
			                  chart: {
			                      renderTo: 'chart_container0r',
			                      plotBackgroundColor: null,
			                      plotBorderWidth: null,
			                      plotShadow: null,
			                  },
			                  colors: ['#538DD5', '#76933C'],
			                  title: {
			                      text: 'ტექნიკური ინფორმაცია'
			                  },
			                  tooltip: {
			                      formatter: function() {
			                          return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
			                      }
			                  },
			                  plotOptions: {
			                   pie: {
			                          allowPointSelect: true,
			                          cursor: 'pointer',
			                          dataLabels: {
			                              enabled: true,
			                              color: '#000000',
			                              connectorColor: '#FA3A3A',
			                              formatter: function() {
			                                  return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
			                              }
			                          },
			                          point: {
			                              events: {
			                                  click: function() {                   
			                                  }
			                              }
			                          }
			                      }
			                  },
			                  series: [{
			                      type: 'pie',
			                      name: 'კატეგორიები',
			                     // color: '#FA3A3A',
			                      data: []
			                  }]
			              }
			    var i=0;
			    
			    agent = '';
			    queuet = '';
			   
			    var optionss = $('#myform_List_Queue_to option');
			    var values = $.map(optionss ,function(option) {
			     if(queuet != ""){
			      queuet+=",";
			      
			     }
			     queuet+="'"+option.value+"'";
			    });
			   
			   var optionss = $('#myform_List_Agent_to option');
			   var values = $.map(optionss ,function(option) {
			    if(agent != ''){
			     agent+=',';
			     
			    }
			    agent+="'"+option.value+"'";
			   });
			   
			   start_time = $('#start_time').val();
			   end_time = $('#end_time').val();
			              $.getJSON("server-side/report/cs_chat1.action.php?start="+start_time + "&end=" + end_time + "&agent=" + agent + "&queuet=" + queuet, function(json) {
			                  options.series[0].data = json;
			                  chart = new Highcharts.Chart(options);
			              });
			   }
		 
		 function getData11(){
			 var options = {
			        chart: {
			            renderTo: 'chart_container11',
			            margin: [ 50, 50, 100, 80]
			        },
			        title: {
			            text: 'ნაპასუხები ჩატები წამების მიხედვით',
			            x: -20 
			        },
			        subtitle: {
			            text: '',
			            x: -20
			        },
			        xAxis: {
			            categories: [],
			            labels: {
			            	 rotation: -45,
			            	align: 'right'
			            }
			        },
			        yAxis: {
			            title: {
			                text: 'ჩატები'
			            },
			            plotLines: [{
			                value: 0,
			                width: 1,
			                color: '#808080'
			            }]
			        },
			        tooltip: {
			        	//valueSuffix: ' áƒªáƒ�áƒšáƒ˜'
			                
			        },
			        legend: {
		                layout: 'vertical',
		                align: 'left',
		                verticalAlign: 'top',
		                borderWidth: 0
			        },
			        series: []
			    }

			 var i=0;
			
				agent	= '';
				queuet = '';
			
				var optionss = $('#myform_List_Queue_to option');
				var values = $.map(optionss ,function(option) {
					if(queuet != ""){
						queuet+=",";
						
					}
					queuet+="'"+option.value+"'";
				});
			
			var optionss = $('#myform_List_Agent_to option');
			var values = $.map(optionss ,function(option) {
				if(agent != ''){
					agent+=',';
					
				}
				agent+="'"+option.value+"'";
			});
			
			start_time = $('#start_time').val();
			end_time = $('#end_time').val();

			    $.getJSON("server-side/report/sales_statistics_chat.action.php?start="+start_time + "&end=" + end_time + "&agent=" + agent + "&queuet=" + queuet, function(json) {
				    
			    	options.xAxis.categories = json[11]['call_second'];
			    	options.tooltip.valueSuffix = json[11]['unit'];
			    	options.series[0] = {};
			    	options.series[0].name = json[11]['name'];
			    	options.series[0].data = json[11]['mas'];
			    	options.series[0].type = "column";
			    	
			        chart = new Highcharts.Chart(options);
			        
			    });
			    
		}
      
		function go_next(val,par,name){
			if(val != undefined){
				$("#myform_List_"+par+"_from option:selected").remove();
				$("#myform_List_"+par+"_to").append(new Option(name, val));
			}
		}

		function go_previous(val,par,name){
			if(val != undefined){
				$("#myform_List_"+par+"_to option:selected").remove();
				$("#myform_List_"+par+"_from").append(new Option(name, val));
			}
		}

		function go_last(par){
			var options = $('#myform_List_'+par+'_from option');
			$("#myform_List_"+par+"_from option").remove();
			var i = 0;
			var values = $.map(options ,function(option) {
			    $("#myform_List_"+par+"_to").append(new Option(options[i].text, options[i].value));
			    i++;
			});
		}

		function go_first(par){
			var options = $('#myform_List_'+par+'_to option');
			$("#myform_List_"+par+"_to option").remove();
			var i = 0;
			var values = $.map(options ,function(option) {
			    $("#myform_List_"+par+"_from").append(new Option(options[i].text, options[i].value));
			    i++;
			});
		}

		$(document).on("click", "#show_report", function () {
			var i=0;
			paramq 			= new Object();
			parama 			= new Object();
			parame 			= new Object();
			parame.agent	= '';
			parame.queuet = '';
			paramm		= "server-side/report/technical_chat.action.php";
			
			drawFirstLevel();
			drawFirstLevel1();
			

			var options = $('#myform_List_Agent_to option');
			var values = $.map(options ,function(option) {
				if(parame.agent != ''){
					parame.agent+=',';
					
				}
				parame.agent+="'"+option.value+"'";
			});
			
			parame.start_time = $('#start_time').val();
			parame.end_time = $('#end_time').val();
			parame.act = 'check';
			if(parame.agent==''){
				alert('აირჩიე ოპერატორი');
			}else{
				$.ajax({
			        url: paramm,
				    data: parame,
			        success: function(data) {		        	
						$("#answer_call").html(data.page.answer_call);
						$("#technik_info").html(data.page.technik_info);
						$(".report_info").html(data.page.report_info);
						$("#answer_call_info").html(data.page.answer_call_info);
						$("#answer_call_by_queue").html(data.page.answer_call_by_queue);
						$("#disconnection_cause").html(data.page.disconnection_cause);
						$("#unanswer_call").html(data.page.unanswer_call);
						$("#disconnection_cause_unanswer").html(data.page.disconnection_cause_unanswer);
						$("#totals").html(data.page.totals);
						$("#call_distribution_per_day").html(data.page.call_distribution_per_day);
						$("#call_distribution_per_hour").html(data.page.call_distribution_per_hour);
						$("#call_distribution_per_day_of_week").html(data.page.call_distribution_per_day_of_week);
						$("#service_level").html(data.page.service_level);
				    }
			    });
			}
        });

		$(document).on("click", "#answear_dialog", function () {
			LoadDialog();
		});
		
		$(document).on("click", "#unanswear_dialog", function () {
			LoadDialog1();
		});

		$(document).on("click", "#undone_dialog", function () {
			LoadDialog2();
		});
		
		var record;
 		function play(record){
			
 			link = 'http://91.233.15.136:8181/' + record;
 			GetDialog_audio("audio_dialog", "auto", "auto","" );
 			$(".ui-dialog-buttonpane").html(" ");
 			$( ".ui-dialog-buttonpane" ).removeClass( "ui-widget-content ui-helper-clearfix ui-dialog-buttonpane" );
 			$("#audio_dialog").html('<audio controls autoplay style="width:500px; min-height: 33px;"><source src="'+link+'" type="audio/wav"> Your browser does not support the audio element.</audio>');
 		}
		function LoadDialog(){
			parame 				= new Object();
			paramm		= "server-side/report/technical_chat.action.php";
			parame.start_time 	= $('#start_time').val();
			parame.end_time 	= $('#end_time').val();
			parame.act 			= 'answear_dialog';
			parame.agent	= '';
			parame.queuet = '';
			
			
			var options = $('#myform_List_Queue_to option');
			var values = $.map(options ,function(option) {
				if(parame.queuet != ""){
					parame.queuet+=",";
					
				}
				parame.queuet+="'"+option.value+"'";
			});

			
			var options = $('#myform_List_Agent_to option');
			var values = $.map(options ,function(option) {
				if(parame.agent != ''){
					parame.agent+=',';
					
				}
				parame.agent+="'"+option.value+"'";
			});
			$.ajax({
		        url: paramm,
			    data: parame,
		        success: function(data) {		        	
					$("#add-edit-form").html(data.page.answear_dialog);
					var button = {
							"cancel": {
					            text: "დახურვა",
					            id: "cancel-dialog",
					            click: function () {
					                $(this).dialog("close");
					            }
					        }
						};
					GetDialog("add-edit-form", 1000, "auto", button);
					/* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
					GetDataTable("example", aJaxURL, "answear_dialog_table&start_time="+parame.start_time+"&end_time="+parame.end_time+"&queuet="+parame.queuet+"&agent="+parame.agent,9, "", 0, "", 1, "desc", "", change_colum_main);

			    }
		    });
		}
		
		function LoadDialog1(){
			parame 				= new Object();
			paramm		= "server-side/report/technical_chat.action.php";
			parame.start_time 	= $('#start_time').val();
			parame.end_time 	= $('#end_time').val();
			parame.act 			= 'unanswear_dialog';
			parame.agent	= '';
			parame.queuet = '';
			
			
			var options = $('#myform_List_Queue_to option');
			var values = $.map(options ,function(option) {
				if(parame.queuet != ""){
					parame.queuet+=",";
					
				}
				parame.queuet+="'"+option.value+"'";
			});

			
			var options = $('#myform_List_Agent_to option');
			var values = $.map(options ,function(option) {
				if(parame.agent != ''){
					parame.agent+=',';
					
				}
				parame.agent+="'"+option.value+"'";
			});
			$.ajax({
		        url: paramm,
			    data: parame,
		        success: function(data) {		        	
					$("#add-edit-form-unanswer").html(data.page.unansver_dialog);
					var button = {
							"cancel": {
					            text: "დახურვა",
					            id: "cancel-dialog",
					            click: function () {
					                $(this).dialog("close");
					            }
					        }
						};
					GetDialog("add-edit-form-unanswer", 1000, "auto", button);
					/* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
					GetDataTable("example_una", aJaxURL, "unanswear_dialog_table&start_time="+parame.start_time+"&end_time="+parame.end_time+"&queuet="+parame.queuet,6, "", 0, "", 1, "desc", "", change_colum_main);

			    }
		    });
		}

		function LoadDialog2(){
			parame 				= new Object();
			paramm		= "server-side/report/technical_chat.action.php";
			parame.start_time 	= $('#start_time').val();
			parame.end_time 	= $('#end_time').val();
			parame.act 			= 'undone_dialog';
			parame.agent	= '';
			parame.queuet = '';
			
			
			var options = $('#myform_List_Queue_to option');
			var values = $.map(options ,function(option) {
				if(parame.queuet != ""){
					parame.queuet+=",";
					
				}
				parame.queuet+="'"+option.value+"'";
			});

			
			var options = $('#myform_List_Agent_to option');
			var values = $.map(options ,function(option) {
				if(parame.agent != ''){
					parame.agent+=',';
					
				}
				parame.agent+="'"+option.value+"'";
			});
			$.ajax({
		        url: paramm,
			    data: parame,
		        success: function(data) {		        	
					$("#add-edit-form-undone").html(data.page.undone_dialog);
					var button = {
							"cancel": {
					            text: "დახურვა",
					            id: "cancel-dialog",
					            click: function () {
					                $(this).dialog("close");
					            }
					        }
						};
					GetDialog("add-edit-form-undone", 1000, "auto", button);
					/* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
					GetDataTable("example2", aJaxURL, "undone_dialog_table&start_time="+parame.start_time+"&end_time="+parame.end_time+"&queuet="+parame.queuet+"&agent="+parame.agent,8, "", 0, "", 1, "desc", "", change_colum_main);
					
					$( "div" ).removeClass( "ui-widget-overlay" );
			    }
		    });
		}
    </script>
</head>
<body>
	<div class="callapp_head" style="padding: 13px 5px 0 5px;">ტექნიკური რეპორტები ჩატი<hr class="callapp_head_hr"></div>
    <div id="tabs" style="width: 99%; margin: 0 auto; height:885px;">
    	<ul>
    		<li><a href="#tab-0">მთავარი</a></li>
    		<li><a href="#tab-1">ნაპასუხები</a></li>
    		<li><a href="#tab-2">უპასუხო</a></li>
    		<li><a href="#tab-3">ჩატების განაწილება</a></li>
    	</ul>
    	<div id="tab-0">
    		
    		<div style="width: 27%; float:left;">
    			<span>აირჩიე ოპერატორი</span>
    			<hr>
    			<table border="0" cellspacing="0" cellpadding="8">
        			<tbody>
            			<tr>
                		    <td>ხელმისაწვდომია<br><br>
                    		<select size="10" name="excel_answer_call_by_agent_info" multiple="multiple" id="myform_List_Agent_from" style="height: 100px;width: 173px;">
							 <?php 
							    $db->setQuery("SELECT  `user_info`.`user_id`,
                                                		`user_info`.`name`
                                                FROM    `user_info`
                                                JOIN     users ON users.id = user_info.user_id
                                                WHERE    users.actived = 1");
							    
							    $res = $db->getResultArray();
							    foreach ($res[result] AS $aRow){
							        echo '<option value="'.$aRow[user_id].'">'.$aRow[name].'</option>';
							    }
							    
							  ?>
							</select>
                			</td>
                			<td align="left">
                    			<a  onclick="go_next($('#myform_List_Agent_from option:selected').val(),'Agent',$('#myform_List_Agent_from option:selected').text())"><img src="media/images/go-next.png" width="16" height="16" border="0"></a>
                    			<a  onclick="go_previous($('#myform_List_Agent_to option:selected').val(),'Agent',$('#myform_List_Agent_to option:selected').text())"><img src="media/images/go-previous.png" width="16" height="16" border="0"></a>
                    			<br>
                    			<br>
                    			<a  onclick="go_last('Agent')"><img src="media/images/go-last.png" width="16" height="16" border="0"></a>
                    			<a  onclick="go_first('Agent')"><img src="media/images/go-first.png" width="16" height="16" border="0"></a>
                			</td>
                			<td>
                    			არჩეული<br><br>
                    		    <select size="10" name="List_Agent[]" multiple="multiple" style="height: 100px;width: 125px;" id="myform_List_Agent_to" >
                    			</select>
                			</td>
            			</tr> 
        			</tbody>
    			</table>
    		</div>
    		<div id="rest" style="margin-top: 200px; width: 100%; float:none;">
    			<h2>თარიღის ამორჩევა</h2>
    			<hr>
    			<div id="button_area">
                	<div class="left" style="width: 180px;">
                		<label for="search_start" class="left" style="margin: 6px 0 0 9px; font-size: 12px;">დასაწყისი</label>
                		<input type="text" name="search_start" id="start_time" value="<?php echo  date('Y-m-d')." 00:00"; ?>" class="inpt right" style="width: 95px; height: 16px;"/>
                    </div>
                    <div class="right" style="width: 190px;">
                		<label for="search_end" class="left" style="margin: 6px 0 0 9px; font-size: 12px;">დასასრული</label>
                		<input type="text" name="search_end" id="end_time" value="<?php echo  date('Y-m-d')." 23:59"; ?>" class="inpt right" style="width: 95px; height: 16px;"/>
            		</div>	
            	</div>
            	<input style="margin-left: 15px;" id="show_report" name="show_report" type="submit" value="რეპორტების ჩვენება">
            	<table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-top: 50px">
                	<caption style="background: #fff;">ტექნიკური ინფორმაცია</caption>
                    <tbody>
                        <tr>
                        	<th></th>
                            <th>სულ</th>
                            <th style="background: #538DD5; color: #FFFFFF">ნაპასუხები</th>
                            <th style="background: #FA3A3A; color: #FFFFFF">უპასუხო</th>
                            <th style="background: #76933C; color: #FFFFFF">დამუშავებული</th>
                            <th style="background: #80080A; color: #FFFFFF">დაუმუშავებული</th>
                            <th>ნაპასუხებია</th>
                            <th>უპასუხოა</th>
                            <th>დამუშავებულია</th>
                            <th>დაუმუშავებელია</th>
                        </tr>
                        <tr id="technik_info">
                            <td>ჩატი</td>
                            <td></td>
                            <td id="answear_dialog" style="cursor: pointer; text-decoration: underline;"></td>
                            <td id="unanswear_dialog" style="cursor: pointer; text-decoration: underline;"></td>
                            <td></td>
                            <td id="undone_dialog" style="cursor: pointer; text-decoration: underline;"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <div id="chart_container0" style="float:left; width: 50%; height: 300px;"></div>
                <div id="chart_container0r" style="float:left; width: 50%; height: 300px; "></div>
    		</div>
    	</div>
    	
    	<div id="tab-1">
    		<table width="99%" cellpadding="3" cellspacing="3" border="0">
                <thead>
                    <tr>
                        <td valign="top" width="50%" style="padding:0 5px 0 0;">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            	<caption style="background: #fff;">რეპორტ ინფო</caption>
                                <tbody class="report_info">
                                    <tr>
                                        <td class="tdstyle">რიგი:</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                    	<td class="tdstyle">საწყისი თარიღი:</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="tdstyle">დასრულების თარიღი:</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="tdstyle">პერიოდი:</td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                		</td>
                        <td valign="top" width="50%">
                    		<table width="100%" border="0" cellpadding="0" cellspacing="0">
                            	<caption style="background: #fff;">ნაპასუხები ჩატები</caption>
                                <tbody id="answer_call_info">
                                    <tr> 
                                        <td class="tdstyle">ნაპასუხები ჩატები</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="tdstyle">საშ. ხანგრძლივობა:</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="tdstyle">სულ საუბრის ხანგრძლივობა:</td>
                                        <td> </td>
                                    </tr>
                                    <tr>
                                        <td class="tdstyle">ლოდინის საშ. ხანგრძლივობა:</td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                		</td>
                    </tr>
                </thead>
        	</table>
            <br>
            <table width="99%" cellpadding="3" cellspacing="3" border="0" class="sortable" id="table1">
            	<caption style="background: #fff;">ნაპასუხები ჩატები ოპერატორების მიხედვით</caption>
                <thead>
                    <tr>
                          <th><a  class="sortheader">ოპერატორი<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                          <th><a  class="sortheader">ჩატები<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                          <th><a  class="sortheader">% ჩატები<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                          <th><a  class="sortheader">ჩატის დრო<br>(სთ-წთ-წმ)<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                          <th><a  class="sortheader">% ჩატის დრო<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                          <th><a  class="sortheader">საშ. ჩატის ხანგრძლივობა<br>(სთ-წთ-წმ)<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                          <th><a  class="sortheader">ლოდინის დრო <br>(სთ-წთ-წმ)<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                          <th><a  class="sortheader">საშ. ლოდისნის ხანგრძლივობა <br>(სთ-წთ-წმ)<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                    </tr>
                </thead>
                <tbody id="answer_call_by_queue">
                </tbody>
            </table>
            <br>
            <div id="chart_container" style="float:left; width: 90%; height: 300px; margin-left: 20px;"></div>
            <br>
            <table width="47%" cellpadding="3" cellspacing="3" border="0" style="float:left;">
            	<caption style="background: #fff;">მომსახურების დონე(Service Level)</caption>
                <thead>
                    <tr>
                        <td valign="top" width="100%" bgcolor="#fffdf3">
                            <table width="100%" cellpadding="1" cellspacing="1" border="0" class="sortable" id="table3">
                                <thead>
                                    <tr> 
                                        <th><a  class="sortheader">პასუხი<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                                        <th><a  class="sortheader">რაოდენობა<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                                        <th><a  class="sortheader">დელტა<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                                        <th><a  class="sortheader">%<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                                    </tr>
                                </thead>
                                <tbody id="service_level">
                                </tbody>
                          	</table>
                        </td>
                        <td valign="top" width="50%" align="center" bgcolor="#fffdf3"></td>
                    </tr>
                </thead>
            </table>
            <div id="chart_container11" bgcolor="#fffdf3" style="float:left; width: 50%; height: 300px;"></div>
            <br>
            
            <table width="47%" cellpadding="3" cellspacing="3" border="0" style="float:left;">
                <caption style="background: #fff;">კავშირის გაწყვეტის მიზეზეი</caption>
                <thead>
                    <tr>
                        <td valign="top" width="100%" bgcolor="#fffdf3">
                            <table width="100%" cellpadding="1" cellspacing="1" border="0" class="sortable" id="table4">
                                <thead>
                                    <tr>
                                        <th><a  class="sortheader" onclick="ts_resortTable(this, 0);return false;">მიზეზი<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                                        <th><a  class="sortheader" onclick="ts_resortTable(this, 1);return false;">სულ<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                                        <th><a  class="sortheader" onclick="ts_resortTable(this, 2);return false;">სულ<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                                    </tr>
                                </thead>
                            	<tbody id="disconnection_cause">
                                    <tr>
                    					<td class="tdstyle">ოპერატორმა გათიშა:</td>
                    					<td></td>
                    					<td></td>
                    				</tr>
                    				<tr>
                    					<td class="tdstyle">აბონენტმა გათიშა:</td>
                    					<td></td>
                    					<td></td>
                    				</tr>
                            	</tbody>
                          	</table>
                        </td>
                    </tr>
                </thead>
            </table>
            <br>
            <div id="chart_container1" style="float:left; width: 50%; height: 300px;"></div>
    	</div>
    	
    	<div id="tab-2">
            <table width="99%" cellpadding="3" cellspacing="3" border="0">
                <thead>
                    <tr>
                        <td valign="top" width="50%" style="padding: 0 5px 0 0;">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            	<caption style="background: #fff;">რეპორტ ინფო</caption>
                                <tbody class="report_info">
                                    <tr>
                                        <td class="tdstyle">რიგი:</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="tdstyle">საწყისი თარიღი:</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="tdstyle">დასრულების თარიღი:</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="tdstyle">პერიოდი:</td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    	<td valign="top" width="50%">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            	<caption style="background: #fff;">უპასუხო ჩატები</caption>
                            	<tbody id="unanswer_call">
                                    <tr> 
                                        <td class="tdstyle">უპასუხო ჩატების რაოდენობა:</td>
                            	        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="tdstyle">ლოდინის საშ. დრო კავშირის გაწყვეტამდე:</td>
                                        <td></td>
                                    </tr>
                            	</tbody>
                            </table>
                    	</td>
                    </tr>
                </thead>
            </table>
    		<br>
            <table width="47%" cellpadding="3" cellspacing="3" border="0" style="float:left;">
                <caption style="background: #fff;">კავშირის გაწყვეტის მიზეზი</caption>
                <thead>
                    <tr>
                        <td valign="top" width="100%" bgcolor="#fffdf3">
                            <table width="100%" cellpadding="1" cellspacing="1" border="0">
                                <thead>
                                    <tr>
                                    	<th>მიზეზი</th>
                                    	<th>სულ</th>
                                    	<th>%</th>
                                    </tr>
                                </thead>
                                <tbody id="disconnection_cause_unanswer">
                                    <tr> 
                                        <td class="tdstyle">აბონენტმა გათიშა</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr> 
                                        <td class="tdstyle">დრო ამოიწურა</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </thead>
            </table>
    		<div id="chart_container5" style="float:left; width: 50%; height: 300px;"></div>
    		<br>
    		
     	 </div>
     	 
    	 <div id="tab-3">
            <table width="99%" cellpadding="3" cellspacing="3" border="0">
                <thead>
                    <tr>
                        <td valign="top" width="50%" style="padding: 0 5px 0 0;">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <caption style="background: #fff;">რეპორტ ინფო</caption>
                                <tbody class="report_info">
                                    <tr>
                                        <td class="tdstyle">რიგი:</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="tdstyle">საწყისი თარიღი:</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="tdstyle">დასრულების თარიღი:</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="tdstyle">პერიოდი:</td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td valign="top" width="50%">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <caption style="background: #fff;">სულ</caption>
                                <tbody id="totals">
                                    <tr> 
                                        <td class="tdstyle">ნაპასუხები ჩატების რაოდენობა:</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="tdstyle">უპასუხო ჩატების რაოდენობა:</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="tdstyle">ოპერატორი შევიდა:</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="tdstyle">ოპერატორი გავიდა:</td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </thead>
            </table>
    		<br>
            <table width="99%" cellpadding="1" cellspacing="1" border="0" class="sortable" id="table1">
            	<caption style="background: #fff;">ჩატის განაწილება დღეების მიხედვით</caption>
                <thead>
                	<tr>
                		<th><a  class="sortheader" onclick="ts_resortTable(this, 0);return false;">თარირი<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                        <th><a  class="sortheader" onclick="ts_resortTable(this, 1);return false;">ნაპასუხები<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                        <th><a  class="sortheader" onclick="ts_resortTable(this, 2);return false;">% ნაპასუხები<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                        <th><a  class="sortheader" onclick="ts_resortTable(this, 3);return false;">უპასუხო<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                        <th><a  class="sortheader" onclick="ts_resortTable(this, 4);return false;">% უპასუხო<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                        <th><a  class="sortheader" onclick="ts_resortTable(this, 5);return false;">საშ. ხანგრძლივობა<br>(სთ-წთ-წმ)<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                        <th><a  class="sortheader" onclick="ts_resortTable(this, 6);return false;">საშ. ლოდინის ხანგრძლივობა<br>(სთ-წთ-წმ)<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                    </tr>
                </thead>
            	<tbody id="call_distribution_per_day">
            		<tr class="odd">
            			<td></td>
            			<td></td>
            			<td></td>
            			<td></td>
            			<td></td>
            			<td></td>
            			<td></td>
            			<td></td>
            			<td></td>
            		</tr>
            	</tbody>
            </table>
    		<br>
    		<div id="chart_container4" style="float:left; width: 47%; height: 300px; margin-left: 20px;"></div>
    		<div id="chart_container8" style="float:right; width: 47%; height: 300px; margin-left: 20px;"></div>
    		<br>
    		<table width="99%" cellpadding="1" cellspacing="1" border="0" class="sortable" id="table2">
    			<caption style="background: #fff;">ჩატის განაწილება საათების მიხედვით</caption>
    			<thead>
    				<tr>
                        <th><a  class="sortheader" onclick="ts_resortTable(this, 0);return false;">საათი<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                        <th><a  class="sortheader" onclick="ts_resortTable(this, 1);return false;">ნაპასუხები<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                        <th><a  class="sortheader" onclick="ts_resortTable(this, 2);return false;">% ნაპასუხები<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                        <th><a  class="sortheader" onclick="ts_resortTable(this, 3);return false;">უპასუხო<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                        <th><a  class="sortheader" onclick="ts_resortTable(this, 4);return false;">% უპასუხო<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                        <th><a  class="sortheader" onclick="ts_resortTable(this, 5);return false;">საშ. ხანგრძლივობა<br>(სთ-წთ-წმ)<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                        <th><a  class="sortheader" onclick="ts_resortTable(this, 6);return false;">საშ. ლოდინის ხანგრძლივობა<br>(სთ-წთ-წმ)<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                    </tr>
    			</thead>
    			<tbody id="call_distribution_per_hour">
    				<tr class="odd">
    					<td></td>
    					<td></td>
    					<td></td>
    					<td></td>
    					<td></td>
    					<td></td>
    					<td></td>
    					<td></td>
    					<td></td>
    				</tr>
    			</tbody>
    		</table>
    		<div id="chart_container2" style="float:left; width: 47%; height: 300px; margin-left: 20px;"></div>
    		<div id="chart_container9" style="float:right; width: 47%; height: 300px; margin-left: 20px;"></div>
    		<br>
    		<table width="99%" cellpadding="1" cellspacing="1" border="0" class="sortable" id="table3">
    			<caption style="background: #fff;">ჩატის განაწილება კვირის დღეების მიხედვით</caption>
    			<thead>
    				<tr>
                        <th><a  class="sortheader" onclick="ts_resortTable(this, 0);return false;">დღე<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                        <th><a  class="sortheader" onclick="ts_resortTable(this, 1);return false;">ნაპასუხები<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                        <th><a  class="sortheader" onclick="ts_resortTable(this, 2);return false;">% ნაპასუხები<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                        <th><a  class="sortheader" onclick="ts_resortTable(this, 3);return false;">უპასუხო<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                        <th><a  class="sortheader" onclick="ts_resortTable(this, 4);return false;">% უპასუხო<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                        <th><a  class="sortheader" onclick="ts_resortTable(this, 5);return false;">საშ. ხანგრძლივობა<br>(სთ-წთ-წმ)<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                        <th><a  class="sortheader" onclick="ts_resortTable(this, 6);return false;">საშ. ლოდინის ხანგრძლივობა<br>(სთ-წთ-წმ)<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
                    </tr>
    			</thead>
    			<tbody id="call_distribution_per_day_of_week">
    				<tr class="odd">
    					<td></td>
    					<td></td>
    					<td></td>
    					<td></td>
    					<td></td>
    					<td></td>
    					<td></td>
    					<td></td>
    					<td></td>
    				</tr>
    			</tbody>
    		</table>
    		<br>
    		<div id="chart_container3" style="float:left; width: 47%; height: 300px; margin-left: 20px;"></div>
    		<div id="chart_container10" style="float:right; width: 47%; height: 300px; margin-left: 20px;"></div>
    	</div>
    </div>
    
    <!-- jQuery Dialog -->
    <div id="add-edit-form" class="form-dialog" title="ნაპასუხები ჩატები">
    	<div id="test"></div>
    </div>
    <div id="add-edit-form-unanswer" class="form-dialog" title="უპასუხო ჩატები">
    	<div id="test"></div>
    </div>
    <div id="add-edit-form-undone" class="form-dialog" title="დაუმუშავებელი ჩატები">
    	<div id="test"></div>
    </div>
    <div id="add-edit-form1" class="form-dialog" title="გამავალი ჩატი"></div>
    <div id="add-edit-form2" class="form-dialog" title="გამავალი ჩატი"></div>
    <div id="add-responsible-person" class="form-dialog" title="პასუხისმგებელი პირი"></div>
    <div id="audio_dialog" title="ჩანაწერი"></div>
    
</body>