<html>
<head>

<script type="text/javascript">
var aJaxURL	= "server-side/report/work_menager.action.php";
var dey=1;
var tbName = "tabs";
$(document).ready(function(){
	$('.date1').datepicker({dateFormat: "yy-mm-dd"});
	SetEvents("change", "", "", "example", "add-edit-form", aJaxURL);
	GetButtons("change", "");
  	$(document).on("change", ".date1", function () 	{LoadTable();	});
  	LoadTable();
  });
function LoadTable(){
	var param= new Object();
	param.act 			= "get_list1";
	param.start 	    = $('#start').val();
	param.end 			= $('#end').val();
  $.getJSON(aJaxURL, param, function(json) {
		$("#time_line").html(json.aaData);
});
}
function LoadDialog(f,buttons,wo){
	
	$('.date').datepicker({	dateFormat: "yy-mm-dd"});
	GetDialog(f,500,200,buttons);

	$("#save-dialog").click(function(){
		var param= new Object();
	        param.id            = wo;
			param.act 			= "save_dialog";
			param.graphic_time 	= $('#graphic_time').val();
			param.user 			= $('#user').val();
			param.date           = $("#date").val();
			$.getJSON(aJaxURL, param, function(data) {
                if (typeof (data.error) != "undefined") {
                    if (data.error != "") {
                        alert(data.error);
                    } else {
        				LoadTable();
        				$("#add-edit-form").dialog("close");
                    }
                }
		});
	});
};
function change_worc(wo) {
	
	var buttons = {
			"delete": {
	            text: "წაშლა",
	            id: "disable",
	            click: function () {		            
	        		var param= new Object();
	    			param.act 			= "disable";
	    			param.graphic_id    = wo;	 
	    			$.getJSON(aJaxURL, param, function(json) {
	    				LoadTable();
	    				$("#add-edit-form").dialog("close");
	    		});
	            	
	            }  
	        },
			"save": {
	            text: "შენახვა",
	            id: "save-dialog",
	            click: function () {}    
	        }, 
        	"cancel": {
	            text: "დახურვა",
	            id: "cancel-dialog",
	            click: function () {
	            	$(this).dialog("close");
	            }
	        } 
	    };
	
	var param= new Object();
	param.act="get_add_page";
	param.id= wo;
	$.getJSON(aJaxURL, param, function(json) {
		
		LoadTable();
		$("#add-edit-form").html(json.page);
		$("#user").attr('disabled', 'disabled');
		$('.date').datepicker({	dateFormat: "yy-mm-dd"});
});
	LoadDialog("add-edit-form",buttons,wo);

	
} 
</script>

<style type="text/css">

#time_line td{   
   border:solid 1px #088888;
}
.high {
    height: 0px;
}
</style>
</head>
<body>
<div id="tabs" style="width: 100%;">
   	<div class="callapp_head">სამუშაო გრაფიკები<hr class="callapp_head_hr"></div>
   	<div id="button_area" style="widows: 100%;" >
       	<table style="width: 100%;">
           	<tr style="width: 100%; height: 50px;">
               	<td style="width: 112px;">
               		<input style="width: 100px; height: 12px;" id="start" value="<?php echo date('Y-m-d');?> " class="date1 inpt" placeholder="დასაწყისი"/>
               	</td>
               	<td style="width: 50px;">
               		<label style="padding-top: 4px;">- დან</label>
               	</td>
               	<td>
               		<input style="width: 100px; height: 12px;" id='end'   value="<?php echo date('Y-m-d', strtotime("+3 day"));?>  " class="date1 inpt " placeholder="დასარული"/>
               	</td>
               	<td style="width: 50px;">
               		<label style="padding-top: 4px;">-მდე</label>
               	</td>
               	<td>
               		<button id="change" style="">დამატება</button>
               	</td>
           	</tr>
       	</table>
    </div>
	<div id="time_line" ></div>
</div>
<div  id="add-edit-form" class="form-dialog" title="თავისუფალი გრაფიკები">
</div>  
<!-- </div> -->
</body>
</html>
