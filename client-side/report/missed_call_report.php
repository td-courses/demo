<head>
<meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8">
	<style type="text/css">
		caption{
		    margin: 0;
			padding: 0;
			background: #f3f3f3;
			height: 40px;
			line-height: 40px;
			text-indent: 2px;
			font-family: "Trebuchet MS", Trebuchet, Arial, sans-serif;
			font-size: 140%;
			font-weight: bold;
			color: #000;
			text-align: left;
			letter-spacing: 1px;
			border-top: dashed 1px #c2c2c2;
			border-bottom: dashed 1px #c2c2c2;
		}
		#technik_info table td:nth-child( 3 ),
		#technik_info table td:nth-child( 4 ),
		#technik_info table td:nth-child( 5 ),
		#technik_info table td:nth-child( 6 ){
		    cursor: pointer;
            text-decoration: underline;
        }
		div, caption, td, th, h2, h3, h4 {
			font-size: 11px;
			font-family: verdana,sans-serif;
			voice-family: "\"}\"";
			voice-family: inherit;
			color: #333;
		}
		table th,table td{
    		color: #333;
            font-family: pvn;
            background: #E6F2F8;
			border: 1px solid #A3D0E4;
			vertical-align: middle;
			
			
		}
		table td{
			word-wrap: break-word;
        }
		#technik_info table td,#technik_info table th
        {
			padding: 6px;
		}
		#technik_info table
        {
			width: 100%;
			padding: 0;
		}
		table {
			padding: 10px;
            text-align: left;
            vertical-align: middle;
			margin: 0 auto;
            clear: both;
            border-collapse: collapse;
            table-layout: fixed;
            border: 1px solid #E6E6E6;
		}
		a{
			cursor: pointer;
		}
    </style>
    <script src="js/highcharts.js"></script>
     <script src="js/exporting.js"></script>
	<script type="text/javascript">
		var aJaxURL		= "server-side/report/missed_call_report.action.php";		//server side folder url
		var tName		= "example0";										//table name
		var tbName		= "tabs";											//tabs name
		var fName		= "add-edit-form";									//form name
		var file_name 	= '';
		var rand_file 	= '';
		
		$(document).ready(function () {   
			GetDateTimes("start_time");
			GetDateTimes("end_time");
			var start_date = new Date();
			start_date.setHours(0,0,0,0)
			$("#start_time").datepicker().datepicker("setDate", start_date);
			$("#end_time").datepicker().datepicker("setDate", new Date());
			$("#show_report,#technik_info_but,#report_info_but,#answer_call_info_but,#answer_call_by_queue_but").button();
			$('#tab-1').css('display','none');
			var ext_option = getQueueExtOptions('ext');
			var queue_option = getQueueExtOptions('queue');
			$("[name='List_Queue_available']").html(queue_option);
			$("[name='List_Agent_available']").html(ext_option);
		});
		 function drawFirstLevel(){
			    var options = {
			                  chart: {
			                      renderTo: 'chart_container0',
			                      plotBackgroundColor: null,
			                      plotBorderWidth: null,
			                      plotShadow: null,
			                  },
			                  colors: ['#538DD5', '#FA3A3A','#bf2109','#6f22a5'],
			                  title: {
			                      text: 'მიტოვებული ზარები'
			                  },
			                  tooltip: {
			                      formatter: function() {
			                          return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
			                      }
			                  },
			                  plotOptions: {
			                   pie: {
			                          allowPointSelect: true,
			                          cursor: 'pointer',
			                          dataLabels: {
			                              enabled: true,
			                              color: '#000000',
			                              connectorColor: '#FA3A3A',
			                              formatter: function() {
			                                  return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
			                              }
			                          },
			                          point: {
			                              events: {
			                                  click: function() {                   
			                                  }
			                              }
			                          }
			                      }
			                  },
			                  series: [{
			                      type: 'pie',
			                      name: 'კატეგორიები',
			                     // color: '#FA3A3A',
			                      data: []
			                  }]
			              }
			    var i=0;
			    
			    agent = '';
			    queuet = '';
			   
			    var optionss = $('#myform_List_Queue_to option');
			    var values = $.map(optionss ,function(option) {
			    if(queuet != ""){
			        queuet +=",";
			    }
			        queuet +="'"+option.value+"'";
			    });

			   var optionss = $('#myform_List_Agent_to option');
			   var values   = $.map(optionss ,function(option) {
			    if(agent != ''){
			        agent +=',';
			    }
			    agent+="'"+option.value+"'";
			   });
			   
			   start_time = $('#start_time').val();
			   end_time = $('#end_time').val();
                $.getJSON("server-side/report/missed_call_tab.action.php?start="+start_time + "&end=" + end_time + "&agent=" + agent + "&queuet=" + queuet, function(json) {
                    options.series[0].data = json;
                    chart = new Highcharts.Chart(options);
                });
			   }

		function go_next(val,par){
			if(val != undefined){
				$("#myform_List_"+par+"_from option:selected").remove();
				$("#myform_List_"+par+"_to").append(new Option(val, val));
			}
		}

		function go_previous(val,par){
			if(val != undefined){
				$("#myform_List_"+par+"_to option:selected").remove();
				$("#myform_List_"+par+"_from").append(new Option(val, val));
			}
		}

		function go_last(par){
			var options = $('#myform_List_'+par+'_from option');
			$("#myform_List_"+par+"_from option").remove();
			var values = $.map(options ,function(option) {
			    $("#myform_List_"+par+"_to").append(new Option(option.value, option.value));
			});
		}

		function go_first(par){
			var options = $('#myform_List_'+par+'_to option');
			$("#myform_List_"+par+"_to option").remove();
			var values = $.map(options ,function(option) {
			    $("#myform_List_"+par+"_from").append(new Option(option.value, option.value));
			});
		}

		$(document).on("click", "#show_report", function () {
			var i=0;
			parame 			= new Object();
			parame.agent	= '';
			parame.queuet = '';
			paramm		= "server-side/report/missed_call_report.action.php";
			
			var options = $('#myform_List_Queue_to option');
			var values = $.map(options ,function(option) {
				if(parame.queuet != ""){
					parame.queuet+=",";
					
				}
				parame.queuet+="'"+option.value+"'";
			});

			
			var options = $('#myform_List_Agent_to option');
			var values = $.map(options ,function(option) {
				if(parame.agent != ''){
					parame.agent+=',';
					
				}
				parame.agent+="'"+option.value+"'";
			});
			parame.act = 'check';
			parame.start_time = $('#start_time').val();
			parame.end_time   = $('#end_time').val();
			parame.change_tab = $('#change_tab').val();
			
			
			if(parame.queuet==''){
				alert('აირჩიე რიგი');
			}
			// else if(parame.agent==''){
			// 	alert('აირჩიე ოპერატორი');
			// }
			else{
                parame.act = 'tab_0';
                drawFirstLevel();
                $('#tabs').css('height','800px');
				$.ajax({
			        url: paramm,
				    data: parame,
			        success: function(data) {
						$($("#technik_info table tr")[1]).html(data.page.technik_info);
				    }
			    });
			}
        });

		$(document).on("click", "#technik_info table td:nth-child( 3 )", function () {
			LoadDialog(1);
		});
		
		$(document).on("click", "#technik_info table td:nth-child( 4 )", function () {
			LoadDialog(2);
		});

		$(document).on("click", "#technik_info table td:nth-child( 5 )", function () {
			LoadDialog(3);
		});
		$(document).on("click", "#technik_info table td:nth-child( 6 )", function () {
			LoadDialog(4);
		});
		
		var record;
 		function play(record){
 			var link = 'http://'+location.hostname + ":8000/" + record;
 			GetDialog_audio("audio_dialog", "auto", "auto","" );
 			$(".ui-dialog-buttonpane").html(" ");
 			$( ".ui-dialog-buttonpane" ).removeClass( "ui-widget-content ui-helper-clearfix ui-dialog-buttonpane" );
 			$("#audio_dialog").html('<audio controls autoplay style="width:500px; min-height: 33px;"><source src="'+link+'" type="audio/wav"> Your browser does not support the audio element.</audio>');
 		}
 		function GetDialog_audio(fname, width, height, buttons,distroy) {
 		    var defoult = {
 		        "save": {
 		            text: "შენახვა",
 		            id: "save-dialog",
 		            click: function () {
 		            }
 		        },
 		        "cancel": {
 		            text: "დახურვა",
 		            id: "cancel-dialog",
 		            click: function () {
 		                $(this).dialog("close");
 		            }
 		        }
 		    };
 		    var ok_defoult = "save-dialog";

 		    if (!empty(buttons)) {
 		        defoult = buttons;
 		    }
 		    
 		    $("#" + fname).dialog({
 		    	position: ['center',20],
 		        resizable: false,
 		        width: width,
 		        height: height,
 		        modal: false,
 		        stack: false,
 		        dialogClass: fname + "-class",
 		        buttons: defoult,
 		        close : function(event, ui) {
 		        	if(distroy!="no")$("#"+fname).html("");
 		         }
 		    });
 		}
		function LoadDialog(missed_call_case){
			parame 							= new Object();
			paramm							= "server-side/report/missed_call_report.action.php";
			parame.start_time 				= $('#start_time').val();
			parame.end_time 				= $('#end_time').val();
			parame.act 						= 'dialog';
			parame.missed_call_case 		= missed_call_case;
			parame.agent					= '';
			parame.queuet 					= '';
			
			
			var options = $('#myform_List_Queue_to option');
			var values = $.map(options ,function(option) {
				if(parame.queuet != ""){
					parame.queuet+=",";
				}
				parame.queuet+="'"+option.value+"'";
			});

			var options = $('#myform_List_Agent_to option');
			var values = $.map(options ,function(option) {
				if(parame.agent != ''){
					parame.agent+=',';
				}
				parame.agent+="'"+option.value+"'";
			});
			$.ajax({
		        url: paramm,
			    data: parame,
		        success: function(data) {
					$("#add-edit-form").html(data.page.dialog);
					var button = {
							"cancel": {
					            text: "დახურვა",
					            id: "cancel-dialog",
					            click: function () {
					                $(this).dialog("close");
					            }
					        }
						};
					GetDialog("add-edit-form", 700, "auto", button, "center top");
					/* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
					GetDataTable("example", aJaxURL, "get_list&start_time="+parame.start_time+"&end_time="+parame.end_time+"&queuet="+parame.queuet+"&agent="+parame.agent+"&missed_call_case="+missed_call_case,8, "", 0, "", 1, "desc",'',"<'dataTable_buttons'T><'F'Cfipl>");
					$(".add-edit-form-class,#add-edit-form").css("overflow","visible");
					setTimeout(function(){
					    $('.ColVis, .dataTable_buttons').css('display','none');
			    	}, 160);
			    }
		    });
		}
		
		
		
		$(document).on("click", ".show_this_phone", function() {
			//alert();
			var firstvar = $(this).text();
			NEWDIALOG('answear_un_dialog',firstvar,'new_dealog_un_table',7);
		});

		function NEWDIALOG(myact,myvar,table_id,row_count){
			parame 				= new Object();
			dialog_link		    = "server-side/report/new_dialog.action.php";
			parame.act 			= myact;
			parame.dialog_date  = myvar;
			parame.agent	    = '';
			parame.queuet       = '';
			
			
			var options = $('#myform_List_Queue_to option');
			var values = $.map(options ,function(option) {
				if(parame.queuet != ""){
					parame.queuet+=",";
					
				}
				parame.queuet+="'"+option.value+"'";
			});

			
			var options = $('#myform_List_Agent_to option');
			var values = $.map(options ,function(option) {
				if(parame.agent != ''){
					parame.agent+=',';
					
				}
				parame.agent+="'"+option.value+"'";
			});
			$.ajax({
		        url: dialog_link,
			    data: parame,
		        success: function(data) {
			        $("#new_dialog").html('');
					$("#new_dialog").html(data.page.answear_dialog);
					GetDialog("new_dialog", "700", "auto", "","no");
					/* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
					GetDataTable(table_id, dialog_link, myact+"_table&dialog_date="+parame.dialog_date+"&start_time="+$('#start_time').val()+"&end_time="+$('#end_time').val()+"&queuet="+parame.queuet+"&agent="+parame.agent, row_count, "", 0, "", 1, "desc",'',"<'dataTable_buttons'T><'F'Cfipl>");
			    }
		    });
		}

		$(document).on("click", "#show_copy_prit_exel", function () {
	        if($(this).attr('myvar') == 0){
	            $('.ColVis,.dataTable_buttons').css('display','block');
	            $(this).css('background','#2681DC');
	            $(this).children('img').attr('src','media/images/icons/select_w.png');
	            $(this).attr('myvar','1');
	        }else{
	        	$('.ColVis,.dataTable_buttons').css('display','none');
	        	$(this).css('background','#E6F2F8');
	            $(this).children('img').attr('src','media/images/icons/select.png');
	            $(this).attr('myvar','0');
	        }
	    });
		$(document).on("click", "button", function (e) {
			var tbl_id = $(this).attr('id');
			var table = tbl_id.substr(0,tbl_id.length - 4);

			var tableToExcel = (function() {
		          var uri = 'data:application/vnd.ms-excel;base64,'
		            , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
		            , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
		            , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
		          return function(table, name) {
		            if (!table.nodeType) table = document.getElementById(table)
		            var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
		            window.location.href = uri + base64(format(template, ctx))
		          }
		        })();
			tableToExcel(table, 'excel export')
		});
    </script>
    
</head>

<body>
<!-- 
    <div id="loading">
        <p><img src="media/images/loader.gif" /></p>
    </div> 
-->
<div id="tabs" style="width: 90%;">
		<div class="callapp_head">მიტოვებული ზარები<hr class="callapp_head_hr"></div>
		
		<div style="width: 27%; float:left;">
			<span>აირჩიე რიგი</span>
			<hr>
			
			    <table border="0" cellspacing="0" cellpadding="8">
					<tbody>
					<tr>
					   	<td>
							ხელმისაწვდომია<br><br>
						    <select name="List_Queue_available" multiple="multiple" id="myform_List_Queue_from" size="10" style="height: 100px;width: 125px;" >
								<?php
                                    $db->setQuery("SELECT `name` 
                                                   FROM   `queue`
                                                   WHERE   actived = 1");
                                    
                                    $res = $db->getResultArray();
                                    foreach($res[result] AS $req){
                                        $data1 .= "<option value='$req[name]'>$req[name]</option>";
                                    }
                                    echo $data1;
                                ?>
							</select>
						</td>
						<td align="left">
							<a onclick="go_next($('#myform_List_Queue_from option:selected').val(),'Queue')"><img src="media/images/go-next.png" width="16" height="16" border="0"></a>
							<a onclick="go_previous($('#myform_List_Queue_to option:selected').val(),'Queue')"><img src="media/images/go-previous.png" width="16" height="16" border="0"></a>
							<br>
							<br>
							<a  onclick="go_last('Queue')"><img src="media/images/go-last.png" width="16" height="16" border="0"></a>
							<a  onclick="go_first('Queue')"><img src="media/images/go-first.png" width="16" height="16" border="0"></a>
						</td>
						<td>
							არჩეული<br><br>
						    <select size="10" name="List_Queue[]" multiple="multiple" style="height: 100px;width: 125px;" id="myform_List_Queue_to">
								
						    </select>
					   </td>
					</tr> 
					</tbody>
				</table>
			</div>
			<div style="width: 27%; float:left; margin-left:75px;display:none;">
				<span>აირჩიე ოპერატორი</span>
				<hr>
				<table border="0" cellspacing="0" cellpadding="8" >
					<tbody><tr>
					   <td>ხელმისაწვდომია<br><br>
					    <select size="10" name="List_Agent_available" multiple="multiple" id="myform_List_Agent_from" style="height: 100px;width: 125px;">
							<option value="100">100</option>
							<option value="130">130</option>
							<option value="148">148</option>
							<option value="180">180</option>
							<option value="181">181</option>
							<option value="200">200</option>
							<option value="300">300</option>
							<option value="410">410</option>
							<option value="500">500</option>
							<option value="600">600</option>
							<option value="700">700</option>
							<option value="800">800</option>
						</select>
					</td>
					<td align="left">
							<a  onclick="go_next($('#myform_List_Agent_from option:selected').val(),'Agent')"><img src="media/images/go-next.png" width="16" height="16" border="0"></a>
							<a  onclick="go_previous($('#myform_List_Agent_to option:selected').val(),'Agent')"><img src="media/images/go-previous.png" width="16" height="16" border="0"></a>
							<br>
							<br>
							<a  onclick="go_last('Agent')"><img src="media/images/go-last.png" width="16" height="16" border="0"></a>
							<a  onclick="go_first('Agent')"><img src="media/images/go-first.png" width="16" height="16" border="0"></a>
					</td>
					<td>
						არჩეული<br><br>
					    <select size="10" name="List_Agent[]" multiple="multiple" style="height: 100px;width: 125px;" id="myform_List_Agent_to" >
					
					    </select>
					   </td>
					</tr> 
					</tbody>
				</table>
			</div>
			<div class="clear"></div>
			<div id="rest" style="width: 100%; float:none;">
				<h2>თარიღის ამორჩევა</h2>
				<hr>
				<div id="button_area">
	            	<div class="left" style="width: 200px;">
	            		<label for="search_start" class="left" style="margin: 6px 0 0 9px; font-size: 12px;">დასაწყისი</label>
	            		<input type="text" name="search_start" id="start_time" value="" class="inpt right" style="width: 110px; height: 16px;"/>
	            	</div>
	            	<div class="right" style="width: 210px;">
	            		<label for="search_end" class="left" style="margin: 6px 0 0 9px; font-size: 12px;">დასასრული</label>
	            		<input type="text" name="search_end" id="end_time" value="" class="inpt right" style="width: 110px; height: 16px;"/>
            		</div>	
            	</div>
            	
            		<input style="margin-left: 15px;" id="show_report" name="show_report" type="submit" value="რეპორტების ჩვენება">
            	
		<div class="clear"></div>
		<div id="tab-0">	
		<div class="clear"></div>
		<div><span style="float: left;">მიტოვებული ზარები</span><button style="float: right;" id="technik_info_but">Excel</button></div>
		<div class="clear"></div>
		<div id="technik_info">
                <table>                
                <tr>
                	<th></th>
                    <th>სულ</th>
                    <th>30 წამი</th>
                    <th>31 - 60 წამი</th>
                    <th>61 - 120 წამი</th>
                    <th>2 წუთზე მეტი</th>
                </tr>
                <tr>
                    <td>ზარი</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                </table>
                </div>
                <br>
                <div id="chart_container0" style="margin:auto; width: 50%; height: 300px;"></div>
                 <div id="chart_container0r" style="float:left; width: 50%; height: 300px; "></div>
		</div>
		 </div>
		  <!-- tab 0 end -->
		
<!-- jQuery Dialog -->
<div id="add-edit-form" class="form-dialog" title="მიტოვებული ზარები">
<div id="test"></div>
</div>
<!-- jQuery Dialog -->
<div id="audio_dialog" title="ჩანაწერი">
</div>
<!-- jQuery Dialog -->
<div id="new_dialog" title="დიალოგი">
</div>
</body>