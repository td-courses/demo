<head>
<meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8">
	<style type="text/css">
		caption{
		    margin: 0;
			padding: 0;
			background: #f3f3f3;
			height: 40px;
			line-height: 40px;
			text-indent: 2px;
			font-family: "Trebuchet MS", Trebuchet, Arial, sans-serif;
			font-size: 140%;
			font-weight: bold;
			color: #000;
			text-align: left;
			letter-spacing: 1px;
			border-top: dashed 1px #c2c2c2;
			border-bottom: dashed 1px #c2c2c2;
		}
		#technik_info table td:nth-child( 3 ),
		#technik_info table td:nth-child( 4 ){
		    cursor: pointer;
            text-decoration: underline;
        }
		div, caption, td, th, h2, h3, h4 {
			font-size: 11px;
			font-family: verdana,sans-serif;
			voice-family: "\"}\"";
			voice-family: inherit;
			color: #333;
		}
		table th,table td{
    		color: #333;
            font-family: pvn;
            background: #E6F2F8;
			border: 1px solid #A3D0E4;
			vertical-align: middle;
		}
		table td{
			word-wrap: break-word;
        }
		#technik_info table td,#technik_info table th
        {
			padding: 6px;
		}
		#technik_info table
        {
			width: 100%;
			padding: 0;
		}
		table {
			padding: 10px;
            text-align: left;
            vertical-align: middle;
			margin: 0 auto;
            clear: both;
            border-collapse: collapse;
            table-layout: fixed;
            border: 1px solid #E6E6E6;
		}
		a{
			cursor: pointer;
		}
    </style>
    <script src="js/highcharts.js"></script>
     <script src="js/exporting.js"></script>
	<script type="text/javascript">
		var aJaxURL			  = "server-side/report/email_report.action.php";		//server side folder url
		var tName			  = "example";												//table name
		var tbName			  = "tabs";													//tabs name
		var fName			  = "add-edit-form";										//form name
		var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
		var file_name 		  = '';
		var rand_file 	      = '';
		
		$(document).ready(function () {
			GetDate("start_time");
			GetDate("end_time");
			$("#show_report,#technik_info_but,#report_info_but,#answer_call_info_but,#answer_call_by_queue_but").button();
			$('#tab-1').css('display','none');
			LoadTable();
		});
		function LoadTable(){
			start_time = $('#start_time').val();
			end_time   = $('#end_time').val();
			/* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
			GetDataTable(tName, aJaxURL, "get_list", 9, "start_time="+start_time + "&end_time=" + end_time, 0, "", 1, "desc", "", change_colum_main);
			setTimeout(function(){
    	    	$('.ColVis, .dataTable_buttons').css('display','none');
  	    	}, 90);
		}
		 function drawFirstLevel(){
			    var options = {
			                  chart: {
			                      renderTo: 'chart_container0',
			                      plotBackgroundColor: null,
			                      plotBorderWidth: null,
			                      plotShadow: null,
			                  },
			                  
			                  title: {
			                      text: 'Mail კომუნიკაცია'
			                  },
			                  tooltip: {
			                      formatter: function() {
			                          return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
			                      }
			                  },
			                  plotOptions: {
			                   pie: {
			                          allowPointSelect: true,
			                          cursor: 'pointer',
			                          dataLabels: {
			                              enabled: true,
			                              color: '#000000',
			                              connectorColor: '#FA3A3A',
			                              formatter: function() {
			                                  return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
			                              }
			                          },
			                          point: {
			                              events: {
			                                  click: function() {
			                                  }
			                              }
			                          }
			                      }
			                  },
			                  series: [{
			                      type: 'pie',
			                      name: 'კატეგორიები',
			                     // color: '#FA3A3A',
			                      data: []
			                  }]
			              }
			    var i=0;
			    
			    agent = '';
			    queuet = '';
			   
			    var optionss = $('#myform_List_Queue_to option');
			    var values = $.map(optionss ,function(option) {
			    if(queuet != ""){
			        queuet +=",";
			    }
			        queuet +="'"+option.value+"'";
			    });

			   var optionss = $('#myform_List_Agent_to option');
			   var values   = $.map(optionss ,function(option) {
			    if(agent != ''){
			        agent +=',';
			    }
			    agent+="'"+option.value+"'";
			   });
			   
			   start_time = $('#start_time').val();
			   end_time = $('#end_time').val();
                $.getJSON("server-side/report/email_report_tab.action.php?start="+start_time + "&end=" + end_time + "&agent=" + agent + "&queuet=" + queuet, function(json) {
                    options.series[0].data = json;
                    chart = new Highcharts.Chart(options);
                });
			   }

		function go_next(val,par){
			if(val != undefined){
				$("#myform_List_"+par+"_from option:selected").remove();
				$("#myform_List_"+par+"_to").append(new Option(val, val));
			}
		}

		function go_previous(val,par){
			if(val != undefined){
				$("#myform_List_"+par+"_to option:selected").remove();
				$("#myform_List_"+par+"_from").append(new Option(val, val));
			}
		}

		function go_last(par){
			var options = $('#myform_List_'+par+'_from option');
			$("#myform_List_"+par+"_from option").remove();
			var values = $.map(options ,function(option) {
			    $("#myform_List_"+par+"_to").append(new Option(option.value, option.value));
			});
		}

		function go_first(par){
			var options = $('#myform_List_'+par+'_to option');
			$("#myform_List_"+par+"_to option").remove();
			var values = $.map(options ,function(option) {
			    $("#myform_List_"+par+"_from").append(new Option(option.value, option.value));
			});
		}

		$(document).on("click", "#show_report", function () {
			var i=0;
			parame 			= new Object();
			parame.agent	= '';
			parame.queuet = '';
			paramm		= "server-side/report/email_report.action.php";
			
			var options = $('#myform_List_Queue_to option');
			var values = $.map(options ,function(option) {
				if(parame.queuet != ""){
					parame.queuet+=",";
					
				}
				parame.queuet+="'"+option.value+"'";
			});

			
			var options = $('#myform_List_Agent_to option');
			var values = $.map(options ,function(option) {
				if(parame.agent != ''){
					parame.agent+=',';
					
				}
				parame.agent+="'"+option.value+"'";
			});
			parame.act = 'check';
			parame.start_time = $('#start_time').val();
			parame.end_time   = $('#end_time').val();
			parame.change_tab = $('#change_tab').val();
			
			
			// if(parame.queuet==''){
			// 	alert('აირჩიე რიგი');
			// }
			// else if(parame.agent==''){
			// 	alert('აირჩიე შიდა ნომერი');
			// }
			// else{
                // parame.act = 'tab_0';
                // drawFirstLevel();
                // $('#tabs').css('height','800px');
				// $.ajax({
			    //     url: paramm,
				//     data: parame,
			    //     success: function(data) {
				// 		$($("#technik_info table tr")[1]).html(data.page.technik_info);
				//     }
			    // });
			// }
			LoadTable();
        });

		$(document).on("click", "#technik_info table td:nth-child( 3 )", function () {
			LoadDialog();
		});
		
		var record;
 		function play(record){
 			var link = 'http://192.168.3.32:8080/records/' + record;
 			GetDialog_audio("audio_dialog", "auto", "auto","" );
 			$(".ui-dialog-buttonpane").html(" ");
 			$( ".ui-dialog-buttonpane" ).removeClass( "ui-widget-content ui-helper-clearfix ui-dialog-buttonpane" );
 			$("#audio_dialog").html('<audio controls autoplay style="width:500px; min-height: 33px;"><source src="'+link+'" type="audio/wav"> Your browser does not support the audio element.</audio>');
 		}
 		function GetDialog_audio(fname, width, height, buttons,distroy) {
 		    var defoult = {
 		        "save": {
 		            text: "შენახვა",
 		            id: "save-dialog",
 		            click: function () {
 		            }
 		        },
 		        "cancel": {
 		            text: "დახურვა",
 		            id: "cancel-dialog",
 		            click: function () {
 		                $(this).dialog("close");
 		            }
 		        }
 		    };
 		    var ok_defoult = "save-dialog";

 		    if (!empty(buttons)) {
 		        defoult = buttons;
 		    }
 		    
 		    $("#" + fname).dialog({
 		    	position: ['center',20],
 		        resizable: false,
 		        width: width,
 		        height: height,
 		        modal: false,
 		        stack: false,
 		        dialogClass: fname + "-class",
 		        buttons: defoult,
 		        close : function(event, ui) {
 		        	if(distroy!="no")$("#"+fname).html("");
 		         }
 		    });
 		}
		function LoadDialog(){
			parame 				= new Object();
			paramm				= "server-side/report/email_report.action.php";
			parame.start_time 	= $('#start_time').val();
			parame.end_time 	= $('#end_time').val();
			parame.act 			= 'answear_dialog';
			parame.agent		= '';
			parame.queuet 		= '';
			
			
			var options = $('#myform_List_Queue_to option');
			var values = $.map(options ,function(option) {
				if(parame.queuet != ""){
					parame.queuet+=",";
					
				}
				parame.queuet+="'"+option.value+"'";
			});

			
			var options = $('#myform_List_Agent_to option');
			var values = $.map(options ,function(option) {
				if(parame.agent != ''){
					parame.agent+=',';
					
				}
				parame.agent+="'"+option.value+"'";
			});
			$.ajax({
		        url: paramm,
			    data: parame,
		        success: function(data) {		        	
					$("#add-edit-form").html(data.page.answear_dialog);
					var button = {
							"cancel": {
					            text: "დახურვა",
					            id: "cancel-dialog",
					            click: function () {
					                $(this).dialog("close");
					            }
					        }
						};
					GetDialog("add-edit-form", 700, "auto", button, "no");
					/* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
					GetDataTable("example", aJaxURL, "answear_dialog_table&start_time="+parame.start_time+"&end_time="+parame.end_time+"&queuet="+parame.queuet+"&agent="+parame.agent,8, "", 0, "", 1, "desc",'',"<'dataTable_buttons'T><'F'Cfipl>");
					$(".add-edit-form-class,#add-edit-form").css("overflow","visible");
					setTimeout(function(){
					    $('.ColVis, .dataTable_buttons').css('display','none');
			    	}, 160);
			    }
		    });
		}

		
		$(document).on("click", ".show_this_phone", function() {
			//alert();
			var firstvar = $(this).text();
			NEWDIALOG('answear_un_dialog',firstvar,'new_dealog_un_table',7);
		});

		function NEWDIALOG(myact,myvar,table_id,row_count){
			parame 				= new Object();
			dialog_link		    = "server-side/report/email_report.action.php";
			parame.act 			= myact;
			parame.dialog_date  = myvar;
			parame.agent	    = '';
			parame.queuet       = '';
			
			
			var options = $('#myform_List_Queue_to option');
			var values = $.map(options ,function(option) {
				if(parame.queuet != ""){
					parame.queuet+=",";
					
				}
				parame.queuet+="'"+option.value+"'";
			});

			
			var options = $('#myform_List_Agent_to option');
			var values = $.map(options ,function(option) {
				if(parame.agent != ''){
					parame.agent+=',';
					
				}
				parame.agent+="'"+option.value+"'";
			});
			$.ajax({
		        url: dialog_link,
			    data: parame,
		        success: function(data) {
			        $("#new_dialog").html('');
					$("#new_dialog").html(data.page.answear_dialog);
					GetDialog("new_dialog", "700", "auto", "","no");
					/* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
					GetDataTable(table_id, dialog_link, myact+"_table&dialog_date="+parame.dialog_date+"&start_time="+$('#start_time').val()+"&end_time="+$('#end_time').val()+"&queuet="+parame.queuet+"&agent="+parame.agent, row_count, "", 0, "", 1, "desc",'',"<'dataTable_buttons'T><'F'Cfipl>");
			    }
		    });
		}

		$(document).on("click", "#show_copy_prit_exel", function () {
	        if($(this).attr('myvar') == 0){
	            $('.ColVis,.dataTable_buttons').css('display','block');
	            $(this).css('background','#2681DC');
	            $(this).children('img').attr('src','media/images/icons/select_w.png');
	            $(this).attr('myvar','1');
	        }else{
	        	$('.ColVis,.dataTable_buttons').css('display','none');
	        	$(this).css('background','#E6F2F8');
	            $(this).children('img').attr('src','media/images/icons/select.png');
	            $(this).attr('myvar','0');
	        }
	    });
		$(document).on("click", "button", function (e) {
			var tbl_id = $(this).attr('id');
			var table = tbl_id.substr(0,tbl_id.length - 4);

			var tableToExcel = (function() {
		          var uri = 'data:application/vnd.ms-excel;base64,'
		            , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
		            , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
		            , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
		          return function(table, name) {
		            if (!table.nodeType) table = document.getElementById(table)
		            var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
		            window.location.href = uri + base64(format(template, ctx))
		          }
		        })();
			tableToExcel(table, 'excel export')
		});
    </script>
    
</head>

<body>
<!-- 
    <div id="loading">
        <p><img src="media/images/loader.gif" /></p>
    </div> 
-->
<div id="tabs" style="width: 90%;">
		<div class="callapp_head">Mail კომუნიკაცია<hr class="callapp_head_hr"></div>
		<div id="button_area">
        	<div class="left" style="width: 200px;">
        		<label for="search_start" class="left" style="margin: 6px 0 0 9px; font-size: 12px;">დასაწყისი</label>
        		<input type="text" name="search_start" id="start_time" value="" class="inpt right" style="width: 110px; height: 16px;"/>
        	</div>
        	<div class="right" style="width: 210px;">
        		<label for="search_end" class="left" style="margin: 6px 0 0 9px; font-size: 12px;">დასასრული</label>
        		<input type="text" name="search_end" id="end_time" value="" class="inpt right" style="width: 110px; height: 16px;"/>
    		</div>	
    	</div>
        <input style="margin-left: 15px;" id="show_report" name="show_report" type="submit" value="რეპორტების ჩვენება">
        
		<table id="table_right_menu">
			<tr>
				<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;background:#2681DC;" id="show_table" myvar="0"><img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
				</td>
				<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_log" myvar="0"><img alt="log" src="media/images/icons/log.png" height="14" width="14">
				</td>
				<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_copy_prit_exel" myvar="0"><img alt="link" src="media/images/icons/select.png" height="14" width="14">
				</td>
			</tr>
		</table>
		<table class="display" id="example">
			<thead>
				<tr id="datatable_header">
					<th>ID</th>
					<th style="width: 1%;">№</th>
					<th style="width: 15%;">თარიღი</th>
					<th style="width: 15%;">ოპერატორი</th>
					<th style="width: 15%;">ადრესატი</th>
					<th style="width: 15%;">cc</th>
					<th style="width: 15%;">bcc</th>
					<th style="width: 15%;">გზავნილი</th>
					<th style="width: 15%;">სტატუსი</th>
				</tr>
			</thead>
			<thead>
				<tr class="search_header">
					<th class="colum_hidden">
						<input type="text" name="search_category" value="ფილტრი" class="search_init" />
					</th>
					<th>
						<input type="text" name="search_category" value="ფილტრი" class="search_init" />
					</th>
					<th>
						<input type="text" name="search_category" value="ფილტრი" class="search_init" />
					</th>
					<th>
						<input type="text" name="search_category" value="ფილტრი" class="search_init" />
					</th>
					<th>
						<input type="text" name="search_category" value="ფილტრი" class="search_init" />
					</th>
					<th>
						<input type="text" name="search_category" value="ფილტრი" class="search_init" />
					</th>
                    <th>
						<input type="text" name="search_category" value="ფილტრი" class="search_init" />
					</th>
                    <th>
						<input type="text" name="search_category" value="ფილტრი" class="search_init" />
					</th>
					<th>
						<input type="text" name="search_category" value="ფილტრი" class="search_init" />
					</th>
				</tr>
			</thead>
		</table>
		<br>
		<div id="chart_container0" style="margin:auto; width: 50%; height: 300px;"></div>
		<div id="chart_container0r" style="float:left; width: 50%; height: 300px; "></div>
		</div>
		 </div>
		  <!-- tab 0 end -->
		
<!-- jQuery Dialog -->
<div id="add-edit-form" class="form-dialog" title="ნაპასუხები ზარები">
<div id="test"></div>
</div>

<!-- jQuery Dialog -->
<div id="add-edit-form-unanswer" class="form-dialog" title="უპასუხო ზარები">
<div id="test"></div>
</div>

<!-- jQuery Dialog -->
<div id="add-edit-form-undone" class="form-dialog" title="დაუმუშავებელი ზარები">
<div id="test"></div>
</div>

<!-- jQuery Dialog -->
<div id="add-edit-form1" class="form-dialog" title="გამავალი ზარი">
<!-- aJax -->
</div>

<!-- jQuery Dialog -->
<div id="add-edit-form2" class="form-dialog" title="გამავალი ზარი">
<!-- aJax -->
</div>

<div id="add-responsible-person" class="form-dialog" title="პასუხისმგებელი პირი">
<!-- aJax -->
</div>
<!-- jQuery Dialog -->
<div id="audio_dialog" title="ჩანაწერი">
</div>
<!-- jQuery Dialog -->
<div id="new_dialog" title="დიალოგი">
</div>
</body>