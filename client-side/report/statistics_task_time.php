<html>
<head>
<style type="text/css">
.green{
	background-color: rgba(255, 0, 29, 0.51) !important;
	color: #0D233A !important;
	font-size: 14px !important;
		
}

.download1 {
    background-color:#F99B03;
	border-radius:0px;
	cursor:pointer;
	color:#ffffff;
	width: 100%;
}

.download {
    background-color:#599bb3;
	border-radius:0px;
	cursor:pointer;
	color:#ffffff;
}
#add-edit-form, .idle{'disabled', true};
</style>
<script src="js/highcharts.js"></script>
<script src="js/exporting.js"></script>
<script type="text/javascript">
var title 	= '0';
var i 		= 0;
var done 	= ['','','','','','',''];
var aJaxURL	= "server-side/report/statistics_task_time.action.php";		//server side folder url
var tName   = "report";
var start	= $("#search_start").val();
var end		= $("#search_end").val();

$(document).ready(function() {
	GetDateTimes("search_start");
	GetDateTimes("search_end");
	$("#fillter").button();
	drawFirstLevel();
	$("#back").button({ disabled: true });
	$("#back").button({ icons: { primary: "ui-icon-arrowthick-1-w" }});
    $('#back').click(function(){
	    i--;
	    drawFirstLevel();
    	if(i==0)$("#back").button({ disabled: true });
 	});  
});

$(document).on("click", "#fillter", function () 	{
	drawFirstLevel();
});

function drawFirstLevel(){
	 if(i==1){name_chart='სთ(საშუალო დრო)';}else{name_chart='დავალება';}
		 var options = {
	                chart: {
	                    renderTo: 'chart_container',
	                    plotBackgroundColor: null,
	                    plotBorderWidth: null,
	                    plotShadow: false
	                },
	                title: {
	                    text: title
	                },
	                tooltip: {
	                    formatter: function() {
		                   
	                        return '<b>'+ this.point.name +': '+this.point.y+' '+ name_chart+':  '+this.percentage.toFixed(2) +' %</b>';

	                    }
	                },
	                plotOptions: {
	                	pie: {
	                        allowPointSelect: true,
	                        cursor: 'pointer',
	                        dataLabels: {
	                            enabled: true,
	                            color: '#000000',
	                            connectorColor: '#000000',
	                            formatter: function() {
	                            	return '<b>'+ this.point.name +': '+this.point.y+' '+name_chart+':  '+this.percentage.toFixed(2) +' %</b>';
	                            }
	                        },
	                        point: {
	                            events: {
	                                click: function() {
	                                	$("#back").button({ disabled: false });
		                        		done[i]=this.name;
		                        		if(i==1) i=0;
		                        		else i++;
		                        		drawFirstLevel();
	                                }
	                            }
	                        }
	                    }
	                },
	                series: [{
	                    type: 'pie',
	                    name: 'კატეგორიები',
	                    data: []
	                }]
	            }
		var start	= $("#search_start").val();
		var end		= $("#search_end").val();
		var d_url   ="&start="+start+"&end="+end+"&done="+i+"&departament="+done[0]+"&type="+done[1]+"&category="+done[2]+"&sub_category="+done[3];
		var url     = aJaxURL+"?act=get_category"+d_url;
		
		GetDataTable(tName, aJaxURL, "get_list", 4, d_url, 0, "",'','',[2], "<'dataTable_buttons'T><'F'Cfipl>");
		setTimeout(function(){$('.ColVis, .dataTable_buttons').css('display','none');}, 90);
		
		$("#report tbody").on("click", "tr", function () {
			 if(i==1){
				d_url1   ="&start="+start+"&end="+end+"&done="+i+"&type="+done[0]+"&category="+done[1]+"&sub_category="+done[2];
			 	var nTds = $("td", this);
		        var rID = $(nTds[1]).text();
	    	    GetDataTable("report_1", aJaxURL, "get_in_page", 13, d_url1+"&rid="+rID, 0,"",'','','',"<'dataTable_buttons'T><'F'Cfipl>");
	    	    setTimeout(function(){$('.ColVis, .dataTable_buttons').css('display','none');}, 90);
	    	    GetDialog("in_form", 1282, "auto","","top");
	     		SetEvents("", "", "", "report_1", "add-edit-form", "server-side/report/tasks.action.php");
	     		$('.ui-dialog-buttonset').hide();
     		}
     	});
        $.getJSON(url, function(json) {
            options.series[0].data = json.data;
            options.title['text']=json.text;
            chart = new Highcharts.Chart(options);
            $("#total_quantity").html("იტვირთება....")
            setTimeout(function(){ $("#total_quantity").html($("#qnt").html().split(">")[1]);}, 500);
            if(i==1){ 
                $("#report td").addClass("green");
            }else{
                $(".green").removeClass("green");
            }
		});
}
$(document).on("click", ".download1", function () {
	var str = 1;
	var link = ($(this).attr("str"));
	link = 'http://91.233.15.136:8181/' + link;
	var btn = {
	        "cancel": {
	            text: "დახურვა",
	            id: "cancel-dialog",
	            click: function () {
	                $(this).dialog("close");
	            }
	        }
	    };
	GetDialog_audio("audio_dialog", "auto", "auto",btn );
	$("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="'+link+'" type="audio/wav"> Your browser does not support the audio element.</audio>');
    $(".download1").css( "background","#F99B03" );
    $(this).css( "background","#33dd33" );
   
});
$(document).on("click", ".download", function () {
	var str = 1;
	var link = ($(this).attr("str"));
	link = 'http://91.233.15.136:8181/' + link;
	var btn = {
	        "cancel": {
	            text: "დახურვა",
	            id: "cancel-dialog",
	            click: function () {
	                $(this).dialog("close");
	            }
	        }
	    };
	GetDialog_audio("audio_dialog", "auto", "auto",btn );
	$("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="'+link+'" type="audio/wav"> Your browser does not support the audio element.</audio>');
    $(".download").css( "background","#408c99" );
    $(this).css( "background","#FF5555" );
   
});
function LoadDialog(fname){

	GetDialog(fname, "1060px", "auto");
	$('#add-edit-form, .idle').attr('disabled', true);
	$('.ui-dialog-buttonset').hide();
};

$(document).on("click", "#show_copy_prit_exel", function () {
    if($(this).attr('myvar') == 0){
        $('.ColVis,.dataTable_buttons').css('display','block');
        $(this).css('background','#2681DC');
        $(this).children('img').attr('src','media/images/icons/select_w.png');
        $(this).attr('myvar','1');
    }else{
    	$('.ColVis,.dataTable_buttons').css('display','none');
    	$(this).css('background','#E6F2F8');
        $(this).children('img').attr('src','media/images/icons/select.png');
        $(this).attr('myvar','0');
    }
});
</script>
</head>
<body>
	<div style="margin-top: 12px;" class="callapp_head">დავალებები შესრულების დროების მიხედვით<hr class="callapp_head_hr"></div>
  	<div id="dt_example" class="ex_highlight_row">
       	 <div id="container" style="width:90%">
            <div id="dynamic">
                <div id="button_area" style="margin: 3% 0 0 0">
                 	<button id="back" style="margin-top:0px">უკან</button>
        		</div>
               	<div id="button_area" style="margin: 3% 0 0 0">
                 	<div class="left" style="width: 175px;">
                   		<input style="height: 15px;" type="text" name="search_start" id="search_start" value="<?php echo date('d/m/y')." 00:00";?>" class="inpt right"/>
                    </div>
                    <label for="search_start" class="left" style="margin:5px 0 0 3px">-დან</label>
                    <div class="left" style="width: 185px;">
        	        	<input style="height: 15px;" type="text" name="search_end" id="search_end" value="<?php echo  date('d/m/y')." 23:59"; ?>" class="inpt right" />
                    </div>
                   	<label for="search_end" class="left" style="margin:5px 0 0 3px">–მდე</label>
                   	<button class="left" id="fillter" style="margin-left: 10px;">ფილტრი</button>
                   	<label class="left" style="margin:5px 0 0 40px">ზარების  ჯამური რაოდენობა: </label> <label id="total_quantity" class="left" style="margin:5px 0 0 2px; font-weight: bold;">0</label>
               		<br /><br /><br />
                </div>
        		<div id="chart_container" style="width: 100%; height: 480px; margin-top:-30px;"></div>
        		<input type="text" id="hidden_name" value="" style="display: none;" />
        		<br><br><br><br><br><br><br><br><br><br>
        		<table id="table_right_menu" style="top: 24px;">
                    <tr>
                    	<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;background:#2681DC;" id="show_table" myvar="0">
                    		<img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
                    	</td>
                    	<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_log" myvar="0">
                    		<img alt="log" src="media/images/icons/log.png" height="14" width="14">
                    	</td>
                    	<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_copy_prit_exel" myvar="0">
                    		<img alt="link" src="media/images/icons/select.png" height="14" width="14">
                    	</td>
                    </tr>
                 </table>
        		 <table class="display" id="report">
                    <thead>
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width:100%">დასახელება</th>
                            <th class="min">რაოდენობა</th>
                            <th class="min">პროცენტი</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                            	<input type="text" name="search_id" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init">
                            </th>
                            <th>
                            	<input type="text" name="search_object" value="ფილტრი" class="search_init">
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                            </th>
        				</tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th id="qnt">&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>
             	<div class="spacer">
        		</div>
    		</div>
		</div>
	</div>
    <div id="audio_dialog" class="form-dialog" title="ჩანაწერი"></div>
    <div id="in_form"  class="form-dialog">
	<table class="display" id="report_1" >
         <thead>
            <tr id="datatable_header">
                <th>ID</th>
                <th style="width: 5%;" >№</th>
                <th style="width:  11%;">თარიღი</th>
                <th style="width:  7%">pin-კოდი</th>
                <th style="width:  9%">პ/ნ</th>
                <th style="width:  9%">ტელეფონი</th>
                <th style="width:  9%">წყარო</th>
                <th style="width:  13%">ქვე კატეგორ.</th>
                <th style="width:  12%">პასუხისმგებელი პ.</th>
                <th style="width:  13%">ოპერატორი</th>
                <th style="width:  15%">სტატუსი</th>
                <th style="width:  5%">ს.დრო</th>
                <th style="width:  8%">ქმედება</th>
            </tr>
        </thead>
        <thead>
            <tr class="search_header">
                <th class="colum_hidden">
                	<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
                </th>
                <th>
                	<input type="text" name="search_number" value="ფილტრი" class="search_init hidden-input" style=""></th>
                <th>
                    <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width:110px;"/>
                </th>
                <th>
                    <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 50px;"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80px;" />
                </th>
                <th>
                    <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 75px;"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width:120px;" />
                </th>
                 <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 120px;" />
                </th>
                 <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 105px;" />
                </th>
                 <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width:150px;" />
                </th>
                 <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width:150px;" />
                </th>
                 <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 40px;" />
                </th>
                 <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 55px;" />
                </th>
            </tr>
        </thead>
   </table>
</div>
<div  id="add-edit-form" class="form-dialog" title="შემომავალი ზარი">	</div>
</body>
</html>

