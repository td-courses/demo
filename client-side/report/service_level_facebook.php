<html>
<head>
<script src="js/highcharts.js"></script>
<!--script src="http://code.highcharts.com/stock/highstock.js"></script-->
<script src="js/exporting.js"></script>
<script type="text/javascript">

var aJaxURL	= "server-side/report/service_level_facebook.action.php";		//server side folder url
var tName   = "report";

$(document).ready(function() {
	$("#show_report").button();
	$("#users").chosen();
	$("#users_chosen").css('border-radius', '0px');
	
	GetDate("search_start");
	GetDate("search_end");
	
	var start	= $("#search_start").val();
	var end		= $("#search_end").val();
	var number	= $("#number").val();
	var procent	= $("#procent").val();
	var users   = $("#users").val();
	getchart(start,end,number,procent,users);
});

$(document).on("click", "#show_report", function () {
	var start	= $("#search_start").val();
	var end		= $("#search_end").val();
	var number	= $("#number").val();
	var procent	= $("#procent").val();
	var users   = $("#users").val();
	
	getchart(start,end,number,procent,users);
});

function getchart(start,end,number,procent,users) {
	var options = {
			chart: {
	            renderTo: 'chart_container',
	            zoomType: 'xy'
	        },
        title: {
            text: 'SL დღეების მიხედვით'
        },
        subtitle: {
            text: ''
        },
        xAxis: [{
            categories: [],
            crosshair: true,
            labels: {
                rotation: -45,
                align: 'right',
                
            }
        }],
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value}%',
                style: {
                    color: Highcharts.getOptions().colors[2]
                }
            },
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[2]
                }
            },
            opposite: true

        }, { // Secondary yAxis
            gridLineWidth: 0,
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            labels: {
                format: '{value} ჩატი',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            }

        }, { // Tertiary yAxis
            gridLineWidth: 0,
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            labels: {
                format: '{value} ჩატი',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            x: 80,
            verticalAlign: 'top',
            y: 55,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        series: []
    }


    $.getJSON("server-side/report/service_level_facebook.action.php?done=1&start="+ start + "&end=" + end + "&number=" + number + "&procent=" + procent + "&users=" + users, function(json) {
    	options.xAxis[0].categories = json[0]['date'];
    	options.tooltip.valueSuffix = json[0]['unit'];

    	$("#all_answer").val(json[0]['all_answer_input']);
    	if(json[0]['all_procent_input']==null){percent=0;}else{percent=json[0]['all_procent_input'];}	
    	$("#all_procent").val(percent+'%');

    	options.series[3]       = {};
    	options.series[3].color = "green";
    	options.series[3].name  = json[0]['name_answer'];
    	options.series[3].data  = json[0]['percent_answer'];
    	options.series[3].type  = "spline";

    	options.series[2]         = {};
    	options.series[2].color   = "red";
    	options.series[2].name    = 'უპასუხო ჩატი';
    	options.series[2].data    = json[0]['count_unanswer'];
    	options.series[2].type    = "column";
    	options.series[2].yAxis   = 1;
    	options.series[2].tooltip = {valueSuffix: ' ჩატი'};

    	options.series[0]         = {};
    	options.series[0].color   = "#9155B4";
    	options.series[0].name    = 'სულ ჩატი';
    	options.series[0].data    = json[0]['count_all'];
    	options.series[0].type    = "column";
    	options.series[0].yAxis   = 1;
    	options.series[0].tooltip = {valueSuffix: ' ჩატი'};
    	
    	options.series[4]       = {};
    	options.series[4].color = "blue";
    	options.series[4].name  = 'SL-გეგმიური';
    	options.series[4].data  = json[0]['limit_percent'];    	
    	options.series[4].type  = "spline";
    	
    	options.series[1]         = {};
    	options.series[1].color   = "#7CB5EC";
    	options.series[1].name    = 'ნაპასუხები ჩატი';
    	options.series[1].data    = json[0]['count_answer'];
    	options.series[1].type    = "column";
    	options.series[1].yAxis   = 1;
    	options.series[1].tooltip = {valueSuffix: ' ჩატი'};
    	
        chart = new Highcharts.Chart(options);
        $(".highcharts-axis-labels").on("click", "text", function() {
        	getoherchart($(this).text(),number,procent,users);
        	getoherchart1($(this).text(),number,procent,users); 
        });
    });
}

function getoherchart(date,number,procent,users){

	var options = {
			chart: {
	            renderTo: 'chart_container1',
	            zoomType: 'xy'
	        },
        title: {
            text: 'SL საათების მიხედვით ' + date
        },
        subtitle: {
            text: ''
        },
        xAxis: [{
            categories: [],
            crosshair: true,
            labels: {
                rotation: -45,
                align: 'right',
                
            }
        }],
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value}%',
                style: {
                    color: Highcharts.getOptions().colors[2]
                }
            },
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[2]
                }
            },
            opposite: true

        }, { // Secondary yAxis
            gridLineWidth: 0,
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            labels: {
                format: '{value} ჩატი',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            }

        }, { // Tertiary yAxis
            gridLineWidth: 0,
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            labels: {
                format: '{value} ჩატი',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            x: 80,
            verticalAlign: 'top',
            y: 55,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        series: []
    }

	    $.getJSON("server-side/report/service_level_facebook.action.php?done=2&day=" + date + "&number=" + number + "&procent=" +procent + "&users=" + users, function(json) {
	    	options.xAxis[0].categories = json[0]['date'];
	    	options.tooltip.valueSuffix = json[0]['unit'];

	    	options.series[0]         = {};
	    	options.series[0].color   = "#9155B4";
	    	options.series[0].name    = 'ყველა ჩატი';
	    	options.series[0].data    = json[0]['count_all'];
	    	options.series[0].type    = "column";
	    	options.series[0].yAxis   = 1;
	    	options.series[0].tooltip = {valueSuffix: ' ჩატი'};
	    	
	    	options.series[1]         = {};
	    	options.series[1].color   = "#7CB5EC";
	    	options.series[1].name    = 'ნაპასუხები ჩატი';
	    	options.series[1].data    = json[0]['count_answer'];
	    	options.series[1].type    = "column";
	    	options.series[1].yAxis   = 1;
	    	options.series[0].tooltip = {valueSuffix: ' ჩატი'};

	    	options.series[2]         = {};
	    	options.series[2].color   = "red";
	    	options.series[2].name    = 'უპასუხო ჩატი';
	    	options.series[2].data    = json[0]['count_unanswer'];
	    	options.series[2].type    = "column";
	    	options.series[2].yAxis   = 1;
	    	options.series[2].tooltip = {valueSuffix: ' ჩატი'};
	    	
	    	options.series[3]       = {};
	    	options.series[3].color = "green";
	    	options.series[3].name  = json[0]['name_answer'];
	    	options.series[3].data  = json[0]['percent_answer'];
	    	options.series[3].type  = "spline";

	    	options.series[4]       = {};
	    	options.series[4].color = "blue";
	    	options.series[4].name  = 'SL-გეგმიური';
	    	options.series[4].data  = json[0]['limit_percent'];
	    	options.series[4].type  = "spline";
	        chart = new Highcharts.Chart(options);
	    });
}

function getoherchart1(date,number,procent,users){
	var options = {
			chart: {
	            renderTo: 'chart_container2',
	            zoomType: 'xy'
	        },
        title: {
            text: 'SL საათების მიხედვით ' + date
        },
        subtitle: {
            text: ''
        },
        xAxis: [{
            categories: [],
            crosshair: true,
            labels: {
                rotation: -45,
                align: 'right',
                
            }
        }],
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value}%',
                style: {
                    color: Highcharts.getOptions().colors[2]
                }
            },
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[2]
                }
            },
            opposite: true

        }, { // Secondary yAxis
            gridLineWidth: 0,
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            labels: {
                format: '{value} ჩატი',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            }

        }, { // Tertiary yAxis
            gridLineWidth: 0,
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            labels: {
                format: '{value} ჩატი',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            x: 80,
            verticalAlign: 'top',
            y: 55,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        series: []
    }

	    $.getJSON("server-side/report/service_level_facebook.action.php?done=3&day=" + date + "&number=" + number + "&procent=" +procent + "&users=" + users, function(json) {
	    	options.xAxis[0].categories = json[0]['date'];
	    	options.tooltip.valueSuffix = json[0]['unit'];

	    	options.series[0]         = {};
	    	options.series[0].color   = "#9155B4";
	    	options.series[0].name    = 'ყველა ჩატი';
	    	options.series[0].data    = json[0]['count_all'];
	    	options.series[0].type    = "column";
	    	options.series[0].yAxis   = 1;
	    	options.series[0].tooltip = {valueSuffix: ' ჩატი'};
	    	
	    	options.series[1]         = {};
	    	options.series[1].color   = "#7CB5EC";
	    	options.series[1].name    = 'ნაპასუხები ჩატი';
	    	options.series[1].data    = json[0]['count_answer'];
	    	options.series[1].type    = "column";
	    	options.series[1].yAxis   = 1;
	    	options.series[1].tooltip = {valueSuffix: ' ჩატი'};

	    	options.series[2]         = {};
	    	options.series[2].color   = "red";
	    	options.series[2].name    = 'უპასუხო ჩატი';
	    	options.series[2].data    = json[0]['count_unanswer'];
	    	options.series[2].type    = "column";
	    	options.series[2].yAxis   = 1;
	    	options.series[2].tooltip = {valueSuffix: ' ჩატი'};
	    	
	    	options.series[3]       = {};
	    	options.series[3].color = "green";
	    	options.series[3].name  = json[0]['name_unanswer'];
	    	options.series[3].data  = json[0]['percent_unanswer'];
	    	options.series[3].type  = "spline";

	    	options.series[4]       = {};
	    	options.series[4].color = "blue";
	    	options.series[4].name  = 'SL-გეგმიური';
	    	options.series[4].data  = json[0]['limit_percent'];
	    	options.series[4].type  = "spline";
	        chart = new Highcharts.Chart(options);
	    });
}

</script>
<style type="text/css">

.highcharts-button{
	float: left;
}
.highcharts-axis-labels{
	cursor: pointer;
}
.high {
    height: 0px; 
}
</style>
</head>
<body>
<div id="tabs">
<div class="callapp_head">სერვის ლეველი FACEBOOK<hr class="callapp_head_hr"></div>
  <div style="width: 99%;margin: auto;">
      <table style="width: 100%; height: 40px;">
          <tr>
              <td style="width: 78px;">
              	<input id="search_start" type="text" style="width: 75px;float: left;">
              </td>
              <td style="width: 35px;">
              	<label style="padding-top: 3px;">- დან</label>
              </td>
              <td style="width: 78px;">
               	<input id="search_end" type="text" style="width: 75px;margin-left: 10px;float: left;">
              </td>
              <td style="width: 45px;">
              	<label style="padding-top: 3px;">- მდე</label>
              </td>
              <td style="width: 155px;">
                  <select id="users" style="width: 150px;" class="idls object">   	 
                   	 <option value="all">ყველა</option>
                   	 <option value="0">ორშაბათი</option>
                   	 <option value="1">სამშაბათი</option>
                   	 <option value="2">ოთხშაბათი</option>
                   	 <option value="3">ხუთშაბათი</option>
                   	 <option value="4">პარასკევი</option>
                   	 <option value="5">შაბათი</option>
                   	 <option value="6">კვირა</option> 
               	 </select>
              </td>
              <td style="width: 50px;">
              	<label style="padding-top: 5px;">ჩატების</label>
              </td>
              <td style="width: 29px;">
              	<input id="procent" type="text" value="85" style="text-align: center; width: 25px;">
              </td>
              <td style="width: 145px;">
              	<label style="padding-top: 5px;">- % ნაპასუხები უნდა იყოს </label>
              </td>
              <td style="width: 25px;">
              	<input id="number" type="text" value="45" style="text-align: center; width: 20px;">
              </td>
              <td style="width: 50px;">
              	<label style="padding-top: 5px;">- წმ-ში</label>
              </td>
              <td>
              	<button id="show_report">რეპორტის ჩვენება</button>
              </td>
          </tr>
      </table>
      <table style="width: 100%;">
          <tr style="height: 30px;">
              <td style="width: 95px;">
              	<label style="padding-top: 4px;">სულ ნაპასუხები:</label>
              </td>
              <td style="width: 80px;">
              	<input type="text" id="all_answer" style="text-align:center; width: 50px;" disabled>
              </td>
          	  <td style="width: 90px;">
              	<label style="padding-top: 4px;">სულ პროცენტი:</label>
              </td>
              <td>
              	<input type="text" id="all_procent" style="float: left; width: 50px; text-align: center;" disabled>
              </td>
          </tr>
      </table>      	 
   	  <div id="chart_container" style="height: 400px; min-width: 310px; margin-top:10px;"></div><br>
   	  <div id="chart_container1" style="height: 400px; min-width: 310px; margin-top:10px;"></div>
   	  <div id="chart_container2" style="height: 400px; min-width: 310px; margin-top:10px;"></div>
  </div>
</body>
</html>
