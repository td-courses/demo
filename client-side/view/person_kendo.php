<html>
<head>
<?
	$mysqli = new dbClass();
	$user_id = $_SESSION['USERID'];
	
	$query = "
	
	SELECT kendo_templates.url AS url,kendo_templates.name AS tname, kendo_templates.id AS kid
	FROM users
	JOIN kendo_templates ON users.kendo_template = kendo_templates.id
	WHERE users.id = $user_id
	
	";
	
	$mysqli->setQuery($query);
	$result = $mysqli->getResultArray();
	echo '
	<link rel="stylesheet" href="'.$result['result'][0]['url'].'" />
	';
?>
<script src="https://kendo.cdn.telerik.com/2020.1.114/js/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="js/kendo.all.min.js"></script>
<script type="text/javascript" language="javascript" src="js/kendo.culture.ru-RU.min.js"></script>
<style type="text/css">
.high {
    height: 0px;
}
</style>
	<script type="text/javascript">
	    var aJaxURL	= "server-side/view/person_kendo.action.php";		//server side folder url

    </script>
</head>

<body>
    <div id="tabs">
		<div class="callapp_head">პერსონები დავალება<hr class="callapp_head_hr"></div>
		

		<div id="cap-view" style="margin-bottom:25px;" class="demo-section k-content">

            <h4 style="margin-top: 2em; "><label for="theme" style="font-size:14px;">აირჩიეთ დიზაინი</label></h4>
            <select id="theme" style="width: 200px;" >
				<?
				
					$mysqli->setQuery("SELECT id,name FROM kendo_templates");
					$themeList = $mysqli->getResultArray();
					
					foreach($themeList['result'] AS $item)
					{
						if($result['result'][0]['kid'] == $item['id'])
						{
							echo '<option value="'.$item['id'].'" selected="selected">'.$item['name'].'</option>';
						}
						else
						{
							echo '<option value="'.$item['id'].'">'.$item['name'].'</option>';
						}
						
					}
				
				?>
            </select>

        </div>
	<div id="example">	
		<div id="grid"></div>
		<script>
			kendo.pdf.defineFont({
				"DejaVu Sans"             : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans.ttf",
				"DejaVu Sans|Bold"        : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Bold.ttf",
				"DejaVu Sans|Bold|Italic" : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Oblique.ttf",
				"DejaVu Sans|Italic"      : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Oblique.ttf",
				"WebComponentsIcons"      : "https://kendo.cdn.telerik.com/2017.1.223/styles/fonts/glyphs/WebComponentsIcons.ttf"
			});
		</script>
		<script src="https://kendo.cdn.telerik.com/2020.1.114/js/pako_deflate.min.js"></script>
		<script type="x/kendo-template" id="page-template">
			<div class="page-template">
				<div class="header">
					<div style="float: right">Page #: pageNum # of #: totalPages #</div>
				</div>
				<div class="watermark">KSOVRELA</div>
				<div class="footer">
					Page #: pageNum # of #: totalPages #
				</div>
			</div>
		</script>
		<script>

			$(document).ready(function () {
				var crudServiceBaseUrl = aJaxURL,
				dataSource = new kendo.data.DataSource({
					transport: {
						read:  {
							url: crudServiceBaseUrl + "?act=get_list",
							dataType: "json"
						},
						update: {
							url: crudServiceBaseUrl + "?act=save_priority",
							dataType: "json"
						},
						destroy: {
							url: crudServiceBaseUrl + "?act=disable",
							dataType: "json"
						},
						create: {
							url: crudServiceBaseUrl + "/Products/Create",
							dataType: "json"
						},
						
						parameterMap: function(data, options, operation) {
							if (operation !== "read" && options.models) {
								return {models: kendo.stringify(options.models)};
							}
							else {
								return {add: kendo.stringify(data)};
							}
						}
					},
					batch: true,
					pageSize: 6,
					aggregate: [ { field: "Age", aggregate: "average" },{ field: "personSurname", aggregate: "sum" },{ field: "Age", aggregate: "sum" }],
					schema: {
						model: {
							id: "personID",
							fields: {
								personID: { editable: false, nullable: true },
								personName: { validation: { required: true }, type: "string" },
								regDate: { editable: false, type: "date"},
								personSurname: { validation: { required: true },type: "string" },
								Age: { type: "number",validation: { required: true, min: 10} },
								sexID: { type: "string" }
							}
						},
						total: 'total',
						data: function (data) {
							return data.data;
						}
					},
					serverFiltering: true,
					serverPaging: true
				});
				$("#grid").kendoGrid({
					dataSource: dataSource,
					dataBound: function() {
					  var pageInfo = this.pager.element.find(".k-pager-info");
					  setTimeout(function() {
						$.ajax({
								url: aJaxURL,
								data: 'act=get_total',
								success: function(data) {       
									pageInfo.append(' სულ: '+data.totales);
								}
						});
						
					  });
					},
					pdf: {
						allPages: true,
						avoidLinks: true,
						paperSize: "A4",
						margin: { top: "2cm", left: "1cm", right: "1cm", bottom: "1cm" },
						landscape: true,
						repeatHeaders: true,
						template: $("#page-template").html(),
						scale: 0.8
					},
					
					selectable: "single",
					persistSelection: true,
					height: 500,
					groupable: false,
					sortable: true,
					filterable: {
						mode: "row",
						operators: {
							date: {
								startswith: "Starts with",
								eq: "Is equal to",
								neq: "Is not equal to"
							}
						}
					},
					pageable: true,
					toolbar: ["sadasd","create","pdf","excel"],
					columns: [{
						field: "regDate",
						title: "თარიღი",
						width: 240,
						format: "{0:yyyy-MM-dd}",
						filterable: {
                            cell: { "template": betweenFilter }

                        }
					}, {
						field: "personName",
						title: "სახელი",
						filterable: {
							cell: {
								showOperators: false
							}
						}
					}, {
						field: "personSurname",
						title: "გვარი",
						aggregates: ["sum"],
						footerTemplate: "sum: #=sum#",
						filterable: {
							cell: {
								showOperators: false
							}
						}
					}, {
						field: "Age",
						title: "ასაკი",
						width: 150,
						aggregates: ["average","sum"],
						footerTemplate: "average: #=average# sum: #=sum#",
						filterable: {
							cell: {
								showOperators: false
							}
						}
					}, {
						field: "sexID",values:[

						{ text: "მამრობითი", value: 1 },
						{ text: "მდედრობითი", value: 2 }

						],
						title: "სქესი",
						filterable: {
							cell: {
								showOperators: false
							}
						}
					},
					{ command: ["edit", "destroy"], title: "&nbsp;", width: "250px" }],
					editable: "inline"
				});
                $(".k-grid-sadasd").click(function(e){
                    alert(1);
                    // handler body
                });
			});

			$(document).ready(function() {

				// create DropDownList from select HTML element
				$("#theme").kendoDropDownList();
				var size = $("#theme").data("kendoDropDownList");

				$(document).on("change", "#theme", function () {
					param = new Object();
					param.act = "change_theme";
					param.theme_id = $('#theme').val();
					$.ajax({
						url: aJaxURL,
						data: param,
						success: function (data) {
							location.reload();
							
						}
					});
				});
				
			});	
			function betweenFilter(args) {
                alert('hello');
					var filterCell = args.element.parents(".k-filtercell");

					filterCell.empty();
					filterCell.html('<span style="display:flex; justify-content:center;"><span>From:</span><input  class="start-date"/><span>To:</span><input  class="end-date"/></span>');

					$(".start-date", filterCell).kendoDatePicker({
						change: function (e) {
							var startDate = e.sender.value(),
								endDate = $("input.end-date", filterCell).data("kendoDatePicker").value(),
								dataSource = $("#grid").data("kendoGrid").dataSource;

							if (startDate & endDate) {
								var filter = { logic: "and", filters: [] };
								filter.filters.push({ field: "reg_date", operator: "gte", value: startDate });
								filter.filters.push({ field: "reg_date", operator: "lte", value: endDate });
								dataSource.filter(filter);
							}
						}
					});
					$(".end-date", filterCell).kendoDatePicker({
						change: function (e) {
							var startDate = $("input.start-date", filterCell).data("kendoDatePicker").value(),
								endDate = e.sender.value(),
								dataSource = $("#grid").data("kendoGrid").dataSource;

							if (startDate & endDate) {
								var filter = { logic: "and", filters: [] };
								filter.filters.push({ field: "reg_date", operator: "gte", value: startDate });
								filter.filters.push({ field: "reg_date", operator: "lte", value: endDate });
								dataSource.filter(filter);
							}
						}
					});

				}


		</script>
	</div>
		<style>
		.page-template {
          font-family: "DejaVu Sans", "Arial", sans-serif;
          position: absolute;
          width: 100%;
          height: 100%;
          top: 0;
          left: 0;
        }
        .page-template .header {
          position: absolute;
          top: 30px;
          left: 30px;
          right: 30px;
          border-bottom: 1px solid #888;
          color: #888;
        }
        .page-template .footer {
          position: absolute;
          bottom: 30px;
          left: 30px;
          right: 30px;
          border-top: 1px solid #888;
          text-align: center;
          color: #888;
        }
        .page-template .watermark {
          font-weight: bold;
          font-size: 400%;
          text-align: center;
          margin-top: 30%;
          color: #aaaaaa;
          opacity: 0.1;
          transform: rotate(-35deg) scale(1.7, 1.5);
        }
		.k-pdf-export .k-filterable .k-grid-filter{
			display:none;
		  }
		</style>
    
	</div>
</body>
</html>