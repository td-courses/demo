<html>
<head>
<script type="text/javascript">
	var aJaxURL	= "server-side/view/task_setting.action.php";		//server side folder url

	    	
	$(document).ready(function () {        	
		loadContent();
		$("#saveSetting").button();
	});
	
    function loadContent(){
    	$.ajax({
	        url: aJaxURL,
		    data: "act=get_setting",
	        success: function(data) {
	        	if(typeof(data.error) != 'undefined'){
	        		if(data.error != ''){
						alert(data.error);
					}else{
						$('#time').val(data.time);
						$('#freq').val(data.freq);
					}
		    	}
	        }
	    });
    }

    function saveSetting(){
    	time = $('#time').val();
		freq = $('#freq').val();
		
    	$.ajax({
	        url: aJaxURL,
		    data: "act=save_setting&time="+time+"&freq=" + freq,
	        success: function(data) {
	        	if(typeof(data.error) != 'undefined'){
	        		if(data.error != ''){
						alert(data.error);
					}else{
						loadContent();
						alert('მონაცემები განახლებულია!');
					}
		    	}
	        }
	    });
    }
</script>
</head>
<body>

    <div id="tabs" style="width: 100%;">
    	<div class="callapp_head">პარამეტრები<hr class="callapp_head_hr"></div>
    <div id="dialog-form">
	    <fieldset>
	    	<legend>ძირითადი პარამეტრები</legend>
	    	<table class="dialog-form-table">
				<tr>
					<td style="width: 250px;"><label for="time" title="მიუთითეთ წუთი!">დავალებების განახლების დრო(წუთი)</label></td>
					<td>
						<input style="width: 50px;" type="number" min="0" max="99" id="time" class="idle" value="0" title="მიუთითეთ წუთი!" />
					</td>
				</tr>
				<tr>
					<td style="width: 250px;"><label for="freq" title="მიუთითეთ 1000-დან 10000-მდე!">აუდიო ჩანაწერის მაქსიმალური სიხშირე</label></td>
					<td>
						<input style="width: 50px;" type="number" min="1000" max="10000" id="freq" class="idle" value="2000" title="მიუთითეთ 1000-დან 10000-მდე!" />
					</td>
				</tr>
			</table>
			<button id="saveSetting" onclick="saveSetting()">განახლება</button>
        </fieldset>
    </div>

</body>
</html>