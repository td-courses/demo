<html>
<head>
	<script type="text/javascript">
		var aJaxURL	= "server-side/view/signature.action.php";		//server side folder url
		var tName	= "example";													//table name
		var fName	= "add-edit-form";												//form name
		var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
		var img_name		= "0.jpg";
		$(document).ready(function () {        	
			LoadTable();	

			/* Add Button ID, Delete Button ID */
			GetButtons("add_button", "delete_button");			
			SetEvents("add_button", "delete_button", "check-all", tName, fName, aJaxURL);
		});
        
		function LoadTable(){

			/* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
			GetDataTable(tName, aJaxURL, "get_list", 5, "", 0, "", 1, "desc", "", change_colum_main);
			setTimeout(function(){
    	    	$('.ColVis, .dataTable_buttons').css('display','none');
  	    	}, 90);
		}

	    function LoadTableLog(){
			
			/* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
			GetDataTable(tName+'_log', aJaxURL, "get_list_log", 8, "", 0, "", 1, "desc", "", change_colum_main);
			setTimeout(function(){
    	    	$('.ColVis, .dataTable_buttons').css('display','none');
  	    	}, 90);
		}
		
		$(document).on('dialogclose',"#add-edit-form", function(event) {
			tinymce.remove("#signature");
		});
		
		function LoadDialog(){
			var buttons = {

				"save": {
					text: "შენახვა",
					id: "save-dialog"
				}, 
				
				"cancel": {
					text: "დახურვა",
					id: "cancel-dialog",
					click: function () {
						tinymce.remove("#action_content");
						$(this).dialog("close");
					}
				}
			};
			GetButtons("choose_button");
			GetButtons("choose_buttondisabled");
			var id		= $("#lang_id").val();
			
			/* Dialog Form Selector Name, Buttons Array */
			GetDialog(fName, 600, "auto", "");
			tinymce.init({
					selector:'#signature',
					plugins : "paste",
					paste_auto_cleanup_on_paste : true
				});
			$("#mail_id").chosen();
		}
		
	    // Add - Save
	    $(document).on("click", "#save-dialog", function () {

			var ed                    = tinyMCE.get('signature').getContent();
			var ed                    = ed.replace(/[&]nbsp[;]/gi," ");
		    param 			          = new Object();
			param.act		          = "save_info_sorce";
		    param.id		          = $("#info_sorce_id").val();
	    	param.signature_increment = $("#signature_increment").val();
	    	param.name		          = $("#name").val();
	    	param.signature	          = $("#signature").val();
	    	param.mail_id	          = $("#mail_id").val();
	    	param.signature	          = ed;
	    	
			if(param.name == ""){
				alert("შეავსეთ ველი!");
			}else {
			    $.ajax({
			        url: aJaxURL,
				    data: param,
			        success: function(data) {			        
						if(typeof(data.error) != 'undefined'){
							if(data.error != ''){
								alert(data.error);
							}else{
								LoadTable();
				        		CloseDialog(fName);
							}
						}
				    }
			    });
			}
		});
		
	    $(document).on("click", "#show_copy_prit_exel", function () {
	        if($(this).attr('myvar') == 0){
	            $('.ColVis,.dataTable_buttons').css('display','block');
	            $(this).css('background','#2681DC');
	            $(this).children('img').attr('src','media/images/icons/select_w.png');
	            $(this).attr('myvar','1');
	        }else{
	        	$('.ColVis,.dataTable_buttons').css('display','none');
	        	$(this).css('background','#E6F2F8');
	            $(this).children('img').attr('src','media/images/icons/select.png');
	            $(this).attr('myvar','0');
	        }
	    });

	    $(document).on("click", "#show_log", function () {
	    	LoadTableLog();
	    	$("#example_wrapper,#example").css('display','none');
	    	$("#example_log_wrapper").css('display','block');
	    	$("#example_log").css('display','table');
	    	$('#add_button,#delete_button').attr('disabled','disabled');
	    	
	    	$(this).css('background','#2681DC');
            $(this).children('img').attr('src','media/images/icons/log_w.png');
    	
    		$("#show_table").css('background','#E6F2F8');
            $("#show_table").children('img').attr('src','media/images/icons/table.png');
	    	
	    });

	    $(document).on("click", "#show_table", function () {
	    	LoadTable();
	    	$("#example_log_wrapper,#example_log").css('display','none');
	    	$("#example_wrapper").css('display','block');
	    	$("#example").css('display','table');
	    	$('#add_button,#delete_button').removeAttr('disabled');
	    	
	    	$("#show_log").css('background','#E6F2F8');
            $("#show_log").children('img').attr('src','media/images/icons/log.png');

            $(this).css('background','#2681DC');
            $(this).children('img').attr('src','media/images/icons/table_w.png');
		});
		$(document).on("click", "#choose_button", function () {
		    $("#choose_file").click();
		});

	    $(document).on("click", "#choose_buttondisabled", function () {
		    alert('თუ გსურთ ახალი სურათის ატვირთვა, წაშალეთ მიმდინარე სურათი!');
		});

	    
	    $(document).on("change", "#choose_file", function () {
	        var file_url  = $(this).val();
	        var file_name = this.files[0].name;
	        var file_size = this.files[0].size;
	        var file_type = file_url.split('.').pop().toLowerCase();
	        var path	  = "../../media/uploads/file/";

	        if($.inArray(file_type, ['png','jpg']) == -1){
	            alert("დაშვებულია მხოლოდ 'png', 'jpg'  გაფართოება");
	        }else if(file_size > '15728639'){
	            alert("ფაილის ზომა 15MB-ზე მეტია");
	        }else{
		        signature_id = $("#signature_increment").val();
	        	$.ajaxFileUpload({
			        url: "server-side/upload/file.action.php",
			        secureuri: false,
	     			fileElementId: "choose_file",
	     			dataType: 'json',
				    data: {
						act: "file_upload",
						button_id: "choose_file",
						table_name: 'signature',
						file_name: Math.ceil(Math.random()*99999999999),
						file_name_original: file_name,
						file_type: file_type,
						file_size: file_size,
						path: path,
						table_id: signature_id,

					},
			        success: function(data) {			        
				        if(typeof(data.error) != 'undefined'){
							if(data.error != ''){
								alert(data.error);
							}else{
								$("#upload_img").attr('src','media/uploads/file/'+data.page[0].rand_name);
								$('#choose_button').attr('id','choose_buttondisabled');
								$("#delete_image").attr('image_id',data.page[0].id);
								$(".complate").attr('onclick','view_image('+ data.page[0].id + ')');
							}						
						}					
				    }
			    });
	        }
	    });
		function view_image(id){
			param = new Object();

	        //Action
	    	param.act	= "view_img";
	    	param.id    = id;
	    	
			$.ajax({
		        url: aJaxURL,
			    data: param,
		        success: function(data) {
					if(typeof(data.error) != "undefined"){
						if(data.error != ""){
							alert(data.error);
						}else{
							var buttons = {
						        	"cancel": {
							            text: "დახურვა",
							            id: "cancel-dialog",
							            click: function () {
							            	$(this).dialog("close");
							            }
							        }
							    };
							GetDialog("add-edit-form-img", 401, "auto", buttons, 'center top');
							$("#add-edit-form-img").html(data.page);
						}
					}
			    }
		    });
		}
		$(document).on("click", "#delete_image", function () {
		    $.ajax({
	            url: "server-side/upload/file.action.php",
	            data: "act=delete_file&file_id="+$(this).attr('image_id')+"&table_name=client",
	            success: function(data) {
	               $('#upload_img').attr('src','media/uploads/file/0.jpg');               
	               $("#choose_button").button();
	               $('#choose_buttondisabled').attr('id','choose_button')
	            }
	        });
		});
    </script>
    <style type="text/css">
        #table_right_menu{
            top: 42px;
        }
		#signature_dialog_form_main_table input,#signature_dialog_form_main_table textarea{
			width:340px!important;
		}
    </style>
</head>

<body>
<div id="tabs">
<div class="callapp_head">signature<hr class="callapp_head_hr"></div>
<div id="button_area">
	<button id="add_button">დამატება</button>
	<button id="delete_button">წაშლა</button>
</div>
<table id="table_right_menu">
<tr>
<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;background:#2681DC;" id="show_table" myvar="0"><img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
</td>
<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_log" myvar="0"><img alt="log" src="media/images/icons/log.png" height="14" width="14">
</td>
<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_copy_prit_exel" myvar="0"><img alt="link" src="media/images/icons/select.png" height="14" width="14">
</td>
</tr>
</table>
    <table class="display" id="example">
        <thead>
            <tr id="datatable_header">
                <th>ID</th>
                <th style="width: 5%;">№</th>
                <th style="width: 20%;">სახელი</th>
                <th style="width: 20%;">მეილი</th>
                <th style="width: 55%;">signature</th>
            	<th class="check">#</th>
            </tr>
        </thead>
        <thead>
            <tr class="search_header">
                <th class="colum_hidden">
                    <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                </th>
                <th>
                	<div class="callapp_checkbox">
                        <input type="checkbox" id="check-all" name="check-all" />
                        <label for="check-all"></label>
                    </div>
                </th>
            </tr>
        </thead>
    </table>

    <table class="display" id="example_log" style="display: none;">
        <thead>
            <tr id="datatable_header">
                <th>ID</th>
                <th style="width: 7%;">№</th>
                <th style="width: 15%;">ქმედების თარიღი</th>
                <th style="width: 15%;">მომხმარებელი</th>
                <th style="width: 15%;">ქმედება</th>
                <th style="width: 15%;">ველი</th>
                <th style="width: 15%;">ძველი მნიშვნელობა</th>
                <th style="width: 20%;">ახალი მნიშვნელობა</th>
            </tr>
        </thead>
        <thead>
            <tr class="search_header">
                <th class="colum_hidden">
                    <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                </th>
            </tr>
        </thead>
    </table>
	<!-- jQuery Dialog -->
	<div id="image-form" class="form-dialog" title="თანამშრომლის სურათი">
    	<img id="view_img" src="media/uploads/images/worker/0.jpg">
	</div>
    <!-- jQuery Dialog -->
    <div id="add-edit-form" class="form-dialog" title="signature">
    	<!-- aJax -->
	</div>
	<div id="add-edit-form-img" class="form-dialog" title="თანამშრომლის სურათი">
	</div>
</body>
</html>


