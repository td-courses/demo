<html>
<head>
<style type="text/css">
.high {
    height: 0px;
}
.ui-widget-content {
	border: 0px solid #aaaaaa;
}
</style>
	<script type="text/javascript">
	    var aJaxURL	= "server-side/view/chat_work.action.php";	
	    var aJaxURL1= "server-side/view/chat_free_work.action.php";	
		var tName	= "example";										
		var fName	= "add-edit-form";
		var tbName  = "tabs1";												
		var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
		 	
		$(document).ready(function () {
			GetTabs(tbName);
			LoadTable('example');	
		});
        
		function LoadTable(tName){
			if(tName == 'example'){
				GetDataTable(tName, aJaxURL, "get_list_free_day", 3, "", 0, "", 1, "desc", "", change_colum_main);
				GetButtons("add_button", "delete_button");
				SetEvents("add_button", "", "", tName, fName, aJaxURL);
    			setTimeout(function(){$('.ColVis, .dataTable_buttons').css('display','none');}, 90);
			}else if(tName == 'example1'){
				GetDataTable(tName, aJaxURL, "get_list_week_day", 4, "", 0, "", 1, "desc", "", change_colum_main);
				GetButtons("add_button1", "delete_button1");
				SetEvents("add_button1", "", "", tName, "add-edit-form1", aJaxURL1);
    			setTimeout(function(){$('.ColVis, .dataTable_buttons').css('display','none');}, 90);
			}
		}

		function LoadDialog(fName){
			if(fName == "add-edit-form"){
    			GetDialog(fName, 278, "auto", "", "center top");
    			$("#week_day_id").chosen();
    			$('#start,#end').timepicker({});
    			$("#add-edit-form, .add-edit-form-class").css('overflow','visible');
			}else if(fName == "add-edit-form1"){
				var buttons = {
					"save": {
			            text: "შენახვა",
			            id: "save-free_work"
			        }, 
		        	"cancel": {
			            text: "დახურვა",
			            id: "cancel-dialog",
			            click: function () {
			            	$(this).dialog("close");
			            }
			        } 
			    };
				GetDialog(fName, 345, "auto", buttons, "center top");
				$("#holidayCategory, #holidaYear, #holidayMonth, #holidayDate").chosen();
				$("#add-edit-form1, .add-edit-form1-class").css('overflow','visible');
			}
		}
		$(document).on("click", "#week_work_day", function() {
			LoadTable('example');
        });
        
		$(document).on("click", "#free_day", function() {
			LoadTable('example1');
        });

		$(document).on("change", "#holidayCreeping", function() {

	      // define check status and selec value
	      let checkStatus = !$(this).is(":checked");
	      let value = !checkStatus ? $("#holidaYear").val() : 0;

	      // chenge select disabled property
	      $("#holidaYear").val(value).prop("disabled", checkStatus).trigger("chosen:updated");

	    });
    
        $(document).on("click", "#delete_button", function() {
	    	
	        var data = $(".check:checked").map(function () {
	            return this.value;
	        }).get();
	    	

	        for (var i = 0; i < data.length; i++) {
	            $.ajax({
	                url: aJaxURL,
	                type: "POST",
	                data: "act=disable&id=" + data[i] + "&source=1",
	                dataType: "json",
	                success: function (data) {
	                    if (data.error != ""){
	                        alert(data.error);
	                    }else{
							LoadTable('example');
	                    	$("#check-all").attr("checked", false);
	                    }
	                }
	            });
	        }
		});

		$(document).on("click", "#delete_button1", function() {
	    	
	        var data = $(".check:checked").map(function () {
	            return this.value;
	        }).get();
	    	

	        for (var i = 0; i < data.length; i++) {
	            $.ajax({
	                url: aJaxURL,
	                type: "POST",
	                data: "act=disable&id=" + data[i] + "&source=2",
	                dataType: "json",
	                success: function (data) {
		                    if (data.error != "") {
		                        alert(data.error);
		                    } else {

		                    	LoadTable('example1');
		                    	$("#check-all1").attr("checked", false);
		                    }
	                }
	            });
	        }
		});

		$(document).on("click", "#save-free_work", function() {
			param 			      = new Object();
			param.act		      = "save_free_work";
		    param.free_work_id    = $("#free_work_id").val();
	    	param.holidayCategory = $("#holidayCategory").val();
	    	param.connect		  = $('input[id=holidayCreeping]:checked').val();
	    	param.holidaYear	  = $("#holidaYear").val();
	    	param.holidayMonth	  = $("#holidayMonth").val();
	    	param.holidayDate	  = $("#holidayDate").val();
	    	param.holidayName	  = $("#holidayName").val();
	    	
			if(param.holidayCategory == 0){
				alert("შეავსეთ კატეგორია!");
			}else if(param.holidayMonth == 0){
				alert("შეავსეთ თვე!");
			}else if(param.holidayDate == 0){
				alert("შეავსეთ დღე!");
			}else if(param.holidayName == 0){
				alert("შეავსეთ დასახელება!");
			}else{
			    $.ajax({
			        url: aJaxURL1,
				    data: param,
			        success: function(data) {			        
						if(typeof(data.error) != 'undefined'){
							if(data.error != ''){
								alert(data.error);
							}else{
								LoadTable('example1');
				        		$("#add-edit-form1").dialog('close');
							}
						}
				    }
			    });
			}
		});

		$(document).on("click", "#save-dialog", function() {
			param 			  = new Object();
			param.act		  = "save_work";
			param.id	      = $("#hidde_id").val();
		    param.week_day_id = $("#week_day_id").val();
	    	param.start       = $("#start").val();
	    	param.end		  = $('#end').val();
	    	
	    	if(param.week_day_id == 0){
				alert("შეავსეთ კვირის დღე!");
			}else if(param.start == ""){
				alert("შეავსეთ დასაწყისი!");
			}else if(param.start == ""){
				alert("შეავსეთ დასასრული!");
			}else{
			    $.ajax({
			        url: aJaxURL,
				    data: param,
			        success: function(data) {			        
						if(typeof(data.error) != 'undefined'){
							if(data.error != ''){
								alert(data.error);
							}else{
								LoadTable('example');
				        		$("#add-edit-form").dialog('close');
							}
						}
				    }
			    });
			}
		});
		$(document).on("click", "#show_copy_prit_exel", function () {
	        if($(this).attr('myvar') == 0){
	            $('.ColVis,.dataTable_buttons').css('display','block');
	            $(this).css('background','#2681DC');
	            $(this).children('img').attr('src','media/images/icons/select_w.png');
	            $(this).attr('myvar','1');
	        }else{
	        	$('.ColVis,.dataTable_buttons').css('display','none');
	        	$(this).css('background','#E6F2F8');
	            $(this).children('img').attr('src','media/images/icons/select.png');
	            $(this).attr('myvar','0');
	        }
	    });
	</script>
</head>

<body>
    <div id="tabs">
		<div class="callapp_head">სამუშაო საათები<hr class="callapp_head_hr"></div>
		<table id="table_right_menu" style="top: 90px; left: -27px;">
        	<tr>
        		<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;background:#2681DC;" id="show_table" myvar="0">
        			<img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
        		</td>
        		<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_log" myvar="0">
        			<img alt="log" src="media/images/icons/log.png" height="14" width="14">
        		</td>
        		<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_copy_prit_exel" myvar="0">
        			<img alt="link" src="media/images/icons/select.png" height="14" width="14">
        		</td>
        	</tr>
        </table>
		<div id="tabs1" style="width: 99%;">
        	<ul>
        		<li id="week_work_day"><a href="#tab-0">კვირის სამუშაო საათები</a></li>
        		<li id="free_day"><a href="#tab-1">დასვენების დღეები</a></li>
        	</ul>
        	<div id="tab-0">
            	<div id="button_area">
            	    <button id="add_button">დამატება</button>
        			<button id="delete_button">წაშლა</button>
        		</div>
                <table class="display" id="example">
                    <thead >
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width: 50%;">დღე</th>
                            <th style="width: 50%;">შუალედი(საათი)</th>
                            <th class="check">#</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                             	<input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
        					<th>
                            	<div class="callapp_checkbox">
                                    <input type="checkbox" id="check-all" name="check-all" />
                                    <label for="check-all"></label>
                                </div>
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div id="tab-1">
            	<div id="button_area">
        			<button id="add_button1">დამატება</button>
        			<button id="delete_button1">წაშლა</button>
        		</div>
                <table class="display" id="example1">
                    <thead >
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width: 30%;">თარიღი</th>
                            <th style="width: 40%;">დასახელება</th>
                            <th style="width: 30%;">კატეგორია</th>
        					<th class="check">#</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                             	<input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
        					<th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                          	<th>
                            	<div class="callapp_checkbox">
                                    <input type="checkbox" id="check-all" name="check-all" />
                                    <label for="check-all"></label>
                                </div>
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
   </div>
       
    <!-- jQuery Dialog -->
    <div id="add-edit-form" class="form-dialog" title="სამუშაო საათები">
    	<!-- aJax -->
	</div>
	
	<!-- jQuery Dialog -->
    <div id="add-edit-form1" class="form-dialog" title="დასვენების დღეები">
    	<!-- aJax -->
	</div>
</body>
</html>