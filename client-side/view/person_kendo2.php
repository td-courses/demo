<html>
<head>
<?
	$mysqli = new dbClass();
	$user_id = $_SESSION['USERID'];
	
	$query = "
	
	SELECT kendo_templates.url AS url,kendo_templates.name AS tname, kendo_templates.id AS kid
	FROM users
	JOIN kendo_templates ON users.kendo_template = kendo_templates.id
	WHERE users.id = $user_id
	
	";
	
	$mysqli->setQuery($query);
	$result = $mysqli->getResultArray();
	echo '
	<link rel="stylesheet" href="'.$result['result'][0]['url'].'" />
	';
?>
<script src="https://kendo.cdn.telerik.com/2020.1.114/js/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="js/kendo.all.min.js"></script>
<!--<script type="text/javascript" language="javascript" src="js/kendo.main.js"></script>-->
<script type="text/javascript" language="javascript" src="js/kendo.main.class.js"></script>
<style type="text/css">
.high {
    height: 0px;
}
</style>
</head>

<body>
    <div id="tabs">
		<div class="callapp_head">პერსონები დავალება<hr class="callapp_head_hr"></div>
		

		<div id="cap-view" style="margin-bottom:25px;" class="demo-section k-content">

            <h4 style="margin-top: 2em; "><label for="theme" style="font-size:14px;">აირჩიეთ დიზაინი</label></h4>
            <select id="theme" style="width: 200px;" >
				<?
				
					$mysqli->setQuery("SELECT id,name FROM kendo_templates");
					$themeList = $mysqli->getResultArray();
					
					foreach($themeList['result'] AS $item)
					{
						if($result['result'][0]['kid'] == $item['id'])
						{
							echo '<option value="'.$item['id'].'" selected="selected">'.$item['name'].'</option>';
						}
						else
						{
							echo '<option value="'.$item['id'].'">'.$item['name'].'</option>';
						}
						
					}
				
				?>
            </select>

        </div>
	<div id="example">	
		<div id="grid"></div>
		<script>
			kendo.pdf.defineFont({
				"DejaVu Sans"             : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans.ttf",
				"DejaVu Sans|Bold"        : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Bold.ttf",
				"DejaVu Sans|Bold|Italic" : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Oblique.ttf",
				"DejaVu Sans|Italic"      : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Oblique.ttf",
				"WebComponentsIcons"      : "https://kendo.cdn.telerik.com/2017.1.223/styles/fonts/glyphs/WebComponentsIcons.ttf"
			});
		</script>
		<script src="https://kendo.cdn.telerik.com/2020.1.114/js/pako_deflate.min.js"></script>
		<script type="x/kendo-template" id="page-template">
			<div class="page-template">
				<div class="header">
					<div style="float: right">Page #: pageNum # of #: totalPages #</div>
				</div>
				<div class="watermark">KSOVRELA</div>
				<div class="footer">
					Page #: pageNum # of #: totalPages #
				</div>
			</div>
		</script>
		<script>
			
			$(document).ready(function () {
			<!-- KendoUI CLASS CONFIGS BEGIN -->


				var aJaxURL	= 				    "server-side/view/person_kendo.action2.php";
				var gridName = 				    'grid';
				var actions = 				    ["create",{"name":"asdad","text":"gagsa","iconClass":"fab fa-accessible-icon"},"pdf","excel"];
				var editType = 		 		    "inline"; // Two types "popup" and "inline"
				var itemPerPage = 	 		    6;
				var columnsCount =			    6;
				var columnGeoNames = 		    ["თარიღი","სახელი","გვარი","ასაკი","სქესი"];
				
				
				var showOperatorsByColumns =    [0,0,0,1,0]; //IF NEED USE 1 ELSE USE 0
				var selectors = 			    [0,0,0,0,'sex']; //IF NEED NOT USE 0
				
				
				var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
				
				
			<!-- KendoUI CLASS CONFIGS END ----->


				const kendo = new kendoUI();
				kendo.loadKendoUI(aJaxURL,itemPerPage,columnsCount,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors);




                // create DropDownList from select HTML element
                $("#theme").kendoDropDownList();
                var size = $("#theme").data("kendoDropDownList");
                $(document).on("change", "#theme", function () {
                    param = new Object();
                    param.act = "change_theme";
                    param.theme_id = $('#theme').val();
                    $.ajax({
                        url: aJaxURL,
                        data: param,
                        success: function (data) {
                            location.reload();
                        }
                    });
                });




			});

            $(document).on('click', '.k-grid-asdad', function(){
                alert(1);
            });


		</script>
	</div>
		<style>
		.page-template {
          font-family: "DejaVu Sans", "Arial", sans-serif;
          position: absolute;
          width: 100%;
          height: 100%;
          top: 0;
          left: 0;
        }
        .page-template .header {
          position: absolute;
          top: 30px;
          left: 30px;
          right: 30px;
          border-bottom: 1px solid #888;
          color: #888;
        }
        .page-template .footer {
          position: absolute;
          bottom: 30px;
          left: 30px;
          right: 30px;
          border-top: 1px solid #888;
          text-align: center;
          color: #888;
        }
        .page-template .watermark {
          font-weight: bold;
          font-size: 400%;
          text-align: center;
          margin-top: 30%;
          color: #aaaaaa;
          opacity: 0.1;
          transform: rotate(-35deg) scale(1.7, 1.5);
        }
		.k-pdf-export .k-filterable .k-grid-filter{
			display:none;
		  }
		</style>
    
	</div>
</body>
</html>