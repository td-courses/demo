<html>

<head>
    <style type="text/css">
        .high {
            height: 0;
        }

        .excel_input {
            position: absolute;
            width: 200px;
            height: 40px;
            margin-top: -40px;
            z-index: 10;
        }
    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
</head>

<body>
    <div id="container">
        <div id="button_area">
            <button id="add_button">დამატება</button>
            <button id="delete_button">წაშლა</button>
        </div>

        <table class="display" id="Expresions_Table">
            <thead>
                <tr id="datatable_header">
                    <th>ID</th>
                    <th style="width: 50%;">მეტსახელი</th>
                    <th style="width: 50%;">სახელი</th>
                    <th class="check">#</th>
                </tr>
            </thead>
            <thead>
                <tr class="search_header">
                    <th class="colum_hidden"></th>
                    <th>
                        <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                    </th>
                    <th>
                        <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                    </th>
                    <th>
                        <div class="callapp_checkbox">
                            <input type="checkbox" id="check-all" name="check-all" />
                            <label for="check-all"></label>
                        </div>
                    </th>
                </tr>
            </thead>
        </table>
    </div>

    <!-- jQuery Dialog -->
    <div id="Expresions_Edit_Modal" class="form-dialog" title="ინფორმაციის წყარო">
        <!-- aJax -->
    </div>
    <!-- jQuery Dialog -->
    <div id="Expresions_Edit_Modal2" class="form-dialog" title="ინფორმაციის წყარო">
        <!-- aJax -->
    </div>
    <script type="text/javascript">
        var aJaxURL = "server-side/view/expresion.action.php";
        var importaJaxURL = "server-side/view/expresion.action.php?act=users_import";
        var tableName = "Expresions_Table";
        var importTable = "usersImport";
        var modalName = "Expresions_Edit_Modal";
        // var modalName2 = "Expresions_Edit_Modal2";
        var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>"; //form name

        let id;

        let tmp_id;
        let tmp_id2;



        $(document).ready(function() {
            LoadTable();

            /* Add Button ID, Delete Button ID */


        });

        function LoadTable() {

            GetDataTable(
                tableName,
                aJaxURL,
                "getExpresions",
                3,
                "",
                0,
                "",
                1,
                "desc",
                "",
                change_colum_main
            );
            setTimeout(function() {
                $('.ColVis, .dataTable_buttons').css('display', 'none');
            }, 90);



            if (tmp_id2 !== undefined) {
                tmp_id = tmp_id2;

            } else {
                tmp_id = Math.floor((Math.random() * 1234567890) + 155);
            }

            GetButtons("add_button", "delete_button");
            SetEvents(
                "add_button",
                "delete_button",
                "check-all",
                tableName,
                modalName,
                aJaxURL,
                "&tmp_id=" + tmp_id
            );

            SetEvents(importTable, modalName, aJaxURL);
        }

        function LoadModalTable() {

            id = $("#data_id").val();

            GetDataTable(
                importTable,
                aJaxURL,
                "users_import",
                4,
                "data_id=" + id + "&tmp_id=" + tmp_id,
                0,
                "",
                1,
                "desc",
                "",
                change_colum_main
            );

            setTimeout(function() {
                $('.ColVis, .dataTable_buttons').css('display', 'none');
            }, 90);

        }

        function LoadDialog() {

            GetDialog(modalName, 700, "auto", "", 'center top');

            LoadModalTable();

            GetButtons("export_button", "ExcelImport", "import_button");

            excel_upload()

        }

        function excel_upload() {

            document.getElementById("ExcelUploadForm").onchange = function() {


                $("#import_button").trigger("click");

            };

            var percent = $('.percent');
            percent.fadeOut(0);

            $('#ExcelUploadForm').ajaxForm({
                beforeSend: function() {
                    var percentVal = '0%';
                    percent.html(percentVal);
                    percent.show();

                },
                uploadProgress: function(event, position, total, percentComplete) {
                    var percentVal = percentComplete + '%';
                    percent.html(percentVal);
                },
                complete: function(xhr) {
                    percent.fadeOut(0);
                },
                success: function() {
                    $('#uploadStatus').html('');

                    LoadModalTable();

                },
                error: function() {
                    $('#uploadStatus').html('Upload cant be done');
                }
            });
        }

        $(document).on("click", "#save-dialog", function() {


            param = new Object();

            param.act = "add_user";
            param.nickname = $("#nickname").val();
            param.name = $("#name").val();
            param.tmp_id = tmp_id;

            if (param.name == "" || param.nickname == "") {
                alert("შეავსეთ ველი!");
            } else {
                $.ajax({
                    url: aJaxURL,
                    data: param,
                    success: function(data) {
                        if (typeof(data.error) != 'undefined') {
                            if (data.error != '') {
                                alert(data.error);
                            } else {
                                tmp_id2 = Math.floor((Math.random() * 9876543210) + 155);
                                CloseDialog(modalName);
                                LoadTable();
                                console.log(tmp_id2)
                            }
                        }
                    }
                });
            }
        });
    </script>
</body>

</html>