<html>
<head>
<script type="text/javascript">
	var aJaxURL	= "server-side/view/chat_setting.action.php";		//server side folder url

	    	
	$(document).ready(function () {        	
		loadContent();
		$("#saveSetting").button();
	});
	
    function loadContent(){
    	$.ajax({
	        url: aJaxURL,
		    data: "act=get_setting",
	        success: function(data) {
	        	if(typeof(data.error) != 'undefined'){
	        		if(data.error != ''){
						alert(data.error);
					}else{
						$('#ip_count').val(data.ip_count);
						if(data.welcome_on_off == 1){
							$( "#welcome_on_off" ).prop( "checked", true );
						}else{
							$( "#welcome_on_off" ).prop( "checked", false );
						}
						$('#welcome_text').val(data.welcome_text);
						if(data.input_on_off == 1){
							$( "#input_on_off" ).prop( "checked", true );
						}else{
							$( "#input_on_off" ).prop( "checked", false );
						}
						if(data.input_name == 1){
							$( "#input_name" ).prop( "checked", true );
						}else{
							$( "#input_name" ).prop( "checked", false );
						}
						if(data.input_mail == 1){
							$( "#input_mail" ).prop( "checked", true );
						}else{
							$( "#input_mail" ).prop( "checked", false );
						}
						if(data.input_phone == 1){
							$( "#input_phone" ).prop( "checked", true );
						}else{
							$( "#input_phone" ).prop( "checked", false );
						}
						if(data.sound_on_off == 1){
							$( "#sound_on_off" ).prop( "checked", true );
						}else{
							$( "#sound_on_off" ).prop( "checked", false );
						}
						if(data.send_file == 1){
							$( "#send_file" ).prop( "checked", true );
						}else{
							$( "#send_file" ).prop( "checked", false );
						}
						if(data.send_mail == 1){
							$( "#send_mail" ).prop( "checked", true );
						}else{
							$( "#send_mail" ).prop( "checked", false );
						}
						if(data.send_sound == 1){
							$( "#send_sound" ).prop( "checked", true );
						}else{
							$( "#send_sound" ).prop( "checked", false );
						}
						if(data.send_close == 1){
							$( "#send_close" ).prop( "checked", true );
						}else{
							$( "#send_close" ).prop( "checked", false );
						}
						
						$('#color_id').val(data.color_id);
						$('#text_color').val(data.text_color);
						$('#taimer').val(data.taimer);
					}
		    	}
	        }
	    });
    }

    function saveSetting(){
    	ip_count = $('#ip_count').val();
		if($("#welcome_on_off").is(':checked')){
			welcome_on_off = 1;
		}else{
			welcome_on_off = 0;
		}
		welcome_text = $('#welcome_text').val();
		if($("#input_on_off").is(':checked')){
			input_on_off = 1
		}else{
			input_on_off = 0;
		}
		if($("#input_name").is(':checked')){
			input_name = 1;
		}else{
			input_name = 0;
		}
		if($("#input_mail").is(':checked')){
			input_mail = 1;
		}else{
			input_mail = 0;
		}
		if($("#input_mail").is(':checked')){
			input_phone = 1;
		}else{
			input_phone = 0;
		}
		if($("#sound_on_off").is(':checked')){
			sound_on_off = 1;
		}else{
			sound_on_off = 0;
		}

		if($("#send_file").is(':checked')){
			send_file = 1;
		}else{
			send_file = 0;
		}
		if($("#send_mail").is(':checked')){
			send_mail = 1;
		}else{
			send_mail = 0;
		}
		if($("#send_sound").is(':checked')){
			send_sound = 1;
		}else{
			send_sound = 0;
		}
		if($("#send_close").is(':checked')){
			send_close = 1;
		}else{
			send_close = 0;
		}
		+"&send_file="+send_file+"&send_mail="+send_mail+"&send_sound="+send_sound+"&send_close="+send_close+
    	$.ajax({
	        url: aJaxURL,
		    data: "act=save_setting&ip_count="+ip_count+"&welcome_on_off="+welcome_on_off+"&welcome_text="+welcome_text+"&input_on_off="+input_on_off+"&input_name="+input_name+"&input_mail="+input_mail+"&input_phone="+input_phone+"&sound_on_off="+sound_on_off+"&color_id="+$('#color_id').val()+"&text_color="+$('#text_color').val()+"&send_file="+send_file+"&send_mail="+send_mail+"&send_sound="+send_sound+"&send_close="+send_close+'&taimer='+$('#taimer').val(),
	        success: function(data) {
	        	if(typeof(data.error) != 'undefined'){
	        		if(data.error != ''){
						alert(data.error);
					}else{
						loadContent();
					}
		    	}
	        }
	    });
    }

    $(document).on("click", "#input_on_off", function () {
    	if ($(this).is(':checked')) {
    		$( "#input_mail" ).prop( "checked", true );
    		$( "#input_phone" ).prop( "checked", true );
    		$( "#input_name" ).prop( "checked", true );
    	}else{
    		$( "#input_mail" ).prop( "checked", false );
    		$( "#input_phone" ).prop( "checked", false );
    		$( "#input_name" ).prop( "checked", false );
    	}
    });
</script>
</head>

<body>
    <div id="tabs" style="width: 100%;">
    	<div class="callapp_head">ჩატის პარამეტრები<hr class="callapp_head_hr"></div>
    <div id="dialog-form">
	    <fieldset>
	    	<legend>ძირითადი პარამეტრები</legend>

	    	<table class="dialog-form-table">
				<tr>
					<td style="width: 250px;"><label for="ip_count" title="თუ ip რაოდენობა მითითებულია 0 მაშინ ულიმიტოა!">მაქსიმალური რაოდენობა ერთი IP იდან</label></td>
					<td>
						<input style="width: 50px;" type="number" min="0" max="99" id="ip_count" class="idle" value="0" title="თუ ip რაოდენობა მითითებულია 0 მაშინ  ულიმიტოა!" />
					</td>
				</tr>
				<tr>
					<td style="width: 250px;"><label for="welcome_on_off">მისალმების ტექსტის ჩართვა/გამორთვა</label></td>
					<td>
						<input type="checkbox" id="welcome_on_off"  />
					</td>
					<td style="width: 150px;"><label for="welcome_text">მისალმების ტექსტი</label></td>
					<td style="width: 250px;"><textarea style="width: 455px; height: 80px;" id="welcome_text" rows="" cols=""></textarea></td>
				</tr>
			</table>
			<table class="dialog-form-table">
				<tr>
					<td style="width: 250px;"><label for="input_on_off">შესასვლელად საჭირო აუცილებელი ველების ჩართვა/გამორთვა</label></td>
					<td style="width: 60px;">
						<input type="checkbox" id="input_on_off"  />
					</td>
					<td style="width: 60px;"><label for="input_name">სახელი *</label></td>
					<td style="width: 60px;"><input type="checkbox" id="input_name"  /></td>
					<td style="width: 70px;"><label for="input_mail">ე-ლფოსტა *</label></td>
					<td style="width: 60px;"><input type="checkbox" id="input_mail"  /></td>
					<td style="width: 70px;"><label for="input_phone">ტელეფონი *</label></td>
					<td style="width: 60px;"><input type="checkbox" id="input_phone"  /></td>
				</tr>
			</table>
			<table class="dialog-form-table">
			     <tr>
					<td style="width: 250px;"><label for="sound_on_off">მესიჯების ხმის  ჩართვა/გამორთვა</label></td>
					<td>
						<input type="checkbox" id="sound_on_off"  />
					</td>
				 </tr>
			</table>
			<table class="dialog-form-table">
			     <tr>
					<td style="width: 250px;"><label for="color_id">ჩატის ფერი</label></td>
					<td>
						<input type="color" id="color_id" value="#f5f5f5" />
					</td>
				 </tr>
			</table>
			<table class="dialog-form-table">
			     <tr>
					<td style="width: 250px;"><label for="text_color">ჩატის სათაურის ფერი</label></td>
					<td>
						<input type="color" id="text_color" value="#f5f5f5" />
					</td>
				 </tr>
			</table>
			
			<table class="dialog-form-table">
			     <tr>
					<td style="width: 250px;"><label for="send_file">ფაილის ღილაკის  ჩართვა/გამორთვა</label></td>
					<td>
						<input type="checkbox" id="send_file"  />
					</td>
				 </tr>
			</table>
			<table class="dialog-form-table">
			     <tr>
					<td style="width: 250px;"><label for="send_mail">მეილის ღილაკის  ჩართვა/გამორთვა</label></td>
					<td>
						<input type="checkbox" id="send_mail"  />
					</td>
				 </tr>
			</table>
			<table class="dialog-form-table">
			     <tr>
					<td style="width: 250px;"><label for="send_sound">ხმის ღილაკის  ჩართვა/გამორთვა</label></td>
					<td>
						<input type="checkbox" id="send_sound"  />
					</td>
				 </tr>
			</table>
			<table class="dialog-form-table">
			     <tr>
					<td style="width: 250px;"><label for="send_close">გამორთვის ღილაკის  ჩართვა/გამორთვა</label></td>
					<td>
						<input type="checkbox" id="send_close"  />
					</td>
				 </tr>
			</table>
			
			<table class="dialog-form-table">
			     <tr>
					<td style="width: 250px;"><label for="taimer">ჩატის ტაიმერი</label></td>
					<td>
						<input style="width: 50px;" type="number" id="taimer" min="1" maq="60" value="1" />
					</td>
				 </tr>
			</table>
			<button id="saveSetting" onclick="saveSetting()">განახლება</button>
        </fieldset>
    </div>

</body>
</html>