<html>
<head>
<style type="text/css">
.high {
    height: 0px;
}
</style>
	<script type="text/javascript">
    	var aJaxURL	= "server-side/view/department.action.php";		//server side folder url
    	var tName	= "example";													//table name
    	var fName	= "add-edit-form";												//form name
    	var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
    	    	
    	$(document).ready(function () {        	
    		LoadTable();	
    		GetButtons("add_button", "delete_button");			
    		SetEvents("add_button", "delete_button", "check-all", tName, fName, aJaxURL);
    	});
        
    	function LoadTable(){
    		GetDataTable(tName, aJaxURL, "get_list", 2, "", 0, "", 1, "desc", "", change_colum_main);
    		setTimeout(function(){$('.ColVis, .dataTable_buttons').css('display','none');}, 90);
    	}
    	
    	function LoadDialog(){
    		var id		= $("#department_id").val();
    		GetDialog(fName, 360, "auto", "","center top");
    	}

    	$(document).on("click", "#show_copy_prit_exel", function () {
	        if($(this).attr('myvar') == 0){
	            $('.ColVis,.dataTable_buttons').css('display','block');
	            $(this).css('background','#2681DC');
	            $(this).children('img').attr('src','media/images/icons/select_w.png');
	            $(this).attr('myvar','1');
	        }else{
	        	$('.ColVis,.dataTable_buttons').css('display','none');
	        	$(this).css('background','#E6F2F8');
	            $(this).children('img').attr('src','media/images/icons/select.png');
	            $(this).attr('myvar','0');
	        }
	    });
	    // Add - Save
	    $(document).on("click", "#save-dialog", function () {
		    param 			= new Object();

		    param.act		="save_department";
	    	param.id		= $("#department_id").val();
	    	param.name		= $("#name").val();
	    	
			if(param.name == ""){
				alert("შეავსეთ ველი!");
			}else {
			    $.ajax({
			        url: aJaxURL,
				    data: param,
			        success: function(data) {			        
						if(typeof(data.error) != 'undefined'){
							if(data.error != ''){
								alert(data.error);
							}else{
								LoadTable();
				        		CloseDialog(fName);
							}
						}
				    }
			    });
			}
		});

	   
    </script>
</head>

<body>
    <div id="tabs">
    	<div class="callapp_head">განყოფილება<hr class="callapp_head_hr"></div>
        <div id="button_area">
        	<button id="add_button">დამატება</button>
        	<button id="delete_button">წაშლა</button>
        </div>
        <table id="table_right_menu" style="top: 37px;">
            <tr>
            	<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;background:#2681DC;" id="show_table" myvar="0">
            		<img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
            	</td>
                <td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_log" myvar="0">
                	<img alt="log" src="media/images/icons/log.png" height="14" width="14">
                </td>
                <td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_copy_prit_exel" myvar="0">
                	<img alt="link" src="media/images/icons/select.png" height="14" width="14">
                </td>
            </tr>
        </table>
        <table class="display" id="example">
            <thead>
                <tr id="datatable_header">
                    <th>ID</th>
                    <th style="width: 100%;">დასახელება</th>
                	<th class="check">#</th>
                </tr>
            </thead>
            <thead>
                <tr class="search_header">
                     <th class="colum_hidden">
                     	<input type="text" name="search_category" value="ფილტრი" class="search_init" />
                     </th>
                     <th>
                         <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                     </th>
                     <th>
                    	<div class="callapp_checkbox">
                            <input type="checkbox" id="check-all" name="check-all" />
                            <label for="check-all"></label>
                        </div>
                     </th>
                </tr>
            </thead>
        </table>
            
        
    
    <!-- jQuery Dialog -->
    <div id="add-edit-form" class="form-dialog" title="განყოფილებები">
    	<!-- aJax -->
	</div>
</body>
</html>


