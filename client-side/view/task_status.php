<html>
<head>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <script type="text/javascript">
        var aJaxURL	          = "server-side/view/task_status.action.php";
        var tName             = "example";
        var dialog            = "add-edit-form";
        var colum_number      = 5;
        var main_act          = "get_list";
        var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";

        $.fn.extend({
            treed: function (o) {

                var openedClass = 'glyphicon-minus-sign';
                var closedClass = 'glyphicon-plus-sign';

                if (typeof o != 'undefined'){
                    if (typeof o.openedClass != 'undefined'){
                        openedClass = o.openedClass;
                    }
                    if (typeof o.closedClass != 'undefined'){
                        closedClass = o.closedClass;
                    }
                };

                //initialize each of the top levels
                var tree = $(this);
                tree.addClass("tree");
                tree.find('li').has("ul").each(function () {
                    var branch = $(this); //li with children ul
                    branch.prepend("<i class='indicator glyphicon " + closedClass + "'></i>");
                    branch.addClass('branch');
                    branch.on('click', function (e) {
                        if (this == e.target) {
                            var icon = $(this).children('i:first');
                            icon.toggleClass(openedClass + " " + closedClass);
                            $(this).children().children().toggle();
                        }
                    })
                    branch.children().children().toggle();
                });
                //fire event from the dynamically added icon
                tree.find('.branch .indicator').each(function(){
                    $(this).on('click', function () {
                        $(this).closest('li').click();
                    });
                });
                //fire event to open branch if the li contains an anchor instead of text
                tree.find('.branch>a').each(function () {
                    $(this).on('click', function (e) {
                        $(this).closest('li').click();
                        e.preventDefault();
                    });
                });
                //fire event to open branch if the li contains a button instead of text
                tree.find('.branch>button').each(function () {
                    $(this).on('click', function (e) {
                        $(this).closest('li').click();
                        e.preventDefault();
                    });
                });
            }
        });

        $(document).ready(function () {
            //malibu
            GetButtons("add_button","delete_button");
            $("[data-button='jquery-ui-button']").button();

            $.ajax({
                url: aJaxURL,
                data: 'act=get_list',
                success: function(data) {
                    if(typeof(data.error) != 'undefined'){
                        if(data.error != ''){
                            alert(data.error);
                        }else{
                            $('#tree1').html(data.page);
                            $('#tree1').treed();
//                            $("#tree1 li").click();
                        }
                    }
                }
            });
        });

        function LoadDialog(fName){
            GetDialog(fName, 600, "auto", '', 'center top');
            $('#parent_id,#client_id').chosen({ search_contains: true });
            $('#add-edit-form, .add-edit-form-class').css('overflow','visible');
        }

        $(document).on("click", "#add_button", function () {
            $.ajax({
                url: aJaxURL,
                data: 'act=get_add_page',
                success: function(data) {
                    if(typeof(data.error) != 'undefined'){
                        if(data.error != ''){
                            alert(data.error);
                        }else{
                            LoadDialog(dialog);
                            $('#'+dialog).html(data.page);
                        }
                    }
                }
            });

        });

        $(document).on("click", ".cat_edit", function () {
            $.ajax({
                url: aJaxURL,
                data: 'act=get_edit_page&id='+$(this).attr('my_id'),
                success: function(data) {
                    if(typeof(data.error) != 'undefined'){
                        if(data.error != ''){
                            alert(data.error);
                        }else{
                            LoadDialog(dialog);
                            $('#'+dialog).html(data.page);
                        }
                    }
                }
            });
        });

        function deleteCategory(objid) {

            $.ajax({
                url: aJaxURL,
                data: 'act=disable&id='+objid,
                success: function(data) {
                    if(typeof(data.error) != 'undefined'){
                        if(data.error != ''){
                            alert(data.error);
                        }else{
                            $.ajax({
                                url: aJaxURL,
                                data: 'act=get_list',
                                success: function(data) {
                                    if(typeof(data.error) != 'undefined'){
                                        if(data.error != ''){
                                            alert(data.error);
                                        }else{
                                            $('#tree1').html(data.page);
                                            $('#tree1').treed();
                                        }
                                    }
                                }
                            });
                        }
                    }
                }
            });

        }

        $(document).on("click", ".cat_delete", function () {

            var objId = $(this).attr("my_id");

            var buttons2 = {
                "save": {
                    text: "კი",
                    id: "confirm-delete-category",
                    click: function () {

                        $('#ask-delete-category').dialog("close");
                        deleteCategory(objId);

                    }
                },"reject-delete-category": {
                    text: "არა",
                    id: "no-cc",
                    click: function () {
                        $('#ask-delete-category').dialog("close");
                    }
                }
            };

            GetDialog("ask-delete-category","300","auto",buttons2);

        });
        // Add - Save
        $(document).on("click", "#save-dialog", function () {

            var subCatecoryIsChecked = $("#show_sub_category_1_value").is(":checked");
            var endCatecoryIsChecked = $("#show_sub_category_2_value").is(":checked");

            param 			           = new Object();

            param.act		           = "save_category";
            param.id		           = $("#cat_id").val();
            param.category_value	   = $("#category_value").val();
            param.category_select	   = $("#category_select").val();
            param.sub_category_1_value = $('#sub_category_1_value').val();
            param.sub_category_1_select= $('#sub_category_1_select').val();
            param.sub_category_2_value = $('#sub_category_2_value').val();

            if(!subCatecoryIsChecked && (param.category_value === "" && param.category_select === "0")) {
                alert("გთხოვთ დააფიქსიროთ კატეგორია");
            } else if(subCatecoryIsChecked && (param.category_value === "" && param.category_select === "0") && (param.sub_category_1_value === "" && param.sub_category_1_select ==="0")) {
                alert("გთხოვთ დააფიქსიროთ ქვე კატეგორია 1");
            } else {

                $.ajax({
                    url: aJaxURL,
                    data: param,
                    success: function(data) {
                        if(typeof(data.error) != 'undefined'){
                            if(data.error != ''){
                                alert(data.error);
                            }else{
                                $.ajax({
                                    url: aJaxURL,
                                    data: 'act=get_list',
                                    success: function(data) {
                                        if(typeof(data.error) != 'undefined'){
                                            if(data.error != ''){
                                                alert(data.error);
                                            }else{
                                                $('#tree1').html(data.page);
                                                $('#tree1').treed();
                                            }
                                        }
                                    }
                                });
                                CloseDialog(dialog);
                            }
                        }
                    }
                });

            }

        });
        $(document).on("change", "#category_select", function () {
            if($(this).val() == 0){
                $('#show_sub_category_2_value').prop("disabled",true);
            }else{
                $('#show_sub_category_2_value').prop("disabled",false);
            }
            $.ajax({
                url: aJaxURL,
                data: 'act=get_sub_cat&category_id='+$(this).val(),
                success: function(data) {
                    if(typeof(data.error) != 'undefined'){
                        if(data.error != ''){
                            alert(data.error);
                        }else{
                            $('#sub_category_1_select').html(data.page);
                        }
                    }
                }
            });
        });
        $(document).on("click", "#show_sub_category_1_value", function () {

            //define main category value
            var mainCategory = $("#category_value").val();

            // if main category in not selected, that means
            // it is new category adding proccess, but if
            // main category input has some value, it means
            // going on editing proccess
            // get main category id
            let mainCatValue = mainCategory !== "" ? $("#category_value").val() : false;
            let mainCatIndex = mainCatValue ? getOptionIndexByVal('#category_select', mainCatValue) : 0;

            if ($(this).is(':checked')) {
                $('#show_sub_category_2_value').prop("disabled",true);
                $('#category_value').css('display','none').val('');
                $('#category_select,#first_tr').css('display','');
                $('#sub_category_1_value').css({display: "block"});
                $('#sub_category_1_select').val(0);
                $("#cat_id").val("");
                document.getElementById('category_select').selectedIndex = mainCatIndex;

                if(!mainCatIndex) {
                    $('#sub_category_1_select').css({display: "none"});
                } else {
                    $('#show_sub_category_2_value').prop("disabled",false);
                }

            } else {
                $('#category_value').css('display','');
                $('#category_select,#first_tr').css('display','none');
                $("#sub_category_1_value").css({display: "none"});
                $("#sub_category_1_select").css({display: "none"});
                document.getElementById('category_select').selectedIndex = 0;
            }

        });
        $(document).on("click", "#show_sub_category_2_value", function () {

            // define variables for getting main category
            // index and retrieve sub category list
            var categoryId = $("#category_select").val();
            var self = this;

            getSubCategoryList(categoryId, function(subCategoryList) {

                // insert subcategory list in relevant select
                $('#sub_category_1_select').html(subCategoryList);

                //define main category value
                var subCategory = $("#sub_category_1_value").val();

                // if main category in not selected, that means
                // it is new category adding proccess, but if
                // main category input has some value, it means
                // going on editing proccess
                // get main category id
                let subCatValue = subCategory !== "" ? $("#sub_category_1_value").val() : false;
                let subCatIndex = subCatValue ? getOptionIndexByVal('#sub_category_1_select', subCatValue) : 0;

                if ($(self).is(':checked')) {

                    $('#show_sub_category_1_value').prop("disabled",true);
                    $('#sub_category_1_value').css('display','none');
                    $('#sub_category_1_value').val('');
                    $('#sub_category_1_select,#second_tr').css('display','');
                    $('#sub_category_2_value').css({display: "block"});
                    $("#cat_id").val("");
                    document.getElementById('sub_category_1_select').selectedIndex = subCatIndex;

                } else {
                    $('#show_sub_category_1_value').prop("disabled",false);
                    $('#sub_category_1_value').css('display','');
                    $('#sub_category_1_select,#second_tr').css('display','none');
                    document.getElementById('sub_category_1_select').selectedIndex = 0;
                    $('#sub_category_2_value').val('');
                }

            });

        });

        // get index of option from selec
        function getOptionIndexByVal(select, value) {

            var returnValue = 0;

            $(select).find("option").each(function(i) {

                var optionValue = $(this).text();

                if(value === optionValue) {
                    returnValue = i;
                }

            });

            return returnValue;

        }

        // get subcategory list
        function getSubCategoryList(categoryid, callback) {
            $.post(aJaxURL, {act: "get_sub_cat", category_id: categoryid}, result => {
                callback(result.page);
            });
        }

        // click on all categories toggle button
        $(document).on("click", ".toggle_all_categories", function() {

            //define variables
            let action = $(this).data("action");

            //toggle category list items
            if(action === "open") {
                $(".branch").find("li").css({display: "list-item"});
                $(".branch > i").removeClass("glyphicon-plus-sign").addClass("glyphicon-minus-sign");
            } else if(action === "close") {
                $(".branch").find("li").css({display: "none"});
                $(".branch > i").removeClass("glyphicon-minus-sign").addClass("glyphicon-plus-sign");
            }

        });

        //toscript
    </script>
    <style type="text/css">
        .tree, .tree ul {
            margin:0;
            padding:0;
            list-style:none
        }
        .tree ul {
            margin-left:1em;
            position:relative
        }
        .tree ul ul {
            margin-left:.5em
        }
        .tree ul:before {
            content:"";
            display:block;
            width:0;
            position:absolute;
            top:0;
            bottom:0;
            left:0;
            border-left:1px solid
        }
        .tree li {
            margin:0;
            padding:0 1em;
            line-height:19px;
            color:#369;
            font-weight:700;
            font-size:13px;
            position:relative
        }
        .tree ul li:before {
            content:"";
            display:block;
            width:10px;
            height:0;
            border-top:1px solid;
            margin-top:-1px;
            position:absolute;
            top:1em;
            left:0
        }
        .tree ul li:last-child:before {
            background:#fff;
            height:auto;
            top:1em;
            bottom:0
        }
        .indicator {
            margin-right:5px;
        }
        .tree li a {
            text-decoration: none;
            color:#369;
        }
        .tree li button, .tree li button:active, .tree li button:focus {
            text-decoration: none;
            color:#369;
            border:none;
            background:transparent;
            margin:0px 0px 0px 0px;
            padding:0px 0px 0px 0px;
            outline: 0;
        }
        .cat_img{
            width: 12px;
            margin-right: 7px;
            cursor: pointer;
        }
        .cat_img:hover {
            color: red;
        }
        i{
            cursor: pointer;
        }

        /*toggle all categories styles*/
        .toggle_all_categories {
            font-family: pvn;
            font-size: 13px;
            background: #E6F2F8;
            border: 1px solid #A3D0E4;
            padding: 4px 10px;
            position: relative;
            top: -3px;
        }

        .toggle_all_categories:hover {
            background: #dddddd;
            border: 1px solid #9b9b9b;
        }

        /*ask delete category text styles*/
        .ads-text {
            font-size: 12px;
        }

    </style>
</head>

<body>
<div id="tabs" style="height: 2963px;">
    <div class="callapp_head">სტატუსები<hr class="callapp_head_hr"></div>
    <div id="button_area">
        <button id="add_button" data-button="jquery-ui-button">დამატება</button>
        <button class="toggle_all_categories" data-action="open">
            ყველას გახსნა
        </button>
        <button class="toggle_all_categories" data-action="close">
            ყველას დახურვა
        </button>
    </div>

    <div class="container" style="margin-top:65px;">

        <ul id="tree1">

        </ul>

    </div>
    <!-- jQuery Dialog -->
    <div id="add-edit-form" class="form-dialog" title="მიტანის ტიპები">
        <!-- aJax -->
    </div>

    <!-- jQuery Dialog -->
    <div id="ask-delete-category" class="form-dialog" title="&nbsp;">
        <span class="ads-text">დარწმუნებული ბრძანდებით რომ გსურთ ამ კატეგორიის წაშლა?</span>
    </div>
</body>
</html>
