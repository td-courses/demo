<html>
<head>
<style type="text/css">
.high {
    height: 0px;
}
</style>
	<script type="text/javascript">
	    var aJaxURL	= "server-side/view/comment.action.php";		//server side folder url
		var tName	= "example";													//table name
		var fName	= "add-edit-form";												//form name
		var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
		 	
		$(document).ready(function () {
			GetTabs('tabs'); 
			LoadTable();
			LoadTable1();
				
			/* Add Button ID, Delete Button ID */
			GetButtons("add_button", "delete_button");
			GetButtons("add_button_chat", "delete_button_chat");	
			SetEvents("add_button", "delete_button", "check-all", tName, fName, aJaxURL,'check=1');
			SetEvents("add_button_chat", "delete_button_chat", "check-all-chat", 'example1', 'add-edit-form1', aJaxURL,'check=2');
		});
        
		function LoadTable(){
			GetDataTable_cnobari('example', aJaxURL, "get_list", 4, "", 0, "", 1, "desc", "", change_colum_main);
			setTimeout(function(){$('.ColVis, .dataTable_buttons').css('display','none');}, 90);
		}
		function LoadTable1(){
			GetDataTable_cnobari('example1', aJaxURL, "get_list1", 4, "", 0, "", 1, "desc", "", change_colum_main);
			setTimeout(function(){$('.ColVis, .dataTable_buttons').css('display','none');}, 90);
		}
		
		$(document).on("tabsactivate", "#tabs", function() {
        	var tab = GetSelectedTab('tabs');
        	if (tab == 0) {
        		LoadTable();
         	}else if(tab == 1){
         		LoadTable1();
            }
        });
		
		function LoadDialog(fr){
			if(fr == 'add-edit-form'){
    			var buttons = {
    					"save": {
    			            text: "შენახვა",
    			            id: "save-dialog"
    			        }, 
    		        	"cancel": {
    			            text: "დახურვა",
    			            id: "cancel-dialog",
    			            click: function () {
    			            	$(this).dialog("close");
    			            }
    			        } 
    			    };
    			
    			GetDialog(fName, 600, "auto", buttons,"center top");
    			$("#source_id").chosen({ search_contains: true });
    			$("#my_site").chosen({ search_contains: true });
			}else{
				var buttons = {
						"save": {
				            text: "შენახვა",
				            id: "save-dialog1"
				        }, 
			        	"cancel": {
				            text: "დახურვა",
				            id: "cancel-dialog",
				            click: function () {
				            	$(this).dialog("close");
				            }
				        } 
				    };
				
				GetDialog('add-edit-form1', 600, "auto", buttons, "center top");
				$("#source_id1").chosen({ search_contains: true });
				$("#my_site1").chosen({ search_contains: true });
			}
		}

		$(document).on("click", "#show_copy_prit_exel", function () {
	        if($(this).attr('myvar') == 0){
	            $('.ColVis,.dataTable_buttons').css('display','block');
	            $(this).css('background','#2681DC');
	            $(this).children('img').attr('src','media/images/icons/select_w.png');
	            $(this).attr('myvar','1');
	        }else{
	        	$('.ColVis,.dataTable_buttons').css('display','none');
	        	$(this).css('background','#E6F2F8');
	            $(this).children('img').attr('src','media/images/icons/select.png');
	            $(this).attr('myvar','0');
	        }
	    });
	    
		$(document).on("click", "#delete_button_chat", function () {
			LoadTable1();
		});
		
		// Add - Save
	    $(document).on("click", "#save-dialog", function () {
	    	site                = $("#my_site").chosen().val();
	    	source_id           = $("#source_id").chosen().val();
	    	
		    param 			= new Object();

		    param.act		="save_priority";
	    	param.id		= $("#comment_id").val();
	    	param.comment	= $("#comment").val();
	    	param.site	    = site;
	    	param.source_id = source_id;
	    	
			if(param.name == ""){
				alert("შეავსეთ ველი!");
			}else {
				var link = GetAjaxData(param);
			    $.ajax({
			        url: aJaxURL,
				    data: param,
			        success: function(data) {			        
						if(typeof(data.error) != 'undefined'){
							if(data.error != ''){
								alert(data.error);
							}else{
								LoadTable();
				        		CloseDialog(fName);
							}
						}
				    }
			    });
			}
		});

	    $(document).on("click", "#save-dialog1", function () {
	    	site                = $("#my_site1").chosen().val();
	    	source_id           = $("#source_id1").chosen().val();
	    	
		    param 			= new Object();

		    param.act		="save_priority1";
	    	param.id		= $("#comment_id1").val();
	    	param.comment	= $("#comment1").val();
	    	param.site	    = site;
	    	param.source_id = source_id;
	    	
			if(param.name == ""){
				alert("შეავსეთ ველი!");
			}else {
				var link = GetAjaxData(param);
			    $.ajax({
			        url: aJaxURL,
				    data: param,
			        success: function(data) {			        
						if(typeof(data.error) != 'undefined'){
							if(data.error != ''){
								alert(data.error);
							}else{
								LoadTable1();
				        		CloseDialog('add-edit-form1');
							}
						}
				    }
			    });
			}
		});

    </script>
</head>

<body>
<div class="callapp_head" style="padding: 12px 0px 0px 6px;">საუბრის შინაარსები<hr class="callapp_head_hr"></div>
    <div id="tabs" style="width: 97%;margin-left: 6px; border: 0px solid #aaaaaa;">
		<ul>
			<li><a href="#tab-0">ზარის საუბრის შინაარსები</a></li>
			<li><a href="#tab-1">ჩატის საუბრის შინაარსები</a></li>
		</ul>
		<div id="tab-0">
        	<div id="button_area">
    			<button id="add_button">დამატება</button>
    			<button id="delete_button">წაშლა</button>
    		</div>
    		<table id="table_right_menu" style="top: 37px;">
                <tr>
                	<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;background:#2681DC;" id="show_table" myvar="0">
                		<img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
                	</td>
                    <td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_log" myvar="0">
                    	<img alt="log" src="media/images/icons/log.png" height="14" width="14">
                    </td>
                    <td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_copy_prit_exel" myvar="0">
                    	<img alt="link" src="media/images/icons/select.png" height="14" width="14">
                    </td>
                </tr>
            </table>
            <table class="display" id="example">
                <thead >
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width: 30%;">საიტი</th>
                        <th style="width: 30%;">წყარო</th>
                        <th style="width: 40%;">კომენტარი</th>
                        <th class="check">#</th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                         	<input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                      	<th>
                        	<div class="callapp_checkbox">
                                <input type="checkbox" id="check-all" name="check-all" />
                                <label for="check-all"></label>
                            </div>
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
        <div id="tab-1">
            <table id="table_right_menu" style="top: 37px;">
                <tr>
                	<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;background:#2681DC;" id="show_table" myvar="0">
                		<img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
                	</td>
                    <td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_log" myvar="0">
                    	<img alt="log" src="media/images/icons/log.png" height="14" width="14">
                    </td>
                    <td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_copy_prit_exel" myvar="0">
                    	<img alt="link" src="media/images/icons/select.png" height="14" width="14">
                    </td>
                </tr>
            </table>
            <div id="button_area">
    			<button id="add_button_chat">დამატება</button>
    			<button id="delete_button_chat">წაშლა</button>
    		</div>
            <table class="display" id="example1">
                <thead >
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width: 30%;">საიტი</th>
                        <th style="width: 30%;">წყარო</th>
                        <th style="width: 40%;">კომენტარი</th>
                    	<th class="check">#</th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                         	<input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                      	<th>
                        	<div class="callapp_checkbox">
                                <input type="checkbox" id="check-all-chat" name="check-all-chat" />
                                <label for="check-all-chat"></label>
                            </div>
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <!-- jQuery Dialog -->
    <div id="add-edit-form" class="form-dialog" title="კომენტარი">
    	<!-- aJax -->
	</div>
	<!-- jQuery Dialog -->
    <div id="add-edit-form1" class="form-dialog" title="კომენტარი">
    	<!-- aJax -->
	</div>
</body>
</html>






