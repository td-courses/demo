<html>
<head>
<style type="text/css">
.high {
    height: 0px;
}

#dialog_emojis{
	position:absolute;
	cursor:pointer;
	bottom:5px;
	right:5px;
	background:#fff;
	border:none;
}

#emoji_close{
	float: left;
    font-size: 15px;
    cursor: pointer;
    color: red;
    width: -webkit-fill-available;
    text-align: center;
    padding: 5px;
    border: 1px solid #CCC;
    margin-bottom: 5px;
    background: #EEE;
}

#emoji_wrapper{
	height: 0;
    width: 100%;
    position: absolute;
    z-index: 20;
	overflow:auto;
	transition: height 0.2s;
	background: #EEE;
	bottom:0;
}

#emoji_wrapper::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	background-color: #F5F5F5;
}

#emoji_wrapper::-webkit-scrollbar
{
	width: 7px;
	background-color: #F5F5F5;
}

#emoji_wrapper::-webkit-scrollbar-thumb
{
	background-color: #A3D0E4;	
	background-image: -webkit-linear-gradient(45deg,
	                                          rgba(255, 255, 255, .2) 25%,
											  transparent 25%,
											  transparent 50%,
											  rgba(255, 255, 255, .2) 50%,
											  rgba(255, 255, 255, .2) 75%,
											  transparent 75%,
											  transparent)
}


#chat_scroll::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	background-color: #F5F5F5;
}

#chat_scroll::-webkit-scrollbar
{
	width: 7px;
	background-color: #F5F5F5;
}

#chat_scroll::-webkit-scrollbar-thumb
{
	background-color: #A3D0E4;	
	background-image: -webkit-linear-gradient(45deg,
	                                          rgba(255, 255, 255, .2) 25%,
											  transparent 25%,
											  transparent 50%,
											  rgba(255, 255, 255, .2) 50%,
											  rgba(255, 255, 255, .2) 75%,
											  transparent 75%,
											  transparent)
}

#emoji_wrapper span {
	font-size: 15px;
	cursor: pointer;
	line-height: 30px;
}
</style>
	<script type="text/javascript">
	    var aJaxURL	= "server-side/view/chat_comment.action.php";		//server side folder url
		var tName	= "example";													//table name
		var fName	= "add-edit-form";												//form name
		var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
		 	
		$(document).ready(function () {
			LoadTable();	
				
			/* Add Button ID, Delete Button ID */
			GetButtons("add_button", "delete_button");			
			SetEvents("add_button", "delete_button", "check-all", tName, fName, aJaxURL);
		});
        
		function LoadTable(){
			GetDataTable_cnobari(tName, aJaxURL, "get_list", 5, "", 0, "", 1, "desc", "", change_colum_main);
			setTimeout(function(){$('.ColVis, .dataTable_buttons').css('display','none');}, 90);
		}
		
		function LoadDialog(){
			var id		= $("#comment_id").val();
			
			GetDialog(fName, 600, "auto", "");
			$("#source_id").chosen({ search_contains: true });
			$("#my_site").chosen({ search_contains: true });
			$("#comment").cleditor();
			// setTimeout(function(){ 
			// 	new TINY.editor.edit('editor',{
			// 		id:'comment',
			// 		width:"500px",
			// 		height:"100%",
			// 		cssclass:'te',
			// 		controlclass:'tecontrol',
			// 		dividerclass:'tedivider',
			// 		controls:['bold','italic','underline','strikethrough','|','subscript','superscript','|',
			// 		'orderedlist','unorderedlist','|','outdent','indent','|','leftalign',
			// 		'centeralign','rightalign','blockjustify','|','unformat','|','undo','redo','n',
			// 		'font','size','|','image','hr','link','unlink','|','print'],
			// 		footer:true,
			// 		fonts:['Verdana','Arial','Georgia','Trebuchet MS'],
			// 		xhtml:true,
			// 		bodyid:'editor',
			// 		footerclass:'tefooter',
			// 		resize:{cssclass:'resize'}
			// 	}); }, 100);
		}

		$(document).on("click", "#show_copy_prit_exel", function () {
	        if($(this).attr('myvar') == 0){
	            $('.ColVis,.dataTable_buttons').css('display','block');
	            $(this).css('background','#2681DC');
	            $(this).children('img').attr('src','media/images/icons/select_w.png');
	            $(this).attr('myvar','1');
	        }else{
	        	$('.ColVis,.dataTable_buttons').css('display','none');
	        	$(this).css('background','#E6F2F8');
	            $(this).children('img').attr('src','media/images/icons/select.png');
	            $(this).attr('myvar','0');
	        }
	    });
		
		$(document).on("click","#dialog_emojis",function(){
				var height = $("#emoji_wrapper").height();
				if(height > 0){
					$("#emoji_wrapper").css("height","0");
				}
				else{
					$("#emoji_wrapper").css("height","35%");
					
				}
			})

			$(document).on("click","#emoji_wrapper span",function(){
				var emoji = $(this).attr("code");
				if(emoji != "0"){
					$("#comment").val($("#comment").val()+" "+emoji);
					$("#comment").trigger('focus');
					$("#emoji_wrapper").css("height","0");
				}
				else {
					$("#comment").trigger('focus');
					$("#emoji_wrapper").css("height","0");
				}
				

			})


	    // Add - Save
	    $(document).on("click", "#save-dialog", function () {
	    	site                = $("#my_site").chosen().val();
	    	source_id           = $("#source_id").chosen().val();
	    	param 			    = new Object();
			param.act		    ="save_priority";
	    	param.id		    = $("#comment_id").val();
	    	comment	    = $("iframe").contents().find("body").html();
			comment = comment.replace(/[&]nbsp[;]/gi," ");
			param.comment = comment.replace(/[+]/g,"W20W");
			var comment_name = $("#comment_name").val();
	    	param.comment_name	= comment_name.replace(/[+]/g,"W20W");
	    	param.site	        = site;
	    	param.source_id  	= source_id;
	    	
			if(param.name == ""){
				alert("შეავსეთ ველი!");
			}else {
				var link = GetAjaxData(param);
			    $.ajax({
			        url: aJaxURL,
				    data: param,
			        success: function(data) {			        
						if(typeof(data.error) != 'undefined'){
							if(data.error != ''){
								alert(data.error);
							}else{
								LoadTable();
				        		CloseDialog(fName);
							}
						}
				    }
			    });
			}
		});

	   
    </script>
	<link rel="stylesheet" href="media/css/tinyeditor.css" />

<script type="text/javascript" src="js/tinyeditor.js"></script>
</head>

<body>
    <div id="tabs" style="width: 100%;">
		<div class="callapp_head">ჩატის შაბლონები<hr class="callapp_head_hr"></div>
    	<div id="button_area">
			<button id="add_button">დამატება</button>
			<button id="delete_button">წაშლა</button>
		</div>
		<table id="table_right_menu" style="top: 37px;">
            <tr>
            	<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;background:#2681DC;" id="show_table" myvar="0">
            		<img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
            	</td>
                <td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_log" myvar="0">
                	<img alt="log" src="media/images/icons/log.png" height="14" width="14">
                </td>
                <td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_copy_prit_exel" myvar="0">
                	<img alt="link" src="media/images/icons/select.png" height="14" width="14">
                </td>
            </tr>
        </table>
        <table class="display" id="example">
            <thead >
                <tr id="datatable_header">
                    <th>ID</th>
                    <th style="width: 20%;">დასახელება</th>
                    <th style="width: 20%;">საიტი</th>
                    <th style="width: 20%;">წყარო</th>
                    <th style="width: 40%;">ტექსტი</th>
                	<th class="check">#</th>
                </tr>
            </thead>
            <thead>
                <tr class="search_header">
                    <th class="colum_hidden">
                     	<input type="text" name="search_category" value="ფილტრი" class="search_init" />
                    </th>
                    <th>
                        <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                    </th>
                    <th>
                        <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                    </th>
                    <th>
                        <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                    </th>
                    <th>
                        <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                    </th>
                  	<th>
                    	<div class="callapp_checkbox">
                            <input type="checkbox" id="check-all" name="check-all" />
                            <label for="check-all"></label>
                        </div>
                    </th>
                </tr>
            </thead>
        </table>
    </div>
    <!-- jQuery Dialog -->
    <div id="add-edit-form" class="form-dialog" title="ჩატის კომენტარი">
    	<!-- aJax -->
	</div>
</body>
</html>






