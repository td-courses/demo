<html>
<head>
<script src="https://code.highcharts.com/highcharts.js"></script>
<style type="text/css">
.high {
    height: 0px;
}
</style>
	
</head>

<body>
    <div id="tabs">
		<div class="callapp_head">პერსონები<hr class="callapp_head_hr"></div>
		<div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
		<div id="mailer" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
	</div>
    <!-- jQuery Dialog -->
    <div id="add-edit-form" class="form-dialog" title="პერსონა">
    	<!-- aJax -->
	</div>
	<div id="add-edit-form-img" class="form-dialog" title="თანამშრომლის სურათი">
	</div>
	<div id="add-edit-form3" class="form-dialog" title="განყოფილება">
	<!-- aJax -->
	</div>
<script type="text/javascript">
	    // Build the chart
$(document).ready(function() {
	var aJaxURL	= "server-side/view/person_stat.action.php";
	//var data2 = JSON.parse('[{"name": "Brands","colorByPoint": true,"data": [{"name": "Chrome","y": 61.41,"sliced": true,"selected": true}]}]');
	$.ajax({
		async: true,
		url: aJaxURL,
		data: 'act=get_name_stat',
		success: function(data) {
				console.log(data);
				// Build the chart
				Highcharts.chart('container', {
				  chart: {
					plotBackgroundColor: null,
					plotBorderWidth: null,
					plotShadow: false,
					type: 'pie'
				  },
				  title: {
					text: 'პერსონების სტატისტიკა სახელის მიხედვით'
				  },
				  tooltip: {
					pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
				  },
				  plotOptions: {
					pie: {
					  allowPointSelect: true,
					  cursor: 'pointer',
					  dataLabels: {
						enabled: false
					  },
					  showInLegend: true
					}
				  },
				  series: data
				});
		}
	});
	$.ajax({
		async: true,
		url: aJaxURL,
		data: 'act=get_person_email_stat',
		success: function(data) {
				console.log(data);
				// Build the chart
				Highcharts.chart('mailer', {
				  chart: {
					plotBackgroundColor: null,
					plotBorderWidth: null,
					plotShadow: false,
					type: 'pie'
				  },
				  title: {
					text: 'პერსონების სტატისტიკა რეგისტრირებული Email-ების მიხედვით'
				  },
				  tooltip: {
					pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
				  },
				  plotOptions: {
					pie: {
					  allowPointSelect: true,
					  cursor: 'pointer',
					  dataLabels: {
						enabled: false
					  },
					  showInLegend: true
					}
				  },
				  series: data
				});
		}
	});
});

		 
    </script>
</body>
</html>