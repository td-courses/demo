<html>
<head>
<style type="text/css">
.high {
    height: 0px;
}
</style>
	<script type="text/javascript">
    	var aJaxURL	= "server-side/view/dnd_duration.action.php";		//server side folder url
    	var tName	= "example";													//table name
    	var fName	= "add-edit-form";												//form name
    	var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
    	    	
    	$(document).ready(function () {        	
			update_sec();
			$("#save-dialog").button();
    	});
        
		function update_sec() {

			$.ajax({
				url:aJaxURL,
				data:'act=get_sec',
				success:function(data){
					$("#sec").val(data.sec);
				}
			})
		}

    	function LoadTable(){
    		GetDataTable(tName, aJaxURL, "get_list", 2, "", 0, "", 1, "desc", "", change_colum_main);
    		setTimeout(function(){$('.ColVis, .dataTable_buttons').css('display','none');}, 90);
    	}
    	
    	function LoadDialog(){
    		var id		= $("#department_id").val();
    		GetDialog(fName, 360, "auto", "","center top");
    	}

    	$(document).on("click", "#show_copy_prit_exel", function () {
	        if($(this).attr('myvar') == 0){
	            $('.ColVis,.dataTable_buttons').css('display','block');
	            $(this).css('background','#2681DC');
	            $(this).children('img').attr('src','media/images/icons/select_w.png');
	            $(this).attr('myvar','1');
	        }else{
	        	$('.ColVis,.dataTable_buttons').css('display','none');
	        	$(this).css('background','#E6F2F8');
	            $(this).children('img').attr('src','media/images/icons/select.png');
	            $(this).attr('myvar','0');
	        }
	    });
	    // Add - Save
	    $(document).on("click", "#save-dialog", function () {
		    param 			= new Object();

		    param.act		="save_department";
	    	param.id		= $("#department_id").val();
	    	param.sec		= $("#sec").val();
	    	
			if(param.sec == ""){
				alert("შეავსეთ ველი!");
			}else {
			    $.ajax({
			        url: aJaxURL,
				    data: param,
			        success: function(data) {			        
						if(typeof(data.error) != 'undefined'){
							if(data.error != ''){
								alert(data.error);
							}else{
								update_sec();

							}
						}
				    }
			    });
			}
		});

	   
    </script>
</head>

<body>
    <div id="tabs">
    	<div class="callapp_head">პარამეტრები<hr class="callapp_head_hr"></div>
        <!-- <div id="button_area">
        	<button id="add_button">დამატება</button>
        	<button id="delete_button">წაშლა</button>
        </div> -->
        <!-- <table id="table_right_menu" style="top: 37px;">
            <tr>
            	<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;background:#2681DC;" id="show_table" myvar="0">
            		<img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
            	</td>
                <td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_log" myvar="0">
                	<img alt="log" src="media/images/icons/log.png" height="14" width="14">
                </td>
                <td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_copy_prit_exel" myvar="0">
                	<img alt="link" src="media/images/icons/select.png" height="14" width="14">
                </td>
            </tr>
        </table>
        <table style="width:100%" class="display" id="example">
            <thead>
                <tr id="datatable_header">
                    <th>ID</th>
                    <th style="width: 100%;">დრო</th>

                </tr>
            </thead>
            <thead>
                <tr class="search_header">
                     <th class="colum_hidden">
                     	<input type="text" name="search_category" value="ფილტრი" class="search_init" />
                     </th>
                     <th>
                         <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                     </th>
                     
                </tr>
            </thead>
        </table> -->


<div id="dialog-form">
	    <fieldset>
	    	<legend>ძირითადი ინფორმაცია</legend>

	    	<table class="dialog-form-table">
				<tr>
					<td style="width: 170px;"><label for="CallType" style="font-size: 15px;">WrapUp Time (წამი)</label></td>
					<td>
						<input type="number" id="sec" class="idle address"   />
					</td>
				</tr>

			</table>
			<!-- ID -->
			<input type="hidden" id="department_id"  />
        </fieldset>
		<button style="margin: 12px;" id="save-dialog">შენახვა</button>
    </div>
            
        
    
    <!-- jQuery Dialog -->
    <div id="add-edit-form" class="form-dialog" title="DND">
    	<!-- aJax -->
	</div>
</body>
</html>


