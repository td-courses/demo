<html>
<head>
<style type="text/css">
.high {
    height: 0px;
}
</style>
	<script type="text/javascript">
	    var aJaxURL	= "server-side/view/person_crud.action.php";		//server side folder url
		var aJaxURL2	= "server-side/view/person_crud.action2.php";
		var tName	= "example";													//table name
		var fName	= "add-edit-form";												//form name
		var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
		 	
		$(document).ready(function () {
			LoadTable();	
				
			/* Add Button ID, Delete Button ID */
			GetButtons("add_button", "delete_button");			
			SetEvents("add_button", "delete_button", "check-all", tName, fName, aJaxURL);
			GetDate('start_date');
			GetDate('end_date');
			$('#name_id').chosen({ search_contains: true });
			
		});
        
		function LoadTable(){
			var count = [4];
			GetDataTable(tName, aJaxURL, "get_list", 6, "start="+$('#start_date').val()+"&end="+$('#end_date').val()+"&name_id="+$('#name_id').val()+"", 0, "", 1, "desc", count, change_colum_main);
			setTimeout(function(){$('.ColVis, .dataTable_buttons').css('display','none');}, 90);
		}
		function LoadTable2(){						
			/* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
			GetDataTable("example4", aJaxURL2, "get_list", 2,"act_id="+$('#person_id').val(), 0, "", 1, "asc", '', "<'F'lip>");
			$(".dataTables_length").css('top', '0px');
			$('#sex_id,#cat_l,#subcat_l').chosen({ search_contains: true });
		}
		
		function LoadDialog(fName){
			switch(fName){
				case "add-edit-form":
					var id		= $("#person_id").val();
			
					GetDialog(fName, 600, "auto", "");
					LoadTable2();
					GetButtons("add_button_pp","del_button_pp");
					SetEvents("add_button_pp", "del_button_pp", "check-all2", "example4", "add-edit-form3", aJaxURL2, "act_id="+$('#person_id').val());			
					GetButtons("choose_button");
					GetButtons("choose_buttondisabled");					
					break;
				case "add-edit-form3":
					var buttons = {
						"save": {
				            text: "შენახვა",
				            id: "save-dialog3"
				        }, 
			        	"cancel": {
				            text: "დახურვა",
				            id: "cancel-dialog",
				            click: function () {
				            	$(this).dialog("close");
				            }
				        }
				    };
					GetDialog("add-edit-form3", 400, "auto", buttons);
					LoadTable2();					
					break;
			}
							
			
		}

		$(document).on("click", "#show_copy_prit_exel", function () {
	        if($(this).attr('myvar') == 0){
	            $('.ColVis,.dataTable_buttons').css('display','block');
	            $(this).css('background','#2681DC');
	            $(this).children('img').attr('src','media/images/icons/select_w.png');
	            $(this).attr('myvar','1');
	        }else{
	        	$('.ColVis,.dataTable_buttons').css('display','none');
	        	$(this).css('background','#E6F2F8');
	            $(this).children('img').attr('src','media/images/icons/select.png');
	            $(this).attr('myvar','0');
	        }
	    });
	    
	    // Add - Save
	    $(document).on("click", "#save-dialog", function () {
		    param 			= new Object();

		    param.act		="save_priority";
	    	param.id		= $("#person_id").val();
			param.saveT		= $("#saveType").val();
	    	param.firstname	= $("#firstname").val();
			param.surname	= $("#surname").val();
			param.age	= $("#age").val();
			param.sex	= $("#sex_id").val();
			param.cat_id	= $("#cat_l").val();
	    	param.subcat_id	= $("#subcat_l").val();
			
			
			if(param.firstname == ""){
				$("#firstname").css({"border":"solid 1px red"});
			}
			if(param.surname == ""){
				$("#surname").css({"border":"solid 1px red"});
			}
			if(param.age == ""){
				$("#age").css({"border":"solid 1px red"});
			}
			if(param.sex == 0){
				$("#sex_id").css({"border":"solid 1px red"});
			}
			
			if(param.firstname != '' && param.surname != '' && param.age != '' && param.sex_id != 0){
			    $.ajax({
			        url: aJaxURL,
				    data: param,
			        success: function(data) {			        
						if(typeof(data.error) != 'undefined'){
							if(data.error != ''){
								alert(data.error);
							}else{
								LoadTable();
				        		CloseDialog(fName);
							}
						}
				    }
			    });
			}
		});
		$(document).on("keypress", "#firstname", function () {
		    $("#firstname").css({"border":"solid 1px #85b1de"});
	    	
		});
		$(document).on("keypress", "#surname", function () {
		    $("#surname").css({"border":"solid 1px #85b1de"});
	    	
		});
		$(document).on("keypress", "#age", function () {
			if($.isNumeric($("#age").val()))
			{
				$("#age").css({"border":"solid 1px #85b1de"});
			}else{
				$("#age").css({"border":"solid 1px red"});
			}
	    	
		});
		$(document).on("change", "#sex_id", function () {
		    $("#sex_id").css({"border":"solid 1px #85b1de"});
	    	
		});
		$(document).on("click", "#save-dialog3", function () {
	    	
			param 					= new Object();
			param.act				= "save_action_3";
 			param.id				= $("#id").val();
			param.pid				= $("#person_id").val();			
 			param.email				= $("#email").val();
			
			
	 
 	    	$.ajax({
 			        url: aJaxURL2,
 				    data: param,
 			        success: function(data) {       
 						if(typeof(data.error) != "undefined"){
 							if(data.error != ""){
 								alert(data.error);
 							}else{
 								LoadTable2();
 								CloseDialog("add-edit-form3");
 							}
						}
 				    }
 			});
 		});
		$(document).on("click", "#del_button_pp", function () {
			LoadTable2();
 		});
		
		$(document).on("click", "#choose_button", function () {
		    $("#choose_file").click();
		});

	    $(document).on("click", "#choose_buttondisabled", function () {
		    alert('თუ გსურთ ახალი სურათის ატვირთვა, წაშალეთ მიმდინარე სურათი!');
		});

	    
	    $(document).on("change", "#choose_file", function () {
	        var file_url  = $(this).val();
	        var file_name = this.files[0].name;
			var file_name_n = Math.ceil(Math.random()*99999999999);
	        var file_size = this.files[0].size;
	        var file_type = file_url.split('.').pop().toLowerCase();
			var person_id = $("#person_id").val();
	        var path	  = "../../media/uploads/file/";

	        if($.inArray(file_type, ['png','jpg']) == -1){
	            alert("დაშვებულია მხოლოდ 'png', 'jpg'  გაფართოება");
	        }else if(file_size > '15728639'){
	            alert("ფაილის ზომა 15MB-ზე მეტია");
	        }else{
	            if($("#pers_id").val() == ''){
		            users_id = $("#is_user").val();
	            }else{
	            	users_id = $("#pers_id").val()
	            }
	        	$.ajaxFileUpload({
			        url: "server-side/upload/person_file.action.php",
			        secureuri: false,
	     			fileElementId: "choose_file",
	     			dataType: 'json',
				    data: {
						act: "file_upload",
						button_id: "choose_file",
						table_name: 'users',
						file_name: file_name_n,
						file_name_original: file_name,
						file_type: file_type,
						file_size: file_size,
						path: path,
						table_id: users_id,
						person_id: person_id,

					},
			        success: function(data) {			        
				     
								$("#upload_img").attr('src','media/uploads/file/'+file_name_n+'.'+file_type);
								$("#upload_img").attr('img',file_name_n+'.'+file_type);
								$('#choose_button').attr('id','choose_buttondisabled');
								$("#delete_image").attr('image_id',file_name_n+'.'+file_type);
								$(".complate").attr('onclick','view_image(123)');
												
											
				    }
			    });
	        }
	    });
		$(document).on("click", "#delete_image", function () {
		    $.ajax({
	            url: aJaxURL,
	            data: "act=delete_image&file_name="+$("#upload_img").attr('img'),
	            success: function(data) {
	               $('#upload_img').attr('src','media/uploads/file/0.jpg');
				   $('#upload_img').attr('img','0.jpg');               
	               $("#choose_button").button();
	               $('#choose_buttondisabled').attr('id','choose_button')
	            }
	        });
		});
		$(document).on("click", "#callapp_show_filter_button", function () {
	        if($('.callapp_filter_body').attr('myvar') == 0){
	        	$('.callapp_filter_body').css('display','block');
	        	$('.callapp_filter_body').attr('myvar',1);

	        	$('#go_exel').css('top','264px');
	        	$("#shh").css('background-position','0 0px');
	        }else{
	        	$('.callapp_filter_body').css('display','none');
	        	$('.callapp_filter_body').attr('myvar',0);

	        	$('#go_exel').css('top','171px');
	        	$("#shh").css('background-position','0 9px');
	        }
	    });
		$(document).on("click", "#loadtable", function () {
			LoadTable();
		});
		
		
		function view_image(id){
			param = new Object();

	        //Action
	    	param.act	= "view_img";
	    	param.id    = $("#person_id").val();
	    	
			$.ajax({
		        url: aJaxURL,
			    data: param,
		        success: function(data) {
					if(typeof(data.error) != "undefined"){
						if(data.error != ""){
							alert(data.error);
						}else{
							var buttons = {
						        	"cancel": {
							            text: "დახურვა",
							            id: "cancel-dialog",
							            click: function () {
							            	$(this).dialog("close");
							            }
							        }
							    };
							GetDialog("add-edit-form-img", 401, "auto", buttons, 'center top');
							$("#add-edit-form-img").html(data.page);
						}
					}
			    }
		    });
		}
		
		$(document).on("change", "#cat_l", function () {
            param = new Object();
            param.act = "cat_2";
            param.cat_id = $('#cat_l').val();
            $.ajax({
                url: aJaxURL,
                data: param,
                success: function (data) {
                    $("#subcat_l").html(data.page);
                    $('#subcat_l').trigger("chosen:updated");
                    
                }
            });
        });
		
		$(document).on("change", "#subcat_l", function () {
            param = new Object();
            param.act = "cat_1";
            param.subcat_id = $('#subcat_l').val();
            $.ajax({
                url: aJaxURL,
                data: param,
                success: function (data) {
                    $("#cat_l").html(data.page);
                    $('#cat_l').trigger("chosen:updated");
                    
                }
            });
        });
	   
    </script>
</head>

<body>
    <div id="tabs">
		<div class="callapp_head">პერსონები დავალება<hr class="callapp_head_hr"></div>
		<div class="callapp_filter_show">
<button id="callapp_show_filter_button" style="    background: #fff;">ფილტრი <div id="shh" style="background: url('media/images/icons/show.png'); width: 24px; height: 9px;background-position: 0 0px;margin-top: 5px;float: right;"></div></button>
    <div class="callapp_filter_body" myvar="1">
    	<div style="float: left; width: 100%;">
    		<table>
        		<tr>
            		<td style="width: 300px;">
                		<span>
                            <label for="start_date" style="margin-left: 110px;">-დან</label>
                            <input value="2015-01-01" class="callapp_filter_body_span_input" type="text" id="start_date" style="width: 100px;">
                        </span>
                        <span>
                            <label for="end_date" style="margin-left: 110px;">-მდე</label>
                            <input value="<? echo date("Y-m-d"); ?>" class="callapp_filter_body_span_input" type="text" id="end_date" style="width: 100px;">
                        </span>
            		</td>
            		<td rowspan="4">
            			
                    </td>
            	</tr>
            	<tr>
            		<td>
            			<select id="name_id" style="width: 281px;">
                        	<option value="0">ყველა სახელი</option>
                            <?php
                                $db->setQuery("SELECT `id`,`name` FROM `person` WHERE `actived`=1");
                                
                                $res = $db->getResultArray();
                                foreach($res[result] AS $req){
                                    $data .= "<option value='$req[id]'>$req[name]</option>";
                                }
                                echo $data;
                            ?>
                        </select>
                    </td>
                 </tr>
            </table>
        </div>
        
        
        
</div>
<div style="width: 100%;margin-top: 22px;">
    
</div>
    	<div id="button_area">
			<button id="add_button">დამატება</button>
			<button id="delete_button">წაშლა</button>
			<button id="loadtable" style="cursor: pointer; font-size: 13px;border: 1px solid #A3D0E4;background: #E6F2F8;padding: 5px 5px 3px 5px;font-family: pvn;">ფილტრი</button>
		</div>
		<table id="table_right_menu" style="top: 37px;">
            <tr>
            	<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;background:#2681DC;" id="show_table" myvar="0">
            		<img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
            	</td>
                <td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_log" myvar="0">
                	<img alt="log" src="media/images/icons/log.png" height="14" width="14">
                </td>
                <td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_copy_prit_exel" myvar="0">
                	<img alt="link" src="media/images/icons/select.png" height="14" width="14">
                </td>
            </tr>
        </table>
        <table class="display" id="example">
            <thead >
                <tr id="datatable_header">
                    <th>ID</th>
					<th style="width: 20%;">თარიღი</th>
                    <th style="width: 20%;">სახელი</th>
                    <th style="width: 20%;">გვარი</th>
                    <th style="width: 20%;">ასაკი</th>
					<th style="width: 20%;">სქესი</th>
                	<th class="check">#</th>
                </tr>
            </thead>
            <thead>
                <tr class="search_header">
                    <th class="colum_hidden">
                     	<input type="text" name="search_category" value="ფილტრი" class="search_init" />
                    </th>
					<th>
                        <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                    </th>
                    <th>
                        <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                    </th>
                    <th>
                        <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                    </th>
                    <th>
                        <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                    </th>
					<th>
                        <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                    </th>
                  	<th>
                    	<div class="callapp_checkbox">
                            <input type="checkbox" id="check-all" name="check-all" />
                            <label for="check-all"></label>
                        </div>
                    </th>
                </tr>
            </thead>
			<tfoot>
				<th>
					
				</th>
				<th>
					
				</th>
				<th>
					
				</th>
				<th>
					
				</th>
				<th>
					
				</th>
				<th>
					
				</th>
				<th>
					
				</th>
				
			</tfoot>
        </table>
    </div>
	</div>
    <!-- jQuery Dialog -->
    <div id="add-edit-form" class="form-dialog" title="პერსონა">
    	<!-- aJax -->
	</div>
	<div id="add-edit-form-img" class="form-dialog" title="თანამშრომლის სურათი">
	</div>
	<div id="add-edit-form3" class="form-dialog" title="განყოფილება">
	<!-- aJax -->
	</div>
</body>
</html>