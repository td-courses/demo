<html>
<head>
	<script type="text/javascript">
		var aJaxURL	= "server-side/view/chat_quest.action.php";		//server side folder url
		var tName	= "example";													//table name
		var fName	= "add-edit-form";												//form name
		var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
		    	
		$(document).ready(function () {        	
			LoadTable();	
						
			/* Add Button ID, Delete Button ID */
			GetButtons("add_button", "delete_button");			
			SetEvents("add_button", "delete_button", "check-all", tName, fName, aJaxURL);
		});
        
		function LoadTable(){
			
			/* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
			GetDataTable_cnobari(tName, aJaxURL, "get_list", 5, "", 0, "", 1, "desc", "", change_colum_main);
			setTimeout(function(){$('.ColVis, .dataTable_buttons').css('display','none');}, 90);
		}
		
		function LoadDialog(){
			var id		= $("#id").val();
			
			/* Dialog Form Selector Name, Buttons Array */
			GetDialog(fName, 415, "auto", "", "center top");
			$("#my_site").chosen();
			$("#add-edit-form, .add-edit-form-class").css('overflow','visible');
			
		}
		
	    // Add - Save
	    $(document).on("click", "#save-dialog", function () {
		    param 		 = new Object();

		    param.act	      = "save_problem";
	    	param.id	      = $("#id").val();
	    	param.quest	      = $("#quest").val();
	    	param.answer      = $("#answer").val();
	    	param.my_site     = $("#my_site").chosen().val();
	    	param.position    = $("#quest_position").val();
	    	param.answer_time = $("#answer_time").val();
	    	var link = GetAjaxData(param);
			if(param.quest == ""){
				alert("შეავსეთ კითხვა!");
			}else if(param.answer == ""){
				alert("შეავსეთ პასუხი!");
			}else{
			    $.ajax({
			        url: aJaxURL,
				    data: link,
			        success: function(data) {			        
						if(typeof(data.error) != 'undefined'){
							if(data.error != ''){
								alert(data.error);
							}else{
								LoadTable();
				        		CloseDialog(fName);
							}
						}
				    }
			    });
			}
		});

	    $(document).on("click", "#show_copy_prit_exel", function () {
	        if($(this).attr('myvar') == 0){
	            $('.ColVis,.dataTable_buttons').css('display','block');
	            $(this).css('background','#2681DC');
	            $(this).children('img').attr('src','media/images/icons/select_w.png');
	            $(this).attr('myvar','1');
	        }else{
	        	$('.ColVis,.dataTable_buttons').css('display','none');
	        	$(this).css('background','#E6F2F8');
	            $(this).children('img').attr('src','media/images/icons/select.png');
	            $(this).attr('myvar','0');
	        }
	    });
    </script>
</head>

<body>
    <div id="tabs">
		<div class="callapp_head">ხშირად დასმული კითხვები<hr class="callapp_head_hr"></div>
    	<div id="button_area">
			<button id="add_button">დამატება</button>
			<button id="delete_button">წაშლა</button>
		</div>
		<table id="table_right_menu" style="top: 37px;">
            <tr>
            	<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;background:#2681DC;" id="show_table" myvar="0">
            		<img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
            	</td>
                <td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_log" myvar="0">
                	<img alt="log" src="media/images/icons/log.png" height="14" width="14">
                </td>
                <td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_copy_prit_exel" myvar="0">
                	<img alt="link" src="media/images/icons/select.png" height="14" width="14">
                </td>
            </tr>
        </table>
        <table class="display" id="example">
            <thead >
                <tr id="datatable_header">
                    <th>ID</th>
                    <th style="width: 30%;">კითხვა</th>
                    <th style="width: 30%;">პასუხი</th>
                    <th style="width: 30%;">საიტები</th>
                    <th style="width: 10%;">პოზიცია</th>
                	<th class="check">#</th>
                </tr>
            </thead>
            <thead>
                <tr class="search_header">
                    <th class="colum_hidden">
                    	<input type="text" name="search_category" value="ფილტრი" class="search_init" />
                    </th>
                    <th>
                        <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                    </th>
                    <th>
                        <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                    </th>
                    <th>
                        <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                    </th>
                    <th>
                        <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                    </th>
                    <th>
                    	<div class="callapp_checkbox">
                            <input type="checkbox" id="check-all" name="check-all" />
                            <label for="check-all"></label>
                        </div>
                    </th>
                </tr>
            </thead>
        </table>
    </div>

    
    <!-- jQuery Dialog -->
    <div id="add-edit-form" class="form-dialog" title="ახალი კითხვა">
    	<!-- aJax -->
	</div>
</body>
</html>





