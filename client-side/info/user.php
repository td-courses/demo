<html>
<head>
	<script type="text/javascript">
		var aJaxURL	     = "server-side/info/user.action.php";		//server side folder url
		var upJaxURL     = "server-side/upload/file.action.php";				//server side folder url
		var tName	     = "example";											//table name
		var fName	     = "add-edit-form";										//form name
		var img_name	 = "0.jpg";
		var colum_number = 7;
	    var main_act     = "get_list";
	    
		var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";

		$(document).ready(function () {
			LoadTable();

			/* Add Button ID, Delete Button ID */
			GetButtons("add_button", "delete_button");

			SetEvents("add_button", "delete_button", "check-all", tName, fName, aJaxURL);

			$("#restore_button").button();
			GetTabs("tabs");
// 			$("#current").button();
// 			$("#archive").button();


		});

		function LoadTable(action = "get_list"){
			/* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
			GetDataTable(tName, aJaxURL, action, 7, "", 0, "", 1, "asc", "", change_colum_main);
			setTimeout(function(){
		    	$('.ColVis, .dataTable_buttons').css('display','none');
		    }, 90);
		}

		function LoadDialog(){
			var id		= $("#pers_id").val();
			if(id != ""){
				$("#lname_fname").attr("disabled", "disabled");
			}

			GetButtons("choose_button");
			GetButtons("choose_buttondisabled");
			
			/* Dialog Form Selector Name, Buttons Array */
			GetDialog(fName, 450, "auto", "");

			if( $("#position").val() == 13 ){
					$("#passwordTR").removeClass('hidden');
			}
			$( "#accordion" ).accordion({
				active: false,
				collapsible: true,
				heightStyle: "content",
				activate: function(event, ui) {
					$("#is_user").val();
				}
			});
			$('#position,#dep_id,#service_center_id,#branch_id,#ext, #site_ids').chosen({ search_contains: true });
		}

	    // Add - Save
		$(document).on("click", "#save-dialog", function () {
			values = $("#site_ids").chosen().val();
			param = new Object();

            //Action
	    	param.act	= "save_pers";

		    param.id	= $("#pers_id").val();

		    param.n		= $("#name").val();
		    param.t		= $("#tin").val();
		    param.p		= $("#position").val();
		    param.dep_id= $("#dep_id").val();
		    param.a		= $("#address").val();
		    param.pas	= $("#password").val();
		    param.h_n	= $("#home_number").val();
		    param.m_n	= $("#mobile_number").val();
		    param.comm	= $("#comment").val();
		    param.service_center_id	= $("#service_center_id").val();
		    param.branch_id    	 = $("#branch_id").val();
		    param.task_send_mail = $('input[id=task_send_mail]:checked').val();
		    param.user_mail      = $("#user_mail").val();
		    param.site           = values;
		    param.user	= $("#user").val();
		    param.userp	= $("#user_password").val();
		    param.gp	= $("#group_permission").val();
		    param.ext	= $("#ext").val();
			if ($("#upload_img").attr('img') != img_name){
				param.img = $("#upload_img").attr('img');
			}
			else {
				param.img 	= img_name;
			}
		    
		    
			if(param.n == ""){
				alert("შეავსეთ სახელი და გვარი!");
			} else if(!param.user || !param.userp){
				alert("შეავსეთ მომხმარებლის სახელი და პაროლი");
			}else if(param.task_send_mail == 1 && $("#user_mail").val()==''){
				alert("შეავსეთ მეილი");
			}else if(param.task_send_mail == 1 && (values == null || values == '')){
				alert("შეავსეთ საიტი");
			} else{
				var link = GetAjaxData(param);
			    $.ajax({
			        url: aJaxURL,
				    data: link,
			        success: function(data) {
						if(typeof(data.error) != "undefined"){
							if(data.error != ""){
								alert(data.error);
							}else{
								LoadTable();
				        		CloseDialog(fName);
							}
						}
				    }
			    });
			}

		});

	    $(document).on("click", "#choose_button", function () {
		    $("#choose_file").click();
		});

	    $(document).on("click", "#choose_buttondisabled", function () {
		    alert('თუ გსურთ ახალი სურათის ატვირთვა, წაშალეთ მიმდინარე სურათი!');
		});

	    
	    $(document).on("change", "#choose_file", function () {
	        var file_url  = $(this).val();
	        var file_name = this.files[0].name;
	        var file_size = this.files[0].size;
	        var file_type = file_url.split('.').pop().toLowerCase();
	        var path	  = "../../media/uploads/file/";

	        if($.inArray(file_type, ['png','jpg']) == -1){
	            alert("დაშვებულია მხოლოდ 'png', 'jpg'  გაფართოება");
	        }else if(file_size > '15728639'){
	            alert("ფაილის ზომა 15MB-ზე მეტია");
	        }else{
	            if($("#pers_id").val() == ''){
		            users_id = $("#is_user").val();
	            }else{
	            	users_id = $("#pers_id").val()
	            }
	        	$.ajaxFileUpload({
			        url: "server-side/upload/file.action.php",
			        secureuri: false,
	     			fileElementId: "choose_file",
	     			dataType: 'json',
				    data: {
						act: "file_upload",
						button_id: "choose_file",
						table_name: 'users',
						file_name: Math.ceil(Math.random()*99999999999),
						file_name_original: file_name,
						file_type: file_type,
						file_size: file_size,
						path: path,
						table_id: users_id,

					},
			        success: function(data) {			        
				        if(typeof(data.error) != 'undefined'){
							if(data.error != ''){
								alert(data.error);
							}else{
								$("#upload_img").attr('src','media/uploads/file/'+data.page[0].rand_name);
								$("#upload_img").attr('img',data.page[0].rand_name);
								$('#choose_button').attr('id','choose_buttondisabled');
								$("#delete_image").attr('image_id',data.page[0].id);
								$(".complate").attr('onclick','view_image('+ data.page[0].id + ')');
							}						
						}					
				    }
			    });
	        }
	    });

	    $(document).on("click", "#delete_image", function () {
		    $.ajax({
	            url: aJaxURL,
	            data: "act=delete_image&file_name="+$("#upload_img").attr('img'),
	            success: function(data) {
	               $('#upload_img').attr('src','media/uploads/file/0.jpg');
				   $('#upload_img').attr('img','0.jpg');               
	               $("#choose_button").button();
	               $('#choose_buttondisabled').attr('id','choose_button')
	            }
	        });
		});

		function view_image(id){
			param = new Object();

	        //Action
	    	param.act	= "view_img";
	    	param.id    = $("#pers_id").val();
	    	
			$.ajax({
		        url: aJaxURL,
			    data: param,
		        success: function(data) {
					if(typeof(data.error) != "undefined"){
						if(data.error != ""){
							alert(data.error);
						}else{
							var buttons = {
						        	"cancel": {
							            text: "დახურვა",
							            id: "cancel-dialog",
							            click: function () {
							            	$(this).dialog("close");
							            }
							        }
							    };
							GetDialog("add-edit-form-img", 401, "auto", buttons, 'center top');
							$("#add-edit-form-img").html(data.page);
						}
					}
			    }
		    });
		}
		$(document).on("change", "#branch_id", function () {
			$.ajax({
		        url: aJaxURL,
			    data: 'act=GetServiceCenter&branch_id='+$(this).val(),
		        success: function(data) {
					if(typeof(data.error) != "undefined"){
						if(data.error != ""){
							alert(data.error);
						}else{
						    $('#service_center_id').html(data.page);
						    $('#service_center_id').trigger("chosen:updated");
						}
					}
			    }
		    });
	    });
	    
	    $(document).on("click", "#show_copy_prit_exel", function () {
	        if($(this).attr('myvar') == 0){
	            $('.ColVis,.dataTable_buttons').css('display','block');
	            $(this).css('background','#2681DC');
	            $(this).children('img').attr('src','media/images/icons/select_w.png');
	            $(this).attr('myvar','1');
	        }else{
	        	$('.ColVis,.dataTable_buttons').css('display','none');
	        	$(this).css('background','#E6F2F8');
	            $(this).children('img').attr('src','media/images/icons/select.png');
	            $(this).attr('myvar','0');
	        }
	    });



 /* Enable Event */
 $(document).on("click", "#restore_button", function () {
    	
        var data = $(".check:checked").map(function () {
            return this.value;
        }).get();
    	
		
        for (var i = 0; i < data.length; i++) {
            $.ajax({
                url: aJaxURL,
                type: "POST",
                data: "act=enable&id=" + data[i] ,
                dataType: "json",
                success: function (data) {
	                    if (data.error != "") {
	                        alert(data.error);
	                    } else {
	                    	LoadTable('get_list_archive');
	                        $(".check:checked").attr("checked", false);
	                    }
                }
            });
        }

    });


		$(document).on("click","#current",function(){
			$("#restore_button").css("display","none");
			$("#add_button").css("display","block");
			$("#delete_button").css("display","block");
			LoadTable();
		})

		$(document).on("click","#archive", function(){
			$("#add_button").css("display","none");
			$("#delete_button").css("display","none");
			$("#restore_button").css("display","block");
			LoadTable('get_list_archive');
		})

		function change_btn_color(id) {
            document.getElementById("current").style.backgroundColor = "white";
            document.getElementById("archive").style.backgroundColor = "white";
            document.getElementById(id).style.backgroundColor = "#a9a9a940";
            document.getElementById(id).style.marginBottom = "-2px";
            loadtabs(activep = "", activech = "");

        }
    </script>
	<style>
	.label_td{
		width:170px;
	}
	#info_table input{
		width: 230px !important;
	}
	#info_table select{
		width: 236px !important;
	}
	[aria-describedby="example_info"]{
		width: 100%
	}
    
	</style>
</head>

<body>
<div id="tabs" style="width: 90%; border: 0px solid #aaaaaa;">
	<div class="callapp_head">თანამშრომლები<hr class="callapp_head_hr"></div>
	<table id="table_right_menu" style="top: 80px; left: -1px;">
    	<tr>
    		<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;background:#2681DC;" id="show_table" myvar="0">
    			<img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
    		</td>
    		<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_log" myvar="0">
    			<img alt="log" src="media/images/icons/log.png" height="14" width="14">
    		</td>
    		<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_copy_prit_exel" myvar="0">
    			<img alt="link" src="media/images/icons/select.png" height="14" width="14">
    		</td>
    	</tr>
    </table>
    <ul style="margin-left: -3px">
        <li style="border: none; padding-bottom: 2px">
        	<a onclick="change_btn_color(this.id)" id="current" style="font-family: pvn; background-color:#a9a9a940 !important; margin-bottom: -2px">მიმდინარე</a>
        </li>
        <li style=" border: none;">
        	<a onclick="change_btn_color(this.id)" id="archive" style="font-family: pvn; background-color: white">არქივი</a>
        </li>
    </ul>
    <div style="margin-top: 15px; margin-bottom: 5px; display: flex;">
        <button id="add_button">დამატება</button>
		<button id="restore_button" style="display:none">აღდგენა</button>
        <button id="delete_button" style="margin-left:5px">წაშლა</button>
    </div>

<table class="display" id="example" >
    <thead>
        <tr id="datatable_header">
            <th>ID</th>
            <th style="width:17%">მომხმარებელი</th>
            <th style="width:16%">ფილიალი</th>
            <th style="width:16%">ექსთენშენი</th>
            <th style="width:16%">მობილური</th>
            <th style="width:16%">თანამდებობა</th>
            <th style="width:16%">მისამართი</th>
            <th class="check">#</th>
        </tr>
    </thead>
    <thead>
        <tr class="search_header">
            <th class="colum_hidden">
            	<input type="text" name="search_id" value="ფილტრი" class="search_init" />
            </th>
            <th>
                <input type="text" name="search_name" value="ფილტრი" class="search_init" />
            </th>
            <th>
                <input type="text" name="search_tin" value="ფილტრი" class="search_init" />
            </th>
            <th>
                <input type="text" name="search_position" value="ფილტრი" class="search_init" />
            </th>
            <th>
                <input type="text" name="search_tin" value="ფილტრი" class="search_init" />
            </th>
            <th>
                <input type="text" name="search_position" value="ფილტრი" class="search_init" />
            </th>
            <th>
                <input type="text" name="search_address" value="ფილტრი" class="search_init" />
            </th>
            <th>
            	<div class="callapp_checkbox">
                    <input type="checkbox" id="check-all" name="check-all" />
                    <label for="check-all"></label>
                </div>
            </th>
        </tr>
    </thead>
</table>
</div>

    <!-- jQuery Dialog -->
    <div id="add-edit-form" class="form-dialog" title="თანამშრომლები">
    	<!-- aJax -->
	</div>
    <!-- jQuery Dialog -->
    <div id="image-form" class="form-dialog" title="თანამშრომლის სურათი">
    	<img id="view_img" src="media/uploads/images/worker/0.jpg">
	</div>
	 <!-- jQuery Dialog -->
    <div id="add-group-form" class="form-dialog" title="ჯგუფი">
	</div>
	<div id="add-edit-form-img" class="form-dialog" title="თანამშრომლის სურათი">
	</div>
	
</body>
<style>
.container {
  display: flex;
  align-items: center;
  justify-content:space-between;
}
</style>
</html>