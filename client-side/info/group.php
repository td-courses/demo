<html>
<head>
	<style type="text/css">
	.hidden{
		display : none;
	}
    .high {
        height: 0px;
    }
	</style>
	<script type="text/javascript">
		var aJaxURL	= "server-side/info/group.action.php";		//server side folder url
		var WSDLURL	= "server-side/wsld/wsdl.action.php?act=check_group";		//server side folder url
		var tName	= "example";											//table name
		var fName	= "add-edit-form";										//form name
		var img_name		= "0.jpg";
		var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";	

		$(document).ready(function () {
			// checkGroup();
			LoadTable();

			/* Add Button ID, Delete Button ID */
			GetButtons("add_button", "delete_button");

			SetEvents("add_button", "delete_button", "check-all", tName, fName, aJaxURL);
		});

		function LoadTable(){
			/* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
			GetDataTable(tName, aJaxURL, "get_list", 2, "", 0, "",1,"","",change_colum_main);
			
			setTimeout(function(){$('.ColVis, .dataTable_buttons').css('display','none');}, 90);
		}

		function LoadDialog(){

			GetDialog(fName, 450, "auto", "", "top");
			$("#wellcome_page").chosen();
			$("#wellcome_page_chosen").css("width","250px");
			var group_id = $("#group_id").val();
			GetDataTable("pages", aJaxURL, "get_pages_list&group_id=" + group_id, 2, "", 0, [[-1],["ყველა"]], "", "", "", "250px", "true");
			setTimeout(function(){$('.ColVis, .dataTable_buttons').css('display','none');}, 90);
		}

		function checkGroup(){

			$.ajax({
    	        url: WSDLURL,
    	        success: function(data) {
    				if(typeof(data.error) != "undefined"){
    					if(data.error != ""){
    						alert(data.error);
    					}
    				}
    		    }
    	    });
    	    
		}

		$(document).on("click", "#show_copy_prit_exel", function () {
	        if($(this).attr('myvar') == 0){
	            $('.ColVis,.dataTable_buttons').css('display','block');
	            $(this).css('background','#2681DC');
	            $(this).children('img').attr('src','media/images/icons/select_w.png');
	            $(this).attr('myvar','1');
	        }else{
	        	$('.ColVis,.dataTable_buttons').css('display','none');
	        	$(this).css('background','#E6F2F8');
	            $(this).children('img').attr('src','media/images/icons/select.png');
	            $(this).attr('myvar','0');
	        }
	    });
	    
	    // Add - Save
		$(document).on("click", "#save-dialog", function () {

		    var data = $(".check1:checked").map(function () { //Get Checked checkbox array
		        return this.value;
		    }).get();

			var pages = new Array;

 		    for (var i = 0; i < data.length; i++) {
 		    	pages.push(data[i]);
 		    }

     		param = new Object();
     	    //Action
     		param.act	   = "save_group";
 			param.nam	   = $("#group_name").val();
 			param.pag	   = JSON.stringify(pages);
 			param.group_id = $("#group_id").val();
			param.wellcome_page_id = $("#wellcome_page").val();
 			//var link	=  GetAjaxData(param);

 			if( param.nam == "" ){
 				alert("შეიყვანეთ ჯგუფის სახელი!");
 			}else{
 	    	    $.ajax({
 	    	        url: aJaxURL,
 	    		    data: param,
 	    	        success: function(data) {
 	    				if(typeof(data.error) != "undefined"){
 	    					if(data.error != ""){
 	    						alert(data.error);
 	    					}else{
 	    						$("#add-edit-form").dialog("close");
 	    						LoadTable();
 	    					}
 	    				}
 	    		    }
 	    	    });
 			}


		});

    </script>
</head>

<body>
    <div id="tabs">
		<div class="callapp_head">ჯგუფები<hr class="callapp_head_hr"></div>
    	<div id="button_area">
    		<button id="add_button">დამატება</button>
    		<button id="delete_button">წაშლა</button>
    	</div>
    	<table id="table_right_menu" style="top: 37px;">
            <tr>
            	<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;background:#2681DC;" id="show_table" myvar="0">
            		<img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
            	</td>
                <td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_log" myvar="0">
                	<img alt="log" src="media/images/icons/log.png" height="14" width="14">
                </td>
                <td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_copy_prit_exel" myvar="0">
                	<img alt="link" src="media/images/icons/select.png" height="14" width="14">
                </td>
            </tr>
        </table>
        <table class="display" id="example">
            <thead>
                <tr id="datatable_header">
                    <th>ID</th>
                    <th style="width: 100%">ჯგუფის სახელი</th>
                    <th class="check">#</th>
                </tr>
            </thead>
            <thead>
                <tr class="search_header">
                    <th class="colum_hidden">
                    	<input type="text" name="search_id" value="ფილტრი" class="search_init" />
                    </th>
                    <th>
                        <input type="text" name="search_address" value="ფილტრი" class="search_init" />
                    </th>
                    <th>
                    	<div class="callapp_checkbox">
                            <input type="checkbox" id="check-all" name="check-all" />
                            <label for="check-all"></label>
                        </div>
                    </th>
                </tr>
            </thead>
        </table>
    </div>
            
    <!-- jQuery Dialog -->
    <div id="add-edit-form" class="form-dialog" title="ჯგუფი">
    	<!-- aJax -->
	</div>
    
</body>
</html>