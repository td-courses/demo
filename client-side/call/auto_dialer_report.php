<style>
    .btn {
        width: 120px;
        margin-right: 1%;
        height: 23px;
        border-width: 1px;
        cursor: pointer;
        margin: 0px;
        font-family: pvn;
        display: inline-block;
        line-height: 1.4;
        position: relative;
    }

    i {
        margin-left: 0px;
        text-align: right;
    }

    .row {
        display: flex;

    }


    * {
        box-sizing: border-box
    }

    .container {
        width: 100%;
        background-color: #ddd;
    }

    .skills {
        text-align: right;
        padding: 1px;
        /*color: white;*/
    }

    #example {
        width: 100%;
    }


</style>


<script type="text/javascript">
    var aJaxURL = "server-side/call/auto_dialer_report_action.php";
    var tName = "example";
    var fName = "add-edit-form";
    var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";

    $(document).on("click", "#check-all-details", function () {

        $("#example" + " INPUT[type='checkbox']").prop("checked", $("#check-all-details").is(":checked"));
    });
    $(document).on("click", "#check-all-call", function () {

        $("#example_details" + " INPUT[type='checkbox']").prop("checked", $("#check-all-call").is(":checked"));
    });


    $(document).ready(function () {

        LoadTable();
        SetEvents("", "", "check-all", tName, fName, aJaxURL, "");


    });
    function LoadDialog(form) {
        var button_modal = {

            "cancel": {
                text: "დახურვა",
                id: "cancel-dialog",
                click: function () {
                    $(this).dialog("close");
                }
            }
        }
        GetDialog(fName, 1100, "auto", button_modal, "");
        $.ajax({
            url: aJaxURL,
            type: "POST",
            data: "act=data_length&id=" + $("#details_id").val(),
            dataType: "json",
            success: function (data) {
                if (data.error != "") {
                    alert(data.error);


                }else {

                    LoadTable_details(data.data_length);
                }
            }
        });


    }

    function LoadTable_details(c) {

        /* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
        GetDataTable("example_call", aJaxURL, "get_list_details", c, "details_id=" + $("#details_id").val()+"&id=" + $("#id").val()+"&c="+c, 0, "", 1, "desc", "", '');

        setTimeout(function () {
            $('.ColVis, .dataTable_buttons').css('display', 'none');
        }, 90);
    }

    function LoadTable() {

        /* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
        GetDataTable(tName, aJaxURL, "get_list", 9, "", 0, [[10, 30, 50, -1], [10, 30, 50, "ყველა"]], 0, "desc", "", change_colum_main);

        setTimeout(function () {
            $('.ColVis, .dataTable_buttons').css('display', 'none');
        }, 90);
    }

    $(document).on("click", "#play_btn", function () {

        var data = $(".check:checked").map(function () {
            return this.value;
        }).get();


        for (var i = 0; i < data.length; i++) {
            $.ajax({
                url: aJaxURL,
                type: "POST",
                data: "act=play&id=" + data[i],
                dataType: "json",
                success: function (data) {
                    if (data.error != "") {
                        alert(data.error);

                        LoadTable();
                        $("#" + check).attr("checked", false);

                    }
                }
            });
        }

    });

    $(document).on("click", "#stop_btn1", function () {

        var data = $(".check:checked").map(function () {
            return this.value;
        }).get();


        for (var i = 0; i < data.length; i++) {
            $.ajax({
                url: aJaxURL,
                type: "POST",
                data: "act=paus&id=" + data[i],
                dataType: "json",
                success: function (data) {
                    if (data.error != "") {
                        alert(data.error);

                        LoadTable();
                        $("#" + check).attr("checked", false);

                    }
                }
            });
        }

    });

    $(document).on("click", "#stop_btn", function () {

        var data = $(".check:checked").map(function () {
            return this.value;
        }).get();


        for (var i = 0; i < data.length; i++) {
            $.ajax({
                url: aJaxURL,
                type: "POST",
                data: "act=stop&id=" + data[i],
                dataType: "json",
                success: function (data) {
                    if (data.error != "") {
                        alert(data.error);

                        LoadTable();
                        $("#" + check).attr("checked", false);

                    }
                }
            });
        }

    });
    $(document).on("click", "#delete_main_table", function () {

        var data = $(".check:checked").map(function () {
            return this.value;
        }).get();


        for (var i = 0; i < data.length; i++) {
            $.ajax({
                url: aJaxURL,
                type: "POST",
                data: "act=disable&id=" + data[i],
                dataType: "json",
                success: function (data) {
                    if (data.error != "") {
                        alert(data.error);

                        LoadTable();
                        $("#" + check).attr("checked", false);

                    }
                }
            });
        }

    });

</script>

<body>
<div class="callapp_head" style="margin-left: 1.2%; margin-top: 10px">რეპორტი
    <hr class="callapp_head_hr">
</div>
<div style=" margin-top: 10px; margin-left: 0% ">

    <div id="button_area" class="column" style=" margin-left: 1.2%; ">
        <table>
            <tbody>
            <tr>

                <td>
                    <button id="stop_btn1" class="btn btn-sm"
                            style="border-color: orange; color: orange; background-color: white; font-weight: 400; margin-bottom:30px ">
                        <img
                                src="media\images\paus_outgoing.png"
                                height="15"
                                width="15"> დაპაუზება
                    </button>
                </td>
                <td>
                    <button id="play_btn" class="btn btn-sm"
                            style="border-color: green; color: green; background-color: white; font-weight: 400; margin-bottom:30px ">
                        <img
                                src="media\images\play_outgoing.png"
                                height="15"
                                width="15"> დაწყება
                    </button>
                </td>
                <td>
                    <button class="btn btn-sm" id="stop_btn"
                            style="border-color: red; color: red; background-color: white; font-weight: 400">
                        <i><img
                                    src="media\images\stop_outgoing.png"
                            " height="15" width="15"></i>
                        შეწყვეტა
                    </button>
                </td>
                <td>
                    <button id="delete_main_table" class="btn btn-sm"
                            style="border-color: black; background-color: white; font-weight: 400">
                        <i><img
                                    src="media\images\trash_outgoing.png"
                            " height="15" width="15"></i>
                        წაშლა
                    </button>
                </td>



            </tr>
            </tbody>
        </table>


    </div>


    <div id="tab" style="margin-left: 1.2%">
        <table class="display" id="example">
            <thead>
            <tr id="datatable_header">

                <th></th>
                <th style="width: 10%">დამატების თარიღი</th>
                <th style="width: 15%">კამპანიის დასახელება</th>
                <th style="width: 5%">კამპანიის კოდი</th>
                <th style="width: 20%">კამპანიის ტიპი</th>
                <th style="width: 30%">კომენტარი</th>
                <th style="width: 10%">პროგრესი</th>
                <th style="width: 5%">რაოდენობა</th>
                <th style="width: 5%">სტატუსი</th>

                <th class="check">#</th>
            </tr>
            </thead>
            <thead>
            <tr class="search_header">
                <th class="colum_hidden">
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>

                <th>
                    <div class="callapp_checkbox">
                        <input type="checkbox" id="check-all-details" name="check-all-details"/>
                        <label for="check-all-details"></label>
                    </div>
                </th>
            </tr>
            </thead>
        </table>

    </div>


</div>

<div id="add-edit-form" class="form-dialog" title="ნომრები"></div>
</body>