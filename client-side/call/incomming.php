<?php
require_once("AsteriskManager/config.php");
include("AsteriskManager/sesvars.php");

if (isset($_SESSION['QSTATS']['hideloggedoff'])) {
	$ocultar = $_SESSION['QSTATS']['hideloggedoff'];
} else {
	$ocultar = "false";
}

?>

<head>
	<link href="media/fontawesome-free-5.6.3-web/css/all.css" rel="stylesheet">
	<script type="text/javascript" src="js/MyWebSocket.js?v=1"></script>
	<script type="text/javascript" src="js/jquery.timeago.js"></script>


	<style type="text/css">
		#cancel-dialog {
			float: left !important;
		}

		#ipaddres_block {
			float: left !important;
		}

		#num_block {
			float: left !important;
		}

		#send_chat_program {
			margin-left: 10px;
			width: 35px;
			height: 35px;
			border-radius: 50%;
			margin-top: 10px;
			border: solid 1px #7bb4aa;
			box-shadow: none;
			background-color: #7bb4aa;
			background-image: url(media/images/icons/send_fff.png);
			background-repeat: no-repeat, repeat !important;
			background-position: center !important;
			background-repeat: no-repeat !important;
			background-size: cover !important;
			background-size: 50% !important;
			transition: 0.2s;
			position: absolute;
			right: 1px;
			bottom: 1px;
			cursor: pointer;
		}

		.my_mail_file {
			position: relative;
			display: flex;
			padding: 5px 10px 4px 15px;
			border: 1px solid #AAA;
			border-radius: 30px 12px;
			margin: 5px;
			box-shadow: 8px 7px 7px -5px #AAA;
			height: 50px;
		}

		.delete_upload_file {
			position: absolute;
			padding: 0px 4px;
			border-radius: 50%;
			background-color: #A3D0E4;
			color: #ffffff;
			font-size: 12px;
			cursor: pointer;
			z-index: 999;
			right: 5px;
			top: 5px;
		}

		#email_attachment_files {
			overflow: auto;
			height: 0;
			transition: height 0.2s;
			display: flex;
			width: 98%;
			margin-bottom: 10px;
		}

		#email_attachment_files::-webkit-scrollbar-track {
			-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
			background-color: #F5F5F5;
		}

		#email_attachment_files::-webkit-scrollbar {
			height: 7px;
			width: 7px;
			background-color: #F5F5F5;
		}

		#email_attachment_files::-webkit-scrollbar-thumb {
			background-color: #A3D0E4;
			background-image: -webkit-linear-gradient(45deg,
					rgba(255, 255, 255, .2) 25%,
					transparent 25%,
					transparent 50%,
					rgba(255, 255, 255, .2) 50%,
					rgba(255, 255, 255, .2) 75%,
					transparent 75%,
					transparent)
		}

		#send_message::-webkit-scrollbar-track {
			-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
			background-color: #F5F5F5;
		}

		#send_message::-webkit-scrollbar {
			height: 7px;
			width: 7px;
			background-color: #F5F5F5;
		}

		#send_message::-webkit-scrollbar-thumb {
			background-color: #A3D0E4;
			background-image: -webkit-linear-gradient(45deg,
					rgba(255, 255, 255, .2) 25%,
					transparent 25%,
					transparent 50%,
					rgba(255, 255, 255, .2) 50%,
					rgba(255, 255, 255, .2) 75%,
					transparent 75%,
					transparent)
		}

		#paste_files1 div,
		#paste_files1 a {
			min-height: 41px;
			overflow: auto;
		}

		#ul_tab li {
			display: inline;
			margin: 13px;
			color: #0b5c9a;
			border-bottom: 1px solid gray;
			padding-bottom: 7px;
			cursor: pointer;
		}

		#ul_tab {
			margin-left: -45px;
		}

		#ul_tab a {
			color: gray;
			font-family: pvn;
		}

		.search_button {
			padding: 2px;
			border-radius: 0px;
			background: #009688;
			cursor: pointer;
			float: left;
		}

		#zomm_div {
			float: right;
			cursor: pointer;
			background: #C8D6DC;
			border-radius: 50%;
			padding: 5px;
			border: 1px solid silver;
			margin-bottom: 10px;
			font-weight: bold;
		}

		.fabtext {
			width: 8px;
			padding: 3px;
			text-align: center;
			border: solid 1px #ccc;
			font-size: 8px;
			border-radius: 50%;
			display: block;
			position: absolute;
			top: -7px;
			right: -7px;
			padding-right: 8px !important;
			z-index: 1 !important;
		}

		#table_index tr td:nth-child(11) {
			padding: 0 0 !important;
		}

		#flesh_panel {
			right: 0px !important;
		}

		#incoming_chat_tabs {
			boreder: none;
		}

		#incoming_chat_tabs .ui-tabs-panel {
			padding: 0;
		}

		/* .hovered_image:hover{ */
		/* 	position: absolute; */
		/*     width: 60px; */
		/*     height: 60px; */
		/*     margin: -30px 2px; */
		/* } */
		.hovered_image {
			transition: transform .2s;
			display: block;
			border-radius: 50%;
			margin: auto;
			width: 20px;
		}

		.download {
			background-color: #2dc100;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 12px;
			border: 0px !important;
			text-decoration: none;
			text-overflow: ellipsis;
			width: 100%;
			font-weight: normal !important;
		}

		.download1 {
			background-color: #ce8a14;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 14px;
			border: 0px !important;
			text-decoration: none;
			text-overflow: ellipsis;
			width: 100%;
			font-weight: normal !important;
		}

		[name="task_table_length"] {
			position: absolute;
			top: 32px;
		}

		.download2 {
			background-color: #d35454;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 12px;
			border: 0px !important;
			text-decoration: none;
			text-overflow: ellipsis;
			width: 100%;
			font-weight: normal !important;
			height: 22px;
		}

		.download4 {
			background-color: #2dc100;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 12px;
			text-decoration: none;
			border: 0px !important;
			text-overflow: ellipsis;
			width: 100%;
			font-weight: normal !important;
		}

		#right_side {

			position: absolute;
			right: 13px;
			top: 55px;
			z-index: 0;

		}

		.download3 {
			background-color: #ce8a14;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 12px;
			border: 0px !important;
			text-decoration: none;
			text-overflow: ellipsis;
			width: 100%;
			font-weight: normal !important;
		}

		.download5 {
			background-color: #4980AF;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 12px;
			border: 0px !important;
			text-decoration: none;
			text-overflow: ellipsis;
			width: 100%;
			font-weight: normal !important;
		}

		.download6 {
			background-color: #CE5F14;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 12px;
			border: 0px !important;
			text-decoration: none;
			text-overflow: ellipsis;
			width: 98%;
			font-weight: normal !important;
		}

		.download7 {
			background-color: #f80aea;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 12px;
			border: 0px !important;
			text-decoration: none;
			text-overflow: ellipsis;
			width: 100%;
			font-weight: normal !important;
		}

		.download8 {
			background-color: #f80aea;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 12px;
			border: 0px !important;
			text-decoration: none;
			text-overflow: ellipsis;
			width: 100%;
			font-weight: normal !important;
		}

		.download9 {
			background-color: #D1EC38;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 12px;
			border: 0px !important;
			text-decoration: none;
			text-overflow: ellipsis;
			width: 100%;
			font-weight: normal !important;
		}

		.download10 {
			background-color: #f29d09;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 12px;
			border: 0px !important;
			text-decoration: none;
			text-overflow: ellipsis;
			width: 100%;
			font-weight: normal !important;
		}

		.download11 {
			background-color: #2196f3;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 12px;
			border: 0px !important;
			text-decoration: none;
			text-overflow: ellipsis;
			width: 100%;
			font-weight: normal !important;
		}

		.download12 {
			background-color: #48a7f3;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 12px;
			border: 0px !important;
			text-decoration: none;
			text-overflow: ellipsis;
			width: 100%;
			font-weight: normal !important;
		}

		.download13 {
			background-color: #030202;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 12px;
			border: 0px !important;
			text-decoration: none;
			text-overflow: ellipsis;
			width: 100%;
			font-weight: normal !important;
		}

		.download14 {
			background-color: #089b98;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 12px;
			border: 0px !important;
			text-decoration: none;
			text-overflow: ellipsis;
			width: 100%;
			font-weight: normal !important;
		}

		#chatcontent a {
			color: #AAA;
			text-decoration: none;
			cursor: pointer;
		}

		#chatcontent a:hover {
			color: #333;

		}

		.download15 {
			background-color: #e81d42;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 12px;
			border: 0px !important;
			text-decoration: none;
			text-overflow: ellipsis;
			width: 100%;
			font-weight: normal !important;
		}

		#chat_count {
			-webkit-transition: background 1s;
			-moz-transition: background 1s;
			-o-transition: background 1s;
			transition: background 1s;
		}

		.hidden {
			display: none;
		}

		#chat_status {
			width: 135px;
			height: 26px;
			padding: 0px 0px 4px 0px;
			border-radius: 7px;
			background-repeat: no-repeat !important;
			padding-left: 20px !important;
			background-position-x: 5px !important;
			background-position-y: 4px !important;
			background-size: 15px 15px !important;
			border-radius: 7px;
			padding-top: 6px;
			float: right;
			font-family: pvn;
			font-weight: bold;
		}

		#chat_status[data-status="on"] {
			background: url(media/images/icons/green_status.png);
		}

		#chat_status[data-status="off"] {
			background: url(media/images/icons/red_status.png);
		}

		#chat_user {
			width: 82px;
			display: block;
			font-weight: bold;
			font-size: 12px;
			margin-left: 5px;
			overflow: hidden;
			text-overflow: ellipsis;
			white-space: nowrap;
		}

		#table_index td:nth-child(10) {
			padding: 0px;
		}

		.sms-template-table-wrapper {
			width: 100%;
			max-height: 500px;
			overflow: hidden;
			overflow-y: scroll;
		}

		/*choose activitie wrapper*/
		.callapp-op-activitie-wrapper {
			display: grid;
			width: 190px;
			height: 26px;
			float: right;
			grid-template-columns: 26px auto 26px;
			border: 1px solid grey;
			border-radius: 5px;
		}

		.callapp-op-activitie-color-holder {
			display: flex;
			align-items: center;
			justify-content: center;
			;
		}

		.callapp-op-activitie-color {
			width: 15px;
			height: 15px;
			border-radius: 100%;
			background: #bdc3c7;
		}

		.callapp-op-activitie-choose-wrapper {
			position: relative;
		}

		.callapp-op-activitie-choose-receiver {
			display: flex;
			height: 33px;
			align-items: center;
			justify-content: flex-start;
			font-family: pvn;
			font-size: 14px;
			color: black;
			cursor: default;
			user-select: none;
		}

		#callapp_op_activitie_choose {
			width: 190px;
			position: absolute;
			top: 28px;
			left: -26px;
			background: white;
			border: 1px solid #2980b9;
			list-style: none;
			padding: 0;
			z-index: 999999;
			visibility: hidden;
			transform: scale(.8);
			opacity: 0;
			transition: transform .2s ease, opacity .1s ease;
		}

		@keyframes loader-animation {
			0% {
				left: -100%;
			}

			100% {
				left: 100%;
			}
		}

		.progress {
			height: 5px;
			width: 100%;
		}

		.progress .progress-bar {
			width: 100%;
			position: absolute;
			height: 5px;
			background-color: dodgerblue;
			animation-name: loader-animation;
			animation-duration: 1.5s;
			animation-iteration-count: infinite;
			animation-timing-function: ease-in-out;
		}

		#callapp_op_activitie_choose.on {
			visibility: visible;
			opacity: 1;
			transform: scale(1);
		}

		#callapp_op_activitie_choose li {
			display: flex;
			padding: 5px;
			box-sizing: border-box;
			border-bottom: 1px solid #bdc3c7;
			cursor: default;
		}

		#callapp_op_activitie_choose li.is-active {
			background: #3498db;
		}

		#callapp_op_activitie_choose li:last-child {
			border-bottom: none;
		}

		#callapp_op_activitie_choose li:hover {
			background: #3498db;
		}

		#callapp_op_activitie_choose li .activitie-name {
			font-family: pvn;
			font-size: 13px;
			color: black;
			letter-spacing: .3px;
		}

		#callapp_op_activitie_choose li.is-active .activitie-name {
			color: #ecf0f1;
		}

		#callapp_op_activitie_choose li:hover .activitie-name {
			color: #ecf0f1;
		}

		.activitie-color-block {
			width: 15px;
			height: 15px;
			margin-right: 5px;
		}

		.choose_shablon {
			background: #A3D0E4;
			padding: 2px;
			width: 100%;
			font-size: 13px;
			cursor: pointer;
		}

		#dialog_emojis {
			position: absolute;
			cursor: pointer;
			bottom: 10px;
			right: 50px;
			background: transparent;
		}

		#emoji_close {
			float: left;
			font-size: 15px;
			cursor: pointer;
			color: red;
			width: -webkit-fill-available;
			text-align: center;
			padding: 5px;
			border: 1px solid #CCC;
			margin-bottom: 5px;
			background: #EEE;
		}

		#emoji_wrapper {
			height: 0;
			width: 100%;
			position: absolute;
			z-index: 20;
			overflow: auto;
			transition: height 0.2s;
			background: #EEE;
		}

		#emoji_wrapper::-webkit-scrollbar-track {
			-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
			background-color: #F5F5F5;
		}

		#emoji_wrapper::-webkit-scrollbar {
			height: 7px;
			width: 7px;
			background-color: #F5F5F5;
		}

		#emoji_wrapper::-webkit-scrollbar-thumb {
			background-color: #A3D0E4;
			background-image: -webkit-linear-gradient(45deg,
					rgba(255, 255, 255, .2) 25%,
					transparent 25%,
					transparent 50%,
					rgba(255, 255, 255, .2) 50%,
					rgba(255, 255, 255, .2) 75%,
					transparent 75%,
					transparent)
		}


		#chat_scroll::-webkit-scrollbar-track {
			-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
			background-color: #F5F5F5;
		}

		#chat_scroll::-webkit-scrollbar {
			height: 7px;
			width: 7px;
			background-color: #F5F5F5;
		}

		#chat_scroll::-webkit-scrollbar-thumb {
			background-color: #A3D0E4;
			background-image: -webkit-linear-gradient(45deg,
					rgba(255, 255, 255, .2) 25%,
					transparent 25%,
					transparent 50%,
					rgba(255, 255, 255, .2) 50%,
					rgba(255, 255, 255, .2) 75%,
					transparent 75%,
					transparent)
		}

		#emoji_wrapper span {
			font-size: 15px;
			cursor: pointer;
			line-height: 30px;
		}

		.callapp-op-activitie-choose-togbutton {
			overflow: hidden;
		}

		#input {
			width: 400px;
			height: 200px;
			overflow: auto;
			margin: 5px;
			border: 1px solid #85b1de;
			padding: 30px;
		}


		#input::-webkit-scrollbar-track {
			-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
			background-color: #F5F5F5;
		}

		#input::-webkit-scrollbar {
			height: 7px;
			width: 7px;
			background-color: #F5F5F5;
		}

		#input::-webkit-scrollbar-thumb {
			background-color: #85b1de;
			background-image: -webkit-linear-gradient(45deg,
					rgba(255, 255, 255, .2) 25%,
					transparent 25%,
					transparent 50%,
					rgba(255, 255, 255, .2) 50%,
					rgba(255, 255, 255, .2) 75%,
					transparent 75%,
					transparent)
		}

		.shablon_scroll::-webkit-scrollbar-track {
			-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
			background-color: #F5F5F5;
		}

		.shablon_scroll::-webkit-scrollbar {
			height: 7px;
			width: 7px;
			background-color: #F5F5F5;
		}

		.shablon_scroll::-webkit-scrollbar-thumb {
			background-color: #A3D0E4;
			background-image: -webkit-linear-gradient(45deg,
					rgba(255, 255, 255, .2) 25%,
					transparent 25%,
					transparent 50%,
					rgba(255, 255, 255, .2) 50%,
					rgba(255, 255, 255, .2) 75%,
					transparent 75%,
					transparent)
		}

		.callapp-op-activitie-choose-togbutton button {
			width: 25px;
			height: 26px;
			background: none;
			border: none;
			border-top-right-radius: 10px;
			border-bottom-right-radius: 10px;
			position: relative;
			top: -1px;
			font-size: 18px;
			color: grey;
			box-sizing: border-box;
			padding-top: 6px;
		}

		.callapp-op-activitie-wrapper:hover button {
			background: #dfe4ea;
		}

		.callapp-op-activitie-choose-togbutton button i {
			color: red;
		}

		.red-color {
			color: red;
		}

		.file_text {
			width: max-content;
			display: flex;
			margin-left: 10px;
		}

		.hero-image {

			height: 500px;
			/* You must set a specified height */
			background-position: center;
			/* Center the image */
			background-repeat: no-repeat;
			/* Do not repeat the image */
			background-size: cover;
			/* Resize the background image to cover the entire container */
			margin: 5px;
			width: 40px;
			height: 40px;
		}
	</style>
	<?PHP if ($_SESSION['chat_off_on'] == '') {
		$_SESSION['chat_off_on'] = 1;
	} ?>
	<script type="text/javascript">
		var all_chats = 0;
		var micr_val = 0;
		var aJaxURL = "server-side/call/incomming.action.php"; //server side folder url
		var aJaxURLsport = "server-side/call/action/action.action4.php";
		var aJaxURL_crm = "server-side/call/auto_dialer_documents.action.php";
		var aJaxURL_crm_file = "server-side/call/auto_dialer_file.action.php";
		var seoyURL = "server-side/seoy/seoy.action.php";
		var upJaxURL = "server-side/upload/file.action.php";
		var wsdl_url = "includes/search_client_service.php";
		var wsdl_url_log = "includes/search_client_log.php";
		var asterisk_url = "server-side/asterisk/asterisk.action.php";
		var aJaxURL_task = "server-side/call/incomming/incomming_task.action.php";
		var aJaxURL_mail = "server-side/call/Email_sender.action.php";
		var aJaxURL_getmail = "includes/send_mail.php";
		var aJaxURL_answer = "server-side/call/chat_answer.action.php";
		var aJaxURL_sms = "includes/sendsms.php";
		var tName = "table_index"; //table name
		var fName = "add-edit-form";
		var tbName = "tabs"; //form name
		var file_name = '';
		var rand_file = '';
		var tab = 0;
		var fillter = '';
		var fillter_all = '';
		var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
		var sites;
		//////////////////
		function loadDndData() {
			param = new Object();
			param.act = "check_user_dnd_on_local";

			$.ajax({
				url: aJaxURL,
				data: param,
				success: function(data) {
					if (typeof(data.error) != 'undefined') {
						if (data.error != '') {} else {
							if (data.status == 0) {
								$("#incallToggleDnd").attr('src', 'media/images/icons/comunication/Phone_ON.png');
							} else if (data.status == 1) {
								$("#incallToggleDnd").attr('src', 'media/images/icons/comunication/Phone_INSIDE.png');
							} else if (data.status == 2) {
								$("#incallToggleDnd").attr('src', 'media/images/icons/comunication/Phone_OFF.png');
							}

							if (data.muted == 1) {
								$('#micr').attr('src', 'media/images/icons/5.png');
								$(this).attr('value', '1');
								micr_val = 1;
							} else {
								$('#micr').attr('src', 'media/images/icons/4.png');
								$(this).attr('value', '0');
								micr_val = 0;
							}

							if (data.queries_unrid_count > 0) {
								$("#client_conversation_count_hint").html(data.queries_unrid_count);
								$("#client_conversation_count_hint").css('display', '');
							} else {
								$("#client_conversation_count_hint").html(data.queries_unrid_count);
								$("#client_conversation_count_hint").css('display', 'none');
							}
							setTimeout(loadDndData, 1000);
						}
					}
				}
			});
		}
		//////////////////////////////
		$(document).ready(function() {
			loadDndData();
			check_user_comunication();
			GetButtons("add_button", "delete_button");
			GetDate('start_date');
			GetDate('end_date');
			$('#operator_id,#tab_id,#user_info_id').chosen({
				search_contains: true
			});
			$('.callapp_filter_body').css('display', 'block');
			$.session.clear();
			$.session.set("checker_st", "1");
			runAjax();
			LoadTable();
			GetButtons("add_button", "delete_button", "refresh");
			SetEvents("add_button", "delete_button", "check-all_arq", "table_index", fName, aJaxURL);
			toggleChatStatus('#chat_status', <?PHP echo $_SESSION['chat_off_on']; ?>);
			$(".checkbox_check").css('display', 'none');
			$("#delete_button").css('display', 'none');

			$('#flesh_panel').css('width', '300px');
			$("#show_station").css('background', '#ccc');
			loadDndData1();
		});

		function loadDndData1() {

			// define request data
			const data = {
				act: "load_dnd_data",
				getOptions: $("#callapp_op_activitie_choose").find("li").length === 1
			}

			// send ajax request
			$.post(aJaxURL, data, responce => {

				// restruct responce
				const {
					error,
					options,
					dndStatus
				} = responce;

				// check for error
				if (error !== "") {
					// alert(error);
					alert("We where in Munxern");
				} else {

					// customize dnd button
					if (options !== "") {
						$("#callapp_op_activitie_choose").html(options);
					}

					// make check interval
					setTimeout(loadDndData, 1000);

				}
			});
		}

		function toggleChatStatus(selector, status) {
			$D.sessionStatus = status === 2 ? 1 : 0;
			const dataStatus = status === 2 ? "on" : "off";
			$(selector).val(status).attr("data-status", dataStatus).data("status", dataStatus);
		}


		function displayAmountStandartSubCat(status) {
			$("#card_type_id_holder").css({
				display: status
			});
			$("#for_card_type_id").css({
				display: status
			});
		}

		function displayAmountInOutSubCat(status) {
			$("#for_transaction_date_time").css({
				display: status
			});
			$("#transaction_date_time").css({
				display: status
			});
			$("#for_card_16_number").css({
				display: status
			});
			$("#card_16_number_holder").css({
				display: status
			});
		}


		$(document).on("click", "#filter_dam_shem", function() {
			fillter = $("#filter_dam_shem").val();
			LoadTable();
		});
		$(document).on("click", "#filter_transf_shem", function() {
			fillter = $("#filter_transf_shem").val();
			LoadTable();
		});
		$(document).on("click", "#filter_dam_gam", function() {
			fillter = $("#filter_dam_gam").val();
			LoadTable();
		});
		$(document).on("click", "#filter_transf_gam", function() {
			fillter = $("#filter_transf_gam").val();
			LoadTable();
		});
		$(document).on("click", "#filter_unansver", function() {
			fillter = $("#filter_unansver").val();
			LoadTable();
		});
		$(document).on("click", "#filter_dau_gam", function() {
			fillter = $("#filter_dau_gam").val();
			LoadTable();
		});
		$(document).on("click", "#noansverdone", function() {
			fillter = $("#noansverdone").val();
			LoadTable();
		});
		$(document).on("click", "#filter_dau_shem", function() {
			fillter = $("#filter_dau_shem").val();
			LoadTable();
		});
		$(document).on("click", "#filter_chati1", function() {
			fillter = $("#filter_chati1").val();
			LoadTable();
		});
		$(document).on("click", "#filter_chati2", function() {
			fillter = $("#filter_chati2").val();
			LoadTable();
		});
		$(document).on("click", "#filter_chati3", function() {
			fillter = $("#filter_chati3").val();
			LoadTable();
		});
		$(document).on("click", "#filter_saubr", function() {
			fillter = $("#filter_saubr").val();
			LoadTable();
		});
		$(document).on("click", "#filter_all1", function() {
			fillter = $("#filter_all1").val();
			LoadTable();
			$('#fi_call,#fi_chat').css('display', 'table');
		});
		$(document).on("click", "#filter_all2", function() {
			fillter = $("#filter_all2").val();
			LoadTable();
			$('#fi_chat').css('display', 'none');
			$('#fi_call').css('display', 'table');
		});
		$(document).on("click", "#filter_all3", function() {
			fillter = $("#filter_all3").val();
			LoadTable();
			$('#fi_call').css('display', 'none');
			$('#fi_chat').css('display', 'table');
		});


		$(document).on("click", ".client_history", function() {
			$("#callapp_filter_body_span_input").val($("#s_u_user_id").val());
			GetDate("date_from");
			GetDate("date_to");
			$("#search_client_history, #search_client_orders, #search_client_transaction").button();
			$("#client_history_filter_date, #client_history_filter_activie, #client_history_filter_site, #client_history_filter_group, #client_history_filter_transaction_type").chosen({
				search_contains: true
			});
			$("#client_history_filter_site_chosen").css('width', '135px');
			$("#client_history_filter_group_chosen").css('width', '135px');
			$("#client_history_filter_transaction_type_chosen").css('width', '190px');
			var dLength = [
				[10, 30, 50, -1],
				[10, 30, 50, "ყველა"]
			];
			GetDataTable("example4_0", AJ, "get_list_client_order", 10, "incomming_call_id=" + $("#incomming_call_id").val() + "&history_user_id=" + $("#client_history_user_id").val() + "&client_history_filter_date=" + $("#client_history_filter_date").val() + "&client_history_filter_activie=" + $("#client_history_filter_activie").val(), 0, dLength, 2, "desc", '', "<'F'lip>");
			GetDataTable("example4_1", AJ, "get_list_client_transaction", 10, "incomming_call_id=" + $("#incomming_call_id").val() + "&history_user_id=" + $("#client_history_user_id").val() + "&client_history_filter_site=" + $("#client_history_filter_site").val() + "&client_history_filter_group=" + $("#client_history_filter_group").val() + "&client_history_filter_transaction_type=" + $("#client_history_filter_transaction_type").val(), 0, "", 2, "desc", '', "<'F'lip>");
			$("#example4_0_length").css('top', '0px');
			$("#example4_0_wrapper").css('width', '1000px');
			$("#example4_1_length").css('top', '0px');
			$("#example4_1_wrapper").css('width', '790px');
			setTimeout(function() {
				$('.ColVis, .dataTable_buttons').css('display', 'none');
			}, 90);
			$("#client_history_user_id").val($("#s_u_user_id").val());
		});

		$(document).on("click", "#control_tab_id_4_0", function() {
			var dLength = [
				[10, 30, 50, -1],
				[10, 30, 50, "ყველა"]
			];
			GetDataTable("example4_0", AJ, "get_list_client_order", 10, "incomming_call_id=" + $("#incomming_call_id").val() + "&history_user_id=" + $("#client_history_user_id").val() + "&client_history_filter_date=" + $("#client_history_filter_date").val() + "&client_history_filter_activie=" + $("#client_history_filter_activie").val(), 0, dLength, 2, "desc", '', "<'F'lip>");
			$("#example4_0_length").css('top', '0px');
			$("#example4_0_wrapper").css('width', '1000px');
			setTimeout(function() {
				$('.ColVis, .dataTable_buttons').css('display', 'none');
			}, 90);
		});

		$(document).on("click", "#search_client_orders", function() {
			var dLength = [
				[10, 30, 50, -1],
				[10, 30, 50, "ყველა"]
			];
			GetDataTable("example4_0", AJ, "get_list_client_order", 10, "incomming_call_id=" + $("#incomming_call_id").val() + "&history_user_id=" + $("#client_history_user_id").val() + "&client_history_filter_date=" + $("#client_history_filter_date").val() + "&client_history_filter_activie=" + $("#client_history_filter_activie").val(), 0, dLength, 2, "desc", '', "<'F'lip>");
			$("#example4_0_length").css('top', '0px');
			$("#example4_0_wrapper").css('width', '1000px');
			setTimeout(function() {
				$('.ColVis, .dataTable_buttons').css('display', 'none');
			}, 90);
		});

		$(document).on("click", "#control_tab_id_4_1", function() {
			var dLength = [
				[10, 30, 50, -1],
				[10, 30, 50, "ყველა"]
			];
			GetDataTable("example4_1", AJ, "get_list_client_transaction", 10, "incomming_call_id=" + $("#incomming_call_id").val() + "&history_user_id=" + $("#client_history_user_id").val() + "&client_history_filter_site=" + $("#client_history_filter_site").val() + "&client_history_filter_group=" + $("#client_history_filter_group").val() + "&client_history_filter_transaction_type=" + $("#client_history_filter_transaction_type").val(), 0, dLength, 2, "desc", '', "<'F'lip>");
			$("#example4_1_length").css('top', '0px');
			$("#example4_1_wrapper").css('width', '790px');
			setTimeout(function() {
				$('.ColVis, .dataTable_buttons').css('display', 'none');
			}, 90);
		});

		$(document).on("click", "#search_client_transaction", function() {
			var dLength = [
				[10, 30, 50, -1],
				[10, 30, 50, "ყველა"]
			];
			GetDataTable("example4_1", AJ, "get_list_client_transaction", 10, "incomming_call_id=" + $("#incomming_call_id").val() + "&history_user_id=" + $("#client_history_user_id").val() + "&client_history_filter_site=" + $("#client_history_filter_site").val() + "&client_history_filter_group=" + $("#client_history_filter_group").val() + "&client_history_filter_transaction_type=" + $("#client_history_filter_transaction_type").val(), 0, dLength, 2, "desc", '', "<'F'lip>");
			$("#example4_1_length").css('top', '0px');
			$("#example4_1_wrapper").css('width', '790px');
			setTimeout(function() {
				$('.ColVis, .dataTable_buttons').css('display', 'none');
			}, 90);
		});

		$(document).on("click", "#search_client_history", function() {
			$("#loading_search_my_client_history").show();
			param = new Object();
			param.act = 'search_order';
			param.incomming_call_id = $("#incomming_call_id").val();
			param.client_history_user_id = $("#client_history_user_id").val();
			param.date_from = $("#date_from").val();
			param.date_to = $("#date_to").val();

			$.ajax({
				url: wsdl_url_log,
				data: param,
				success: function(data) {
					GetDataTable("example4_0", AJ, "get_list_client_order", 10, "incomming_call_id=" + $("#incomming_call_id").val() + "&history_user_id=" + $("#client_history_user_id").val() + "&client_history_filter_date=" + $("#client_history_filter_date").val() + "&client_history_filter_activie=" + $("#client_history_filter_activie").val(), 0, "", 2, "desc", '', "<'F'lip>");
					$("#example4_0_length").css('top', '0px');
					$("#example4_0_wrapper").css('width', '1000px');
					parame = new Object();
					parame.act = 'search_transaction';
					parame.incomming_call_id = $("#incomming_call_id").val();
					parame.client_history_user_id = $("#client_history_user_id").val();
					parame.date_from = $("#date_from").val();
					parame.date_to = $("#date_to").val();
					$.ajax({
						url: wsdl_url_log,
						data: parame,
						success: function(data) {
							GetDataTable("example4_1", AJ, "get_list_client_transaction", 10, "incomming_call_id=" + $("#incomming_call_id").val() + "&history_user_id=" + $("#client_history_user_id").val() + "&client_history_filter_site=" + $("#client_history_filter_site").val() + "&client_history_filter_group=" + $("#client_history_filter_group").val() + "&client_history_filter_transaction_type=" + $("#client_history_filter_transaction_type").val(), 0, "", 2, "desc", '', "<'F'lip>");
							$("#example4_1_length").css('top', '0px');
							$("#example4_1_wrapper").css('width', '790px');
							$("#loading_search_my_client_history").hide();
						}
					});
				}
			});
		});

		$(document).on("change, focus", "[data-hendler='error']", function() {
			var inputId = $(this).attr("id");
			$(`label[for="${inputId}"]`).removeClass("red-color");
		});


		$(document).on("change", "#tab_id", function() {
			if ($(this).val() == 5) {
				$(".checkbox_check").css('display', '');
				$("#delete_button").css('display', '');
			} else {
				$(".checkbox_check").css('display', 'none');
				$("#delete_button").css('display', 'none');
			}
		});
		$(document).on("click", "#loadtable", function() {
			LoadTable();
		});

		$(document).on("click", ".callapp_refresh", function() {
			fillter = '';
			LoadTable();
		});
		$(document).on("click", ".comunication_button", function() {
			if ($("#comunication_table_check").val() == 0) {
				$("#comunication_table_check").val(1);
				$("#comucation_status").css('display', '');
			} else {
				$("#comunication_table_check").val(0);
				$("#comucation_status").css('display', 'none');
			}
		});

		function LoadTable() {
			AJ = aJaxURL;
			columcaunt = 13;

			/* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
			GetDataTable("table_index", AJ, "get_list", columcaunt, "start=" + $('#start_date').val() + "&end=" + $('#end_date').val() + "&operator_id=" + $('#operator_id').val() + "&user_info_id=" + $("#user_info_id").val() + "&tab_id=" + $('#tab_id').val() + "&filter_1=" + $('#filter_1:checked').val() + "&filter_2=" + $('#filter_2:checked').val() + "&filter_3=" + $('#filter_3:checked').val() + "&filter_4=" + $('#filter_4:checked').val() + "&filter_5=" + $('#filter_5:checked').val() + "&filter_6=" + $('#filter_6:checked').val() + "&filter_7=" + $('#filter_7:checked').val() + "&filter_8=" + $('#filter_8:checked').val() + "&filter_9=" + $('#filter_9:checked').val() + "&filter_10=" + $('#filter_10:checked').val() + "&filter_11=" + $('#filter_11:checked').val(), 0, "", 2, "desc", '', change_colum_main);
			setTimeout(function() {
				$('.ColVis, .dataTable_buttons').css('display', 'none');

				$('#table_index td:nth-child(10)').css('padding', 0);
				$('#table_index td:nth-child(11)').css('padding', 0);

			}, 90);
		}


		$(document).on("click", ".hide_said_menu", function() {
			$("#right_side fieldset").hide();
			$(".add-edit-form-class").css("width", "560");
			//$('#add-edit-form').dialog({ position: 'top' });
			hide_right_side();
		});
		///////////////////////////////////---------------aqedan---------------------------/////////////////////////////

		$(document).on("click", "#chat_end", function() {
			if ($('#chat_original_id').val() != '0') {
				var buttons = {
					"cancel": {
						text: "არა",
						id: "cancel-dialog",
						click: function() {
							$(this).dialog("close");
							$(this).html("");
						}
					},
					"save": {
						text: "დიახ",
						id: "close_chat_dialog",
						click: function() {
							$.ajax({
								url: aJaxURL,
								data: "act=chat_end&chat_id=" + $('#chat_original_id').val(),
								success: function(data) {
									if (typeof(data.error) != 'undefined') {
										if (data.error != '') {
											// alert(data.error);
											alert("Caucasus Jixv");
										} else {

										}
									}
								}
							});
						}
					}

				};


				/* Dialog Form Selector Name, Buttons Array */
				GetDialog("chat_close_dialog", 400, "auto", buttons);

			}
		});

		$(document).on("click", ".clickmetostart", function() {
			//$("#loading").show();
			if ($('#add-edit-form input').length != 0) {
				//$("#loading").show();	
				all_chats = 0;
				values = $("#my_site").chosen().val();
				param = new Object();

				param.act = "save_incomming";
				param.hidde_incomming_call_id = $("#hidde_incomming_call_id").val();
				param.id = $("#incomming_call_id").val();
				param.phone = $("#phone").val();
				param.ipaddres = $("#ipaddres").val();
				param.call_date = $("#call_date").val();
				param.pin = $("#site_user_pin").val();
				param.hidden_user = $("#hidden_user").val();
				param.chat_id = $('#chat_original_id').val();
				param.source = $('#chat_source').val();
				param.incomming_cat_1 = $("#incomming_cat_1").val();
				param.incomming_cat_1_1 = $("#incomming_cat_1_1").val();
				param.incomming_cat_1_1_1 = $("#incomming_cat_1_1_1").val();
				param.site_id = values;
				param.search_user_id = $("#search_user_id").val();
				param.s_u_mail = $("#s_u_mail").val();
				param.s_u_name = $("#s_u_name").val();
				param.s_u_pid = $("#s_u_pid").val();
				param.s_u_user_id = $("#s_u_user_id").val();
				param.client_sex = $("#client_sex").val();
				param.client_birth_year = $("#client_birth_year").val();
				param.s_u_status = $("#s_u_status").val();
				param.source_id = $('#source_id').val();
				param.inc_status = $("#inc_status").val();
				param.call_content = $("#call_content").val();
				param.lid = $('input[id=lid]:checked').val();
				param.rand_file = rand_file;
				param.file_name = file_name;
				param.hidden_inc = $("#hidden_inc").val();
				param.imchat = $('#imchat').val();
				param.ii = 2;
				var link = GetAjaxData(param);
				$.ajax({
					url: aJaxURL,
					data: link,
					success: function(data) {
						if (typeof(data.error) != 'undefined') {
							if (data.error != '') {
								// alert(data.error);
								alert("Click me to start");
								//$(".save-incomming-main-dialog").attr('disabled', false);

							} else {
								//LoadTable();
							}
						}
					}
				});
			}

			$('#chat_original_id').val($(this).attr('chat_id'));
			var color = $(this).attr('color');
			param = new Object();
			param.act = "get_edit_page";
			param.number = '';
			param.imby = '1';
			param.chat_id = $(this).attr('chat_id');
			param.source = $(this).data('source');

			if (param.source == 'video') {
				call_id = $(this).data('call_id');
				chat_id = $(this).attr('chat_id');
				if (chat_id > 0) {
					newwindow = window.open('https://ccapi.crystalbet.com/videocall/index.html#' + call_id, 'name', 'height=500,width=700');
					if (window.focus) {
						newwindow.focus()
					}
				}
			}

			$.ajax({
				async: true,
				url: aJaxURL,
				data: param,
				success: function(data) {
					if (typeof(data.error) != 'undefined') {
						if (data.error != '') {
							// alert(data.error);
							alert("Check original id");
						} else {

							$("#add-edit-form").html(data.page);
							//$("#loading").show();	
							$.ajax({
								async: true,
								url: aJaxURL,
								data: {
									'act': 'get_all_chat',
									'chat_id': $('#chat_original_id').val(),
									'source': $("#chat_source").val()
								},
								success: function(data) {
									if (typeof(data.error) != 'undefined') {
										if (data.error != '') {

										} else {

											$('#user_block').attr('ip', data.ip);
											$('#user_block').attr('pin', data.pin);
											$('#log').html('');
											$('#log').html(data.sms);
											$('#chat_detail_id').val(data.chat_detail_id);


											$('#chatcontent,#gare_div').css('display', 'block');
											$('#chatistema').css('width', '210px');
											$('.add-edit-form-class').css('width', '1206px');

											$('.add-edit-form-class .ui-widget-header').css('background', color);
											jQuery("time.timeago").timeago();
											$('#send_message').prop('disabled', false);
											$('#blocklogo').css('display', 'table');
											document.getElementById('chat_scroll').scrollTop = 25000000000;
											all_chats = 1;
										}
									}
								}
							});
							runAjax();

						}
					}
				}
			});
		});
		$(document).on("click", ".add-edit-form-class .ui-dialog-titlebar-close", function() {
			$('#add-edit-form').dialog("close");
		});


		$(document).on("click", "#chat_hist", function() {
			$.ajax({
				url: aJaxURL,
				data: {
					'act': 'get_history',
					'ip': $('#user_block').attr('ip'),
					'chat_id': $('#chat_original_id').val(),
					'source': $("#chat_source").val()

				},
				success: function(data) {
					if (typeof(data.error) != 'undefined') {
						if (data.error != '') {

						} else {
							$('#log').html('');
							$('#log').html(data.sms);
							jQuery("time.timeago").timeago();
							document.getElementById('chat_scroll').scrollTop = 25000000000;
							$('#chat_live').css('border-bottom', 'none');
							$('#chat_hist').css('border-bottom', '2px solid #333');
						}
					}
				}
			});
		});
		/////////////////////////////////// aqamde//////////////////////////////////////
		$(document).on("click", "#user_block_save", function() {
			$.ajax({
				url: aJaxURL,
				data: "act=block_ip&ip=" + $("#user_block").attr('ip') + "&pin=" + $("#user_block").attr('pin') + "&chat_comment=" + $('#block_comment').val() + "&chat_id=" + $('#chat_original_id').val(),
				success: function(data) {
					if (typeof(data.error) != 'undefined') {
						if (data.error != '') {

						} else {
							$('#log').html('');
							$('#add_blocked_ip').dialog("close");

						}
					}
				}
			});
		});
		$(document).on("click", "#user_block_pin", function() {
			$.ajax({
				url: aJaxURL,
				data: "act=block_pin&ip=" + $("#user_block").attr('ip') + "&pin=" + $("#user_block").attr('pin') + "&chat_comment=" + $('#block_comment').val() + "&chat_id=" + $('#chat_original_id').val(),
				success: function(data) {
					if (typeof(data.error) != 'undefined') {
						if (data.error != '') {

						} else {
							$('#log').html('');
							$('#add_blocked_ip').dialog("close");

						}
					}
				}
			});
		});
		/////////////////////////////////////////////////////////////////////////////////
		$(document).on("change", "#transfer_resp_ext_id", function() {
			var value = $(this).val();
			$.ajax({
				url: aJaxURL,
				data: "act=get_resp_user&extention=" + value,
				success: function(data) {
					if (typeof(data.error) != 'undefined') {
						if (data.error != '') {

						} else {
							$('#transfer_resp_id').html(data.name);
							$('#transfer_resp_id').attr('val', data.id);
						}
					}
				}
			});
		});
		/////////////////////////////////////////////////////////////////////////////////		
		$(document).on("change", "#check_transfer", function() {
			var value = $('input[name=check_transfer]:checked').val();
			if (value == 1) {
				$("#transfer_resp_ext_id").css('display', '');
				$("#transfer_resp_id").css('display', '');
			} else {
				$("#transfer_resp_ext_id").css('display', 'none');
				$("#transfer_resp_id").css('display', 'none');
			}
		});
		////////////////////////////////////////////////////////////////////////////////
		$(document).on("click", "#user_block", function() {
			if ($("#source_id").val() == 6 || $("#source_id").val() == 7 || $("#source_id").val() == 9 || $("#source_id").val() == 11 || $("#source_id").val() == 12) {
				var buttons = {
					"chat_block_locked": {
						text: "დაბლოკვა",
						id: "chat_block_locked",
					},
					"cancel": {
						text: "დახურვა",
						id: "cancel",
						click: function() {
							$(this).dialog('close');
						}
					}

				};
				GetDialog("numlock_dialog", 280, "auto", buttons);
				$("#numlock_dialog").html('<span>კომენტარი</span><textarea style="width: 245px; height: 80px; resize: none;" id="chat_block_reason" class="idle"></textarea>');
			} else {
				if ($(this).attr('ip') != '') {
					var buttons = {

						"cancel": {
							text: "დახურვა",
							id: "cancel-dialog",
							click: function() {
								$(this).dialog("close");
								$(this).html("");
							}
						},
						"save": {
							text: "IP_ით დაბლოკვა",
							id: "user_block_save"
						},

						"save_pin": {
							text: "PIN_ით დაბლოკვა",
							id: "user_block_pin"
						}

					};
					//////////////////////////////////////////////////////////////////////////

					/* Dialog Form Selector Name, Buttons Array */
					GetDialog("add_blocked_ip", 400, "auto", buttons);
				} else {
					alert('ჯერ მონიშნეთ აქტიური ჩატი!');
				}
			}
		});
		//////////////////////////////////////////////////////////////////////////


		$(document).on("click", "#chat_live", function() {

			$.ajax({
				url: aJaxURL,
				data: {
					'act': 'get_all_chat',
					'chat_id': $('#chat_original_id').val(),
					'source': $("#chat_source").val()
				},
				success: function(data) {
					if (typeof(data.error) != 'undefined') {
						if (data.error != '') {

						} else {

							$('#log').html('');
							$('#log').html(data.sms);
							$('#chat_detail_id').val(data.chat_detail_id);

							document.getElementById('chat_scroll').scrollTop = 25000000000;
							jQuery("time.timeago").timeago();
							$('#chat_hist').css('border-bottom', 'none');
							$('#chat_live').css('border-bottom', '2px solid #333');
							all_chats = 1;

						}
					}
				}
			});
		});

		$(document).on("click", "#num_block", function LoadDialog2() {
			var buttons = {
				"cancel": {
					text: "დახურვა",
					id: "cancel",
					click: function() {
						$(this).dialog("close");
					}
				},
				"num_block_locked": {
					text: "დაბლოკვა",
					id: "num_block_locked",
				}

			};
			GetDialog("numlock_dialog", 280, "auto", buttons);
			$("#numlock_dialog").html('<span>კომენტარი</span><textarea style="width: 245px; height: 80px; resize: none;" id="block_comment2" class="idle"></textarea>');
		});

		function LoadDialog(fNm) {

			$("#loading").show();
			if (fNm == 'add-edit-form') {

				var buttons = {
					"ipaddres_block": {
						text: "IP დაბლოკვა",
						id: "ipaddres_block"
					},
					"num_block": {
						text: "ნომრის დაბლოკვა",
						id: "num_block"
					},
					"cancel": {
						text: "დახურვა",
						id: "cancel-dialog",
						click: function() {
							var buttons1 = {
								"done": {
									text: "კი",
									id: "save-yes",
									click: function() {
										$("#add-edit-form").dialog("close");
										$(this).dialog("close");
									}
								},
								"no": {
									text: "არა",
									id: "save-no",
									click: function() {
										$(this).dialog("close");
									}
								}
							};
							GetDialog("hint_close", 300, "auto", buttons1);
							$("#save-yes").css('float', 'right');
							$("#save-no").css('float', 'right');
						}
					},
					"done": {
						text: "შენახვა",
						id: "save-dialog",
						class: "save-incomming-main-dialog"
					}

				};


				/* Dialog Form Selector Name, Buttons Array */
				GetDialog(fName, 1206, "auto", buttons);
				$(".save-incomming-main-dialog").button();
				$('.add-edit-form-class .ui-dialog-titlebar-close').remove();
				$('.add-edit-form-class').css('left', '84.5px');
				$('.add-edit-form-class').css('top', '0px');
				$('.ui-dialog-buttonset').css('width', '100%');
				$("#cancel-dialog").css('float', 'right');
				$("#save-dialog").css('float', 'right');
				$("#search_ab_pin").button();
				$('#add_comment').button();
				$("#incomming_cat_1, #incomming_cat_1_1, #incomming_cat_1_1_1, #my_site_resume, #my_site, #source_id, #inc_status, #s_u_status, #client_sex").chosen({
					search_contains: true
				});
				var dLength = [
					[5, 10, 30, -1],
					[5, 10, 30, "ყველა"]
				];
				$("#my_site_resume_chosen").css('width', '560px');
				GetDate('start_check');
				GetDate('end_check');
				GetDataTable("table_history", aJaxURL, "get_list_history", 8, "&start_check=" + $('#start_check').val() + "&end_check=" + $('#end_check').val() + "&phone=" + $("#phone").val() + "&s_u_user_id=" + $("#s_u_user_id").val(), 0, dLength, 1, "desc", '', "<'F'lip>");

				$("#table_history_length").css('top', '0px');
				$("#table_sms_length").css('top', '0px');

				$(".jquery-ui-button").button();

				$("#call_content").off();
				$("#call_content1").off();
				$("#send_message").off();

				sites = $("#my_site").chosen().val();

				$("#call_content").autocomplete("server-side/seoy/seoy_textarea.action.php?source_id=" + $("#source_id").val() + "&site=" + $("#my_site").chosen().val(), {
					width: 300,
					multiple: true,
					matchContains: true
				});
				$("#call_content1").autocomplete("server-side/seoy/seoy_textarea1.action.php?source_id=" + $("#source_id").val() + "&site=" + $("#my_site").chosen().val(), {
					width: 300,
					multiple: true,
					matchContains: true
				});

				$("#send_message").autocomplete("server-side/seoy/chat_textarea.action.php?source_id=" + $("#source_id").val() + "&site=" + $("#my_site").chosen().val(), {
					width: 360,
					multiple: true,
					matchContains: true
				});

				//////////
				var id = $("#incomming_id").val();
				var cat_id = $("#category_parent_id").val();

				if (id != '' && cat_id == 407) {
					$("#additional").removeClass('hidden');
				}
				if ($('#ipaddres').val() == 0 || $('#ipaddres').val() == '') {
					$('#ipaddres_block').css('display', 'none');
				}
				GetDateTimes("problem_date");
				GetDateTimes("transaction_date_time");


				if ($("#tab_id").val() == 5) {
					$("#num_block, .save-incomming-main-dialog").css('display', 'none');
				} else {
					$("#num_block, .save-incomming-main-dialog").css('display', '');
				}


				if (($('#imchat').val() == 1 && $('#ast_source').val() == '')) {
					$('#source_id').val(4);
					$('#source_id').trigger("chosen:updated");
					$('.add-edit-form-class .ui-dialog-title').html('შემომავალი ჩატი');
					$('#num_block').css('display', 'none');
				} else if (($('#site_chat').val() > 0 && $('#ast_source').val() == '')) {
					$('#source_id').val(9);
					$('#source_id').trigger("chosen:updated");
					$('.add-edit-form-class .ui-dialog-title').html('შემომავალი ჩატი');
					$('#num_block').css('display', 'none');
				} else if (($('#fb_chat').val() > 0 && $('#ast_source').val() == '')) {
					$('#source_id').val(6);
					$('#source_id').trigger("chosen:updated");
					$('.add-edit-form-class .ui-dialog-title').html('შემომავალი ჩატი');
					$('#num_block').css('display', 'none');
				} else if (($('#mail_chat').val() > 0 && $('#ast_source').val() == '')) {
					$('#source_id').val(7);
					$('#source_id').trigger("chosen:updated");
					$('.add-edit-form-class .ui-dialog-title').html('შემომავალი ჩატი');
					$('#num_block').css('display', 'none');
				} else if (($('#video_chat_id').val() > 0 && $('#ast_source').val() == '')) {
					$('#source_id').val(12);
					$('#source_id').trigger("chosen:updated");
					$('#chatcontent,#gare_div').css('display', 'none');
					$('.add-edit-form-class .ui-dialog-title').html('ვიდეო ზარი');
					$('#chatistema').css('width', '210px');
					$('.add-edit-form-class').css('width', '879px');
					$("#cancel-chat").css('display', 'none');
					$('#num_block').css('display', 'none');
				} else {
					//$('#source_id option:eq(1)').prop('selected', true);
					$('#chatcontent,#gare_div').css('display', 'none');
					$('#chatistema').css('width', '210px');
					$('.add-edit-form-class').css('width', '879px');
					$("#cancel-chat").css('display', 'none');
					$('.add-edit-form-class .ui-dialog-title').html('ზარის დამუშავება');
					$('.add-edit-form-class .ui-dialog-titlebar').css('background', '#673AB7');
					$('.add-edit-form-class .ui-dialog-title').css('color', '#fff');
				}

				$('#send_message').keyup(function(e) {
					console.log("click");
					if ($("#source_id").val() == 4) {

						if (e.key == 8) {

							param = new Object();
							param.act = "focusout";
							param.id = $("#chat_original_id").val();

							$.ajax({
								url: aJaxURL,
								data: param,
								success: function(data) {}
							});
						} else {
							param = new Object();
							param.act = "focusin";
							param.id = $("#chat_original_id").val();

							$.ajax({
								url: aJaxURL,
								data: param,
								success: function(data) {}
							});
						}
					}
				});

				jQuery("time.timeago").timeago();
				//getchats();
				$('#choose_button').button();


				///////////////////
				// ai dunia      //
				///////////////////
				if ($('#add-edit-form input').length != 0) {

					/*
	            $.ajax({
			        url: "server-side/call/getchats.action.php",
				    data: "act=getchats",
				    dataType: "json",
			        success: function(data) {
				        if(data.count != 0 && $D.sessionStatus == 1){
				        	$('#chat_count').css('background','#FFCC00');
				        	setTimeout(function(){
				        		$('#chat_count').css('background','#F44336');
				    	    	}, 400);
				        	var audio = new Audio('sms.mp3');
				        	if(micr_val == 0){
								audio.play();
					        	}
				        }else{
				        	$('#chat_count').css('background','none');
				        }
			        	
						$("#incoming_chat_tab_chat").html(data.page.chat);
						$("#incoming_chat_tab_site").html(data.page.site);
						$("#incoming_chat_tab_mail").html(data.page.mail);
						$("#incoming_chat_tab_fbm").html(data.page.fbm);			     
			        	$('#chat_count').attr('first_id', data.first_id);
			        }
		    	});
				*/
					if ($('#chat_user_id').attr('ext') != '0') {
						$.ajax({
							url: "AsteriskManager/checkme.php",
							data: "ext=" + $('#chat_user_id').attr('ext'),
							dataType: "json",
							success: function(data) {
								//console.log(data.phone);
								$('#chat_call_live').attr('src', 'media/images/icons/' + data.icon);
								if (data.phone != '' && data.phone != null) {
									if (data.ipaddres == '2004') {
										iporphone = data.ipaddres;
									} else {
										iporphone = data.phone;
									}
									$('#chat_call_queue').css('display', 'table-row');
									$('#chat_call_number').html(iporphone);
									$('#chat_call_duration').html(data.duration);
									$('#chat_call_number').attr('extention', $('#chat_user_id').attr('ext'));
									$('#chat_call_number').attr('number', data.phone);
									$('#chat_call_number').attr('ipaddres', data.ipaddres);
								} else {
									$('#chat_call_queue').css('display', 'none');
									$('#chat_call_number').html('');
									$('#chat_call_duration').html('');
								}
							}
						});
					} else {
						$('#main_call_chat').css('display', 'none');
					}
				}
				all_chats = 1;
				$("#incoming_chat_tabs").tabs();

				////////////////////
			} else if (fNm == 'crm_dialog') {
				var buttons = {
					"cancel": {
						text: "დახურვა",
						id: "cancel-dialog",
						click: function() {
							$(this).dialog("close");
							$(this).html("");
						}
					}
				};
				GetDialog("crm_dialog", 900, "auto", buttons, "top");
				GetDataTable("example_all_file", aJaxURL_crm, "get_list", 3, "request_table_id=" + $("#request_table_id").val(), 0, "", 1, "desc", "", '');
				SetEvents("", "", "", "example_all_file", "crm_file_dialog", aJaxURL_crm_file);
				setTimeout(function() {
					$('.ColVis, .dataTable_buttons').css('display', 'none');
				}, 90);
			} else if (fNm == 'crm_file_dialog') {
				var buttons = {
					"cancel": {
						text: "დახურვა",
						id: "cancel-dialog",
						click: function() {
							$(this).dialog("close");
							$(this).html("");
						}
					}
				};
				GetDialog("crm_file_dialog", "auto", "auto", buttons, "top");

			} else {
				$("#incoming_chat_tabs").tabs();
				var buttons = {

					"ipaddres_block": {
						text: "IP დაბლოკვა",
						id: "ipaddres_block"
					},
					"num_block": {
						text: "ნომრის დაბლოკვა",
						id: "num_block"
					},
					"cancel": {
						text: "დახურვა",
						id: "cancel-dialog",
						click: function() {
							var buttons1 = {
								"done": {
									text: "კი",
									id: "save-yes",
									click: function() {
										$("#add-edit-form").dialog("close");
										$(this).dialog("close");
									}
								},
								"no": {
									text: "არა",
									id: "save-no",
									click: function() {
										$(this).dialog("close");
									}
								}
							};
							GetDialog("hint_close", 300, "auto", buttons1);
							$("#save-yes").css('float', 'right');
							$("#save-no").css('float', 'right');
						}
					},
					"done": {
						text: "შენახვა",
						id: "save-dialog",
						class: "save-incomming-main-dialog"
					}

				};


				/* Dialog Form Selector Name, Buttons Array */
				GetDialog(fName, 1206, "auto", buttons);
				$(".save-incomming-main-dialog").button();
				$('.add-edit-form-class .ui-dialog-titlebar-close').remove();
				$('.add-edit-form-class').css('left', '84.5px');
				$('.add-edit-form-class').css('top', '0px');
				$('.ui-dialog-buttonset').css('width', '100%');
				$("#cancel-dialog").css('float', 'right');
				$("#save-dialog").css('float', 'right');
				$("#incomming_cat_1, #incomming_cat_1_1, #incomming_cat_1_1_1, #my_site_resume, #my_site, #source_id, #inc_status, #s_u_status, #client_sex").chosen({
					search_contains: true
				});
				$("#search_ab_pin").button();
				$('#add_comment').button();
				$("#my_site_resume_chosen").css('width', '560px')
				var dLength = [
					[5, 10, 30, -1],
					[5, 10, 30, "ყველა"]
				];
				GetDate('start_check');
				GetDate('end_check');
				GetDataTable("table_history", aJaxURL, "get_list_history", 8, "&start_check=" + $('#start_check').val() + "&end_check=" + $('#end_check').val() + "&phone=" + $("#phone").val() + "&s_u_user_id=" + $("#s_u_user_id").val(), 0, dLength, 1, "desc", '', "<'F'lip>");
				$("#table_history_length").css('top', '0px');
				$(".jquery-ui-button").button();
				GetTabs('tabs_sport');

				$("#call_content").off();
				$("#call_content1").off();
				$("#send_message").off();

				$("#call_content").autocomplete("server-side/seoy/seoy_textarea.action.php?source_id=" + $("#source_id").val() + "&site=" + $("#my_site").chosen().val(), {
					width: 300,
					multiple: true,
					matchContains: true
				});

				$("#call_content1").autocomplete("server-side/seoy/seoy_textarea1.action.php?source_id=" + $("#source_id").val() + "&site=" + $("#my_site").chosen().val(), {
					width: 300,
					multiple: true,
					matchContains: true
				});

				$("#send_message").autocomplete("server-side/seoy/chat_textarea.action.php?source_id=" + $("#source_id").val() + "&site=" + $("#my_site").chosen().val(), {
					width: 360,
					multiple: true,
					matchContains: true
				});

				var id = $("#incomming_id").val();

				if ($("#tab_id").val() == 5) {
					$("#num_block, .save-incomming-main-dialog").css('display', 'none');
				} else {
					$("#num_block, .save-incomming-main-dialog").css('display', '');
				}

				if (($('#imchat').val() == 1 && $('#ast_source').val() == '')) {
					$('#source_id').val(4);
					$('#source_id').trigger("chosen:updated");
					$('.add-edit-form-class .ui-dialog-title').html('შემომავალი ჩატი');
					$('#num_block').css('display', 'none');
				} else if (($('#site_chat').val() > 0 && $('#ast_source').val() == '')) {
					$('#source_id').val(9);
					$('#source_id').trigger("chosen:updated");
					$('.add-edit-form-class .ui-dialog-title').html('შემომავალი ჩატი');
					$('#num_block').css('display', 'none');
				} else if (($('#fb_chat').val() > 0 && $('#ast_source').val() == '')) {
					$('#source_id').val(6);
					$('#source_id').trigger("chosen:updated");
					$('.add-edit-form-class .ui-dialog-title').html('შემომავალი ჩატი');
					$('#num_block').css('display', 'none');
				} else if (($('#mail_chat').val() > 0 && $('#ast_source').val() == '')) {
					$('#source_id').val(7);
					$('#source_id').trigger("chosen:updated");
					$('.add-edit-form-class .ui-dialog-title').html('შემომავალი ჩატი');
					$('#num_block').css('display', 'none');
				} else if (($('#video_chat_id').val() > 0 && $('#ast_source').val() == '')) {
					$('#source_id').val(12);
					$('#source_id').trigger("chosen:updated");
					$('.add-edit-form-class .ui-dialog-title').html('ვიდეო ზარი');
					$('#num_block').css('display', 'none');
					$('#chatistema').css('width', '210px');
					$('.add-edit-form-class').css('width', '879px');
					$("#cancel-chat").css('display', 'none');
					$('#chatcontent,#gare_div').css('display', 'none');
				} else {
					//$('#source_id option:eq(1)').prop('selected', true);
					$('#chatcontent,#gare_div').css('display', 'none');
					$('#chatistema').css('width', '210px');
					$('.add-edit-form-class').css('width', '879px');
					$("#cancel-chat").css('display', 'none');
					$('.add-edit-form-class .ui-dialog-title').html('ზარის დამუშავება');
					$('.add-edit-form-class .ui-dialog-titlebar').css('background', '#673AB7');
					$('.add-edit-form-class .ui-dialog-title').css('color', '#fff');
				}

				$('#send_message').keyup(function(e) {
					if ($("#source_id").val() == 4) {
						console.log("click");
						if (e.key == 8) {

							param = new Object();
							param.act = "focusout";
							param.id = $("#chat_original_id").val();

							$.ajax({
								url: aJaxURL,
								data: param,
								success: function(data) {}
							});
						} else {
							param = new Object();
							param.act = "focusin";
							param.id = $("#chat_original_id").val();

							$.ajax({
								url: aJaxURL,
								data: param,
								success: function(data) {}
							});
						}
					}
				});


				jQuery("time.timeago").timeago();
				//getchats();
				$('#choose_button').button();


				///////////////////
				// ai dunia      //
				///////////////////
				if ($('#add-edit-form input').length != 0) {

					/*
		            $.ajax({
				        url: "server-side/call/getchats.action.php",
					    data: "act=getchats",
					    dataType: "json",
				        success: function(data) {
					        if(data.count != 0 && $D.sessionStatus == 1){
					        	$('#chat_count').css('background','#FFCC00');
					        	setTimeout(function(){
					        		$('#chat_count').css('background','#F44336');
					    	    	}, 400);
					        	var audio = new Audio('sms.mp3');
					        	if(micr_val == 0){
									audio.play();
						        	}
					        }else{
					        	$('#chat_count').css('background','none');
					        }
				        	
							$("#incoming_chat_tab_chat").html(data.page.chat);
							$("#incoming_chat_tab_site").html(data.page.site);
							$("#incoming_chat_tab_mail").html(data.page.mail);
							$("#incoming_chat_tab_fbm").html(data.page.fbm);
				        	
				        	$('#chat_count').attr('first_id', data.first_id);
				        }
			    	});
		            if($('#chat_user_id').attr('ext') !='0'){
		    			$.ajax({
		    		        url: "AsteriskManager/checkme.php",
		    			    data: "ext="+$('#chat_user_id').attr('ext'),
		    			    dataType: "json",
		    		        success: function(data) {
		    		        	//console.log(data.phone);
		    		        	$('#chat_call_live').attr('src','media/images/icons/'+data.icon);
		    		        	if(data.phone != '' && data.phone != null){
		    		        		if(data.ipaddres == '2004'){
		        		        	    iporphone = data.ipaddres;
		        		        	}else{
		        		        		iporphone = data.phone;
		        		        	}
		    			        	$('#chat_call_queue').css('display','table-row');
		    		        		$('#chat_call_number').html(iporphone);
		    		        		$('#chat_call_duration').html(data.duration);
		    		        		$('#chat_call_number').attr('extention',$('#chat_user_id').attr('ext'));
		    		        		$('#chat_call_number').attr('number',data.phone);
		    		        		$('#chat_call_number').attr('ipaddres',data.ipaddres);
		    		        	}else{
		    		        		$('#chat_call_queue').css('display','none');
		    		        		$('#chat_call_number').html('');
		    		        		$('#chat_call_duration').html('');
		    		        	}
		    		        }
		    	    	});
		            }else{
		                $('#main_call_chat').css('display','none');
		            }
					*/
				}
			}
		}

		$(document).on("click", "#save-edit-form-sport", function() {

			param = new Object();
			param.act = "save-edit-form-sport";
			param.end_date = $("#add_sport_dialog #action_end_date").val();
			param.start_date = $("#add_sport_dialog #action_start_date").val();
			param.id = $("#add_sport_dialog #action_sport_id").val();
			param.act_sport_id = $("#add_sport_dialog #act_id").val();
			param.category_id = $("#add_sport_dialog #action_category_id").val();
			param.game_list = $("#add_sport_dialog #game_list").val();
			param.info = $("#add_sport_dialog #action_info").val();

			$.ajax({
				url: aJaxURLsport,
				data: param,
				success: function(data) {
					if (typeof(data.error) != "undefined") {
						if (data.error != "") {
							alert(data.error);
						} else {
							GetDataTable("table_sport", aJaxURLsport, "get_list", 7, "", 0, "", 0, "desc", '', "<'F'lip>");
							$("#add_sport_dialog").dialog('close');
							$("#table_sport_length").css('top', '0px');
						}
					}
				}
			});
		});

		/*	function getchats(){
			clearTimeout(t);

            var t = setTimeout(function(){


	    			getchats();


	    		}, 1000000);
		}*/
		$(document).on("click", "#search_ab_pin", function() {

			GetDataTable("table_history", aJaxURL, "get_list_history", 8, "&start_check=" + $('#start_check').val() + "&end_check=" + $('#end_check').val() + "&phone=" + $("#check_ab_phone").val() + "&s_u_user_id=" + $("#check_ab_user_id").val(), 0, "", 2, "desc", '', "<'F'lip>");

			$("#table_history_length").css('top', '0px');
		});
		$(document).on("click", "#tab_sport0", function() {
			GetDataTable("table_sport", aJaxURLsport, "get_list", 7, "", 0, "", 0, "desc", '', "<'F'lip>");
			$("#table_sport_length").css('top', '0px');
		});

		$(document).on("click", "#tab_sport1", function() {
			GetDataTable("table_sport1", aJaxURLsport, "get_list_archive", 7, "", 0, "", 0, "desc", '', "<'F'lip>");
			$("#table_sport1_length").css('top', '0px');
		});

		$(document).on("click", ".download6", function() {
			var str = 1;
			var link = ($(this).attr("str"));
			$call_date = new Date($(this).parent().parent().find(":nth-child(3)").html());
			$limit_date = new Date("2018-06-29 19:45:24");
			if ($call_date > $limit_date) {
				link = 'http://pbx.my.ge/records/' + link;
			} else {
				link = 'http://pbx.my.ge/records/' + link;
			}
			var btn = {
				"cancel": {
					text: "დახურვა",
					id: "cancel-dialog",
					click: function() {
						$(this).dialog("close");
						$(this).html("");
					}
				}
			};
			GetDialog_audio("audio_dialog", "auto", "auto", btn);

			$("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="' + link + '" type="audio/wav"> Your browser does not support the audio element.</audio>');
			$(".download1").css("background", "#F99B03");
			$(this).css("background", "#33dd33");

		});


		$(document).on("click", ".download2", function() {
			var str = 1;
			var link = ($(this).attr("str"));
			$call_date = new Date($(this).parent().parent().find(":nth-child(3)").html());
			$limit_date = new Date("2018-06-29 19:45:24");
			if ($call_date > $limit_date) {
				link = 'http://pbx.my.ge/records/' + link;
			} else {
				link = 'http://pbx.my.ge/records/' + link;
			}
			var btn = {
				"cancel": {
					text: "დახურვა",
					id: "cancel-dialog",
					click: function() {
						$(this).dialog("close");
						$(this).html("");
					}
				}
			};
			GetDialog_audio("audio_dialog", "auto", "auto", btn);
			//	alert('hfgj');
			$("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="' + link + '" type="audio/wav"> Your browser does not support the audio element.</audio>');
			$(".download").css("background", "#408c99");
			$(this).css("background", "#FF5555");

		});

		$(document).on("click", ".download4", function() {
			var str = 1;
			var link = ($(this).attr("str"));
			$call_date = new Date($(this).parent().parent().find(":nth-child(3)").html());
			$limit_date = new Date("2018-06-29 19:45:24");
			if ($call_date > $limit_date) {
				link = 'http://pbx.my.ge/records/' + link;
			} else {
				link = 'http://pbx.my.ge/records/' + link;
			}
			var btn = {
				"cancel": {
					text: "დახურვა",
					id: "cancel-dialog",
					click: function() {
						$(this).dialog("close");
						$(this).html("");
					}
				}
			};
			GetDialog_audio("audio_dialog", "auto", "auto", btn);
			//	alert('hfgj');
			$("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="' + link + '" type="audio/wav"> Your browser does not support the audio element.</audio>');
			$(".download").css("background", "#2dc100");
			$(".download4").css("background", "#2dc100");
			$(this).css("background", "#FF5555");

		});


		//ჩანაწერები
		$(document).on("click", ".download", function() {
			var str = 1;
			var link = ($(this).attr("str"));
			$call_date = new Date($(this).parent().parent().find(":nth-child(3)").html());
			$limit_date = new Date("2018-06-29 19:45:24");
			if ($call_date > $limit_date) {
				link = 'http://pbx.my.ge/records/' + link;
			} else {
				link = 'http://pbx.my.ge/records/' + link;
			}

			var btn = {
				"cancel": {
					text: "დახურვა",
					id: "cancel-dialog",
					click: function() {
						$(this).dialog("close");
						$(this).html("");
					}
				}
			};
			GetDialog_audio("audio_dialog", "auto", "auto", btn);

			$("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="' + link + '" type="audio/wav"> Your browser does not support the audio element.</audio>');
			$(".download").css("background", "#2dc100");
			$(".download4").css("background", "#2dc100");
			$(this).css("background", "#FF5555");

		});

		$(document).on("click", ".download1", function() {
			var str = 1;
			var link = ($(this).attr("str"));
			$call_date = new Date($(this).parent().parent().find(":nth-child(3)").html());
			$limit_date = new Date("2018-06-29 19:45:24");
			if ($call_date > $limit_date) {
				link = 'http://pbx.my.ge/records/' + link;
			} else {
				link = 'http://pbx.my.ge/records/' + link;
			}
			var btn = {
				"cancel": {
					text: "დახურვა",
					id: "cancel-dialog",
					click: function() {
						$(this).dialog("close");
						$(this).html("");
					}
				}
			};
			GetDialog_audio("audio_dialog", "auto", "auto", btn);

			$("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="' + link + '" type="audio/wav"> Your browser does not support the audio element.</audio>');
			$(".download1").css("background", "#F99B03");
			$(".download3").css("background", "#ce8a14");
			$(".download5").css("background", "#4980AF");
			$(".download").css("background", "#2dc100");
			$(this).css("background", "#5757FF");

		});

		$(document).on("click", ".download3", function() {
			var str = 1;
			var link = ($(this).attr("str"));
			$call_date = new Date($(this).parent().parent().find(":nth-child(3)").html());
			$limit_date = new Date("2018-06-29 19:45:24");
			if ($call_date > $limit_date) {
				link = 'http://pbx.my.ge/records/' + link;
			} else {
				link = 'http://pbx.my.ge/records/' + link;
			}
			var btn = {
				"cancel": {
					text: "დახურვა",
					id: "cancel-dialog",
					click: function() {
						$(this).dialog("close");
						$(this).html("");
					}
				}
			};
			GetDialog_audio("audio_dialog", "auto", "auto", btn);

			$("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="' + link + '" type="audio/wav"> Your browser does not support the audio element.</audio>');
			$(".download3").css("background", "#ce8a14");
			$(".download5").css("background", "#4980AF");
			$(".download1").css("background", "#F99B03");
			$(".download").css("background", "#2dc100");
			$(this).css("background", "#5757FF");

		});

		$(document).on("click", ".download5", function() {
			var str = 1;
			var link = ($(this).attr("str"));
			$call_date = new Date($(this).parent().parent().find(":nth-child(3)").html());
			$limit_date = new Date("2018-06-29 19:45:24");
			if ($call_date > $limit_date) {
				link = 'http://pbx.my.ge/records/' + link;
			} else {
				link = 'http://pbx.my.ge/records/' + link;
			}
			var btn = {
				"cancel": {
					text: "დახურვა",
					id: "cancel-dialog",
					click: function() {
						$(this).dialog("close");
						$(this).html("");
					}
				}
			};

			GetDialog_audio("audio_dialog", "auto", "auto", btn);

			$("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="' + link + '" type="audio/wav"> Your browser does not support the audio element.</audio>');
			$(".download5").css("background", "#4980AF");
			$(".download3").css("background", "#ce8a14");
			$(".download1").css("background", "#F99B03");
			$(".download").css("background", "#2dc100");
			$(this).css("background", "#5757FF");

		});
		// end record

		// 		 $(button).on("click", ".download", function (){
		//         	  $(this).css( "background","#FF5555" );
		//          });

		function CloseDialog() {
			$("#" + fName).dialog("close");
		}

		// Add - Save
		$(document).on("click", ".save-incomming-main-dialog", function() {
			//$('.save-incomming-main-dialog').attr('disabled','disabled');
			save_inc(1);
		});

		$(document).on("click", "#lid", function() {
			if ($('input[id=lid]:checked').val() == 1) {
				$("#lid_comment").css('display', '');
			} else {
				$("#lid_comment").css('display', 'none');
			}
		});

		function save_inc(ii) {

			if ($("#id").val() == '') {
				$('.save-incomming-main-dialog').attr('disabled', 'disabled');
			}

			values = $("#my_site").chosen().val();

			change = ischange(values, sites);

			param = new Object();

			param.act = "save_incomming";
			param.hidde_incomming_call_id = $("#hidde_incomming_call_id").val();
			param.id = $("#incomming_call_id").val();
			param.phone = $("#phone").val();
			param.ipaddres = $("#ipaddres").val();
			param.call_date = $("#call_date").val();
			param.hidden_user = $("#hidden_user").val();
			param.processing_start_date = $("#processing_start_date").val();
			param.chat_id = $('#chat_original_id').val();
			param.source = $('#chat_source').val();
			param.incomming_cat_1 = $("#incomming_cat_1").val();
			param.incomming_cat_1_1 = $("#incomming_cat_1_1").val();
			param.incomming_cat_1_1_1 = $("#incomming_cat_1_1_1").val();
			param.site_id = values;
			param.ischange = change;
			param.s_u_user_id = $("#s_u_user_id").val();
			param.s_u_mail = $("#s_u_mail").val();
			param.s_u_name = $("#s_u_name").val();
			param.s_u_pid = $("#s_u_pid").val();
			param.client_sex = $("#client_sex").val();
			param.client_birth_year = $("#client_birth_year").val();
			param.s_u_status = $("#s_u_status").val();
			param.source_id = $('#source_id').val();
			param.inc_status = $("#inc_status").val();
			param.call_content = $("#call_content").val();
			param.lid = $('input[id=lid]:checked').val();
			param.lid_comment = $('#lid_comment').val();

			param.rand_file = rand_file;
			param.file_name = file_name;
			param.hidden_inc = $("#hidden_inc").val();
			param.imchat = $('#imchat').val();
			param.ii = ii;

			var allowSave = true;
			var link = GetAjaxData(param);
			if ($("#phone").val() == "" && $('#source_id').val() == 1 && ii == 1) {
				allowSave = false;
				alert("შეავსეთ ტელეფონის ნომერი!");
			}

			if ($("#incomming_cat_1").val() == 0 && ii == 1) {
				allowSave = false;
				alert("შეავსეთ კატეგორია!");
			} else if ($("#incomming_cat_1_1").val() == 0 && ii == 1) {
				allowSave = false;
				alert("შეავსეთ კატეგორია1!");
			} else if ($("#incomming_cat_1_1_1").val() == 0 && ii == 1) {
				allowSave = false;
				alert("შეავსეთ კატეგორია2!");
			} else if (values == '' || values == null) {
				allowSave = false;
				alert('შეავსეთ საიტი!!!');
			} else if ($("#s_u_name").val() == '' && $('#source_id').val() == 1 && ii == 1) {
				allowSave = false;
				alert("შეავსეთ სახელი/გვარი!");
			} else if ($("#s_u_mail").val() == '' && $('#source_id').val() == 7 && ii == 1) {
				allowSave = false;
				alert("შეავსეთ ელ.ფოსტა!");
			} else if ($("#inc_status").val() == 0 && ii == 1) {
				allowSave = false;
				alert("შეავსეთ რეაგირება!");
			} else if ($("#source_id").val() == 0 && ii == 1) {
				allowSave = false;
				alert("შეავსეთ ინფორმაციის წყარო!");
			}

			if (allowSave) {

				$.ajax({
					url: aJaxURL,
					data: link,
					success: function(data) {
						if (typeof(data.error) != 'undefined') {
							if (data.error != '') {
								alert(data.error);
								$(".save-incomming-main-dialog").attr('disabled', false);

							} else {
								//	$('#add-edit-form').dialog("close");
								/*
								$.ajax({
							        url: "server-side/call/getchats.action.php",
								    data: "act=getchats",
								    dataType: "json",
							        success: function(data) {
								        if(data.count != 0 && $D.sessionStatus == 1){
								        	$('#chat_count').css('background','#FFCC00');
								        	setTimeout(function(){
								        		$('#chat_count').css('background','#F44336');
								    	    	}, 400);
								        	var audio = new Audio('sms.mp3');
								        	if(micr_val == 0){
												audio.play();
									        	}
								        }else{
								        	$('#chat_count').css('background','none');
								        }
							        	
										$("#incoming_chat_tab_chat").html(data.page.chat);
										$("#incoming_chat_tab_site").html(data.page.site);
										$("#incoming_chat_tab_mail").html(data.page.mail);
										$("#incoming_chat_tab_fbm").html(data.page.fbm);
							        	
							        	$('#chat_count').attr('first_id', data.first_id);
							        	//alert($('#inc_chats tr').length)
							        	if($('#inc_chats tr').length < 1){
											CloseDialog();
											LoadTable();
    									}else{
        									if(ii!=0){
        										$('.clickmetostart[color="#69D2E7"]').click();
        									}
	                			        }
							        }
						    	});
*/
								var btn = {
									"cancel": {
										text: "დახურვა",
										id: "cancel-dialog",
										click: function() {
											$(this).dialog("close");
											$(this).html("");
										}
									}
								};
								GetDialog("show_hint", "auto", "auto", btn);
								if ($('#add-edit-form input').length != 0) {
									LoadTable();
									setTimeout(function() {
										$('#show_hint').dialog("close");
									}, 700);
									$('#right_side, #side_menu, #gare_div').css('pointer-events', 'none');
									$('#right_side, #side_menu, #gare_div').css('opacity', '0.5');
									$('#right_side, #side_menu, #gare_div').css('background', '#ccc');
									$('#send_message').attr('disabled', 'disabled');
								}
								if (ii == 1 && $('#imchat').val() == 1) {

								}
							}
						}
					}
				});
			}
		}

		// open send sms dialog
		$(document).on("click", ".new-sms-button", function() {

			// define request data
			const data = {
				act: "open_new_sms_dialog",
				type: $(this).data("type")
			};

			$.ajax({
				url: aJaxURL,
				data,
				success: function(data) {

					var buttons = {
						"cancel": {
							text: "დახურვა",
							id: "cancel-dialog",
							click: function() {
								$(this).dialog("close");
								$(this).html("");
							}
						}
					};

					$("#add-edit-form-sms").html(data.page);
					$("[data-button='jquery-ui-button']").css({
						"font-size": "7px"
					}).button();

					GetDialog("add-edit-form-sms", 561, "auto", buttons, 'center top');

					// check new sms dialog type
					if (data.type === "phone") {

						// copy adressee phone number
						const phoneNum = $("#phone").val();

						// insert phone number into adressee receiver input
						$("#smsAddresseeByPhone").val(phoneNum);

					}

					// set send sms button type
					$("#sendNewSms").data("type", data.type);

				}
			});

		});

		$(document).on("click", ".client_conversation", function(event) {
			param = new Object();
			param.act = "get_client_conversation_count";

			$.ajax({
				url: aJaxURL,
				data: param,
				success: function(data) {
					if (typeof(data.error) != 'undefined') {
						if (data.error != '') {} else {
							$("#client_conversation_count_hint").html(0);
							$("#client_conversation_count_hint").css('display', 'none');

							GetDataTable("table_question", aJaxURL, "get_list_quest", 5, "source_id=" + $("#source_id").val() + "&site=" + $("#my_site").chosen().val(), 0, "", 0, "desc", '', "<'F'lip>");
							$("#table_question_length").css('top', '0px');
						}
					}
				}
			});

		});

		$(document).on("click", ".client_mail", function() {
			GetDataTable("table_mail", aJaxURL, "get_list_mail", 5, 'incomming_id=' + $('#incomming_call_id').val(), 0, "", 0, "desc", '', "<'F'lip>");
			GetButtons("add_mail", "");
			setTimeout(function() {
				$('.ColVis, .dataTable_buttons').css('display', 'none');
			}, 160);
			$("#table_mail_length").css('top', '0px');
		});

		$(document).on("click", ".client_mail", function() {
			GetDataTable("table_mail", aJaxURL, "get_list_mail", 5, 'incomming_id=' + $('#incomming_call_id').val(), 0, "", 0, "desc", '', "<'F'lip>");
			GetButtons("add_mail", "");
			setTimeout(function() {
				$('.ColVis, .dataTable_buttons').css('display', 'none');
			}, 160);
			$("#table_mail_length").css('top', '0px');
		});
		$(document).on("click", ".delete_upload_file", function() {
			$(this).parent().parent().html("");

			var isemty = true;
			var divs = document.getElementById("email_attachment_files").children;
			for (i = 0; i < divs.length; i++) {

				if (divs[i].innerHTML != "") {
					isemty = false;
				}
			}
			if (isemty) {
				$("#email_attachment_files").css("height", "0");
				$("#send_message").css("height", "120px");
			}


		});

		$(document).on("keyup", "#send_message", function(key) {
			var length = $(this).text().length;
			var text = $(this).text();

			if (key.keyCode == 8 || key.keyCode == 46) {
				if (length === 1 && text == " ") {
					$(this).text('');
					$("#call_content").focus();
					$(this).focus();

				}
			}
		})

		$(document).on("click", "#send_chat_program", function() {
			$(".delete_upload_file").html("") // delete x buttons \\
			var message_value = $("#send_message").html();

			var links = '';
			var file_name = '';
			var file_html = '';
			var f_html = '';
			var message_value_check = message_value;
			var array_links = [];
			var fb_obj = new Object();
			if ($("#source_id").val() != 7) {
				$('#email_attachment_files .my_mail_file').each(function() {
					links += $(this).attr('link') + ';';
					array_links.push($(this).attr('link'));
					file_name += $(this).attr("file_name") + ";";
					f_html += "<hr>" + $(this).html() + "<br><hr>";
					file_html += '<a "' + links + '">' + $(this).html() + '</a><hr>';
				});
				message_value = f_html + message_value;
				//.replace(/\s/g, '');
				if (message_value.length != 0 || links != 0) {
					sms_text = '<tr style="height: 17px;">' +
						'<td colspan="2" style=" word-break: break-word;">' +
						'<div style="float: right!important; background: rgba(80, 181, 0, 0.24) none repeat scroll 0 0; border-radius: 15px;padding: 7px 15px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word;box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);">'

						+
						'<p style="text-align: right; font-weight: bold; font-size: 14px;">' + $('#chat_user_id').attr('chat_user_name') + '</p>'
						// +'<p style="padding-top: 7px; line-height: 20x; font-size: 14px; text-align: right;">'+file_html+'</p>'
						+
						'<p style="overflow-x: auto; padding-top: 7px; line-height: 20x; font-size: 14px;">' + message_value + '</p>' +
						'<div style="text-align: right; padding-top: 7px;">' +
						'<p style="color: #878787; width: 100%;">' + getDateTime() + '</p>' +
						'</div>' +
						'</div>' +
						'</td>' +
						'</tr>' +
						'<tr style="height:10px;"></tr>';
					$("#log").append(sms_text);
					if ($("#source_id").val() == "6") {
						fb_obj.arr = array_links;
						fb_obj.text = $("#send_message").html();
						fb_obj.text_fb = document.querySelector("#send_message").textContent;
					}
					console.log(fb_obj);
					sendSms(message_value, fb_obj, links, file_name);
					jQuery("time.timeago").timeago();
					document.getElementById('chat_scroll').scrollTop = 25000000000;

					if ($("#source_id").val() == 4) {
						param = new Object();
						param.act = "focusout";
						param.id = $("#chat_original_id").val();

						$.ajax({
							url: aJaxURL,
							data: param,
							success: function(data) {}
						});
					}

				}
			} else {
				$('#email_attachment_files .my_mail_file').each(function() {
					links += $(this).attr('link') + ';';
					file_name += $(this).attr("file_name") + ";";
					f_html += $(this).html() + "<hr>";
					file_html += '<a "' + links + '">' + $(this).html() + '</a><hr>';
				});
				var txt = $("#send_message").html();
				var txt_html = "<p>" + txt + "</p><hr>";
				// message_value += " <br><br><br> "+file_name +" ";
				message_value_check = message_value; //.replace(/\s/g, '');

				console.log(file_name + " ****** " + message_value + " /// " + links);
				if (message_value_check.length != 0 || file_name != 0) {
					sms_text = '<tr style="height: 17px;">' +
						'<td colspan="2" style=" word-break: break-word;">' +
						'<div style="float: right!important; background: rgba(80, 181, 0, 0.24) none repeat scroll 0 0; border-radius: 15px;padding: 7px 15px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word;box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);">'

						+
						'<p style="text-align: right; font-weight: bold; font-size: 14px;">' + $('#chat_user_id').attr('chat_user_name') + '</p>' +
						'<p style="padding-top: 7px; line-height: 20x; font-size: 14px; text-align: right;">' + f_html + '</p>' +
						'<p style="padding-top: 7px; line-height: 20x; font-size: 14px; text-align: right;">' + txt + '</p>' +
						'<div style="text-align: right; padding-top: 7px;">' +
						'<p style="color: #878787; width: 100%;">' + getDateTime() + '</p>' +
						'</div>' +
						'</div>' +
						'</td>' +
						'</tr>' +
						'<tr style="height:10px;"></tr>';
					console.log(sms_text);
					$("#log").append(sms_text);
					sendSms(message_value, f_html + txt_html, links, file_name);
					jQuery("time.timeago").timeago();
					document.getElementById('chat_scroll').scrollTop = 25000000000;

					if ($("#source_id").val() == 4) {
						param = new Object();
						param.act = "focusout";
						param.id = $("#chat_original_id").val();

						$.ajax({
							url: aJaxURL,
							data: param,
							success: function(data) {}
						});
					}

				}
			}
			$("#send_message").html('');
			$("#email_attachment_files").html('');
			$("#email_attachment_files").css("height", "0");
			$("#send_message").css("height", "120px");
			return false;
		});

		$(document).on("click", "#refresh", function() {
			fillter = '';
			LoadTable(fillter);
		});
		$(document).on("click", "#refresh_button", function() {
			fillter_all = '';
			LoadTable1();

		});

		$(document).on("click", "#refresh_button1", function() {

			param = new Object();
			param.act = "refresh";

			$.ajax({
				url: aJaxURL4,
				data: param,
				success: function(data) {
					if (typeof(data.error) != 'undefined') {
						if (data.error != '') {
							// alert(data.error);
							alert("Serjio");
						} else {
							$("#search_end_my1").val(data.date);
							LoadTable4();
						}
					}
				}
			});
		});
		$(document).on("click", "#add_task_button", () => {
			$.ajax({
				url: aJaxURL_task,
				type: "POST",
				data: "act=get_add_page",
				dataType: "json",
				success: function(data) {
					if (typeof(data.error) != "undefined") {
						if (data.error != "") {
							alert(data.error);
						} else {
							$("#add-edit-form-task").html(data.page);
							var buttons = {
								"save": {
									text: "შენახვა",
									id: "save-dialog-task"
								},
								"cancel": {
									text: "დახურვა",
									id: "cancel-dialog-task",
									click: function() {
										$(this).dialog("close");
										$(this).html("");
									}
								}
							};
							GetDialog("add-edit-form-task", 800, "auto", buttons, "center top");
							GetDateTimes("task_end_date");
							GetDateTimes("task_start_date");
							$("[data-select='chosen']").chosen({
								search_contains: true
							});

						}
					}
				}
			});
		});

		$(document).on("click", "#save-dialog-task", function() {
			$("#loading").show();
			values = $("#my_site").chosen().val();

			param = new Object();
			param.act = "save_task";
			param.task_type = $('#task_type').val();
			param.task_status_id = $('#task_status_id').val();
			param.task_status_id_2 = $('#task_status_id_2').val();
			param.task_branch = $('#task_branch').val();
			param.task_recipient = $('#task_recipient').val();
			param.task_create_date = $('#task_create_date').val();
			param.task_start_date = $('#task_start_date').val();
			param.task_end_date = $('#task_end_date').val();
			param.task_description = $('#task_description').val();
			param.task_note = $('#task_note').val();
			param.incomming_call_id = $('#incomming_call_id').val();
			param.site = values;


			if ($('#task_type').val() == 0) {
				alert('აირჩიე დავალების ტიპი!!!');
			} else if ($('#task_status_id').val() == 0) {
				alert('აირჩიე სტატუსი!!!');
			} else if ($("#task_status_id").val() == "77" && $("#task_status_id_2").val() == "0") {
				alert("შეავსეთ ქვე-სტატუსი !");
			} else if ($('#task_recipient').val() == 0) {
				alert('აირჩიე დავალების მიმღები!!!');
			} else if ($('#task_branch').val() == 0) {
				alert('აირჩიე განყოფილება!!!');
			} else if ($('#task_start_date').val() == '') {
				alert('შეავსე პერიოდის დასაწყისი!!!');
			} else if ($('#task_end_date').val() == '') {
				alert('შეავსე პერიოდის დასასრული!!!');
			} else if (param.task_description == '') {
				alert('შეავსეთ კომენტარი!!!');
			} else if (values == '' || values == null) {
				alert('შეავსეთ საიტი!!!');
			} else {
				var link = GetAjaxData(param);
				$.ajax({
					url: aJaxURL_task,
					data: link,
					success: function(data) {
						if (typeof(data.error) != "undefined") {
							if (data.error != "") {
								alert(data.error);
							} else {
								GetDataTable('task_table', aJaxURL_task, 'get_list', 6, 'incomming_call_id=' + $("#incomming_call_id").val(), 0, "", 2, "desc", '', "<'F'lip>");
								// $("#task_table_length").css('top','0px');
								$("#add-edit-form-task").dialog('close');

								//if(data.send_check>=1){
								param1 = new Object();
								param1.task_id = data.task_id;
								param1.task_type = data.task_type;
								param1.status_name = data.status_name;
								param1.site = values;
								param1.time = data.time;
								param1.send_type = 1;
								param1.incomming_call_id = $("#incomming_call_id").val();
								param1.task_recipient = $('#task_recipient').val();
								param1.type = 1;
								var link1 = GetAjaxData(param1);

								$.ajax({
									url: aJaxURL_getmail,
									data: link1,
									success: function(data) {
										$("#loading").hide();
									}
								});
								//}
							}
						}
					}
				});
			}
		});

		function ischange(arr1, arr2) {
			size = (arr1.length > arr2.length ? arr1.length : arr2.length);
			change = false;
			for (var i = 0; i < size; i++) {
				change = arr1[i] == arr2[i];
			}
			return !change;
		}

		$(document).on("click", ".task", function() {
			GetDataTable('task_table', aJaxURL_task, 'get_list', 6, 'incomming_call_id=' + $("#incomming_call_id").val(), 0, "", 2, "desc", '', "<'F'lip>");
			$("#add_task_button").button();
		})

		$(document).on("click", "#refresh_button2", function() {

			param = new Object();
			param.act = "refresh";

			$.ajax({
				url: aJaxURL6,
				data: param,
				success: function(data) {
					if (typeof(data.error) != 'undefined') {
						if (data.error != '') {
							// alert(data.error);
							alert("Console log");
						} else {
							$("#search_end_my2").val(data.date);
							LoadTable6();
						}
					}
				}
			});
		});


		$(document).on("click", "#ipaddres_block", function() {
			var buttons = {
				"cancel": {
					text: "დახურვა",
					id: "cancel",
					click: function() {
						$(this).dialog("close");
					}
				},

				"ipaddr": {
					text: "დაბლოკვა",
					id: "ipaddr"
				}

			};


			/* Dialog Form Selector Name, Buttons Array */
			GetDialog("ipaddr_dialog", 400, "auto", buttons);
		});

		$(document).on("click", "#ipaddr", function() {
			parame = new Object();
			parame.ipaddres = $("#ipaddres").val();
			parame.ipaddr_block_comment = $("#ipaddr_block_comment").val();
			parame.act = "ipaddr_check"

			$.ajax({
				url: aJaxURL,
				data: parame,
				success: function(data) {
					if (data.check == '0') {
						alert('ესეთი IP უკვე დაბლოკილია!');
					} else {
						$("#ipaddr_dialog").dialog("close");
					}
				}
			});
		});

		$(document).on("click", "#num_block_locked", function() {
			parame = new Object();
			parame.phone = $("#phone").val();
			parame.ipaddres = $("#ipaddres").val();

			parame.act = "num_check"
			$.ajax({
				url: aJaxURL,
				data: parame,
				success: function(data) {
					if (typeof(data.error) != 'undefined') {
						if (data.error != '') {
							// alert(data.error);
							alert("Loading");
						} else {
							if (data.check == 1) {
								param = new Object();
								param.phone = $("#phone").val();
								param.block_comment = $("#block_comment2").val();

								param.act = "black_list_add";
								if (param.phone == "") {
									alert("შეავსეთ ტელეფონის ნომერი!");
								} else {
									$.ajax({
										url: asterisk_url,
										data: param,
										success: function(data) {
											if (typeof(data.error) != 'undefined') {
												if (data.error != '') {
													// alert(data.error);
													alert("Container");
												} else {
													LoadTable();
													$("#numlock_dialog").dialog("close");

												}
											}
										}
									});

									$.ajax({
										url: aJaxURL,
										data: param,
										success: function(data) {
											if (typeof(data.error) != 'undefined') {
												if (data.error != '') {
													// alert(data.error);
													alert("Hello there");
												} else {
													LoadTable();
													$("#numlock_dialog").dialog("close");

												}
											}
										}
									});
								}
							} else {
								alert('ნომერი უკვე არის დაბლოკილთა სიაში');
							}
						}
					}
				}
			});


		});

		$(document).on("click", "#dialog_emojis", function() {
			var height = $("#emoji_wrapper").height();
			if (height > 0) {
				$("#emoji_wrapper").css("height", "0");
			} else {
				$("#emoji_wrapper").css("height", "100%");

			}
		})

		$(document).on("click", "#emoji_wrapper span", function() {
			var emoji = $(this).attr("code");
			if (emoji != "0") {
				$("#send_message").html($("#send_message").html() + " " + emoji);
				$("#send_message").trigger('focus');
				$("#emoji_wrapper").css("height", "0");
			} else {
				$("#send_message").trigger('focus');
				$("#emoji_wrapper").css("height", "0");
			}


		})



		$(document).on("click", "#chat_block_locked", function() {

			parame = new Object();
			parame.act = "black_list_add_chat"
			parame.source_id = $("#source_id").val();
			parame.id = $("#id").val();
			parame.chat_block_reason = $("#chat_block_reason").val();
			parame.site_chat_id = $("#site_chat_id").val();

			if (parame.chat_block_reason == '') {
				alert('შეავსეთ კომენტარი !!!');
			} else if (parame.id == '') {
				alert('აირჩიეთ შესაბამისი ჩატი !!!')
			} else {
				$.ajax({
					url: aJaxURL,
					data: parame,
					success: function(data) {
						if (typeof(data.error) != 'undefined') {
							if (data.error != '') {
								alert(data.error);
							} else {
								LoadTable();
								$("#numlock_dialog").dialog("close");
								alert('ჩატი წარმატებით დაიბლოკა');

								param = new Object();
								param.source = 'fbm_block';
								param.chat_id = $('#chat_original_id').val();

								if ($("#source_id").val() == '6') {
									$.ajax({
										url: "server.php",
										data: param,
										dataType: "json",
										success: function(data) {
											console.log('დაიბლოკა');
										}
									});
								}
							}
						}
					}
				});
			}
		});

		function run(number, ipaddres, ab_pin, check_save_chat) {

			if (check_save_chat == 1) {
				save_inc(0);
			}

			param = new Object();
			param.act = "get_add_page";
			param.number = number;
			param.ipaddres = ipaddres;
			param.ab_pin = ab_pin;

			$.ajax({
				url: aJaxURL,
				data: param,
				success: function(data) {
					if (typeof(data.error) != 'undefined') {
						if (data.error != '') {
							// alert(data.error);
							alert("Abc");
						} else {
							$("#add-edit-form").html(data.page);
							LoadDialog();
							all_chats = 1;
							runAjax();

						}
					}
				}
			});
		}

		$(document).on("click", "#chat_count_open, #fb_chat_count_open, #mail_chat_count_open, #site_chat_count_open, #video_chat_count_open", function() {


			if ($(this).html() != '') {
				var check_chat_status = 0;

				if ($(this).data('source') == 'chat') {
					if ($("input[id='web_chat_checkbox']:checked").val() == 1) {
						check_chat_status = 1;
					}
				}

				if ($(this).data('source') == 'site') {
					if ($("input[id='site_chat_checkbox']:checked").val() == 1) {
						check_chat_status = 1;
					}
				}

				if ($(this).data('source') == 'fbm') {
					if ($("input[id='messanger_checkbox']:checked").val() == 1) {
						check_chat_status = 1;
					}
				}

				if ($(this).data('source') == 'mail') {
					if ($("input[id='mail_chat_checkbox']:checked").val() == 1) {
						check_chat_status = 1;
					}
				}

				if ($(this).data('source') == 'video') {
					if ($("input[id='video_call_checkbox']:checked").val() == 1) {
						check_chat_status = 1;
					}
				}

				if ($(this).data('source') == 'video') {
					check_chat_status = 1;
					call_id = $(this).data('call_id');
					chat_id = $(this).attr('chat_id');
					if (chat_id > 0) {
						newwindow = window.open('https://ccapi.crystalbet.com/videocall/index.html#' + call_id, 'name', 'height=500,width=700');
						if (window.focus) {
							newwindow.focus()
						}
					}
				};

				// end check_chat_status //	

				if (check_chat_status == 1) {
					param = new Object();
					param.act = "get_edit_page";
					param.number = '';
					param.imby = '1';
					param.chat_id = $(this).attr('chat_id');
					param.source = $(this).data('source');
					if (param.chat_id != 0) {
						$.ajax({
							url: aJaxURL,
							data: param,
							success: function(data) {
								if (typeof(data.error) != 'undefined') {
									if (data.error != '') {} else {
										$("#add-edit-form").html(data.page);
										runAjax();
										$.ajax({
											url: aJaxURL,
											data: {
												'act': 'get_all_chat',
												'chat_id': $('#chat_original_id').val(),
												'source': $("#chat_source").val()
											},
											success: function(data) {
												if (typeof(data.error) != 'undefined') {
													if (data.error != '') {

													} else {

														$('#log').html('');
														$('#log').html(data.sms);
														$('#chat_detail_id').val(data.chat_detail_id);

														$('#send_message').prop('disabled', false);
														$('#chat_flag,#chat_browser,#chat_device').css('display', 'block');
														$('#chat_flag').attr('src', 'media/images/icons/georgia.png');
														$('#chat_browser').attr('src', 'media/images/icons/' + data.bro + '.png');
														$('#chat_device').attr('src', 'media/images/icons/' + data.os + '.png');
														$('#user_block').attr('ip', data.ip);
														$('#user_block').attr('pin', data.pin);

														document.getElementById('chat_scroll').scrollTop = 25000000000;
														jQuery("time.timeago").timeago();
														$('#chat_count_open').css('background', 'none');
														all_chats = 1;
														LoadDialog();
													}
												}
											}
										});
									}
								}
							}
						});
					}
				} else {
					alert('ჩატის გასახსნელად გააქტიურეთ შესაბამისი კომუნიკაციის არხი!');
				}
			}
		});
		$(document).on("click", "#open_my_chat", function() {
			var check_chat_status = 0;

			if ($(this).data('source') == 'chat') {
				if ($("input[id='web_chat_checkbox']:checked").val() == 1) {
					check_chat_status = 1
				}
			}

			if ($(this).data('source') == 'site') {
				if ($("input[id='site_chat_checkbox']:checked").val() == 1) {
					check_chat_status = 1
				}
			}

			if ($(this).data('source') == 'fbm') {
				if ($("input[id='messanger_checkbox']:checked").val() == 1) {
					check_chat_status = 1
				}
			}

			if ($(this).data('source') == 'mail') {
				if ($("input[id='mail_chat_checkbox']:checked").val() == 1) {
					check_chat_status = 1
				}
			}

			if ($(this).data('source') == 'video') {
				if ($("input[id='video_call_checkbox']:checked").val() == 1) {
					check_chat_status = 1
				}
			}

			if (check_chat_status == 1) {

				param = new Object();
				param.act = "get_edit_page";
				param.number = '';
				param.imby = '1';
				param.chat_id = $(this).attr('chat_id');
				param.source = $(this).data('source');

				$.ajax({
					url: aJaxURL,
					data: param,
					success: function(data) {
						if (typeof(data.error) != 'undefined') {
							if (data.error != '') {
								alert("What is losting");
							} else {
								$("#add-edit-form").html(data.page);
								runAjax();
								$.ajax({
									url: aJaxURL,
									data: {
										'act': 'get_all_chat',
										'chat_id': $('#chat_original_id').val(),
										'source': $("#chat_source").val()
									},
									success: function(data) {
										if (typeof(data.error) != 'undefined') {
											if (data.error != '') {

											} else {

												$('#log').html('');
												$('#log').html(data.sms);
												$('#chat_detail_id').val(data.chat_detail_id);
												$('#send_message').prop('disabled', false);
												$('#chat_flag,#chat_browser,#chat_device').css('display', 'block');
												$('#chat_flag').attr('src', 'media/images/icons/georgia.png');
												$('#chat_browser').attr('src', 'media/images/icons/' + data.bro + '.png');
												$('#chat_device').attr('src', 'media/images/icons/' + data.os + '.png');
												$('#user_block').attr('ip', data.ip);
												$('#user_block').attr('pin', data.pin);
												jQuery("time.timeago").timeago();
												$('#chatcontent,#gare_div').css('display', 'block');
												document.getElementById('chat_scroll').scrollTop = 25000000000;
												$('.clickmetostart[chat_id="' + $('#chat_original_id').val() + '"][data-source="' + $("#chat_source").val() + '"]').click();
												all_chats = 1;
												LoadDialog();
											}
										}
									}
								});
							}
						}

					}
				});
			} else {
				alert('ჩატის გასახსნელად გააქტიურეთ შესაბამისი კომუნიკაციის არხი!');
			}
		});

		$(document).on("click", "#download", function() {

			var download_file = $(this).val();
			var download_name = $('#download_name').val();
			SaveToDisk(download_file, download_name);
		});

		$(document).on("change", "#source_id, #my_site", function() {
			$("#call_content").off();
			$("#call_content1").off();
			$("#send_message").off();

			$("#call_content").autocomplete("server-side/seoy/seoy_textarea.action.php?source_id=" + $("#source_id").val() + "&site=" + $("#my_site").chosen().val(), {
				width: 300,
				multiple: true,
				matchContains: true
			});

			$("#call_content1").autocomplete("server-side/seoy/seoy_textarea1.action.php?source_id=" + $("#source_id").val() + "&site=" + $("#my_site").chosen().val(), {
				width: 300,
				multiple: true,
				matchContains: true
			});

			$("#send_message").autocomplete("server-side/seoy/chat_textarea.action.php?source_id=" + $("#source_id").val() + "&site=" + $("#my_site").chosen().val(), {
				width: 360,
				multiple: true,
				matchContains: true
			});


		});



		function SaveToDisk(fileURL, fileName) {
			$("#downloader").attr("href", fileURL);
			$("#downloader")[0].click();
			var iframe = document.createElement("iframe");
			iframe.src = fileURL;
			iframe.style.display = "none";
			document.body.appendChild(iframe);
			return false;
		}

		$(document).on("click", "#delete", function() {
			var delete_id = $(this).val();
			var r = confirm("გსურთ წაშალოთ?");
			if (r == true) {
				$.ajax({
					url: aJaxURL,
					data: {
						act: "delete_file",
						delete_id: delete_id,
						edit_id: $("#incomming_call_id").val(),
					},
					success: function(data) {
						$("#file_div").html(data.page);
					}

				});
			}
		});

		$(document).on("click", "#choose_button", function() {
			$("#choose_file").click();
		});

		$(document).on("change", "#choose_file", function() {
			var file = $(this).val();
			var files = this.files[0];
			var name = uniqid();
			var path = "../../media/uploads/file/";


			var ext = file.split('.').pop().toLowerCase();
			if ($.inArray(ext, ['pdf', 'pptx', 'png', 'xls', 'xlsx', 'jpg', 'docx', 'doc', 'csv']) == -1) { //echeck file type
				alert('This is not an allowed file type.');
				this.value = '';
			} else {
				file_name = files.name;
				rand_file = name + "." + ext;
				$.ajaxFileUpload({
					url: upJaxURL,
					secureuri: false,
					fileElementId: "choose_file",
					dataType: 'json',
					data: {
						act: "upload_file",
						path: path,
						file_name: name,
						type: ext
					},
					success: function(data, status) {
						if (typeof(data.error) != 'undefined') {
							if (data.error != '') {
								// alert(data.error);
								alert("It is not problem");
							} else {
								$.ajax({
									url: aJaxURL,
									data: {
										act: "up_now",
										rand_file: data['file_name'],
										file_name: file_name,
										edit_id: $("#incomming_call_id").val(),

									},
									success: function(data) {
										$("#file_div").html(data.page);
										$("#choose_file").val('');
									}
								});

							}
						}


					},
					error: function(data, status, e) {
						// alert(e);
						alert("It was E");
					}
				});
			}
		});

		function is_dialog_open() {
			try {
				if ($("#add-edit-form").dialog('isOpen'))
					return true;
				else
					return false;
			} catch (error) {
				return false;
			}
			return false;
		}
		$(document).on("change", "#check_state", function() {
			alert(1)
		});
		////////////////////////////////////////////////////////////////////////////////////
		var ajaxtimer;

		function runAjax() {
			try {
				clearTimeout(ajaxtimer);
			} catch (err) {}



			$.ajax({
				async: true,
				dataType: "JSON",
				url: 'AsteriskManager/liveState.php',
				data: {
					'sesvar': 'hideloggedoff',
					'value': true,
					'stst': 1,
					'checkState': $("#check_state").val(),
					'chat_id': $('#chat_original_id').val(),
					'chat_detail_id': $('#chat_detail_id').val(),
					'whoami': $('#whoami').val()
				},
				success: function(data) {
					//////////////////////////--------start flash paneli-------------////////////////////////////					

					if ($("#check_state").val() == 1) {
						$('#mini_call_queue').html('');
						$('#mini_call_ext').html('');
						$('#mini_call_ext').html(data.mini_call_ext);
						$('#mini_call_queue').html(data.mini_call_queue);
					} else {
						$('#call_queue').html('');
						$('#call_ext').html('');
						$('#call_ext').html(data.call_ext);
						$('#call_queue').html(data.call_queue);
					}
					var fabc = $('#chat_count_open .fabtext, .chat.fabtext');
					if (data.chat_count > 0) {
						$('#chat_count_open').attr('chat_id', data.chat_id);
						fabc.html(data.chat_count).show().css('background', '#FFCC00');
						setTimeout(function() {
							fabc.css('background', '#F44336');
						}, 450);
						var audio = new Audio('sms.mp3');
						if (micr_val == 0) {
							//	audio.play();
						}
					} else {
						fabc.hide();
					}
					var fabsite = $('#site_chat_count_open .fabtext, .site_chat.fabtext');
					if (data.site_chat_count > 0) {
						$('#site_chat_count_open').attr('chat_id', data.site_chat_id);
						fabsite.html(data.site_chat_count).show().css('background', '#FFCC00');
						setTimeout(function() {
							fabsite.css('background', '#F44336');
						}, 400);
						var audio = new Audio('sms.mp3');
						if (micr_val == 0) {
							//	audio.play();
						}
					} else {
						fabsite.hide();
					}
					var fabmail = $('#mail_chat_count_open .fabtext, .mail_chat.fabtext');
					if (data.mail_chat_count > 0) {
						$('#mail_chat_count_open').attr('chat_id', data.mail_chat_id);
						fabmail.html(data.mail_chat_count).show().css('background', '#FFCC00');
						setTimeout(function() {
							fabmail.css('background', '#F44336');
						}, 380);
						var audio = new Audio('sms.mp3');
						if (micr_val == 0) {
							//	audio.play();
						}
					} else {
						fabmail.hide();
					}
					var fabfbm = $('#fb_chat_count_open .fabtext, .fbm_chat.fabtext');
					if (data.fb_chat_count > 0) {
						$('#fb_chat_count_open').attr('chat_id', data.fb_chat_id);
						fabfbm.html(data.fb_chat_count).show().css('background', '#FFCC00');
						setTimeout(function() {
							fabfbm.css('background', '#F44336');
						}, 420);
						var audio = new Audio('sms.mp3');
						if (micr_val == 0) {
							//	audio.play();
						}
					} else {
						fabfbm.hide();
					}


					var videositem = $('#video_chat_count_open .fabtext, .video_chat.fabtext');
					if (data.video_chat_count > 0) {
						$('#video_chat_count_open').attr('chat_id', data.video_chat_id);
						$('#video_chat_count_open').data('call_id', data.video_chat_call_id);
						videositem.html(data.video_chat_count).show().css('background', '#FFCC00');
						setTimeout(function() {
							videositem.css('background', '#F44336');
						}, 420);
						var audio = new Audio('sms.mp3');
						if (micr_val == 0) {
							//	audio.play();
						}
					} else {
						videositem.hide();
					}


					///////////////////////----------end flesh paneli----------------//////////////////////////

					if (all_chats == 2 && !is_dialog_open()) {
						all_chats = 0;
					}
					if ($('#add-edit-form input').length != 0 && all_chats) {
						$.ajax({
							async: true,
							url: "server-side/call/getchats.action.php",
							data: {
								'act': 'getchats',
								'chat_id': $('#chat_original_id').val(),
								'chat_detail_id': $('#chat_detail_id').val(),
								'source': $("#chat_source").val()
							},
							dataType: "json",
							success: function(data) {
								$('#chat_flag,#chat_browser,#chat_device').css('display', 'none');
								if (data.chat_seen == 1) {
									$("#span_seen").css('display', '');
									document.getElementById('chat_scroll').scrollTop = 25000000000;
								} else {
									$("#span_seen").css('display', 'none');
								}
								if (data.chat_detail_id && data.chat_detail_id != $('#chat_detail_id').val()) {
									$('#log').append(data.sms);
									$('#chat_detail_id').val(data.chat_detail_id);
									document.getElementById('chat_scroll').scrollTop = 25000000000;
								}
								if (data.count != 0 && $D.sessionStatus == 1) {
									$('#chat_count').css('background', '#FFCC00');
									setTimeout(function() {
										$('#chat_count').css('background', '#F44336');
									}, 400);
									var audio = new Audio('sms.mp3');
									if (micr_val == 0) {
										audio.play();
									}
								} else {
									$('#chat_count').css('background', 'none');
								}

								$("#incoming_chat_tab_chat1").html(data.page.chat);
								$("#incoming_chat_tab_chat_queue").html(data.page.chat_queue);

								if (data.page.chat_color_status == 1) {
									$(".incoming_chat_tab_chat").css('background-color', '#ffeb3b');
									setTimeout(function() {
										$(".incoming_chat_tab_chat").css('background-color', '#E6F2F8');
									}, 400);
								} else {
									$(".incoming_chat_tab_chat").css('background-color', '#E6F2F8');
								}

								$("#incoming_chat_tab_site1").html(data.page.site);
								$("#incoming_chat_tab_site_queue").html(data.page.site_queue);

								if (data.page.site_color_status == 1) {
									$(".incoming_chat_tab_site").css('background-color', '#ffeb3b');
									setTimeout(function() {
										$(".incoming_chat_tab_site").css('background-color', '#E6F2F8');
									}, 400);
								} else {
									$(".incoming_chat_tab_site").css('background-color', '#E6F2F8');
								}

								$("#incoming_chat_tab_mail1").html(data.page.mail);
								$("#incoming_chat_tab_mail_queue").html(data.page.mail_queue);

								if (data.page.mail_color_status == 1) {
									$(".incoming_chat_tab_mail").css('background-color', '#ffeb3b');
									setTimeout(function() {
										$(".incoming_chat_tab_mail").css('background-color', '#E6F2F8');
									}, 400);
								} else {
									$(".incoming_chat_tab_mail").css('background-color', '#E6F2F8');
								}

								$("#incoming_chat_tab_fbm1").html(data.page.fbm);
								$("#incoming_chat_tab_fbm_queue").html(data.page.fbm_queue);

								if (data.page.fbm_color_status == 1) {
									$(".incoming_chat_tab_fbm").css('background-color', '#ffeb3b');
									setTimeout(function() {
										$(".incoming_chat_tab_fbm").css('background-color', '#E6F2F8');
									}, 400);
								} else {
									$(".incoming_chat_tab_fbm").css('background-color', '#E6F2F8');
								}

								$("#incoming_chat_video1").html(data.page.video);
								$("#incoming_chat_video_queue").html(data.page.video_queue);

								if (all_chats == 1) {
									LoadDialog();
									var source = $('#chat_source').val() || 'chat';
									var index = $('#incoming_chat_tabs a[href="#incoming_chat_tab_' + source).parent().index();
									$("#incoming_chat_tabs").tabs("option", "active", index);
								}

								all_chats = 2;

								if ($('#chat_user_id').attr('ext') != '0') {
									$.ajax({
										async: true,
										url: "AsteriskManager/checkme.php",
										data: "ext=" + $('#chat_user_id').attr('ext'),
										dataType: "json",
										success: function(data) {
											//console.log(data.phone);
											$('#chat_call_live').attr('src', 'media/images/icons/' + data.icon);
											if (data.phone != '' && data.phone != null) {
												if (data.ipaddres == '2004') {
													iporphone = data.ipaddres;
												} else {
													iporphone = data.phone;
												}
												$('#chat_call_queue').css('display', 'table-row');
												$('#chat_call_number').html(iporphone);
												$('#chat_call_duration').html(data.duration);
												$('#chat_call_number').attr('extention', $('#chat_user_id').attr('ext'));
												$('#chat_call_number').attr('number', data.phone);
												$('#chat_call_number').attr('ipaddres', data.ipaddres);
											} else {
												$('#chat_call_queue').css('display', 'none');
												$('#chat_call_number').html('');
												$('#chat_call_duration').html('');
											}
											ajaxtimer = setTimeout(runAjax, 1000);
											$("#loading").hide();
										}
									});
								} else {
									$('#main_call_chat').css('display', 'none');
									$("#loading").hide();
									ajaxtimer = setTimeout(runAjax, 1000);
								}



							}
						});

					} else {
						ajaxtimer = setTimeout(runAjax, 1000);
						$("#loading").hide();
					}

					//$("#loading").hide()

				}

			}).done(function(data) {

			});
		}

		///////////////////////////////////////////////////////

		$(document).on("click", ".open_dialog", function() {
			var number = $(this).attr("number");
			var ipaddres = $(this).attr("ipaddres");
			var ab_pin = $(this).attr("ab_pin");
			var check_save_chat = $(this).attr("check_save_chat");
			if ($('#imchat').val() == 1) {

			}
			if (number != "") {
				run(number, ipaddres, ab_pin, check_save_chat);
			}
		});

		$(document).on("click", ".insert", function() {
			var phone = $(this).attr("number");

			if (phone != "") {
				$('#phone').val(phone);
			}
		});

		$(document).on("change", "#task_type_id", function() {
			var task_type = $("#task_type_id").val();

			if (task_type == 1) {
				$("#task_department_id").val(37);
			}

		});
		$(document).on("change", "#task_type", function() {
			param = new Object();
			param.act = "task_type_changed";
			param.cat_id = $('#task_type').val();
			$.ajax({
				url: aJaxURL_task,
				data: param,
				success: function(data) {
					$("#task_status_id").html(data.page);
					$('#task_status_id').trigger("chosen:updated");
					$('#task_branch').trigger("chosen:updated");
					$('#task_status_id').trigger("change");
				}
			});
		});

		$(document).on("change", "#task_status_id", function() {
			param = new Object();
			param.act = "get_task_status_2";
			param.cat_id = $('#task_status_id').val();
			$.ajax({
				url: aJaxURL_task,
				data: param,
				success: function(data) {
					$("#task_status_id_2").html(data.page);
					$('#task_status_id_2').trigger("chosen:updated");
				}
			});
		});

		$(document).on("change", "#incomming_cat_1", function() {
			param = new Object();
			param.act = "cat_2";
			param.cat_id = $('#incomming_cat_1').val();
			$.ajax({
				url: aJaxURL,
				data: param,
				success: function(data) {
					$("#incomming_cat_1_1").html(data.page);
					$('#incomming_cat_1_1').trigger("chosen:updated");
					if ($('#incomming_cat_1_1 option:selected').val() == 999) {
						param = new Object();
						param.act = "cat_3";
						param.cat_id = $('#incomming_cat_1_1').val();
						$.ajax({
							url: aJaxURL,
							data: param,
							success: function(data) {
								$("#incomming_cat_1_1_1").html(data.page);
								$('#incomming_cat_1_1_1').trigger("chosen:updated");
							}
						});
					}
				}
			});
		});

		$(document).on("change", "#incomming_cat_1_1", function() {
			param = new Object();
			param.act = "cat_3";
			param.cat_id = $('#incomming_cat_1_1').val();
			$.ajax({
				url: aJaxURL,
				data: param,
				success: function(data) {
					$("#incomming_cat_1_1_1").html(data.page);
					$('#incomming_cat_1_1_1').trigger("chosen:updated");
				}
			});
		});



		$(document).on("click", "#refresh-dialog", function() {
			param = new Object();
			param.act = "get_calls";

			$.ajax({
				url: aJaxURL,
				data: param,
				success: function(data) {
					if (typeof(data.error) != 'undefined') {
						if (data.error != '') {
							// alert(data.error);
							alert("Jimsher G");
						} else {
							$("#last_calls").html(data.calls);
							$(".insert").button({
								icons: {
									primary: "ui-icon-plus"
								}
							});
						}
					}
				}
			});

		});

		$(document).on("click", "#search_user_id", function() {
			param = new Object();
			param.act = 'search_user_id';
			param.s_u_user_id = $("#s_u_user_id").val();
			$.ajax({
				url: wsdl_url,
				data: param,
				success: function(data) {
					if (data.count == 1) {
						if (data.phone != '' && data.phone != 'null' && data.phone != null) {
							$("#phone").val(data.phone);
							$("#check_ab_phone").val(data.phone);
						}
						$("#s_u_user_id").val(data.user_id);
						$("#check_ab_user_id").val(data.user_id);
						$("#s_u_mail").val(data.email);
						$("#s_u_name").val(data.user_name + ' ' + data.user_surname);
						$("#s_u_pid").val(data.id_code);
						$('#client_sex').val(data.gender_id);
						$("#client_birth_year").val(data.birth_year);
						$('#client_sex').trigger("chosen:updated");
					} else if (data.count == 0) {
						alert('მონაცემები ვერ მოიძებნა');
					}
				}
			});

		});

		$(document).on("click", "#search_phone", function() {
			param = new Object();
			param.act = 'search_phone';
			param.phone = $("#phone").val();
			$.ajax({
				url: wsdl_url,
				data: param,
				success: function(data) {
					if (data.count == 1) {
						if (data.phone != '' && data.phone != 'null' && data.phone != null) {
							$("#phone").val(data.phone);
							$("#check_ab_phone").val(data.phone);
						}
						$("#s_u_user_id").val(data.user_id);
						$("#check_ab_user_id").val(data.user_id);
						$("#s_u_mail").val(data.email);
						$("#s_u_name").val(data.user_name + ' ' + data.user_surname);
						$("#s_u_pid").val(data.id_code);
						$('#client_sex').val(data.gender_id);
						$("#client_birth_year").val(data.birth_year);
						$('#client_sex').trigger("chosen:updated");
					} else if (data.count == 0) {
						alert('მონაცემები ვერ მოიძებნა');
					}
				}
			});

		});

		$(document).on("click", "#search_mail", function() {
			param = new Object();
			param.act = 'search_mail';
			param.s_u_mail = $("#s_u_mail").val();
			$.ajax({
				url: wsdl_url,
				data: param,
				success: function(data) {
					if (data.count == 1) {
						if (data.phone != '' && data.phone != 'null' && data.phone != null) {
							$("#phone").val(data.phone);
							$("#check_ab_phone").val(data.phone);
						}
						$("#s_u_user_id").val(data.user_id);
						$("#check_ab_user_id").val(data.user_id);
						$("#s_u_mail").val(data.email);
						$("#s_u_name").val(data.user_name + ' ' + data.user_surname);
						$("#s_u_pid").val(data.id_code);
						$('#client_sex').val(data.gender_id);
						$("#client_birth_year").val(data.birth_year);
						$('#client_sex').trigger("chosen:updated");
					} else if (data.count == 0) {
						alert('მონაცემები ვერ მოიძებნა');
					}
				}
			});

		});

		$(document).on("click", "#search_s_u_pid", function(event) {
			var check_lenght = $("#s_u_pid").val();


			param = new Object();
			param.act = 'search_s_u_pid';
			param.s_u_pid = $("#s_u_pid").val();
			if (check_lenght.length != 9) {
				alert('ძებნა ხორციელდება მხოლოდ საიდენტიფიკაციო ნომრით');
			} else {
				$.ajax({
					url: wsdl_url,
					data: param,
					success: function(data) {
						if (data.count == 1) {
							if (data.phone != '' && data.phone != 'null' && data.phone != null) {
								$("#phone").val(data.phone);
								$("#check_ab_phone").val(data.phone);
							}
							$("#s_u_user_id").val(data.user_id);
							$("#check_ab_user_id").val(data.user_id);
							$("#s_u_mail").val(data.email);
							$("#s_u_name").val(data.user_name + ' ' + data.user_surname);
							$("#s_u_pid").val(data.id_code);
							$('#client_sex').val(data.gender_id);
							$("#client_birth_year").val(data.birth_year);
							$('#client_sex').trigger("chosen:updated");
						} else if (data.count == 0) {
							alert('მონაცემები ვერ მოიძებნა');
						}
					}
				});
			}
		});
		//
		function my_filter() {
			var myhtml = '';
			if ($.session.get("operator_id") == 'on') {
				myhtml += '<span>ოპერატორი<close cl="operator_id">X</close></span>';
			} else {
				$('#operator_id option:eq(0)').prop('selected', true);
			}
			if ($.session.get("tab_id") == 'on') {
				myhtml += '<span>ტაბი<close cl="tab_id">X</close></span>';
			} else {
				$('#tab_id option:eq(0)').prop('selected', true);
			}
			if ($.session.get("filter_1") == 'on') {
				myhtml += '<span>შე. დამუშავებული<close cl="filter_1">X</close></span>';
			}
			if ($.session.get("filter_2") == 'on') {
				myhtml += '<span>შე. დაუმუშავებელი<close cl="filter_2">X</close></span>';
			}
			if ($.session.get("filter_3") == 'on') {
				myhtml += '<span>შე. უპასუხო<close cl="filter_3">X</close></span>';
			}
			if ($.session.get("filter_4") == 'on') {
				myhtml += '<span>შეხვედრა<close cl="filter_4">X</close></span>';
			}
			if ($.session.get("filter_5") == 'on') {
				myhtml += '<span>ინტერნეტი<close cl="filter_5">X</close></span>';
			}
			if ($.session.get("filter_6") == 'on') {
				myhtml += '<span>ტელეფონი<close cl="filter_6">X</close></span>';
			}
			if ($.session.get("filter_7") == 'on') {
				myhtml += '<span>გამცხადებელი<close cl="filter_7">X</close></span>';
			}

			$('.callapp_tabs').html(myhtml);
			LoadTable('index', colum_number, main_act, change_colum_main);
			$('#operator_id, #tab_id').trigger("chosen:updated");
		}

		function show_right_side(id) {
			$("#right_side fieldset").hide();
			$("#" + id).show();
			//$(".add-edit-form-class").css("width", "1260");
			//$('#add-edit-form').dialog({ position: 'left top' });
			hide_right_side();

			var str = $("." + id).children('img').attr('src');
			str = str.substring(0, str.length - 4);
			$("#side_menu span").children('img').css('border-bottom', '');
			$("." + id).children('img').css('filter', 'brightness(0.1)');
			$("." + id).children('img').css('border-bottom', '2px solid #333');
		}

		function hide_right_side() {

			$(".info").children('img').css('filter', 'brightness(1.1)');
			$(".client_history").children('img').css('filter', 'brightness(1.1)');
			$(".task").children('img').css('filter', 'brightness(1.1)');
			$(".record").children('img').css('filter', 'brightness(1.1)');
			$(".file").children('img').css('filter', 'brightness(1.1)');
			$(".sms").children('img').css('filter', 'brightness(1.1)');
			$(".client_mail").children('img').css('filter', 'brightness(1.1)');
			$(".client_conversation").children('img').css('filter', 'brightness(1.1)');
			$(".crm").children('img').css('filter', 'brightness(1.1)');
			$(".chat_question").children('img').css('filter', 'brightness(1.1)');
			$(".user_logs").children('img').css('filter', 'brightness(1.1)');
			$(".call_resume").children('img').css('filter', 'brightness(1.1)');
			$(".newsnews").children('img').css('filter', 'brightness(1.1)');
			$("#record fieldset").show();
		}
		$(document).on("click", "#search_ab_crm_pin", function() {

			GetDataTable("table_crm", aJaxURL, "get_list_crm", 9, "start_crm=" + $("#start_crm").val() + "&end_crm=" + $("#end_crm").val() + "&pin=" + $("#check_ab_crm_pin").val() + "&phone=" + $("#check_ab_crm_phone").val(), 0, [
				[50, -1],
				[50, "ყველა"]
			], 0, "desc", "", "<'F'lip>");
			SetEvents("", "", "", "table_crm", "crm_dialog", aJaxURL_crm);
			$("#table_crm_length").css('top', '0px');

			setTimeout(function() {
				$('.ColVis, .dataTable_buttons').css('display', 'none');
			}, 90);
		});
		$(document).on("click", ".user_logs", function() {
			GetDataTable("user_log_table", aJaxURL, "get_user_log", 7, "incomming_call_id=" + $("#incomming_call_id").val(), 0, "", 1, "desc", '', "<'F'lip>");
			setTimeout(function() {
				$('.ColVis, .dataTable_buttons').css('display', 'none');
			}, 160);
			$("#user_log_table_length").css("top", "0");
		});
		$(document).on("click", ".crm", function() {
			$("#check_ab_crm_pin").val($("#personal_pin").val());
			GetDataTable("table_crm", aJaxURL, "get_list_crm", 9, "start_crm=" + $("#start_crm").val() + "&end_crm=" + $("#end_crm").val() + "&pin=" + $("#check_ab_crm_pin").val() + "&phone=" + $("#check_ab_crm_phone").val(), 0, [
				[50, -1],
				[50, "ყველა"]
			], 0, "desc", "", "<'F'lip>");
			SetEvents("", "", "", "table_crm", "crm_dialog", aJaxURL_crm);
			$("#table_crm_length").css('top', '0px');
			$("#search_ab_crm_pin").button();
			GetDate('start_crm');
			GetDate('start_crm');

			setTimeout(function() {
				$('.ColVis, .dataTable_buttons').css('display', 'none');
			}, 90);
		});

		$(document).on("click", ".sms", function() {
			GetDataTable("table_sms", aJaxURL, "get_list_sms", 5, "&incomming_id=" + $('#incomming_call_id').val(), 0, "", 2, "desc", '', "<'F'lip>");
			$("#table_sms_length").css('top', '0px');
			setTimeout(function() {
				$('.ColVis, .dataTable_buttons').css('display', 'none');
			}, 90);
		});

		$(document).on("click", ".newsnews", function() {
			GetDataTable("table_news", aJaxURL, "get_list_news", 6, "", 0, "", 0, "desc", '', "<'F'lip>");
			$("#table_news_length").css('top', '0px');
			setTimeout(function() {
				$('.ColVis, .dataTable_buttons').css('display', 'none');
			}, 90);
		});

		$(document).on("change", "#chat_status", function() {

			toggleChatStatus('#chat_status', Number($(this).val()));

			$.ajax({
				url: aJaxURL,
				data: 'act=chat_on_off&value=' + $(this).val(),
				success: function(data) {
					if (typeof(data.error) != 'undefined') {
						if (data.error != '') {
							// alert(data.error);
							alert("Funny moments");
						} else {

						}
					}
				}
			});
		});
		$(document).on("click", "#callapp_show_filter_button", function() {
			if ($('.callapp_filter_body').attr('myvar') == 0) {
				$('.callapp_filter_body').css('display', 'block');
				$('.callapp_filter_body').attr('myvar', 1);

				$('#go_exel').css('top', '264px');
				$("#shh").css('background-position', '0 0px');
			} else {
				$('.callapp_filter_body').css('display', 'none');
				$('.callapp_filter_body').attr('myvar', 0);

				$('#go_exel').css('top', '171px');
				$("#shh").css('background-position', '0 9px');
			}
		});
		$(document).on("click", "#show_copy_prit_exel", function() {
			if ($(this).attr('myvar') == 0) {
				$('.ColVis,.dataTable_buttons').css('display', 'block');
				$(this).css('background', '#2681DC');
				$(this).children('img').attr('src', 'media/images/icons/select_w.png');
				$(this).attr('myvar', '1');
			} else {
				$('.ColVis,.dataTable_buttons').css('display', 'none');
				$(this).css('background', '#E6F2F8');
				$(this).children('img').attr('src', 'media/images/icons/select.png');
				$(this).attr('myvar', '0');
			}
		});
		$(document).on("click", "#show_station", function() {
			$(".flash_menu_link").css('background', 'rgb(255, 255, 255)');
			$(".flash_menu_link").css('color', 'rgb(38, 129, 220)');
			$(this).css('background', '#ccc');
			$(this).css('color', '#000');
			$("#whoami").val(1)
			if ($("#check_state").val() == 0) {
				$('#pirveli').html('რიგი');
				$('#meore').html('შიდა ნომერი');
				$('#mesame').html('თანამშრომელი');
				$('#meotxe').html('სტატუსი');
				$('#mexute').html('ნომერი');
				$('#meeqvse').html('დრო');
				$('#meshvide').html('სახელი');

				$('#q_pirveli').html('რიგი');
				$('#q_meore').html('პოზიცია');
				$('#q_mesame').html('ნომერი');
				$('#q_meotxe').html('სახელი');
				$('#q_mexute').html('ლოდინის დრო');
			} else {
				$('#mini_pirveli').html('შიდა ნომერი');
				$('#mini_meore').html('მომ. ავტორი');
				$('#mini_mesame').html('სტატუსი');
				$('#m_q_pirveli').html('პოზიცია');
				$('#m_q_meore').html('ნომერი');
			}
			runAjax();
		});
		$(document).on("click", "#show_chat", function() {
			$(".flash_menu_link").css('background', 'rgb(255, 255, 255)');
			$(".flash_menu_link").css('color', 'rgb(38, 129, 220)');
			$(this).css('background', '#ccc');
			$(this).css('color', '#000');
			$("#whoami").val(0)
			if ($("#check_state").val() == 0) {
				$('#pirveli').html('ქვეყანა');
				$('#meore').html('ბრაუ.');
				$('#mesame').html('მომ. ავტორი');
				$('#meotxe').html('სტატუსი');
				$('#mexute').html('ოპერატორი');
				$('#meeqvse').html('ხ-ბა');
				$('#meshvide').html('სულ ხ-ბა');

				$('#q_pirveli').html('პოზიცია');
				$('#q_meore').html('ქვეყანა');
				$('#q_mesame').html('ბრაუზერი');
				$('#q_meotxe').html('მომართვის ავტორი');
				$('#q_mexute').html('ლოდინის დრო');
			} else {
				$('#mini_pirveli').html('ოპერატორი');
				$('#mini_meore').html('მომ. ავტორი');
				$('#mini_mesame').html('სტატუსი');

				$('#m_q_pirveli').html('პოზიცია');
				$('#m_q_meore').html('სახელი');
			}
			runAjax();
		});
		$(document).on("click", "#show_site", function() {

			$(".flash_menu_link").css('background', 'rgb(255, 255, 255)');
			$(".flash_menu_link").css('color', 'rgb(38, 129, 220)');
			$(this).css('background', '#ccc');
			$(this).css('color', '#000');
			$("#whoami").val(2);
			if ($("#check_state").val() == 0) {
				$('#pirveli').html('ქვეყანა');
				$('#meore').html('სურათი');
				$('#mesame').html('მომ. ავტორი');
				$('#meotxe').html('სტატუსი');
				$('#mexute').html('ოპერატორი');
				$('#meeqvse').html('ხ-ბა');
				$('#meshvide').html('სულ ხ-ბა');

				$('#q_pirveli').html('პოზიცია');
				$('#q_meore').html('სურათი');
				$('#q_mesame').html('ნომერი');
				$('#q_meotxe').html('მომართვის ავტორი');
				$('#q_mexute').html('ლოდინის დრო');
			} else {
				$('#mini_pirveli').html('ოპერატორი');
				$('#mini_meore').html('მომ. ავტორი');
				$('#mini_mesame').html('სტატუსი');

				$('#m_q_pirveli').html('პოზიცია');
				$('#m_q_meore').html('სურათი');
				$('#m_q_mesame').html('სახელი');
			}
			runAjax();
		});

		$(document).on("click", "#show_mail", function() {

			$(".flash_menu_link").css('background', 'rgb(255, 255, 255)');
			$(".flash_menu_link").css('color', 'rgb(38, 129, 220)');
			$(this).css('background', '#ccc');
			$(this).css('color', '#000');
			$("#whoami").val(3);
			if ($("#check_state").val() == 0) {
				$('#pirveli').html('ქვეყანა');
				$('#meore').html('სურათი');
				$('#mesame').html('მომ. ავტორი');
				$('#meotxe').html('სტატუსი');
				$('#mexute').html('ოპერატორი');
				$('#meeqvse').html('ხ-ბა');
				$('#meshvide').html('სულ ხ-ბა');

				$('#q_pirveli').html('პოზიცია');
				$('#q_meore').html('სურათი');
				$('#q_mesame').html('ნომერი');
				$('#q_meotxe').html('მომართვის ავტორი');
				$('#q_mexute').html('ლოდინის დრო');
			} else {
				$('#mini_pirveli').html('ოპერატორი');
				$('#mini_meore').html('მომ. ავტორი');
				$('#mini_mesame').html('სტატუსი');

				$('#m_q_pirveli').html('პოზიცია');
				$('#m_q_meore').html('სურათი');
				$('#m_q_mesame').html('სახელი');
			}
			runAjax();
		});
		$(document).on("click", "#show_fbm", function() {
			$(".flash_menu_link").css('background', 'rgb(255, 255, 255)');
			$(".flash_menu_link").css('color', 'rgb(38, 129, 220)');
			$(this).css('background', '#ccc');
			$(this).css('color', '#000');
			$("#whoami").val(4);
			if ($("#check_state").val() == 0) {
				$('#pirveli').html('ქვეყანა');
				$('#meore').html('სურათი');
				$('#mesame').html('მომ. ავტორი');
				$('#meotxe').html('სტატუსი');
				$('#mexute').html('ოპერატორი');
				$('#meeqvse').html('ხ-ბა');
				$('#meshvide').html('სულ ხ-ბა');

				$('#q_pirveli').html('პოზიცია');
				$('#q_meore').html('სურათი');
				$('#q_mesame').html('ნომერი');
				$('#q_meotxe').html('მომართვის ავტორი');
				$('#q_mexute').html('ლოდინის დრო');
			} else {
				$('#mini_pirveli').html('ოპერატორი');
				$('#mini_meore').html('მომ. ავტორი');
				$('#mini_mesame').html('სტატუსი');

				$('#m_q_pirveli').html('პოზიცია');
				$('#m_q_meore').html('სახელი');
			}
			runAjax();
		});

		$(document).on("click", "#show_video", function() {
			$(".flash_menu_link").css('background', 'rgb(255, 255, 255)');
			$(".flash_menu_link").css('color', 'rgb(38, 129, 220)');
			$(this).css('background', '#ccc');
			$(this).css('color', '#000');
			$("#whoami").val(6);
			if ($("#check_state").val() == 0) {
				$('#pirveli').html('ქვეყანა');
				$('#meore').html('სურათი');
				$('#mesame').html('მომ. ავტორი');
				$('#meotxe').html('სტატუსი');
				$('#mexute').html('ოპერატორი');
				$('#meeqvse').html('ხ-ბა');
				$('#meshvide').html('სულ ხ-ბა');

				$('#q_pirveli').html('პოზიცია');
				$('#q_meore').html('სურათი');
				$('#q_mesame').html('ნომერი');
				$('#q_meotxe').html('მომართვის ავტორი');
				$('#q_mexute').html('ლოდინის დრო');
			} else {
				$('#mini_pirveli').html('ოპერატორი');
				$('#mini_meore').html('მომ. ავტორი');
				$('#mini_mesame').html('სტატუსი');

				$('#m_q_pirveli').html('პოზიცია');
				$('#m_q_meore').html('სახელი');
			}
			runAjax();
		});
		$(document).on("click", "#show_flesh_panel", function() {
			$("#check_state").val(0);
			$("#flesh_panel").animate({
				width: "560px"
			}, 1400);
			$('#show_flesh_panel').attr('src', 'media/images/icons/arrow_right.png');
			$('#show_flesh_panel').attr('id', 'show_flesh_panel_right');
			$('#flesh_panel_table_mini').css('display', 'none');
			$('#flesh_panel_table').css('display', 'block');
			$('#flesh_panel').css('z-index', '99');
			$('#show_flesh_panel_right').attr('title', 'პანელის დაპატარევება');
			$.session.set("checker_st", "2");

			$('#flesh_panel_table').html('<thead>' +
				'<tr>' +
				'<td colspan = "7" style=" text-align: left;">' +
				'<div id="show_station" class="flash_menu_link" style="background: #e8e8e8;padding: 9px 8px;display: inline-block;cursor:pointer;font-size:12px;font-weight:bold;font-family: pvn !important;color:#2681DC;;">ტელეფონი</div>' +
				'<div id="show_chat"  class="flash_menu_link"   style="background: #e8e8e8;padding: 9px 8px;display: inline-block;cursor:pointer;font-size:12px;font-weight:bold;font-family: pvn !important;color:#2681DC;">ჩატი</div>' +
				'<div id="show_fbm"  class="flash_menu_link"	 style="background: #e8e8e8;padding: 9px 8px;display: inline-block;cursor:pointer;font-size:12px;font-weight:bold;font-family: pvn !important;color:#2681DC;">FB მესენჯერი</div>' +
				'<div id="show_site" class="flash_menu_link"	 style="background: #e8e8e8;padding: 9px 8px;display: inline-block;cursor:pointer;font-size:12px;font-weight:bold;font-family: pvn !important;color:#2681DC;">myge messanger</div>' +
				'<div id="show_mail" class="flash_menu_link"	 style="background: #e8e8e8;padding: 9px 8px;display: inline-block;cursor:pointer;font-size:12px;font-weight:bold;font-family: pvn !important;color:#2681DC;">მეილი</div>' +
				'<div id="show_video" class="flash_menu_link"	 style="background: #e8e8e8;padding: 9px 8px;display: none;cursor:pointer;font-size:12px;font-weight:bold;font-family: pvn !important;color:#2681DC;">ვიდეო ზარი</div>' +
				'</td>' +
				'</tr>' +
				'<tr class="tb_head" style="border: 1px solid #E6E6E6;">' +
				'<td style="width:70px" id="pirveli">რიგი</td>' +
				'<td style="width:70px" id="meore">შიდა ნომერი</td>' +
				'<td style="width:50%" id="mesame">თანამშრომელი</td>' +
				'<td style="width:50px" id="meotxe">სტატუსი</td>' +
				'<td style="width:50%;" id="mexute">ნომერი</td>' +
				'<td style="width:80px;" id="meeqvse">დრო</td>' +
				'<td style="width:80px;" id="meshvide">სახელი</td>' +
				'</tr>' +
				'</thead>' +
				'<tbody id="call_ext">' +
				'</tbody>' +
				'<thead>' +
				'<tr>' +
				'<td colspan="7" style="border-left: 1px solid #E6E6E6;border-right: 1px solid #E6E6E6;"></td>' +
				'</tr>' +
				'<tr>' +
				'<td colspan="7" style="border-left: 1px solid #E6E6E6;border-right: 1px solid #E6E6E6;">რიგი</td>' +
				'</tr>' +
				'<tr class="tb_head" style="border: 1px solid #E6E6E6;">' +
				'<td id="q_pirveli">რიგი</td>' +
				'<td id="q_meore">პოზიცია</th>' +
				'<td id="q_mesame">ნომერი</td>' +
				'<td id="q_meotxe" colspan="3">პინი</td>' +
				'<td id="q_mexute">ლოდინის დრო</td>' +
				'</tr>' +
				'</thead>' +
				'<tbody id="call_queue">' +
				'</tbody>');

			if ($("#whoami").val() == 0) {
				$(this).css('color', '#2681DC');
				$(this).css('text-decoration', 'underline');

				//$(this).css('font-weight;','bold');	
				$('#show_station').css('color', '#000');
				$('#show_station').css('text-decoration', 'none');
				$('#pirveli').html('ქვეყანა');
				$('#meore').html('ბრაუზ.');
				$('#mesame').html('მომ. ავტორი');
				$('#meotxe').html('სტატუსი');
				$('#mexute').html('ოპერატორი');
				$('#meeqvse').html('ხ-ბა');
				$('#meshvide').html('სულ ხ-ბა');

				$('#q_pirveli').html('პოზიცია');
				$('#q_meore').html('ქვეყანა');
				$('#q_mesame').html('ბრაუზერი');
				$('#q_meotxe').html('მომართვის ავტორი');
				$('#q_mexute').html('ლოდინის დრო');

				$('#mini_pirveli').html('ოპერატორი');
				$('#mini_meore').html('მომ. ავტორი');
				$('#mini_mesame').html('სტატუსი');
				$('#m_q_pirveli').html('პოზიცია');
				$('#m_q_meore').html('პინი');
			} else {
				$(this).css('color', '#2681DC');
				$(this).css('text-decoration', 'underline');

				//$(this).css('font-weight;','bold');	
				$('#show_chat').css('color', '#000');
				$('#show_chat').css('text-decoration', 'none');
				$('#pirveli').html('რიგი');
				$('#meore').html('შიდა ნომერი');
				$('#mesame').html('თანამშრომელი');
				$('#meotxe').html('სტატუსი');
				$('#mexute').html('ნომერი');
				$('#meeqvse').html('დრო');
				$('#meshvide').html('პინი');

				$('#q_pirveli').html('რიგი');
				$('#q_meore').html('პოზიცია');
				$('#q_mesame').html('ნომერი');
				$('#q_meotxe').html('პინი');
				$('#q_mexute').html('ლოდინის დრო');

				$('#mini_pirveli').html('შიდა ნომერი');
				$('#mini_meore').html('მომ. ავტორი');
				$('#mini_mesame').html('სტატუსი');
				$('#m_q_pirveli').html('პოზიცია');
				$('#m_q_meore').html('ნომერი');
			}

			if ($("#whoami").val() == 2) {
				$("#show_site").click();
			} else if ($("#whoami").val() == 3) {
				$("#show_mail").click();
			} else if ($("#whoami").val() == 4) {
				$("#show_fbm").click();
			} else if ($("#whoami").val() == 0) {
				$("#show_chat").click();
			} else {
				$("#show_station").click();
			}
			runAjax();
		});

		$(document).on("click", "#show_flesh_panel_right", function() {
			$("#check_state").val(1);
			$("#flesh_panel").animate({
				width: "300px"
			}, 800);
			$('#show_flesh_panel_right').attr('src', 'media/images/icons/arrow_left.png');
			$('#show_flesh_panel_right').attr('id', 'show_flesh_panel');
			$('#flesh_panel_table_mini').css('width', '300px');
			$('#flesh_panel_table').css('display', '');
			$('#flesh_panel').css('z-index', '99');
			$('#show_flesh_panel').attr('title', 'პანელის გადიდება');
			$.session.set("checker_st", "1");
			$('#flesh_panel_table').html('<thead>' +
				'<tr>' +
				'<td colspan="4">' +
				'<table>' +
				'<tr>' +
				'<td id="show_station" class="flash_menu_link" style="padding: 0px 6px;cursor:pointer;font-size:12px;font-weight:bold;font-family: pvn !important;color:#2681DC;"><img src="media/images/icons/comunication/phone.png" height="27" width="27"></td>' +
				'<td id="show_chat"    class="flash_menu_link" style="padding: 0px 6px;cursor:pointer;font-size:12px;font-weight:bold;font-family: pvn !important;color:#2681DC;"><img src="media/images/icons/comunication/Chat.png" height="30" width="30"></td>' +
				'<td id="show_fbm"     class="flash_menu_link" style="padding: 0px 6px;cursor:pointer;font-size:12px;font-weight:bold;font-family: pvn !important;color:#2681DC;"><img src="media/images/icons/comunication/Messenger.png" height="30" width="30"></td>' +
				'<td id="show_site"   class="flash_menu_link" style="padding: 0px 6px;cursor:pointer;font-size:12px;font-weight:bold;font-family: pvn !important;color:#2681DC;"><img src="media/images/icons/comunication/my_site.ico" height="30" width="30"></td>' +
				'<td id="show_mail"    class="flash_menu_link" style="padding: 0px 6px;cursor:pointer;font-size:12px;font-weight:bold;font-family: pvn !important;color:#2681DC;"><img src="media/images/icons/comunication/E-MAIL.png" height="30" width="30"></td>' +
				'<td id="show_video"   class="flash_menu_link" style="display:none; padding: 0px 6px;cursor:pointer;font-size:12px;font-weight:bold;font-family: pvn !important;color:#2681DC;"><img src="media/images/icons/comunication/Video.png" height="30" width="30"></td>' +
				'</tr>' +
				'</table>' +
				'</td>' +
				'</tr>' +
				'<tr class="tb_head" style="border: 1px solid #E6E6E6;">' +
				'<td style="width:85px" id="mini_pirveli">შიდა ნომერი</td>' +
				'<td colspan ="2" style="width:120px" id="mini_meore">მომ. ავტორი</td>' +
				'<td style="width: 87px;" id="mini_mesame">სტატუსი</td>' +
				'</tr>' +
				'</thead>' +
				'<tbody id="mini_call_ext">' +
				'</tbody>' +
				'<thead>' +
				'<tr>' +
				'<td colspan="4" style="border-left: 1px solid #E6E6E6;border-right: 1px solid #E6E6E6;"></td>' +
				'</tr>' +
				'<tr>' +
				'<td colspan="4" style="border-left: 1px solid #E6E6E6;border-right: 1px solid #E6E6E6;">რიგი</td>' +
				'</tr>' +
				'<tr class="tb_head" style="border: 1px solid #E6E6E6;">' +
				'<td id="m_q_pirveli">პოზიცია</td>' +
				'<td colspan="3" id="m_q_meotxe">ნომერი</td>' +
				'</tr>' +
				'</thead>' +
				'<tbody id="mini_call_queue">' +
				'</tbody>');
			if ($("#whoami").val() == 0) {
				$(this).css('color', '#2681DC');
				$(this).css('text-decoration', 'underline');
				//$(this).css('font-weight;','bold');	
				$('#show_station').css('color', '#000');
				$('#show_station').css('text-decoration', 'none');
				$('#pirveli').html('ქვეყანა');
				$('#meore').html('ბრაუზ.');
				$('#mesame').html('მომ. ავტორი');
				$('#meotxe').html('სტატუსი');
				$('#mexute').html('ოპერატორი');
				$('#meeqvse').html('ხ-ბა');
				$('#meshvide').html('სულ ხ-ბა');

				$('#q_pirveli').html('პოზიცია');
				$('#q_meore').html('ქვეყანა');
				$('#q_mesame').html('ბრაუზერი');
				$('#q_meotxe').html('მომართვის ავტორი');
				$('#q_mexute').html('ლოდინის დრო');

				$('#mini_pirveli').html('ოპერატორი');
				$('#mini_meore').html('მომ. ავტორი');
				$('#mini_mesame').html('სტატუსი');
				$('#m_q_pirveli').html('პოზიცია');
				$('#m_q_meore').html('პინი');
			} else {
				$(this).css('color', '#2681DC');

				//$(this).css('font-weight;','bold');	
				$('#show_chat').css('color', '#000');
				$('#show_chat').css('text-decoration', 'none');
				$('#pirveli').html('რიგი');
				$('#meore').html('შიდა ნომერი');
				$('#mesame').html('თანამშრომელი');
				$('#meotxe').html('სტატუსი');
				$('#mexute').html('ნომერი');
				$('#meeqvse').html('დრო');
				$('#meshvide').html('პინი');

				$('#q_pirveli').html('რიგი');
				$('#q_meore').html('პოზიცია');
				$('#q_mesame').html('ნომერი');
				$('#q_meotxe').html('პინი');
				$('#q_mexute').html('ლოდინის დრო');

				$('#mini_pirveli').html('შიდა ნომერი');
				$('#mini_meore').html('მომ. ავტორი');
				$('#mini_mesame').html('სტატუსი');
				$('#m_q_pirveli').html('პოზიცია');
				$('#m_q_meore').html('ნომერი');
			}
			if ($("#whoami").val() == 2) {
				$("#show_site").click();
			} else if ($("#whoami").val() == 3) {
				$("#show_mail").click();
			} else if ($("#whoami").val() == 4) {
				$("#show_fbm").click();
			} else if ($("#whoami").val() == 0) {
				$("#show_chat").click();
			} else {
				$("#show_station").click();
			}
			runAjax();
		});

		function listen(file) {
			$('#auau').each(function() {
				this.pause(); // Stop playing
				this.currentTime = 0; // Reset time
			});
			var url = 'http://pbx.my.ge/records/' + file;
			$("#auau source").attr('src', url);
			$("#auau").load();
		}



		$(document).on("click", "#micr", function() {

			if (micr_val == 1) {
				$('#micr').attr('src', 'media/images/icons/4.png');
				$(this).attr('value', '0');
				micr_val = 0;
			} else {
				$('#micr').attr('src', 'media/images/icons/5.png');
				$(this).attr('value', '1');
				micr_val = 1;
			}

			param = new Object();
			param.act = "save_micr";
			param.activeStatus = micr_val;

			$.ajax({
				url: aJaxURL,
				data: param,
				success: function(data) {
					if (typeof(data.error) != 'undefined') {
						if (data.error != '') {} else {

						}
					}
				}
			});
		});

		$(document).on("click", "#choose_button5", function() {
			$("#choose_file1").click();
		});

		$(document).on("change", "#choose_file1", function() {
			var file = $(this).val();
			var files = this.files[0];
			var name = uniqid();


			if ($("#source_id").val() == 4) {
				var file_url = "myge_live_chat/up.php";
				var path = "files/";
			} else if ($("#source_id").val() == 6) {
				var file_url = "fbmyge/up.php";
				var path = "file/";
			} else if ($("#source_id").val() == 7) {
				var file_url = "mailmyge/up.php";
				var path = "file/";
			} else if ($("#source_id").val() == 9) {
				var file_url = "site_message_myge/up.php";
				var path = "file/";
			}


			var ext = file.split('.').pop().toLowerCase();
			if ($.inArray(ext, ['gif', 'png', 'pptx', 'webp', 'bmp', 'jpg', 'psd', 'pdf', 'docx', 'xlsx', 'xls']) == -1) { //echeck file type
				alert('This is not an allowed file type. allowed only gif, psd, png, webp, jpg, bmp, pdf, docx, xlsx');
				this.value = '';
			} else {
				file_name = files.name;
				rand_file = name + "." + ext;
				$(".progress").show();
				$.ajaxFileUpload({
					url: file_url,
					secureuri: false,
					fileElementId: "choose_file1",
					dataType: 'json',
					data: {
						act: "upload_file",
						path: path,
						file_name: name,
						chat_id: $('#chat_original_id').val(),
						type: ext,
						source: $("#source_id").val()
					},

					success: function(data, status) {
						$("#choose_file1").val('');
						console.log(data);
						file = '';
						if (data.format == '1') {
							if ($("#source_id").val() == 4) {
								ge = 'https://crm.my.ge/myge_live_chat/files/' + data['file_name'];
								ge1 = '<a href="https://crm.my.ge/myge_live_chat/files/' + data['file_name'] + '" target="_blank">' + data['file_name'] + '</a>';
								file = 'myge_live_chat/files';
							} else if ($("#source_id").val() == 6) {
								ge = 'https://crm.my.ge/fbmyge/file/' + data['file_name'];
								ge1 = '<a href="https://crm.my.ge/fbmyge/file/' + data['file_name'] + '" target="_blank">' + data['file_name'] + '</a>';
								file = 'fbmyge/file';
							} else if ($("#source_id").val() == 7) {
								ge = 'https://crm.my.ge/mailmyge/file/' + data['file_name'];
								ge1 = '<a href="https://crm.my.ge/mailmyge/file/' + data['file_name'] + '" target="_blank">' + data['file_name'] + '</a>';
								file = 'mailmyge/file';
							} else if ($("#source_id").val() == 9) {
								ge = 'https://crm.my.ge/site_message_myge/file/' + data['file_name'];
								ge1 = '<a href="https://crm.my.ge/site_message_myge/file/' + data['file_name'] + '" target="_blank">' + data['file_name'] + '</a>';
								file = 'site_message_myge/file';
							}
						} else {
							if ($("#source_id").val() == 4) {
								ge = 'https://crm.my.ge/myge_live_chat/files/' + data['file_name'];
								ge1 = `<a href="https://crm.my.ge/myge_live_chat/files/${data['file_name']}" target="_blank"><div class="hero-image" style="background-image: url('https://crm.my.ge/myge_live_chat/files/${data['file_name']}');"></div></a>`;
								file = 'myge_live_chat/files';
							} else if ($("#source_id").val() == 6) {
								ge = 'https://crm.my.ge/fbmyge/file/' + data['file_name'];
								ge1 = `<div class="hero-image" style="background-image: url('https://crm.my.ge/fbmyge/file/${data['file_name']}');" ></div>`;
								file = 'fbmyge/file';
							} else if ($("#source_id").val() == 7) {
								ge = 'https://crm.my.ge/mailmyge/file/' + data['file_name'];
								ge1 = `<div class="hero-image" style="background-image: url('https://crm.my.ge/mailmyge/file/${data['file_name']}');"></div>`;
								file = 'mailmyge/file';
							} else if ($("#source_id").val() == 9) {
								ge = 'https://crm.my.ge/site_message_myge/file/' + data['file_name'];
								ge1 = `<div class="hero-image" style="background-image: url('https://crm.my.ge/site_message_myge/file/${data['file_name']}');"></div>`;
								file = 'site_message_myge/file';
							}
						}

						ge_type = ge.slice(ge.lastIndexOf('.') - ge.length + 1);
						console.log(ge_type);
						switch (ge_type) {
							case 'png':
								ge1 = ge1;
								break;
							case 'jpg':
								ge1 = ge1;
								break;
							case 'xlsx':
								ge1 = '<a href="https://crm.my.ge/' + file + '/' + data['file_name'] + '" target="_blank"><img style="width: 50px; float: left;"  src="https://crm.my.ge/media/images/icons/xlsx_icon.png" alt="pic"></a>';
								break;
							case 'xls':
								ge1 = '<a href="https://crm.my.ge/' + file + '/' + data['file_name'] + '" target="_blank"><img style="width: 50px; float: left;"  src="https://crm.my.ge/media/images/icons/xls_icon.png" alt="pic"></a>';
								break;
							case 'pptx':
								ge1 = '<a href="https://crm.my.ge/' + file + '/' + data['file_name'] + '" target="_blank"><img style="width: 50px; float: left;"  src="https://crm.my.ge/media/images/icons/pptx_icon.png" alt="pic"></a>';
								break;
							case 'docx':
								ge1 = '<a href="https://crm.my.ge/' + file + '/' + data['file_name'] + '" target="_blank"><img style="width: 50px; float: left;"  src="https://crm.my.ge/media/images/icons/word_icon.png" alt="pic"></a>';
								break;
							case 'pdf':
								ge1 = '<a href="https://crm.my.ge/' + file + '/' + data['file_name'] + '" target="_blank"><img style="width: 50px; float: left;"  src="https://crm.my.ge/media/images/icons/pdf_icon.png" alt="pic"></a>';
								break;

							default:
								ge1 = '<a href="https://crm.my.ge/' + file + '/' + data['file_name'] + '" target="_blank"><img style="width: 50px; float: left;"  src="https://crm.my.ge/media/images/icons/file_icons.png" alt="pic"></a>';
						}
						console.log(ge1);

						// 				if($("#source_id").val() != 4){
						// 					$("#send_message").html($("#send_message").html()+" "+ge);
						// 				}
						// if($("#source_id").val() != 7){
						// 	$("#log").append('<tr style="height: 17px;"><td colspan=2 style="font-weight:bold; word-break: break-word; float: right; margin-right: 5px;">'+$('#chat_user_id').attr('chat_user_name')+'</td></tr><tr style="height: 17px;"><td colspan=2 style=" word-break: break-word; float: right; margin-right: 5px;">'+$("#send_message").html()+" "+ge1+'</td></tr><tr><td colspan=2 style="border-bottom:2px dotted #DBDBDB;text-align: right;"><p style="color: #878787; width: 100%;">'+getDateTime()+'</p></td></tr>');
						// }
						// if(data.format != '1' && $("#source_id").val() == 6){
						// 	sendSms($("#send_message").html(),0, ge);
						// 	sendSms(ge,1);
						// }else{
						// 	$text_message = $("#send_message").html()+" "+ge;
						// 	sendSms($text_message,'');
						// }

						$text_message = $("#send_message").html() + " " + ge;

						if ($("#send_message").html().replace(/\s/g, '').length == 0) { // $("#source_id").val() == 7 &&
							$text_message = '<a style="display:none"></a>';
						}
						// if($("#source_id").val() == 7){
						a_teg = '<div><div class="my_mail_file" file_name="' + file_name + '" style="float:left;" link="' + ge + '" >' + ge1 + '<div class="file_text" style="display: contents;float:right;margin-top:15px;"><span>' + file_name + '</span></div>' + '<span class="delete_upload_file">x</span></div></div> '; // link="'+ge+'"
						// if($("#source_id").val() == 6){
						// 	var val = $("#send_message").html();
						// 	 $("#send_message").html(` ${val} ${ge} `);
						// }
						// else {
						attach_html = $("#email_attachment_files").html() + a_teg;
						$("#email_attachment_files").html(attach_html);
						$("#email_attachment_files").css("height", "47%");
						$("#send_message").css("height", "70px");
						// }
						// }else{
						// 	sendSms($text_message,'', ge, file_name);
						// }
						jQuery("time.timeago").timeago();
						document.getElementById('chat_scroll').scrollTop = 25000000000;
						$(".progress").hide();
					},
					error: function(data, status, e) {
						//alert(e);
					}
				});
			}
		});

		// toggle dnd button
		$(document).on("click", "#incallToggleDnd", function() {

			// define extention from php session
			const phpSesExten = '<?php echo $_SESSION['EXTENSION']; ?>';

			if (phpSesExten === 0 || phpSesExten === "0" || phpSesExten === undefined || phpSesExten === null) {
				alert("თქვენ არ გაქვთ არჩეული ექსთენშენი, ამიტომ მოქმედების განხორციელება შეუძლებელია");
			} else {
				var activeStatus = $("#dnd_on_local_status").val();
				if (activeStatus == 0) {
					$("#dnd_on_local_status").val(1)
					var action = 'local';
					activeStatus = 1;
				} else if (activeStatus == 1) {
					$("#dnd_on_local_status").val(2)
					var action = 'dndon';
					activeStatus = 2;
				} else if (activeStatus == 2) {
					$("#dnd_on_local_status").val(0)
					var action = 'dndoff';
					activeStatus = 0;
				}

				param = new Object();
				param.act = "change_incall_dnd_status";
				param.activeStatus = $("#dnd_on_local_status").val();

				$.ajax({
					url: aJaxURL,
					data: param,
					success: function(data) {
						if (typeof(data.error) != 'undefined') {
							if (data.error != '') {} else {
								//     					$.post("AsteriskManager/dndAction.php", {
								//     						action: action
								//     					}, result => {
								if (activeStatus == 0) {
									$("#incallToggleDnd").attr('src', 'media/images/icons/comunication/Phone_ON.png');
								} else if (activeStatus == 1) {
									$("#incallToggleDnd").attr('src', 'media/images/icons/comunication/Phone_INSIDE.png');
								} else if (activeStatus == 2) {
									$("#incallToggleDnd").attr('src', 'media/images/icons/comunication/Phone_OFF.png');
								}
								//});
							}
						}
					}
				});
			}
		});

		// open activitie chooe container
		$(document).on("click", ".callapp-op-activitie-wrapper", function(event) {

			// stop propagation
			event.stopPropagation();

			// define open status
			const openStatus = $("#callapp_op_activitie_choose").data("active");

			// check active status
			if (openStatus) {
				$("#callapp_op_activitie_choose").data("active", false).removeClass("on");
			} else {
				$("#callapp_op_activitie_choose").data("active", true).addClass("on");
			}

		});

		// choose activitie
		$(document).on("click", "#callapp_op_activitie_choose li:not(.is-active)", function() {

			// reactivate activitie in list
			$("#callapp_op_activitie_choose li").removeClass("is-active");
			$(this).addClass("is-active");

			// define activitie data
			$D.chosedActivitieId = $(this).data("id");
			const activitieColor = $(this).data("color");
			const activitieName = $(this).find(".activitie-name").text();

			// define request data
			const data = {
				act: "change_incall_activitie_status",
				activitieId: $D.chosedActivitieId,
				type: "on"
			};

			// send ajax request
			$.post(aJaxURL, data, responce => {

				// restruct responce
				const {
					error
				} = responce;

				// check for error
				if (error !== "") {
					alert(error);
				} else {

					// change color label
					$(".callapp-op-activitie-color").css({
						background: activitieColor
					});

					// fix selected activitie
					$(".callapp-op-activitie-choose-receiver").text(activitieName);

					// start time count
					$D.activitieOnTimer();

					// chat status off
					toggleChatStatus('#chat_status', 1);

					// on dnd
					$.post("AsteriskManager/dndAction.php", {
						action: "dndon"
					});
				}

			});

		});

		// zero fixer
		function zeroFixer(a) {
			return a < 10 ? `0${a}` : a;
		}

		$(document).on("click", ".choose_shablon", function() {
			var shablon_html = $(this).parent().parent().children()[2].textContent;
			$("#input").html(shablon_html);
			$("#add-edit-form-mail-shablon").dialog('close');
		});

		//open template dialog
		$(document).on("click", "#smsTemplate", function() {

			//request data
			let data = {
				act: "get_sms_template_dialog"
			};

			//buttons
			let buttons = {
				"cancel": {
					text: "დახურვა",
					id: "cancel-dialog",
					click: function() {
						$(this).dialog("close");
						$(this).html("");
					}
				}
			};

			//ajax request
			$.post("server-side/call/incomming.action.php", data, structure => {

				//open dialog malibu
				$("#sms-templates").html(structure.html);
				GetDialog("sms-templates", "530", "auto", buttons, "right+100 top");

				GetDataTable("sms_template_table", aJaxURL, "phone_directory", 4, "", 0, "", 2, "desc", '', "<'F'lip>");
				$("#sms_template_table_length").css('top', '0px');
			});

		});

		// download sms shablon
		$(document).on("click", ".download_shablon", function() {

			//define variables
			var message = $(this).data("message");
			var sms_id = $(this).attr("sms_id");

			//check length of message text
			if (message != "" && message.length <= 150) {

				$('#newSmsText').val(message);
				$('#smsCharCounter').val((message.length) + '/150');

				// set choosed sms id
				$D.choosedSmsId = sms_id;

			} else if (message.length > 150) {
				alert("შაბლონის ტექსტი შეიცავს დასაშვებზე (150) მეტ სიმბოლოს (" + message.length + ")");
			}

			//close sms template dialog
			$("#sms-templates").dialog("close");

		});


		// send sms
		$(document).on("click", "#sendNewSms", function() {
			// define send type
			const sendType = $(this).data("type");
			// define request data
			const data = {
				act: `send_sms_${sendType}`,
				incommingId: $("#incomming_call_id").val(),
				smsId: $D.choosedSmsId,
				smsText: $("#newSmsText").val()
			};

			// check type of request malibu

			data.phone = $("#smsAddresseeByPhone").val();


			// send ajax request
			$.post(aJaxURL_sms, data, result => {
				if (result) {
					alert("SMS წარმატებით გაიგზავნა");
				} else {
					alert("SMS გაგზავნა ვერ განხორციელდა");
				}
				$("#add-edit-form-sms").dialog("close");
				GetDataTable("table_sms", aJaxURL, "get_list_sms", 5, "&incomming_id=" + $('#incomming_call_id').val(), 0, "", 2, "desc", '', "<'F'lip>");
				$("#table_sms_length").css('top', '0px');
			});
			$("#smsAddresseeByPhone").val("");
			$("#newSmsText").val("");

		});
		//new sms textarea key up
		$(document).on("keyup", "#newSmsText", function() {

			charCounter(this, "#smsCharCounter");

		});

		//new sms textarea focus
		$(document).on("keydown", "#newSmsText", function(e) {

			if (e.key !== "Backspace" && e.key !== "Delete") {
				textAreaFocusAllowed(this, "#smsCharCounter");
			}

		});
		//textarea character counter
		function charCounter(checkobj, updateobj) {

			var textLength = $(checkobj).val().length;
			var charLimit = $(updateobj).data("limit");

			$(updateobj).val(`${textLength}/${charLimit}`);

			if (textLength === charLimit) {
				$(checkobj).blur();
			}

		}

		//text area focus allowed
		function textAreaFocusAllowed(checkobj, limitinput) {

			var textLength = $(checkobj).val().length;
			var charLimit = $(limitinput).data("limit");

			if (textLength >= charLimit) {
				$(checkobj).blur();
			}

		}

		$(document).on("click", "#web_chat_checkbox", function() {
			if ($("input[id='web_chat_checkbox']:checked").val() == 1) {
				$("#VebChatImg").attr('src', 'media/images/icons/comunication/Chat.png');
			} else {
				$("#VebChatImg").attr('src', 'media/images/icons/comunication/Chat_OFF.png');
			}
		});

		$(document).on("click", "#site_chat_checkbox", function() {
			if ($("input[id='site_chat_checkbox']:checked").val() == 1) {
				$("#siteImg").attr('src', 'media/images/icons/comunication/my_site.ico');
			} else {
				$("#siteImg").attr('src', 'media/images/icons/comunication/OFF-my.png');
			}
		});

		$(document).on("click", "#messanger_checkbox", function() {
			if ($("input[id='messanger_checkbox']:checked").val() == 1) {
				$("#MessengerImg").attr('src', 'media/images/icons/comunication/Messenger.png');
			} else {
				$("#MessengerImg").attr('src', 'media/images/icons/comunication/Messenger_OFF.png');
			}
		});

		$(document).on("click", "#mail_chat_checkbox", function() {
			if ($("input[id='mail_chat_checkbox']:checked").val() == 1) {
				$("#MailImg").attr('src', 'media/images/icons/comunication/E-MAIL.png');
			} else {
				$("#MailImg").attr('src', 'media/images/icons/comunication/E-MAIL_OFF.png');
			}
		});

		$(document).on("click", "#zomm_div", function() {
			if ($(this).attr('check_zoom') == 0) {
				$(this).attr('check_zoom', '1');
				$(this).html('<<');
				$("#right_side").css('z-index', '-1');
				$("#chat_scroll").css('width', '907px');
			} else {
				$(this).attr('check_zoom', '0');
				$(this).html('>>');
				$("#right_side").css('z-index', '0');
				$("#chat_scroll").css('width', '288px');
			}
		});


		$(document).on("click", ".comunication_checkbox", function() {
			param = new Object();
			param.act = "save_user_comunication";

			param.web_chat_checkbox = $("input[id='web_chat_checkbox']:checked").val();
			param.site_chat_checkbox = $("input[id='site_chat_checkbox']:checked").val();
			param.messanger_checkbox = $("input[id='messanger_checkbox']:checked").val();
			param.mail_chat_checkbox = $("input[id='mail_chat_checkbox']:checked").val();
			param.video_call_checkbox = $("input[id='video_call_checkbox']:checked").val();

			$.ajax({
				url: aJaxURL,
				data: param,
				success: function(data) {
					if (typeof(data.error) != 'undefined') {
						if (data.error != '') {} else {}
					}
				}
			});
		});

		// $(document).on("change", "#from_mail_id", function() {
		// 	param 	  = new Object();
		// 	param.act = "get_signature";

		// 	param.site_id   = $(this).val();

		// 	$.ajax({
		//         url: aJaxURL,
		// 	    data: param,
		//         success: function(data) {
		// 			if(typeof(data.error) != 'undefined'){
		// 				if(data.error != ''){
		// 				}else{
		// 					old_text = $("#input").val().split("<br>");
		// 					text     = old_text[1];
		// 					$("#input").html(text+"<br><br><br><br>"+data.signature);
		// 					setTimeout(() => {
		// 						var activeEditor = tinyMCE.get('input');
		// 						var content = text+"<br><br><br><br>"+data.signature;
		// 						activeEditor.setContent(content);
		// 				    }, 100);
		// 				}
		// 			}
		// 	    }
		//     });
		// });

		function check_user_comunication() {
			param = new Object();
			param.act = "check_user_comunication";

			$.ajax({
				url: aJaxURL,
				data: param,
				success: function(data) {
					if (typeof(data.error) != 'undefined') {
						if (data.error != '') {} else {
							if (data.web_chat_checkbox == 1) {
								$('#web_chat_checkbox').attr('checked', 'checked');
								$("#VebChatImg").attr('src', 'media/images/icons/comunication/Chat.png');
							}

							if (data.site_chat_checkbox == 1) {
								$('#site_chat_checkbox').attr('checked', 'checked');
								$("#siteImg").attr('src', 'media/images/icons/comunication/my_site.ico');
							}

							if (data.messanger_checkbox == 1) {
								$('#messanger_checkbox').attr('checked', 'checked');
								$("#MessengerImg").attr('src', 'media/images/icons/comunication/Messenger.png');
							}

							if (data.mail_chat_checkbox == 1) {
								$('#mail_chat_checkbox').attr('checked', 'checked');
								$("#MailImg").attr('src', 'media/images/icons/comunication/E-MAIL.png');
							}

							if (data.video_call_checkbox == 1) {
								$('#video_call_checkbox').attr('checked', 'checked');
								$("#VideoImg").attr('src', 'media/images/icons/comunication/Video.png');
							}
						}
					}
				}
			});
		}

		$(document).on("click", "#add_mail", function() {
			param = new Object();
			param.act = "send_mail";
			param.call_type = 'inc';
			param.outgoing_id = $('#outgoing_id').val();
			$.ajax({
				url: aJaxURL_mail,
				data: param,
				success: function(data) {
					$("#add-edit-form-mail").html(data.page);
					$("#smsPhoneDir").button();
					$("#email_shablob,#choose_button_mail,#send_email").button();

				}
			});
			var buttons = {
				"cancel": {
					text: "დახურვა",
					id: "cancel-dialog",
					click: function() {
						$(this).dialog("close");
						$(this).html("");
					}
				}
			};
			GetDialog("add-edit-form-mail", 640, "auto", buttons, 'center top');
			setTimeout(() => {
				$("#from_mail_id").chosen({
					search_contains: true
				});
			}, 150);
			$("#smsPhoneDir").button();
		});

		$(document).on("dialogopen", "#add-edit-form-mail", function() {
			$("#email_shablob,#choose_button_mail,#send_email,#smsPhoneDir").button();
			$("#signature").chosen({
				search_contains: true
			});
			setTimeout(() => {
				//     tinymce.init({
				//         selector: '#input',
				//         width: "580px",
				//         height: "200px",
				//         cssclass: 'te',
				//         controlclass: 'tecontrol',
				//         dividerclass: 'tedivider',
				//         controls: ['bold', 'italic', 'underline', 'strikethrough', '|', 'subscript', 'superscript', '|',
				//             'orderedlist', 'unorderedlist', '|', 'outdent', 'indent', '|', 'leftalign',
				//             'centeralign', 'rightalign', 'blockjustify', '|', 'unformat', '|', 'undo', 'redo', 'n',
				//             'font', 'size', '|', 'image', 'hr', 'link', 'unlink', '|', 'print'],
				//         footer: true,
				//         fonts: ['Verdana', 'Arial', 'Georgia', 'Trebuchet MS'],
				//         xhtml: true,
				//         bodyid: 'editor',
				//         footerclass: 'tefooter',
				//         resize: {cssclass: 'resize'}
				//     });
				$("#input").cleditor();
			}, 100);


		});

		$(document).on('dialogclose', "#add-edit-form-mail", function(event) {
			tinymce.remove("#input");
		});

		$(document).on("click", "#smsPhoneDir", () => {

			//define data
			let data = {
				act: "phone_dir_list",
				type: 2
			};

			//buttons
			let buttons = {
				"choose": {
					text: "არჩევა",
					id: "choose-abonents",
					click: function() {

						//execute function whitch collects checked items value
						choosePhoneFromDir(this);

					}
				},
				"cancel": {
					text: "დახურვა",
					id: "cancel-dialog",
					click: function() {
						$(this).dialog("close");
						$(this).html("");
					}
				}
			};

			//send dialog opening request to server side
			$.post(aJaxURL, data, result => {

				//insert received html structure in relevant dialog
				$("#phone-directory-list").html(result.html);

				//open relevant dialog
				GetDialog("phone-directory-list", 960, "auto", buttons, 'center top');
				GetDataTable("phoneDirectoryList", aJaxURL, "get_phone_dir_list", 4, "&type=" + $("#view_type").val(), 0, "", 2, "desc", '', "");
			});

		});

		//get conclusion container data
		function getConclusionContainerData(input) {

			// define variables
			const inputWidth = $(input).outerWidth();
			const charLength = $(input).text().length;
			const nextInput = $(input).next("br").next(".input-div");
			const nextLine = nextInput.length;

			// return data
			return {
				nextInput,
				nextLine,
				inputWidth,
				charLength
			}

		}
		$(document).on("click", ".itm-unit:not(.itm-unit-active)", function() {
			//define variables
			let selector = $(this).data("selector");

			//deactivate all menu items and activate relevant one
			$(".itm-unit").removeClass("itm-unit-active");
			$(this).addClass("itm-unit-active");

			//deactivate info sections and activate menu relevant one
			$(".inc-info-section").removeClass("is-active");
			$(`.inc-info-section[data-selector='${selector}']`).addClass("is-active");

			//stylize chosen objects
			setTimeout(function() {
				$(".iiss-sc-unit").find(".chosen-container").css({
					width: "100%"
				});
			}, 10);
		});

		function infoEmailButInit(category) {

			let modifiedCategory = category.charAt(0).toUpperCase() + category.slice(1);
			return `mailTo${modifiedCategory}`;

		}
		//function which chooses numbers which was selected in phone directory
		function choosePhoneFromDir(dialogobj) {

			//define variables
			let checkBoxCollector = [];
			let uncheckedCollector = [];
			var receiver = '';
			if ($("#view_type").val() == 1) {
				var receiver = $("#smsAddressee");
			} else {
				var receiver = $("#mail_address");
			}

			let receiverData = receiver.val();
			let returnData;

			//collect checkbox checked values
			$(".pdl-check").each(function(i) {

				//check if checkbox is checked
				if ($(this).is(":checked")) {

					//get value of checked inut and reach to data collector
					checkBoxCollector.push($(this).val().trim());
				}

			});

			//collect checkbox unchecked values
			$(".pdl-check").each(function(i) {

				//check if checkbox is checked
				if (!$(this).is(":checked")) {

					//get value of checked inut and reach to data collector
					uncheckedCollector.push($(this).val().trim());
				}

			});
			//check if there is already data in receiver input
			if (receiverData !== "") {

				checkBoxCollector = checkBoxCollector.concat(receiverData.split(",").filter(function(item) {
					return checkBoxCollector.indexOf(item.trim()) < 0;
				}));

			}

			//filter data by unchecked box values
			checkBoxCollector = checkBoxCollector.filter(function(item) {
				return uncheckedCollector.indexOf(item) < 0;
			});

			//join array with comma
			returnData = checkBoxCollector.join();

			//send collected data to receiver
			receiver.val(returnData);

			//close dialog of phone directory list
			$(dialogobj).dialog("close");

		}

		$(document).on("click", "#choose_button_mail", function() {
			$("#choose_mail_file").click();
		});

		$(document).on("change", "#choose_mail_file", function() {
			var file_url = $(this).val();
			var file_name = this.files[0].name;
			var file_size = this.files[0].size;
			var file_type = file_url.split('.').pop().toLowerCase();
			var path = "../../mailmyge/file_attachment/";

			if ($.inArray(file_type, ['pdf', 'pptx', 'png', 'xls', 'xlsx', 'jpg', 'docx', 'doc', 'csv']) == -1) {
				alert("დაშვებულია მხოლოდ 'pdf', 'png', 'xls', 'xlsx', 'jpg', 'docx', 'doc', 'csv' გაფართოება");
			} else if (file_size > '15728639') {
				alert("ფაილის ზომა 15MB-ზე მეტია");
			} else {
				$.ajaxFileUpload({
					url: "server-side/upload/file.action.php",
					secureuri: false,
					fileElementId: "choose_mail_file",
					dataType: 'json',
					data: {
						act: "upload_file_mail",
						button_id: "choose_mail_file",
						table_name: 'mail',
						file_name: Math.ceil(Math.random() * 99999999999),
						file_name_original: file_name,
						file_type: file_type,
						file_size: file_size,
						path: path,
						table_id: $("#hidden_increment").val(),

					},
					success: function(data) {
						if (typeof(data.error) != 'undefined') {
							if (data.error != '') {
								alert(data.error);
							} else {
								var tbody = '';
								$("#choose_mail_file").val('');
								for (i = 0; i <= data.page.length; i++) {
									tbody += "<div id=\"first_div\">" + data.page[i].file_date + "</div>";
									tbody += "<div id=\"two_div\">" + data.page[i].name + "</div>";
									tbody += "<a id=\"tree_div\" download href=\'media/uploads/file/" + data.page[i].rand_name + "\'>ჩამოტვირთვა</a>";
									tbody += "<div id=\"for_div\" onclick=\"delete_file1('" + data.page[i].id + "')\">-</div>";
									tbody += "<input type='hidden' class=\"attachment_address\" value='" + data.page[i].rand_name + "'>";
									$("#paste_files1").html(tbody);
								}


							}
						}

					}
				});
			}
		});

		function delete_file1(id) {
			$.ajax({
				url: "server-side/upload/file.action.php",
				data: "act=delete_file1&file_id=" + id + "&table_name=mail",
				success: function(data) {

					var tbody = '';
					if (data.page.length == 0) {
						$("#paste_files1").html('');
					} else {
						for (i = 0; i <= data.page.length; i++) {
							tbody += "<div id=\"first_div\">" + data.page[i].file_date + "</div>";
							tbody += "<div id=\"two_div\">" + data.page[i].name + "</div>";
							tbody += "<a id=\"tree_div\" download href=\'media/uploads/file/" + data.page[i].rand_name + "\'>ჩამოტვირთვა</a>";
							tbody += "<div id=\"for_div\" onclick=\"delete_file1('" + data.page[i].id + "')\">-</div>";
							tbody += "<input type='hidden' class=\"attachment_address\" value='" + data.page[i].rand_name + "'>";
							$("#paste_files1").html(tbody);
						}
					}
				}
			});
		}

		$(document).on("click", "#email_shablob", function() {
			param = new Object();
			param.act = "send_mail_shablon";
			param.mail_id = $("#from_mail_id").val();
			$.ajax({
				url: aJaxURL_mail,
				data: param,
				success: function(data) {
					$("#add-edit-form-mail-shablon").html(data.page);
					GetDataTable("example_shablon_table", aJaxURL_mail, "get_list_shablon", 5, "site=" + $("#my_site").chosen().val(), 0, "", 2, "desc", '', "<'F'lip>");
					$("#example_shablon_table_length").css('top', '0px');
				}
			});
			var buttons = {
				"cancel": {
					text: "დახურვა",
					id: "cancel-dialog",
					click: function() {
						$(this).dialog("close");
						$(this).html("");
					}
				}
			};
			GetDialog("add-edit-form-mail-shablon", 1200, "auto", buttons, 'left top');



		});

		function pase_body(id, head) {
			$('#mail_text').val(head);
			$("iframe").contents().find("body").html($('#' + id).html());
			$('#add-edit-form-mail-shablon').dialog('close');
		}

		$(document).on("click", "#send_email", function() {
			param = new Object();
			param.from_mail_id = $("#from_mail_id").val();
			param.source_id = $("#source_id").val();
			param.address = $("#mail_address").val();
			param.cc_address = $("#mail_address1").val();
			param.bcc_address = $("#mail_address2").val();
			param.subject = $("#mail_text").val();
			param.send_mail_id = $("#send_email_hidde").val();
			param.incomming_call_id = $("#incomming_call_id").val();
			param.hidden_increment = $("#hidden_increment").val();
			param.body = $("#input").html();
			var attachments = [];
			var counter = 0;

			$(".attachment_address").each(function() {
				attachments[counter] = $(this).val();
				counter++;
			});

			param.attachments = attachments.join(",");

			$.ajax({
				url: aJaxURL_getmail,
				data: param,
				success: function(data) {
					if (data.status == 2) {
						alert('შეტყობინება წარმატებით გაიგზავნა!');
						$("#mail_text").val('');
						$("iframe").contents().find("body").html('');
						$("#file_div_mail").html('');
						$("#add-edit-form-mail").dialog("close");
						$("#add-edit-form-mail").html('');
						GetDataTable("table_mail", aJaxURL, "get_list_mail", 5, 'incomming_id=' + $('#incomming_call_id').val(), 0, "", 0, "desc", '', "<'F'lip>");
						setTimeout(function() {
							$('.ColVis, .dataTable_buttons').css('display', 'none');
						}, 160);
						$("#table_mail_length").css('top', '0px');
					} else {
						// alert('შეტყობინება არ გაიგზავნა!');
					}
				}
			});
		});

		//chat question functions

		$(document).on("click", "#add_comment", function() { // save answer 
			param = new Object();
			param.act = 'save_comment';
			param.comment = $("#add_ask").val();
			param.incomming_call_id = $("#incomming_call_id").val();
			if (param.comment != "") {
				$.ajax({
					url: aJaxURL,
					data: param,
					success: function(data) {
						param2 = new Object();
						param2.act = 'get_add_question';
						param2.incomming_call_id = $("#incomming_call_id").val();
						$.ajax({
							url: aJaxURL,
							data: param2,
							success: function(data) {
								$("#chat_question").html(data.page);
								$("#add_comment").button();
							}
						});
					}
				});
			}
		});
		$(document).on("click", "#get_answer", function() { // get answer page

			param = new Object();
			param.act = 'get_add_page';
			comment_id = $(this).attr('class');
			$.ajax({
				url: aJaxURL_answer,
				data: param,
				success: function(data) {
					$("#add-edit-form-answer").html(data.page);

					var buttons = {
						"save": {
							text: "შენახვა",
							id: "save-dialog-answer",
						},
						"cancel": {
							text: "დახურვა",
							id: "cancel-dialog-answer",
							click: function() {
								$(this).dialog("close");
								$(this).html("");
								//$('#yesnoclose').dialog("close");
							}
						}
					};
					GetDialog("add-edit-form-answer", 357, "auto", buttons, "top");
					$("#add_comment").button();
				}
			});
		});
		$(document).on("click", "#save-dialog-answer", function() {
			// save answer 
			if ($("#comment_info_sorce_id").val() == "") {

				param = new Object();
				param.act = 'save_answer';
				param.comment = $("#in_answer").val();
				param.comment_id = $('#hidden_comment_id_' + comment_id + '').val();
				param.incomming_call_id = $("#incomming_call_id").val();
				if (param.comment != "") {
					$.ajax({
						url: aJaxURL_answer,
						data: param,
						success: function(data) {
							param2 = new Object();
							param2.act = 'get_add_question';
							param2.incomming_call_id = $("#incomming_call_id").val();
							$.ajax({
								url: aJaxURL,
								data: param2,
								success: function(data) {
									$("#add-edit-form-answer").dialog("close");
									$("#chat_question").html(data.page);
									$("#add_comment").button();
								}
							});
						}
					});
				} else {
					alert("შეავსეთ ველი")
				}
			} else {

				param2 = new Object();
				param2.act = 'update_comment';
				param2.id = edit_comment_id;
				param2.comment = $("#in_answer").val();
				$.ajax({
					url: aJaxURL,
					data: param2,
					success: function(data) {
						param2 = new Object();
						param2.act = 'get_add_question';
						param2.incomming_call_id = $("#incomming_call_id").val();
						$.ajax({
							url: aJaxURL,
							data: param2,
							success: function(data) {
								$("#add-edit-form-answer").dialog("close");
								$("#chat_question").html(data.page);
								$("#add_comment").button();
							}
						});
					}
				});
			}
		});

		$(document).on("click", "#delete_comment", function() { // save answer 
			param = new Object();
			param.act = 'delete_comment';
			param.comment_id = $(this).attr('my_id');
			$.ajax({
				url: aJaxURL,
				data: param,
				success: function(data) {
					param2 = new Object();
					param2.act = 'get_add_question';
					param2.incomming_call_id = $("#incomming_call_id").val();
					$.ajax({
						url: aJaxURL,
						data: param2,
						success: function(data) {
							$("#chat_question").html(data.page);
							$("#add_comment").button();
						}
					});
				}
			});
		});
		$(document).on("click", "#edit_comment", function() { // save answer 
			param = new Object();
			param.act = 'get_edit_page';
			edit_comment_id = $(this).attr('my_id');
			param.id = $(this).attr('my_id');
			$.ajax({
				url: aJaxURL_answer,
				data: param,
				success: function(data) {
					$("#add-edit-form-answer").html(data.page);
					var buttons = {
						"save": {
							text: "შენახვა",
							id: "save-dialog-answer",
						},
						"cancel": {
							text: "დახურვა",
							id: "cancel-dialog-answer",
							click: function() {
								$(this).dialog("close");
								$(this).html("");
								//$('#yesnoclose').dialog("close");
							}
						}
					};
					GetDialog("add-edit-form-answer", 357, "auto", buttons, "top");
				}
			});
		});

		function get_sub_tab4(id, control_tab) {
			document.getElementById('control_tab_4_0').style.display = "none";
			document.getElementById('control_tab_4_1').style.display = "none";
			document.getElementById(control_tab).style.display = "block";

			document.getElementById("control_tab_id_4_0").style.borderBottomWidth = "1px";
			document.getElementById("control_tab_id_4_0").style.borderBottomColor = "gray";

			document.getElementById("control_tab_id_4_1").style.borderBottomWidth = "1px";
			document.getElementById("control_tab_id_4_1").style.borderBottomColor = "gray";

			document.getElementById(id).style.borderBottomWidth = "2px";
			document.getElementById(id).style.borderBottomColor = "#0b5c9a";
			document.getElementById(id).children[0].style.color = "black";


		}
		//chat question functions END
	</script>
	<style type="text/css">
		.callapp_tabs {
			margin-top: 5px;
			margin-bottom: 5px;
			float: right;
			width: 100%;

		}

		.callapp_tabs span {
			color: #FFF;
			border-radius: 5px;
			padding: 5px;
			float: left;
			margin: 0 3px 0 3px;
			background: #2681DC;
			font-weight: bold;
			font-size: 11px;
			margin-bottom: 2px;
		}

		.callapp_tabs span close {
			cursor: pointer;
			margin-left: 5px;
		}

		.callapp_head {
			font-family: pvn;
			font-weight: bold;
			font-size: 20px;
			color: #2681DC;
		}

		.callapp_head select {
			position: relative;
			top: -2px;
		}

		#chatcontent hr {
			border-top-width: 0px;
		}

		.callapp_head_hr {
			border: 1px solid #2681DC;
		}

		.callapp_refresh {
			padding: 5px;
			border-radius: 8px;
			color: #FFF;
			background: #9AAF24;
			float: right;
			font-size: 13px;
			cursor: pointer;
		}

		.comunication_button {
			padding: 5px;
			border-radius: 8px;
			float: right;
			font-size: 13px;
			cursor: pointer;
		}

		.callapp_dnd_button {
			width: 40px;
			height: 40px;
			background: none;
			border: none;
			cursor: pointer;
			margin: 0 5px;
			position: relative;
			color: #7f8c8d;
			font-size: 21px;
		}

		.callapp_filter_show {
			margin-bottom: 50px;
			float: right;
			width: 100%;
		}

		.callapp_filter_show button {
			margin-bottom: 10px;
			border: none;
			color: #2681DC;
			font-weight: bold;
			cursor: pointer;
		}

		.callapp_filter_body {
			width: 1030px;
			height: 10px;
			padding: 5px;
			margin-bottom: 0px;
		}

		.callapp_filter_body span {
			float: left;
			margin-right: 10px;
			height: 22px;
		}

		.callapp_filter_body span label {
			color: #555;
			font-weight: bold;
			margin-left: 20px;
		}

		.callapp_filter_body_span_input {
			position: relative;
			top: -15px;
		}

		.callapp_filter_header {
			color: #2681DC;
			font-family: pvn;
			font-weight: bold;
		}

		.ColVis,
		.dataTable_buttons {
			z-index: 50;
		}

		#flesh_panel {
			height: auto;
			width: 300px;
			position: absolute;
			top: 0;
			padding: 15px;
			right: 2px;
			z-index: 49;
			background: #FFF;
		}

		#table_index_wrapper #table_index_filter {
			width: 56%;

		}

		.callapp_filter_show button {
			margin-bottom: 0;
		}

		#table_right_menu {
			top: 39px;
			z-index: 1;
		}

		#table_index tbody td:last-child {
			padding: 0;
		}

		#table_index_wrapper .ui-widget-header {
			height: 55px;
		}

		/* -SMS DIALOG STYLES- */
		.new-sms-row {
			width: 100%;
			height: auto;
			margin-top: 11.3px;
			box-sizing: border-box;
		}

		.new-sms-row:last-child {
			margin-top: 4px;
		}

		.new-sms-row.grid {
			display: grid;
			grid-template-columns: repeat(2, 50%);
		}

		.nsrg-col {
			padding-right: 10px;
		}

		.nsrg-col:last-child {
			padding-right: 0;
		}

		.new-sms-input-holder {
			width: 100%;
			min-height: 27px;
			display: flex;
		}

		.new-sms-input-holder input {
			height: 18px;
			flex-grow: 1;
			font-size: 11.3px;
		}

		.new-sms-input-holder textarea {
			height: 75%;
			min-height: 20px;
			flex-grow: 1;
			font-size: 11.3px;
			resize: vertical;
		}

		#smsCopyNum {
			/*button: copy*/
			margin: 0 5px;
		}

		#smsTemplate {
			/*button: open template*/
			margin-right: 0;
		}

		#smsNewNum i {
			/*icon: add new number in send new sms child*/
			font-size: 14px;
		}

		#smsCharCounter {
			/*input: sms character counter*/
			width: 55px;
			height: 18px;
		}

		#smsCharCounter {
			/*input: send new sms character counter*/
			position: relative;
			top: 0;
			text-align: center;
		}

		#sendNewSms {
			/*button: send new message action inplement*/
			float: right;
		}

		.empty-sms-shablon {
			width: 100%;
			height: auto;
			padding: 10px 0;
			text-align: center;
			font-family: pvn;
			font-weight: bold;
		}

		#box-table-b1 {
			font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
			font-size: 12px;
			text-align: center;
			border-collapse: collapse;
			border-top: 7px solid #70A9D2;
			border-bottom: 7px solid #70A9D2;
			width: 300px;
		}

		#box-table-b th {
			font-size: 13px;
			font-weight: normal;
			padding: 8px;
			background: #E8F3FC;
			;
			border-right: 1px solid #9baff1;
			border-left: 1px solid #9baff1;
			color: #4496D5;
		}

		#box-table-b1 th {
			font-size: 13px;
			font-weight: normal;
			padding: 8px;
			background: #E8F3FC;
			;
			border-right: 1px solid #9baff1;
			border-left: 1px solid #9baff1;
			color: #4496D5;
		}

		#box-table-b td {
			padding: 8px;
			background: #e8edff;
			border-right: 1px solid #aabcfe;
			border-left: 1px solid #aabcfe;
			color: #669;
		}

		#box-table-b1 td {
			padding: 8px;
			background: #e8edff;
			border-right: 1px solid #aabcfe;
			border-left: 1px solid #aabcfe;
			color: #669;
		}

		.download_shablon {

			width: 100%;
			height: 20px;
			background-color: #4997ab;
			border-radius: 2px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff;
			font-family: arial;
			font-size: 14px;
			border: 0px;
			text-decoration: none;
			text-overflow: ellipsis;
		}

		#comucation_status {
			position: absolute;
			z-index: 25000000;
			background: #fff;
			width: 199px;
			display: block;
			box-shadow: 0px 0px 25px #888888;
			left: -55%;
		}

		.download_shablon:hover {
			background-color: #ffffff;
			color: #4997ab;
			box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .2);
		}

		.ui-dialog-titlebar-close {
			display: none;
		}

		.imagespan {
			padding: 0px 0px 0px 10px;
		}

		.comunicationTr {
			border-top: 0.5px #c1cbd0 solid;
		}

		.comunication_name {
			font-family: pvn;
			font-weight: bold;
			font-size: 13px;
			color: #2681DC;
		}
	</style>
</head>

<body>
	<div id="tabs" style="width: 80.5%">
		<div class="callapp_head" style="width: calc(100% - 200px);">
			შემომავალი კომუნიკაცია
			<span class="comunication_button"><img alt="refresh" src="media/images/icons/comunication/comunication.png" height="30" width="30"></span>

			<span activ_status="0" style="float: right; cursor:pointer;"><img id="incallToggleDnd" alt="micr" src="media/images/icons/comunication/Phone_ON.png" height="40" width="40" value="0"></span>

			<span style="float: right; cursor: pointer;"><img id="micr" alt="micr" src="media/images/icons/4.png" height="35" width="30" value="0"></span>

			<div style="margin-right:15px; height:33px" class="callapp-op-activitie-wrapper">
				<div class="callapp-op-activitie-color-holder">
					<div class="callapp-op-activitie-color"></div>
				</div>
				<div class="callapp-op-activitie-choose-wrapper">
					<div class="callapp-op-activitie-choose-receiver">აირჩიეთ აქტივობა</div>
					<ul id="callapp_op_activitie_choose" data-active="false">
						<li data-value="0" data-color="#bdc3c7">აირჩიეთ აქტივობა</li>
					</ul>
				</div>
				<div class="callapp-op-activitie-choose-togbutton">
					<button id="openActivitieChoose">
						<i class="fas fa-angle-down"></i>
					</button>
				</div>
			</div>

			<span style="margin-right:20px;" class="callapp_refresh"><img alt="refresh" src="media/images/icons/refresh.png" height="25" width="28"></span>
			<hr class="callapp_head_hr">
		</div>
		<div class="callapp_tabs">

		</div>
		<div class="callapp_filter_show">
			<button id="callapp_show_filter_button" style="    background: #fff;">ფილტრი <div id="shh" style="background: url('media/images/icons/show.png'); width: 24px; height: 9px;background-position: 0 0px;margin-top: 5px;float: right;"></div></button>
			<div class="callapp_filter_body" myvar="1">
				<div style="float: left; width: 100%;">
					<table>
						<tr>
							<td style="width: 300px;">
								<span>
									<label for="start_date" style="margin-left: 110px;">-დან</label>
									<input value="" class="callapp_filter_body_span_input" type="text" id="start_date" style="width: 100px;">
								</span>
								<span>
									<label for="end_date" style="margin-left: 110px;">-მდე</label>
									<input value="" class="callapp_filter_body_span_input" type="text" id="end_date" style="width: 100px;">
								</span>
							</td>
							<td rowspan="4" style="width: 100px;">
								<table>
									<tr>
										<td>
											<span style="margin-left: 15px">
												<label for="filter_1">დამუშავებული</label>
												<div class="callapp_checkbox" style="margin-top: -16px;margin-left: -15px;">
													<input class="callapp_filter_body_span_input" id="filter_1" type="checkbox" value="1" />
													<label for="filter_1" style="background: #4CAE50;"></label>
												</div>
											</span>
										</td>
									</tr>
									<tr>
										<td>
											<span style="margin-left: 15px">
												<label for="filter_2" style="">დაუმუშავებელი</label>
												<div class="callapp_checkbox" style="margin-top: -16px;margin-left: -15px;">
													<input class="callapp_filter_body_span_input" id="filter_2" type="checkbox" value="2" />
													<label for="filter_2" style="background: #9E9E9E;"></label>
												</div>
											</span>
										</td>
									</tr>
									<tr>
										<td>
											<span style="margin-left: 15px">
												<label for="filter_3">უპასუხო</label>
												<div class="callapp_checkbox" style="margin-top: -16px;margin-left: -15px;">
													<input class="callapp_filter_body_span_input" id="filter_3" type="checkbox" value="3" />
													<label for="filter_3" style="background: #F44336;"></label>
												</div>
											</span>
										</td>
									</tr>
									<tr>
										<td>
											<span style="margin-left: 15px;width: 60px;">
												<label for="filter_4">საუბრობს</label>
												<div class="callapp_checkbox" style="margin-top: -18px;margin-left: -15px;">
													<input class="callapp_filter_body_span_input" id="filter_4" type="checkbox" value="4" />
													<label for="filter_4" style="background: #ecaf00;"></label>
												</div>
											</span>
										</td>
									</tr>
									<tr>
										<td>
											<span style="margin-left: 15px;width: 60px;">
												<label for="filter_5">რიგშია</label>
												<div class="callapp_checkbox" style="margin-top: -18px;margin-left: -15px;">
													<input class="callapp_filter_body_span_input" id="filter_5" type="checkbox" value="5" />
													<label for="filter_5" style="background: #ecaf00;"></label>
												</div>
											</span>
										</td>
									</tr>
								</table>
							</td>
							<td rowspan="4" style="width: 120px;">
								<table style="width: 100%;">
									<tr>
										<td>
											<span style="margin-left: 15px">
												<label style="padding: 0;" for="filter_6">Phone</label>
												<input class="callapp_filter_body_span_input" id="filter_6" type="checkbox" value="6">
											</span>
										</td>
									</tr>
									<tr>
										<td>
											<span style="margin-left: 15px">
												<label style="padding: 0;" for="filter_7">Live Chat</label>
												<input class="callapp_filter_body_span_input" id="filter_7" type="checkbox" value="7">
											</span>
										</td>
									</tr>
									<tr>
										<td>
											<span style="margin-left: 15px">
												<label style="padding: 0;" for="filter_9">Facebook</label>
												<input class="callapp_filter_body_span_input" id="filter_9" type="checkbox" value="9">
											</span>
										</td>
									</tr>
									<tr>
										<td>
											<span style="margin-left: 15px">
												<label style="padding: 0;" for="filter_8">Local Chat</label>
												<input class="callapp_filter_body_span_input" id="filter_8" type="checkbox" value="8">
											</span>
										</td>
									</tr>
									<tr>
										<td>
											<span style="margin-left: 15px">
												<label style="padding: 0;" for="filter_11">Mail</label>
												<input class="callapp_filter_body_span_input" id="filter_11" type="checkbox" value="11">
											</span>
										</td>
									</tr>
									<tr>
										<td>
											<span style="margin-left: 15px">
												<label style="padding: 0;" for="filter_10">მანუალური</label>
												<input class="callapp_filter_body_span_input" id="filter_10" type="checkbox" value="10">
											</span>
										</td>
									</tr>
								</table>
							</td>
							<td rowspan="4">
								<button id="loadtable" style="margin-top: 106px; float:right; font-size: 13px;border: 1px solid #A3D0E4;background: #E6F2F8;padding: 5px 5px 3px 5px;font-family: pvn;">ფილტრი</button>
							</td>
						</tr>
						<tr>
							<td>
								<select id="operator_id" style="width: 281px;">
									<option value="0">ყველა ოპერატორი</option>
									<?php
									$db->setQuery("SELECT users.`id`,
                                                      user_info.`name`
                                               FROM   users
                                               JOIN   user_info ON users.id = user_info.user_id
                                               WHERE `group_id` = 1 AND users.`actived` = 1");

									$res = $db->getResultArray();
									foreach ($res[result] as $req) {
										$data .= "<option value='$req[id]'>$req[name]</option>";
									}
									echo $data;
									?>
								</select>
							</td>
						</tr>
						<tr>
							<td>
								<select id="tab_id" style="width: 281px;">
									<option value="0">ყველა ზარი</option>
									<?php
									$db->setQuery("SELECT id,`name`
                                               FROM   inc_status
                                               WHERE  actived = 1");

									$res1 = $db->getResultArray();
									foreach ($res1[result] as $req1) {
										$data1 .= "<option value='$req1[id]'>$req1[name]</option>";
									}
									echo $data1;
									?>
								</select>
							</td>
						</tr>
						<tr>
							<td>
								<span>
									<select id="user_info_id" style="width: 281px;">
										<option value="4">ყველა</option>
										<option value="1">ჩემი მომართვები</option>
									</select>
								</span>
							</td>
						</tr>
					</table>
				</div>



			</div>
			<div style="width: 100%;margin-top: 22px;">

			</div>
			<div class="clear"></div>
			<div style="position:relative;">
				<button id="add_button" style="font-family: BPG arial !important;font-size: 11px;border: 1px solid #A3D0E4;background: #E6F2F8;margin-top: 2px;margin-right: 4px;margin-bottom: 10px;">დამატება</button>
				<table id="table_right_menu" style="position:absolute;right:0;">
					<tr>
						<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;background:#2681DC;" id="show_table" myvar="0"><img alt="table" src="media/images/icons/table_w.png" height="14" width="14"></td>
						<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_log" myvar="0"><img alt="log" src="media/images/icons/log.png" height="14" width="14"></td>
						<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_copy_prit_exel" myvar="0"><img alt="link" src="media/images/icons/select.png" height="14" width="14"></td>
					</tr>
				</table>
				<table class="display" id="table_index" style="width: 100%;">
					<thead style="font-size: 10px;">
						<tr id="datatable_header">
							<th>ID</th>
							<th style="width: 6%;">№</th>
							<th style="width:  13%;">თარიღი</th>
							<th style="width:  10%">სახელი/გვარი</th>
							<th style="width:  6%">User Id</th>
							<th style="width:  10%">ტელეფონი</th>
							<th style="width:  11%">ელ ფოსტა</th>
							<th style="width:  8%">საიტი</th>
							<th style="width:  11%">ოპერატორი</th>
							<th style="width:  10%">რეაგირება</th>
							<th style="width:  5%">დრო</th>
							<th style="width:  12%">ქმედება</th>
						</tr>
					</thead>
					<thead>
						<tr class="search_header">
							<th class="colum_hidden">
								<input type="text" name="search_id" value="ფილტრი" class="search_init" style="" />
							</th>
							<th>
								<input type="text" name="search_number" value="" class="search_init" style="width:100%;"></th>
							<th>
								<input type="text" name="search_date" value="ფილტრი" class="search_init" style="width:100%;" />
							</th>
							<th>
								<input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 100%;" />
							</th>
							<th>
								<input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 100%;" />
							</th>
							<th>
								<input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 100%;" />
							</th>
							<th>
								<input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 100%;" />
							</th>
							<th>
								<input type="text" name="search_category" value="ფილტრი" class="search_init" style="width:100%;" />
							</th>
							<th>
								<input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 100%;" />
							</th>
							<th>
								<input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 100%;" />
							</th>
							<th>
								<input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 100%;" />
							</th>
							<th>
								<input type="text" name="search_category" value="ფილტრი" class="search_init" style="width:100%;" />
							</th>
							<th>
								<input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 100%;" />
							</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
		<style>
			#flesh_panel_table,
			#flesh_panel_table_mini {
				box-shadow: 0px 0px 7px #888888;
			}

			#flesh_panel_table td,
			#flesh_panel_table_mini td {
				height: 25px;
				vertical-align: middle;
				text-align: left;
				padding: 0 6px;
				background: #FFF;

			}

			.tb_head td {
				border-right: 1px solid #E6E6E6;
			}

			#show_flesh_panel,
			#show_flesh_panel_right {
				float: left;
				cursor: pointer;
			}

			.td_center {
				text-align: center !important;
			}

			/* The container checkbox*/
			.container {
				display: block;
				position: relative;
				padding-left: 35px;
				margin-bottom: 12px;
				cursor: pointer;
				font-size: 22px;
				-webkit-user-select: none;
				-moz-user-select: none;
				-ms-user-select: none;
				user-select: none;
			}

			/* Hide the browser's default checkbox */
			.container input {
				position: absolute;
				opacity: 0;
				cursor: pointer;
				height: 0;
				width: 0;
			}

			/* Create a custom checkbox checkmark*/
			.checkmark {
				position: absolute;
				top: 0;
				left: 0;
				height: 25px;
				width: 25px;
				background-color: #fff;
			}

			/* On mouse-over, add a grey background color */
			.container:hover input~.checkmark {
				background-color: #fff;
			}

			/* When the checkbox is checked, add a blue background */
			.container input:checked~.checkmark {
				background-color: #fff;
			}

			/* Create the checkmark/indicator (hidden when not checked) */
			.checkmark:after {
				content: "";
				position: absolute;
				display: none;
			}

			/* Show the checkmark when checked */
			.container input:checked~.checkmark:after {
				display: block;
			}

			/* Style the checkmark/indicator */
			.container .checkmark:after {
				left: 9px;
				top: 5px;
				width: 5px;
				height: 10px;
				border: solid #f80aea;
				border-width: 0 3px 3px 0;
				-webkit-transform: rotate(45deg);
				-ms-transform: rotate(45deg);
				transform: rotate(45deg);
			}

			/*------------------------------------------*/
			/* Create a custom checkbox checkmark1*/

			.checkmark1 {
				position: absolute;
				top: 0;
				left: 0;
				height: 25px;
				width: 25px;
				background-color: #fff;
			}

			/* On mouse-over, add a grey background color */
			.container:hover input~.checkmark1 {
				background-color: #fff;
			}

			/* When the checkbox is checked, add a blue background */
			.container input:checked~.checkmark1 {
				background-color: #fff;
			}

			/* Create the checkmark/indicator (hidden when not checked) */
			.checkmark1:after {
				content: "";
				position: absolute;
				display: none;
			}

			/* Show the checkmark when checked */
			.container input:checked~.checkmark1:after {
				display: block;
			}

			/* Style the checkmark/indicator */
			.container .checkmark1:after {
				left: 9px;
				top: 5px;
				width: 5px;
				height: 10px;
				border: solid #f29d09;
				border-width: 0 3px 3px 0;
				-webkit-transform: rotate(45deg);
				-ms-transform: rotate(45deg);
				transform: rotate(45deg);
			}

			/*------------------------------------------*/
			/* Create a custom checkbox checkmark2*/

			.checkmark2 {
				position: absolute;
				top: 0;
				left: 0;
				height: 25px;
				width: 25px;
				background-color: #fff;
			}

			/* On mouse-over, add a grey background color */
			.container:hover input~.checkmark2 {
				background-color: #fff;
			}

			/* When the checkbox is checked, add a blue background */
			.container input:checked~.checkmark2 {
				background-color: #fff;
			}

			/* Create the checkmark/indicator (hidden when not checked) */
			.checkmark2:after {
				content: "";
				position: absolute;
				display: none;
			}

			/* Show the checkmark when checked */
			.container input:checked~.checkmark2:after {
				display: block;
			}

			/* Style the checkmark/indicator */
			.container .checkmark2:after {
				left: 9px;
				top: 5px;
				width: 5px;
				height: 10px;
				border: solid #2196f3;
				border-width: 0 3px 3px 0;
				-webkit-transform: rotate(45deg);
				-ms-transform: rotate(45deg);
				transform: rotate(45deg);
			}

			/*------------------------------------------*/

			/* Create a custom checkbox checkmark3*/

			.checkmark3 {
				position: absolute;
				top: 0;
				left: 0;
				height: 25px;
				width: 25px;
				background-color: #fff;
			}

			/* On mouse-over, add a grey background color */
			.container:hover input~.checkmark3 {
				background-color: #fff;
			}

			/* When the checkbox is checked, add a blue background */
			.container input:checked~.checkmark3 {
				background-color: #fff;
			}

			/* Create the checkmark/indicator (hidden when not checked) */
			.checkmark3:after {
				content: "";
				position: absolute;
				display: none;
			}

			/* Show the checkmark when checked */
			.container input:checked~.checkmark3:after {
				display: block;
			}

			/* Style the checkmark/indicator */
			.container .checkmark3:after {
				left: 9px;
				top: 5px;
				width: 5px;
				height: 10px;
				border: solid #e81d42;
				border-width: 0 3px 3px 0;
				-webkit-transform: rotate(45deg);
				-ms-transform: rotate(45deg);
				transform: rotate(45deg);
			}

			/*------------------------------------------*/

			/* Create a custom checkbox checkmark3*/

			.checkmark4 {
				position: absolute;
				top: 0;
				left: 0;
				height: 25px;
				width: 25px;
				background-color: #fff;
			}

			/* On mouse-over, add a grey background color */
			.container:hover input~.checkmark4 {
				background-color: #fff;
			}

			/* When the checkbox is checked, add a blue background */
			.container input:checked~.checkmark4 {
				background-color: #fff;
			}

			/* Create the checkmark/indicator (hidden when not checked) */
			.checkmark4:after {
				content: "";
				position: absolute;
				display: none;
			}

			/* Show the checkmark when checked */
			.container input:checked~.checkmark4:after {
				display: block;
			}

			/* Style the checkmark/indicator */
			.container .checkmark4:after {
				left: 9px;
				top: 5px;
				width: 5px;
				height: 10px;
				border: solid #030202;
				border-width: 0 3px 3px 0;
				-webkit-transform: rotate(45deg);
				-ms-transform: rotate(45deg);
				transform: rotate(45deg);
			}

			/*------------------------------------------*/

			/* Create a custom checkbox checkmark3*/

			.checkmark5 {
				position: absolute;
				top: 0;
				left: 0;
				height: 25px;
				width: 25px;
				background-color: #fff;
			}

			/* On mouse-over, add a grey background color */
			.container:hover input~.checkmark5 {
				background-color: #fff;
			}

			/* When the checkbox is checked, add a blue background */
			.container input:checked~.checkmark5 {
				background-color: #fff;
			}

			/* Create the checkmark/indicator (hidden when not checked) */
			.checkmark5:after {
				content: "";
				position: absolute;
				display: none;
			}

			/* Show the checkmark when checked */
			.container input:checked~.checkmark5:after {
				display: block;
			}

			/* Style the checkmark/indicator */
			.container .checkmark5:after {
				left: 9px;
				top: 5px;
				width: 5px;
				height: 10px;
				border: solid #009895;
				border-width: 0 3px 3px 0;
				-webkit-transform: rotate(45deg);
				-ms-transform: rotate(45deg);
				transform: rotate(45deg);
			}

			#incoming_chat_tabs .ui-dialog .ui-widget-header {
				border: none;
				background: #fff;
			}

			#incoming_chat_tabs .ui-tabs .ui-tabs-nav {
				margin: 0;
				padding: .0em .0em 0;
			}

			/*------------------------------------------*/
		</style>
		<div id="flesh_panel">
			<div class="callapp_head" style="text-align: right;"><img id="show_flesh_panel" title="პანელის გადიდება" alt="arrow" src="media/images/icons/arrow_left.png" height="18" width="18">ქოლ-ცენტრი
				<hr class="callapp_head_hr">
			</div>
			<table style="display:none;" id="comucation_status">
				<tr>
					<td colspan="3">
						<div style="padding: 12px 0px 0px 10px; font-size:16px" class="callapp_head" style="text-align: left;">კომუნიკაციის არხები</div>
					</td>
				</tr>
				<tr style="height: 6px"></tr>
				<tr class="comunicationTr">
					<td style="width: 60px;"><span class="imagespan"><img id="VebChatImg" src="media/images/icons/comunication/Chat_OFF.png" height="35" width="35"></span></td>
					<td style="padding: 10px 0px 0px 0px; width: 100px;"><span style="color: #f80aea;" class="comunication_name">ვებ-ჩათი</span></td>
					<td style="padding: 5px 0 0 0px;"><label class="container">
							<input class="comunication_checkbox" type="checkbox" id="web_chat_checkbox" value="1">
							<span style="border: 1px #f80aea solid;" class="checkmark"></span>
						</label></td>
				</tr>
				<tr class="comunicationTr">
					<td><span class="imagespan"><img id="siteImg" src="media/images/icons/comunication/OFF-my.png" height="35" width="35"></span></td>
					<td style="padding: 10px 0px 0px 0px;"><span style="color:#f29d09;" class="comunication_name">my მესენენჯერი</span></td>
					<td style="padding: 5px 0 0 0px;"><label class="container">
							<input class="comunication_checkbox" type="checkbox" id="site_chat_checkbox" value="1">
							<span style="border: 1px #f29d09 solid;" class="checkmark1"></span>
						</label></td>
				</tr>
				<tr class="comunicationTr">
					<td><span class="imagespan"><img id="MessengerImg" src="media/images/icons/comunication/Messenger_OFF.png" height="35" width="35"></span></td>
					<td style="padding: 10px 0px 0px 0px;"><span style="color: #2196f3;" class="comunication_name">მესენჯერი</span></td>
					<td style="padding: 5px 0 0 0px;"><label class="container">
							<input class="comunication_checkbox" type="checkbox" id="messanger_checkbox" value="1">
							<span style="border: 1px #2196f3 solid;" class="checkmark2"></span>
						</label></td>
				</tr>
				<tr class="comunicationTr">
					<td><span class="imagespan"><img id="MailImg" src="media/images/icons/comunication/E-MAIL_OFF.png" height="35" width="35"></span></td>
					<td style="padding: 10px 0px 0px 0px;"><span style="color: #e81d42;" class="comunication_name">ელ-ფოსტა</span></td>
					<td style="padding: 5px 0 0 0px;"><label class="container">
							<input class="comunication_checkbox" type="checkbox" id="mail_chat_checkbox" value="1">
							<span style="border: 1px #e81d42 solid;" class="checkmark3"></span>
						</label></td>
				</tr>

				<!-- <tr class="comunicationTr" style="height: 40px; display:none;">
    	    <td><span class="imagespan"><img id="VideoImg" src="media/images/icons/comunication/Video_OFF.png" height="35" width="35"></span></td>
    		<td style="padding: 10px 0px 0px 0px;"><span style="color: #009895;" class="comunication_name">ვიდეო ზარი</span></td>
    		<td style="padding: 5px 0 0 0px;"><label class="container">
              <input class="comunication_checkbox" type="checkbox" id="video_call_checkbox" value="1">
              <span style="border: 1px #009895 solid;" class="checkmark5"></span>
            </label></td>
    	</tr> -->
			</table>
			<table id="flesh_panel_table" style="margin-bottom: 0px; width: 100%;">

				<thead>
					<tr>
						<td colspan="4">
							<table>
								<tr>
									<td id="show_station" class="flash_menu_link" style="cursor:pointer;font-size:12px;font-weight:bold;font-family: pvn !important;color:#2681DC;text-decoration: underline;"><img src="media/images/icons/comunication/phone.png" height="27" width="27"></td>
									<td id="show_chat" class="flash_menu_link" style="cursor:pointer;font-size:12px;font-weight:bold;font-family: pvn !important;"><img src="media/images/icons/comunication/Chat.png" height="30" width="30"></td>
									<td id="show_fbm" class="flash_menu_link" style="cursor:pointer;font-size:12px;font-weight:bold;font-family: pvn !important;"><img src="media/images/icons/comunication/Messenger.png" height="30" width="30"></td>
									<td id="show_site" class="flash_menu_link" style="cursor:pointer;font-size:12px;font-weight:bold;font-family: pvn !important;"><img src="media/images/icons/comunication/my_site.ico" height="30" width="30"></td>
									<td id="show_mail" class="flash_menu_link" style="cursor:pointer;font-size:12px;font-weight:bold;font-family: pvn !important; "><img src="media/images/icons/comunication/E-MAIL.png" height="30" width="30"></td>
									<!-- <td id="show_video"    class="flash_menu_link" style="display:none; cursor:pointer;font-size:12px;font-weight:bold;font-family: pvn !important;"><img src="media/images/icons/comunication/Video.png" height="30" width="30"></td> -->
								</tr>
							</table>
						</td>


					</tr>
					<tr class="tb_head" style="border: 1px solid #E6E6E6;">
						<td style="width:85px" id="mini_pirveli">შიდა ნომერი</td>
						<td colspan="2" style="width: 120px;" id="mini_meore">მომ. ავტორი</td>
						<td style="width: 90px;" id="mini_mesame">სტატუსი</td>
					</tr>
				</thead>
				<tbody id="mini_call_ext">
				</tbody>
				<thead>
					<tr>
						<td colspan="4" style="border-left: 1px solid #E6E6E6;border-right: 1px solid #E6E6E6;"></td>
					</tr>
					<tr>
						<td colspan="4" style="border-left: 1px solid #E6E6E6;border-right: 1px solid #E6E6E6;">რიგი</td>
					</tr>
					<tr class="tb_head" style="border: 1px solid #E6E6E6;">
						<td id="m_q_pirveli">პოზიცია</td>
						<td colspan="3" id="m_q_mesame">ნომერი</td>
					</tr>
				</thead>
				<tbody id="mini_call_queue">
				</tbody>

			</table>



		</div>

		<div id="loading" style="z-index: 999999;padding: 160px 0;height: 100%;width: 100%;position: fixed;background: #f9f9f9;top: 0;">
			<div class="loading-circle-1">
			</div>
			<div class="loading-circle-2">
			</div>
		</div>
		<input id="check_state" type="hidden" value="1" />
		<input id="whoami" type="hidden" value="1" />
		<!-- jQuery Dialog -->
		<div id="loading_search_my_client_history" style="display:none; z-index: 999999;padding: 155px 0;height: 100%;width: 75%;position: fixed;top: 0;">
			<div class="loading-circle-1">
			</div>
			<div class="loading-circle-2">
			</div>
		</div>
		<div id="add-edit-form" class="form-dialog" title="შემომავალი ზარი">

		</div>
		<div id="crm_dialog" class="form-dialog" title="ატვირთული ბაზები"></div>
		<div id="crm_file_dialog" class="form-dialog" title="ფაილი"></div>
		<!-- jQuery Dialog -->
		<div id="add-edit-form-sms" class="form-dialog" title="ახალი ესემესი">
		</div>
		<!-- jQuery Dialog -->
		<div id="add-edit-form-task" class="form-dialog" title="დავალება">
		</div>
		<!-- jQuery Dialog -->
		<div id="sms-templates" class="form-dialog" title="sms შაბლონები">
		</div>

		<!-- jQuery Dialog -->
		<div id="phone-directory-list" class="form-dialog" title="სატელეფონო ცნობარი">
		</div>

		<!-- jQuery Dialog -->
		<div id="last_calls" title="ბოლო ზარები">
		</div>
		<!-- jQuery Dialog -->
		<div id="audio_dialog" title="ჩანაწერი">
		</div>

		<!-- jQuery Dialog -->
		<div id="numlock_dialog" title="დაბლოკვა">
		</div>
		<!-- jQuery Dialog -->
		<div id="chat_close_dialog" class="form-dialog" title="დახურვა">
			<div id="dialog-form">
				<fieldset>
					გსურთ ჩატის დახურვა?
				</fieldset>
			</div>
		</div>

		<!-- jQuery Dialog -->
		<div id="show_hint" class="form-dialog" title="შეტყობინება">
			<div style="padding-top: 15px; text-align: center;" id="dialog-form">
				მონაცემები შენახულია!
			</div>
		</div>

		<!-- jQuery Dialog -->
		<div id="hint_close" class="form-dialog" title="შეტყობინება">
			<div style="padding-top: 15px; text-align: center;" id="dialog-form">
				გსურთ დიალოგის დახურვა ?
			</div>
		</div>

		<!-- jQuery Dialog -->
		<div id="hint_chat_close" class="form-dialog" title="შეტყობინება">
			<div style="padding-top: 15px; text-align: center;" id="dialog-form">
				ნამდვილად გსურთ ჩატის დახურვა დამუშავების გარეშე?
			</div>
		</div>


		<!-- jQuery Dialog -->
		<div id="add_blocked_ip" class="form-dialog" title="შავ სიაში დამატება">
			<div id="dialog-form">
				<fieldset>
					<table style="width:100%;margin-bottom:5px;" id="blocklogo">
						<tr>
							<td>
								<label for="">კომენტარი</label>
							</td>
						</tr>
						<tr>
							<td>
								<textarea style="width: 99%;resize: vertical;" id="block_comment" class="idle"></textarea>
							</td>
						</tr>
					</table>
				</fieldset>
			</div>
		</div>
		<!-- jQuery Dialog -->
		<div id="ipaddr_dialog" class="form-dialog" title="შავ სიაში დამატება">
			<div id="dialog-form">
				<fieldset>
					<table style="width:100%;margin-bottom:5px;">
						<tr>
							<td>
								<label for="">კომენტარი</label>
							</td>
						</tr>
						<tr>
							<td>
								<textarea style="width: 99%;resize: vertical;" id="ipaddr_block_comment" class="idle"></textarea>
							</td>
						</tr>
					</table>
				</fieldset>
			</div>
		</div>
		<div id="add-edit-form-answer" class="form-dialog" title="პასუხი">
		</div>
		<!-- jQuery Dialog -->
		<div id="add-edit-form-mail" class="form-dialog" title="ახალი E-mail">
		</div>
		<!-- jQuery Dialog -->
		<div id="add-edit-form-mail-shablon" class="form-dialog" title="E-mail შაბლონი">
		</div>
		<div id="phone-directory-list" class="form-dialog" title="სატელეფონო ცნობარი">
		</div>
		<div style='position:fixed;bottom: 5px;right: 5px;width: 40px;display: block;'>
			<input id="imoriginalchat" type="hidden" value="0">
			<input id="comunication_table_check" type="hidden" value="0">
			<input id="dnd_on_local_status" type="hidden" value="0">

			<span data-source='chat' id="chat_count_open" chat_id="0" style="position:sticky; z-index: 100;cursor: pointer; display: block; padding: 7px; margin-top: 10px; background: none;">
				<img src="media/images/icons/comunication/Chat.png" height="35" width="35">
				<span class='fabtext' style='width: 8px;  padding-right:8px; z-index:1;  padding: 3px;    text-align: center;    border: solid 1px #ccc;    font-size: 8px;   border-radius: 50%;    display: block;    position: absolute;    top: 0;    right: 0;'></span>
			</span>

			<span data-source='fbm' id="fb_chat_count_open" chat_id="0" style="position:sticky;z-index: 100;cursor: pointer; display: block; padding: 7px; margin-top: 10px; background: none;">
				<img src="media/images/icons/comunication/Messenger.png" height="35" width="35">
				<span class='fabtext' style='width: 8px;  padding-right:8px; z-index:1;  padding: 3px;    text-align: center;    border: solid 1px #ccc;    font-size: 8px;   border-radius: 50%;    display: block;    position: absolute;    top: 0;    right: 0;'></span>
			</span>
			<span data-source='site' id="site_chat_count_open" chat_id="0" style="position sticky; z-index: 100;;cursor: pointer; padding: 7px; margin-top: 10px; background: none;">
				<img src="media/images/icons/comunication/my_site.ico" height="35" width="35">
				<span class='fabtext' style='display:none; width: 8px;  padding-right:8px; z-index:1;  padding: 3px;    text-align: center;    border: solid 1px #ccc;    font-size: 8px;   border-radius: 50%;    display: block;    position: absolute;    top: 112px;    right: 0;'></span>
			</span>
			<span data-source='mail' id="mail_chat_count_open" chat_id="0" style="position:sticky;z-index: 100;cursor: pointer; display: block; padding: 7px; margin-top: 10px; background: none;">
				<img src="media/images/icons/comunication/E-MAIL.png" height="35" width="35">
				<span class='fabtext' style='width: 8px;  padding-right:8px; z-index:1;  padding: 3px;    text-align: center;    border: solid 1px #ccc;    font-size: 8px;   border-radius: 50%;    display: block;    position: absolute;    top: 0;    right: 0;'></span>
			</span>
			<!-- <span data-source='video' id="video_chat_count_open" chat_id="0" style="position:sticky;z-index: 100;cursor: pointer; display:none; padding: 7px; margin-top: 10px; background: none;">
		<img src="media/images/icons/comunication/Video.png" height="35" width="35">
		<span class='fabtext' style='width: 8px;  padding-right:8px; z-index:1;   padding: 3px;    text-align: center;    border: solid 1px #ccc;    font-size: 8px;   border-radius: 50%;    display: block;    position: absolute;    top: 0;    right: 0;'></span>
	</span>-->
		</div>
		<a id="downloader" href="" style="display:none" target="_blank"></a>
		<div id="add_sport_dialog" class="form-dialog" title="სპორტი"></div>

</body>