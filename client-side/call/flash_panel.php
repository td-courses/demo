
<head>
	<script type="text/javascript">					
		      
	$(document).ready(function () {
		$.ajax({
        	dataType: "JSON",
	        url: 'server-side/call/flash_panel.action.php',
		    data: 'act=1',
	        success: function(data) {
				$("#queue").html(data.queue);
				$("#branch").html(data.branch);
				$("#ext").html(data.ext);
				$("#user").html(data.user);
				$("#state").html(data.state);
		    }
        }).done(function(data) {
       runAjax();
       $('#queue, #branch, #ext, #user, #state').chosen({ search_contains: true });
        });
   });

	function runAjax() {
        $.ajax({
        	async: false,
        	dataType: "html",
	        url: 'AsteriskManager/liveState_FP.php',
		    data: 'sesvar=hideloggedoff&value=true',
		    beforeSend: false,
            complete: false,
	        success: function(data) {

				// insert data in reseiver div
				$("#jq").html(data);

				// FILTERS
				var filterCollector = "";
				var filters = {
					queue: $("#queue").val(),
					// branch: $("#branch").val(),
					ext: $("#ext").val(),
					user: $("#user").val(),
					state: $("#state").val(),

				}
				

				// queue
				if(filters.queue != 0){
					filterCollector += `[queue="${filters.queue}"]`;
				}

				// branch
				// if(filters.branch != 0){
				// 	filterCollector += `[dep="${filters.branch}"]`;
				// }

				// extension
				if(filters.ext != 0){
					filterCollector += `[ext="${filters.ext}"]`;
				}

				// user
				if(filters.user != 0){
					filterCollector += `[user="${filters.user}"]`;
				}

				// satet
				if(filters.state != 0){
					filterCollector += `[state="${filters.state}"]`;
				}

				// check the filter
				if(filters.queue == 0 && filters.branch == 0 && filters.ext == 0 && filters.user == 0 && filters.state == 0){
					$("#flesh_table tbody tr").css('display','');
				} else {
					$("#flesh_table tbody tr").css('display','none');
					$(`#flesh_table tbody tr${filterCollector}`).css('display','');
					console.log(`${filterCollector}`);
				}

				// check filtered table tr length
				if(!$("#flesh_table tbody tr:visible").length) {
					$("#flesh_table").css('display','none');
					$("#empty-data").css('display','block');
				}

		    }

        }).done(function(data) {
            setTimeout(runAjax, 1000);
        });
	}
	</script>
</head>

<style type='text/css'>
#my_div{
    margin-top: 30px;
}
#my_selector{
	width: 97%;
    margin: auto;
	background: #fff;
	border-radius: 5px;
    border: 1px solid #BABDBF;
}
#flesh_table,#tb{
    width: 99%;
    margin: auto;
}
#flesh_table thead tr th,#tb thead tr th{
	text-align: left;
	padding: 10px;
}
#flesh_table tbody tr td,#tb tbody tr td{
	height: 30px;
	padding-left: 10px;
	text-align: left;
	vertical-align: middle;	
}
#flesh_table tbody tr,#tb tbody tr{
	border-top: 1px solid #E5E5E5;
}
#flesh_table tbody tr:last-child,#tb tbody tr:last-child{
	border-bottom: 1px solid #E5E5E5;
}
#filter{
	padding: 10px 10px 20px 10px;
	height: 43px;
}
#filter span {
	float: left;
	margin-right: 38px;
}
.chosen-container {
	margin-top: 6px;
}
#jq{
	margin-top: 20px;
}
td{
	 word-break: break-word;
}
.high {
    height: 0px;
}

#flesh_table1,#tr{
    width: 10%;
    margin: auto;
}
#flesh_table1 thead tr th,#tb thead tr th{
	text-align: left;
	padding: 10px;
}
#flesh_table1 tbody tr td,#tb tbody tr td{
	height: 30px;
	padding-left: 10px;
	text-align: left;
	vertical-align: middle;
	font-size:15px;	
}
#flesh_table1 tbody tr,#tb tbody tr{
	border-top: 1px solid #E5E5E5;
	border-left: 1px solid #E5E5E5;
    border-right: 1px solid #E5E5E5;
}
#flesh_table1 tbody tr:last-child,#tb tbody tr:last-child{
	border-bottom: 1px solid #E5E5E5;
}

</style>

<body>	
<div id="tabs" style="height: 950px;    margin-bottom: 150px;">
<div class="callapp_head">Flesh Panel<hr class="callapp_head_hr"></div>	
    <div id="my_div">
        <div id="my_selector">
            <div id="filter">
                <span>
                <label>რიგი</label>
                <select id="queue" style="width: 165px"></select>
                </span>
                
                <span>
                <label>შიდა ნომერი</label>
                <select id="ext" style="width: 165px"></select>
                </span>
                
                <span>
                <label>თანამშრომელი</label>
                <select id="user" style="width: 225px"></select>
                </span>
                
                <span style="  margin-right: 0px;">
                <label>მდგომარეობა</label>
                <select id="state" style="width: 165px"></select>
                </span>
            </div>
        </div>
    </div>
    <div id="my_div"> 
        <div id="my_selector">
            <div id="jq">
            </div>
        </div> 
    </div>
</div>
</body>