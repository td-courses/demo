<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">-->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>-->
<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>-->

<style>
    .btn {
        width: 120px;
        margin-right: 1%;
        height: 23px;
        border-width: 1px;
        cursor: pointer;
        margin: 0px;
        font-family: pvn;
        display: inline-block;
        line-height: 1.4;
        position: relative;
    }

    i {
        margin-left: 0px;
        text-align: right;
    }

    .row {
        display: flex;

    }



    * {
        box-sizing: border-box
    }

    .container {
        width: 100%;
        background-color: #ddd;
    }

    .skills {
        text-align: right;
        padding: 1px;
        color: white;
    }

</style>
<script type="text/javascript">
    var aJaxURL = "server-side/call/outgoing_new_action.php";
    var tName = "example";
    var fName = "add-edit-form";
    var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";

    $(document).ready(function () {

        LoadTable();
        GetButtons("add_button");
        SetEvents("add_button", "", "check-all", tName, fName, aJaxURL);

    });

    function LoadTable() {

        /* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
        GetDataTable(tName, aJaxURL, "get_list", 9, "", 0, "", 0, "desc", "", change_colum_main);

        setTimeout(function () {
            $('.ColVis, .dataTable_buttons').css('display', 'none');
        }, 90);
    }


</script>
<body>
<div class="callapp_head" style="margin-left: 0.5%; margin-top: 10px">გამავალი ზარები
    <hr class="callapp_head_hr">
</div>
<div style=" margin-top: 10px; margin-left: 0% ">

    <div id="button_area" class="column" style=" margin-left: 1.2%; ">
        <table>
            <tbody>
            <tr>
                <td><button id="add_button">დამატება</button> </td>
                <td><button id="stop_btn1" class="btn btn-sm"
                            style="border-color: orange; color: orange; background-color: white; font-weight: 400; margin-bottom:30px ">
                        <img
                                src="media\images\paus_outgoing.png"
                                height="15"
                                width="15"> დაპაუზება
                    </button></td>
                <td>
                    <button class="btn btn-sm" style="border-color: red; color: red; background-color: white; font-weight: 400">
                        <i><img
                                    src="media\images\stop_outgoing.png"
                            " height="15" width="15"></i>
                        შეწყვეტა
                    </button>
                </td>
                <td>
                    <button class="btn btn-sm" style="border-color: black; background-color: white; font-weight: 400"><i><img
                                    src="media\images\trash_outgoing.png"

                                    height="15" width="15"></i> წაშლა
                    </button>
                </td>
            </tr>
            </tbody>
        </table>




    </div>


    <div id="tab" style="margin-left: 1.2%">
        <table class="display" id="example">
            <thead>
            <tr id="datatable_header">

                <th>შექმნის თარიღი</th>
                <th>დასრულების თარიღი</th>
                <th>კამპანიის დასახელება</th>
                <th>წყარო</th>
                <th>კონტაქტების რ-ბა</th>
                <th>კომპანიის ტიპი</th>
                <th>პროგრესი</th>
                <th style="width: 15%">ოპერატორის/არხის რ-ბა</th>
                <th>სტატუსი</th>
                <th class="check">#</th>
            </tr>
            </thead>
            <thead>
            <tr class="search_header">
                <th class="colum_hidden">
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>

                <th>
                    <div class="callapp_checkbox">
                        <input type="checkbox" id="check-all" name="check-all"/>
                        <label for="check-all"></label>
                    </div>
                </th>
            </tr>
            </thead>
        </table>

    </div>


</div>
<div id="add-edit-form" class="form-dialog" title="პროდუქტი">

</div>
</body>