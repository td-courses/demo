<html>
<head>
    <script type="text/javascript">
        var aJaxURL = "server-side/call/site_comunication.action.php";
        var aJaxURL1 = "server-side/call/site_comunication1.action.php";
        var aJaxURL2 = "server-side/call/hard_site_comunication.action.php";
        var aJaxURL_send_event  = "includes/send_message_site.php";
        var aJaxURL_send_event1 = "includes/hard_sendsmsevent.php";	
        var wsdl_url         = "includes/search_client_service.php";
        var tName = "main_table";
        var fName = "add-edit-form";
        var fName1 = "add-edit-form1";
        var tbName = "tabs_tabs";

        // main constructor function
        const Constructor = function () {

            this.ajaxUrl = "server-side/call/site_comunication.action.php";
            this.smsUrl = "server-side/call/hard_site_comunication.action.php";
            this.dialogIsLoaded = "";
            this.allowOpenDialog = false;
            this.dialog = {
                easyMailingDialog: {
                    id: "add-edit-form-sms",
                    url: this.ajaxUrl,
                    data: {
                        act: "send_sms_easy"
                    },
                    width: 561,
                    config: "auto",
                    positon: "center top",
                    buttons: {
                        "cancel": {
                            text: "დახურვა",
                            id: "cancel-dialog",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    }
                },
                complicateMailingDialog: {
                    id: "add-edit-form-sms",
                     url: this.ajaxUrl,
                    data: {
                        act: "send_sms_complicated"
                    },
                    width: 561,
                    config: "auto",
                    positon: "center top",
                    buttons: {
                        "cancel": {
                            text: "დახურვა",
                            id: "cancel-dialog",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    }
                }
            };
        };

        // main options ($M as main)
        const $M = new Constructor();

        /*========================================================= FUNCTIONS ====================================================*/

        // load dialog
        function loadDialog_event(dialogName) {
            // define dialog
            let {dialog} = $M;
            dialog = dialog[dialogName];

            // check if dialog exists
            if (dialog && (dialog !== "" && dialog !== undefined)) {

                // destruct table data and load table
                let {id, url, data, width, config, positon, buttons} = dialog;

                // ajax request
                post(url, data, data => {

                    // load dialog
                    $(`#${id}`).html(data);
                    GetDialog(id, width, config, buttons, positon);

                    // get complicated sms list andstylize it
                    GetDataTable("example_2", aJaxURL, "get_complicated_sms_list", 6, "sms_send_event_id="+$("#hidde_message_event_id").val(), 0, "", 1, "desc", '', "<'F'lip>");
                    $("#my_site").chosen({ search_contains: true });
                    $("#example_2_length").css({top: "3px"});


                });
            }

        }

        // load dialog

        // post request function
        function post(url, data, callback) {

            $.ajax({
                type: "POST",
                url,
                data,
                cache: false,
                success(result) {

                    // define received data
                    let {error, data} = result;

                    if (error !== '') {
                        alert(error);
                    } else {
                        callback(data);
                    }

                }
            });

        }

        // post request function


        //save new phone in directory
        function saveNewPhoneInDir() {

            //collect data from inputs
            let data = {
                act: "save_new_phone_in_directory",
                name: $("#name").val(),
                position: $("#position").val(),
                number: $("#telNumber").val()
            };

            //send save request
            $.post($M.ajaxUrl, data, result => {

                /*result data will return two different
                * values: saved and exists or third: error*/
                /*Here we are checking which data was received
                * from ajax request and executing relevant action*/
                if (result.data === "exists") {

                    alert("მსგავსი ტელეფონის ნომერი უკვე არის დაფიქსირებული ბაზაში");

                } else if (result.data === "saved") {

                    alert("მონაცემები წარმატებით დაემატა ბაზაში");
                    CloseDialog("new-number-in-directory");

                } else {

                    alert("მოქმედების განხორციელებისას დაფიქსირედა შეცდომა");

                }

            });

        }

        //save new phone in directory


        //check checkboxes that are choosen in addressee list
        function checkAddresseeExist() {

            //define variables
            let inputValues = $("#smsAddressee").val().split(",");

            //loop throw checkboxes
            $(".pdl-check").each(function () {

                //loop throw existed data array
                for (let item of inputValues) {

                    let checkboxVal = $(this).val();

                    if (checkboxVal === item.trim()) {
                        $(this).prop("checked", true);
                    }

                }

            });

        }

        //check checkboxes that are choosen in addressee list


        //function which chooses numbers which was selected in phone directory
        function choosePhoneFromDir(dialogobj) {

            //define variables
            let checkBoxCollector = [];
            let uncheckedCollector = [];
            let receiver = $("#smsAddressee");
            let receiverData = receiver.val();
            let returnData;

            //collect checkbox checked values
            $(".pdl-check").each(function (i) {

                //check if checkbox is checked
                if ($(this).is(":checked")) {

                    //get value of checked inut and reach to data collector
                    checkBoxCollector.push($(this).val().trim());
                }

            });

            //collect checkbox unchecked values
            $(".pdl-check").each(function (i) {

                //check if checkbox is checked
                if (!$(this).is(":checked")) {

                    //get value of checked inut and reach to data collector
                    uncheckedCollector.push($(this).val().trim());
                }

            });

            //check if there is already data in receiver input
            if (receiverData !== "") {

                checkBoxCollector = checkBoxCollector.concat(receiverData.split(",").filter(function (item) {
                    return checkBoxCollector.indexOf(item.trim()) < 0;
                }));

            }

            //filter data by unchecked box values
            checkBoxCollector = checkBoxCollector.filter(function (item) {
                return uncheckedCollector.indexOf(item) < 0;
            });

            //join array with comma
            returnData = checkBoxCollector.join();

            //send collected data to receiver
            receiver.val(returnData);

            //close dialog of phone directory list
            $(dialogobj).dialog("close");

        }

        //function which chooses numbers which was selected in phone directory

        /*========================================================== ACTIONS ========================================================*/

        // click on dialog loader buttons
        $(document).on("click", ".dialog-loader:not(.btn-active)", function () {

            // reacivate control buttons
            let self = $(this);

            // get requested dialog name
            let dialog = self.data("dialog");

            // load dialog
            //loadDialog(dialog);

        });
        // click on dialog loader buttons


        $(document).on("keyup  paste", "#newSmsText", function () {

            var sms_text = $(this).val();
            isValid(sms_text);
            $('#smsCharCounter').val(($(this).val().length) + '/150');
        });

        function isValid(str) {
            var check = false;
            for (var i = 0; i < str.length; i++) {
                if (str.charCodeAt(i) > 127) {
                    check = true;
                }
            }
            if (check) {
                var string = $('#newSmsText').val();
                var replaced = string.replace(/[^\x00-\x7F]/g, "");
                $('#newSmsText').val(replaced);
                alert('არასწორი სიმბოლო');
            }
        }


        //open new sms send phone directory template dialog
        //open template dialog
        $(document).on("click", "#smsTemplate", function() {

       	//request data
       	let data = {
       		act: "get_sms_template_dialog"
       	};

       	//buttons
       	let buttons = {
       		"cancel": {
       			text: "დახურვა",
       			id: "cancel-dialog",
       			click: function () {
       				$(this).dialog("close");
       				$(this).html("");
       			}
       		}
       	};

       	//ajax request
       	$.post("server-side/call/incomming.action.php", data, structure => {

       		//open dialog malibu
       		$("#sms-templates").html(structure.html);
       		GetDialog("sms-templates","530","auto", buttons, "right+100 top");

       		GetDataTable("sms_template_table", "server-side/call/incomming.action.php", "phone_directory", 4, "", 0, "", 2, "desc", '', "<'F'lip>");
       		$("#sms_template_table_length").css('top','0px');
       	});

       });
        //open new sms send phone directory template dialog

        //open phone directory dialog
//         $(document).on("click", "#smsPhoneDir", () => {

//             //define data
//             let data = {
//                 act: "phone_dir_list"
//             };

//             //buttons
//             let buttons = {
//                 "choose": {
//                     text: "არჩევა",
//                     id: "choose-abonents",
//                     click: function () {

//                         //execute function whitch collects checked items value
//                         choosePhoneFromDir(this);

//                     }
//                 },
//                 "cancel": {
//                     text: "დახურვა",
//                     id: "cancel-dialog",
//                     click: function () {
//                         $(this).dialog("close");
//                     }
//                 }
//             };

//             //send dialog opening request to server side
//             $.post($M.ajaxUrl, data, result => {

//                 //insert received html structure in relevant dialog
//                 $("#phone-directory-list").html(result.html);

//                 //open relevant dialog
//                 GetDialog("phone-directory-list", 960, "auto", buttons, 'center top');
//                 GetDataTable("phoneDirectoryList", $M.ajaxUrl, "get_phone_dir_list", 5, "", 0, "", 2, "desc", '', "");

//                 //check boxex relevan to input values
//                 setTimeout(function () {
//                     checkAddresseeExist();
//                     $(".pdl-check").trigger("change");
//                 }, 50);


//             });

//         });
        //open phone directory dialog

        // choose sms from shablone
        $(document).on("click", ".download_shablon", function () {

            //define variables
            var message = $(this).data("message");
            var sms_id = $(this).attr("sms_id");

            //check length of message text
            if (message != "" && message.length <= 150) {

                $('#newSmsText').val(message);
                $('#smsCharCounter').val((message.length) + '/150');

            } else if (message.length > 150) {

                alert("შაბლონის ტექსტი შეიცავს დასაშვებზე (150) მეტ სიმბოლოს (" + message.length + ")");

            }

            //close sms template dialog
            CloseDialog("sms-templates");
        });
        // choose sms from shablone


        //check all units in phone directory list
        $(document).on("change", "#pdl_check_all", function () {

            //toggle input checked attribute
            if ($(this).is(":checked")) {
                $(".pdl-check").prop("checked", true);
            } else {
                $(".pdl-check").prop("checked", false);
            }


        });
        //check all units in phone directory list

        $(document).ready(function () {
            LoadTable();

            /* Add Button ID, Delete Button ID */
            GetButtons("add_button", "");
            SetEvents("", "", "", tName, "add-edit-form-sms", $M.ajaxUrl);
        });

        function LoadTable() {

            /* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
            GetDataTable("main_table", $M.ajaxUrl, "get_list", 6, "", 0, "", 2, "desc", '', "<'F'lip>");

            //stylize table detiles
            $("#main_table_length").css({position: "absolute", top: "3px", left: "3px"});

        }

        function LoadTable1() {

            /* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
            GetDataTable("example_1", aJaxURL, "get_list", 6, "", 0, "", 1, "desc", "", "");

        }

        function LoadTable2() {

            /* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
            GetDataTable("example_2", $M.ajaxUrl, "get_complicated_sms_list", 6, "sms_send_event_id=" + $("#hidde_message_event_id").val(), 0, "", 1, "desc", "", "");

        }

        function LoadDialog(fName) {
            if (fName == 'add-edit-form-sms') {
                var button = {
                    "cancel": {
                        text: "დახურვა",
                        id: "cancel-dialog",
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                };
                /* Dialog Form Selector Name, Buttons Array */
                GetDialog(fName, 504, "auto", button);
                
                //$("#smsTemplate").css('display','none');
                $("#my_site").chosen();
            }
        }

        function LoadDialog_shablon() {
            var button = {
                "save": {
                    text: "განახლება",
                    id: "shablon_dialog",
                    click: function () {
                    }
                }
            };

            /* Dialog Form Selector Name, Buttons Array */
            GetDialogCalls('smsShablon', 279, "auto", button);

        }

        function isValid(str) {
            var check = false;
            for (var i = 0; i < str.length; i++) {
                if (str.charCodeAt(i) > 127) {
                    check = true;
                }
            }
            if (check) {
                var string = $('#content').val();
                var replaced = string.replace(/[^\x00-\x7F]/g, "");
                $('#content').val(replaced);
                alert('არასწორი სიმბოლო');
            }
        }

        // Add - Save
        $(document).on("click", "#save-dialog", function () {
            param = new Object();

            param.act = "save_source";
            param.id = $("#source_id").val();
            param.name = $("#name").val();
            param.content = $("#content").val();

            if (param.name == "") {
                alert("შეავსეთ ველი!");
            } else {
                $.ajax({
                    url: aJaxURL,
                    data: param,
                    success: function (data) {
                        if (typeof(data.error) != 'undefined') {
                            if (data.error != '') {
                                alert(data.error);
                            } else {
                                LoadTable();
                                CloseDialog(fName);
                            }
                        }
                    }
                });
            }
        });
        $(document).on("click", "#sms_shablon", function () {
            LoadDialog_shablon();
            $('#shablon_dialog').click();
        });
        $(".calls").button({
            icons: {
                primary: " ui-icon-contact"
            }
        });
        $(document).on("click", "#shablon_dialog", function () {
            param = new Object();
            param.act = "get_shablon";

            $.ajax({
                url: aJaxURL,
                data: param,
                success: function (data) {
                    if (typeof(data.error) != 'undefined') {
                        if (data.error != '') {
                            alert(data.error);
                        } else {
                            $("#smsShablon").html(data.shablon);
                            $(".download_shablon").button({});
                        }
                    }
                }
            });

        });

        function CloseDialog(fName) {
            $("#" + fName).dialog("close");
        }

        $(document).on("click", ".download_shablon", function () {
            var test = $(this).attr("number");
            sms_id = $(this).attr("sms_id");
            sms_name = $(this).attr("sms_name");
            param = new Object();
            param.test = $(this).attr("number");
            param.sms_id = $(this).attr("sms_id");
            param.act = 'get_smsevent';

            console.log(test);
            if (test != "") {
                $('#sms_text').val(test);
                $('#sms_name').val(sms_name);
                $('#simbol_caunt').val((test.length) + '/150');
            }


            $.ajax({
                url: aJaxURL,
                data: param,
                success: function (data) {
                    if (typeof(data.error) != 'undefined') {
                        if (data.error != '') {
                            alert(data.error);
                        } else {
                            CloseDialog("smsShablon");
                            $("#choose_file").attr("sms_event_id", data.insert);
                            $("#sms_send_event_id").attr("sms_send_event_id", data.insert);
                        }
                    }
                }
            });
        });


        $(document).on("keyup  paste", "#sms_text", function () {
            var sms_text = $('#sms_text').val();
            isValid(sms_text);
            $('#simbol_caunt').val((sms_text.length) + '/150');
        });


        function isValid(str) {
            var check = false;
            for (var i = 0; i < str.length; i++) {
                if (str.charCodeAt(i) > 127) {
                    check = true;
                }
            }
            if (check) {
                var string = $('#sms_text').val();
                var replaced = string.replace(/[^\x00-\x7F]/g, "");
                $('#sms_text').val(replaced);
                alert('არასწორი სიმბოლო');
            }
        }

        $(document).on("keyup  paste", "#content", function () {
            var sms_text = $('#content').val();
            isValid(sms_text);
            $('#simbol_caunt').val((sms_text.length) + '/150');
        });

        // send sms new code
        $(document).on("click", "#sendNewSms", function() {

            // define data
            let data = {
                smsAddressee: $("#smsAddressee").val(),
                smsText: $("#newSmsText").val(),
                name: $("#user_name_surname").val(),
                sms_event_id: $("#site_sms_id").val(),
                sms_inc_increm_id: 0,
                site:$("#my_site").chosen().val()
            };
            var link = GetAjaxData(data);
            // send ajax request
            $.post(aJaxURL_send_event, link, result => {
				// get responsed values
                let {data} = result;

                // checking the result data
                if(result.result == '1') {
                    alert("შეტყობინება წარმატებით გაიგზავნა");
                    $("#smsAddressee").val("");
                    CloseDialog("add-edit-form-sms");
                }

                $("#newSmsText").val("");
                LoadTable();
			});
		});

        $(document).on("click", "#search_user_info", function () {
	    	param 		      = new Object();
			param.act	      = 'search_user_id';
		 	param.s_u_user_id = $("#smsAddressee").val();
		 	$.ajax({
		        url: wsdl_url,
		        data: param,
			  	success: function(data) {
			  		if(data.count == 1){
			  			$("#user_name_surname").val(data.user_name+' '+data.user_surname);
    				}else if(data.count == 0){
					  	alert('მონაცემები ვერ მოიძებნა');
				  	} 
				}
		    });

	    });
	    
        function markWrongNumbers(data) {

            // convert data array to string
            let string = "";

            data.forEach(item => {

                string += string !== "" ? "," : "";
                string += item.phone;

            });

            return string;

        }

        // send sms old code
        $(document).on("click", "#send_sms", function (fName) {

            param = new Object();


            param.sms_event_id = $("#sms_send_event_id").attr("sms_send_event_id");
            param.text = $("#sms_text").val();
            param.sms_inc_increm_id = $("#sms_inc_increm_id").val();

            $.ajax({
                url: aJaxURL_send_event,
                data: param,
                success: function (data) {
                    $("#sms_text").val('');
                    alert('SMS-ები წარმატებით გაიგზავნა');
                    LoadTable();
                    CloseDialog("add-edit-form");
                }
            });
        });
        $(document).on("click", "#send_sms_hard", function (fName) {
			param = new Object();
			param.sms_inc_increm_id = $("#hidde_message_event_id").val();
			
            $.ajax({
                url: aJaxURL_send_event1,
                data: param,
                success: function (data) {
                    alert('შეტყობინებები წარმატებით გაიგზავნა');
                    $("#add-edit-form-sms").dialog('close');
                    LoadTable();
                }
            });
        });

        //--------------------------hard sms--------------------------//

        $(document).on("click", "#hard_choose_button", function () {

            $("#hard_choose_file").click();

        });

        $(document).on("change", "#hard_choose_file", function () {

            var file = $(this).val();

            var sms_id = $(this).attr("sms_event_id");
            if (sms_id > 0) {
                sms_event_id = sms_id;
            } else {
                sms_event_id = $("#hidde_message_event_id").val();
            }

            var name = uniqid();
            var path = "../../media/uploads/images/client/";

            var ext = file.split('.').pop().toLowerCase();
            if ($.inArray(ext, ['xls']) == -1) { //echeck file type
                alert('This is not an allowed file type.');
                this.value = '';
            } else {
                img_name = name + "." + ext;


                $.ajaxFileUpload({
                    url: 'includes/hard_import.php',
                    secureuri: false,
                    fileElementId: "hard_choose_file",
                    dataType: 'json',
                    data: {

                        act: "upload_file",
                        path: path,
                        file_name: name,
                        sms_event_id: sms_event_id,
                        type: ext
                    },

                    success: function (data, status){ 
                        if (data.status == 1) {

                            alert('ფაილი წარმატებით აიტვირთა');
                            //location.reload();
                            // $("#hard_choose_button").button("disable");
                            LoadTable2();

                        } else {
                            alert('ფაილი არ აიტვირთა');
                        }
                    },
                });

            }
        });
        //-------------------------------------------------//


        //-----------------exel shablon------------------//
        $(document).on("click", "#excel_template", function () {
            SaveToDisk('server-side/view/template.xls', 'template.xls');
        });

        //-----------------hard_exel shablon------------------//
        $(document).on("click", "#hard_excel_template", function () {
            SaveToDisk('server-side/view/hard_template.xls', 'hard_template.xls');
        });

        function SaveToDisk(fileURL, fileName) {
            var iframe = document.createElement("iframe");
            iframe.src = fileURL;
            iframe.style.display = "none";
            document.body.appendChild(iframe);
            return false;
        }

        // click on dialog loader buttons
        $(document).on("click", ".dialog-loader:not(.btn-active)", function () {

            // reacivate control buttons
            let self = $(this);

            // get requested dialog name
            let dialog = self.data("dialog");

            // load dialog
            loadDialog_event(dialog);

        });
        // click on dialog loader buttons

    </script>

    <!-- styles -->
    <style>

        /*controll button area*/
        #button_area {
            width: 100%;
            height: auto;
            box-sizing: padding-box;
            position: relative;
        }

        /*control button styles*/
        .jquery-ui-button {
            border: 1px solid #A3D0E4;
            background: #E6F2F8;
            color: #4d4c48;
            padding: 5px 10px;
            cursor: pointer;
        }

        .jquery-ui-button:not(.btn-active):hover {
            border: 1px solid #d3d2ce;
            background: #f5f4f0;
        }

        .btn-active {
            border: 1px solid #9c9b97;
            background: #e0dfdb;
            color: #4d4c48;
        }

        .jquery-ui-button span {
            position: relative;
            top: 1px;
            font-family: pvn;
        }

        /*main content container*/
        .main-content-container {
            width: 100%;
            height: auto;
            box-sizing: border-box;
            padding: 13px 0;
            margin-top: 50px;
        }

        /*main table styles*/
        .display {
            width: 100%;
        }

        /*SEND new sms styles*/
        .new-sms-send-container {
            border: 1px solid #cccccc;
            background: #F1F1F1;
            padding-bottom: 18px;
        }

        .new-sms-row {
            width: 100%;
            height: auto;
            margin-top: 11.3px;
            box-sizing: border-box;
        }

        .new-sms-row:last-child {
            margin-top: 4px;
        }

        .new-sms-input-holder {
            width: 100%;
            height: 27px;
            display: flex;
        }

        .new-sms-input-holder input {
            height: 75%;
            flex-grow: 1;
            font-size: 11.3px;
        }

        .new-sms-input-holder textarea {
            height: 75%;
            flex-grow: 1;
            font-size: 11.3px;
        }

        #smsCopyNum { /*button: copy*/
            margin: 0 5px;
        }

        #smsTemplate { /*button: open template*/
            margin-right: 0;
        }

        #smsNewNum i { /*icon: add new number in send new sms child*/
            font-size: 14px;
        }

        #smsCharCounter { /*input: sms character counter*/
            width: 55px;
            height: 18px;
        }

        #smsCharCounter { /*input: send new sms character counter*/
            position: relative;
            top: 0;
            text-align: center;
        }

        #sendNewSms { /*button: send new message action inplement*/
            float: right;
        }

        /*sms templates styles*/
        #box-table-b1 {
            font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
            font-size: 12px;
            text-align: center;
            border-collapse: collapse;
            border-top: 7px solid #70A9D2;
            border-bottom: 7px solid #70A9D2;
            width: 300px;
        }

        #box-table-b1 th {
            font-size: 13px;
            font-weight: normal;
            padding: 8px;
            background: #E8F3FC;;
            border-right: 1px solid #9baff1;
            border-left: 1px solid #9baff1;
            color: #4496D5;
        }

        #box-table-b1 td {
            padding: 8px;
            background: #e8edff;
            border-right: 1px solid #aabcfe;
            border-left: 1px solid #aabcfe;
            color: #669;
        }

        .download_shablon {
            background-color: #4997ab;
            border-radius: 2px;
            display: inline-block;
            cursor: pointer;
            color: #ffffff;
            font-family: arial;
            font-size: 14px;
            border: 0px;
            text-decoration: none;
            text-overflow: ellipsis;
        }

        .download_shablon:hover {
            background-color: #ffffff;
            color: #4997ab;
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .2);
        }

    </style>
    <!-- styles -->

</head>

<body>

<div id="tabs">
    <div class="callapp_head">
        საიტზე შეტყობინების დაგზავნა
        <hr class="callapp_head_hr">
    </div>

    <!-- controls area -->
    <div id="button_area">
        <button id="easyMailing" class="jquery-ui-button dialog-loader" data-dialog="easyMailingDialog">
            <span>მარტივი დაგზავნა</span>
        </button>
        <button id="complicateMailing" class="jquery-ui-button dialog-loader" data-dialog="complicateMailingDialog">
            <span>რთული დაგზავნა</span>
        </button>
    </div>
    <!-- controls area -->

    <!-- main content container -->
    <section class="main-content-container">

        <table class="display" id="main_table">
            <thead>
                <tr id="datatable_header">
                    <th>ID</th>
                    <th style="width: 12%;">თარიღი</th>
                    <th style="width: 7%;">User_Id</th>
                    <th style="width: 15%;">სახელი,გვარი</th>
                    <th style="width: 56%;">ტექსტი</th>
                    <th style="width: 10%;">სტატუსი</th>
                </tr>
            </thead>
            <thead>
                <tr class="search_header">
                    <th class="colum_hidden">
                        <input type="text" name="search_id" value="ფილტრი" class="search_init"/>
                    </th>
                    <th>
                        <input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                    </th>
                    <th>
                        <input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                    </th>
                    <th>
                        <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                    </th>
                    <th>
                        <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                    </th>
                    <th>
                        <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                    </th>
                </tr>
            </thead>
        </table>

    </section>
    <!-- main content container -->

    <!-- jQuery Dialog -->
    <div id="add-edit-form" class="form-dialog" title="ახალი შეტყობინება">
        <!-- aJax -->
    </div>
    <div id="add-edit-form1" class="form-dialog" title="შეტყობინებები-ები">
        <!-- aJax -->
    </div>
    <div id="smsShablon" title="შაბლონები">
    </div>

    <!-- jQuery Dialog -->
    <div id="add-edit-form-sms" class="form-dialog" title="ახალი შეტყობინება">
    </div>
    <!-- jQuery Dialog -->
    <div id="sms-templates" class="form-dialog" title="sms შაბლონები">
    </div>
    <!-- jQuery Dialog -->
    <div id="phone-directory-list" class="form-dialog" title="სატელეფონო ცნობარი">
    </div>
    <!-- jQuery Dialog -->
    <div id="new-number-in-directory" class="form-dialog" title="ნომრის დამატება">
    </div>

</body>







