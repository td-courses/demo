<html>
<head>
<script type="text/javascript">
    var aJaxURL_tabs            = "server-side/call/task/task_tabs.action.php";
    var aJaxURL_task            = "server-side/call/task/task.action.php";
    var aJaxURL_answer          = "server-side/call/chat_answer.action.php";
    var aJaxURL_getmail         = "includes/send_mail.php";
    var tName                   = "table_";
    var change_colum_main       = "<'dataTable_buttons'T><'F'Cfipl>";
    var dialog                  = "add-edit-form";
    var colum_number            = 11;
    var main_act                = "get_list";
	var addEditFormWidth        = 1063;
    var coment_id               = "";
    var coment_user_id          = "";
    var edit_coment_id          = "";
    var quantity                = "";


    $(document).ready(function (){
    	
        loadtabs();
        GetButtons("add_button_task","delete_button_task")
        setTimeout(() => {
            SetEvents("add_button_task", "delete_button_task", "", tName+'index', dialog, aJaxURL_task);
        }, 500);
        $("#filter_button").button();
        $("#excel_export").button();
        GetDate("start_date");
        GetDate("end_date");
        var start_date = new Date();
        start_date.setHours(0,0,0,0)
        $("#start_date").datepicker().datepicker("setDate", start_date);
        $("#end_date").datepicker().datepicker("setDate", new Date());
    })
    function active(id){
        
        $(id).attr( "aria-selected", "true");
        
        $(id).css("cursor", "grabbing");
    }
    function deactive(id){
        $(id).attr( "aria-selected", "false");
        
        $(id).css("cursor", "pointer");
    }
    $(document).on("click", "#view", function() {
        param 	  = new Object();
    	param.act = "view";
    	param.id  = $("#hidden_id").val();
    	$.ajax({
    		url: aJaxURL_task,
    		data: param,
    		success: function(data) {
    			if(typeof(data.error) != "undefined"){
    				if(data.error != ""){
    					alert(data.error);
    				}else{
        				alert('ოპერაცია წარმატებით შესრულდა')
    					var obj = new Object;
    		            obj.start_date  = $('#start_date').val();
    		            obj.end_date    = $('#end_date').val();
    		            obj.parent=$("#activeparent").val();
    		            obj.child=$("#activechild").val();
    		            loadtabs("#t_"+obj.parent,"#t_"+obj.child);
    		            LoadTable('index',colum_number,main_act,change_colum_main,obj);
    				}
    			}
    		}
    	});
    });
    $(document).on("click", "#tabs1 li", function() {
        if(!$(this).hasClass("child")){ // parent
            deactive("li[id^='t_']");
            $('div[id^="tab_"]').css("display","block");
            GetTabs("tab_"+$(this).attr("name"));
            active("#t_"+$(this).attr("name") );
            $('div[id^="tab_"]').not(".main_tab").not('.tab_'+$(this).attr("name")).css("display","none");
            active("#tab_"+$(this).attr("name")+" ul li:first-child");
            var obj = new Object;
            obj.parent=$(this).attr("name");
            obj.child=$("#tab_"+$(this).attr("name")+" ul li:first-child").attr("name");
            obj.start_date  = $('#start_date').val();
            obj.end_date    = $('#end_date').val();
            quantity_ajax();
            $("#activeparent").val(""+obj.parent);
            $("#activechild").val(""+obj.child);
            LoadTable('index',colum_number,main_act,change_colum_main,obj);
        } else { // child   
            deactive("li[id^='t_']");
            active("#"+$(this).attr("id") );
            active("#t_"+$(this).attr("id_select") );
            var obj = new Object;
            obj.parent  =$(this).attr("id_select");
            obj.child   =$(this).attr("name");
            obj.start_date  = $('#start_date').val();
            obj.end_date    = $('#end_date').val();
            quantity_ajax();
            $("#activeparent").val(""+obj.parent);
            $("#activechild").val(""+obj.child);
            LoadTable('index',colum_number,main_act,change_colum_main,obj);
            
        }
    });
   
    function LoadDialog(fName){
            if(fName == 'add-edit-form'){
                var buttons = {
                    "view": {
				            text: "გავეცანი",
				            id: "view"
				    },
                    "save": {
                        text: "შენახვა",
                        id: "save-dialog"
                    },
                    "cancel": {
                        text: "დახურვა",
                        id: "cancel-dialog",
                        click: function () {
                            $('#add-edit-form').dialog("close");
                        }
                    }
                };

                $("#side_menu").find(".info").trigger("click");
                ///// checkbox
                GetDialog(fName, "1063", "auto", buttons, 'top+43 top');
                $(".add-edit-form-class").css('left','86.5px');
                
                GetDateTimes("task_start_date");
                GetDateTimes("task_end_date");
                GetDateTimes("info_last_pay_date");
                GetDateTimes("monitoring");
                GetDateTimes("info_loan_date");
                GetDateTimes("history_start_date");
                GetDateTimes("history_end_date");
                GetDateTimes("ambition_registration_date");
                GetDateTimes("ambition_date");
                GetDateTimes1("date_time_input");
                $("#info_born_date, #info_ensuring_author_birthday, #info_entrusted_birthday").datepicker({
                    dateFormat: "yy-mm-dd",
                    changeMonth: true,
                    changeYear: true,
                    yearRange: "1900:2100"
                });
                
                if($("#wiev_status").val()==1){
                	param 	  = new Object();
        			param.act = "view";
        			param.id  = $("#hidden_id").val();
        			$.ajax({
        				url: aJaxURL_task,
        				data: param,
        				success: function(data) {
        					var obj = new Object;
				            obj.start_date  = $('#start_date').val();
				            obj.end_date    = $('#end_date').val();
				            obj.parent=$("#activeparent").val();
				            obj.child=$("#activechild").val();
				            loadtabs("#t_"+obj.parent,"#t_"+obj.child);
				            LoadTable('index',colum_number,main_act,change_colum_main,obj);
        				}
        			});
        		}
                
                $(".jquery_button").button();
                $('#add_comment').button();
                $("[data-select='chosen']").chosen({ search_contains: true });
                $("[data-button='jquery-ui-button']").button();
                $(".cat-listener").find(".chosen-container").css({width:"100%"});

                $('#task_recipient_id,#task_status_id,#source_info_id,branch_id,#in_type_id,#incomming_cat_1,#incomming_cat_1_1,#incomming_cat_1_1_1').chosen({ search_contains: true });
                $('#task_recipient_id_chosen,#task_status_id_chosen').css('width','240px');
                $("#incomming_cat_1, #incomming_cat_1_1, #incomming_cat_1_1_1, #my_site, #source_id, #inc_status, #s_u_status, #client_sex").chosen({ search_contains: true });
                // $("#info input,#info textarea").prop("readonly", true);
                // $('#info select').prop('disabled', true).trigger("chosen:updated");
            } else if(fName == 'add-edit-form-answer'){
                var buttons = {
                    "save": {
                        text: "შენახვა",
                        id: "save-dialog-answer",

                    },
                    "cancel": {
                        text: "დახურვა",
                        id: "cancel-dialog-answer", 
                        click: function () {
                                        $('#'+fName).dialog("close");
                        }  
                    }
                };
                GetDialog(fName, 358, "auto", buttons);
            }
            $('.ui-widget-overlay').css('z-index',99);
        }
        function LoadTable(tbl,col_num,act,change_colum,data){
        	start_date = $('#start_date').val();
            end_date   = $('#end_date').val();
            parent     = $("#activeparent").val();
            child      = $("#activechild").val();
            
            GetDataTable(tName+tbl,aJaxURL_tabs,act,col_num,"start_date="+start_date+"&end_date="+end_date+"&parent="+parent+"&child="+child, 0, "",1,"desc",'',change_colum);
            setTimeout(function(){
                $('.ColVis, .dataTable_buttons').css('display','none');
            }, 120);
        }

        function quantity_ajax(){
            var param = new Object;
            param.act         = "get_quantity";
            param.start_date  = $('#start_date').val();
            param.end_date    = $('#end_date').val();
            $.ajax({
                url:aJaxURL_tabs,
                data: param,
                success: function(data){
                    $( "li[id^='t_']" ).each(function(index) {
                        if(""+data.quantity[$(this).attr("name")]!="null")
                            $("#input_"+$(this).attr("name")).val(""+data.quantity[$(this).attr("name")]);
                    });
                }
            });
        }
          $(document).on("click", "#save-dialog", function () {
        	values         = $("#my_site").chosen().val();
        	task_recipient = $("#add-edit-form #task_recipient").val();
            param 				      = new Object();
            param.act			      = "save_incomming";
            param.task_id			  = $("#task_id").val();
            param.id			      = $("#hidden_id").val();
            param.incomming_call_id   = $("#incomming_call_id").val();

            param.task_status         = $("#task_status").val();
            param.task_status_2       = $("#task_status_2").val();
            param.task_branch         = $("#task_branch").val();
            param.task_date           = $("#task_date").val();
            param.task_start_date     = $("#task_start_date").val();
            param.task_end_date       = $("#task_end_date").val();
            param.task_description    = $("#task_description").val();
            param.task_position       = $("#task_position").val();
            param.task_recipient      = $("#add-edit-form #task_recipient").val();
            param.task_note           = $("#task_note").val();
           
            param.incomming_cat_1     = $("#incomming_cat_1").val();
            param.incomming_cat_1_1   = $("#incomming_cat_1_1").val();
            param.incomming_cat_1_1_1 = $("#incomming_cat_1_1_1").val();
            param.site                = values;
            
            
            if($('#task_start_date').val() == ''){
            	alert('შეავსე პერიოდის დასაწყისი!!!');
            }else if ($('#task_end_date').val() == ''){
            	alert('შეავსე პერიოდის დასასრული!!!');
            }else if($('#task_status').val() == 0){
                alert('აირჩიე სტატუსი!!!');
            }else if ($("#task_status").val() == "77" && $("#task_status_2").val() == "0"){
				alert("შეავსეთ ქვე-სტატუსი !");
			}else if ($('#task_branch').val() == 0){
            	alert('აირჩიე განყოფილება!!!');
            }else if ($('#task_recipient').val() == 0){
            	alert('აირჩიე დავალების მიმღები!!!');
            }else if (param.task_description == ''){
            	alert('შეავსეთ კომენტარი!!!');
            }else if (values == '' || values == null){
            	alert('შეავსეთ საიტი!!!');
            }else {
            	var link = GetAjaxData(param);
                $.ajax({
                    url: aJaxURL_task,
                    data: link, 
                    success: function(data) {
                        if(typeof(data.error) != "undefined"){
                            if(data.error != ""){
                                alert(data.error);
                            }else{
                                LoadTable('index', colum_number, main_act, change_colum_main);
                                
                                var obj = new Object;
                                obj.start_date  = $('#start_date').val();
                                obj.end_date    = $('#end_date').val();
                                obj.parent      = $("#activeparent").val();
                                obj.child       = $("#activechild").val();
                                loadtabs("#t_"+obj.parent,"#t_"+obj.child);
                                LoadTable('index',colum_number,main_act,change_colum_main,obj);
                                CloseDialog("add-edit-form");
                                console.log(data);
                                param1                   = new Object();
                                param1.task_id           = data.task_id;
                                param1.time              = data.time;
                                param1.task_type         = "1";
                                param1.status_name       = data.status_name;
                                param1.site              = values;
                                param1.send_type         = 1;
                                param1.type              = data.type;
                                
                                
                                var link1                = GetAjaxData(param1);
                                
                                $.ajax({
                                    url: aJaxURL_getmail,
                                    data: link1,
                            		success: function (data) {
                                    }
                                });
                                
                            }
                        }
                    }
                });
            }
        });
        
        $(document).on("click","#delete_button_task",function(){
            setTimeout(() => {
                location.reload();
            }, 200);
        });
        
        $(document).on("click","#filter_button",function(){
            var obj = new Object;
            obj.start_date  = $('#start_date').val();
            obj.end_date    = $('#end_date').val();
            obj.parent=$("#activeparent").val();
            obj.child=$("#activechild").val();
            loadtabs("#t_"+obj.parent,"#t_"+obj.child);
            LoadTable('index',colum_number,main_act,change_colum_main,obj);
            setTimeout(() => {
                SetEvents("add_button_task", "delete_button_task", "", 'table_index', 'add-edit-form', aJaxURL_task);
            }, 200);
        });
        
        function loadtabs(activep="", activech=""){
            
            obj = new Object;
            obj.act="get_tabs";
            $.ajax({
                url: aJaxURL_tabs,
                data:obj,
                success: function (data)
                {
                    $("#tabs1").html(data.page);
                    GetTabs("tab_0");
                    if(activep==""){
                        $("[id^=t_]").first().click();
                    } else{
                        $(activep).click();
                        if(activech!=""){
                            $(activech).click();
                        }
                    }
                    quantity_ajax();
                }
            })
        }
        function show_right_side(id){
            $("#right_side fieldset").not('.nothide').hide();
            $(".main-container").hide();

            $("#" + id).show();
            // check switch data for dialog container resizing
            $(".add-edit-form-class").css("width", addEditFormWidth);

            //$('#add-edit-form').dialog({ position: 'left top' });
            hide_right_side();
            var str = $("."+id).children('img').attr('src');
            str = str.substring(0, str.length - 4);
            $("."+id).children('img').attr('src',str+'_blue.png');
            $("."+id).children('div').css('color','#2681DC');

        }


        function hide_right_side(){
            $("#side_menu").children('spam').children('div').css('color','#FFF');
            $(".info").children('img').attr('src','media/images/icons/info.png');
            $(".record").children('img').attr('src','media/images/icons/record.png');
            $(".file").children('img').attr('src','media/images/icons/file.png');
            $(".chat_question").children('img').attr('src', 'media/images/icons/call_chat.png');
        }
        $(document).on("click", ".itm-unit:not(.itm-unit-active)", function() {
            //define variables
            let selector = $(this).data("selector");

            //deactivate all menu items and activate relevant one
            $(".itm-unit").removeClass("itm-unit-active");
            $(this).addClass("itm-unit-active");

            //deactivate info sections and activate menu relevant one
            $(".inc-info-section").removeClass("is-active");
            $(`.inc-info-section[data-selector='${selector}']`).addClass("is-active");

            //stylize chosen objects
            setTimeout(function() {
                $(".iiss-sc-unit").find(".chosen-container").css({width:"100%"});
            }, 10);
        });
        $(document).on("change", "#incomming_cat_1", function () {
            param = new Object();
            param.act = "cat_2";
            param.cat_id = $('#incomming_cat_1').val();
            $.ajax({
                url: aJaxURL_task,
                data: param,
                success: function (data) {
                    $("#incomming_cat_1_1").html(data.page);
                    $('#incomming_cat_1_1').trigger("chosen:updated");
                    if ($('#incomming_cat_1_1 option:selected').val() == 999) {
                        param = new Object();
                        param.act = "cat_3";
                        param.cat_id = $('#incomming_cat_1_1').val();
                        $.ajax({
                            url: aJaxURL_task,
                            data: param,
                            success: function (data) {
                                $("#incomming_cat_1_1_1").html(data.page);
                                $('#incomming_cat_1_1_1').trigger("chosen:updated");
                            }
                        });
                    }
                }
            });
        });
        $(document).on("change", "#incomming_cat_1_1", function () {
            param = new Object();
            param.act = "cat_3";
            param.cat_id = $('#incomming_cat_1_1').val();
            $.ajax({
                url: aJaxURL_task,
                data: param,
                success: function (data) {
                    $("#incomming_cat_1_1_1").html(data.page);
                    $('#incomming_cat_1_1_1').trigger("chosen:updated");
                }
            });
        });
         $(document).on("change", "#task_status, #task_status_2", function () {

            //define variables
            let actionObj = $(this).attr("id");
            let filterCat = $("#task_status").val();

            //reset sub category values
            if(actionObj === "task_status") {
                $.post(aJaxURL_task, {
                    act: "task_status_cat",
                    cat_id: filterCat
                }, function(result) {
                    $("#task_status_2").html(result.page);
                    $("#task_status_2").trigger("chosen:updated");
                });
            }

            //define variables
            let subCat = $("#task_status_2").val();
            let subFiltCat = `${filterCat}:${subCat}`;

            //remove class that gives elements visibility to hide them all
            $(".cat-listener").removeClass("incomming-cat-visible");

            //give visibility class to objects that belongs to chosen categories
            $(`.cat-listener[data-category="cat-level-${filterCat}"]`).addClass("incomming-cat-visible");
            $(`.cat-listener[data-subCategory="cat-level-${subFiltCat}"]`).addClass("incomming-cat-visible");

            //stylize chosen containers
            setTimeout(function() {
                $(".cat-listener").find(".chosen-container").css({width:"100%"});
            }, 10);
        });
        $(document).on("change", "#task_branch, #task_position", function () {
            var task_type       = $('#task_type').val();
            param               = new Object();
            param.act           = "task_branch_changed";
            param.cat_id        = $('#task_branch').val();
            param.position_id   = $('#task_position').val();
                $.ajax({
                    url: aJaxURL_task,
                    data: param,
                    success: function (data) {
                        $("#task_recipient").html(data.page);
                        $('#task_recipient').trigger("chosen:updated");
                    }
                });
        });
        //log-tab functions log functions
        $(document).on("click", ".log-tab", function(){
            GetDataTable("user_log_table",aJaxURL,"get_user_log",7,"incomming_call_id="+$("#incomming_id").val(),0,"",1,"desc",'',"<'dataTable_buttons'T><'F'Cfipl>");
            setTimeout(function(){
                $('.ColVis, .dataTable_buttons').css('display','none');
                }, 160);
            $("#log-tab").css("width", "100%");
        });
        //chat question functions
        $(document).on("click", "#add_comment", function () { // save answer 
            param                   = new Object();
            param.act               = 'save_comment';
            param.comment           = $("#add_ask").val();
            param.incomming_call_id = $("#incomming_id").val();
            param.task_id           = $("#task_id").val();
            if(param.comment!=""){
                $.ajax({
                    url: aJaxURL_task,
                    data: param,
                    success: function(data) {
                        param2                      = new Object();
                        param2.act                  = 'get_add_question';
                        param2.incomming_call_id    = $("#incomming_id").val();
                        param2.task_id    = $("#task_id").val();
                        $.ajax({
                            url: aJaxURL_task,
                            data: param2,
                            success: function(data) {
                                $("#chat_question").html(data.page);
                                $("#add_comment").button();
                            }
                        });
                    }
                });
            }
        });
        $(document).on("click", "#get_answer", function () { // get answer page
            LoadDialog("add-edit-form-answer");
            param           = new Object();
            param.act       = 'get_add_page';
            comment_id      = $(this).attr('class');
            $.ajax({
                url: aJaxURL_answer,
                data: param,
                success: function(data) {
                    $("#add-edit-form-answer").html(data.page);
                    $("#add_comment").button();
                }
            });
        });
        $(document).on("click", "#save-dialog-answer", function () { 
            // save answer 
            if($("#comment_info_sorce_id").val()==""){
                param                   = new Object();
                param.act               = 'save_answer';
                param.comment           = $("#in_answer").val();
                param.comment_id        = $('#hidden_comment_id_'+comment_id+'').val();
                param.incomming_call_id = $("#incomming_id").val();
                param.task_id           = $("#task_id").val();
                if(param.comment!=""){
                    $.ajax({
                        url: aJaxURL_answer,
                        data: param,
                        success: function(data) {
                            param2                   = new Object();
                            param2.act               = 'get_add_question';
                            param2.incomming_call_id = $("#incomming_id").val();
                            param2.task_id           = $("#task_id").val();
                            $.ajax({
                                url: aJaxURL_task,
                                data: param2,
                                success: function(data) {
                                    $("#add-edit-form-answer").dialog("close");
                                    $("#chat_question").html(data.page);
                                    $("#add_comment").button();
                                }
                            });
                        }
                    });
                } else {
                    alert("შეავსეთ ველი")
                }
            } else {
                param2          = new Object();
                param2.act      = 'update_comment';
                param2.id       = edit_comment_id;
                param2.comment  = $("#in_answer").val();
                $.ajax({
                    url: aJaxURL_task,
                    data: param2,
                    success: function(data) {
                        param2 = new Object();
                        param2.act='get_add_question';
                        param2.incomming_call_id = $("#incomming_id").val();
                        param2.task_id           = $("#task_id").val();
                        $.ajax({
                            url: aJaxURL_task,
                            data: param2,
                            success: function(data) {
                                $("#add-edit-form-answer").dialog("close");
                                $("#chat_question").html(data.page);
                                $("#add_comment").button();
                            }
                        });
                    }
                });
            }
        });
        $(document).on("click", "#delete_comment", function () { // save answer 
            param = new Object();
            param.act='delete_comment';
            param.comment_id=$(this).attr('my_id');
            $.ajax({
                    url: aJaxURL_task,
                    data: param,
                    success: function(data) {
                        param2 = new Object();
                        param2.act='get_add_question';
                        param2.incomming_call_id = $("#incomming_id").val();
                        param2.task_id           = $("#task_id").val();
                        $.ajax({
                            url: aJaxURL_task,
                            data: param2,
                            success: function(data) {
                                $("#chat_question").html(data.page);
                                $("#add_comment").button();
                            }
                        });
                    }
            });
        });
        $(document).on("click", "#edit_comment", function () { // save answer 
            param = new Object();
            param.act='get_edit_page';
            edit_comment_id=$(this).attr('my_id');
            param.id= $(this).attr('my_id');
            $.ajax({
                url: aJaxURL_answer,
                data: param,
                success: function(data) {
                    $("#add-edit-form-answer").html(data.page);
                    LoadDialog("add-edit-form-answer");
                    }
                });
            });
        //chat question functions end
        $(document).on("click", "#upload_file", function () {
            $('#file_name').click();
        });

        $(document).on("click", "#show_copy_prit_exel", function () {
	        if($(this).attr('myvar') == 0){
	            $('.ColVis,.dataTable_buttons').css('display','block');
	            $(this).css('background','#2681DC');
	            $(this).children('img').attr('src','media/images/icons/select_w.png');
	            $(this).attr('myvar','1');
	        }else{
	        	$('.ColVis,.dataTable_buttons').css('display','none');
	        	$(this).css('background','#E6F2F8');
	            $(this).children('img').attr('src','media/images/icons/select.png');
	            $(this).attr('myvar','0');
	        }
	    });
	    
        $(document).on("change", "#file_name", function () {
            var file_url  = $(this).val();
            var file_name = this.files[0].name;
            var file_size = this.files[0].size;
            var file_type = file_url.split('.').pop().toLowerCase();
            var path	  = "../../media/uploads/file/";

            if($("#incomming_id").val()!='' && $("#incomming_id").val()!=0){
                table_id   = $("#incomming_id").val();
                table_name = 'incomming_call';
            }else{
            	table_id = $("#task_id").val();
            	table_name = 'task';
            }
            
            if($.inArray(file_type, ['pdf', 'pptx', 'png','xls','xlsx','jpg','docx','doc','csv']) == -1){
                alert("დაშვებულია მხოლოდ 'pdf', 'png', 'xls', 'xlsx', 'jpg', 'docx', 'doc', 'csv' გაფართოება");
            }else if(file_size > '15728639'){
                alert("ფაილის ზომა 15MB-ზე მეტია");
            }else{
                $.ajaxFileUpload({
                    url: "server-side/upload/file.action.php",
                    secureuri: false,
                    fileElementId: "file_name",
                    dataType: 'json',
                    data: {
                        act: "file_upload",
                        button_id: "file_name",
                        table_name: table_name,
                        file_name: Math.ceil(Math.random()*99999999999),
                        file_name_original: file_name,
                        file_type: file_type,
                        file_size: file_size,
                        path: path,
                        table_id: table_id,
					},
                    success: function(data) {
                        if(typeof(data.error) != 'undefined'){
                            if(data.error != ''){
                                alert(data.error);
                            }else{
                                var tbody = '';
                                for(i = 0;i <= data.page.length;i++){
                                    tbody += "<div id=\"first_div\">" + data.page[i].file_date + "</div>";
                                    tbody += "<div id=\"two_div\">" + data.page[i].name + "</div>";
                                    tbody += "<a id=\"tree_div\" download href=\'media/uploads/file/" + data.page[i].rand_name + "\'>ჩამოტვირთვა</a>";
                                    tbody += "<div id=\"for_div\" onclick=\"delete_file('" + data.page[i].id + "', 'outgoing')\">-</div>";
                                    $("#add-edit-form #paste_files").html(tbody);
                                }							
                            }						
                        }					
                    }
                });
            }
        });
        function delete_file(id) {
        	if($("#incomming_id").val()!='' && $("#incomming_id").val()!=0){
                table_name = 'incomming_call';
            }else{
            	table_name = 'task';
            }
            $.ajax({
                url: "server-side/upload/file.action.php",
                data: "act=delete_file1&file_id=" + id + "&table_name="+table_name,
                success: function (data) {

                    var tbody = '';
                    if (data.page.length == 0) {
                        $("#paste_files").html('');
                    }
                    ;
                    for (i = 0; i <= data.page.length; i++) {
                        tbody += "<div id=\"first_div\">" + data.page[i].file_date + "</div>";
                        tbody += "<div id=\"two_div\">" + data.page[i].name + "</div>";
                        tbody += "<a id=\"tree_div\" download href=\'media/uploads/file/" + data.page[i].rand_name + "\'>ჩამოტვირთვა</a>";
                        tbody += "<div id=\"for_div\" onclick=\"delete_file('" + data.page[i].id + "')\">-</div>";
                        $("#paste_files").html(tbody);
                    }
                }
            });
        }
        function listen(file) {
            var url = 'http://pbx.my.ge/records/' + file;
            $("#record audio source").attr('src', url);
            $("#record audio").load();
        }
        //ინფოსა და პრეტენზიის ტაბების ზოგიერთი ველის დაკავშირება
        $(document).on("change paste keyup","#info_loan_number, #info_pid, #info_mobile, #info_email, #info_address", function() {
            var loan_number = $("#info_loan_number").val();
            var pid         = $("#info_pid").val();
            var mobile      = $("#info_mobile").val();
            var email       = $("#info_email").val();
            var address     = $("#info_address").val();
            var name        = $("#info_name").val();
            $("#ambition_loan_number").val(loan_number);
            $("#ambition_pid").val(pid);
            $("#ambition_mobile").val(mobile);
            $("#ambition_client_email").val(email);
            $("#ambition_client_address").val(address);
            $("#ambition_name").val(name);
        });

        $(document).on("click", "#excel_export", function () {
        	var parent = $("#activeparent").val();
            var child  = $("#activechild").val();
			var start  = $("#start_date").val();
			var end    = $("#end_date").val(); 
	        url="includes/excel/writexlsx.php?act=task&start="+start+"&end="+end+"&parent="+parent+"&child="+child;
	        window.open(url);
	    });
	    
        </script>
        <style>
    .ui-tabs .ui-tabs-panel {
        display: block!important;
        border-width: 0;
        padding: 0.2em 0em;
        background: none;
        
    }
    .caunt_imput{   
        width: 42px;
        border-radius: 22px;
        text-align: center;
        background: white;
        color: #F44336;
        
    }

    #tabs1 div {
        border:none;
    }
    #tabs{
         width: 99%;
    }
    #tabs1 li {
        border: none;
        background: none;
        outline: none;
        border-bottom: 1px solid #222422;
        cursor: pointer;
        margin-right: 11px;
        font-family: pvn;
        font-size: 11px;
        font-weight: bold;
        color: #222422;
    }

    #tabs1 li[aria-selected="true"] {
        font-size: 13px;
        color: #0b5c9a;
        border-bottom: 2px solid #0b5c9a;
    }
    #tabs1{
        height:70px!important;
        margin-bottom:50px;
    }
    a{
        cursor:pointer!important;
    }

    .download_shablon {
        background-color:#4997ab;
        border-radius:2px;
        display:inline-block;
        cursor:pointer;
        color:#ffffff;
        font-family:arial;
        font-size:14px;
        border:0px;
        text-decoration:none;
        text-overflow: ellipsis;
    }

    .download_shablon:hover {
        background-color:#ffffff;
        color:#4997ab;
        box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .2);
    }

    .ui-dialog-titlebar-close{
        display: none;
    }
    /*-INFO TOP MENU-*/
    .ifo-top-menu {
        width: 100%; height: 26px;
        margin-top: -50px;
        margin-left: -10px;
        position: relative;
        border-bottom: 1px solid #A3D0E4;
    }

    /*menu buttons*/
    .itm-unit {
        height: 100%;
        padding: 0 16px;
        float: left;
        background: none;
        border-width: 1px;
        border-style: solid;
        border-color: transparent;
        outline: none;
        cursor: pointer;
        font-family: pvn !important;
    }

    .itm-unit-active {
        border-color: #A3D0E4;
        border-bottom: none;
        background: #E6F2F8;
    }

    .itm-unit:not(.itm-unit-active):hover {
        color: #158da4;
    }


    /*hide side menu*/
    .hide_said_menu {
        height: 17px;
        top: 6px; right: 12px;
    }

    .ifo-top-menu .hide_said_menu {
        height: 15px;
        top: 4px; right: -21px;
        border: 1px solid #A3D0E4;
        background: #E6F2F8;
    }

    /*info section*/
    .inc-info-section {
        display: none;
    }

    .is-active {
        display: block;
    }

    /*info multiple selects section side container unit*/
    .iiss-sc-unit {
        width: 100%; height: auto;
        margin-right: 10px;
        margin-top: 7px;
        box-sizing: content-box;
    }

    [data-select="chosen"] {
        width: 100% !important;
    }

    /*info page category filter display*/
    .cat-listener {
        display: none;
    }

    .incomming-cat-visible {
        display: block;
    }

    /*all inputs styles*/
    #dialog-form input {
        height: 18px !important;
        font-size: 11.4px !important;
    }

    /*all chosen styles*/
    .chosen-container {
        font-size: 11px;
    }
    /*input edit button styles*/
    .input-edit-button {
        width: 37px !important; height: 21px !important;
    }
    .section-separator {
        width: 102.8%; height: 10px;
        background: white;
        margin-left: -11px;
        border-top: 1px solid #ccc;
        border-bottom: 1px solid #ccc;
    }

    .hidden{
        display: none;
    }
    #table_index td:nth-child(12) {
        padding:0px;
    }

    

    #question 
    {
        overflow-y : scroll;
        height: 584px;
    }
    
    #get_answer
    {
        cursor:pointer;
    }

    

    /*empty input style*/
    .empty-input {
        animation: empty-input .3s ease forwards;
        -webkit-animation: empty-input .3s ease forwards;
        -moz-animation: empty-input .3s ease forwards;
    }

    /*inputs*/
    .main-container input {
        background: none;
        outline: none;
        padding-left: 6px;
    }


    /*main container*/
    .main-container {
        max-width: 860px; height: 543px !important;
        margin-left: 0;
        font-size: 11px;
        letter-spacing: .7px;
        line-height: 21px;
        user-select: none;
        overflow: hidden;
        overflow-y: scroll;
        padding: 21px !important;
    }

    /*print wrapper*/
    .print-wrapper {
        width: 100%; height: auto;
    }

    /*header*/
    header {
        width: 100%; height: 52px;
        display: -webkit-flex;
        display: flex;
    }


    .header-lefter {
        -webkit-flex-grow: 1;
        flex-grow: 1;
    }

    #appCompleteDate {
        border: none;
        outline: none;
        border-bottom: 1px solid black;
        position: relative;
        top: 3px;
    }

    .header-img {
        -webkit-flex-basis: 141px;
        flex-basis: 141px;
        height: 100%;
    }

    .header-img img {
        height: 100%;
    }

    /*title holder*/
    .title-holder {
        width: 100%;
        padding: 8px;
        margin: 15px 0;
        text-align: center;
    }

    .title-holder h1 {
        font-size: 13px;
        font-family: pvn;
    }

    
    #add_comment
    {
        margin-left: 5px;
        top: -6px;
    }

    #edit_coment
    {
        height:13px;
    }

    #delete_coment
    {
        height:13px;
    }
    /*print button holder container*/
    .print-button-holder-container {
        width: 100%; height: 36px;
        margin-top: 18px;
        margin-bottom: 81px;
    }

    #printTemplate {
        padding: 8px 16px;
        background: darkgreen;
        font-family: pvn;
        color: whitesmoke;
        float: right;
        cursor: pointer;
        transition: background .2s ease;
    }

    #printTemplate:hover {
        background: green;
    }

    input[type="number"]::-webkit-outer-spin-button,
    input[type="number"]::-webkit-inner-spin-button {
        -webkit-appearance: none;
    }

    @media print {

        html body {
            display:none;
        }

        #menuwrapper {
            display: none;
        }

        .main-container {
            max-width: 860px; height: auto;
            margin-left: 0;
            font-size: 12.3px;
            letter-spacing: .7px;
            line-height: 27px;
            user-select: none;
            overflow: visible;
            padding: 0;
        }

        .print-wrapper {
            display: block !important;
            width: 100%; height: 100vh;
            position: absolute;
            top: 0; left: 0;
            z-index: 999999;
            background: white;
        }

        .header-lefter p {
            margin: 0 auto;
        }

        /*title holder*/
        .title-holder {
            width: 100%;
            text-align: center;
            padding: 0;
            margin: 0;
            margin-top: 5px;
        }

        /*fill up container*/
        .fill-up-container {
            margin-top: -10px;
        }

        /*contact data container*/
        .contact-data-container {
            margin-top: -20px;
        }

        /*conclusion container*/
        .conclusion-container {
            margin-top: -20px;
        }

        /*attachment container*/
        .attachment-container {
            margin-top: -20px;
        }

    }
    .tb_head td{
        border-right: 1px solid #E6E6E6;
    }

    .td_center{
        text-align: center !important;
    }
    .callapp_tabs{
        margin-top: 5px;
        margin-bottom: 5px;
        float: right;
        width: 100%;

    }
    .callapp_tabs span{
        color: #FFF;
        border-radius: 5px;
        padding: 5px;
        float: left;
        margin: 0 3px 0 3px;
        background: #2681DC;
        font-weight: bold;
        font-size: 11px;
        margin-bottom: 2px;
    }
    .callapp_tabs span close{
        cursor: pointer;
        margin-left: 5px;
    }
    .callapp_head_hr{
        border: 1px solid #2681DC;
    }
    .callapp_refresh{
        padding: 5px;
        border-radius:3px;
        color:#FFF;
        background: #9AAF24;
        float: right;
        font-size: 13px;
        cursor: pointer;
    }
    .callapp_filter_show{
        margin-bottom: 50px;
        float: right;
        width: 100%;
    }
    .callapp_filter_show button{
        margin-bottom: 10px;
        border: none;
        color: #2681DC;
        font-weight: bold;
        cursor: pointer;
    }
    .callapp_filter_header{
        color: #2681DC;
        font-family: pvn;
        font-weight: bold;
    }

    .ColVis, .dataTable_buttons{
        z-index: 50;
    }
    #table_index_wrapper #table_index_filter{
        width: 56%;
    }
    .callapp_filter_show button{
        margin-bottom: 0;
    }
    #table_right_menu{
        top: 39px;
    }
    #table_index tbody td:last-child {
        padding: 0;
    }
    #table_index_wrapper .ui-widget-header{
        height: 55px;
    }
    .callapp_filter_body{
        width: 100%;
        height: 83px;
        padding: 5px;
        margin-bottom: 0px;
    }
    .callapp_filter_body span {
        float: left;
        margin-right: 10px;
        height: 22px;
    }
    .callapp_filter_body span label {
        color: #555;
        font-weight: bold;
        margin-left: 20px;
    }
    .callapp_filter_body_label_fixer label {
        margin-left: 0 !important;
        margin-bottom: 1px;
        font-size: 10.6px !important;
    }
    .callapp_filter_body_label_fixer input {
        height: 13px;
        font-size: 11.6px;
        margin-bottom: 6px;
    }

    .callapp_filter_body_label_fixer input:last-of-type {
        margin-bottom: 13px;
    }

    .callapp_filter_header{
        color: #2681DC;
        font-family: pvn;
        font-weight: bold;
    }

    .ColVis, .dataTable_buttons{
        z-index: 50;
    }
    .callapp_filter_show button{
        margin-bottom: 0;
    }
    #table_right_menu{
        top: 23px;
    }
    #table_index tbody td:last-child {
        padding: 0;
    }
    #table_index thead th:last-child .DataTables_sort_wrapper{
        display: none;
    }

    .switch-listener {
        display: none;
    }

    .switch-listener-show {
        display: block;
    }

    .dialog-right-side {
        width: 67%;
        float: left;
        margin-left: 10px;
    }
    
    @keyframes empty-input {
        0% {
            border: 1px solid #c0392b;
        }
        30% {
            border: 1px solid transparent;
        }
        60% {
            border: 1px solid #c0392b;
        }
        90% {
            border: 1px solid transparent;
        }
        100% {
            border: 1px solid #c0392b;
        }
    }

    @-webkit-keyframes empty-input {
        0% {
            border: 1px solid #c0392b;
        }
        30% {
            border: 1px solid transparent;
        }
        60% {
            border: 1px solid #c0392b;
        }
        90% {
            border: 1px solid transparent;
        }
        100% {
            border: 1px solid #c0392b;
        }
    }

    @-moz-keyframes empty-input {
        0% {
            border: 1px solid #c0392b;
        }
        30% {
            border: 1px solid transparent;
        }
        60% {
            border: 1px solid #c0392b;
        }
        90% {
            border: 1px solid transparent;
        }
        100% {
            border: 1px solid #c0392b;
        }
    }
    
        /* info section css */
        #personal_info_table, #contact_info_table, #extra_info, #loan_info_table, .comment_table, #category_table, #ambition_table, #credit_card_info {
            width: 100%;
        }
        [data-selector="info"] td{
            vertical-align: middle;
            height:16px !important;
            width:150px;
        }
        [data-selector="info"] input{
            height:16px !important;
            width:120px;
        }
        [data-selector="info"]{
            height:550px;
            overflow-y: auto;
        }
        [data-selector="info"] legend{
            font-size:11px;
        }
        .arrow_input{
            float:left;
        }
        .arrow_button{
            cursor: pointer;
            margin-left: 4px;
            padding: 0px 5px;
            background: #E6F2F8;
            border: 1px solid #A3D0E4;
        }
        #loan_plan_button .ui-button-text{
            padding: 1px 8px 0px 8px;
        }
        .fa-exchange-alt{
            font-size: 14px;
        }
        .comment_table input, .comment_table textarea{
            width: 99.1%;
            resize: vertical;
        }
        .comment_table textarea{
            resize: vertical;
        }
        #right_side{
            width: 620px;
            float: left;
            margin-left: 4px;
        }
        
        [data-select="chosen"] {
            width: 100%;
        }
        
        #info table{
            border-collapse:separate; 
            border-spacing:0 0.2em;
        }

        [data-selector="ambition"] input{
            height:16px !important;
            width:195px;
        }
        [data-selector="ambition"] select{
            width:200px;
        }
        #ambition_table .chosen-container{
            width: 200px!important;
        }
        #ambition_description{
            height:50px;
            resize: vertical;
            width:100%;
        }
        /* ifo top menu */
        .ifo-top-menu {
            width: 100%; 
            height: 36px;
            margin-top: -13px;
            margin-left: -10px; 
            position: relative;
            border-bottom: 1px solid #A3D0E4;
        }
        .itm-unit {
            height: 100%;
            padding: 0 5px;
            float: left;
            background: none;
            border-width: 1px;
            border-style: solid;
            border-color: transparent;
            outline: none;
            cursor: pointer;
            font-family: pvn !important;
            font-weight:700;
        }
        .itm-unit-active {
            border-color: #A3D0E4;
            border-bottom: none;
            background: #E6F2F8;
        }
        .itm-unit:not(.itm-unit-active):hover {
            color: #158da4;
        }
        .inc-info-section {
            display: none;
        }

        .is-active {
            display: block;
        }
        .iiss-sc-unit {
            width: 100%; height: auto;
            margin-right: 10px;
            margin-top: 7px;
            box-sizing: content-box;
        }
        /* ifo top menu end*/
        /* task submenu */
        #task_info_table tr{
            width:100%;
        }
        #task_info_table td{
            width:230px;
        }
        #task_info_table input{
            width:150px;
        }
        #task_info_table select{
            width:155px;
        }
        #task_info_table .chosen-container{
            width: 155px!important;
        }
        #add_task_button{
            margin-bottom:10px;
        }
        /* task submenu end */

        /* chat question */
        
        #add_comment{
            margin-left: 5px;
            top: -6px;
        }
        #get_answer{
            cursor:pointer;
        }
        /* left side fieldset css */
        #left_side_fieldset input{
            width: 140px;
        }
        #left_side_fieldset select{
            width: 145px!important;
        }
        #left_side_fieldset textarea{
            height:69px;
            margin:0;
            resize:vertical;
            width:93%
        }
        #left_side_fieldset td{
            width: 170px;
        }
        /* main table status */
        .read-action {
            width: 100%;
			height: 100%;
			display: flex;
			align-items: center;
			justify-content: center;
			font-family: pvn;
			font-weight: bold;
			color: white;
			border-radius: 2px;
			background: rgba(39,174,96,0.8);;
		}
		.unread-action {
            width: 100%;
			height: 100%;
			display: flex;
			align-items: center;
			justify-content: center;
			font-family: pvn;
			font-weight: bold;
			color: white;
			border-radius: 2px;
			background: rgba(231,76,60,0.8);;
		}
.search_button {
    padding: 2px;
    border-radius: 0px;
    background: #009688;
    cursor: pointer;
    float: left;
}
        /* main table status end */
        /* filter div */
        #filter_div{
            margin-bottom: 10px;
        }
        #filter_button span{
            padding: 2px 8px 0px 8px;
        }
        /* filter div end*/
    </style>

    </head>
    <body>
    <div id="tabs">
        <div class="callapp_head"> დავალება<hr class="callapp_head_hr"></div>
        	<table id="table_right_menu" style="top: 183px; left: -1px;">
            	<tr>
            		<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;background:#2681DC;" id="show_table" myvar="0">
            			<img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
            		</td>
            		<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_log" myvar="0">
            			<img alt="log" src="media/images/icons/log.png" height="14" width="14">
            		</td>
            		<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_copy_prit_exel" myvar="0">
            			<img alt="link" src="media/images/icons/select.png" height="14" width="14">
            		</td>
            	</tr>
            </table>
        	<div id="tabs1"></div>
            <div id="section" class="section"></div>
            <div id="filter_div">
                <input type="text" id="start_date" value="">
                <input type="text" id="end_date" value="">
                <button id="filter_button">ფილტრი</button>
            </div>
            <button style="margin-bottom: 5px;" id="add_button_task">ახალი დავალება</button>
            <button id="delete_button_task">წაშლა</button>
    		<button id="excel_export">EXCEL</button>
            <table class="display" id="table_index" style="width: 100%;">
                <thead>
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width:6%;">N:</th>
                        <th style="width:11%;">ფორმირების თარიღი</th>
                        <th style="width:11%;">დასაწყისი</th>
                        <th style="width:11%;">დასასრული</th>
                        <th style="width:10%;">შემქმნელი</th>
                        <th style="width:10%;">დავალების მიმღები</th>
                        <th style="width:10%;">სტატუსი</th>
                        <th style="width:10%;">სტატუსი 2</th>
                        <th style="width:11%;">განყოფილება</th>
                        <th style="width:10%;">კომენტარი</th>
                        <th class="check">#</th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                            <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_number" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <div class="callapp_checkbox" >
                                <input type="checkbox" id="check-all"  name="check-all" />
                                <label for="check-all"></label>
                            </div>
                        </th>
                    </tr>
                </thead>
            </table>

<!-- statment styles -->
<input id="check_state" type="hidden" value="1" />
<input id="whoami" type="hidden" value="1" />
<input id="activeparent" type="hidden" value=""/>
<input id="activechild" type="hidden" value=""/>
<!-- jQuery Dialog -->
<div  id="add-edit-form" class="form-dialog" title="დავალება">
</div>
<div  id="add-edit-form-answer" class="form-dialog" title="პასუხი">
</div>
<input id="confirm_update" type="hidden" value="">
<input id="confirm_click" type="hidden" value="0">
    </body>
</html>