<head>
<style type="text/css">

.download {
	background-color:#4997ab;
	border-radius:2px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff !important;
	font-size:12px;
	border:0px !important;
	text-decoration:none;
	text-overflow: ellipsis;
	width: 98%;
	font-weight: normal !important;
}
 
.download1 {
   
	background: rgb(249, 155, 3);
	border-radius:0px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
}

.picture{
    height:22px;
    width:20px;
    
	background-image:url(monitoring_volume.png);
	background-size: 19px 25px;
	width: 100%;
            
}
.myButton:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #408c99), color-stop(1, #599bb3));
	background:-moz-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-webkit-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-o-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-ms-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:linear-gradient(to bottom, #408c99 5%, #599bb3 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#408c99', endColorstr='#599bb3',GradientType=0);
	background-color:#408c99;
}

#save-print{
    position: absolute;
    left: 17px;
}

.myButton:active {
	position:relative;
	top:1px;
}

.hidden{
	display: none;
}
.reqves_hist td{
    padding: 0 13px;
    border: inset 1px #B4D2E2;
    text-align: left;
}	
.reqves_hist td:nth-child(4){
    padding: 0 13px;
    border-right: inset 1px rgba(0, 0, 0, 0);
    text-align: left;
}
.high {
    height: 0px;
}
.callapp_head{
	font-family: pvn;
	font-weight: bold;
	font-size: 20px;
	color: #2681DC;
}
.callapp_head_hr{
	border: 1px solid #2681DC;
}
.callapp_refresh{
    padding: 5px;
    border-radius:3px;
    color:#FFF;
    background: #9AAF24;
    float: right;
    font-size: 13px;
    cursor: pointer;
}
.ui-widget-content {
    border: 0px solid #2681dc;
}
#example td:nth-child(14),#example1 td:nth-child(14),#example2 td:nth-child(14),#example3 td:nth-child(14) {
	padding:0px;
}
.chosen-container{
	width:200px!important;
}
</style>
<script type="text/javascript">
		var aJaxURL		= "server-side/call/inc_monitoring.action.php";	
		var aJaxURL1	= "server-side/call/task.action1.php";		//server side folder url
		var aJaxURL2	= "server-side/call/task.action2.php";
		var aJaxURL3	= "server-side/call/task.action3.php";
		var upJaxURL	= "server-side/upload/file.action.php";	
		
		var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
		var tName	          = "example";										//table name
		var fName	          = "add-edit-form";
		var tbName		      = "tabs";			
		var file_name         = '';
		var rand_file         = '';
		
		$(document).ready(function () {
			GetTabs(tbName);  
			GetTable0();
			var param = new Object();
			param.act = 'get_users';
			$("[data-select='chosen']").chosen({search_contains: 'true'});
			$.ajax({
				dataType:'json',
				url:aJaxURL,
				type: "POST",
				data:param,
				success:function(data){
					$("#operator_filter1").html(data.page);
					$("#operator_filter2").html(data.page);
					$("[data-select='chosen']").trigger("chosen:updated");
				}
			})
		});
		
		$(document).on("tabsactivate", "#tabs", function() {
        	var tab = GetSelectedTab(tbName);
        	if (tab == 0) {
        		GetTable0();
         	}else if(tab == 1){
        		GetTable1();
            }else if(tab == 2){
         		GetTable2();
          	}
        });
		
		function GetTable0() {
            LoadTable();
            $("#fillter").button();
            GetDateTimes("search_start_my");
			GetDateTimes("search_end_my");
            GetButtons("add_button", "","");			
			SetEvents("add_button", "", "", tName, fName, aJaxURL);
			
        }
        
        function GetTable1() {
            LoadTable1();
            SetEvents("", "", "", "example1", fName, aJaxURL);
            GetDateTimes("search_start_my1");
			GetDateTimes("search_end_my1");
			$("#fillter1").button();
        }
        
        function GetTable2() {
            LoadTable2();
            SetEvents("", "", "", "example2", fName, aJaxURL);
            GetDateTimes("search_start_my2");
			GetDateTimes("search_end_my2");
			$("#fillter2").button();
        }
        
		function LoadTable(){
			var start	= $("#search_start_my").val();
	    	var end		= $("#search_end_my").val();
			GetDataTable("example", aJaxURL, "get_list", 16, "", 0, "", 1, "desc","",change_colum_main);
			setTimeout(function(){$('.ColVis, .dataTable_buttons').css('display','none');}, 90);
		}
		
		function LoadTable1(){
			var start	= $("#search_start_my1").val();
			var end		= $("#search_end_my1").val();
			var user_id	= $("#operator_filter1").val();
			GetDataTable("example1", aJaxURL, "get_list1", 16, "start=" + start + "&end=" + end+"&user_id="+user_id, 0, "", 1, "desc","",change_colum_main);
			setTimeout(function(){$('.ColVis, .dataTable_buttons').css('display','none');}, 90);
		}
		
		function LoadTable2(){
			var start	= $("#search_start_my2").val();
	    	var end		= $("#search_end_my2").val();
	    	var user_id	= $("#operator_filter2").val();
			GetDataTable("example2", aJaxURL, "get_list2", 16, "start=" + start + "&end=" + end+"&user_id="+user_id, 0, "", 1, "desc","",change_colum_main);
			setTimeout(function(){$('.ColVis, .dataTable_buttons').css('display','none');}, 90);
		}
		
		
		function LoadDialog(){
			var buttons = {
					"print": {
			            text: "ბეჭვდა",
			            id: "save-print",
			            click: function () {
                            var divToPrint=document.getElementById('dialog-form');
                            var newWin=window.open('','Print-Window');
                            newWin.document.open();
                            newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');
                            newWin.document.close();
                            setTimeout(function(){newWin.close();},10);
			            }
			        },
			        "save": {
			            text: "შენახვა",
			            id: "save-dialog",
			            click: function () {
			            }
			        },
			        "cancel": {
			            text: "დახურვა",
			            id: "cancel-dialog",
			            click: function () {
			                $(this).dialog("close");
			            }
			        }
			    };
			
			GetDialog(fName, 1260, "auto", buttons);
			var id = $("#incomming_id").val();
			var cat_id = $("#category_parent_id").val();
			GetDateTimes("fact_end_date");
			

			if(id != '' && cat_id == 407){
				$("#additional").removeClass('hidden');
			}

			if($("#source_id").val()==4){
				$("#gare_div").css('display', '');
				$("#add-edit-form, .add-edit-form-class").css('width','1260px');
			}else{
				$("#gare_div").css('display', 'none');
				$("#add-edit-form, .add-edit-form-class").css('width','860px');
			}
			
			GetDateTimes("problem_date");

			$(".download5").button();
			
			$("#choose_button").button();
			$("#choose_button1").button();
			$("#choose_button2").button();
			$("#choose_button3").button();
			
			$(".dialog-form-table tr").css('height', '10px');
		}
		
		function CloseDialog(){
			$("#" + fName).dialog("close");
		}
		
		$(document).on("click", "#fillter", function () {
	    	LoadTable();
	    });

		$(document).on("click", "#fillter1", function () {
	    	LoadTable1();
	    });

		$(document).on("click", "#fillter2", function () {
	    	LoadTable2();
	    });

	    $(document).on("click", ".callapp_refresh", function () {
		    
	    	var tab = GetSelectedTab(tbName);

	    	if(tab == 0){
		    	LoadTable();
		    }else if(tab == 1){
		    	LoadTable1();
		    }else if(tab == 2){
		    	LoadTable2();
			}
	    });
	    
		$(document).on("click", ".download", function () {
			var str = 1;
			var link = ($(this).attr("str"));
			link = 'http://91.233.15.136:8181/' + link;
			var btn = {
			        "cancel": {
			            text: "დახურვა",
			            id: "cancel-dialog",
			            click: function () {
			                $(this).dialog("close");
			            }
			        }
			    };
			GetDialog_audio("audio_dialog", "auto", "auto",btn);
			$("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="'+link+'" type="audio/wav"> Your browser does not support the audio element.</audio>');
            $(".download").css( "background","#408c99" );
            $(this).css( "background","#FF5555" );
           
        });
        
		$(document).on("click", ".download1", function () {
			var str = 1;
			var link = ($(this).attr("str"));
			link = 'http://91.233.15.136:8181/' + link;
			var btn = {
			        "cancel": {
			            text: "დახურვა",
			            id: "cancel-dialog",
			            click: function () {
			                $(this).dialog("close");
			            }
			        }
			    };
			GetDialog_audio("audio_dialog", "auto", "auto",btn );
			$("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="'+link+'" type="audio/wav"> Your browser does not support the audio element.</audio>');
            $(".download1").css( "background","#F99B03" );
            $(this).css( "background","#33dd33" );
           
        });
        
	    $(document).on("click", "#jNotify", function () {
	    	var task_check_id = $("#task_check_id").val();
	    	$.ajax({
                url: aJaxURL3,
                type: "POST",
                data: "act=get_edit_page&id=" + task_check_id+"&viewed=viewed",
                success: function (data) {
                   $("#" + fName).html(data.page);
                   LoadDialog(fName);
                   LoadTable3();
                }
            });
        });
        
	    // Add - Save
	    $(document).on("click", "#save-dialog", function () {
	    	param                      = new Object();
	    	param.id                   = $("#id").val();
	    	param.monitoring_status_id = $("#monitoring_status_id").val();
	    	param.monitoring_coment    = $("#monitoring_coment").val();
	    	param.answer               = '';
	    	param.act                  = "save_incomming";
	    	
	    	var data = $('input[class=radio]:checked').map(function () { //Get Checked checkbox array
				 return $(this).attr('name');
	        }).get();
	        
	        for (var i = 0; i < data.length; i++) {
				var value     = $('input[name='+data[i]+']:checked').val();
				param.answer += "(1, NOW(),"+param.id+","+data[i]+","+value+"),";
			}
		    
			$.ajax({
		        url: aJaxURL,
			    data: param,
		        success: function(data) {       
					if(typeof(data.error) != 'undefined'){
						if(data.error != ''){
							alert(data.error);
						}else{
							$("#add-edit-form").dialog('close');
							var tab = GetSelectedTab(tbName);
							
					    	if(tab == 0){
						    	LoadTable();
						    }else if(tab == 1){
						    	LoadTable1();
						    }else if(tab == 2){
						    	LoadTable2();
							}
						}
					}
			    }
		    });
		});

		$(document).on("click", ".radio", function () {
	    	param 	       = new Object();
	    	
	    	param.act      = "get_percent";
	    	param.id       = $("#id").val();
	    	param.answer   = '';
	    	var data = $('input[class=radio]:checked').map(function () { //Get Checked checkbox array
				 return $(this).attr('name');
	        }).get();
	        
	        for (var i = 0; i < data.length; i++) {
				var value     = $('input[name='+data[i]+']:checked').val();
				param.answer += "("+param.id+","+data[i]+","+value+"),";
			}
	    	
	    	$.ajax({
		        url: aJaxURL,
			    data: param,
		        success: function(data) {
					if(typeof(data.error) != 'undefined'){
						if(data.error != ''){
							alert(data.error);
						}else{
							 $("#monitoring_percent").html(data.percent);
						}
					}
			    }
		    });
		});
		
		$(document).on("click", "#refresh_button", function () {
	    	
	    	param 				= new Object();
	    	param.act			= "refresh";
	    	
	    	$.ajax({
		        url: aJaxURL,
			    data: param,
		        success: function(data) {
					if(typeof(data.error) != 'undefined'){
						if(data.error != ''){
							alert(data.error);
						}else{
							 $("#search_end_my").val(data.date);
							 LoadTable();
						}
					}
			    }
		    });
		});

 		$(document).on("click", "#refresh_button1", function () {
	    	
	    	param 				= new Object();
	    	param.act			= "refresh";
	    	
	    	$.ajax({
		        url: aJaxURL3,
			    data: param,
		        success: function(data) {
					if(typeof(data.error) != 'undefined'){
						if(data.error != ''){
							alert(data.error);
						}else{
							 $("#search_end_my1").val(data.date);
							 LoadTable3();
						}
					}
			    }
		    });
		});
		
	    	
	    $(document).on("click", "#refresh-dialog", function () {
    	 	param 			= new Object();
		 	param.act		= "get_calls";
		 	
	    	$.ajax({
		        url: aJaxURL,
			    data: param,
		        success: function(data) {       
					if(typeof(data.error) != 'undefined'){
						if(data.error != ''){
							alert(data.error);
						}else{
							$("#last_calls").html(data.calls);
							$( ".insert" ).button({
							      icons: {
							        primary: "ui-icon-plus"
							      }
							});
						}
					}
			    }
		    });
		});

	    function show_right_side(id){
	        $("#right_side fieldset").hide();
	        $("#" + id).show();
	        //$(".add-edit-form-class").css("width", "1260");
	        //$('#add-edit-form').dialog({ position: 'left top' });
	        hide_right_side();
	        
	        var str = $("."+id).children('img').attr('src');
			str = str.substring(0, str.length - 4);
			$("#side_menu spam").children('img').css('border-bottom','none');
	        $("."+id).children('img').css('filter','brightness(0.1)');
	        $("."+id).children('img').css('border-bottom','2px solid #333');
	    }

	    function hide_right_side(){
	    	
	        $(".info").children('img').css('filter','brightness(1.1)');
	        $(".task").children('img').css('filter','brightness(1.1)');
	        $(".record").children('img').css('filter','brightness(1.1)');
	        $(".file").children('img').css('filter','brightness(1.1)');
	        $(".history").children('img').attr('src','media/images/icons/history.png');
	        $(".monitoring").children('img').css('filter','brightness(1.1)');
	        $("#record fieldset").show();
	    }

	    $(document).on("click", "#show_copy_prit_exel", function () {
	        if($(this).attr('myvar') == 0){
	            $('.ColVis,.dataTable_buttons').css('display','block');
	            $(this).css('background','#2681DC');
	            $(this).children('img').attr('src','media/images/icons/select_w.png');
	            $(this).attr('myvar','1');
	        }else{
	        	$('.ColVis,.dataTable_buttons').css('display','none');
	        	$(this).css('background','#E6F2F8');
	            $(this).children('img').attr('src','media/images/icons/select.png');
	            $(this).attr('myvar','0');
	        }
	    }); 
</script>
</head>

<body>
<div class="callapp_head" style="padding: 14px 13px 0 5px;">მონიტორინგი<span class="callapp_refresh"><img alt="refresh" src="media/images/icons/refresh.png" height="14" width="14">   განახლება</span><hr class="callapp_head_hr"></div>
<div id="tabs" style="width: 99%;">
	<ul>
		<li><a href="#tab-0">შესამოწმებელი</a></li>
		<li><a href="#tab-1">შემოწმებული</a></li>
		<li><a href="#tab-2">არქივი</a></li>
	</ul>
	<table id="table_right_menu" style="top: 100px;left: -18px;">
        <tr>
        	<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;background:#2681DC;" id="show_table" myvar="0">
        		<img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
        	</td>
            <td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_log" myvar="0">
            	<img alt="log" src="media/images/icons/log.png" height="14" width="14">
            </td>
            <td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_copy_prit_exel" myvar="0">
            	<img alt="link" src="media/images/icons/select.png" height="14" width="14">
            </td>
        </tr>
    </table>
    <div id="tab-0">
    	<div id="dt_example" class="ex_highlight_row">
       		<div id="button_area" style="margin-top: 50px;">
           		
        	</div>	
        	<table class="display" id="example" style="width: 100%;">
                <thead>
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width: 100px;">ID</th>
                        <th style="width: 25%;">თარიღი</th>
                        <th style="width: 25%;">მეთოდი</th>
                        <th style="width: 25%;">ტელეფონი</th>
                        <th style="width: 25%;">ოპერატორი</th>
                        <th style="width: 100px;">საუბ.<br>დრო</th>
                        <th style="width: 100px;">ქმედება</th>
                        <th style="width: 30px;"></th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                        	<input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 80%"/>
                        </th>
                        <th>
                        	<input type="text" name="search_number" value="ფილტრი" class="search_init" style="width: 80%">
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 80%"/>
                        </th>
                        <th>
                            <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 80%"/>
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80%" />
                        </th>
                        <th>
                            <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 80%"/>
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80%" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80%" />
                        </th>
                        <th>
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
     </div>

     <div id="tab-1">
    	<div id="dt_example" class="ex_highlight_row">
        	<div id="button_area" style="margin-top: 50px;">
        		<table>
               		<tr>
                   		<td>
                   			<label for="search_end_my" class="left" style="margin: 8px 0 0 9px;">დან-</label>
                   		</td>
                   		<td>
                   			<input style="width: 100px; height: 15px;" value="<?php echo date('d/m/y').' 00:00';?>" type="text" name="search_start" id="search_start_my1" class="inpt left"/>
                   		</td>
                   		<td>
                   			<label for="search_end_my" class="left" style="margin: 8px 0 0 9px;">მდე-</label>
                   		</td>
                   		<td>
                   			<input style="width: 100px; height: 15px;" value="<?php echo  date('d/m/y')." 23:59"; ?>" type="text" name="search_end" id="search_end_my1" class="inpt right" />
                   		</td>
						<td>
                   			<label for="operator_filter1" class="left" style="margin: 8px 0 0 9px;">ოპერატორი </label>
                   		</td>
						<td>
                   			<select id="operator_filter1" data-select='chosen' style="margin-left: 10px;width:200px;">
							
							</select>
                   		</td>
                   		<td>
                   			<button id="fillter1" style="height: 25px;margin-left: 10px; border-radius: 0px">ფილტრი</button>
                   		</td>
               		</tr>
           		</table>
        	</div>
       		<table class="display" id="example1" style="width: 100%;">
                <thead>
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width: 100px;">ID</th>
                        <th style="width: 25%;">თარიღი</th>
                        <th style="width: 25%;">მეთოდი</th>
                        <th style="width: 25%;">ტელეფონი</th>
                        <th style="width: 25%;">ოპერატორი</th>
                        <th style="width: 100px;">ქულა</th>
                        <th style="width: 100px;">საუბ.<br>დრო</th>
                        <th style="width: 100px;">ქმედება</th>
                        <th style="width: 30px;"></th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                        	<input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 80%"/>
                        </th>
                        <th>
                        	<input type="text" name="search_number" value="ფილტრი" class="search_init" style="width: 80%">
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 80%"/>
                        </th>
                        <th>
                            <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 80%"/>
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80%" />
                        </th>
                        <th>
                            <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 80%"/>
                        </th>
                        <th>
                            <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 80%"/>
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80%" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80%" />
                        </th>
                        <th>
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
       </div>
       <div id="tab-2">
       		<div id="dt_example" class="ex_highlight_row">
       			<div id="button_area" style="margin-top: 50px;">
       				<table>
               		<tr>
                   		<td>
                   			<label for="search_end_my" class="left" style="margin: 8px 0 0 9px;">დან-</label>
                   		</td>
                   		<td>
                   			<input style="width: 100px; height: 15px;" value="<?php echo date('d/m/y').' 00:00';?>" type="text" name="search_start" id="search_start_my2" class="inpt left"/>
                   		</td>
                   		<td>
                   			<label for="search_end_my" class="left" style="margin: 8px 0 0 9px;">მდე-</label>
                   		</td>
                   		<td>
                   			<input style="width: 100px; height: 15px;" value="<?php echo  date('d/m/y')." 23:59"; ?>" type="text" name="search_end" id="search_end_my2" class="inpt right" />
                   		</td>
						<td>
                   			<label for="operator_filter2" class="left" style="margin: 8px 0 0 9px;">ოპერატორი </label>
                   		</td>
						<td>
                   			<select id="operator_filter2" data-select='chosen' style="margin-left: 10px;width:200px;">
							
							</select>
                   		</td>
                   		<td>
                   			<button id="fillter2" style="height: 25px;margin-left: 10px; border-radius: 0px">ფილტრი</button>
                   		</td>
               		</tr>
           		</table>
        		</div>
            	<table class="display" id="example2" style="width: 100%;">
                    <thead>
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width: 100px;">ID</th>
                            <th style="width: 25%;">თარიღი</th>
                            <th style="width: 25%;">მეთოდი</th>
                            <th style="width: 25%;">ტელეფონი</th>
                            <th style="width: 25%;">ოპერატორი</th>
                            <th style="width: 100px;">ქულა</th>
                            <th style="width: 100px;">საუბ.<br>დრო</th>
                            <th style="width: 100px;">ქმედება</th>
                            <th style="width: 30px;"></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                            	<input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 80%"/>
                            </th>
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init" style="width: 80%">
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80%" />
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80%" />
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80%" />
                            </th>
                            <th>
                            </th>
                        </tr>
                    </thead>
                </table>
         </div>
     </div>
</div>
<div id="add-edit-form" class="form-dialog" title="შემომავალი"></div>
<div id="audio_dialog" title="ჩანაწერი">
	</div>
</body>
	