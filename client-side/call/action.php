<head>

<style type="text/css">\
.myButton:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #408c99), color-stop(1, #599bb3));
	background:-moz-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-webkit-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-o-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-ms-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:linear-gradient(to bottom, #408c99 5%, #599bb3 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#408c99', endColorstr='#599bb3',GradientType=0);
	background-color:#408c99;
}

.myButton:active {
	position:relative;
	top:1px;
}

.hidden{
	display: none;
}

.reqves_hist td{
    padding: 0 13px;
    border: inset 1px #B4D2E2;
    text-align: left;
}	

.reqves_hist td:nth-child(4){
    padding: 0 13px;
    border-right: inset 1px rgba(0, 0, 0, 0);
    text-align: left;
}
.reqves_hist td{
    padding: 0 13px;
    border: inset 1px #B4D2E2;
    text-align: left;
}	
.reqves_hist td:nth-child(4){
    padding: 0 13px;
    border-right: inset 1px rgba(0, 0, 0, 0);
    text-align: left;
}
.high {
    height: 15px;
}
.callapp_head{
	font-family: pvn;
	font-weight: bold;
	font-size: 20px;
	color: #2681DC;
}
.callapp_head_hr{
	border: 1px solid #2681DC;
}
.callapp_refresh{
    padding: 5px;
    border-radius:3px;
    color:#FFF;
    background: #9AAF24;
    float: right;
    font-size: 13px;
    cursor: pointer;
}
.ui-widget-content {
    border: 0px solid #2681dc;
}
.action-status {
	width: 100%; height: 100%;
	display: flex;
	align-items: center;
	justify-content: center;
	font-family: pvn;
	font-weight: bold;
	color: white;
	border-radius: 2px;
}
.read-action {
	background: green;
}
.unread-action {
	background: red;
}
#action_content {
	min-height: 360px;
}
#tabs1 {
    width: 85.5%;
    height: 600px;
    background: #FFF;
    padding: 15px;
    margin-bottom: 10px;
}
</style>
	<script type="text/javascript">
		var aJaxURL		= "server-side/call/action/action.action.php";
		var curent_url	= "server-side/call/action/action.action.php";		//server side folder url
		var aJaxURL1	= "server-side/call/action/action.action1.php";
		var aJaxURL2	= "server-side/call/action/action.action2.php";		//server side folder url
		var aJaxURL3	= "server-side/call/action/action.action3.php";	
		var upJaxURL	= "server-side/upload/file.action.php";	
		var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
		var tName		= "example0";	
		var root_tbName	= "root_tabs";										//table name
		var tbName		= "tabs";											//tabs name
		var fName		= "add-edit-form";
		var fName1		= "add-edit-form-sport";							//form name
		var file_name = '';
		var rand_file = '';		
		$(document).ready(function () {     
			GetTabs(tbName);   	
			GetTabs('tabs1');	
			GetTable0();
			GetButtons("add_button","delete_button");	
			GetButtons("add_button1","delete_button1");		
		});

		$(document).on("tabsactivate", "#tabs", function() {
			curent_url = aJaxURL;
        	var tab = GetSelectedTab(tbName);
        	if (tab == 0) {
        		GetTable0();
        	}else if(tab == 1){
        		GetTable1();
            }
        });
        
		 function GetTable0() {
            LoadTable();
            SetEvents("add_button", "delete_button", "check-all_action", "example11_1", fName, aJaxURL);
         } 
               
		 function GetTable1() {
			 GetButtons("","delete_button_ar");
             LoadTable1();
             SetEvents("", "delete_button_ar", "check-all_arq", "example11_11", fName, aJaxURL);
		 }


		function LoadTable(hide){			
			/* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
			GetDataTable("example11_1", aJaxURL, "get_list", 8, "tarns_hid_id=" + hide, 0, "", 1,"asc", "","<'dataTable_buttons'T><'F'Cfipl>");
			setTimeout(function(){$('.ColVis, .dataTable_buttons').css('display','none');}, 90);
		}		
			
		function LoadTable1(hide1){	
			/* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
			GetDataTable("example11_11", aJaxURL1, "get_list", 8, "tarns_hid_id=" + hide1, 0, "", 1, "asc", "","<'dataTable_buttons'T><'F'Cfipl>");
			setTimeout(function(){$('.ColVis, .dataTable_buttons').css('display','none');}, 90);
		}
		
		function LoadTable2(){						
			/* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
			GetDataTable("example3", aJaxURL2, "get_list", 4,"act_id="+$('#act_id').val(), 0, "", 1, "asc", '', "<'F'lip>");
			$(".dataTables_length").css('top', '0px');
		}
		
		function LoadTable3(){						
			/* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
			GetDataTable("example4", aJaxURL3, "get_list", 4,"act_id="+$('#act_id').val(), 0, "", 1, "asc", '', "<'F'lip>");
			$(".dataTables_length").css('top', '0px');
		}
		

	//SeoYyy
		$(document.body).click(function (e) {
        	$("#send_to").autocomplete("close");
        });

        function LoadDialog(fName){ 
			switch(fName){
				case "add-edit-form":
					var buttons = {
						"view": {
				            text: "გავეცანი",
				            id: "view"
				        }, 
						"save": {
				            text: "შენახვა",
				            id: "save-dialog"
				        }, 
			        	"cancel": {
				            text: "დახურვა",
				            id: "cancel-dialog",
				            click: function () {
				            	$(this).dialog("close");
				            }
				        } 
				    };
					
					GetDialog("add-edit-form", 1015, "auto", buttons);
					$("#my_site").chosen({ search_contains: true });
					GetDateTimes("start_date");
					GetDateTimes("end_date");
					LoadTable2();
					LoadTable3();
					GetButtons("add_button_p","");
					GetButtons("add_button_pp","");
					SetEvents("add_button_p", "", "", "example3", "add-edit-form2", aJaxURL2, "act_idd="+$('#act_id').val());						
					SetEvents("add_button_pp", "", "", "example4", "add-edit-form3", aJaxURL3, "act_id="+$('#act_id').val());						
					break;	
				case "add-edit-form1":
					var buttons = {
						"save": {
				            text: "შენახვა",
				            id: "save-dialog1"
				        }, 
			        	"cancel": {
				            text: "დახურვა",
				            id: "cancel-dialog",
				            click: function () {
				            	$(this).dialog("close");
				            }
				        }
				       
				    };
					GetDialog("add-edit-form1", 1080, "auto", buttons);
					LoadTable2();	
					break;
				case "add-edit-form2":
					var buttons = {
						"save": {
				            text: "შენახვა",
				            id: "save-dialog2"
				        }, 
			        	"cancel": {
				            text: "დახურვა",
				            id: "cancel-dialog",
				            click: function () {
				            	$(this).dialog("close");
				            }
				        }
				    };
					GetDialog("add-edit-form2", 400, "auto", buttons);
					LoadTable2();
					GetDate("date");
					break;	
				case "add-edit-form3":
					var buttons = {
						"save": {
				            text: "შენახვა",
				            id: "save-dialog3"
				        }, 
			        	"cancel": {
				            text: "დახურვა",
				            id: "cancel-dialog",
				            click: function () {
				            	$(this).dialog("close");
				            }
				        }
				    };
					GetDialog("add-edit-form3", 400, "auto", buttons);
					LoadTable3();
					GetDate("date");					
					break;	
				
		}
		GetDateTimes("planned_end_date");
			
			$( ".calls" ).button({
			      icons: {
			        primary: " ui-icon-contact"
			      }
			});
			$("#choose_button").button({
	            
		    });
		}

		
	    // Add - Save
	    $(document).on("click", "#save-dialog", function () {
		    
	    	values = $("#my_site").chosen().val();
			param 				 = new Object();
			param.act			 = "save_action";
			param.id			 = $("#actionn_id").val();
			param.act_id		 = $("#act_id").val();
			param.action_name	 = $("#action_name").val();
			param.start_date	 = $("#start_date").val();
			param.end_date		 = $("#end_date").val();
			param.action_content = $("#action_content").val();
			param.h_start_date	 = $("#h_start_date").val();
			param.h_end_date	 = $("#h_end_date").val();
			param.site_id		 = values;
			var link = GetAjaxData(param);
		    $.ajax({
		        url: aJaxURL,
			    data: link,
		        success: function(data) {       
					if(typeof(data.error) != "undefined"){
						if(data.error != ""){
							alert(data.error);
						}else{
							LoadTable();
							LoadTable1()
							CloseDialog("add-edit-form");
						}
					}
			    }
		    });
		});
	    $(document).on("click", "#view", function () {

			param 	  = new Object();
			param.act = "view_action";
			param.id  = $("#actionn_id").val();
			
			$.ajax({
		        url: aJaxURL,
			    data: param,
		        success: function(data) {       
					if(typeof(data.error) != "undefined"){
						if(data.error != ""){
							alert(data.error);
						}else{
							alert('სიახლის გაცნობა წარმატებით განხორციელდა');
							LoadTable();
							LoadTable1();
							getUnreadActionCount();
							CloseDialog("add-edit-form");
							
						}
					}
			    }
		    });
		});

		$(document).on("click", "#save-dialog2", function () {
	    	
			param 					= new Object();
			param.local_id			= $("#action_id").val();
 			param.act				= "save_action_1";
 			
 			param.act_id			= $("#act_id").val();	
 			param.act_iddd			= $("#act_idd").val();
 			param.id				= $("#id").val();
			param.production_id		= $("#production_id").val();
	    	param.object_id			= $("#object_id").val();
	    	param.action_id			= $("#action_id").val();
	    	param.price				= $("#price").val();
			param.date				= $("#date").val();
			param.adress			= $("#adress").val();
			
			
	 
 	    	$.ajax({
 			        url: aJaxURL2,
 				    data: param,
 			        success: function(data) {       
 						if(typeof(data.error) != "undefined"){
 							if(data.error != ""){
 								alert(data.error);
 							}else{
 								LoadTable2();
 								CloseDialog("add-edit-form2");
 							}
						}
 				    }
 			});
 		});
 		
        $(document).on("click", "#save-dialog3", function () {
	    	
			param 					= new Object();
			param.act				= "save_action_3";
 			
 			param.id				= $("#id").val();			
 			param.act_id			= $("#act_id").val();
			param.object_id			= $("#object_id").val();
	    	param.adress			= $("#adress").val();
	    	param.action_id			= $("#action_id").val();
			
			
	 
 	    	$.ajax({
 			        url: aJaxURL3,
 				    data: param,
 			        success: function(data) {       
 						if(typeof(data.error) != "undefined"){
 							if(data.error != ""){
 								alert(data.error);
 							}else{
 								LoadTable3();
 								CloseDialog("add-edit-form3");
 							}
						}
 				    }
 			});
 		});
	 function SetPrivateEvents(add,check,formName){
		$(document).on("click", "#" + add, function () {    
	        $.ajax({
	            url: aJaxURL,
	            type: "POST",
	            data: "act=get_responsible_person_add_page",
	            dataType: "json",
	            success: function (data) {
	                if (typeof (data.error) != "undefined") {
	                    if (data.error != "") {
	                        alert(data.error);
	                    }else{
	                        $("#" + formName).html(data.page);
	                        if ($.isFunction(window.LoadDialog)){
	                            //execute it
	                        	LoadDialog1();
	                        }
	                    }
	                }
	            }
	        });
	    });
		
	    $(document).on("click", "#" + check, function () {
	    	$("#" + tName + " INPUT[type='checkbox']").prop("checked", $("#" + check).is(":checked"));
	    });	
	}

	 $(document).on("click", ".callapp_refresh", function () {
	   	 var tab = GetSelectedTab(tbName);
         if (tab == 0) {
          		LoadTable();
           	}else{
             	LoadTable1();
            }
	    });
	
	 $(document).on("click", "#show_copy_prit_exel", function () {
	        if($(this).attr('myvar') == 0){
	            $('.ColVis,.dataTable_buttons').css('display','block');
	            $(this).css('background','#2681DC');
	            $(this).children('img').attr('src','media/images/icons/select_w.png');
	            $(this).attr('myvar','1');
	        }else{
	        	$('.ColVis,.dataTable_buttons').css('display','none');
	        	$(this).css('background','#E6F2F8');
	            $(this).children('img').attr('src','media/images/icons/select.png');
	            $(this).attr('myvar','0');
	        }
	    });
	
	    $(document).on("keyup", "#req_time1, #req_time2", function() {
	        var val = $(this).val();
	        if(isNaN(val) || (val>60)){
		        
	         alert("მოცემულ ველში შეიყვანეთ მხოლოდ ციფრები");
	             val = val.replace(/[^0-9\.]/g,'');
	             if(val.split('.').length>2) 
	                 val =val.replace(/\.+$/,"");
	        }
	        $(this).val(val); 
	    });

	    $(document).on("change", "#task_type_id",function(){
		    var task_type = $("#task_type_id").val();

			if(task_type == 1){
				$("#task_department_id").val(37);
			}
		    
	    });

	    $(document).on("click", "#download", function () {
	    	var download_file	= $(this).val();
	    	var download_name 	= $('#download_name').val();
	    	SaveToDisk(download_file, download_name);
	    });

	    // function SaveToDisk(fileURL, fileName) {
	    //     // for non-IE
	    //     if (!window.ActiveXObject) {
	    //         var save = document.createElement('a');
	    //         save.href = fileURL;
	    //         save.target = '_blank';
	    //         save.download = fileName || 'unknown';

	    //         var event = document.createEvent('Event');
	    //         event.initEvent('click', true, true);
	    //         save.dispatchEvent(event);
	    //         (window.URL || window.webkitURL).revokeObjectURL(save.href);
	    //     }
		//      // for IE
	    //     else if ( !! window.ActiveXObject && document.execCommand)     {
	    //         var _window = window.open(fileURL, "_blank");
	    //         _window.document.close();
	    //         _window.document.execCommand('SaveAs', true, fileName || fileURL)
	    //         _window.close();
	    //     }
	    // } 

		function SaveToDisk(fileURL, fileName) {
			$("#downloader").attr("href",fileURL);
			$("#downloader")[0].click();
	    	var iframe = document.createElement("iframe");
	        iframe.src = fileURL;
	        iframe.style.display = "none";
	        document.body.appendChild(iframe);
	        return false;
	    }
	    
	    $(document).on("click", "#delete", function () {
	    	var delete_id	= $(this).val();
	    	
	    	$.ajax({
		        url: curent_url,
			    data: {
					act: "delete_file",
					delete_id: delete_id,
					edit_id: $("#act_id").val(),
				},
		        success: function(data) {
			        $("#file_div").html(data.page);
			    }
		    });	
		});
		
	    $(document).on("click", "#choose_button", function () {
		    $("#choose_file").click();
		});
		
	    $(document).on("change", "#choose_file", function () {
	    	var file		= $(this).val();	    
	    	var files 		= this.files[0];
		    var name		= uniqid();
		    var path		= "../../media/uploads/file/";
		    
		    var ext = file.split('.').pop().toLowerCase();
	        if($.inArray(ext, ['pdf', 'pptx','png','xls','xlsx','jpg','docx','doc','csv']) == -1) { //echeck file type
	        	alert('This is not an allowed file type.');
                this.value = '';
	        }else{
	        	file_name = files.name;
	        	rand_file = name + "." + ext;
	        	$.ajaxFileUpload({
	    			url: upJaxURL,
	    			secureuri: false,
	    			fileElementId: "choose_file",
	    			dataType: 'json',
	    			data:{
						act: "upload_file",
						path: path,
						file_name: name,
						type: ext
					},
	    			success: function (data, status){
	    				if(typeof(data.error) != 'undefined'){
    						if(data.error != ''){
    							alert(data.error);
    						}
						}
						
    							
	    				$.ajax({
					        url: curent_url,
						    data: {
								act: "up_now",
								rand_file: rand_file,
					    		file_name: file_name,
								edit_id: $("#act_id").val(),

							},
					        success: function(data) {
						        $("#file_div").html(data.page);
						    }
					    });	   					    				
    				},
    				error: function (data, status, e)
    				{
    					alert(e);
    				}    				
    			});
	        }
		});
		
    </script>
</head>

<body>

<div class="callapp_head" style="padding: 13px 5px 0 5px;">სიახლეები<span class="callapp_refresh"><img alt="refresh" src="media/images/icons/refresh.png" height="14" width="14">   განახლება</span><hr class="callapp_head_hr"></div>
	<table id="table_right_menu" style="top: 116px; left: -19px;">
		<tr>
			<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;background:#2681DC;" id="show_table" myvar="0">
				<img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
			</td>
			<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_log" myvar="0">
				<img alt="log" src="media/images/icons/log.png" height="14" width="14">
			</td>
			<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_copy_prit_exel" myvar="0">
				<img alt="link" src="media/images/icons/select.png" height="14" width="14">
			</td>
		</tr>
	</table>
	<div id="tabs" style="width: 99%;">
		<ul>
			<li><a href="#tab-0">მიმდინარე</a></li>
			<li><a href="#tab-1">არქივი</a></li>
		</ul>			
		<div id="tab-0">
			<div id="dt_example" class="ex_highlight_row">
				<div id="button_area" style="margin-top: 15px;">
					<button id="add_button">დამატება</button>
					<?php
						if($_SESSION["USERID"] == 1) {
							echo '<button id="delete_button">წაშლა</button>';
						}
					?>	
				</div>
				<table class="display" id="example11_1" style="width: 100%;">
					<thead>
						<tr id="datatable_header">
						<th>ID</th>
							<th style="width:10%; word-break:break-all;">დასაწყისი</th>
							<th style="width:10%; word-break:break-all;">დასასრული</th>
							<th style="width:14%; word-break:break-all;">დასახელება</th>
							<th style="width:14%; word-break:break-all;">საიტი</th>
							<th style="width:29%; word-break:break-all;">შინაარსი</th>
							<th style="width:12%; word-break:break-all;">ავტორი</th>
							<th style="width:8%;">სტატუსი</th>
							<th style="width:3%; word-break:break-all;">#</th>
						</tr>
					</thead>
					<thead>
						<tr class="search_header">
							<th class="colum_hidden">
								<input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 10px"/>
							</th>
							<th>
								<input style="width:20px;" type="text" name="search_overhead" value="" class="search_init" />
							</th>
							<th>
								<input style="width:97%;" type="text" name="search_partner" value="ფილტრი" class="search_init" />
							</th>
							<th>
								<input style="width:97%;" type="text" name="search_partner" value="ფილტრი" class="search_init" />
							</th>
							<th>
								<input style="width:97%;" type="text" name="search_sum_cost" value="ფილტრი" class="search_init" />
							</th>
							<th>
								<input style="width:97%;" type="text" name="search_partner" value="ფილტრი" class="search_init" />
							</th>
							<th>
								<input style="width:97%;" type="text" name="search_op_date" value="ფილტრი" class="search_init" />
							</th>
							<th>
								<input style="width:97%;" type="text" name="search_op_date" value="ფილტრი" class="search_init" />
							</th>
							<th>
								<div class="callapp_checkbox">
									<input type="checkbox" id="check-all_action" name="check-all_action" />
									<label for="check-all_action"></label>
								</div>
							</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
		<div id="tab-1">
			<div id="dt_example" class="ex_highlight_row">
				<div id="button_area" style="margin-top: 15px;"></div>
				<table class="display" id="example11_11" style="width: 100%;">
					<thead>
						<tr id="datatable_header">
							<th>ID</th>
							<th style="width:10%; word-break:break-all;">დასაწყისი</th>
							<th style="width:10%; word-break:break-all;">დასასრული</th>
							<th style="width:14%; word-break:break-all;">დასახელება</th>
							<th style="width:14%; word-break:break-all;">საიტი</th>
							<th style="width:29%; word-break:break-all;">შინაარსი</th>
							<th style="width:12%; word-break:break-all;">ავტორი</th>
							<th style="width:8%;">სტატუსი</th>
							<th style="width:3%; word-break:break-all;">#</th>
						</tr>
					</thead>
					<thead>
						<tr class="search_header">
							<th class="colum_hidden">
								<input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 10px"/>
							</th>
							<th>
								<input style="width:20px;" type="text" name="search_overhead" value="" class="search_init" />
							</th>
							<th>
								<input style="width:97%;" type="text" name="search_partner" value="ფილტრი" class="search_init" />
							</th>
							<th>
								<input style="width:97%;" type="text" name="search_partner" value="ფილტრი" class="search_init" />
							</th>
							<th>
								<input style="width:97%;" type="text" name="search_sum_cost" value="ფილტრი" class="search_init" />
							</th>
							<th>
								<input style="width:97%;" type="text" name="search_partner" value="ფილტრი" class="search_init" />
							</th>
							<th>
								<input style="width:97%;" type="text" name="search_op_date" value="ფილტრი" class="search_init" />
							</th>
							<th>
								<input style="width:97%;" type="text" name="search_op_date" value="ფილტრი" class="search_init" />
							</th>
							<th>
								<div class="callapp_checkbox">
									<input type="checkbox" id="check-all_arq" name="check-all_arq" />
									<label for="check-all_arq"></label>
								</div>
							</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<!-- jQuery Dialog -->
<div id="add-edit-form" class="form-dialog" title="სიახლეები">
<!-- aJax -->
</div>

<!-- jQuery Dialog -->
<div id="add-edit-form1" class="form-dialog" title="გამავალი ზარი">
<!-- aJax -->
</div>
<!-- jQuery Dialog -->
<div id="add-edit-form3" class="form-dialog" title="განყოფილება">
<!-- aJax -->
</div>

<!-- jQuery Dialog -->
<div id="add-edit-form2" class="form-dialog" title="აქციების პროდუქტები">
<!-- aJax -->
</div>

<div id="add-responsible-person" class="form-dialog" title="პასუხისმგებელი პირი">
<!-- aJax -->
</div>
<a id="downloader" href="" style="display:none" target="_blank"></a>
</body>

