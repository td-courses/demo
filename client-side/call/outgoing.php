<head>
    <link href="media/fontawesome-free-5.6.3-web/css/all.css" rel="stylesheet">
    <script type="text/javascript" src="js/MyWebSocket.js"></script>
    <script type="text/javascript" src="js/jquery.timeago.js"></script>
    <style type="text/css">
        .callapp_tabs {
            margin-top: 5px;
            margin-bottom: 5px;
            float: right;
            width: 100%;

        }

        .callapp_tabs span {
            color: #FFF;
            border-radius: 5px;
            padding: 5px;
            float: left;
            margin: 0 3px 0 3px;
            background: #2681DC;
            font-weight: bold;
            font-size: 11px;
            margin-bottom: 2px;
        }

        .callapp_tabs span close {
            cursor: pointer;
            margin-left: 5px;
        }

        .callapp_head {
            font-family: pvn;
            font-weight: bold;
            font-size: 20px;
            color: #2681DC;
        }

        .callapp_head select {
            position: relative;
            top: -2px;
        }

        .callapp_head_hr {
            border: 1px solid #2681DC;
        }

        .callapp_refresh {
            padding: 5px;
            border-radius: 8px;
            color: #FFF;
            background: #9AAF24;
            float: right;
            font-size: 13px;
            cursor: pointer;
        }

        .comunication_button {
            padding: 5px;
            border-radius: 8px;
            float: right;
            font-size: 13px;
            cursor: pointer;
        }

        .callapp_dnd_button {
            width: 40px;
            height: 40px;
            background: none;
            border: none;
            cursor: pointer;
            margin: 0 5px;
            position: relative;
            color: #7f8c8d;
            font-size: 21px;
        }

        .callapp_filter_show {
            margin-bottom: 50px;
            float: right;
            width: 100%;
        }

        .callapp_filter_show button {
            margin-bottom: 10px;
            border: none;
            color: #2681DC;
            font-weight: bold;
            cursor: pointer;
        }

        .callapp_filter_body {
            width: 1030px;
            height: 83px;
            padding: 5px;
            margin-bottom: 0px;
        }

        .callapp_filter_body span {
            float: left;
            margin-right: 10px;
            height: 22px;
        }

        .callapp_filter_body span label {
            color: #555;
            font-weight: bold;
            margin-left: 20px;
        }

        .callapp_filter_body_span_input {
            position: relative;
            top: -15px;
        }

        .search_button {
            padding: 2px;
            border-radius: 0px;
            background: #009688;
            cursor: pointer;
        	float: left;
        }
        .callapp_filter_header {
            color: #2681DC;
            font-family: pvn;
            font-weight: bold;
        }

        .ColVis, .dataTable_buttons {
            z-index: 50;
        }

        #flesh_panel {
            height: auto;
            width: 406px;
            position: absolute;
            top: 0;
            padding: 15px;
            right: 2px;
            z-index: 49;
            background: #FFF;
        }

        #table_index_wrapper #table_index_filter {
            width: 56%;

        }

        .callapp_filter_show button {
            margin-bottom: 0;
        }

        #table_index tbody td:last-child {
            padding: 0;
        }

        #table_index_wrapper .ui-widget-header {
            height: 55px;
        }

        /* -SMS DIALOG STYLES- */
        .new-sms-row {
            width: 100%;
            height: auto;
            margin-top: 11.3px;
            box-sizing: border-box;
        }

        .new-sms-row:last-child {
            margin-top: 4px;
        }

        .new-sms-row.grid {
            display: grid;
            grid-template-columns: repeat(2, 50%);
        }

        .nsrg-col {
            padding-right: 10px;
        }

        .nsrg-col:last-child {
            padding-right: 0;
        }

        .new-sms-input-holder {
            width: 100%;
            min-height: 27px;
            display: flex;
        }

        .new-sms-input-holder input {
            height: 18px;
            flex-grow: 1;
            font-size: 11.3px;
        }

        .new-sms-input-holder textarea {
            height: 75%;
            min-height: 20px;
            flex-grow: 1;
            font-size: 11.3px;
            resize: vertical;
        }

        #smsCopyNum { /*button: copy*/
            margin: 0 5px;
        }

        #smsTemplate { /*button: open template*/
            margin-right: 0;
        }

        #smsNewNum i { /*icon: add new number in send new sms child*/
            font-size: 14px;
        }

        #smsCharCounter { /*input: sms character counter*/
            width: 55px;
            height: 18px;
        }

        #smsCharCounter { /*input: send new sms character counter*/
            position: relative;
            top: 0;
            text-align: center;
        }

        #sendNewSms { /*button: send new message action inplement*/
            float: right;
        }

        .empty-sms-shablon {
            width: 100%;
            height: auto;
            padding: 10px 0;
            text-align: center;
            font-family: pvn;
            font-weight: bold;
        }

        #box-table-b1 {
            font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
            font-size: 12px;
            text-align: center;
            border-collapse: collapse;
            border-top: 7px solid #70A9D2;
            border-bottom: 7px solid #70A9D2;
            width: 300px;
        }

        #box-table-b th {
            font-size: 13px;
            font-weight: normal;
            padding: 8px;
            background: #E8F3FC;;
            border-right: 1px solid #9baff1;
            border-left: 1px solid #9baff1;
            color: #4496D5;
        }

        #box-table-b1 th {
            font-size: 13px;
            font-weight: normal;
            padding: 8px;
            background: #E8F3FC;;
            border-right: 1px solid #9baff1;
            border-left: 1px solid #9baff1;
            color: #4496D5;
        }

        #box-table-b td {
            padding: 8px;
            background: #e8edff;
            border-right: 1px solid #aabcfe;
            border-left: 1px solid #aabcfe;
            color: #669;
        }

        #box-table-b1 td {
            padding: 8px;
            background: #e8edff;
            border-right: 1px solid #aabcfe;
            border-left: 1px solid #aabcfe;
            color: #669;
        }

        .download_shablon {
            height: 20px;
            background-color: #4997ab;
            border-radius: 2px;
            display: inline-block;
            cursor: pointer;
            color: #ffffff;
            font-family: arial;
            font-size: 14px;
            border: 0px;
            text-decoration: none;
            text-overflow: ellipsis;
        }

        #comucation_status {
            position: absolute;
            z-index: 25000000;
            background: #fff;
            width: 199px;
            display: block;
            box-shadow: 0px 0px 25px #888888;
            left: -20%;
        }

        .download_shablon:hover {
            background-color: #ffffff;
            color: #4997ab;
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .2);
        }

        .ui-dialog-titlebar-close {
            display: none;
        }

        .imagespan {
            padding: 0px 0px 0px 10px;
        }

        .comunicationTr {
            border-top: 0.5px #c1cbd0 solid;
        }

        .comunication_name {
            font-family: pvn;
            font-weight: bold;
            font-size: 13px;
            color: #2681DC;
        }

    </style>

    <style type="text/css">

        .download {

            background: linear-gradient(to bottom, #599bb3 5%, #408c99 100%);
            border-radius: 0px;
            display: inline-block;
            cursor: pointer;
            color: #ffffff !important;
            font-size: 12px;
            border: 0px !important;
            text-decoration: none;
            text-overflow: ellipsis;
            width: 100%;
            font-weight: normal !important;
        }

        .download1 {

            background-color: #ce8a14;
            border-radius: 0px;
            display: inline-block;
            cursor: pointer;
            color: #ffffff !important;
            font-size: 14px;
            border: 0px !important;
            text-decoration: none;
            text-overflow: ellipsis;
            width: 100%;
            font-weight: normal !important;
        }


        .myButton:hover {
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #408c99), color-stop(1, #599bb3));
            background: -moz-linear-gradient(top, #408c99 5%, #599bb3 100%);
            background: -webkit-linear-gradient(top, #408c99 5%, #599bb3 100%);
            background: -o-linear-gradient(top, #408c99 5%, #599bb3 100%);
            background: -ms-linear-gradient(top, #408c99 5%, #599bb3 100%);
            background: linear-gradient(to bottom, #408c99 5%, #599bb3 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#408c99', endColorstr='#599bb3', GradientType=0);
            background-color: #408c99;
        }

        .myButton:active {
            position: relative;
            top: 1px;
        }

        .hidden {
            display: none;
        }

        .reqves_hist td {
            padding: 0 13px;
            border: inset 1px #B4D2E2;
            text-align: left;
        }

        .reqves_hist td:nth-child(4) {
            padding: 0 13px;
            border-right: inset 1px rgba(0, 0, 0, 0);
            text-align: left;
        }

        .reqves_hist td {
            padding: 0 13px;
            border: inset 1px #B4D2E2;
            text-align: left;
        }

        .reqves_hist td:nth-child(4) {
            padding: 0 13px;
            border-right: inset 1px rgba(0, 0, 0, 0);
            text-align: left;
        }

        .add-edit-form-class .ui-dialog-buttonset {
            width:100%;
        }

        #save-dialog{
            float: right;
        }

        .high {
            height: 15px;
        }

        .callapp_head {
            font-family: pvn;
            font-weight: bold;
            font-size: 20px;
            color: #2681DC;
        }

        .callapp_head_hr {
            border: 1px solid #2681DC;
        }

        .callapp_refresh {
            padding: 5px;
            border-radius: 3px;
            color: #FFF;
            background: #9AAF24;
            float: right;
            font-size: 13px;
            cursor: pointer;
        }

        .ui-widget-content {
            border: 0px solid #2681dc;
        }

        #example0 td:nth-child(12), #example1 td:nth-child(13), #example2 td:nth-child(13) {
            padding: 0px;
        }


    </style>
    <script type="text/javascript">
    	var wsdl_url     = "includes/search_client_service.php";
    	var aJaxURL_answer  = "server-side/call/chat_answer.action.php";
    	var aJaxURL_task = "server-side/call/outgoing/outgoing_task.action.php";
        var aJaxURL_in   = "server-side/call/incomming.action.php";
        var aJaxURL_file = "server-side/call/auto_dialer_action.php";
        var aJaxURL      = "server-side/call/outgoing/outgoing_tab0.action.php";		//server side folder url
        var aJaxURL1     = "server-side/call/outgoing/outgoing_tab2.action.php";		//server side folder url
        var aJaxURL2     = "server-side/call/outgoing/outgoing_tab3.action.php";		//server side folder url
        var upJaxURL     = "server-side/upload/file.action.php";
        var aJaxURL_tabs = "server-side/call/outgoing/order_tabs.action.php";
        var aJaxURL_sms  = "includes/sendsms.php";
        var aJaxURL_getmail  = "includes/send_mail.php";
        var aJaxURL_mail     = "server-side/call/Email_sender.action.php";
        var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
        var tName = "example0";
        var File_Table_Name = "example_all_file";//table name
        var tbName = "tabs";												//tabs name
        var fName = "add-edit-form";
        var M_fName = "add-edit-manual";
        var AD_fName = "add-edit-auto-dialer";
        var F_fName = "add_edit_file";
        var M_tName = "manual";
        var AD_tName = "auto_dialer";
        var file_name = '';
        var rand_file = '';
        var call_id_1 = '7';
        var call_id_30 = '36';
        var main_li_id_1 = '2';
        var main_li_id_30 = '31';

        

        function listen(file){
            $('#auau').each(function(){
                this.pause(); // Stop playing
                this.currentTime = 0; // Reset time
            });
            var url = 'http://pbx.my.ge/records/' + file;
            $("#auau source").attr('src',url);
            $("#auau").load();
        }

        $(document).on("click", "#sendNewSms", function () {
            
            const data = {
                incommingId:       $('#incomming_call_id').val(),
                hidde_outgoing_id: $('#hidde_outgoing_id').val(),
                smsText:           $("#newSmsText").val(),
            };

            data.phone = $("#smsAddresseeByPhone").val();
            

            // send ajax request
            $.post(aJaxURL_sms, data, result => {
                if (result.result == 2) {
                    alert("SMS წარმატებით გაიგზავნა");
                } else {
                    alert("SMS გაგზავნა ვერ განხორციელდა");
                }
                $("#add-edit-form-sms").dialog("close");
                GetDataTable("table_sms", aJaxURL, "get_list_sms", 5, "incomming_id="+$('#incomming_call_id').val()+"&hidde_outgoing_id="+$('#hidde_outgoing_id').val(), 0, "", 2, "desc", '', "<'F'lip>");
                $("#table_sms_length").css('top','0px');
            });
            $("#smsAddresseeByPhone").val("");
            $("#newSmsText").val("");

        });

        //new sms textarea key up
        $(document).on("keyup", "#newSmsText", function() {

            charCounter(this, "#smsCharCounter");

        });

        //new sms textarea focus
        $(document).on("keydown", "#newSmsText", function(e) {

            if(e.key !== "Backspace" && e.key !== "Delete") {
                textAreaFocusAllowed(this, "#smsCharCounter");
            }

        });
        //open template dialog
        $(document).on("click", "#smsTemplate", function() {

       	//request data
       	let data = {
       		act: "get_sms_template_dialog"
       	};

       	//buttons
       	let buttons = {
       		"cancel": {
       			text: "დახურვა",
       			id: "cancel-dialog",
       			click: function () {
       				$(this).dialog("close");
       				$(this).html("");
       			}
       		}
       	};

       	//ajax request
       	$.post(aJaxURL, data, structure => {

       		//open dialog malibu
       		
       		$("#sms-templates").html(structure.html);
       		
       		
       		GetDialog("sms-templates","530","auto", buttons, "center top");
       		GetDataTable("sms_template_table", aJaxURL, "phone_directory", 4, "", 0, "", 2, "desc", '', "<'F'lip>");
       		$("#sms_template_table_length").css('top','0px');
       	});

       });
        //textarea character counter
        function charCounter(checkobj, updateobj) {

            var textLength = $(checkobj).val().length;
            var charLimit = $(updateobj).data("limit");

            $(updateobj).val(`${textLength}/${charLimit}`);

            if(textLength === charLimit) {
                $(checkobj).blur();
            }

        }

        //text area focus allowed
        function textAreaFocusAllowed(checkobj, limitinput) {

            var textLength = $(checkobj).val().length;
            var charLimit = $(limitinput).data("limit");

            if(textLength >= charLimit) {
                $(checkobj).blur();
            }

        }

        // download sms shablon
        $(document).on("click", ".download_shablon", function () {

        	//define variables
        	var message = $(this).data("message");
        	var sms_id   = $(this).attr("sms_id");

        	//check length of message text
        	if(message != "" && message.length <= 150){

        		$('#newSmsText').val(message);
        		$('#smsCharCounter').val((message.length)+'/150');

        		// set choosed sms id
        		$D.choosedSmsId = sms_id;

        	}else if(message.length > 150) {
        		alert("შაბლონის ტექსტი შეიცავს დასაშვებზე (150) მეტ სიმბოლოს (" + message.length + ")");
        	}

        	//close sms template dialog
        	$("#sms-templates").dialog("close");
        	
        });

        $(document).on("click", "#add_mail", function () {
            param = new Object();
            param.act = "send_mail";
            param.call_type = 'inc';
            param.outgoing_id = $('#outgoing_id').val();
            $.ajax({
                url: aJaxURL_mail,
                data: param,
                success: function (data) {
                    $("#add-edit-form-mail").html(data.page);
                    $("#smsPhoneDir").button();
                    $("#email_shablob,#choose_button_mail,#send_email").button();

                }
            });
            var buttons = {
                "cancel": {
                    text: "დახურვა",
                    id: "cancel-dialog",
                    click: function () {
                        $(this).dialog("close");
                        $(this).html("");
                    }
                }
            };
            GetDialog("add-edit-form-mail", 640, "auto", buttons, 'center top');
            setTimeout(() => {
                $("#from_mail_id").chosen({search_contains:true});                
            }, 150);
            $("#smsPhoneDir").button();
        });

        $(document).on( "dialogopen","#add-edit-form-mail", function() {
            $("#email_shablob,#choose_button_mail,#send_email,#smsPhoneDir").button();
            $("#signature").chosen({search_contains:true});
            setTimeout(() => {
            //     tinymce.init({
            //         selector: '#input',
            //         width: "580px",
            //         height: "200px",
            //         cssclass: 'te',
            //         controlclass: 'tecontrol',
            //         dividerclass: 'tedivider',
            //         controls: ['bold', 'italic', 'underline', 'strikethrough', '|', 'subscript', 'superscript', '|',
            //             'orderedlist', 'unorderedlist', '|', 'outdent', 'indent', '|', 'leftalign',
            //             'centeralign', 'rightalign', 'blockjustify', '|', 'unformat', '|', 'undo', 'redo', 'n',
            //             'font', 'size', '|', 'image', 'hr', 'link', 'unlink', '|', 'print'],
            //         footer: true,
            //         fonts: ['Verdana', 'Arial', 'Georgia', 'Trebuchet MS'],
            //         xhtml: true,
            //         bodyid: 'editor',
            //         footerclass: 'tefooter',
            //         resize: {cssclass: 'resize'}
            //     });
            $("#input").cleditor();
            }, 100);
            
        });

        $(document).on('dialogclose',"#add-edit-form-mail", function(event) {
            tinymce.remove("#input");
        });

        $(document).on("click", "#email_shablob", function () {
            param = new Object();
            param.act = "send_mail_shablon";
            $.ajax({
                url: aJaxURL_mail,
                data: param,
                success: function (data) {
                    $("#add-edit-form-mail-shablon").html(data.page);
                    GetDataTable("example_shablon_table", aJaxURL_mail, "get_list_shablon", 5, "mail_id="+$("#from_mail_id").val(), 0, "", 2, "desc",'', "<'F'lip>"); 
                    $("#example_shablon_table_length").css('top','0px');
                }
            });
            var buttons = {
                "cancel": {
                    text: "დახურვა",
                    id: "cancel-dialog",
                    click: function () {
                        $(this).dialog("close");
                        $(this).html("");
                    }
                }
            };
            GetDialog("add-edit-form-mail-shablon", 1200, "auto", buttons, 'left top');
        });

        $(document).on("click", "#choose_button_mail", function () {
            $("#choose_mail_file").click();
        });

        $(document).on("change", "#choose_mail_file", function () {
            var file_url = $(this).val();
            var file_name = this.files[0].name;
            var file_size = this.files[0].size;
            var file_type = file_url.split('.').pop().toLowerCase();
            var path = "../../mailmyge/file_attachment/";

            if ($.inArray(file_type, ['pdf', 'pptx', 'png', 'xls', 'xlsx', 'jpg', 'docx', 'doc', 'csv']) == -1) {
                alert("დაშვებულია მხოლოდ 'pdf', 'png', 'xls', 'xlsx', 'jpg', 'docx', 'doc', 'csv' გაფართოება");
            } else if (file_size > '15728639') {
                alert("ფაილის ზომა 15MB-ზე მეტია");
            } else {
                $.ajaxFileUpload({
                    url: "server-side/upload/file.action.php",
                    secureuri: false,
                    fileElementId: "choose_mail_file",
                    dataType: 'json',
                    data: {
                        act: "upload_file_mail",
                        button_id: "choose_mail_file",
                        table_name: 'mail',
                        file_name: Math.ceil(Math.random() * 99999999999),
                        file_name_original: file_name,
                        file_type: file_type,
                        file_size: file_size,
                        path: path,
                        table_id: $("#hidden_increment").val(),

                    },
                    success: function (data) {
                        if (typeof(data.error) != 'undefined') {
                            if (data.error != '') {
                                alert(data.error);
                            } else {
                                var tbody = '';
                                $("#choose_mail_file").val('');
                                for (i = 0; i <= data.page.length; i++) {
                                    tbody += "<div id=\"first_div\">" + data.page[i].file_date + "</div>";
                                    tbody += "<div id=\"two_div\">" + data.page[i].name + "</div>";
                                    tbody += "<a id=\"tree_div\" download href=\'media/uploads/file/" + data.page[i].rand_name + "\'>ჩამოტვირთვა</a>";
                                    tbody += "<div id=\"for_div\" onclick=\"delete_file1('" + data.page[i].id + "')\">-</div>";
                                    tbody += "<input type='hidden' class=\"attachment_address\" value='"+ data.page[i].rand_name + "'>";
                                    $("#paste_files1").html(tbody);
                                }
                            }
                        }
                    }
                });
            }
        });

        function delete_file1(id) {
            $.ajax({
                url: "server-side/upload/file.action.php",
                data: "act=delete_file1&file_id=" + id + "&table_name=mail",
                success: function (data) {

                    var tbody = '';
                    if (data.page.length == 0) {
                        $("#paste_files1").html('');
                    }else{
                        for (i = 0; i <= data.page.length; i++) {
                            tbody += "<div id=\"first_div\">" + data.page[i].file_date + "</div>";
                            tbody += "<div id=\"two_div\">" + data.page[i].name + "</div>";
                            tbody += "<a id=\"tree_div\" download href=\'media/uploads/file/" + data.page[i].rand_name + "\'>ჩამოტვირთვა</a>";
                            tbody += "<div id=\"for_div\" onclick=\"delete_file1('" + data.page[i].id + "')\">-</div>";
                            tbody += "<input type='hidden' class=\"attachment_address\" value='"+ data.page[i].rand_name + "'>";
                            $("#paste_files1").html(tbody);
                        }
                    }
                }
            });
        }
        function pase_body(id, head) {
        	$('#mail_text').val(head);
            $("iframe").contents().find("body").html($('#' + id).html());
            $('#add-edit-form-mail-shablon').dialog('close');
        }

        $(document).on("click", "#send_email", function () {
            param = new Object();
            param.from_mail_id      = $("#from_mail_id").val();
            param.source_id         = $("#source_id").val();
            param.address           = $("#mail_address").val();
            param.cc_address        = $("#mail_address1").val();
            param.bcc_address       = $("#mail_address2").val();
        	param.subject           = $("#mail_text").val();
            param.send_mail_id      = $("#send_email_hidde").val();
            param.incomming_call_id = $("#incomming_call_id").val();
            param.hidde_outgoing_id = $('#hidde_outgoing_id').val();
            param.hidden_increment  = $("#hidden_increment").val();
            param.body              = $("iframe").contents().find("body").html();
            var attachments         = [];
            var counter             = 0;
            
            $(".attachment_address").each(function() {
                attachments[counter]=$(this).val();
                counter++;
            });
            
            param.attachments       = attachments.join(",");
            
            $.ajax({
                url: aJaxURL_getmail,
                data: param,
        		success: function (data) {
                    if (data.status == 2) {
                        alert('შეტყობინება წარმატებით გაიგზავნა!');
                        $("#mail_text").val('');
                        $("iframe").contents().find("body").html('');
                        $("#file_div_mail").html('');
                        $("#add-edit-form-mail").dialog("close");
                        $("#add-edit-form-mail").html('');
                        GetDataTable("table_mail", aJaxURL, "get_list_mail", 5, "incomming_id="+$('#incomming_call_id').val()+"&hidde_outgoing_id="+$('#hidde_outgoing_id').val(), 0, "", 0, "desc",'',"<'F'lip>");
                        setTimeout(function(){$('.ColVis, .dataTable_buttons').css('display','none');}, 160);
                        $("#table_mail_length").css('top','0px');
                    }else{
                        alert('შეტყობინება არ გაიგზავნა!');
                    }
                }
            });
        });
        
        function show_right_side1(id){
            $("#right_side fieldset").hide();
	        $("#" + id).show();
	        //$(".add-edit-form-class").css("width", "1260");
	        //$('#add-edit-form').dialog({ position: 'left top' });
	        hide_right_side1();

	        var str = $("."+id).children('img').attr('src');
			str = str.substring(0, str.length - 4);
			$("#side_menu span").children('img').css('border-bottom','');
	        $("."+id).children('img').css('filter','brightness(0.1)');
	        $("."+id).children('img').css('border-bottom','2px solid #333');
	    }

        function hide_right_side1(){

			$(".info").children('img').css('filter','brightness(1.1)');
			$(".task").children('img').css('filter','brightness(1.1)');
	        $(".record").children('img').css('filter','brightness(1.1)');
	        $(".file").children('img').css('filter','brightness(1.1)');
	        $(".chat_question").children('img').css('filter','brightness(1.1)');
	        $(".sms").children('img').css('filter','brightness(1.1)');
	        $(".client_mail").children('img').css('filter','brightness(1.1)');
	        $(".newsnews").children('img').css('filter','brightness(1.1)');
	        $(".call_resume").children('img').css('filter','brightness(1.1)');
	        $("#record fieldset").show();
	    }

        $(document).on("click", ".sms", function () {
	    	GetDataTable("table_sms", aJaxURL, "get_list_sms", 5, "&incomming_id="+$('#incomming_call_id').val()+"&hidde_outgoing_id="+$('#hidde_outgoing_id').val(), 0, "", 2, "desc",'',"<'F'lip>");
	    	GetButtons("add_sms_phone", "");
			$("#table_sms_length").css('top','0px');
			setTimeout(function () {$('.ColVis, .dataTable_buttons').css('display', 'none');}, 90);
		});

        $(document).on("click", ".client_mail", function(){
        	GetDataTable("table_mail", aJaxURL, "get_list_mail", 5, "incomming_id="+$('#incomming_call_id').val()+"&hidde_outgoing_id="+$('#hidde_outgoing_id').val(), 0, "", 0, "desc",'',"<'F'lip>");
            GetButtons("add_mail", "");
            setTimeout(function(){$('.ColVis, .dataTable_buttons').css('display','none');}, 160);
            $("#table_mail_length").css('top','0px');
        });

        $(document).on("click", ".new-sms-button", function () {

			// define request data
			const data = {
				act: "open_new_sms_dialog",
				type: $(this).data("type")
			};
            
            $.ajax({
                url: aJaxURL,
                data,
                success: function (data) {

					var buttons = {
						"cancel": {
							text: "დახურვა",
							id: "cancel-dialog",
							click: function () {
								$(this).dialog("close");
								$(this).html("");
							}
						}
					};

					$("#add-edit-form-sms").html(data.page);
                    $("[data-button='jquery-ui-button']").css({"font-size": "7px"}).button();

					GetDialog("add-edit-form-sms", 561, "auto", buttons, 'center top');

					// check new sms dialog type
					if(data.type === "phone") {

						// copy adressee phone number
						const phoneNum = $("#phone").val();

						// insert phone number into adressee receiver input
						$("#smsAddresseeByPhone").val(phoneNum);

					}

					// set send sms button type
					$("#sendNewSms").data("type", data.type);

                }
            });

        });
        
                
        $(document).on("click", ".new-sms-button", function () {

            // define request data
            const data = {
                act: "open_new_sms_dialog",
                type: $(this).data("type"),
                crm: 1,
                crm_base_id: $("#request_base_id").val()
            };

            $.ajax({
                url: aJaxURL_in,
                data,
                success: function (data) {

                    var buttons = {
                        "cancel": {
                            text: "დახურვა",
                            id: "cancel-dialog",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    };

                    $("#add-edit-form-sms").html(data.page);
                    $("[data-button='jquery-ui-button']").css({"font-size": "7px"}).button();

                    GetDialog("add-edit-form-sms", 561, "auto", buttons, 'center top');

                    // check new sms dialog type
                    if (data.type === "phone") {

                        // copy adressee phone number
                        // const phoneNum = $("#phone").val();

                        // insert phone number into adressee receiver input
                        // $("#smsAddresseeByPhone").val(phoneNum);

                    } else if (data.type === "ip") {

                        // copy pin code and personal number
                        const pinCode = $("#personal_pin").val();
                        const idNumber = $("#personal_id").val();

                        // insert pin code and personal number into adressee receiver inputs
                        $("#smsAddresseeByPIN").val(pinCode);
                        $("#smsAddresseeByIDNum").val(idNumber);

                    }

                    // set send sms button type
                    $("#sendNewSms").data("type", data.type);

                }
            });

        });


        function show_right_side(id) {
            $(".tabs").hide();
            $("#" + id).show();
            //$(".add-edit-form-class").css("width", "1260");
            //$('#add-edit-form').dialog({ position: 'left top' });
            hide_right_side();

            var str = $("." + id).children('img').attr('src');
            str = str.substring(0, str.length - 4);
            $("#side_menu span").children('img').css('border-bottom', '');
            $("." + id).children('img').css('filter', 'brightness(0.1)');
            $("." + id).children('img').css('border-bottom', '2px solid #333');
        }

        function hide_right_side() {

            $(".info").children('img').css('filter', 'brightness(1.1)');
            $(".task").children('img').css('filter', 'brightness(1.1)');
            $(".record").children('img').css('filter', 'brightness(1.1)');
            $(".file").children('img').css('filter', 'brightness(1.1)');
            $(".file").children('img').css('filter', 'brightness(1.1)');
            $(".sms").children('img').css('filter', 'brightness(1.1)');
            $(".sport").children('img').css('filter', 'brightness(1.1)');
            $(".crm").children('img').css('filter', 'brightness(1.1)');
            $(".monitoring").children('img').css('filter', 'brightness(1.1)');
            $("#record fieldset").show();
        }

        function open_tab(tab_id, count) {
            for (var i = 1; i <= tab_id; i++) {
                if (i == count) {
                    document.getElementById(`tab_${i}`).style.display = "block";
                    document.getElementById(`a_${i}`).style.background = "";
                } else {
                    document.getElementById(`tab_${i}`).style.display = "none";
                    document.getElementById(`a_${i}`).style.background = "#e6f2f8";
                }
            }
        }


        $(document).on("click", "#check-all-out2", function () {

            $("#example2" + " INPUT[type='checkbox']").prop("checked", $("#check-all-out2").is(":checked"));
        });


        function loadtabs(activep = "", activech = "") {

            obj = new Object;
            obj.act = "get_tabs";
            obj.c = 1;
            obj.t_c = 1;


            $.ajax({
                url: aJaxURL_tabs,
                data: obj,
                success: function (data) {
                    $("#tabs1").html(data.page);
                    obj.c = 20;
                    obj.t_c = 20;
                    $("#ul_tab1").append(" <li class=\"main_li_1\" tree_id=\"-1\" id=\"1111\" onclick=\" call_table1111(this.id)\" style=\"color: red; border-bottom-width: 2px; border-bottom-color: red;\"><a style=\"color: red;text-decoration: none\" href=\"#tab-2\">ვადაგასული</a></li>");


                    $.ajax({
                        url: aJaxURL_tabs,
                        data: obj,
                        success: function (data) {
                            $("#tabs2").html(data.page);
                            obj.c = 30;
                            obj.t_c = 30;
                            $.ajax({
                                url: aJaxURL_tabs,
                                data: obj,
                                success: function (data) {
                                    $("#tabs3").html(data.page);
                                    $("#ul_tab30").append(" <li class=\"main_li_1\" tree_id=\"-1\" id=\"1111\" onclick=\" call_table30(this.id)\" style=\"color: red; border-bottom-width: 2px; border-bottom-color: red;\"><a style=\"color: red;text-decoration: none\" href=\"#tab-2\">ვადაგასული</a></li>");
								}
                            })


                        }
                    })

                }
            })


        }

        $(document).ready(function () {
			$("#add_responsible_person").button();
			SetPrivateEvents("add_responsible_person", "check-all", "add-responsible-person", "example0");
            GetTabs(tbName);
            GetTable0();
            
            GetButtons("add_button", '', "");
            loadtabs();
			$( "#"+tbName ).tabs({ active: 0 });
        });

        $(document).on("tabsactivate", "#tabs", function () {
            var tab = GetSelectedTab(tbName);
            if (tab == 0) {
                GetTable0();
            }else if (tab == 1) {
            	GetTable4_0();
            }else if (tab == 2) {
            	GetTable5_0();
            }else if (tab == 3) {
                GetTableManual(3, 1);
            }else if (tab == 4){
                GetTableAutoDialer(3);
            }else if (tab == 5){
            	GetTable6_0();
            }
        });

        $(document).on("click", "#control_tab_id_0", function () {
        	GetTable0();  
        });

		$(document).on("click", "#control_tab_id_1", function () {
			GetTable1();
        });

        $(document).on("click", "#control_tab_id_2", function () {
        	GetTable2()
        });
        
        function GetTableAutoDialer(tree_id) {
            LoadAutoDialer(tree_id);
            SetEvents("", "", "check-all-auto-dialer", AD_tName, AD_fName, aJaxURL, "&auto_dialer_id=1");

        }

        function GetTableManual(tree_id, check) {
			LoadManual(tree_id);
			SetEvents("", "", "check-all-manual", M_tName, M_fName, aJaxURL, "&manual_id=1&check=" + check);
		}

        function GetTable0() {
            LoadTable0();
            SetEvents("add_button", "", "", "example0", fName, aJaxURL);
        }

        function GetTable1() {
            LoadTable1();
            //GetButtons("", "", "");
            //$('.add_responsible').button();
            SetEvents("", "", "", "example1", fName, aJaxURL);
        }

        function GetTable2() {
            LoadTable2();
            SetEvents("", "", "", "example2", fName, aJaxURL);
            GetDateTimes("search_start_my");
            GetDateTimes("search_end_my");
            $("#fillter").button();

        }

        
        
        function LoadManual(tree_id) {
			GetDataTable(M_tName, aJaxURL_tabs, "get_list_manual", 6, "tree_id=" + tree_id, 0, "", 1, "desc", "", change_colum_main);
            setTimeout(function () {
                $('.ColVis, .dataTable_buttons').css('display', 'none');
            }, 90);
		}

        function LoadAutoDialer(tree_id) {


            GetDataTable(AD_tName, aJaxURL_tabs, "get_list_auto_dialer", 6, "tree_id=" + tree_id, 0, "", 1, "desc", "", change_colum_main);
            setTimeout(function () {
                $('.ColVis, .dataTable_buttons').css('display', 'none');
            }, 90);


        }


        function LoadTable0() {
        	GetDataTable_outgoing("example0", aJaxURL, "get_list", 10, "", 0, "", 1, "desc", "", change_colum_main);
        	$("#add_responsible_person").button();
            setTimeout(function () {
                $('.ColVis, .dataTable_buttons').css('display', 'none');
                SetPrivateEvents("add_responsible_person", "check-all", "add-responsible-person", "example0");
            }, 90);
        }

        function LoadTable1() {
        	GetDataTable_outgoing("example1", aJaxURL1, "get_list", 13, "", 0, "", 1, "desc", "", change_colum_main);
            setTimeout(function () {
                $('.ColVis, .dataTable_buttons').css('display', 'none');
            }, 90);
        }

        function LoadTable2() {
            var start = $("#search_start_my").val();
            var end   = $("#search_end_my").val();

            GetDataTable_outgoing("example2", aJaxURL2, "get_list", 15, "start=" + start + "&end=" + end, 0, "", 1, "desc", "", change_colum_main);
            setTimeout(function () {
                $('.ColVis, .dataTable_buttons').css('display', 'none');
            }, 90);
        }

        //ჩემი ჩამატებული ტაბები
        $(document).on("click", "#control_tab_id_4_0", function () {
        	GetTable4_0();  
        });

		$(document).on("click", "#control_tab_id_4_1", function () {
			GetTable4_1();
        });

        $(document).on("click", "#control_tab_id_4_2", function () {
        	GetTable4_2()
        });
        
        function GetTable4_0() {
            LoadTable4_0();
            SetEvents("", "", "", "example4_0", fName, aJaxURL);
        }

        function GetTable4_1() {
            LoadTable4_1();
            SetEvents("", "", "", "example4_1", fName, aJaxURL);
        }

        function GetTable4_2() {
            LoadTable4_2();
            SetEvents("", "", "", "example4_2", fName, aJaxURL);
            GetDateTimes("search_start_my_4");
            GetDateTimes("search_end_my_4");
            $("#fillter4").button();

        }
        
        function LoadTable4_0() {
        	GetDataTable_outgoing("example4_0", aJaxURL, "get_list4_0", 10, "", 0, "", 1, "desc", "", change_colum_main);
        	$("#add_responsible_person_nonwork").button();
            setTimeout(function () {
                $('.ColVis, .dataTable_buttons').css('display', 'none');
                SetPrivateEvents("add_responsible_person_nonwork", "check-all-nonwork", "add-responsible-person", "example4_0");
            }, 90);
            
        }

        function LoadTable4_1() {
        	GetDataTable_outgoing("example4_1", aJaxURL, "get_list4_1", 11, "", 0, "", 1, "desc", "", change_colum_main);
            setTimeout(function () {
                $('.ColVis, .dataTable_buttons').css('display', 'none');
            }, 90);
        }

        function LoadTable4_2() {
            var start = $("#search_start_my_4").val();
            var end   = $("#search_end_my_4").val();

            GetDataTable_outgoing("example4_2", aJaxURL, "get_list4_2", 15, "start=" + start + "&end=" + end, 0, "", 1, "desc", "", change_colum_main);
            setTimeout(function () {
                $('.ColVis, .dataTable_buttons').css('display', 'none');
            }, 90);
        }


        $(document).on("click", "#control_tab_id_5_0", function () {
        	GetTable5_0();  
        });

		$(document).on("click", "#control_tab_id_5_1", function () {
			GetTable5_1();
        });

        $(document).on("click", "#control_tab_id_5_2", function () {
        	GetTable5_2()
        });

        function GetTable5_0() {
            LoadTable5_0();
            SetEvents("", "", "", "example5_0", fName, aJaxURL);
        }

        function GetTable5_1() {
            LoadTable5_1();
            SetEvents("", "", "", "example5_1", fName, aJaxURL);
        }

        function GetTable5_2() {
            LoadTable5_2();
            SetEvents("", "", "", "example5_2", fName, aJaxURL);
            GetDateTimes("search_start_my_5");
            GetDateTimes("search_end_my_5");
            $("#fillter5").button();

        }
        
        function LoadTable5_0() {
        	GetDataTable_outgoing("example5_0", aJaxURL, "get_list5_0", 10, "", 0, "", 1, "desc", "", change_colum_main);
        	$("#add_responsible_person_active").button();
            setTimeout(function () {
                $('.ColVis, .dataTable_buttons').css('display', 'none');
                SetPrivateEvents("add_responsible_person_active", "check-all-active", "add-responsible-person", "example5_0");
            }, 90);
        }

        function LoadTable5_1() {
        	GetDataTable_outgoing("example5_1", aJaxURL, "get_list5_1", 11, "", 0, "", 1, "desc", "", change_colum_main);
            setTimeout(function () {
                $('.ColVis, .dataTable_buttons').css('display', 'none');
            }, 90);
        }

        function LoadTable5_2() {
            var start = $("#search_start_my_5").val();
            var end   = $("#search_end_my_5").val();

            GetDataTable_outgoing("example5_2", aJaxURL, "get_list5_2", 15, "start=" + start + "&end=" + end, 0, "", 1, "desc", "", change_colum_main);
            setTimeout(function () {
                $('.ColVis, .dataTable_buttons').css('display', 'none');
            }, 90);
        }

        $(document).on("click", "#control_tab_id_6_0", function () {
        	GetTable6_0();  
        });

		$(document).on("click", "#control_tab_id_6_1", function () {
			GetTable6_1();
        });

        $(document).on("click", "#control_tab_id_6_2", function () {
        	GetTable6_2()
        });

        function GetTable6_0() {
            LoadTable6_0();
            SetEvents("", "", "", "example6_0", fName, aJaxURL);
        }

        function GetTable6_1() {
            LoadTable6_1();
            SetEvents("", "", "", "example6_1", fName, aJaxURL);
        }

        function GetTable6_2() {
            LoadTable6_2();
            SetEvents("", "", "", "example6_2", fName, aJaxURL);
            GetDateTimes("search_start_my_6");
            GetDateTimes("search_end_my_6");
            $("#fillter6").button();

        }
        
        function LoadTable6_0() {
        	GetDataTable_outgoing("example6_0", aJaxURL, "get_list6_0", 10, "", 0, "", 1, "desc", "", change_colum_main);
        	$("#add_responsible_person_lid").button();
			setTimeout(function () {
                $('.ColVis, .dataTable_buttons').css('display', 'none');
                SetPrivateEvents("add_responsible_person_lid", "check-all-lid", "add-responsible-person", "example6_0");
            }, 90);
        }

        function LoadTable6_1() {
        	GetDataTable_outgoing("example6_1", aJaxURL, "get_list6_1", 11, "", 0, "", 1, "desc", "", change_colum_main);
            setTimeout(function () {
                $('.ColVis, .dataTable_buttons').css('display', 'none');
            }, 90);
        }

        function LoadTable6_2() {
            var start = $("#search_start_my_6").val();
            var end   = $("#search_end_my_6").val();

            GetDataTable_outgoing("example6_2", aJaxURL, "get_list6_2", 15, "start=" + start + "&end=" + end, 0, "", 1, "desc", "", change_colum_main);
            setTimeout(function () {
                $('.ColVis, .dataTable_buttons').css('display', 'none');
            }, 90);
        }
        
        //---------------------------------------//


        
        function Load_Table_all_file() {

            /* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
            GetDataTable(File_Table_Name, aJaxURL_file, "get_list_all_file", 3, "request_base_id=" + $("#request_base_id").val(), 0, "", 1, "desc", "", '');

            setTimeout(function () {
                $('.ColVis, .dataTable_buttons').css('display', 'none');
            }, 90);
            SetEvents("", "", "check-all-file", File_Table_Name, F_fName, aJaxURL_file, "file_id=" + $("#request_base_id").val() + "&tb_name= autocall_request_file");


        }


        function LoadDialog(fName) {

            switch (fName) {
                case "add_edit_file":

                    var button_modal = {

                        "cancel": {
                            text: "დახურვა",
                            id: "cancel-dialog",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }

                    }
                    GetDialog(F_fName, "auto", "auto", button_modal, "");
                    break;
                case "add-edit-auto-dialer":

                    var buttons = {
                        "save": {
                            text: "შენახვა",
                            id: "save-auto-dialer"
                        },
                        "cancel": {
                            text: "დახურვა",
                            id: "cancel-dialog",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    };
                    GetDialog("add-edit-auto-dialer", 650, "auto", buttons);

                    break;
                case "add-edit-manual":
                    var buttons = {
                        "save": {
                            text: "შენახვა",
                            id: "save-manual"
                        },
                        "cancel": {
                            text: "დახურვა",
                            id: "cancel-dialog",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    };
                    GetDialog("add-edit-manual", 645, "auto", buttons);



                    Load_Table_all_file();
                    GetDataTable("table_sms", aJaxURL, "get_list_sms", 5, "&incomming_id="+$('#incomming_call_id').val()+"&hidde_outgoing_id="+$('#hidde_outgoing_id').val(), 0, "", 2, "desc", '', "<'F'lip>");

                    break;
                case "add-edit-form":
                    var buttons = {
                        "save": {
                            text: "შენახვა",
                            id: "save-dialog"
                        },
                        "cancel": {
                            text: "დახურვა",
                            id: "cancel-dialog",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    };
                    GetDialog("add-edit-form", 641, "auto", buttons, 'center top');
                    $("#add-edit-form, .add-edit-form-class").css('overflow','visible');
                    GetDateTimes("out_start_date");
                    GetDateTimes("out_end_date");
                    $("#choose_button").button();
                    $("#choose_button3").button();
                    $("#choose_button2").button();
                    $('#add_comment').button();
                    $("#out_status_id, #task_branch_id, #out_recepient_id, #incomming_cat_1, #incomming_cat_1_1, #incomming_cat_1_1_1, #my_site, #source_id, #inc_status, #s_u_status, #client_sex").chosen({ search_contains: true });
                    $(".dialog-form-table tr").css('height', '10px');
                    break;
                case "add-edit-form-answer":
                        var buttons = {
                            "save": {
                                text: "შენახვა",
                                id: "save-dialog-answer",

                            },
                            "cancel": {
                                text: "დახურვა",
                                id: "cancel-dialog-answer", 
                                click: function () {
                                     $(this).dialog("close");
                                }  
                            }
                        };
                        GetDialog(fName, 358, "auto", buttons);
                        break; 
                case "add-edit-form1":
                    var buttons = {
                        "done_unansver": {
                            text: "უპასუხოს დასრულება",
                            id: "done-unansver"
                        },
                        "done": {
                            text: "დასრულება",
                            id: "done-dialog2"
                        },
                        "save": {
                            text: "შენახვა",
                            id: "save-dialog2"
                        },
                        "cancel": {
                            text: "დახურვა",
                            id: "cancel-dialog",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    };
                    GetDialog("add-edit-form1", 1087, "auto", buttons);
                    $("#choose_button1").button();
                    $(".dialog-form-table tr").css('height', '10px');

                    break;
            }

            var id = $("#incomming_id").val();
            var cat_id = $("#category_parent_id").val();

            if (id != '' && cat_id == 407) {
                $("#additional").removeClass('hidden');
            }
        }


        $(document).on("click", "#show_copy_prit_exel", function () {
            if ($(this).attr('myvar') == 0) {
                $('.ColVis,.dataTable_buttons').css('display', 'block');
                $(this).css('background', '#2681DC');
                $(this).children('img').attr('src', 'media/images/icons/select_w.png');
                $(this).attr('myvar', '1');
            } else {
                $('.ColVis,.dataTable_buttons').css('display', 'none');
                $(this).css('background', '#E6F2F8');
                $(this).children('img').attr('src', 'media/images/icons/select.png');
                $(this).attr('myvar', '0');
            }
        });
        $(document).on("click", ".download1", function () {
            var str = 1;
            var link = ($(this).attr("str"));
            link = 'http://91.233.15.136:8181/' + link;
            var btn = {
                "cancel": {
                    text: "დახურვა",
                    id: "cancel-dialog",
                    click: function () {
                        $(this).dialog("close");
                    }
                }
            };
            GetDialog_audio("audio_dialog", "auto", "auto", btn);
            $("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="' + link + '" type="audio/wav"> Your browser does not support the audio element.</audio>');
            $(".download1").css("background", "#F99B03");
            $(this).css("background", "#33dd33");

        });
        $(document).on("click", ".download", function () {
            var str = 1;
            var link = ($(this).attr("str"));
            link = 'http://91.233.15.136:8181/' + link;
            var btn = {
                "cancel": {
                    text: "დახურვა",
                    id: "cancel-dialog",
                    click: function () {
                        $(this).dialog("close");
                    }
                }
            };
            GetDialog_audio("audio_dialog", "auto", "auto", btn);
            $("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="' + link + '" type="audio/wav"> Your browser does not support the audio element.</audio>');
            $(".download").css("background", "#408c99");
            $(this).css("background", "#FF5555");

        });

        function LoadDialog1(tname) {

            var buttons = {
                "save": {
                    text: "შენახვა",
                    id: "save-printer",
                    click: function () {
                        Change_person(tname);
                    }
                },
                "cancel": {
                    text: "დახურვა",
                    id: "cancel-dialog",
                    click: function () {
                        $(this).dialog("close");
                    }
                }
            };
            GetDialog("add-responsible-person", 293, "auto", buttons, "center top");
            $("#add-responsible-person, .add-responsible-person-class").css('overflow','visible');
            $("#responsible_person").chosen({ search_contains: true });
        }

        $(document).on("click", ".task", function(){
            GetDataTable('task_table', aJaxURL_task, 'get_list', 6, 'incomming_call_id='+$("#hidde_outgoing_id").val(), 0, "", 2, "desc", '', "<'F'lip>");
            GetButtons("add_task_button", "");
            //SetEvents("add_task_button", "", "", 'task_table', 'add-edit-form-task', aJaxURL_task);
            
            setTimeout(function(){$('.ColVis, .dataTable_buttons').css('display','none');}, 160);
            $("#task_table_length").css('top','0px');
        });

        $(document).on("click","#add_task_button",  () => {
            $.ajax({
                url: aJaxURL_task,
                type: "POST",
                data: "act=get_add_page",
                dataType: "json",
                success: function (data) {
                    if (typeof (data.error) != "undefined") {
                        if (data.error != "") {
                            alert(data.error);
                        } else {
                                $("#add-edit-form-task").html(data.page);
                                var buttons = {
                                        "save": {
                                            text: "შენახვა",
                                            id: "save-dialog-task"
                                        },
                                        "cancel": {
                                            text: "დახურვა",
                                            id: "cancel-dialog-task", 
                                            click: function () {
                                                            $(this).dialog( "close" ); 
                                                        }
                                        }
                                    };
                                    GetDialog("add-edit-form-task", 800, "auto", buttons, "center top");
                                    GetDateTimes("task_end_date");
                                    GetDateTimes("task_start_date");
                                    $("[data-select='chosen']").chosen({search_contains: true});
                                
                        }
                    }
                }
            });
        });

 		$(document).on("click", "#save-dialog-task", function () {

 			values = $("#my_site").chosen().val();
 			task_recipient     = $('#task_recipient').val();
            param                    = new Object();
            param.act                = "save_task";
            param.task_type          = $('#task_type').val();
            param.task_status_id     = $('#task_status_id').val();
            param.task_status_id_2   = $('#task_status_id_2').val();
            param.task_branch        = $('#task_branch').val();
            param.task_recipient     = $('#task_recipient').val();
            param.task_create_date   = $('#task_create_date').val();
            param.task_start_date    = $('#task_start_date').val();
            param.task_end_date      = $('#task_end_date').val();
            param.task_description   = $('#task_description').val();
            param.task_note          = $('#task_note').val();
            param.incomming_call_id  = $('#hidde_outgoing_id').val();
            param.incomming_call1_id = $('#incomming_call_id').val();
            param.site               = values;
            
            if($('#task_status_id').val() == 0){
                alert('აირჩიე სტატუსი!!!');
            }else if ($("#task_status_id").val() == "77" && $("#task_status_id_2").val() == "0"){
				alert("შეავსეთ ქვე-სტატუსი !");
			}else if ($('#task_recipient').val() == 0){
            	alert('აირჩიე დავალების მიმღები!!!');
            }else if ($('#task_branch').val() == 0){
            	alert('აირჩიე განყოფილება!!!');
            }else if ($('#task_start_date').val() == ''){
            	alert('შეავსე პერიოდის დასაწყისი!!!');
            }else if ($('#task_end_date').val() == ''){
            	alert('შეავსე პერიოდის დასასრული!!!');
            }else if (values == '' || values == null){
            	alert('შეავსეთ საიტი!!!');
            }else{
            	var link = GetAjaxData(param);
                $.ajax({
                    url: aJaxURL_task,
                    data: link,
                    success: function (data) {
                        if (typeof (data.error) != "undefined") {
                            if (data.error != "") {
                                alert(data.error);
                            }else{
                                GetDataTable('task_table', aJaxURL_task, 'get_list', 6, 'incomming_call_id='+$("#hidde_outgoing_id").val(), 0, "", 2, "desc", '', "<'F'lip>");
								$("#task_table_length").css('top','0px');
                                $("#add-edit-form-task").dialog('close');

                                param1                   = new Object();
                                param1.task_id           = data.task_id;
                                param1.task_type         = "2";
                                param1.status_name       = data.status_name;
                                param1.site              = values;
                                param1.send_type         = 1;
                                param1.incomming_call_id = $("#incomming_call_id").val();
                                param1.hidde_outgoing_id = $('#hidde_outgoing_id').val();
                                param1.task_recipient     = task_recipient;
                                param1.type               = "1";
                                var link1                = GetAjaxData(param1);
                                
                                $.ajax({
                                    url: aJaxURL_getmail,
                                    data: link1,
                            		success: function (data) {
                                    }
                                });
                            }
                        }
                    }
                });
            }
        });

 		$(document).on("change", "#task_type", function () {
            param        = new Object();
            param.act    = "task_type_changed";
            param.cat_id = $('#task_type').val();
            $.ajax({
                url: aJaxURL_task,
                data: param,
                success: function (data) {
                    $("#task_status_id").html(data.page);
                    $('#task_status_id').trigger("chosen:updated");
                    $('#task_branch').trigger("chosen:updated");
                    $('#task_status_id').trigger("change");
                }
            });
        });

	    $(document).on("change", "#task_status_id", function () {
            param = new Object();
            param.act = "get_task_status_2";
            param.cat_id = $('#task_status_id').val();
            $.ajax({
                url: aJaxURL_task,
                data: param,
                success: function (data) {
                    $("#task_status_id_2").html(data.page);
                    $('#task_status_id_2').trigger("chosen:updated");
                }
            });
        });
        
        $(document).on("click", "#fillter", function () {
            LoadTable2();
        });

        $(document).on("click", "#fillter4", function () {
            LoadTable4_2();
        });

        $(document).on("click", "#fillter5", function () {
            LoadTable5_2();
        });

        $(document).on("click", "#fillter6", function () {
            LoadTable6_2();
        });
        
        $(document).on("click", ".callapp_refresh", function () {
            var tab = GetSelectedTab(tbName);
            if (tab == 0) {
                LoadTable0();
            } else if (tab == 1) {
                LoadTable1();
            } else {
                LoadTable2();
            }
        });

        $(document.body).click(function (e) {
            $("#send_to").autocomplete("close");
        });

        // Add - Save

        $(document).on("click", "#save-dialog", function () {
            //$('#save-dialog').attr('disabled', 'disabled');
            
            values                    = $("#my_site").chosen().val();
            out_recepient_id    = $("#add-edit-form #out_recepient_id").val();
            param                     = new Object();
            param.act                 = "save_outgoing";
			param.id                  = $("#add-edit-form #outgoing_call_id").val();
            param.hidde_outgoing_id   = $("#add-edit-form #hidde_outgoing_id").val();
            param.incomming_call_id   = $("#add-edit-form #incomming_call_id").val();
            param.incomming_cat_1     = $("#add-edit-form #incomming_cat_1").val();
            param.incomming_cat_1_1   = $("#add-edit-form #incomming_cat_1_1").val();
            param.incomming_cat_1_1_1 = $("#add-edit-form #incomming_cat_1_1_1").val();
            param.phone               = $("#add-edit-form #phone").val();
            param.s_u_user_id         = $("#add-edit-form #s_u_user_id").val();
            param.s_u_mail            = $("#add-edit-form #s_u_mail").val();
            param.s_u_pid             = $("#add-edit-form #s_u_pid").val();
            param.s_u_name            = $("#add-edit-form #s_u_name").val();
            param.client_sex          = $("#add-edit-form #client_sex").val();
            param.s_u_status          = $("#add-edit-form #s_u_status").val();
            param.source_id           = $("#add-edit-form #source_id").val();
            param.call_content        = $("#add-edit-form #call_content").val();
            param.out_status_id       = $("#add-edit-form #out_status_id").val();
			param.task_branch_id      = $("#add-edit-form #task_branch_id").val();
            param.out_recepient_id    = $("#add-edit-form #out_recepient_id").val();
            param.out_start_date      = $("#add-edit-form #out_start_date").val();
            param.out_end_date        = $("#add-edit-form #out_end_date").val();
            param.call_description    = $("#add-edit-form #call_description").val();
            param.site_id             = values;
            
            if($("#add-edit-form #out_status_id").val() == 0){
                alert('აირჩიე სტატუსი!!!');
            }else if ($("#add-edit-form #out_recepient_id").val() == 0){
            	alert('აირჩიე დავალების მიმღები!!!');
            }else if ($("#add-edit-form #task_branch_id").val() == 0){
            	alert('აირჩიე განყოფილება!!!');
            }else if ($("#add-edit-form #out_start_date").val() == ''){
            	alert('შეავსე პერიოდის დასაწყისი!!!');
            }else if ($("#add-edit-form #out_end_date").val() == ''){
            	alert('შეავსე პერიოდის დასასრული!!!');
            }else if (values == '' || values == null){
            	alert('შეავსეთ საიტი!!!');
            }else{
            	var link  = GetAjaxData(param);
                $.ajax({
                    url: aJaxURL,
                    data: link,
                    success: function (data) {
                        if (typeof (data.error) != "undefined") {
                            if(data.error != ""){
                                alert(data.error);
                            }else{
                                if(data.out_type_id == 1){
                                    if(data.out_status_id == 1){
                                    	LoadTable0();
                                    }else if(data.out_status_id == 2){
                                    	LoadTable1();
                                    }else if(data.out_status_id == 3){
                                    	LoadTable2();
                                    }else{
                                    	LoadTable0();
                                    }
                                }else if(data.out_type_id == 2){
                                	if(data.out_status_id == 1){
                                    	LoadTable4_0();
                                    }else if(data.out_status_id == 2){
                                    	LoadTable4_1();
                                    }else if(data.out_status_id == 3){
                                    	LoadTable4_2();
                                    }else{
                                    	LoadTable4_0();
                                    }
                                }else if (data.out_type_id == 3){
                                	if(data.out_status_id == 1){
                                    	LoadTable5_0();
                                    }else if(data.out_status_id == 2){
                                    	LoadTable5_1();
                                    }else if(data.out_status_id == 3){
                                    	LoadTable5_2();
                                    }else{
                                    	LoadTable5_0();
                                    }
                                }else if (data.out_type_id == 4){
                                	if(data.out_status_id == 1){
                                    	LoadTable6_0();
                                    }else if(data.out_status_id == 2){
                                    	LoadTable6_1();
                                    }else if(data.out_status_id == 3){
                                    	LoadTable6_2();
                                    }else{
                                    	LoadTable6_0();
                                    }
                                }else{
                                	LoadTable0();
                                }
                                
                                $("#add-edit-form").dialog('close');
    
                                param1                   = new Object();
                                param1.task_id           = data.task_id;
                                param1.task_type         = "2";
                                param1.status_name       = data.status_name;
                                param1.site              = values;
                                param1.time 			 = data.time;
                                param1.send_type         = 1;
                                param1.incomming_call_id = $("#incomming_call_id").val();
                                param1.hidde_outgoing_id = $('#hidde_outgoing_id').val();
                                param1.recepient_id      = out_recepient_id;
                                param1.type              = data.type;
                                var link1                = GetAjaxData(param1);
                                
                                $.ajax({
                                    url: aJaxURL_getmail,
                                    data: link1,
                            		success: function (data) {
                                    }
                                });
                            }
                        }
                    }
                });
            }
        });
        
		$(document).on("click", "#download", function () {
			
	    	var download_file	= $(this).val();
	    	var download_name 	= $('#download_name').val();
	    	SaveToDisk(download_file, download_name);
	    });
        $(document).on("click", "#download1", function () {
            var download_file = $(this).val();
            var download_name = $('#download_name').val();
            SaveToDisk1(download_file, download_name);
            alert(fileURL);
        });

        function SaveToDisk1(fileURL, fileName) {
            $("#downloader").attr("href", fileURL);
            $("#downloader")[0].click();
            var iframe = document.createElement("iframe");
            iframe.src = fileURL;
            iframe.style.display = "none";
            document.body.appendChild(iframe);
            return false;
        }

        function SaveToDisk(fileURL, fileName) {
			$("#downloader").attr("href",fileURL);
			$("#downloader")[0].click();
	    	var iframe = document.createElement("iframe");
	        iframe.src = fileURL;
	        iframe.style.display = "none";
	        document.body.appendChild(iframe);
	        return false;
	    }

        $(document).on("click", "#choose_button", function () {
            $("#choose_file").click();
        });
        $(document).on("click", "#choose_button3", function () {

            $("#choose_file3").click();
        });

        $(document).on("click", "#delete", function () {
            var delete_id = $(this).val();

            var r = confirm("გსურთ წაშალოთ?");
            if (r == true) {
                $.ajax({
                    url: aJaxURL,
                    data: {
                        act: "delete_file",
                        delete_id: delete_id,
                        inc_id: $("#incomming_call_id").val(),
                        out_id: $("#hidde_outgoing_id").val(),
                    },
                    success: function (data) {
                        $("#file_div").html(data.page);
                    }

                });
            }
        });
        $(document).on("click", "#delete1", function () {
            var delete_id = $(this).val();

            var r = confirm("გსურთ წაშალოთ?");
            if (r == true) {
                $.ajax({
                    url: aJaxURL,
                    data: {
                        act: "delete_file1",
                        delete_id: delete_id,
                        edit_id: $("#request_base_id").val(),
                    },
                    success: function (data) {
                        $("#file_div").html(data.page);
                    }

                });
            }
        });

        $(document).on("change", "#choose_file", function () {
            var file    = $(this).val();
            var files   = this.files[0];
            var name    = uniqid();
            var path    = "../../media/uploads/file/";
            var inc_id  = $('#incomming_call_id').val();
            var task_id = $('#hidde_outgoing_id').val();

            
            var ext     = file.split('.').pop().toLowerCase();
            if ($.inArray(ext, ['pdf', 'pptx','png','xls','xlsx','jpg','docx','doc','csv']) == -1) { //echeck file type
                alert('This is not an allowed file type');
                this.value = '';
            } else {
                file_name = files.name;
                rand_file = name + "." + ext;
                $.ajaxFileUpload({
                    url: upJaxURL,
                    secureuri: false,
                    fileElementId: "choose_file",
                    dataType: 'json',
                    data: {
                        act: "upload_file",
                        path: path,
                        file_name: name,
                        type: ext
                    },
                    success: function (data, status) {
                        if (typeof (data.error) != 'undefined') {
                            if (data.error != '') {
                                alert(data.error);
                            }else {
                                $.ajax({
                                    url: aJaxURL,
                                    data: {
                                        act: "up_now",
                                        rand_file: data['file_name'],
                                        file_name: file_name,
                                        inc_id: inc_id,
                                        task_id: task_id
                                    },
                                    success: function (data) {
                                        $("#file_div").html(data.page);
                                        $("#choose_file").val('');
                                    }
                                });
                            }
                        }


                    },
                    error: function (data, status, e) {
                        alert(e);
                    }
                });
            }
        });
        $(document).on("change", "#choose_file3", function () {
            var file = $(this).val();
            var files = this.files[0];
            var name = uniqid();
            var path = "../../media/uploads/file/";
            var id1 = $('#request_base_id').val();
            var ext = file.split('.').pop().toLowerCase();
            if ($.inArray(ext, ['pdf', 'png', 'pptx', 'xls', 'xlsx', 'jpg', 'docx', 'csv']) == -1) { //echeck file type
                alert('This is not an allowed file type');
                this.value = '';
            } else {
                file_name = files.name;
                rand_file = name + "." + ext;
                $.ajaxFileUpload({
                    url: upJaxURL,
                    secureuri: false,
                    fileElementId: "choose_file3",
                    dataType: 'json',
                    data: {
                        act: "upload_file3",
                        path: path,
                        file_name: name,
                        type: ext
                    },
                    success: function (data, status) {
                        if (typeof (data.error) != 'undefined') {
                            if (data.error != '') {
                                alert(data.error);
                            }
                        }

                        $.ajax({
                            url: aJaxURL,
                            data: {
                                act: "up_now1",
                                rand_file: rand_file,
                                file_name: file_name,
                                edit_id: id1

                            },
                            success: function (data) {
                                $("#file_div").html(data.page);
                            }
                        });
                    },
                    error: function (data, status, e) {
                        alert(e);
                    }
                });
            }
        });


        $(document).on("click", "#save-dialog2", function () {
            param = new Object();
            param.act = "save_outgoing";

            param.id = $("#add-edit-form1 #id").val();
            param.id1 = $("#add-edit-form1 #id1").val();
            param.call_date = $("#add-edit-form1 #call_date").val();
            param.problem_date = $("#add-edit-form1 #problem_date").val();
            param.persons_id = $("#add-edit-form1 #persons_id").val();
            param.task_type_id = $("#add-edit-form1 #task_type_id").val();
            param.connect = $('#add-edit-form1 input[name=check_transfer]:checked').val();

            param.priority_id = $("#add-edit-form1 #priority_id").val();
            param.planned_end_date = $("#add-edit-form1 #planned_end_date").val();
            param.fact_end_date = $("#add-edit-form1 #fact_end_date").val();
            param.call_duration = $("#add-edit-form1 #call_duration").val();
            param.phone = $("#add-edit-form1 #phone").val();
            param.coller = $("#add-edit-form1 #coller").val();
            param.comment = $("#add-edit-form1 #comment").val();
            param.problem_comment = $("#add-edit-form1 #problem_comment").val();

            $.ajax({
                url: aJaxURL1,
                data: param,
                success: function (data) {
                    if (typeof (data.error) != "undefined") {
                        if (data.error != "") {
                            alert(data.error);
                        } else {
                            LoadTable1();
                            CloseDialog("add-edit-form1");
                        }
                    }
                }
            });
        });
        
        $(document).on("click", "#done-dialog2", function () {
            param = new Object();
            param.act = "done_outgoing";

            param.id = $("#add-edit-form1 #id").val();
            param.id1 = $("#add-edit-form1 #id1").val();
            param.call_date = $("#add-edit-form1 #call_date").val();
            param.problem_date = $("#add-edit-form1 #problem_date").val();
            param.persons_id = $("#add-edit-form1 #persons_id").val();
            param.task_type_id = $("#add-edit-form1 #task_type_id").val();
            param.priority_id = $("#add-edit-form1 #priority_id").val();
            param.planned_end_date = $("#add-edit-form1 #planned_end_date").val();
            param.fact_end_date = $("#add-edit-form1 #fact_end_date").val();
            param.call_duration = $("#add-edit-form1 #call_duration").val();
            param.connect = $('#add-edit-form1 input[name=check_transfer]:checked').val();
            param.phone = $("#add-edit-form1 #phone").val();
            param.out_phone = $("#add-edit-form1 #out_phone").val();
            param.coller = $("#add-edit-form1 #coller").val();
            param.comment = $("#add-edit-form1 #comment").val();
            param.problem_comment = $("#add-edit-form1 #problem_comment").val();

            $.ajax({
                url: aJaxURL1,
                data: param,
                success: function (data) {
                    if (typeof (data.error) != "undefined") {
                        if (data.error != "") {
                            alert(data.error);
                        } else {
                            LoadTable1();
                            CloseDialog("add-edit-form1");
                        }
                    }
                }
            });
        });

        $(document).on("change", "#incomming_cat_1", function () {
            param = new Object();
            param.act = "cat_2";
            param.cat_id = $('#incomming_cat_1').val();
            $.ajax({
                url: aJaxURL,
                data: param,
                success: function (data) {
                    $("#incomming_cat_1_1").html(data.page);
                    $('#incomming_cat_1_1').trigger("chosen:updated");
                    if ($('#incomming_cat_1_1 option:selected').val() == 999) {
                        param = new Object();
                        param.act = "cat_3";
                        param.cat_id = $('#incomming_cat_1_1').val();
                        $.ajax({
                            url: aJaxURL,
                            data: param,
                            success: function (data) {
                                $("#incomming_cat_1_1_1").html(data.page);
                                $('#incomming_cat_1_1_1').trigger("chosen:updated");
                            }
                        });
                    }
                }
            });
        });
        
        $(document).on("change", "#incomming_cat_1_1", function () {
            param = new Object();
            param.act = "cat_3";
            param.cat_id = $('#incomming_cat_1_1').val();
            $.ajax({
                url: aJaxURL,
                data: param,
                success: function (data) {
                    $("#incomming_cat_1_1_1").html(data.page);
                    $('#incomming_cat_1_1_1').trigger("chosen:updated");
                }
            });
        });
        $(document).on("click", "#done-unansver", function () {
            param = new Object();
            param.act = "done_outgoing_unansver";

            param.id = $("#add-edit-form1 #id").val();
            param.id1 = $("#add-edit-form1 #id1").val();
            param.call_date = $("#add-edit-form1 #call_date").val();
            param.problem_date = $("#add-edit-form1 #problem_date").val();
            param.persons_id = $("#add-edit-form1 #persons_id").val();
            param.task_type_id = $("#add-edit-form1 #task_type_id").val();
            param.connect = $('#add-edit-form1 input[name=check_transfer]:checked').val();
            param.priority_id = $("#add-edit-form1 #priority_id").val();
            param.planned_end_date = $("#add-edit-form1 #planned_end_date").val();
            param.fact_end_date = $("#add-edit-form1 #fact_end_date").val();
            param.call_duration = $("#add-edit-form1 #call_duration").val();
            param.phone = $("#add-edit-form1 #phone").val();
            param.out_phone = $("#add-edit-form1 #out_phone").val();
            param.coller = $("#add-edit-form1 #coller").val();
            param.comment = $("#add-edit-form1 #comment").val();
            param.problem_comment = $("#add-edit-form1 #problem_comment").val();

            $.ajax({
                url: aJaxURL1,
                data: param,
                success: function (data) {
                    if (typeof (data.error) != "undefined") {
                        if (data.error != "") {
                            alert(data.error);
                        } else {
                            LoadTable1();
                            CloseDialog("add-edit-form1");
                        }
                    }
                }
            });
        });

        function sub_type_call(sub_tree_id) {
            param = new Object();
            param.act = "change_sub_tee";
            param.sub_tree_id = sub_tree_id;
            $.ajax({
                url: aJaxURL_tabs,
                data: param,
                success: function (data) {

                    document.getElementById("for_sub_change").innerHTML = data.sub_tree;


                }
            });

        }

        $(document).on("click", "#save-manual", function () {
            param = new Object();
            param.act = "sava-manual";
            param.request_base_id = $("#request_base_id").val();
            param.first_type = $("#first_type").val();
            param.sub_type = $("#sub_type").val();
            param.comment = $("#comment").val();

            // დავალების ფორმირებე

            param.task_phone = $("#task_phone").val();
            param.controler_id = $("#controler_id").val();
            param.priority_id = $("#priority_id").val();
            param.persons_id = $("#persons_id").val();
            param.task_department_id = $("#task_department_id").val();
            param.task_type_id = $("#task_type_id").val();
            param.problem_coment = $("#problem_coment").val();

            $.ajax({
                url: aJaxURL_tabs,
                data: param,
                success: function (data) {
                    if (data.error_crm != "") {
                        alert(data.error_crm);
                    } else {
                        CloseDialog(M_fName);
                        GetTableManual(data.sub_type, 1);
                     }


                }
            });


        });

        $(document).on("click", "#save-auto-dialer", function () {
            param = new Object();
            param.act = "sava-manual";
            param.request_base_id = $("#request_base_id").val();
            param.first_type = $("#first_type").val();
            param.sub_type = $("#sub_type").val();
            param.comment = $("#comment").val();

            $.ajax({
                url: aJaxURL_tabs,
                data: param,
                success: function (data) {


                    CloseDialog(AD_fName);
                    
                    GetTableAutoDialer(data.sub_type);
                    // loadtabs(activep = "", activech = "");
               }
            });

        });

        function SetPrivateEvents(add, check, formName, tname) {
            $(document).on("click", "#" + add, function () {
                $.ajax({
                    url: aJaxURL,
                    type: "POST",
                    data: "act=get_responsible_person_add_page",
                    dataType: "json",
                    success: function (data) {
                        if (typeof (data.error) != "undefined") {
                            if (data.error != "") {
                                alert(data.error);
                            } else {
                                $("#" + formName).html(data.page);
                                if ($.isFunction(window.LoadDialog)) {
                                    //execute it
                                    LoadDialog1(tname);
                                }
                            }
                        }
                    }
                });
            });

            $(document).on("click", "#" + check, function () {
                $("#" + tname + " INPUT[type='checkbox']").prop("checked", $("#" + check).is(":checked"));
            });
        }

        function Change_person(tname) {
            var data = $(".check:checked").map(function () {
                return this.value;
            }).get();

            var letters = [];

            for (var i = 0; i < data.length; i++) {
                letters.push(data[i]);
            }
            param       = new Object();
            param.act   = "change_responsible_person";
            param.tname = tname;
            param.lt    = letters;
            param.rp    = $("#responsible_person").val();
			var link    = GetAjaxData(param);

            if (param.rp == "0") {
                alert("აირჩიეთ პასუხისმგებელი პირი!");
            } else {
                $.ajax({
                    url: aJaxURL,
                    type: "POST",
                    data: link,
                    dataType: "json",
                    success: function (data) {
                        $("#add-responsible-person").dialog("close");
                        if(tname == 'example0'){
                        	LoadTable0();
                        }else if(tname == 'example4_0'){
                        	LoadTable4_0();
                        }else if(tname == 'example5_0'){
                        	LoadTable5_0();
                        }else if(tname == 'example6_0'){
                        	LoadTable6_0();
                        }
                    }
                });
            }
        }

        function change_btn_color(id) {
            document.getElementById("li_tab_1").style.backgroundColor = "white";
            document.getElementById("li_tab_2").style.backgroundColor = "white";
            document.getElementById("li_tab_3").style.backgroundColor = "white";
            document.getElementById("li_tab_4").style.backgroundColor = "white";
            document.getElementById("li_tab_5").style.backgroundColor = "white";
            document.getElementById("li_tab_6").style.backgroundColor = "white";
            document.getElementById(id).style.backgroundColor = "#a9a9a940";
            document.getElementById(id).style.marginBottom = "-2px";
            loadtabs(activep = "", activech = "");

        }


        function get_sub_tab(id, control_tab) {
            document.getElementById('control_tab_0').style.display = "none";
            document.getElementById('control_tab_1').style.display = "none";
            document.getElementById('control_tab_2').style.display = "none";
            document.getElementById(control_tab).style.display = "block";

            document.getElementById("control_tab_id_0").style.borderBottomWidth = "1px";
            document.getElementById("control_tab_id_0").style.borderBottomColor = "gray";
            document.getElementById("control_tab_id_0").children[0].style.color = "gray";

            document.getElementById("control_tab_id_1").style.borderBottomWidth = "1px";
            document.getElementById("control_tab_id_1").style.borderBottomColor = "gray";
            document.getElementById("control_tab_id_1").children[0].style.color = "gray";
            

            document.getElementById("control_tab_id_2").style.borderBottomWidth = "1px";
            document.getElementById("control_tab_id_2").style.borderBottomColor = "gray";
            document.getElementById("control_tab_id_2").children[0].style.color = "gray";

            document.getElementById(id).style.borderBottomWidth = "2px";
            document.getElementById(id).style.borderBottomColor = "#0b5c9a";
            document.getElementById(id).children[0].style.color = "black";


        }

        function get_sub_tab4(id, control_tab) {
            document.getElementById('control_tab_4_0').style.display = "none";
            document.getElementById('control_tab_4_1').style.display = "none";
            document.getElementById('control_tab_4_2').style.display = "none";
            document.getElementById(control_tab).style.display = "block";

            document.getElementById("control_tab_id_4_0").style.borderBottomWidth = "1px";
            document.getElementById("control_tab_id_4_0").style.borderBottomColor = "gray";
            document.getElementById("control_tab_id_4_0").children[0].style.color = "gray";

            document.getElementById("control_tab_id_4_1").style.borderBottomWidth = "1px";
            document.getElementById("control_tab_id_4_1").style.borderBottomColor = "gray";
            document.getElementById("control_tab_id_4_1").children[0].style.color = "gray";
            

            document.getElementById("control_tab_id_4_2").style.borderBottomWidth = "1px";
            document.getElementById("control_tab_id_4_2").style.borderBottomColor = "gray";
            document.getElementById("control_tab_id_4_2").children[0].style.color = "gray";

            document.getElementById(id).style.borderBottomWidth = "2px";
            document.getElementById(id).style.borderBottomColor = "#0b5c9a";
            document.getElementById(id).children[0].style.color = "black";


        }

        function get_sub_tab5(id, control_tab) {
            document.getElementById('control_tab_5_0').style.display = "none";
            document.getElementById('control_tab_5_1').style.display = "none";
            document.getElementById('control_tab_5_2').style.display = "none";
            document.getElementById(control_tab).style.display = "block";

            document.getElementById("control_tab_id_5_0").style.borderBottomWidth = "1px";
            document.getElementById("control_tab_id_5_0").style.borderBottomColor = "gray";
            document.getElementById("control_tab_id_5_0").children[0].style.color = "gray";

            document.getElementById("control_tab_id_5_1").style.borderBottomWidth = "1px";
            document.getElementById("control_tab_id_5_1").style.borderBottomColor = "gray";
            document.getElementById("control_tab_id_5_1").children[0].style.color = "gray";
            

            document.getElementById("control_tab_id_5_2").style.borderBottomWidth = "1px";
            document.getElementById("control_tab_id_5_2").style.borderBottomColor = "gray";
            document.getElementById("control_tab_id_5_2").children[0].style.color = "gray";

            document.getElementById(id).style.borderBottomWidth = "2px";
            document.getElementById(id).style.borderBottomColor = "#0b5c9a";
            document.getElementById(id).children[0].style.color = "black";


        }

        function get_sub_tab6(id, control_tab) {
            document.getElementById('control_tab_6_0').style.display = "none";
            document.getElementById('control_tab_6_1').style.display = "none";
            document.getElementById('control_tab_6_2').style.display = "none";
            document.getElementById(control_tab).style.display = "block";

            document.getElementById("control_tab_id_6_0").style.borderBottomWidth = "1px";
            document.getElementById("control_tab_id_6_0").style.borderBottomColor = "gray";
            document.getElementById("control_tab_id_6_0").children[0].style.color = "gray";

            document.getElementById("control_tab_id_6_1").style.borderBottomWidth = "1px";
            document.getElementById("control_tab_id_6_1").style.borderBottomColor = "gray";
            document.getElementById("control_tab_id_6_1").children[0].style.color = "gray";
            

            document.getElementById("control_tab_id_6_2").style.borderBottomWidth = "1px";
            document.getElementById("control_tab_id_6_2").style.borderBottomColor = "gray";
            document.getElementById("control_tab_id_6_2").children[0].style.color = "gray";

            document.getElementById(id).style.borderBottomWidth = "2px";
            document.getElementById(id).style.borderBottomColor = "#0b5c9a";
            document.getElementById(id).children[0].style.color = "black";


        }
        
        function call_first_tab(id) {
            var id1 = document.getElementById(id).getAttribute("tree_id");

            if (id1 == 1) {
                main_li_id_1 = id;
                main_li_id_30 = id;
                document.getElementById("7").click();
                document.getElementById("36").click();


            } else if (id1 == 2) {
                main_li_id_1 = id;
                main_li_id_30 = id;
                document.getElementById("9").click();
                document.getElementById("38").click();

            }


        }


        function call_table1(id) {
            call_id_1 = id;
            var tree_id = document.getElementById(id).getAttribute("tree_id");

            GetTableManual(tree_id, 1);

        }

        function call_table1111(id) {
            call_id_1 = id;
            var tree_id = document.getElementById(id).getAttribute("tree_id");

            GetTableManual(tree_id, 0);

        }

        function call_table30(id) {
            call_id_30 = id;
            var tree_id = document.getElementById(id).getAttribute("tree_id");
            GetTableAutoDialer(tree_id);

        }

        $(document).on("click", "#search_user_id", function (){
	    	param 		      = new Object();
			param.act	      = 'search_user_id';
		 	param.s_u_user_id = $("#s_u_user_id").val();
		 	$.ajax({
		        url: wsdl_url,
		        data: param,
			  	success: function(data) {
			  		if(data.count == 1){
    				  	$("#phone").val(data.phone);
    				  	$("#s_u_user_id").val(data.user_id);
    				  	$("#s_u_mail").val(data.email);
    				  	$("#s_u_name").val(data.user_name+' '+data.user_surname);
    				  	$("#s_u_pid").val(data.id_code);
    				  	$('#client_sex').val(data.gender_id);
    					$('#client_sex').trigger("chosen:updated");
				  	}else if(data.count == 0){
					  	alert('მონაცემები ვერ მოიძებნა');
				  	} 
				}
		    });

	    });

	    $(document).on("click", "#search_phone", function (){
	    	param 		= new Object();
			param.act	= 'search_phone';
		 	param.phone	= $("#phone").val();
		 	$.ajax({
		        url: wsdl_url,
		        data: param,
			  	success: function(data) {
			  		if(data.count == 1){
    				  	$("#phone").val(data.phone);
    				  	$("#s_u_user_id").val(data.user_id);
    				  	$("#s_u_mail").val(data.email);
    				  	$("#s_u_name").val(data.user_name+' '+data.user_surname);
    				  	$("#s_u_pid").val(data.id_code);
    				  	$('#client_sex').val(data.gender_id);
    					$('#client_sex').trigger("chosen:updated");
				  	}else if(data.count == 0){
					  	alert('მონაცემები ვერ მოიძებნა');
				  	}
				}
		    });

	    });

	    $(document).on("click", "#search_mail", function () {
	    	param 		    = new Object();
			param.act	    = 'search_mail';
		 	param.s_u_mail	= $("#s_u_mail").val();
		 	$.ajax({
		        url: wsdl_url,
		        data: param,
			  	success: function(data) {
			  		if(data.count == 1){
    				  	$("#phone").val(data.phone);
    				  	$("#s_u_user_id").val(data.user_id);
    				  	$("#s_u_mail").val(data.email);
    				  	$("#s_u_name").val(data.user_name+' '+data.user_surname);
    				  	$("#s_u_pid").val(data.id_code);
    				  	$('#client_sex').val(data.gender_id);
    					$('#client_sex').trigger("chosen:updated");
				  	}else if(data.count == 0){
					  	alert('მონაცემები ვერ მოიძებნა');
				  	}
				}
		    });

	    });

	    $(document).on("click", "#search_s_u_pid", function(event) {
		    var check_lenght = $("#s_u_pid").val();

		    
	    	param 		    = new Object();
			param.act	    = 'search_s_u_pid';
		 	param.s_u_pid	= $("#s_u_pid").val();
		 	if(check_lenght.length != 9){
			 	alert('ძებნა ხორციელდება მხოლოდ საიდენტიფიკაციო ნომრით');
		 	}else{
    		 	$.ajax({
    		        url: wsdl_url,
    		        data: param,
    			  	success: function(data) {
    			  		if(data.count == 1){
        				  	$("#phone").val(data.phone);
        				  	$("#s_u_user_id").val(data.user_id);
        				  	$("#s_u_mail").val(data.email);
        				  	$("#s_u_name").val(data.user_name+' '+data.user_surname);
        				  	$("#s_u_pid").val(data.id_code);
        				  	$('#client_sex').val(data.gender_id);
        					$('#client_sex').trigger("chosen:updated");
    				  	}else if(data.count == 0){
    					  	alert('მონაცემები ვერ მოიძებნა');
    				  	} 	
    				}
    		    });
		 	}
        });

	    $(document).on("click", "#add_comment", function () {
		    param                   = new Object();
            param.act               = 'save_comment';
            param.comment           = $("#add_ask").val();
            param.incomming_call_id = $("#incomming_id").val();
            param.hidde_outgoing_id = $("#hidde_outgoing_id").val();
            if(param.comment!=""){
                $.ajax({
                    url: aJaxURL,
                    data: param,
                    success: function(data) {
                        param2                      = new Object();
                        param2.act                  = 'get_add_question';
                        param2.incomming_call_id    = $("#incomming_id").val();
                        param2.hidde_outgoing_id    = $("#hidde_outgoing_id").val();
                        $.ajax({
                            url: aJaxURL,
                            data: param2,
                            success: function(data) {
                                $("#chat_question").html(data.page);
                                $("#add_comment").button();
                            }
                        });
                    }
                });
            }
        });
        $(document).on("click", "#get_answer", function () { // get answer page
            LoadDialog("add-edit-form-answer");
            param           = new Object();
            param.act       = 'get_add_page';
            comment_id      = $(this).attr('class');
            $.ajax({
                url: aJaxURL_answer,
                data: param,
                success: function(data) {
                    $("#add-edit-form-answer").html(data.page);
                    $("#add_comment").button();
                }
            });
        });
        $(document).on("click", "#save-dialog-answer", function () { 
            // save answer 
            if($("#comment_info_sorce_id").val()==""){
                param                   = new Object();
                param.act               = 'save_answer';
                param.comment           = $("#in_answer").val();
                param.comment_id        = $('#hidden_comment_id_'+comment_id+'').val();
                param.incomming_call_id = $("#incomming_id").val();
                param.hidde_outgoing_id = $("#hidde_outgoing_id").val();
                if(param.comment!=""){
                    $.ajax({
                        url: aJaxURL_answer,
                        data: param,
                        success: function(data) {
                            param2                   = new Object();
                            param2.act               = 'get_add_question';
                            param2.incomming_call_id = $("#incomming_id").val();
                            param2.hidde_outgoing_id           = $("#hidde_outgoing_id").val();
                            $.ajax({
                                url: aJaxURL,
                                data: param2,
                                success: function(data) {
                                    $("#add-edit-form-answer").dialog("close");
                                    $("#chat_question").html(data.page);
                                    $("#add_comment").button();
                                }
                            });
                        }
                    });
                } else {
                    alert("შეავსეთ ველი")
                }
            } else {
                param2          = new Object();
                param2.act      = 'update_comment';
                param2.id       = edit_comment_id;
                param2.comment  = $("#in_answer").val();
                $.ajax({
                    url: aJaxURL,
                    data: param2,
                    success: function(data) {
                        param2 = new Object();
                        param2.act='get_add_question';
                        param2.incomming_call_id = $("#incomming_id").val();
                        param2.hidde_outgoing_id           = $("#hidde_outgoing_id").val();
                        $.ajax({
                            url: aJaxURL,
                            data: param2,
                            success: function(data) {
                                $("#add-edit-form-answer").dialog("close");
                                $("#chat_question").html(data.page);
                                $("#add_comment").button();
                            }
                        });
                    }
                });
            }
        });
        $(document).on("click", "#delete_comment", function () { // save answer 
            param = new Object();
            param.act='delete_comment';
            param.comment_id=$(this).attr('my_id');
            $.ajax({
                    url: aJaxURL,
                    data: param,
                    success: function(data) {
                        param2 = new Object();
                        param2.act='get_add_question';
                        param2.incomming_call_id = $("#incomming_id").val();
                        param2.hidde_outgoing_id = $("#hidde_outgoing_id").val();
                        $.ajax({
                            url: aJaxURL,
                            data: param2,
                            success: function(data) {
                                $("#chat_question").html(data.page);
                                $("#add_comment").button();
                            }
                        });
                    }
            });
        });
        $(document).on("click", "#edit_comment", function () { // save answer 
            param = new Object();
            param.act='get_edit_page';
            edit_comment_id=$(this).attr('my_id');
            param.id= $(this).attr('my_id');
            $.ajax({
                url: aJaxURL_answer,
                data: param,
                success: function(data) {
                    $("#add-edit-form-answer").html(data.page);
                    LoadDialog("add-edit-form-answer");
                    }
                });
            });
    </script>
</head>
<style>


    #ul_tab li {
        display: inline;
        margin: 13px;
        color: #0b5c9a;
        border-bottom: 1px solid gray;
        padding-bottom: 7px;
        cursor: pointer;
    }

    #ul_tab {
        margin-left: -45px;
    }

    #ul_tab input {

        width: 50px;
        border: 1px solid gray;
        border-width: 1px;
        border-radius: 22px;
        text-align: center;
        background-color: white;
        margin-left: 5px;
        color: gray;
        margin-top: -5px;
        height: 15px;
    }

    #ul_tab a {
        color: gray;
        font-family: pvn;
    }
    
#button_area {
    float: left;
    height: 30px;
}
</style>

<body>
<div class="callapp_head" style="padding: 13px 5px 0 5px;">გამავალი ზარები<span class="callapp_refresh"><img alt="refresh" src="media/images/icons/refresh.png" height="14" width="14">   განახლება</span><hr class="callapp_head_hr"></div>
<table id="table_right_menu" style="top: 140px; left: -19px;">
	<tr>
		<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;background:#2681DC;" id="show_table" myvar="0">
			<img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
		</td>
		<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_log" myvar="0">
			<img alt="log" src="media/images/icons/log.png" height="14" width="14">
		</td>
		<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_copy_prit_exel" myvar="0">
			<img alt="link" src="media/images/icons/select.png" height="14" width="14">
		</td>
	</tr>
</table>
<div id="tabs" style="width: 99%;">
    <ul style="margin-left: -3px">
        <li style="border: none; padding-bottom: 2px"><a onclick="change_btn_color(this.id)" id="li_tab_1"
                                                         style="font-family: pvn; background-color:#a9a9a940 !important; margin-bottom: -2px"
                                                         href="#tab-0">გამავალი ზარები</a></li>
        <li style=" border: none;"><a onclick="change_btn_color(this.id)" id="li_tab_4" style="font-family: pvn; background-color: white"
                                      href="#tab-4">გამოტოვებული ზარები</a></li>
        <li style=" border: none;"><a onclick="change_btn_color(this.id)" id="li_tab_5" style="font-family: pvn; background-color: white"
                                      href="#tab-5">მცდელობები</a></li>
        <li style=" border: none;"><a onclick="change_btn_color(this.id)" id="li_tab_2" style="font-family: pvn; background-color: white"
                                      href="#tab-1">მანუალური</a></li>
        <li style=" border: none"><a onclick="change_btn_color(this.id)" id="li_tab_3" style="font-family: pvn; background-color: white"
                                     href="#tab-2">ავტოდაილერი</a></li>
        <li style=" border: none"><a onclick="change_btn_color(this.id)" id="li_tab_6" style="font-family: pvn; background-color: white"
                                     href="#tab-6">ლიდი</a></li>
    </ul>


    <div id="tab-0">
        <div>
            <ul id="ul_tab" style="margin-top: 5px; display: block;">
                <li role="presentation" id="control_tab_id_0" onclick="get_sub_tab(this.id,'control_tab_0')"
                    style="color: black; border-bottom-width: 2px; border-bottom-color: rgb(11, 92, 154);">
                    <a style="color: black; text-decoration: none;" href="#">პირველადი</a></li>
                <li role="presentation" id="control_tab_id_1" onclick="get_sub_tab(this.id,'control_tab_1')"
                    style="border-bottom-width: 1px; border-bottom-color: gray;"><a href="#"
                                                                                                     style="text-decoration: none; color: gray;">მიმდინარე</a></li>
                <li role="presentation" id="control_tab_id_2" onclick="get_sub_tab(this.id,'control_tab_2')"
                    style="border-bottom-width: 1px; border-bottom-color: gray;"><a href="#"
                                                                                                     style="text-decoration: none; color: gray;">დასრულებული</a></li>
            </ul>

        </div>
        <div id="control_tab_0" class="ex_highlight_row">
            <div id="dt_example">
                <div id="button_area" style="margin-top: 25px;">
                    <button id="add_button" style="height: 25px; border-radius: 0px;">დამატება</button>
                    <button id="add_responsible_person" style="height: 25px; border-radius: 0px">პ. პირის აქტივაცია
                    </button>
                </div>

                <table class="display" id="example0">
                    <thead>
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width:7%;">ID</th>
                            <th style="width: 12%;">თარიღი</th>
                            <th style="width: 12%;">ტელეფონი</th>
                            <th style="width: 9%;">User Id</th>
                            <th style="width: 12%;">ქვე კატეგორია</th>
                            <th style="width: 12%;">სახელი,გვარი</th>
                            <th style="width: 12%;">პასუხისმგებელი პირი</th>
                            <th style="width: 12%;">დამფორმირებელი</th>
                            <th style="width: 13%;">სტატუსი</th>
                            <th class="check">#</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                                <input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <div class="callapp_checkbox">
                                    <input type="checkbox" id="check-all" name="check-all"/>
                                    <label for="check-all"></label>
                                </div>
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div style="display: none" id="control_tab_1" class="ex_highlight_row">
            <div id="dt_example">
				<div id="button_area" class="" style="margin-top: 25px;"></div>
				<table class="display" id="example1">
                    <thead>
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width:7%;">ID</th>
                            <th style="width: 12%;">თარიღი</th>
                            <th style="width: 12%;">ტელეფონი</th>
                            <th style="width: 9%;">User Id</th>
                            <th style="width: 12%;">ქვე კატეგორია</th>
                            <th style="width: 12%;">სახელი,გვარი</th>
                            <th style="width: 12%;">პასუხისმგებელი პირი</th>
                            <th style="width: 12%;">დამფორმირებელი</th>
                            <th style="width: 13%;">სტატუსი</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                                <input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div style="display: none" id="control_tab_2" class="ex_highlight_row">
            <div id="dt_example">
                <div id="button_area" style="margin-top: 25px;">
                    <table>
                        <tr>
                            <td>
                                <label for="search_start_my" class="left" style="margin: 5px 0 0 15px;">დან-</label>
                            </td>
                            <td>
                                <input style="width: 100px; height: 15px;"
                                       value="<?php echo date('Y-m-d') . " 00:00"; ?>" type="text" name="search_start"
                                       id="search_start_my" class="inpt left"/>
                            </td>
                            <td>
                                <label for="search_end_my" class="left" style="margin: 5px 0 0 15px;">მდე-</label>
                            </td>
                            <td>
                                <input style="width: 100px; height: 15px;"
                                       value="<?php echo date('Y-m-d') . " 23:59"; ?>" type="text" name="search_end"
                                       id="search_end_my" class="inpt right"/>
                            </td>
                            <td>
                                <button id="fillter" style="margin-left: 10px;">ფილტრი</button>
                            </td>
                        </tr>
                    </table>
                    <div class="left" style="width: 160px;">


                    </div>
                    <div class="right" style="">


                    </div>
                </div>
                <table class="display" id="example2">
                    <thead>
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width:7%;">ID</th>
                            <th style="width: 12%;">თარიღი</th>
                            <th style="width: 12%;">ტელეფონი</th>
                            <th style="width: 9%;">User Id</th>
                            <th style="width: 12%;">ქვე კატეგორია</th>
                            <th style="width: 12%;">სახელი,გვარი</th>
                            <th style="width: 12%;">პასუხისმგებელი პირი</th>
                            <th style="width: 12%;">დამფორმირებელი</th>
                            <th style="width: 13%;">სტატუსი</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                                <input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>

    </div>

    <div id="tab-1">
        <div id="tabs1" style="padding-bottom: 60px">
        </div>
        <table class="display" id="manual">
            <thead>
            <tr id="datatable_header">
                <th></th>
                <th style="width:15%;">თარიღი</th>
                <th style="width: 25%;">კამპანიის დასახელება</th>
                <th style="width: 15%;">კამპანიის კოდი</th>
                <th style="width: 20%;">ნომერი</th>
                <th style="width: 25%;">კომენტარი</th>
                <th class="check">#</th>

            </tr>
            </thead>
            <thead>
            <tr class="search_header">
                <th class="colum_hidden">
                    <input type="text" name="search_id" value="ფილტრი" class="search_init"/>
                </th>

                <th>
                    <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_phone" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>

                <th>
                    <div class="callapp_checkbox">
                        <input type="checkbox" id="check-all-manual" name="check-all-manual"/>
                        <label for="check-all-manual"></label>
                    </div>
                </th>
            </tr>
            </thead>
        </table>
    </div>
    <div id="tab-2">
        <div id="tabs3" style="padding-bottom: 60px">
        </div>
        <table class="display" id="auto_dialer">
            <thead>
            <tr id="datatable_header">
                <th></th>
                <th style="width:15%;">თარიღი</th>
                <th style="width: 25%;">კამპანიის დასახელება</th>
                <th style="width: 15%;">კამპანიის კოდი</th>
                <th style="width: 20%;">ნომერი</th>
                <th style="width: 25%;">კომენტარი</th>
                <th class="check">#</th>

            </tr>
            </thead>
            <thead>
            <tr class="search_header">
                <th class="colum_hidden">
                    <input type="text" name="search_id" value="ფილტრი" class="search_init"/>
                </th>

                <th>
                    <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_phone" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>

                <th>
                    <div class="callapp_checkbox">
                        <input type="checkbox" id="check-all-auto-dialre" name="check-all-auto-dialer"/>
                        <label for="check-all-auto-dialre"></label>
                    </div>
                </th>
            </tr>
            </thead>
        </table>
    </div>
    
    <div id="tab-4">
        <div>
            <ul id="ul_tab" style="margin-top: 5px; display: block;">
                <li role="presentation" id="control_tab_id_4_0" onclick="get_sub_tab4(this.id,'control_tab_4_0')"
                    style="color: black; border-bottom-width: 2px; border-bottom-color: rgb(11, 92, 154);">
                    <a style="color: black; text-decoration: none;" href="#">პირველადი</a></li>
                <li role="presentation" id="control_tab_id_4_1" onclick="get_sub_tab4(this.id,'control_tab_4_1')"
                    style="border-bottom-width: 1px; border-bottom-color: gray;"><a href="#"
                                                                                                     style="text-decoration: none; color: gray;">მიმდინარე</a></li>
                <li role="presentation" id="control_tab_id_4_2" onclick="get_sub_tab4(this.id,'control_tab_4_2')"
                    style="border-bottom-width: 1px; border-bottom-color: gray;"><a href="#"
                                                                                                     style="text-decoration: none; color: gray;">დასრულებული</a></li>
            </ul>

        </div>
        <div id="control_tab_4_0" class="ex_highlight_row">
            <div id="dt_example">
                <div id="button_area" style="margin-top: 25px;">
                    <button id="add_responsible_person_nonwork" style="height: 25px; border-radius: 0px">პ. პირის აქტივაცია
                    </button>
                </div>

                <table class="display" id="example4_0">
                    <thead>
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width:7%;">ID</th>
                            <th style="width: 12%;">თარიღი</th>
                            <th style="width: 12%;">ტელეფონი</th>
                            <th style="width: 9%;">User Id</th>
                            <th style="width: 12%;">ქვე კატეგორია</th>
                            <th style="width: 12%;">სახელი,გვარი</th>
                            <th style="width: 12%;">პასუხისმგებელი პირი</th>
                            <th style="width: 12%;">დამფორმირებელი</th>
                            <th style="width: 13%;">სტატუსი</th>
                            <th class="check">#</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                                <input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <div class="callapp_checkbox">
                                    <input type="checkbox" id="check-all-nonwork" name="check-all-nonwork"/>
                                    <label for="check-all-nonwork"></label>
                                </div>
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div style="display: none" id="control_tab_4_1" class="ex_highlight_row">
            <div id="dt_example">

                <div id="button_area" class="" style="margin-top: 25px;">
                    <button class="add_responsible" id="add_responsible_person"
                            style="display:none; height: 25px; border-radius: 0px">პ. პირის შეცვლა
                    </button>
                </div>

                <table class="display" id="example4_1">
                    <thead>
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width:7%;">ID</th>
                            <th style="width: 12%;">თარიღი</th>
                            <th style="width: 12%;">ტელეფონი</th>
                            <th style="width: 9%;">User Id</th>
                            <th style="width: 12%;">ქვე კატეგორია</th>
                            <th style="width: 12%;">სახელი,გვარი</th>
                            <th style="width: 12%;">პასუხისმგებელი პირი</th>
                            <th style="width: 12%;">დამფორმირებელი</th>
                            <th style="width: 13%;">სტატუსი</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                                <input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div style="display: none" id="control_tab_4_2" class="ex_highlight_row">
            <div id="dt_example">
                <div id="button_area" style="margin-top: 25px;">
                    <table>
                        <tr>
                            <td>
                                <label for="search_start_my" class="left" style="margin: 5px 0 0 15px;">დან-</label>
                            </td>
                            <td>
                                <input style="width: 100px; height: 15px;"
                                       value="<?php echo date('Y-m-d') . " 00:00"; ?>" type="text" name="search_start"
                                       id="search_start_my_4" class="inpt left"/>
                            </td>
                            <td>
                                <label for="search_end_my" class="left" style="margin: 5px 0 0 15px;">მდე-</label>
                            </td>
                            <td>
                                <input style="width: 100px; height: 15px;"
                                       value="<?php echo date('Y-m-d') . " 23:59"; ?>" type="text" name="search_end"
                                       id="search_end_my_4" class="inpt right"/>
                            </td>
                            <td>
                                <button id="fillter4" style="margin-left: 10px;">ფილტრი</button>
                            </td>
                        </tr>
                    </table>
                    <div class="left" style="width: 160px;">


                    </div>
                    <div class="right" style="">


                    </div>
                </div>
                <table class="display" id="example4_2">
                    <thead>
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width:7%;">ID</th>
                            <th style="width: 12%;">თარიღი</th>
                            <th style="width: 12%;">ტელეფონი</th>
                            <th style="width: 9%;">User Id</th>
                            <th style="width: 12%;">ქვე კატეგორია</th>
                            <th style="width: 12%;">სახელი,გვარი</th>
                            <th style="width: 12%;">პასუხისმგებელი პირი</th>
                            <th style="width: 12%;">დამფორმირებელი</th>
                            <th style="width: 13%;">სტატუსი</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                                <input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>

    </div>
    <div id="tab-5">
        <div>
            <ul id="ul_tab" style="margin-top: 5px; display: block;">
                <li role="presentation" id="control_tab_id_5_0" onclick="get_sub_tab5(this.id,'control_tab_5_0')"
                    style="color: black; border-bottom-width: 2px; border-bottom-color: rgb(11, 92, 154);">
                    <a style="color: black; text-decoration: none;" href="#">პირველადი</a></li>
                <li role="presentation" id="control_tab_id_5_1" onclick="get_sub_tab5(this.id,'control_tab_5_1')"
                    style="border-bottom-width: 1px; border-bottom-color: gray;"><a href="#"
                                                                                                     style="text-decoration: none; color: gray;">მიმდინარე</a></li>
                <li role="presentation" id="control_tab_id_5_2" onclick="get_sub_tab5(this.id,'control_tab_5_2')"
                    style="border-bottom-width: 1px; border-bottom-color: gray;"><a href="#"
                                                                                                     style="text-decoration: none; color: gray;">დასრულებული</a></li>
            </ul>

        </div>
        <div id="control_tab_5_0" class="ex_highlight_row">
            <div id="dt_example">
                <div id="button_area" style="margin-top: 25px;">
                    <button id="add_responsible_person_active" style="height: 25px; border-radius: 0px">პ. პირის აქტივაცია
                    </button>
                </div>

                <table class="display" id="example5_0">
                    <thead>
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width:7%;">ID</th>
                            <th style="width: 12%;">თარიღი</th>
                            <th style="width: 12%;">ტელეფონი</th>
                            <th style="width: 9%;">User Id</th>
                            <th style="width: 12%;">ქვე კატეგორია</th>
                            <th style="width: 12%;">სახელი,გვარი</th>
                            <th style="width: 12%;">პასუხისმგებელი პირი</th>
                            <th style="width: 12%;">დამფორმირებელი</th>
                            <th style="width: 13%;">სტატუსი</th>
                            <th class="check">#</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                                <input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <div class="callapp_checkbox">
                                    <input type="checkbox" id="check-all-active" name="check-all-active"/>
                                    <label for="check-all-active"></label>
                                </div>
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div style="display: none" id="control_tab_5_1" class="ex_highlight_row">
            <div id="dt_example">

                <div id="button_area" class="" style="margin-top: 25px;">
                    <button class="add_responsible" id="add_responsible_person"
                            style="display:none; height: 25px; border-radius: 0px">პ. პირის შეცვლა
                    </button>
                </div>

                <table class="display" id="example5_1">
                    <thead>
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width:7%;">ID</th>
                            <th style="width: 12%;">თარიღი</th>
                            <th style="width: 12%;">ტელეფონი</th>
                            <th style="width: 9%;">User Id</th>
                            <th style="width: 12%;">ქვე კატეგორია</th>
                            <th style="width: 12%;">სახელი,გვარი</th>
                            <th style="width: 12%;">პასუხისმგებელი პირი</th>
                            <th style="width: 12%;">დამფორმირებელი</th>
                            <th style="width: 13%;">სტატუსი</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                                <input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div style="display: none" id="control_tab_5_2" class="ex_highlight_row">
            <div id="dt_example">
                <div id="button_area" style="margin-top: 25px;">
                    <table>
                        <tr>
                            <td>
                                <label for="search_start_my" class="left" style="margin: 5px 0 0 15px;">დან-</label>
                            </td>
                            <td>
                                <input style="width: 100px; height: 15px;"
                                       value="<?php echo date('Y-m-d') . " 00:00"; ?>" type="text" name="search_start"
                                       id="search_start_my_5" class="inpt left"/>
                            </td>
                            <td>
                                <label for="search_end_my" class="left" style="margin: 5px 0 0 15px;">მდე-</label>
                            </td>
                            <td>
                                <input style="width: 100px; height: 15px;"
                                       value="<?php echo date('Y-m-d') . " 23:59"; ?>" type="text" name="search_end"
                                       id="search_end_my_5" class="inpt right"/>
                            </td>
                            <td>
                                <button id="fillter5" style="margin-left: 10px;">ფილტრი</button>
                            </td>
                        </tr>
                    </table>
                    <div class="left" style="width: 160px;">


                    </div>
                    <div class="right" style="">


                    </div>
                </div>
                <table class="display" id="example5_2">
                    <thead>
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width:7%;">ID</th>
                            <th style="width: 12%;">თარიღი</th>
                            <th style="width: 12%;">ტელეფონი</th>
                            <th style="width: 9%;">User Id</th>
                            <th style="width: 12%;">ქვე კატეგორია</th>
                            <th style="width: 12%;">სახელი,გვარი</th>
                            <th style="width: 12%;">პასუხისმგებელი პირი</th>
                            <th style="width: 12%;">დამფორმირებელი</th>
                            <th style="width: 13%;">სტატუსი</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                                <input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>

    </div>
    <div id="tab-6">
        <div>
            <ul id="ul_tab" style="margin-top: 5px; display: block;">
                <li role="presentation" id="control_tab_id_6_0" onclick="get_sub_tab6(this.id,'control_tab_6_0')"
                    style="color: black; border-bottom-width: 2px; border-bottom-color: rgb(11, 92, 154);">
                    <a style="color: black; text-decoration: none;" href="#">პირველადი</a></li>
                <li role="presentation" id="control_tab_id_6_1" onclick="get_sub_tab6(this.id,'control_tab_6_1')"
                    style="border-bottom-width: 1px; border-bottom-color: gray;"><a href="#"
                                                                                                     style="text-decoration: none; color: gray;">მიმდინარე</a></li>
                <li role="presentation" id="control_tab_id_6_2" onclick="get_sub_tab6(this.id,'control_tab_6_2')"
                    style="border-bottom-width: 1px; border-bottom-color: gray;"><a href="#"
                                                                                                     style="text-decoration: none; color: gray;">დასრულებული</a></li>
            </ul>

        </div>
        <div id="control_tab_6_0" class="ex_highlight_row">
            <div id="dt_example">
                <div id="button_area" style="margin-top: 25px;">
                    <button id="add_responsible_person_lid" style="height: 25px; border-radius: 0px">პ. პირის აქტივაცია
                    </button>
                </div>

                <table class="display" id="example6_0">
                    <thead>
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width:7%;">ID</th>
                            <th style="width: 12%;">თარიღი</th>
                            <th style="width: 12%;">ტელეფონი</th>
                            <th style="width: 9%;">User Id</th>
                            <th style="width: 12%;">ქვე კატეგორია</th>
                            <th style="width: 12%;">სახელი,გვარი</th>
                            <th style="width: 12%;">პასუხისმგებელი პირი</th>
                            <th style="width: 12%;">დამფორმირებელი</th>
                            <th style="width: 13%;">სტატუსი</th>
                            <th class="check">#</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                                <input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <div class="callapp_checkbox">
                                    <input type="checkbox" id="check-all-lid" name="check-all-lid"/>
                                    <label for="check-all-lid"></label>
                                </div>
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div style="display: none" id="control_tab_6_1" class="ex_highlight_row">
            <div id="dt_example">

                <div id="button_area" class="" style="margin-top: 25px;">
                </div>

                <table class="display" id="example6_1">
                    <thead>
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width:7%;">ID</th>
                            <th style="width: 12%;">თარიღი</th>
                            <th style="width: 12%;">ტელეფონი</th>
                            <th style="width: 9%;">User Id</th>
                            <th style="width: 12%;">ქვე კატეგორია</th>
                            <th style="width: 12%;">სახელი,გვარი</th>
                            <th style="width: 12%;">პასუხისმგებელი პირი</th>
                            <th style="width: 12%;">დამფორმირებელი</th>
                            <th style="width: 13%;">სტატუსი</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                                <input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div style="display: none" id="control_tab_6_2" class="ex_highlight_row">
            <div id="dt_example">
                <div id="button_area" style="margin-top: 25px;">
                    <table>
                        <tr>
                            <td>
                                <label for="search_start_my" class="left" style="margin: 5px 0 0 15px;">დან-</label>
                            </td>
                            <td>
                                <input style="width: 100px; height: 15px;"
                                       value="<?php echo date('Y-m-d') . " 00:00"; ?>" type="text" name="search_start"
                                       id="search_start_my_6" class="inpt left"/>
                            </td>
                            <td>
                                <label for="search_end_my" class="left" style="margin: 5px 0 0 15px;">მდე-</label>
                            </td>
                            <td>
                                <input style="width: 100px; height: 15px;"
                                       value="<?php echo date('Y-m-d') . " 23:59"; ?>" type="text" name="search_end"
                                       id="search_end_my_6" class="inpt right"/>
                            </td>
                            <td>
                                <button id="fillter6" style="margin-left: 10px;">ფილტრი</button>
                            </td>
                        </tr>
                    </table>
                    <div class="left" style="width: 160px;">


                    </div>
                    <div class="right" style="">


                    </div>
                </div>
                <table class="display" id="example6_2">
                    <thead>
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width:7%;">ID</th>
                            <th style="width: 12%;">თარიღი</th>
                            <th style="width: 12%;">ტელეფონი</th>
                            <th style="width: 9%;">User Id</th>
                            <th style="width: 12%;">ქვე კატეგორია</th>
                            <th style="width: 12%;">სახელი,გვარი</th>
                            <th style="width: 12%;">პასუხისმგებელი პირი</th>
                            <th style="width: 12%;">დამფორმირებელი</th>
                            <th style="width: 13%;">სტატუსი</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                                <input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                       style="width: 80%"/>
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>

    </div>
</div>


<!-- jQuery Dialog -->
<div id="add-edit-form" class="form-dialog" title="გამავალი ზარი">
    <!-- aJax -->
</div>
<div id="add-edit-form-mail" class="form-dialog" title="ახალი E-mail">
</div>
<!-- jQuery Dialog -->
<div id="add-edit-form-mail-shablon" class="form-dialog" title="E-mail შაბლონი">
</div>
<div  id="sms-templates" class="form-dialog" title="ესემეს შაბლონები">
</div>
<!-- jQuery Dialog -->
    <div id="add-edit-form-task" class="form-dialog" title="დავალება">
    </div>
<!-- jQuery Dialog -->
<div id="add-edit-form1" class="form-dialog" title="გამავალი ზარი">
    <!-- aJax -->
</div>
<div id="add-edit-auto-dialer" class="form-dialog" title="გამავალი ზარი">
    <!-- aJax -->
</div>
<!-- jQuery Dialog -->
<div id="add-edit-form2" class="form-dialog" title="გამავალი ზარი">
    <!-- aJax -->
</div>
<div id="add-edit-manual" class="form-dialog" title="გამავალი ზარი">
    <!-- aJax -->
</div>

<div id="add-responsible-person" class="form-dialog" title="პასუხისმგებელი პირი">
    <!-- aJax -->
</div>
<div style="" id="add_edit_file" class="form-dialog" title="ფაილები"></div>

<div id="add-edit-form-sms" class="form-dialog" title="ახალი ესემესი">
    <!-- jQuery Dialog -->
    <div id="audio_dialog" title="ჩანაწერი"></div>
    <input id="activeparent" type="hidden" value=""/>
    <input id="activechild" type="hidden" value=""/>
    <input id="confirm_update" type="hidden" value="">
    <input id="confirm_click" type="hidden" value="0">
    <a id="downloader" href="" style="display:none" target="_blank"></a>
</body>
<div  id="add-edit-form-answer" class="form-dialog" title="პასუხი">
</div>