<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">-->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>-->
<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>-->

<style>
    .btn {
        width: 120px;
        margin-right: 1%;
        height: 23px;
        border-width: 1px;
        cursor: pointer;
        margin: 0px;
        font-family: pvn;
        display: inline-block;
        line-height: 1.4;
        position: relative;
    }

    i {
        margin-left: 0px;
        text-align: right;
    }

    .row {
        display: flex;

    }


    * {
        box-sizing: border-box
    }

    .container {
        width: 100%;
        background-color: #ddd;
    }

    .skills {
        text-align: right;
        padding: 1px;
        /*color: white;*/
    }

    #example {
        width: 100%;
    }
</style>
<script type="text/javascript">
    var aJaxURL = "server-side/call/auto_dialer_action.php";
    var aJaxcreatexcel = "includes/writexlsx.php";
    var aJaxupexcel = "includes/readxlsx.php";
    var upJaxURL = "server-side/call/upload_action.php";
    var tName = "example";
    var Call_Table_Name = "example_call";
    var Details_Table_Name = "example_details";
    var File_Table_Name = "example_all_file";
    var Add_Base_Table_Name = "example_base";
    var Voice_Table_Name = "example_voice";
    var Week_Time_Name = "example_week_time";
    var Position_Table_Name = "example_position";
    var P_fName = "add_edit_position";
    var V_fName = "add_edit_voice";
    var fName = "add-edit-form";
    var C_fName = "add_edit_call";
    var F_fName = "add_edit_file";
    var B_fName = "add_edit_base";
    var AB_fName = "add_add_edit_base";
    var W_fName = "add_edit_week_time";
    var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";

    var file;
    var id;
    var files;
    var name;
    var path = "../../media/upload_voice/";
    var path1 = "../../media/auto_dialer_all_file/";

    var ext;

    //  ნომრების ბაზაში ინპუთის თიპის შემოწმება
    function input_number_type_check(val, id) {
        // var a = parseInt(val)
        // if (!Number.isInteger(a)) {
        //     alert("შეიტანეთ მხოლოდ რიცხვი!");
        //     document.getElementById(id).value = '';
        //
        // }
    }

    function input_type_check(val, id) {

        var a = parseInt(val);
        if (eval(id) == document.getElementById(id).value) {
            document.getElementById(id).value = eval(id);
        } else {
            $.ajax({
                url: aJaxURL,
                type: "POST",
                data: "act=check_code&code=" + val,
                dataType: "json",
                success: function(data) {
                    if (data.error != "") {
                        alert("შეიყვანეთ უნიკალური კოდი!");
                        document.getElementById(id).value = "";
                    }
                }
            });


        }


    }


    $(document).on("click", "#check-all-call", function() {

        $("#example_call" + " INPUT[type='checkbox']").prop("checked", $("#check-all-call").is(":checked"));
    });
    $(document).on("click", "#check-all-details", function() {

        $("#example_details" + " INPUT[type='checkbox']").prop("checked", $("#check-all-details").is(":checked"));
    });

    $(document).on("click", "#play_btn", function() {

        var data = $(".check:checked").map(function() {
            return this.value;
        }).get();


        for (var i = 0; i < data.length; i++) {
            $.ajax({
                url: aJaxURL,
                type: "POST",
                data: "act=play&id=" + data[i],
                dataType: "json",
                success: function(data) {
                    if (data.error != "") {
                        alert(data.error);

                        LoadTable();
                        $("#" + check).attr("checked", false);

                    }
                }
            });
        }

    });

    $(document).on("click", "#stop_btn1", function() {

        var data = $(".check:checked").map(function() {
            return this.value;
        }).get();


        for (var i = 0; i < data.length; i++) {
            $.ajax({
                url: aJaxURL,
                type: "POST",
                data: "act=paus&id=" + data[i],
                dataType: "json",
                success: function(data) {
                    if (data.error != "") {
                        alert(data.error);

                        LoadTable();
                        $("#" + check).attr("checked", false);

                    }
                }
            });
        }

    });

    $(document).on("click", "#stop_btn", function() {

        var data = $(".check:checked").map(function() {
            return this.value;
        }).get();


        for (var i = 0; i < data.length; i++) {
            $.ajax({
                url: aJaxURL,
                type: "POST",
                data: "act=stop&id=" + data[i],
                dataType: "json",
                success: function(data) {
                    if (data.error != "") {
                        alert(data.error);

                        LoadTable();
                        $("#" + check).attr("checked", false);

                    }
                }
            });
        }

    });

    $(document).ready(function() {


        LoadTable();

        GetButtons("add_button", "delete_main_table");

        SetEvents("add_button", "delete_main_table", "check-all", tName, fName, aJaxURL, "&tb_name=autocall_request");


    });

    function LoadTable() {

        /* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
        GetDataTable(tName, aJaxURL, "get_list", 8, "", 0, [
            [10, 30, 50, -1],
            [10, 30, 50, "ყველა"]
        ], 0, "desc", "", change_colum_main);

        setTimeout(function() {
            $('.ColVis, .dataTable_buttons').css('display', 'none');
        }, 90);
    }

    function Load_Table_call(colum_number) {

        /* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
        GetDataTable(Call_Table_Name, aJaxURL, "get_list_call", colum_number, "request_table_id=" + $("#request_table_id").val() + "&details_id=" + $("#details_id").val(), 0, [
            [-1, -1, -1, -1],
            [10, 30, 50, "ყველა"]
        ], 1, "desc", "", '');

        setTimeout(function() {
            $('.ColVis, .dataTable_buttons').css('display', 'none');
        }, 90);
    }

    function Load_Table_details() {

        /* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
        GetDataTable(Details_Table_Name, aJaxURL, "get_list_details", 5, "request_table_id=" + $("#request_table_id").val(), 0, [
            [-1, -1, -1, -1],
            [10, 30, 50, "ყველა"]
        ], 1, "desc", "", '');

        setTimeout(function() {
            $('.ColVis, .dataTable_buttons').css('display', 'none');
        }, 90);
    }

    function Load_Table_voice() {

        /* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
        GetDataTable(Voice_Table_Name, aJaxURL, "get_list_voice", 4, "request_table_id=" + $("#request_table_id").val(), 0, "", 3, "asc", "", '');

        setTimeout(function() {
            $('.ColVis, .dataTable_buttons').css('display', 'none');
        }, 90);
    }

    function Load_Table_week_time() {

        /* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
        GetDataTable(Week_Time_Name, aJaxURL, "get_list_week_time", 4, "request_table_id=" + $("#request_table_id").val(), 0, "", 1, "desc", "", '');

        setTimeout(function() {
            $('.ColVis, .dataTable_buttons').css('display', 'none');
        }, 90);
    }

    function Load_Table_all_file() {

        /* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
        GetDataTable(File_Table_Name, aJaxURL, "get_list_all_file", 3, "request_table_id=" + $("#request_table_id").val(), 0, "", 1, "desc", "", '');

        setTimeout(function() {
            $('.ColVis, .dataTable_buttons').css('display', 'none');
        }, 90);
    }

    function Load_Table_Position() {

        /* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
        GetDataTable(Position_Table_Name, aJaxURL, "get_list_position", 2, "request_table_id=" + $("#request_table_id").val(), 0, "", 1, "desc", "", '');

        setTimeout(function() {
            $('.ColVis, .dataTable_buttons').css('display', 'none');
        }, 90);
    }

    function LoadDialog(form) {
        if (form == fName) {


            var button_modal = {
                "save": {
                    text: "შენახვა",
                    id: "save_modal"
                },
                "cancel": {
                    text: "დახურვა",
                    id: "cancel-dialog",
                    click: function() {
                        $(this).dialog("close");
                    }
                }
            }
            GetDialog(fName, 1100, "auto", button_modal, "");
            // GetDate('datetime1');
            GetDateTimes2('start_datetime');
            GetDateTimes2('end_datetime');


            Load_Table_week_time();

            Load_Table_all_file();
            Load_Table_details();
            Load_Table_voice();

            Load_Table_Position();

            GetButtons("", "delete_button_all_file");
            SetEvents("", "delete_button_all_file", "check-all-file", File_Table_Name, F_fName, aJaxURL, "file_id=" + $("#request_table_id").val() + "&tb_name= autocall_request_file");


            GetButtons("get_button_base", "delete_button_details");
            SetEvents("get_button_base", "delete_button_details", "check-all-base", Details_Table_Name, B_fName, aJaxURL, "base_id=" + $("#request_table_id").val() + "&details_id=" + $("#details_id").val() + "&tb_name= autocall_request_details&call_type_id=" + $("#call_type_id").val());


            GetButtons("add_button_voice", "delete_button_voice");
            SetEvents("add_button_voice", "delete_button_voice", "check-all-voice", Voice_Table_Name, V_fName, aJaxURL, "voice_id=" + $("#request_table_id").val() + "&tb_name= autocall_request_voice");

            GetButtons("add_button_week_time", "delete_button_week_time");
            SetEvents("add_button_week_time", "delete_button_week_time", "check-all-week-time", Week_Time_Name, W_fName, aJaxURL, "week_id=" + $("#request_table_id").val() + "&tb_name= autocall_request_dates");
            GetButtons("add_button_position", "delete_button_position");
            SetEvents("add_button_position", "delete_button_position", "check-all-position", Position_Table_Name, P_fName, aJaxURL, "position_id=" + $("#request_table_id").val() + "&tb_name= autocall_request_positions");

        } else if (form == C_fName) {
            var button_modal = {
                "save": {
                    text: "შენახვა",
                    id: "save_call"
                },
                "cancel": {
                    text: "დახურვა",
                    id: "cancel-dialog",
                    click: function() {
                        $(this).dialog("close");
                    }
                }
            }

            GetDialog(C_fName, 400, "auto", button_modal, "");


        } else if (form == V_fName) {
            var button_modal = {
                "save": {
                    text: "შენახვა",
                    id: "save_voice"
                },
                "cancel": {
                    text: "დახურვა",
                    id: "cancel-dialog",
                    click: function() {
                        $(this).dialog("close");
                    }
                }
            }

            GetDialog(V_fName, 400, "auto", button_modal, "");
        } else if (form == W_fName) {
            var button_modal = {
                "save": {
                    text: "შენახვა",
                    id: "save_week_time"
                },
                "cancel": {
                    text: "დახურვა",
                    id: "cancel-dialog",
                    click: function() {
                        $(this).dialog("close");
                    }
                }
            }

            GetDialog(W_fName, 300, "auto", button_modal, "");
        } else if (form == P_fName) {
            var button_modal = {
                "save": {
                    text: "შენახვა",
                    id: "save_position"
                },
                "cancel": {
                    text: "დახურვა",
                    id: "cancel-dialog",
                    click: function() {
                        $(this).dialog("close");
                    }
                }
            }

            GetDialog(P_fName, 200, "auto", button_modal, "");
        } else if (form == B_fName) {
            var button_modal = {

                "cancel": {
                    text: "დახურვა",
                    id: "cancel-dialog",
                    click: function() {
                        $(this).dialog("close");
                    }
                }
            }
            if (document.getElementById("example_voice").getElementsByTagName("tbody")[0].getElementsByTagName("td").length < 2) {
                alert("დაამატეთ გამავალი ზარი!");

                document.getElementById("voice_bt").click();

            } else {
                GetButtons("add_button_base", "delete_button_base");

                GetDialog(B_fName, 900, "auto", button_modal, "");

                var count = 0;
                for (var i = 2; i <= document.getElementById("example_voice").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length + 1; i++) {

                    if ($('#example_voice').find(`tr:eq(${i})`).find("td:eq(2)").html() == "ფაილი") {
                        count++;

                    }

                }

                Load_Table_call(document.getElementById("example_voice").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length + 2 - count);
                SetEvents("add_button_base", "delete_button_base", "check-all-call", Call_Table_Name, AB_fName, aJaxURL, "add_base_id=" + $("#request_table_id").val() + "&details_id=" + $("#details_id").val() + "&tb_name= autocall_request_base");


            }


        } else if (form == AB_fName) {
            var button_modal = {
                "save": {
                    text: "შენახვა",
                    id: "save_call"
                },
                "cancel": {
                    text: "დახურვა",
                    id: "cancel-dialog",
                    click: function() {
                        $(this).dialog("close");
                    }
                }
            }

            GetDialog(AB_fName, 300, "auto", button_modal, "");
        } else if (form == F_fName) {
            var button_modal = {

                "cancel": {
                    text: "დახურვა",
                    id: "cancel-dialog",
                    click: function() {
                        $(this).dialog("close");
                    }
                }

            }
            GetDialog(F_fName, "auto", "auto", button_modal, "left+300 top+50");

        }


    }

    $(document).on("click", "#exp_button_call", function() {

        param = new Object();
        param.act = "create_excel";
        param.request_id = $("#request_table_id").val();

        $.ajax({
            url: aJaxcreatexcel,
            data: param,
            success: function(data) {
                if (typeof(data.error) != 'undefined') {


                    document.getElementById('download_excel').click();

                }
            }
        });


    });

    $(document).on("click", "#exp_button_code", function() {


        param = new Object();
        param.act = "create_excel_code";


        $.ajax({
            url: aJaxcreatexcel,
            data: param,
            success: function() {


                document.getElementById('download_excel_code').click();


            }
        });


    });


    $(document).on("click", "#voice_file_up", function() {
        $("#choose_voice").click();
    });
    $(document).on("click", "#delete_file", function() {

        $("#choose_voice").val('');
        document.getElementById('voice_audio').innerHTML = '<div style="padding-bottom: 5px;"><span id="file_drop">ფაილი არ არის არჩეული!</span><button style="margin-left: 45%;  text-align: left; cursor: pointer;" id="delete_file"><span class="ui-icon ui-icon-close"></span></button>\n' +
            '                         </div> ';


    });
    $(document).on("change", "#choose_voice", function() {
        file = $(this).val();
        id = $('#id').val();
        files = this.files;
        name = uniqid();
        path = "../../media/upload_voice/";
        ext = file.split('.').pop().toLowerCase();


        file_name = files.name;
        rand_file = name + "." + ext;
        if ($.inArray(ext, ['wav', 'gsm', 'mp3']) == -1) { //echeck file type
            alert('აირჩიეთ wav, gsm ან mp3 ფაილი!');
            this.value = '';
        } else {

            $.ajaxFileUpload({
                url: upJaxURL,
                secureuri: false,
                fileElementId: "choose_voice",
                dataType: 'json',
                data: {
                    act: "upload_voice",
                    path: path,
                    file_name: name,
                    type: ext
                },
                success: function(data, status) {
                    if (typeof(data.error) != 'undefined') {
                        if (data.error != '') {
                            alert(data.error);
                        }
                    }
                    var name = data.name;

                    document.getElementById('voice_audio').innerHTML = '<div><div  style="padding-bottom: 5px;" ><span id="file_drop"> ' + `${name}` + ' </span><button style="margin-left: 45%;  text-align: left; cursor: pointer;" id="delete_file"><span class="ui-icon ui-icon-close"></span></button>\n' +
                        '                         </div>\n' +
                        '                        \n' +
                        '                        <audio id="myAudio" controls>\n' +
                        '                          <source src="media/upload_voice/' + `${name}` + '" type="audio/ogg">\n' +
                        '                          <source src="media/upload_voice/' + `${name}` + '" type="audio/mpeg">\n' +
                        '                           Your browser does not support the audio element.\n' +
                        '                        </audio>';

                },
                error: function(data, status, e) {
                    alert(e);
                }
            });
        }


    });

    $(document).on("click", "#all_file_up", function() {

        $("#choose_all_file").click();
    });
    $(document).on("change", "#choose_all_file", function() {

        // var file_url  = $(this).val();
        // var file_name = this.files[0].name;
        // var file_size = this.files[0].size;
        // var file_type = file_url.split('.').pop().toLowerCase();
        // var path	  = "../../media/uploads/file/";

        var file = $(this).val();
        var id = $('#id').val();
        var files = this.files;
        var name = uniqid();
        var path1 = "../../media/auto_dialer_all_file/";
        var ext = file.split('.').pop().toLowerCase();
        var file_type = 'ფაილი';

        param = new Object();

        param.name = $("#name").val();
        param.call_type_id = $("#call_type_id").val();
        param.start_datetime = $("#start_datetime").val();
        param.end_datetime = $("#end_datetime").val();
        param.duration = $("#duration").val();
        param.retry_time = $("#retry_time").val();
        param.answer_wait_time = $("#answer_wait_time").val();
        param.max_retries = $("#max_retries").val();
        param.max_sum_listen_time = $("#max_sum_listen_time").val();
        param.comment_main = $("#comment_main").val();
        param.note = $("#note").val();
        param.code = $("#code").val();
        param.done_listen_time = $("#done_listen_time").val();
        param.request_table_id = $("#request_table_id").val();
        param.channel_quantity = $("#channel_quantity").val();

        if ($("#call_type_id").val() == 4 && (param.name == "" || param.start_datetime == "" || param.end_datetime == "" || param.duration == "" || param.code == "")) {

            alert("შეავსეთ ყველა  ველი!");

            document.getElementById("defaultOpen").click();

        } else if ($("#call_type_id").val() != 4 && (param.name == "" || param.call_type_id == 0 || param.start_datetime == "" || param.end_datetime == "" || param.duration == "" || param.retry_time == "" || param.answer_wait_time == "" || param.max_retries == "" || param.max_sum_listen_time == "" || param.code == "" || param.done_listen_time == "" || param.channel_quantity == "")) {
            alert("შეავსეთ ყველა  ველი!");
            document.getElementById("defaultOpen").click();
        } else {

            if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg', 'bmp', 'tiff', 'svg']) != -1) {
                file_type = 'სურათი';

            } else if ($.inArray(ext, ['mov', 'mpeg', 'mp4', 'avi', 'mpg', 'wma', "flv", "webm"]) != -1) {
                file_type = 'ვიდეო';
            } else if ($.inArray(ext, ['mp3', 'm4a', 'ac3', 'aiff', 'mid', 'ogg', 'wav']) != -1) {
                file_type = 'აუდიო';
            }


            file_name = files.name;
            rand_file = name + "." + ext;

            $.ajaxFileUpload({
                url: upJaxURL,
                secureuri: false,
                fileElementId: "choose_all_file",
                dataType: 'json',
                data: {
                    act: "upload_all_file",
                    path: path1,
                    file_name: name,
                    type: ext
                },
                success: function(data, status) {
                    if (typeof(data.error) != 'undefined') {
                        if (data.error != '') {
                            alert(data.error);
                        }
                    }
                    param.act = 'upload_all_file';
                    param.request_id = $("#request_table_id").val();
                    param.file_name = data.name;
                    param.file_type = file_type;
                    $.ajax({
                        url: aJaxURL,
                        data: param,
                        success: function(data) {
                            if (typeof(data.error) != 'undefined') {
                                if (data.error != '') {
                                    alert(data.error);
                                } else {

                                    Load_Table_all_file();

                                }
                            }
                        }
                    });
                },
                error: function(data, status, e) {
                    alert(e);
                }
            });
        }
    });


    $(document).on("click", "#up_button_code", function() {
        $("#choose_file_code").click();
    });
    $(document).on("change", "#choose_file_code", function() {
        var file = $(this).val();
        var id = $('#id').val();
        var files = this.files;
        var name = uniqid();
        var path = "../../includes/";
        var ext = file.split('.').pop().toLowerCase();
        if ($.inArray(ext, ['xls', 'pptx', 'xlsx', 'csv']) == -1) { //echeck file type
            alert('This is not an allowed file type.');
            this.value = '';
        } else {
            file_name = files.name;
            rand_file = name + "." + ext;
            $.ajaxFileUpload({
                url: upJaxURL,
                secureuri: false,
                fileElementId: "choose_file_code",
                dataType: 'json',
                data: {
                    act: "upload_file_code",
                    path: path,
                    file_name: name,
                    type: ext
                },
                success: function(data, status) {
                    if (typeof(data.error) != 'undefined') {
                        if (data.error != '') {
                            alert(data.error);
                        }
                    }
                    param = new Object();

                    param.act = 'upload_excel_code';
                    param.file_name = data.name;

                    $.ajax({
                        url: aJaxupexcel,
                        data: param,
                        success: function(data) {
                            // alert("ფაილი აიტვირთა!");
                            if (data.er != '') {
                                alert(data.er);
                            } else {
                                location.reload();
                            }


                        }
                    });
                },
                error: function(data, status, e) {
                    alert(e);
                }
            });
        }
    });


    $(document).on("click", "#up_button_call", function() {
        $("#choose_file").click();
    });
    $(document).on("change", "#choose_file", function() {
        var file = $(this).val();
        var id = $('#id').val();
        var files = this.files;
        var name = uniqid();
        var path = "../../includes/";

        var ext = file.split('.').pop().toLowerCase();
        if ($.inArray(ext, ['xls', 'pptx', 'xlsx', 'csv']) == -1) { //echeck file type
            alert('This is not an allowed file type.');
            this.value = '';
        } else {
            file_name = files.name;
            rand_file = name + "." + ext;
            $.ajaxFileUpload({
                url: upJaxURL,
                secureuri: false,
                fileElementId: "choose_file",
                dataType: 'json',
                data: {
                    act: "upload_file",
                    path: path,
                    file_name: name,
                    type: ext
                },
                success: function(data, status) {
                    if (typeof(data.error) != 'undefined') {
                        if (data.error != '') {
                            alert(data.error);
                        }
                    }
                    param = new Object();

                    param.act = 'upload_excel';
                    param.file_name = data.name;
                    param.request_id = $("#details_id").val();
                    param.request_table_id = $("#request_table_id").val();


                    $.ajax({
                        url: aJaxupexcel,
                        data: param,
                        success: function(data) {

                            if (data.er != '') {
                                alert(data.er);
                                Load_Table_details();

                            } else {
                                var count = 0;
                                for (var i = 2; i <= document.getElementById("example_voice").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length + 1; i++) {

                                    if ($('#example_voice').find(`tr:eq(${i})`).find("td:eq(2)").html() == "ფაილი") {
                                        count++;

                                    }

                                }

                                Load_Table_call(document.getElementById("example_voice").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length + 2 - count);

                                Load_Table_details();

                            }


                        }
                    });
                },
                error: function(data, status, e) {
                    alert(e);
                }
            });
        }
    });

    $(document).on("click", "#delete_button_details", function() {
        Load_Table_details();
    });
    $(document).on("click", "#delete_button_all_file", function() {
        Load_Table_all_file();
    });

    $(document).on("click", "#delete_button_base", function() {


        var count = 0;
        for (var i = 2; i <= document.getElementById("example_voice").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length + 1; i++) {

            if ($('#example_voice').find(`tr:eq(${i})`).find("td:eq(2)").html() == "ფაილი") {
                count++;

            }

        }

        Load_Table_call(document.getElementById("example_voice").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length + 2 - count);

        Load_Table_details();

    });

    $(document).on("click", "#delete_button_call", function() {

        param = new Object();

        param.act = 'disable';
        param.tb_name = 'autocall_request_details';


        $.ajax({
            url: aJaxURL,
            data: param,
            success: function(data) {
                if (typeof(data.error) != 'undefined') {
                    if (data.error != '') {
                        alert(data.error);
                    } else {

                        var count = 0;
                        for (var i = 2; i <= document.getElementById("example_voice").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length + 1; i++) {

                            if ($('#example_voice').find(`tr:eq(${i})`).find("td:eq(2)").html() == "ფაილი") {
                                count++;

                            }

                        }

                        Load_Table_call(document.getElementById("example_voice").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length + 2 - count);


                    }
                }
            }
        });
    });

    // $(document).on("click", "#detail_bt", function () {
    //
    //     param = new Object();
    //     param.request_table_id = $("#request_table_id").val();
    //     param.act = 'call_table';
    //     $.ajax({
    //         url: aJaxURL,
    //         data: param,
    //         success: function (data) {
    //             if (typeof (data.error) != 'undefined') {
    //                 if (data.error != '') {
    //                     alert(data.error);
    //                 } else {
    //                     Load_Table_call(13);
    //
    //
    //                     document.getElementById("datatable_header_call1").innerHTML = data.data_call_table1;
    //                     document.getElementById("datatable_header_call2").innerHTML = data.data_call_table2;
    //                     var count = 0;
    //                     for (var i = 2; i <= document.getElementById("example_voice").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length + 1; i++) {
    //
    //                         if ($('#example_voice').find(`tr:eq(${i})`).find("td:eq(2)").html() == "ფაილი") {
    //                             count++;
    //
    //                         }
    //
    //                     }
    //
    //
    //                     Load_Table_call(document.getElementById("example_voice").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length + 2 - count);
    //
    //                 }
    //             }
    //         }
    //     });
    //
    // });
    $(document).on("click", "#get_button_base", function() {

        if (document.getElementById("example_voice").getElementsByTagName("tbody")[0].getElementsByTagName("td").length < 2) {


        } else {

            Load_Table_call(13);


            var count = 0;
            for (var i = 2; i <= document.getElementById("example_voice").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length + 1; i++) {

                if ($('#example_voice').find(`tr:eq(${i})`).find("td:eq(2)").html() == "ფაილი") {
                    count++;

                }

            }


            Load_Table_call(document.getElementById("example_voice").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length + 2 - count);

        }
    });

    $(document).on("click", "#delete_button_voice", function() {

        Load_Table_voice();

    });
    $(document).on("click", "#delete_button_position", function() {

        Load_Table_Position();

    });
    $(document).on("click", "#delete_button_week_time", function() {

        Load_Table_week_time();

    });

    $(document).on("click", "#delete_main_table", function() {

        LoadTable();

    });

    $(document).on("click", "#save_position", function() {

        param = new Object();


        param.act = "save_position";
        param.position = $("#position_position").val();

        param.request_position_id = $("#request_position_id").val();

        param.name = $("#name").val();
        param.call_type_id = $("#call_type_id").val();
        param.start_datetime = $("#start_datetime").val();
        param.end_datetime = $("#end_datetime").val();
        param.duration = $("#duration").val();


        param.retry_time = $("#retry_time").val();
        param.answer_wait_time = $("#answer_wait_time").val();
        param.max_retries = $("#max_retries").val();
        param.max_sum_listen_time = $("#max_sum_listen_time").val();
        param.comment_main = $("#comment_main").val();
        param.note = $("#note").val();
        param.code = $("#code").val();
        param.done_listen_time = $("#done_listen_time").val();
        param.request_table_id = $("#request_table_id").val();
        param.channel_quantity = $("#channel_quantity").val();

        if ($("#call_type_id").val() == 4 && (param.name == "" || param.start_datetime == "" || param.end_datetime == "" || param.duration == "" || param.code == "")) {

            alert("შეავსეთ ყველა  ველი!");
            CloseDialog(P_fName);
            document.getElementById("defaultOpen").click();

        } else if ($("#call_type_id").val() != 4 && (param.name == "" || param.call_type_id == 0 || param.start_datetime == "" || param.end_datetime == "" || param.duration == "" || param.retry_time == "" || param.answer_wait_time == "" || param.max_retries == "" || param.max_sum_listen_time == "" || param.code == "" || param.done_listen_time == "" || param.channel_quantity == "")) {
            alert("შეავსეთ ყველა  ველი!");
            CloseDialog(P_fName);
            document.getElementById("defaultOpen").click();
        } else if (param.position == "") {
            alert("შეავსეთ რიგის  ველი!");

        } else {
            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {
                    if (typeof(data.error) != 'undefined') {
                        if (data.error != '') {
                            alert(data.error);
                        } else {

                            Load_Table_Position();
                            CloseDialog(P_fName);
                        }
                    }
                }
            });
        }

    });


    $(document).on("click", "#save_week_time", function() {

        param = new Object();


        param.act = "save_week_time";
        param.week_day_id = $("#week_day_id").val();
        param.start_time = $("#start_time").val();
        param.end_time = $("#end_time").val();
        param.request_week_id = $("#request_week_id").val();

        param.name = $("#name").val();
        param.call_type_id = $("#call_type_id").val();
        param.start_datetime = $("#start_datetime").val();
        param.end_datetime = $("#end_datetime").val();
        param.duration = $("#duration").val();


        param.retry_time = $("#retry_time").val();
        param.answer_wait_time = $("#answer_wait_time").val();
        param.max_retries = $("#max_retries").val();
        param.max_sum_listen_time = $("#max_sum_listen_time").val();
        param.comment_main = $("#comment_main").val();
        param.note = $("#note").val();
        param.code = $("#code").val();
        param.done_listen_time = $("#done_listen_time").val();
        param.request_table_id = $("#request_table_id").val();
        param.channel_quantity = $("#channel_quantity").val();

        if ($("#call_type_id").val() == 4 && (param.name == "" || param.start_datetime == "" || param.end_datetime == "" || param.duration == "" || param.code == "")) {

            alert("შეავსეთ ყველა  ველი!");
            CloseDialog(W_fName);
            document.getElementById("defaultOpen").click();

        } else if ($("#call_type_id").val() != 4 && (param.name == "" || param.call_type_id == 0 || param.start_datetime == "" || param.end_datetime == "" || param.duration == "" || param.retry_time == "" || param.answer_wait_time == "" || param.max_retries == "" || param.max_sum_listen_time == "" || param.code == "" || param.done_listen_time == "" || param.channel_quantity == "")) {
            alert("შეავსეთ ყველა  ველი!");
            CloseDialog(W_fName);
            document.getElementById("defaultOpen").click();
        } else if (param.start_time == "" || param.end_time == "" || param.week_day_id == 0) {
            alert("შეავსეთ ყველა  ველი!");

        } else {
            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {
                    if (typeof(data.error) != 'undefined') {
                        if (data.error != '') {
                            alert(data.error);
                        } else if (data.week_error != '') {
                            alert(data.week_error);
                        } else {

                            Load_Table_week_time();
                            CloseDialog(W_fName);
                        }
                    }
                }
            });
        }

    });


    $(document).on("click", "#save_voice", function() {
        param = new Object();
        param.request_id = $("#request_table_id").val();

        // autocall_request_voice
        param.act = "save_voice";
        param.voice_type_id = $("#voice_type_id").val();
        param.request_voice_id = $("#request_voice_id").val();
        // param.value = $("#value").val();
        param.position = $("#position").val();
        param.voice_name = $("#voice_name").val();
        // autocall_request
        param.name = $("#name").val();
        param.call_type_id = $("#call_type_id").val();
        param.start_datetime = $("#start_datetime").val();
        param.end_datetime = $("#end_datetime").val();
        param.duration = $("#duration").val();


        param.retry_time = $("#retry_time").val();
        param.answer_wait_time = $("#answer_wait_time").val();
        param.max_retries = $("#max_retries").val();
        param.comment_main = $("#comment_main").val();
        param.note = $("#note").val();
        param.code = $("#code").val();
        param.done_listen_time = $("#done_listen_time").val();
        param.request_table_id = $("#request_table_id").val();
        param.channel_quantity = $("#channel_quantity").val();


        if ($("#call_type_id").val() == 4 && (param.name == "" || param.start_datetime == "" || param.end_datetime == "" || param.duration == "" || param.code == "")) {

            alert("შეავსეთ ყველა  ველი!");
            CloseDialog(V_fName);
            document.getElementById("defaultOpen").click();

        } else if ($("#call_type_id").val() != 4 && (param.name == "" || param.call_type_id == 0 || param.start_datetime == "" || param.duration == "" || param.end_datetime == "" || param.retry_time == "" || param.answer_wait_time == "" || param.max_retries == "" || param.max_sum_listen_time == "" || param.code == "" || param.done_listen_time == "" || param.channel_quantity == "")) {
            alert("შეავსეთ ყველა  ველი!");
            CloseDialog(V_fName);
            document.getElementById("defaultOpen").click();
        } else if (param.voice_name == "" || param.voice_type_id == "0" || param.position == "") {
            alert("შეავსეთ ყველა  ველი!");
        } else if (param.phone_number == "") {
            alert("შეავსეთ ნომერი  ველი!");
        } else if (document.getElementById('voice_type_id').value != 1) {
            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {
                    if (typeof(data.error) != 'undefined') {
                        if (data.error != '') {
                            alert(data.error);
                        } else {
                            $("#choose_voice").val(null);

                            Load_Table_voice();

                            CloseDialog(V_fName);


                        }
                    }
                }
            });
        } else if (!files || document.getElementById('file_drop').innerText == 'ფაილი არ არის არჩეული!') { //echeck file type
            alert('აირჩიეთ ფაილი!');
            this.value = '';
        } else {

            file_name = files.name;
            rand_file = name + "." + ext;


            param.file_name = rand_file;

            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {
                    if (typeof(data.error) != 'undefined') {
                        if (data.error != '') {
                            alert(data.error);
                        } else {
                            $("#choose_voice").val(null);
                            LoadTable();
                            Load_Table_voice();

                            CloseDialog(V_fName);


                        }
                    }
                }
            });

        }


    });

    $(document).on("click", "#save_call", function() {
        param = new Object();

        // autocall_request_details
        param.act = "save_call";
        param.phone_number = $("#phone_number").val();

        param.call_id = $("#call_id").val();
        param.details_id = $("#details_id").val();
        param.comment = $("#comment").val();
        param.number_1 = $("#number_1").val();
        param.number_2 = $("#number_2").val();
        param.number_3 = $("#number_3").val();
        param.number_4 = $("#number_4").val();
        param.number_5 = $("#number_5").val();
        param.number_6 = $("#number_6").val();
        param.number_7 = $("#number_7").val();
        param.number_8 = $("#number_8").val();
        param.number_9 = $("#number_9").val();
        param.number_10 = $("#number_10").val();
        // autocall_request
        param.name = $("#name").val();
        param.call_type_id = $("#call_type_id").val();
        param.start_datetime = $("#start_datetime").val();
        param.end_datetime = $("#end_datetime").val();
        param.duration = $("#duration").val();
        param.request_details_id = $("#request_details_id").val();


        param.retry_time = $("#retry_time").val();
        param.answer_wait_time = $("#answer_wait_time").val();
        param.max_retries = $("#max_retries").val();
        param.max_sum_listen_time = $("#max_sum_listen_time").val();
        param.comment_main = $("#comment_main").val();
        param.note = $("#note").val();
        param.code = $("#code").val();
        param.done_listen_time = $("#done_listen_time").val();
        param.request_table_id = $("#request_table_id").val();
        param.channel_quantity = $("#channel_quantity").val();

        if ($("#call_type_id").val() == 4 && (param.name == "" || param.start_datetime == "" || param.end_datetime == "" || param.duration == "" || param.code == "")) {

            alert("შეავსეთ ყველა  ველი!");
            CloseDialog(B_fName);
            CloseDialog(AB_fName);
            document.getElementById("defaultOpen").click();

        } else if ($("#call_type_id").val() != 4 && (param.name == "" || param.call_type_id == 0 || param.start_datetime == "" || param.end_datetime == "" || param.duration == "" || param.retry_time == "" || param.answer_wait_time == "" || param.max_retries == "" || param.max_sum_listen_time == "" || param.code == "" || param.done_listen_time == "" || param.channel_quantity == "")) {
            alert("შეავსეთ ყველა  ველი!");
            CloseDialog(AB_fName);
            CloseDialog(B_fName);
            document.getElementById("defaultOpen").click();
        } else if (document.getElementById("example_voice").getElementsByTagName("tbody")[0].getElementsByTagName("td").length < 2) {
            alert("დაამატეთ გამავალი ზარი!");
            document.getElementById("voice_bt").click();
            CloseDialog(AB_fName);
            CloseDialog(B_fName);
        } else if (param.phone_number == "") {
            alert("შეავსეთ ნომერი  ველი!");
        } else {
            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {


                    if (typeof(data.error) != 'undefined') {
                        if (data.error != '') {
                            alert(data.error);
                        } else {

                            LoadTable();
                            var count = 0;
                            for (var i = 2; i <= document.getElementById("example_voice").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length + 1; i++) {

                                if ($('#example_voice').find(`tr:eq(${i})`).find("td:eq(2)").html() == "ფაილი") {
                                    count++;

                                }

                            }

                            Load_Table_call(document.getElementById("example_voice").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length + 2 - count);

                            Load_Table_details();
                            CloseDialog(AB_fName);
                        }
                    }
                }
            });
        }

    });
    $(document).on("click", "#save_modal", function() {


        param = new Object();


        param.act = "save_modal";

        param.name = $("#name").val();
        param.call_type_id = $("#call_type_id").val();
        param.start_datetime = $("#start_datetime").val();
        param.end_datetime = $("#end_datetime").val();
        param.duration = $("#duration").val();


        param.retry_time = $("#retry_time").val();
        param.answer_wait_time = $("#answer_wait_time").val();
        param.max_retries = $("#max_retries").val();
        param.max_sum_listen_time = $("#max_sum_listen_time").val();
        param.comment_main = $("#comment_main").val();
        param.note = $("#note").val();
        param.code = $("#code").val();
        param.done_listen_time = $("#done_listen_time").val();
        param.request_table_id = $("#request_table_id").val();
        param.channel_quantity = $("#channel_quantity").val();

        if ($("#call_type_id").val() == 4 && (param.name == "" || param.start_datetime == "" || param.end_datetime == "" || param.duration == "" || param.code == "")) {

            alert("შეავსეთ ყველა  ველი!");

            document.getElementById("defaultOpen").click();

        } else if ($("#call_type_id").val() != 4 && (param.name == "" || param.call_type_id == 0 || param.start_datetime == "" || param.end_datetime == "" || param.duration == "" || param.retry_time == "" || param.answer_wait_time == "" || param.max_retries == "" || param.max_sum_listen_time == "" || param.code == "" || param.done_listen_time == "" || param.channel_quantity == "")) {
            alert("შეავსეთ ყველა  ველი!");
            document.getElementById("defaultOpen").click();
        } else {

            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {
                    if (typeof(data.error) != 'undefined') {
                        if (data.error != '') {
                            alert(data.error);
                        } else {
                            LoadTable();
                            //Load_Table_modal();
                            CloseDialog(fName);
                        }
                    }
                }
            });
        }


    });
</script>

<body>

    <a id="download_excel" download="outgoing_voice.xlsx" href="includes\outgoing_voice.xlsx"></a>
    <a id="download_excel_code" download="Campaign_Code_Template.xlsx" href="includes\Campaign_Code_Template.xlsx"></a>

    <div class="callapp_head" style="margin-left: 1.2%; margin-top: 10px">კამპანია
        <hr class="callapp_head_hr">
    </div>
    <div style=" margin-top: 10px; margin-left: 0% ">

        <div id="button_area" class="column" style=" margin-left: 1.2%; ">
            <table>
                <tbody>
                    <tr>
                        <td>
                            <button id="add_button">დამატება</button>
                        </td>
                        <td>
                            <button id="delete_main_table">წაშლა</button>
                        </td>

                        <td>

                            <button id="exp_button_code" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button" aria-disabled="false">
                                <span class="ui-button-icon-primary ui-icon ui-icon-arrowreturnthick-1-s"> </span>
                                <span class="ui-button-text">შაბლონის ჩამოტვირთვა</span>
                            </button>

                        </td>

                        <td>
                            <input id="choose_file_code" type="file" name="choose_file_code" class="input" style="display: none;">
                            <button id="up_button_code" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button" aria-disabled="false">
                                <span class="ui-button-icon-primary ui-icon ui-icon-arrowreturnthick-1-n"></span>
                                <span class="ui-button-text">ფაილის ატვირთვა</span>
                            </button>
                        </td>

                        <!--                <td>-->
                        <!--                    <button id="stop_btn1" class="btn btn-sm"-->
                        <!--                            style="border-color: orange; color: orange; background-color: white; font-weight: 400; margin-bottom:30px ">-->
                        <!--                        <img-->
                        <!--                                src="media\images\paus_outgoing.png"-->
                        <!--                                height="15"-->
                        <!--                                width="15"> დაპაუზება-->
                        <!--                    </button>-->
                        <!--                </td>-->
                        <!--                <td>-->
                        <!--                    <button id="play_btn" class="btn btn-sm"-->
                        <!--                            style="border-color: green; color: green; background-color: white; font-weight: 400; margin-bottom:30px ">-->
                        <!--                        <img-->
                        <!--                                src="media\images\play_outgoing.png"-->
                        <!--                                height="15"-->
                        <!--                                width="15"> დაწყება-->
                        <!--                    </button>-->
                        <!--                </td>-->
                        <!--                <td>-->
                        <!--                    <button class="btn btn-sm" id="stop_btn"-->
                        <!--                            style="border-color: red; color: red; background-color: white; font-weight: 400">-->
                        <!--                        <i><img-->
                        <!--                                    src="media\images\stop_outgoing.png"-->
                        <!--                            " height="15" width="15"></i>-->
                        <!--                        შეწყვეტა-->
                        <!--                    </button>-->
                        <!--                </td>-->
                        <!--                <td>-->
                        <!--                    <button id="delete_main_table" class="btn btn-sm"-->
                        <!--                            style="border-color: black; background-color: white; font-weight: 400">-->
                        <!--                        <i><img-->
                        <!--                                    src="media\images\trash_outgoing.png"-->
                        <!--                            " height="15" width="15"></i>-->
                        <!--                        წაშლა-->
                        <!--                    </button>-->
                        <!--                </td>-->


                    </tr>
                </tbody>
            </table>


        </div>


        <div id="tab" style="margin-left: 1.2%">
            <table class="display" id="example">
                <thead>
                    <tr id="datatable_header">

                        <th></th>
                        <th width="10%">დაწყების თარიღი</th>
                        <th width="10%">დასრულების თარიღი</th>
                        <th>კამპანიის დასახელება</th>
                        <th>კოდი</th>
                        <th>წყარო</th>

                        <th>კამპანიის ტიპი</th>

                        <th style="width: 10%">ატვირთვების რ-ბა</th>

                        <th class="check">#</th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>

                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>

                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>


                        <th>
                            <div class="callapp_checkbox">
                                <input type="checkbox" id="check-all" name="check-all" />
                                <label for="check-all"></label>
                            </div>
                        </th>
                    </tr>
                </thead>
            </table>

        </div>


    </div>
    <div id="add-edit-form" class="form-dialog" title="გამავალი ზარები">

    </div>
    </div>
    <div id="add_edit_call" class="form-dialog" title="ნომრები">

    </div>
    <div id="add_edit_voice" class="form-dialog" title="ჩანაწერები"></div>
    <div id="add_edit_week_time" class="form-dialog" title="ზარის პერიოდი"></div>
    <div id="add_edit_position" class="form-dialog" title="რიგი"></div>
    <div id="add_edit_base" class="form-dialog" title="ბაზა"></div>
    <div id="add_add_edit_base" class="form-dialog" title="ნომრების ბაზა"></div>
    <div style="" id="add_edit_file" class="form-dialog" title="ფაილები"></div>
</body>