
// define main class
const DefaulsActions = class {

    // define main constructor method
    constructor() {
        this.sessionStatus = 0;
        this.choosedSmsId = 0;
        this.chosedActivitieId = 0;
        this.activitieTimerInterval = '';
    }

    // zero fixer
    zeroFixer(a) {
        return a < 10 ? `0${a}` : a;
    }

    // time proccessor
    timeProccessor(allseconds) {

        let hours = 0;
        let minutes = 0;
        let seconds = 0;

        // calculate seconds
        seconds = allseconds % 60;

        // calculate minutes
        if(allseconds > 59) {
            minutes = parseInt(allseconds / 60);
        }

        // calculate hours
        if(minutes > 59) {
            hours = parseInt(minutes / 60);;
            minutes = minutes % 60;
        }

        // return value
        return {
            hours,
            minutes,
            seconds
        }

    }

    // activitie on timer
    activitieOnTimer(allseconds = 0) {

        // nullify time in receiver
        $(".activitie-time-counter span").text(`00:00:00`);

        // display screen disabler
        $(".activitie-screen-disabler").addClass("on");

        // stylize body
        $("body").css({overflow: "hidden"});

        // define currnet time data
        const date = new Date();
        const CurHour = date.getHours();
        const CurMinutes = date.getMinutes();
        const CurSeconds = date.getSeconds();

        // insert current time in receiver
        $(".activitie-cur-time").text(`${this.zeroFixer(CurHour)}:${this.zeroFixer(CurMinutes)}:${this.zeroFixer(CurSeconds)}`);

        // start time count
        this.activitieTimerInterval = setInterval(() => {

            // define counter time data
            const { hours, minutes, seconds } = this.timeProccessor(allseconds);

            // insert time in receiver
            $(".activitie-time-counter span").text(`${this.zeroFixer(hours)}:${this.zeroFixer(minutes)}:${this.zeroFixer(seconds)}`);

            // increment counter
            allseconds++;

        }, 1000);

    }


    // activitie off timer
    activitieOffTimer() {

        // clear activitie timer
        clearInterval(this.activitieTimerInterval);

        // dissapere screen disabler
        $(".activitie-screen-disabler").removeClass("on");

        // reset choose activitie
        this.resetChooseActivitie();

    }


    // reset choose activitie
    resetChooseActivitie() {

        // reactivate activitie in list
        $("#callapp_op_activitie_choose li").removeClass("is-active");
        $("#callapp_op_activitie_choose li:eq(0)").addClass("is-active");
        $(".callapp-op-activitie-choose-receiver").text("აირჩიეთ აქტივობა");

        // change color label
        $(".callapp-op-activitie-color").css({background: '#bdc3c7'});

    }

    // chat status off
    chatStatusOff(selector) {
        this.sessionStatus = 0;
        $(selector).val(1).attr("data-status", "off").data("status", "off");		
    }

    // chat status on
    chatStatusOn(selector) {
        this.sessionStatus = 1;
        $(selector).val(2).attr("data-status", "on").data("status", "on");			
    }

    // toggle chat status
    toggleChatStatus(selector, status) {

        // define session status
        this.sessionStatus = status === 1 ? 1 : 0;

        // define data status
        const dataStatus = status === 1 ? "on" : "off";

        // set chat status
        $(selector).val(status).attr("data-status", dataStatus).data("status", dataStatus);
    }

}

// define main use object
const $D = new DefaulsActions();
