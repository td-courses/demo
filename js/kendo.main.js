/*function getFilters(url, table, columnsCount){
	var d = '{"id":"column0","fields":[{"id":{"editable":true,"type":"string"}},{"image":{"editable":true,"type":"string"}},{"name":{"editable":true,"type":"string"}},{"surname":{"editable":true,"type":"string"}},{"age":{"editable":true,"type":"string"}}]}';
	var h = '{"id":"column0","fields":[{"id":{"editable":true,"type":"number"}},{"image":{"editable":true,"type":"number"}},{"name":{"editable":true,"type":"number"}},{"surname":{"editable":true,"type":"number"}},{"age":{"editable":true,"type":"number"}}]}';
	return JSON.parse(d);	
	
	param = new Object();

	param.act	= "get_filt";
	param.count    = columnsCount;
	
	$.ajax({
		url: url,
		data: param,
		success: function(data) {
			
			console.log(data);
			
		}
	});
}
*/
$.ajax({
	url: "server-side/view/person_kendo.action2.php?act=get_columns&count=7",
	dataType: "json",
	success: function(columns) {
		$.ajax({
			url: "server-side/view/person_kendo.action2.php?act=get_filt&count=7",
			dataType: "json",
			success: function(models) {
				
				LoadKendo('server-side/view/person_kendo.action2.php',5,6,columns,models);
			}
		});
	}
});




function LoadKendo(url, columnsCount, itemPerPage, columns, modelss){
		var crudServiceBaseUrl = url;
		var columns = columns;
		var modelss = modelss;
		dataSource = new kendo.data.DataSource({
			transport: {
				read:  {
					url: crudServiceBaseUrl + "?act=get_list",
					dataType: "json"
				},
				update: {
					url: crudServiceBaseUrl + "?act=save_priority",
					dataType: "json"
				},
				destroy: {
					url: crudServiceBaseUrl + "?act=disable",
					dataType: "json"
				},
				create: {
					url: crudServiceBaseUrl + "/Products/Create",
					dataType: "json"
				},
				
				parameterMap: function(data, options, operation) {
					
					if (operation !== "read" && options.models) {
						return {models: kendo.stringify(options.models)};
					}
					else {
						return {add: kendo.stringify(data)};
					}
				}
			},
			batch: true,
			pageSize: itemPerPage,
			
			schema: {
				model: {"id":"id","fields":{"id":{"editable":false,"nullable":true},"image":{"validation":{"required":true},"type":"date"},"name":{"editable":false,"type":"string"},"surname":{"validation":{"required":true},"type":"string"},"age":{"type":"number","validation":{"required":true,"min":10}},"sex_id":{"type":"string"}}},
				total: 'total',
				data: function (data) {
					return data.data;
				}
			},
			serverFiltering: true,
			serverPaging: true
		});
		$("#grid").kendoGrid({
			dataSource: dataSource,
			dataBound: function() {
			  var pageInfo = this.pager.element.find(".k-pager-info");
			  setTimeout(function() {
				$.ajax({
						url: url,
						data: 'act=get_total',
						success: function(data) {       
							pageInfo.append(' სულ: '+data.totales);
						}
				});
				
			  });
			},
			pdf: {
				allPages: true,
				avoidLinks: true,
				paperSize: "A4",
				margin: { top: "2cm", left: "1cm", right: "1cm", bottom: "1cm" },
				landscape: true,
				repeatHeaders: true,
				template: $("#page-template").html(),
				scale: 0.8
			},
			
			selectable: "single",
			persistSelection: true,
			height: 500,
			groupable: false,
			sortable: true,
			filterable: {
				mode: "row"
			},
			pageable: true,
			toolbar: ["create","pdf","excel"],
			columns: columns,
			editable: "inline"
		});
}