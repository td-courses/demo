class kendoUI{
	loadKendoUI(url, itemPerPage,columnsCount, gridName, actButtons, editType, columnNames, filtersCustomOperators, showOperatorsByColumns, selectorsForFilters){
		this.ajaxURL = url;
		this.itemPerPage = itemPerPage;
		this.columnsCount = columnsCount;
		this.gridName = gridName;
		this.actButtons = actButtons;
		this.editType = editType;
		
		if(filtersCustomOperators != ''){
			this.customOperators = JSON.parse(filtersCustomOperators);
		}
		
		
		//  LOADING COLUMNS AND MODELS BEGIN
		this.getColumnsURL = "server-side/view/person_kendo.action2.php?act=get_columns&count="+this.columnsCount;
		var self = this;
		$.ajax({
			url: this.getColumnsURL,
			data: {names: columnNames, operators: showOperatorsByColumns, selectors: selectorsForFilters},
			dataType: "json",
			success: function(kendoData) {
				self.generateGrid(kendoData);
			}
		});
		// LOADING COLUMNS AND MODELS END
	}
	
	generateGrid(kendoData){
		this.dataSource = new kendo.data.DataSource({
			//TODO CUSTOM TRANSPORT GET DATA
			transport: {
				read:  {
					url: this.ajaxURL + "?act=get_list&count="+this.columnsCount,
					dataType: "json"
				},
				update: {
					url: this.ajaxURL + "?act=save_priority",
					dataType: "json"
				},
				destroy: {
					url: this.ajaxURL + "?act=disable",
					dataType: "json"
				},
				create: {
					url: this.ajaxURL + "/Products/Create",
					dataType: "json"
				},
				
				parameterMap: function(data, options, operation) {
					
					if (operation !== "read" && options.models) {
						return {models: kendo.stringify(options.models)};
					}
					else {
						return {add: kendo.stringify(data)};
					}
				}
			},
			batch: true,
			pageSize: this.itemPerPage,
			
			schema: {
				model: kendoData.modelss,
				total: 'total',
				data: function (data) {
					return data.data;
				}
			},
			serverFiltering: true,
			serverPaging: true
		});
		$("#"+this.gridName).kendoGrid({
			dataSource: this.dataSource,
			pdf: {
				allPages: true,
				avoidLinks: true,
				paperSize: "A4",
				margin: { top: "2cm", left: "1cm", right: "1cm", bottom: "1cm" },
				landscape: true,
				repeatHeaders: true,
				template: $("#page-template").html(),
				scale: 1
			},
			
			selectable: "multiple",
			allowCopy: true,
			persistSelection: true,
			height: 500,
			sortable: true,
			filterable: {
				operators: this.customOperators,
				mode: "row"
			},
			pageable: true,
			toolbar: this.actButtons,
			columns: kendoData.columnss,
			editable: this.editType
		});

	}


	
}

