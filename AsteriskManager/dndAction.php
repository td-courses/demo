<?php
require("config.php");
require("asmanager.php");

session_start();

$am = new AsteriskManager();
$am->connect($manager_host,$manager_user,$manager_secret);

$action     = $_REQUEST[action];
$extension  = $_SESSION[EXTENSION];

if ($action == 'dndon') {
    $ext = 'Local/'.$extension.'@from-queue/n';
    print_r($am->LocalCallOff($ext));
    
    echo json_encode($am->DNDON($extension));
    
} else if($action == 'dndoff') {
    $ext = 'Local/'.$extension.'@from-queue/n';
    print_r($am->LocalCallOff($ext));
    
    echo json_encode($am->DNDOFF($extension));
}elseif ($action == 'local'){
    $ext = 'Local/'.$extension.'@from-queue/n';
    echo json_encode($am->LocalCall($ext));
}

$data = array('status' => 1);

echo json_encode($data)
?>