<?php
 
$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$begintime = $time;
$inuse      = Array();
$dict_queue = Array();
$filter_queues = array("2392200");
require_once '../includes/classes/core.php';
require("config.php");
require("asmanager.php");
require("realtime_functions.php");

if(isset($_SESSION['QSTATS']['hideloggedoff'])) {
    $ocultar= $_SESSION['QSTATS']['hideloggedoff'];
} else {
    $ocultar="false";
}
if(isset($_SESSION['QSTATS']['filter'])) {
    $filter= $_SESSION['QSTATS']['filter'];
} else {
    $filter="";
}

$am=new AsteriskManager();
$am->connect($manager_host,$manager_user,$manager_secret);

$channels = get_channels ($am);
foreach($channels as $ch=>$chv) {
  list($chan,$ses) = split("-",$ch,2);
  $inuse["$chan"]=$ch;
}

$queues   = get_queues   ($am,$channels);

foreach ($queues as $key=>$val) {
  $queue[] = $key;
}

$color['unavailable']="flesh_off.png";
$color['unknown']="#dadada";
$color['busy']="flesh_inc.png";
$color['dialout']="#d0303f";
$color['ringing']="flesh_ringing.png";
$color['not in use']="flesh_free.png";
$color['paused']="#000000";

$off=0;
$free=0;
$use=0;
$que=0;
$chat=0;
foreach($filter_queues  as $qn) {
	if($filter=="" || stristr($qn,$filter)) {
		$contador=1;
		if(!isset($queues[$qn]['members'])) continue;
		foreach($queues[$qn]['members'] as $key=>$val) {
		    
		    $aval = $queues[$qn]['members'][$key]['type'];
		    
            if($aval == "unavailable" || $aval == "unknown") {
		        $off++;
		    } else if($aval == "busy") {
		        $use++;
		        
		    } else {
		        $free++;
		    }
		}

	}
	if(!isset($queues[$qn]['calls']))  continue;
	
	foreach($queues[$qn]['calls'] as $key=>$val) {
	    $que++;
	}
}
$chat_call_all = mysql_fetch_array(mysql_query("SELECT COUNT(id) FROM `crystal_users` WHERE logged = 1"));
$chat_res = mysql_fetch_array(mysql_query("SELECT COUNT(id) FROM `crystal_users` WHERE logged = 1 AND `online` = 1"));
$chat_off = mysql_fetch_array(mysql_query("SELECT COUNT(id) FROM `crystal_users` WHERE logged = 1 AND `online` = 0 AND last_extension > 0;"));
$chat_uss = mysql_fetch_array(mysql_query("SELECT COUNT(DISTINCT crystal_users.id) FROM `crystal_users` JOIN chat ON crystal_users.id = chat.answer_user_id AND chat.`status` = 2 WHERE logged = 1 AND `online` = 1;"));
$chat_que = mysql_fetch_array(mysql_query("SELECT COUNT(id) FROM `chat` WHERE `status` = 1;"));
$chat_use = mysql_fetch_array(mysql_query("SELECT COUNT(id) FROM `chat` WHERE `status` = 2;"));
$chat   = (intval($chat_res[0])-intval($chat_uss[0]));
$chat_q = intval($chat_que[0]);
$chat_u = intval($chat_use[0]);
$chat_o = intval($chat_off[0]);
$chat_s = intval($chat_uss[0]);
$cc_all = intval($chat_call_all[0]);
$data = array('off'=>$off,'free'=>$free,'use'=>$use,'que'=>$que,'chat'=>$chat,'chat_que'=>$chat_q,'chat_use'=>$chat_u,'chat_off'=>$chat_o,'chat_in_use'=>$chat_s,'cc_all'=>$cc_all);
echo json_encode($data);
?>

