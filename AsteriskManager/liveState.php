<?php

require_once '../includes/classes/class.Mysqli.php';
global $db;
$db = new dbClass();
//require("config.php");
//require("asmanager.php");
//require("realtime_functions.php");

$user           = $_SESSION['USERID'];
$group          = $_SESSION['GROUPID'];
$checkState     = $_REQUEST['checkState'];
$whoami         = $_REQUEST['whoami'];
$extensions     = Array();
$inuse          = Array();
$data           = Array();
$filter_queues  = Array("2392200");
$dnd_status     = 0;
$phone_icon     = '';


//$am             = new AsteriskManager();

$db->setQuery(" SELECT    user_info.name AS name,
				          `extention`.extention AS `last_extension`
                FROM     `users`
                JOIN      user_info ON user_info.user_id = users.id
                LEFT JOIN extention ON users.extension_id = extention.id
                LEFT JOIN work_activities ON work_activities.id = users.work_activities_id
                WHERE     logged = 1 AND extension_id != 0");

$res = $db->getResultArray();

foreach ($res[result] AS $aRow){
    $extensions[$aRow[last_extension]]=$aRow[name];
}

//$am->connect($manager_host, $manager_user, $manager_secret);
//get channels
//$channels = get_channels($am);

// foreach($channels as $ch=>$chv) {
//     list($chan,$ses) = split("-",$ch,2);
//     $inuse["$chan"]  = $ch;
// }

//get queues
//$queues   = get_queues($am,$channels);

$color['unavailable'] = "flesh_off.png";
$color['unknown']     = "#dadada";
$color['busy']        = "flesh_inc.png";
$color['dialout']     = "#d0303f";
$color['ringing']     = "flesh_ringing.png";
$color['not in use']  = "flesh_free.png";
$color['paused']      = "#000000";
$color['onhold']      = "pause.png";

global $vib;        
global $mai;       
global $fbm;       
global $video;      
global $phone_icon;
global $gela; 

if($whoami == 0){
   $db->setQuery("SELECT    chat.id,
                            users_cumunication.user_id,
                            user_info.`name` AS `nname`,
                    	    1 AS `count`,
                           'georgia.png' AS `country`,
                           `chat`.`browser`,
                           `chat`.`device`,
                    	    chat.`name`,
                            IFNULL((CASE
                                        WHEN users_cumunication.chat = 0 THEN 'CHAT_OFF_OFF.png'
                                        WHEN users_cumunication.chat = 1 AND COUNT(chat.id) = 0 THEN 'CHAT_ON_1.png'
                                        WHEN users_cumunication.chat = 1 AND COUNT(chat.id) = 1 THEN 'CHAT_BUSY1.png'
                                        WHEN users_cumunication.chat = 1 AND COUNT(chat.id) > 1 THEN 'CHAT_BUSY.png'
                                    END),'CHAT_OFF_OFF.png') AS `icon`,
                           (SELECT IF(chat_details.operator_user_id = 0,CONCAT('<img style=\"margin-right: 3px;margin-top: -2px;float: left;width: 17px;\" src=\"media/images/icons/inc_chat.png\"> <span>',TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(chat_details.message_datetime,'%H:%i:%s')),'</span>'),CONCAT('<img style=\"margin-right: 3px;margin-top: -2px;float: left;width: 17px;\" src=\"media/images/icons/out_chat.png\"> <span>',TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(chat_details.message_datetime,'%H:%i:%s')),'</span>'))
                    	    FROM chat_details
                    	    WHERE chat_details.chat_id = chat.id
                    	    ORDER BY chat_details.id DESC
                    	    LIMIT 1) AS `time`,
                    	    TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(chat.join_date,'%H:%i:%s')) AS `time1` 
                  FROM      users_cumunication
                  JOIN      users ON users.id = users_cumunication.user_id
                  JOIN      user_info ON users.id = user_info.user_id
                  JOIN      chat ON chat.last_user_id = users_cumunication.user_id AND chat.`status` = 2
                  WHERE     users_cumunication.chat = 1 AND users.logged = 1
                  GROUP BY  chat.id");
   
   $papalachat = $db->getResultArray();
   
   foreach ($papalachat[result] AS $res_chat){
            
        if($res_chat[count]==0){
            $gela = '<img src="media/images/icons/comunication/'.$res_chat[icon].'" height="14" width="14">';
        }elseif ($res_chat[count]==1){
            $gela = '<img src="media/images/icons/comunication/'.$res_chat[icon].'" height="14" width="14">';
        }else{
            $gela = '<span><span style="position: absolute; font-size: 8px; margin-top: 2px; text-align: center; width: 12px; margin-left: 1px;color: #FFF;">'.$res_chat[count].'</span><img src="media/images/icons/comunication/'.$res_chat[icon].'" height="14" width="14"></span>';
        }
        
        
        $db->setQuery(" SELECT   COUNT(site_chat.id) AS `count`,
                                 IFNULL((CASE
                                            WHEN users_cumunication.site = 0 THEN 'OFF-my.png'
                                            WHEN users_cumunication.site = 1 AND COUNT(site_chat.id) = 0 THEN 'ON-my.png'
                                            WHEN users_cumunication.site = 1 AND COUNT(site_chat.id) = 1 THEN 'Busy - my.png'
                                            WHEN users_cumunication.site = 1 AND COUNT(site_chat.id) > 1 THEN 'busy- multi.png'
                                        END),'OFF-my.png') AS `icon`
                        FROM      users_cumunication
                        LEFT JOIN site_chat ON site_chat.last_user_id = users_cumunication.user_id AND site_chat.`status` = 2
                        WHERE     users_cumunication.`user_id` = '$res_chat[user_id]'");
        
        $r_site = $db->getResultArray();
        $site  = $r_site[result][0];
        
        if($site[count]==0){
            $vib = '<img src="media/images/icons/comunication/'.$site[icon].'" height="14" width="14">';
        }elseif ($site[count] == 1){
            $vib = '<img src="media/images/icons/comunication/'.$site[icon].'" height="14" width="14">';
        }else{
            $vib = '<span><span style="position: absolute; font-size: 8px; margin-top: 2px; text-align: center; width: 12px; margin-left: 1px;color: #FFF;">'.$site[count].'</span><img src="media/images/icons/comunication/'.$site[icon].'" height="14" width="14"></span>';
        }
        
        $db->setQuery(" SELECT    COUNT(mail_chat.id) AS `count`,
                                  IFNULL((CASE
                                            WHEN users_cumunication.mail = 0 THEN 'E-Mail_OFF_OFF.png'
                                            WHEN users_cumunication.mail = 1 AND COUNT(mail_chat.id) = 0 THEN 'E-Mail_ON.png'
                                            WHEN users_cumunication.mail = 1 AND COUNT(mail_chat.id) = 1 THEN 'E-Mail__BUSY.png'
                                            WHEN users_cumunication.mail = 1 AND COUNT(mail_chat.id) > 1 THEN 'E-Mail__BUSY.png'
                                        END),'E-Mail_OFF_OFF.png') AS `icon`
                        FROM      users_cumunication
                        LEFT JOIN mail_chat ON mail_chat.last_user_id = users_cumunication.user_id AND mail_chat.`status` = 2
                        WHERE     users_cumunication.`user_id` = '$res_chat[user_id]'");
        
        $r_mail = $db->getResultArray();
        $mail   = $r_mail[result][0];
        
        if($mail[count]==0){
            $mai = '<img src="media/images/icons/comunication/'.$mail[icon].'" height="14" width="14">';
        }elseif ($mail[count] == 1){
            $mai = '<img src="media/images/icons/comunication/'.$mail[icon].'" height="14" width="14">';
        }else{
            $mai = '<span><span style="position: absolute; font-size: 8px; margin-top: 2px; text-align: center; width: 12px; margin-left: 1px;color: #FFF;">'.$mail[count].'</span><img src="media/images/icons/comunication/'.$mail[icon].'" height="14" width="14"></span>';
        }
        
        
        $db->setQuery(" SELECT   COUNT(fb_chat.id) AS `count`,
                                 IFNULL((CASE
                                            WHEN users_cumunication.messenger = 0 THEN 'FB_OFF.png'
                                            WHEN users_cumunication.messenger = 1 AND COUNT(fb_chat.id) = 0 THEN 'FB_ON.png'
                                            WHEN users_cumunication.messenger = 1 AND COUNT(fb_chat.id) = 1 THEN 'FB_BUSY.png'
                                            WHEN users_cumunication.messenger = 1 AND COUNT(fb_chat.id) > 1 THEN 'FB_BUSY1.png'
                                         END),'FB_OFF.png') AS `icon`
                        FROM      users_cumunication
                        LEFT JOIN fb_chat ON fb_chat.last_user_id = users_cumunication.user_id AND fb_chat.`status` = 2
                        WHERE     users_cumunication.`user_id` = '$res_chat[user_id]'");
        
        $r_fbm1 = $db->getResultArray();
        $fbm1   = $r_fbm1[result][0];
        
        if($fbm1[count]==0){
            $fbm = '<img src="media/images/icons/comunication/'.$fbm1[icon].'" height="14" width="14">';
        }elseif ($fbm1[count] == 1){
            $fbm = '<img src="media/images/icons/comunication/'.$fbm1[icon].'" height="14" width="14">';
        }else{
            $fbm = '<span><span style="position: absolute; font-size: 8px; margin-top: 2px; text-align: center; width: 12px; margin-left: 1px;color: #FFF;">'.$fbm1[count].'</span><img src="media/images/icons/comunication/'.$fbm1[icon].'" height="14" width="14"></span>';
        }
        
//         $db->setQuery(" SELECT     COUNT(videocall.id) AS `count`,
//                                    IFNULL((CASE
//                                                WHEN users_cumunication.video = 0 THEN 'VIDEO_OFF_OFF.png'
//                                                WHEN users_cumunication.video = 1 AND COUNT(videocall.id) = 0 THEN 'VIDEO_ON.png'
//                                                WHEN users_cumunication.video = 1 AND COUNT(videocall.id) = 1 THEN 'VIDEO_BUSY.png'
//                                                WHEN users_cumunication.video = 1 AND COUNT(videocall.id) > 1 THEN 'VIDEO_BUSY.png'
//                                            END),'VIDEO_OFF_OFF.png') AS `icon`
//                          FROM      users_cumunication
//                          LEFT JOIN videocall ON videocall.last_user_id = users_cumunication.user_id AND videocall.`status` = 2
//                          WHERE     users_cumunication.`user_id` = '$res_chat[user_id]'");
        
//         $r_video1 = $db->getResultArray();
//         $video1   = $r_video1[result][0];
        
//         if($video1[count]==0){
//             $video = '<img src="media/images/icons/comunication/'.$video1[icon].'" height="14" width="14">';
//         }elseif ($video1[count] == 1){
//             $video = '<img src="media/images/icons/comunication/'.$video1[icon].'" height="14" width="14">';
//         }else{
//             $video = '<span><span style="position: absolute; font-size: 8px; margin-top: 2px; text-align: center; width: 12px; margin-left: 1px;color: #FFF;">'.$video1[count].'</span><img src="media/images/icons/comunication/'.$video1[icon].'" height="14" width="14"></span>';
//         }
        
        $db->setQuery(" SELECT    users.`online`,
                        		  IFNULL(users_call_dndlocalon.dndlocalon_status,0) AS dnd,
                				  users.work_activities_id,
                				  work_activities.`name` AS activitie_name,
                				  work_activities.color AS activitie_color
                        FROM     `users`
                        LEFT JOIN work_activities ON work_activities.id = users.work_activities_id
                        LEFT JOIN users_call_dndlocalon ON users_call_dndlocalon.user_id = users.id
                        WHERE     users.`id` = '$res_chat[user_id]'");
        
        $r_gela = $db->getResultArray();
        $gela13 = $r_gela[result][0];
        
        $dnd_status       = $gela13[dnd];
        $activitie_status = $gela13[work_activities_id] != 0 ? 1 : 0;
        $avtivitie_name   = $gela13['activitie_name'];
        $avtivitie_color  = $gela13['activitie_color'];
        
        
        if($dnd_status == 0) {
            $phone_icon = "<img alt='inner' style='margin-right:3px;' src=\"media/images/icons/flesh_free.png\" height='14' width='14'>";
        }elseif ($dnd_status == 1) {
            $phone_icon = "<img alt='inner' style='margin-right:3px;' src=\"media/images/icons/comunication/Phone_INSIDE.png\" height='14' width='14'>";
        }elseif ($dnd_status == 2) {
            $phone_icon = "<img alt='inner' style='margin-right:3px;' src=\"media/images/auricular.png\" height='14' width='14'>";
        }
        
        if ($checkState == 1) {
            $data['mini_call_ext'] .= "<tr style=\"border: 1px solid #E6E6E6;\">
                                            <td style=\"font-size: 11px;\">$res_chat[nname]</td>
                                            <td colspan=\"2\" style=\"font-size: 11px;\">$res_chat[name]</td>
                                            <td class='td_center'>
                                                <div style='width: 90px;'>$phone_icon $gela $fbm $vib  $mai</div>
                                            </td>
                                        </tr>";
        }else{
            $data['call_ext'] .= "<tr style=\"cursor: pointer;border: 1px solid #E6E6E6;\" id='open_my_chat' data-source='chat' chat_id='$res_chat[id]'>
                                        <td style=\"font-size: 11px;\"><div style=\"width: 47px;\"><img alt='inner' style='margin-right:3px;' src=\"media/images/icons/$res_chat[country]\" height='16' width='16'></div></td>
                                        <td style=\"font-size: 11px;\"><div style=\"width: 40px;\"><img alt='inner' style='margin-right:3px;' src=\"media/images/icons/$res_chat[browser].png\" height='16' width='16'><img alt='inner' style='margin-right:3px;' src=\"media/images/icons/$res_chat[device].png\" height='16' width='16'></div></td>
                                        <td style=\"font-size: 11px;\">$res_chat[name]</td>
                                        <td class='td_center'>
                                            <div style='width: 90px;'>
                                                $phone_icon $gela $fbm $vib $mai
                                            </div>
                                        </td>
                                        <td style='font-size: 11px;' >$res_chat[nname]</td>
                                        <td style=\"font-size: 11px;\"><div style=\"width: 55px;\">$res_chat[time]</div></td>
                                        <td ><div style=\"width: 55px;\">$res_chat[time1]</div></td>
                                    </tr>";
        }
        
    }
        
    if ($checkState == 1) {
        
        $db->setQuery("SELECT  `chat`.`name`
                              FROM    `chat`
                              WHERE   `chat`.`status` = 1");
        
        $r_chat1 = $db->getResultArray();
        $i=0;
        foreach ($r_chat1[result] AS $res_chat1){
            $i++;
            $data['mini_call_queue'] .= "<tr>
                                            <td>$i</td>
                                            <td colspan=\"3\">$res_chat1[name]</td>
                                        </tr>";
            $data['chat_count'] = 0;
        }
        
        
    }else{
        $db->setQuery("SELECT   `chat`.id,
                                'georgia.png' AS `country`,
                                `chat`.`browser`,
            				    `chat`.`name` AS `name`,
            				     TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(chat.join_date,'%H:%i:%s')) AS `time1`
                       FROM     `chat`
                       WHERE    `chat`.`status` = 1
                       GROUP BY `chat`.`id`");
        
        $r_chat2 = $db->getResultArray();
        $i=0;
        foreach ($r_chat2[result] AS $res_chat2){
            $i++;
            $data['call_queue'] .= "<tr style=\"cursor:pointer;\" id='open_my_chat' data-source='chat' chat_id='$res_chat2[id]'>
                                        <td>$i</td>
                                        <td><img alt='inner' style='margin-right:3px;' src=\"media/images/icons/$res_chat2[country]\" height='16' width='16'></td>
                                        <td><img alt='inner' style='margin-right:3px;' src=\"media/images/icons/$res_chat2[browser].png\" height='16' width='16'></td>
                                        <td colspan=\"3\" style=\"color: rgb(244, 56, 54);cursor:pointer;\" >$res_chat2[name]</td>
                                        <td>$res_chat2[time1]</td>
                                    </tr>";
        }
    }
}elseif($whoami == 2){
    // mysql_query("UPDATE `chat` SET `status`='12' WHERE `status`= 2 AND ABS(UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(last_request_datetime)) > 300");
    $i =0;
    $db->setQuery("SELECT    site_chat.id,
                               sender_avatar,
                               sender_name AS `name`,
                               user_info.`name` AS `operator`,
                               site_chat.`status`,
                              (SELECT IF(ISNULL(site_messages.user_id),CONCAT('<img style=\"margin-right: 3px;margin-top: -2px;float: left;width: 17px;\" src=\"media/images/icons/inc_chat.png\"> <span>',TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(site_messages.datetime,'%H:%i:%s')),'</span>'),CONCAT('<img style=\"margin-right: 3px;margin-top: -2px;float: left;width: 17px;\" src=\"media/images/icons/out_chat.png\"> <span>',TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(site_messages.datetime,'%H:%i:%s')),'</span>'))
            				   FROM site_messages
            				   WHERE site_messages.site_chat_id = site_chat.id
            				   ORDER BY site_messages.id DESC
            				   LIMIT 1) AS `time`,
    
                               TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(site_chat.first_datetime,'%H:%i:%s')) AS `time1`, 
                               users_cumunication.user_id,
                    		   1 AS `count`,
                    		   IFNULL((CASE
                                            WHEN users_cumunication.site = 0 THEN 'OFF-my.png'
                                            WHEN users_cumunication.site = 1 AND COUNT(site_chat.id) = 0 THEN 'ON-my.png'
                                            WHEN users_cumunication.site = 1 AND COUNT(site_chat.id) = 1 THEN 'Busy - my.png'
                                            WHEN users_cumunication.site = 1 AND COUNT(site_chat.id) > 1 THEN 'busy- multi.png'
                                        END),'OFF-my.png') AS `icon`
            				  
                     FROM      users_cumunication
                     JOIN      users ON users.id = users_cumunication.user_id
                     JOIN      user_info ON users.id = user_info.user_id
                     JOIN      site_chat ON site_chat.last_user_id = users_cumunication.user_id AND site_chat.`status` = 2
                     WHERE     users_cumunication.chat = 1 AND users.logged = 1
                     GROUP BY  site_chat.id
                     UNION ALL
                     SELECT  c.id, 
                             sender_avatar,
                             sender_name AS `name`,
                             '' as `operator`,
                             c.`status`,
                             TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(first_datetime,'%H:%i:%s')) AS `time`,
                            '' AS `time1`, '' AS `user_id`,'' AS `count`, '' AS `icon`
                      FROM  `site_chat` as c
                      WHERE `status` = 1 ");  
    
    $r_site = $db->getResultArray();
    foreach ($r_site[result] AS $res_chat){
        $db->setQuery(" SELECT     COUNT(chat.id) AS `count`,
            IFNULL((CASE
            WHEN users_cumunication.chat = 0 THEN 'CHAT_OFF_OFF.png'
            WHEN users_cumunication.chat = 1 AND COUNT(chat.id) = 0 THEN 'CHAT_ON_1.png'
            WHEN users_cumunication.chat = 1 AND COUNT(chat.id) = 1 THEN 'CHAT_BUSY1.png'
            WHEN users_cumunication.chat = 1 AND COUNT(chat.id) > 1 THEN 'CHAT_BUSY.png'
            END),'CHAT_OFF_OFF.png') AS `icon`
            FROM      users_cumunication
            JOIN      users ON users.id = users_cumunication.user_id
            LEFT JOIN chat ON chat.last_user_id = users_cumunication.user_id AND chat.`status` = 2
            WHERE     users.`id` = '$res_chat[user_id]'");
        
        $r_chat = $db->getResultArray();
        $chat   = $r_chat[result][0];
        
        if($chat[count]==0){
            $gela = '<img src="media/images/icons/comunication/'.$chat[icon].'" height="14" width="14">';
        }elseif ($chat[count]==1){
            $gela = '<img src="media/images/icons/comunication/'.$chat[icon].'" height="14" width="14">';
        }else{
            $gela = '<span><span style="position: absolute; font-size: 8px; margin-top: 2px; text-align: center; width: 12px; margin-left: 1px;color: #FFF;">'.$chat[count].'</span><img src="media/images/icons/comunication/'.$chat[icon].'" height="14" width="14"></span>';
        }
        
        if($res_chat[count]==0){
            $vib = '<img src="media/images/icons/comunication/'.$res_chat[icon].'" height="14" width="14">';
        }elseif ($res_chat[count] == 1){
            $vib = '<img src="media/images/icons/comunication/'.$res_chat[icon].'" height="14" width="14">';
        }else{
            $vib = '<span><span style="position: absolute; font-size: 8px; margin-top: 2px; text-align: center; width: 12px; margin-left: 1px;color: #FFF;">'.$res_chat[count].'</span><img src="media/images/icons/comunication/'.$res_chat[icon].'" height="14" width="14"></span>';
        }
        
        $db->setQuery("SELECT     COUNT(mail_chat.id) AS `count`,
                                  IFNULL((CASE
                                             WHEN users_cumunication.mail = 0 THEN 'E-Mail_OFF_OFF.png'
                                             WHEN users_cumunication.mail = 1 AND COUNT(mail_chat.id) = 0 THEN 'E-Mail_ON.png'
                                             WHEN users_cumunication.mail = 1 AND COUNT(mail_chat.id) = 1 THEN 'E-Mail__BUSY.png'
                                             WHEN users_cumunication.mail = 1 AND COUNT(mail_chat.id) > 1 THEN 'E-Mail__BUSY.png'
                                          END),'E-Mail_OFF_OFF.png') AS `icon`
                        FROM      users_cumunication
                        JOIN      users ON users.id = users_cumunication.user_id
                        LEFT JOIN mail_chat ON mail_chat.last_user_id = users_cumunication.user_id AND mail_chat.`status` = 2
                        WHERE     users.`id` = '$res_chat[user_id]'");
        
        $r_mail = $db->getResultArray();
        $mail   = $r_mail[result][0];
        if($mail[count]==0){
            $mai = '<img src="media/images/icons/comunication/'.$mail[icon].'" height="14" width="14">';
        }elseif ($mail[count] == 1){
            $mai = '<img src="media/images/icons/comunication/'.$mail[icon].'" height="14" width="14">';
        }else{
            $mai = '<span><span style="position: absolute; font-size: 8px; margin-top: 2px; text-align: center; width: 12px; margin-left: 1px;color: #FFF;">'.$mail[count].'</span><img src="media/images/icons/comunication/'.$mail[icon].'" height="14" width="14"></span>';
        }
        
        $db->setQuery("SELECT     COUNT(fb_chat.id) AS `count`,
                                  IFNULL((CASE
                                                WHEN users_cumunication.messenger = 0 THEN 'FB_OFF.png'
                                                WHEN users_cumunication.messenger = 1 AND COUNT(fb_chat.id) = 0 THEN 'FB_ON.png'
                                                WHEN users_cumunication.messenger = 1 AND COUNT(fb_chat.id) = 1 THEN 'FB_BUSY.png'
                                                WHEN users_cumunication.messenger = 1 AND COUNT(fb_chat.id) > 1 THEN 'FB_BUSY1.png'
                                            END),'FB_OFF.png') AS `icon`
                        FROM      users_cumunication
                        JOIN      users ON users.id = users_cumunication.user_id
                        LEFT JOIN fb_chat ON fb_chat.last_user_id = users_cumunication.user_id AND fb_chat.`status` = 2
                        WHERE     users.`id` = '$res_chat[user_id]'");
        
        $r_fbm1 = $db->getResultArray();
        $fbm1   = $r_fbm1[result][0];
        
        if($fbm1[count]==0){
            $fbm = '<img src="media/images/icons/comunication/'.$fbm1[icon].'" height="14" width="14">';
        }elseif ($fbm1[count] == 1){
            $fbm = '<img src="media/images/icons/comunication/'.$fbm1[icon].'" height="14" width="14">';
        }else{
            $fbm = '<span><span style="position: absolute; font-size: 8px; margin-top: 2px; text-align: center; width: 12px; margin-left: 1px;color: #FFF;">'.$fbm1[count].'</span><img src="media/images/icons/comunication/'.$fbm1[icon].'" height="14" width="14"></span>';
        }
        
//         $db->setQuery("  SELECT    COUNT(videocall.id) AS `count`,
//                                    IFNULL((CASE
//                                         WHEN users_cumunication.video = 0 THEN 'VIDEO_OFF_OFF.png'
//                                         WHEN users_cumunication.video = 1 AND COUNT(videocall.id) = 0 THEN 'VIDEO_ON.png'
//                                         WHEN users_cumunication.video = 1 AND COUNT(videocall.id) = 1 THEN 'VIDEO_BUSY.png'
//                                         WHEN users_cumunication.video = 1 AND COUNT(videocall.id) > 1 THEN 'VIDEO_BUSY.png'
//                                    END),'VIDEO_OFF_OFF.png') AS `icon`
//                          FROM      users_cumunication
//                          JOIN      users ON users.id = users_cumunication.user_id
//                          LEFT JOIN videocall ON videocall.last_user_id = users_cumunication.user_id AND videocall.`status` = 2
//                          WHERE     users.`id` = '$res_chat[user_id]'");
        
//         $r_video1 = $db->getResultArray();
//         $video1   = $r_video1[result][0];
        
//         if($video1[count]==0){
//             $video = '<img src="media/images/icons/comunication/'.$video1[icon].'" height="14" width="14">';
//         }elseif ($video1[count] == 1){
//             $video = '<img src="media/images/icons/comunication/'.$video1[icon].'" height="14" width="14">';
//         }else{
//             $video = '<span><span style="position: absolute; font-size: 8px; margin-top: 2px; text-align: center; width: 12px; margin-left: 1px;color: #FFF;">'.$video1[count].'</span><img src="media/images/icons/comunication/'.$video1[icon].'" height="14" width="14"></span>';
//         }
        
        $db->setQuery("SELECT    users.online,
                                 IFNULL(users_call_dndlocalon.dndlocalon_status,0) AS dnd,
                                 users.work_activities_id,
                                 work_activities.name AS activitie_name,
                                 work_activities.color AS activitie_color
                       FROM     `users`
                       LEFT JOIN work_activities ON work_activities.id = users.work_activities_id
                       LEFT JOIN users_call_dndlocalon ON users_call_dndlocalon.user_id = users.id
                       WHERE     users.`id` = '$res_chat[user_id]'");
        
        $r_gela13 = $db->getResultArray();
        $gela13   = $r_gela13[result][0];
        
        $dnd_status = $gela13[dnd];
        $activitie_status = $gela13[work_activities_id] != 0 ? 1 : 0;
        $avtivitie_name = $gela13['activitie_name'];
        $avtivitie_color = $gela13['activitie_color'];
        
        
        if($dnd_status == 0) {
            $phone_icon = "<img alt='inner' style='margin-right:3px;' src=\"media/images/icons/flesh_free.png\" height='14' width='14'>";
        }elseif ($dnd_status == 1) {
            $phone_icon = "<img alt='inner' style='margin-right:3px;' src=\"media/images/icons/comunication/Phone_INSIDE.png\" height='14' width='14'>";
        }elseif ($dnd_status == 2) {
            $phone_icon = "<img alt='inner' style='margin-right:3px;' src=\"media/images/auricular.png\" height='14' width='14'>";
        }
        
        if(empty($res_chat['name'])) $res_chat['name']='უცნობი';
        if(empty($res_chat['sender_avatar'])) $res_chat['sender_avatar']='media/images/icons/users.png'; 
        if($checkState == 1){  
            if($res_chat['status']=='2'){
                
                $data['mini_call_ext'] .= "<tr style=\"border: 1px solid #E6E6E6;\">
                                                <td style=\"font-size: 11px;\">$res_chat[operator]</td>
                                                <td style='width: 20px;'><img alt='inner' class='hovered_image'  src='$res_chat[sender_avatar]'></td> 
                                                <td style=\"font-size: 11px;\">$res_chat[name]</td>
                                                <td class='td_center'>
                                                    <div >
                                                    $phone_icon $gela $fbm $mai  $video
                                                    </div>
                                                </td>
                                            </tr>";
            }elseif($res_chat['status']=='1'){
                $i++;
                $data['mini_call_queue'] .= "<tr>
                                                    <td>$i</td>
                                                    <td style=\"width: 20px;\"><img alt='inner' class='hovered_image'  src='$res_chat[sender_avatar]'></td>
                                                    <td colspan=\"2\">$res_chat[name]</td>                                             
                                            </tr>";
            }
            $data['chat_count'] = 0;
        }else{      
            if($res_chat['status']=='1'){  
                $i++;
                $data['call_queue'] .= "<tr style=\"cursor:pointer;\" id='open_my_chat' data-source='site' chat_id='$res_chat[id]'>
                                        <td>$i</td>
                                        <td><img alt='inner' class='hovered_image'  src='$res_chat[sender_avatar]'></td>                                     
                                        <td></td>
                                        <td colspan=\"3\" style=\"color: rgb(244, 56, 54);cursor:pointer;\" >$res_chat[name]</td>
                                        <td>$res_chat[time]</td>
                                    </tr>"; 
            }elseif($res_chat['status']=='2'){
                $data['call_ext'] .= "<tr style=\"cursor: pointer;border: 1px solid #E6E6E6;\" data-source='site' id='open_my_chat' chat_id='$res_chat[id]'>
                                            <td style=\"font-size: 11px;\"></td>
                                            <td><img alt='inner' class='hovered_image'  src='$res_chat[sender_avatar]'></td> 
                                            <td style=\"font-size: 11px;\">$res_chat[name]</td>
                                            <td class='td_center'>
                                                <div style='width: 130px;'>
                                                $phone_icon $gela $fbm $mai  $video
                                                </div>
                                            </td>
                                            <td style='font-size: 11px;' >$res_chat[operator]</td>
                                            <td style=\"font-size: 11px;\">$res_chat[time]</td>
                                            <td >$res_chat[time1]</td>
                                     </tr>";             
            }
        }
    }
    
}elseif($whoami == 3){
    // mysql_query("UPDATE `chat` SET `status`='12' WHERE `status`= 2 AND ABS(UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(last_request_datetime)) > 300");
    $i =0;
    $db->setQuery("SELECT    mail_chat.id,
                               sender_avatar,
                               sender_name AS `name`,
                               user_info.`name` AS `operator`,
                               mail_chat.`status`,
                              (SELECT IF(ISNULL(mail_messages.user_id),CONCAT('<img style=\"margin-right: 3px;margin-top: -2px;float: left;width: 17px;\" src=\"media/images/icons/inc_chat.png\"> <span>',TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(mail_messages.datetime,'%H:%i:%s')),'</span>'),CONCAT('<img style=\"margin-right: 3px;margin-top: -2px;float: left;width: 17px;\" src=\"media/images/icons/out_chat.png\"> <span>',TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(mail_messages.datetime,'%H:%i:%s')),'</span>'))
    						   FROM mail_messages
    						   WHERE mail_messages.mail_chat_id = mail_chat.id
    						   ORDER BY mail_messages.id DESC
    						   LIMIT 1) AS `time`,
    
                               TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(mail_chat.first_datetime,'%H:%i:%s')) AS `time1`, 
                               users_cumunication.user_id,
                    		   1 AS `count`,
                    		   IFNULL((CASE
    										WHEN users_cumunication.mail = 0 THEN 'E-Mail_OFF_OFF.png'
    										WHEN users_cumunication.mail = 1 AND COUNT(mail_chat.id) = 0 THEN 'E-Mail_ON.png'
    										WHEN users_cumunication.mail = 1 AND COUNT(mail_chat.id) > 0 THEN 'E-Mail__BUSY.png'
    								   END),'E-Mail_OFF_OFF.png') AS `icon`
            				  
                     FROM      users_cumunication
                     JOIN      users ON users.id = users_cumunication.user_id
                     JOIN      user_info ON users.id = user_info.user_id
                     JOIN      mail_chat ON mail_chat.last_user_id = users_cumunication.user_id AND mail_chat.`status` = 2
                     WHERE     users_cumunication.chat = 1 AND users.logged = 1
                     GROUP BY  mail_chat.id
                     UNION ALL
                     SELECT  c.id, 
                             sender_avatar,
                             sender_name AS `name`,
                             '' as `operator`,
                             c.`status`,
                             TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(c.first_datetime,'%H:%i:%s')) AS `time`,
                            '' AS `time1`, '' AS `user_id`,'' AS `count`, '' AS `icon`
                      FROM  `mail_chat` as c
                      WHERE c.`status` = 1  ");
    
    $mail_chat = $db->getResultArray();
    foreach ($mail_chat[result] AS $res_chat){
        $db->setQuery(" SELECT     COUNT(chat.id) AS `count`,
                                    IFNULL((CASE
                                                WHEN users_cumunication.chat = 0 THEN 'CHAT_OFF_OFF.png'
                                                WHEN users_cumunication.chat = 1 AND COUNT(chat.id) = 0 THEN 'CHAT_ON_1.png'
                                                WHEN users_cumunication.chat = 1 AND COUNT(chat.id) = 1 THEN 'CHAT_BUSY1.png'
                                                WHEN users_cumunication.chat = 1 AND COUNT(chat.id) > 1 THEN 'CHAT_BUSY.png'
                                            END),'CHAT_OFF_OFF.png') AS `icon`
                        FROM      users_cumunication
                        JOIN      users ON users.id = users_cumunication.user_id
                        LEFT JOIN chat ON chat.last_user_id = users_cumunication.user_id AND chat.`status` = 2
                        WHERE     users.`id` = '$res_chat[user_id]'");
        
        $r_chat = $db->getResultArray();
        $chat   = $r_chat[result][0];
        
        if($chat[count]==0){
            $gela = '<img src="media/images/icons/comunication/'.$chat[icon].'" height="14" width="14">';
        }elseif ($chat[count]==1){
            $gela = '<img src="media/images/icons/comunication/'.$chat[icon].'" height="14" width="14">';
        }else{
            $gela = '<span><span style="position: absolute; font-size: 8px; margin-top: 2px; text-align: center; width: 12px; margin-left: 1px;color: #FFF;">'.$chat[count].'</span><img src="media/images/icons/comunication/'.$chat[icon].'" height="14" width="14"></span>';
        }
        
        $db->setQuery(" SELECT   COUNT(site_chat.id) AS `count`,
                                IFNULL((CASE
                                            WHEN users_cumunication.site = 0 THEN 'OFF-my.png'
                                            WHEN users_cumunication.site = 1 AND COUNT(site_chat.id) = 0 THEN 'ON-my.png'
                                            WHEN users_cumunication.site = 1 AND COUNT(site_chat.id) = 1 THEN 'Busy - my.png'
                                            WHEN users_cumunication.site = 1 AND COUNT(site_chat.id) > 1 THEN 'busy- multi.png'
                                        END),'OFF-my.png') AS `icon`
                        FROM      users_cumunication
                        LEFT JOIN site_chat ON site_chat.last_user_id = users_cumunication.user_id AND site_chat.`status` = 2
                        WHERE     users_cumunication.`user_id` = '$res_chat[user_id]'");
        
        $r_site = $db->getResultArray();
        $site  = $r_site[result][0];
        
        if($site[count]==0){
            $vib = '<img src="media/images/icons/comunication/'.$site[icon].'" height="14" width="14">';
        }elseif ($site[count] == 1){
            $vib = '<img src="media/images/icons/comunication/'.$site[icon].'" height="14" width="14">';
        }else{
            $vib = '<span><span style="position: absolute; font-size: 8px; margin-top: 2px; text-align: center; width: 12px; margin-left: 1px;color: #FFF;">'.$site[count].'</span><img src="media/images/icons/comunication/'.$site[icon].'" height="14" width="14"></span>';
        }
        
        
        
        if($res_chat[count]==0){
            $mai = '<img src="media/images/icons/comunication/'.$res_chat[icon].'" height="14" width="14">';
        }elseif ($res_chat[count] == 1){
            $mai = '<img src="media/images/icons/comunication/'.$res_chat[icon].'" height="14" width="14">';
        }else{
            $mai = '<span><span style="position: absolute; font-size: 8px; margin-top: 2px; text-align: center; width: 12px; margin-left: 1px;color: #FFF;">'.$res_chat[count].'</span><img src="media/images/icons/comunication/'.$res_chat[icon].'" height="14" width="14"></span>';
        }
        
        $db->setQuery("SELECT   COUNT(fb_chat.id) AS `count`,
                                IFNULL((CASE
                                            WHEN users_cumunication.messenger = 0 THEN 'FB_OFF.png'
                                            WHEN users_cumunication.messenger = 1 AND COUNT(fb_chat.id) = 0 THEN 'FB_ON.png'
                                            WHEN users_cumunication.messenger = 1 AND COUNT(fb_chat.id) = 1 THEN 'FB_BUSY.png'
                                            WHEN users_cumunication.messenger = 1 AND COUNT(fb_chat.id) > 1 THEN 'FB_BUSY1.png'
                                        END),'FB_OFF.png') AS `icon`
                        FROM      users_cumunication
                        JOIN      users ON users.id = users_cumunication.user_id
                        LEFT JOIN fb_chat ON fb_chat.last_user_id = users_cumunication.user_id AND fb_chat.`status` = 2
                        WHERE     users.`id` = '$res_chat[user_id]'");
        
        $r_fbm1 = $db->getResultArray();
        $fbm1   = $r_fbm1[result][0];
        
        if($fbm1[count]==0){
            $fbm = '<img src="media/images/icons/comunication/'.$fbm1[icon].'" height="14" width="14">';
        }elseif ($fbm1[count] == 1){
            $fbm = '<img src="media/images/icons/comunication/'.$fbm1[icon].'" height="14" width="14">';
        }else{
            $fbm = '<span><span style="position: absolute; font-size: 8px; margin-top: 2px; text-align: center; width: 12px; margin-left: 1px;color: #FFF;">'.$fbm1[count].'</span><img src="media/images/icons/comunication/'.$fbm1[icon].'" height="14" width="14"></span>';
        }
        
        
//         $db->setQuery(" SELECT    COUNT(videocall.id) AS `count`,
//                                   IFNULL((CASE
//                                             WHEN users_cumunication.video = 0 THEN 'VIDEO_OFF_OFF.png'
//                                             WHEN users_cumunication.video = 1 AND COUNT(videocall.id) = 0 THEN 'VIDEO_ON.png'
//                                             WHEN users_cumunication.video = 1 AND COUNT(videocall.id) = 1 THEN 'VIDEO_BUSY.png'
//                                             WHEN users_cumunication.video = 1 AND COUNT(videocall.id) > 1 THEN 'VIDEO_BUSY.png'
//                                           END),'VIDEO_OFF_OFF.png') AS `icon`
//                         FROM      users_cumunication
//                         JOIN      users ON users.id = users_cumunication.user_id
//                         LEFT JOIN videocall ON videocall.last_user_id = users_cumunication.user_id AND videocall.`status` = 2
//                         WHERE     users.`id` = '$res_chat[user_id]'");
        
//         $r_video1 = $db->getResultArray();
//         $video1   = $r_video1[result][0];
        
//         if($video1[count]==0){
//             $video = '<img src="media/images/icons/comunication/'.$video1[icon].'" height="14" width="14">';
//         }elseif ($video1[count] == 1){
//             $video = '<img src="media/images/icons/comunication/'.$video1[icon].'" height="14" width="14">';
//         }else{
//             $video = '<span><span style="position: absolute; font-size: 8px; margin-top: 2px; text-align: center; width: 12px; margin-left: 1px;color: #FFF;">'.$video1[count].'</span><img src="media/images/icons/comunication/'.$video1[icon].'" height="14" width="14"></span>';
//         }
        
        $db->setQuery("  SELECT    users.online,
                                    IFNULL(users_call_dndlocalon.dndlocalon_status,0) AS dnd,
                                    users.work_activities_id,
                                    work_activities.name AS activitie_name,
                                    work_activities.color AS activitie_color
                         FROM     `users`
                         LEFT JOIN work_activities ON work_activities.id = users.work_activities_id
                         LEFT JOIN users_call_dndlocalon ON users_call_dndlocalon.user_id = users.id
                         WHERE     users.`id` = '$res_chat[user_id]'");
        
        $r_gela13 = $db->getResultArray();
        $gela13   = $r_gela13[result][0];
        
        $dnd_status = $gela13[dnd];
        $activitie_status = $gela13[work_activities_id] != 0 ? 1 : 0;
        $avtivitie_name = $gela13['activitie_name'];
        $avtivitie_color = $gela13['activitie_color'];
        
        
        if($dnd_status == 0) {
            $phone_icon = "<img alt='inner' style='margin-right:3px;' src=\"media/images/icons/flesh_free.png\" height='14' width='14'>";
        }elseif ($dnd_status == 1) {
            $phone_icon = "<img alt='inner' style='margin-right:3px;' src=\"media/images/icons/comunication/Phone_INSIDE.png\" height='14' width='14'>";
        }elseif ($dnd_status == 2) {
            $phone_icon = "<img alt='inner' style='margin-right:3px;' src=\"media/images/auricular.png\" height='14' width='14'>";
        }
        if(empty($res_chat['name'])) $res_chat['name']='უცნობი';
        if(empty($res_chat['sender_avatar'])) $res_chat['sender_avatar']='media/images/icons/users.png'; 
        if($checkState == 1){  
            if($res_chat['status']=='2'){
                
                $data['mini_call_ext'] .= "<tr style=\"border: 1px solid #E6E6E6;\">
                                                <td style=\"font-size: 11px;\">$res_chat[operator]</td>
                                                <td style=\"width: 20px;\"><img alt='inner' class='hovered_image'  src='$res_chat[sender_avatar]'></td> 
                                                <td style=\"font-size: 11px;\">$res_chat[name]</td>
                                                <td class='td_center'>
                                                    <div style='width: 90px;'>
                                                        $phone_icon $gela $fbm $vib $mai
                                                    </div>
                                                </td>
                                            </tr>";
            }elseif($res_chat['status']=='1'){
                $i++;
                $data['mini_call_queue'] .= "<tr>
                                                <td>$i</td>
                                                <td style=\"width: 20px;\"><img alt='inner' class='hovered_image'  src='$res_chat[sender_avatar]'></td>                                            
                                                <td colspan=2 >$res_chat[name]</td>
                                            </tr>";
            }
            $data['chat_count'] = 0;
        }else{      
            if($res_chat['status']=='1'){  
                $i++;
                $data['call_queue'] .= "<tr style=\"cursor:pointer;\" id='open_my_chat' data-source='mail' chat_id='$res_chat[id]'>
                                        <td>$i</td>
                                        <td><img alt='inner' class='hovered_image'  src='$res_chat[sender_avatar]'></td>                                     
                                        <td></td>
                                        <td colspan=\"3\" style=\"color: rgb(244, 56, 54);cursor:pointer;\" >$res_chat[name]</td>
                                        <td>$res_chat[time]</td>
                                    </tr>"; 
            }elseif($res_chat['status']=='2'){
                $data['call_ext'] .= "<tr style=\"cursor: pointer;border: 1px solid #E6E6E6;\" data-source='mail' id='open_my_chat' chat_id='$res_chat[id]'>
                                            <td style=\"font-size: 11px;\"><div style=\"width: 44px;\"></div></td>
                                            <td><div style=\"width: 47px;\"><img alt='inner' class='hovered_image'  src='$res_chat[sender_avatar]'></div></td> 
                                            <td style=\"font-size: 11px;\">$res_chat[name]</td>
                                            <td class='td_center'>
                                                <div style='width: 90px;'>
                                                $phone_icon $gela $fbm $vib $mai
                                                </div>    
                                            </td>
                                            <td style='font-size: 11px;' >$res_chat[operator]</td>
                                            <td style=\"font-size: 11px;\"><div style=\"width: 45px;\">$res_chat[time]</div></td>
                                            <td ><div style=\"width: 45px;\">$res_chat[time]</div></td>
                                    </tr>";
            }
        }
    }        
    
}elseif($whoami == 4){

    $i =0;
    $db->setQuery(" SELECT    fb_chat.id,
                               sender_avatar,
                               sender_name AS `name`,
                               user_info.`name` AS `operator`,
                               fb_chat.`status`,
                              (SELECT IF(ISNULL(fb_messages.user_id),CONCAT('<img style=\"margin-right: 3px;margin-top: -2px;float: left;width: 17px;\" src=\"media/images/icons/inc_chat.png\"> <span>',TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(fb_messages.datetime,'%H:%i:%s')),'</span>'),CONCAT('<img style=\"margin-right: 3px;margin-top: -2px;float: left;width: 17px;\" src=\"media/images/icons/out_chat.png\"> <span>',TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(fb_messages.datetime,'%H:%i:%s')),'</span>'))
								 FROM fb_messages
								 WHERE fb_messages.fb_chat_id = fb_chat.id
								 ORDER BY fb_messages.id DESC
								 LIMIT 1) AS `time`,

                               TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(fb_chat.first_datetime,'%H:%i:%s')) AS `time1`, 
                               users_cumunication.user_id,
                    		   1 AS `count`,
                    		   IFNULL((CASE
    										WHEN users_cumunication.messenger = 0 THEN 'FB_OFF.png'
    										WHEN users_cumunication.messenger = 1 AND COUNT(fb_chat.id) = 0 THEN 'FB_ON.png'
    										WHEN users_cumunication.messenger = 1 AND COUNT(fb_chat.id) = 1 THEN 'FB_BUSY.png'
                                            WHEN users_cumunication.messenger = 1 AND COUNT(fb_chat.id) > 1 THEN 'FB_BUSY1.png'
    								   END),'FB_OFF.png') AS `icon`
            				  
                     FROM      users_cumunication
                     JOIN      users ON users.id = users_cumunication.user_id
                     JOIN      user_info ON users.id = user_info.user_id
                     JOIN      fb_chat ON fb_chat.last_user_id = users_cumunication.user_id AND fb_chat.`status` = 2
                     WHERE     users_cumunication.chat = 1 AND users.logged = 1
                     GROUP BY  fb_chat.id
                     UNION ALL
                     SELECT  c.id, 
                             sender_avatar,
                             sender_name AS `name`,
                             '' as `operator`,
                             c.`status`,
                             TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(c.first_datetime,'%H:%i:%s')) AS `time`,
                            '' AS `time1`, '' AS `user_id`,'' AS `count`, '' AS `icon`
                      FROM  `fb_chat` as c
                      WHERE c.`status` = 1"); 
    
    $fb_chat = $db->getResultArray();
    foreach ($fb_chat[result] AS $res_chat){
    
        $db->setQuery(" SELECT     COUNT(chat.id) AS `count`,
                                    IFNULL((CASE
                                    WHEN users_cumunication.chat = 0 THEN 'CHAT_OFF_OFF.png'
                                    WHEN users_cumunication.chat = 1 AND COUNT(chat.id) = 0 THEN 'CHAT_ON_1.png'
                                    WHEN users_cumunication.chat = 1 AND COUNT(chat.id) = 1 THEN 'CHAT_BUSY1.png'
                                    WHEN users_cumunication.chat = 1 AND COUNT(chat.id) > 1 THEN 'CHAT_BUSY.png'
                                    END),'CHAT_OFF_OFF.png') AS `icon`
                        FROM      users_cumunication
                        JOIN      users ON users.id = users_cumunication.user_id
                        LEFT JOIN chat ON chat.last_user_id = users_cumunication.user_id AND chat.`status` = 2
                        WHERE     users.`id` = '$res_chat[user_id]'");
        
        $r_chat = $db->getResultArray();
        $chat   = $r_chat[result][0];
        
        if($chat[count]==0){
            $gela = '<img src="media/images/icons/comunication/'.$chat[icon].'" height="14" width="14">';
        }elseif ($chat[count]==1){
            $gela = '<img src="media/images/icons/comunication/'.$chat[icon].'" height="14" width="14">';
        }else{
            $gela = '<span><span style="position: absolute; font-size: 8px; margin-top: 2px; text-align: center; width: 12px; margin-left: 1px;color: #FFF;">'.$chat[count].'</span><img src="media/images/icons/comunication/'.$chat[icon].'" height="14" width="14"></span>';
        }
        
        $db->setQuery(" SELECT   COUNT(site_chat.id) AS `count`,
            IFNULL((CASE
                                            WHEN users_cumunication.site = 0 THEN 'OFF-my.png'
                                            WHEN users_cumunication.site = 1 AND COUNT(site_chat.id) = 0 THEN 'ON-my.png'
                                            WHEN users_cumunication.site = 1 AND COUNT(site_chat.id) = 1 THEN 'Busy - my.png'
                                            WHEN users_cumunication.site = 1 AND COUNT(site_chat.id) > 1 THEN 'busy- multi.png'
                                        END),'OFF-my.png') AS `icon`
            FROM      users_cumunication
            LEFT JOIN site_chat ON site_chat.last_user_id = users_cumunication.user_id AND site_chat.`status` = 2
            WHERE     users_cumunication.`user_id` = '$res_chat[user_id]'");
        
        $r_site = $db->getResultArray();
        $site  = $r_site[result][0];
        
        if($site[count]==0){
            $vib = '<img src="media/images/icons/comunication/'.$site[icon].'" height="14" width="14">';
        }elseif ($site[count] == 1){
            $vib = '<img src="media/images/icons/comunication/'.$site[icon].'" height="14" width="14">';
        }else{
            $vib = '<span><span style="position: absolute; font-size: 8px; margin-top: 2px; text-align: center; width: 12px; margin-left: 1px;color: #FFF;">'.$site[count].'</span><img src="media/images/icons/comunication/'.$site[icon].'" height="14" width="14"></span>';
        }
        
        
        $db->setQuery(" SELECT    COUNT(mail_chat.id) AS `count`,
                                    IFNULL((CASE
                                    WHEN users_cumunication.mail = 0 THEN 'E-Mail_OFF_OFF.png'
                                    WHEN users_cumunication.mail = 1 AND COUNT(mail_chat.id) = 0 THEN 'E-Mail_ON.png'
                                    WHEN users_cumunication.mail = 1 AND COUNT(mail_chat.id) = 1 THEN 'E-Mail__BUSY.png'
                                    WHEN users_cumunication.mail = 1 AND COUNT(mail_chat.id) > 1 THEN 'E-Mail__BUSY.png'
                                    END),'E-Mail_OFF_OFF.png') AS `icon`
                        FROM      users_cumunication
                        LEFT JOIN mail_chat ON mail_chat.last_user_id = users_cumunication.user_id AND mail_chat.`status` = 2
                        WHERE     users_cumunication.`user_id` = '$res_chat[user_id]'");
        
        $r_mail = $db->getResultArray();
        $mail   = $r_mail[result][0];
        
        if($mail[count]==0){
            $mai = '<img src="media/images/icons/comunication/'.$mail[icon].'" height="14" width="14">';
        }elseif ($mail[count] == 1){
            $mai = '<img src="media/images/icons/comunication/'.$mail[icon].'" height="14" width="14">';
        }else{
            $mai = '<span><span style="position: absolute; font-size: 8px; margin-top: 2px; text-align: center; width: 12px; margin-left: 1px;color: #FFF;">'.$mail[count].'</span><img src="media/images/icons/comunication/'.$mail[icon].'" height="14" width="14"></span>';
        }
        
       
        if($res_chat[count]==0){
            $fbm = '<img src="media/images/icons/comunication/'.$res_chat[icon].'" height="14" width="14">';
        }elseif ($res_chat[count] == 1){
            $fbm = '<img src="media/images/icons/comunication/'.$res_chat[icon].'" height="14" width="14">';
        }else{
            $fbm = '<span><span style="position: absolute; font-size: 8px; margin-top: 2px; text-align: center; width: 12px; margin-left: 1px;color: #FFF;">'.$res_chat[count].'</span><img src="media/images/icons/comunication/'.$res_chat[icon].'" height="14" width="14"></span>';
        }
        
        
//         $video1 = mysql_fetch_array(mysql_query("SELECT    COUNT(videocall.id) AS `count`,
//                                                             IFNULL((CASE
//                                                             WHEN users_cumunication.video = 0 THEN 'VIDEO_OFF_OFF.png'
//                                                             WHEN users_cumunication.video = 1 AND COUNT(videocall.id) = 0 THEN 'VIDEO_ON.png'
//                                                             WHEN users_cumunication.video = 1 AND COUNT(videocall.id) = 1 THEN 'VIDEO_BUSY.png'
//                                                             WHEN users_cumunication.video = 1 AND COUNT(videocall.id) > 1 THEN 'VIDEO_BUSY.png'
//                                                             END),'VIDEO_OFF_OFF.png') AS `icon`
//                                                 FROM      users_cumunication
//                                                 JOIN      users ON users.id = users_cumunication.user_id
//                                                 LEFT JOIN videocall ON videocall.last_user_id = users_cumunication.user_id AND videocall.`status` = 2
//                                                 WHERE     users.`id` = '$res_chat[user_id]'"));
        
//         $db->setQuery(" SELECT    COUNT(videocall.id) AS `count`,
//                                     IFNULL((CASE
//                                     WHEN users_cumunication.video = 0 THEN 'VIDEO_OFF_OFF.png'
//                                     WHEN users_cumunication.video = 1 AND COUNT(videocall.id) = 0 THEN 'VIDEO_ON.png'
//                                     WHEN users_cumunication.video = 1 AND COUNT(videocall.id) = 1 THEN 'VIDEO_BUSY.png'
//                                     WHEN users_cumunication.video = 1 AND COUNT(videocall.id) > 1 THEN 'VIDEO_BUSY.png'
//                                     END),'VIDEO_OFF_OFF.png') AS `icon`
//                         FROM      users_cumunication
//                         JOIN      users ON users.id = users_cumunication.user_id
//                         LEFT JOIN videocall ON videocall.last_user_id = users_cumunication.user_id AND videocall.`status` = 2
//                         WHERE     users.`id` = '$res_chat[user_id]'");
        
//         $r_video1 = $db->getResultArray();
//         $video1   = $r_video1[result][0];
        
//         if($video1[count]==0){
//             $video = '<img src="media/images/icons/comunication/'.$video1[icon].'" height="14" width="14">';
//         }elseif ($video1[count] == 1){
//             $video = '<img src="media/images/icons/comunication/'.$video1[icon].'" height="14" width="14">';
//         }else{
//             $video = '<span><span style="position: absolute; font-size: 8px; margin-top: 2px; text-align: center; width: 12px; margin-left: 1px;color: #FFF;">'.$video1[count].'</span><img src="media/images/icons/comunication/'.$video1[icon].'" height="14" width="14"></span>';
//         }
        
        $db->setQuery("  SELECT    users.online,
                                   IFNULL(users_call_dndlocalon.dndlocalon_status,0) AS dnd,
                                   users.work_activities_id,
                                   work_activities.name AS activitie_name,
                                   work_activities.color AS activitie_color
                         FROM     `users`
                         LEFT JOIN work_activities ON work_activities.id = users.work_activities_id
                         LEFT JOIN users_call_dndlocalon ON users_call_dndlocalon.user_id = users.id
                         WHERE     users.`id` = '$res_chat[user_id]'");
        
        $r_gela13 = $db->getResultArray();
        $gela13   = $r_gela13[result][0];
        
        
        $dnd_status = $gela13[dnd];
        $activitie_status = $gela13[work_activities_id] != 0 ? 1 : 0;
        $avtivitie_name = $gela13['activitie_name'];
        $avtivitie_color = $gela13['activitie_color'];
        
        
        if($dnd_status == 0) {
            $phone_icon = "<img alt='inner' style='margin-right:3px;' src=\"media/images/icons/flesh_free.png\" height='14' width='14'>";
        }elseif ($dnd_status == 1) {
            $phone_icon = "<img alt='inner' style='margin-right:3px;' src=\"media/images/icons/comunication/Phone_INSIDE.png\" height='14' width='14'>";
        }elseif ($dnd_status == 2) {
            $phone_icon = "<img alt='inner' style='margin-right:3px;' src=\"media/images/auricular.png\" height='14' width='14'>";
        }
        if(empty($res_chat['name'])) $res_chat['name']='უცნობი';
        if(empty($res_chat['sender_avatar'])) $res_chat['sender_avatar']='media/images/icons/users.png'; 
        if($checkState == 1){  
            if($res_chat['status']=='2'){
                
                $data['mini_call_ext'] .= "<tr style=\"border: 1px solid #E6E6E6;\">
                                                <td style=\"font-size: 11px;\">$res_chat[operator]</td>
                                                <td style=\"width: 20px;\"><img alt='inner' class='hovered_image'  src='$res_chat[sender_avatar]'></td> 
                                                <td style=\"font-size: 11px;\">$res_chat[name]</td>
                                                <td class='td_center'>
                                                    <div style='width: 90px;'>
                                                    $phone_icon $gela $fbm $vib $mai
                                                    </div>
                                                </td>
                                            </tr>";
            }elseif($res_chat['status']=='1'){
                $i++;
            $data['mini_call_queue'] .= "<tr>
                                                <td>$i</td>
                                                <td style=\"width: 20px;\"><img alt='inner' class='hovered_image'  src='$res_chat[sender_avatar]'></td> 
                                                <td colspan='2'>$res_chat[name]</td>
                                         </tr>";
            }
            
            $data['chat_count'] = 0;
        }else{      
            if($res_chat['status']=='1'){  
                $i++;
                $data['call_queue'] .= "<tr style=\"cursor:pointer;\" id='open_my_chat' data-source='fbm' chat_id='$res_chat[id]'>
                                        <td>$i</td>
                                        <td><img alt='inner' class='hovered_image'  src='$res_chat[sender_avatar]'></td>                                     
                                        <td></td>
                                        <td colspan=\"3\" style=\"color: rgb(244, 56, 54);cursor:pointer;\" >$res_chat[name]</td>
                                        <td>$res_chat[time]</td>
                                    </tr>"; 
            }elseif($res_chat['status']=='2'){
                $data['call_ext'] .= "<tr style=\"cursor: pointer;border: 1px solid #E6E6E6;\" data-source='fbm' id='open_my_chat' chat_id='$res_chat[id]'>
                                            <td style=\"font-size: 11px;\"><div style=\"width: 44px;\"></div></td>
                                            <td><div style=\"width: 47px;\"><img alt='inner' class='hovered_image'  src='$res_chat[sender_avatar]'</div></td> 
                                            <td style=\"font-size: 11px;\">$res_chat[name]</td>
                                            <td class='td_center'>
                                                <div style='width: 90px;'>
                                                $phone_icon $gela $fbm $vib $mai
                                                </div>    
                                            </td>
                                            <td style='font-size: 11px;' >$res_chat[operator]</div></td>
                                            <td style=\"font-size: 11px;\"><div style=\"width: 45px;\">$res_chat[time1]</td>
                                            <td ><div style=\"width: 45px;\">$res_chat[time]</div></td>
                                    </tr>";
            }
        }
    }
}elseif($whoami == 6){
    $i = 0;
    
    $db->setQuery("SELECT    videocall.id,
                               sender_avatar,
                               sender_name AS `name`,
                               user_info.`name` AS `operator`,
                               videocall.`status`,
                              '' AS `time`,

                               TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(videocall.first_datetime,'%H:%i:%s')) AS `time1`, 
                               users_cumunication.user_id,
                    		   COUNT(videocall.id) AS `count`,
                    		   IFNULL((CASE
    										WHEN users_cumunication.video = 0 THEN 'VIDEO_OFF_OFF.png'
    										WHEN users_cumunication.video = 1 AND COUNT(videocall.id) = 0 THEN 'VIDEO_ON.png'
    										WHEN users_cumunication.video = 1 AND COUNT(videocall.id) > 0 THEN 'VIDEO_BUSY.png'
    								   END),'VIDEO_OFF_OFF.png') AS `icon`
            				  
                     FROM      users_cumunication
                     JOIN      users ON users.id = users_cumunication.user_id
                     JOIN      user_info ON users.id = user_info.user_id
                     JOIN      videocall ON videocall.last_user_id = users_cumunication.user_id AND videocall.`status` = 2
                     WHERE     users_cumunication.chat = 1 AND users.logged = 1
                     GROUP BY  users_cumunication.user_id
                     UNION ALL
                     SELECT  c.id, 
                             sender_avatar,
                             sender_name AS `name`,
                             '' as `operator`,
                             c.`status`,
                             TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(c.first_datetime,'%H:%i:%s')) AS `time`,
                            '' AS `time1`, '' AS `user_id`,'' AS `count`, '' AS `icon`
                      FROM  `videocall` as c
                      WHERE c.`status` = 1 ");
    
    $videocall = $db->getResultArray();
    foreach ($videocall[result] AS $res_video){
   
        $db->setQuery("SELECT     COUNT(chat.id) AS `count`,
                                    IFNULL((CASE
                                    WHEN users_cumunication.chat = 0 THEN 'CHAT_OFF_OFF.png'
                                    WHEN users_cumunication.chat = 1 AND COUNT(chat.id) = 0 THEN 'CHAT_ON_1.png'
                                    WHEN users_cumunication.chat = 1 AND COUNT(chat.id) = 1 THEN 'CHAT_BUSY1.png'
                                    WHEN users_cumunication.chat = 1 AND COUNT(chat.id) > 1 THEN 'CHAT_BUSY.png'
                                    END),'CHAT_OFF_OFF.png') AS `icon`
                        FROM      users_cumunication
                        JOIN      users ON users.id = users_cumunication.user_id
                        LEFT JOIN chat ON chat.last_user_id = users_cumunication.user_id AND chat.`status` = 2
                        WHERE     users.`id` = '$res_video[user_id]'");
        
        $r_chat = $db->getResultArray();
        $chat   = $r_chat[result][0];
        
        if($chat[count]==0){
            $gela = '<img src="media/images/icons/comunication/'.$chat[icon].'" height="14" width="14">';
        }elseif ($chat[count]==1){
            $gela = '<img src="media/images/icons/comunication/'.$chat[icon].'" height="14" width="14">';
        }else{
            $gela = '<span><span style="position: absolute; font-size: 8px; margin-top: 2px; text-align: center; width: 12px; margin-left: 1px;color: #FFF;">'.$chat[count].'</span><img src="media/images/icons/comunication/'.$chat[icon].'" height="14" width="14"></span>';
        }
        
        $db->setQuery("SELECT   COUNT(site_chat.id) AS `count`,
                                IFNULL((CASE
                                            WHEN users_cumunication.site = 0 THEN 'OFF-my.png'
                                            WHEN users_cumunication.site = 1 AND COUNT(site_chat.id) = 0 THEN 'ON-my.png'
                                            WHEN users_cumunication.site = 1 AND COUNT(site_chat.id) = 1 THEN 'Busy - my.png'
                                            WHEN users_cumunication.site = 1 AND COUNT(site_chat.id) > 1 THEN 'busy- multi.png'
                                        END),'OFF-my.png') AS `icon`
                        FROM      users_cumunication
                        JOIN      users ON users.id = users_cumunication.user_id
                        LEFT JOIN site_chat ON site_chat.last_user_id = users_cumunication.user_id AND site_chat.`status` = 2
                        WHERE     users.`id` = '$res_video[user_id]'");
        
        $r_site = $db->getResultArray();
        $site   = $r_site[result][0];
        
        if($site[count]==0){
            $vib = '<img src="media/images/icons/comunication/'.$site[icon].'" height="14" width="14">';
        }elseif ($site[count] == 1){
            $vib = '<img src="media/images/icons/comunication/'.$site[icon].'" height="14" width="14">';
        }else{
            $vib = '<span><span style="position: absolute; font-size: 8px; margin-top: 2px; text-align: center; width: 12px; margin-left: 1px;color: #FFF;">'.$site[count].'</span><img src="media/images/icons/comunication/'.$site[icon].'" height="14" width="14"></span>';
        }
        
        $db->setQuery("SELECT     COUNT(mail_chat.id) AS `count`,
                                    IFNULL((CASE
                                    WHEN users_cumunication.mail = 0 THEN 'E-Mail_OFF_OFF.png'
                                    WHEN users_cumunication.mail = 1 AND COUNT(mail_chat.id) = 0 THEN 'E-Mail_ON.png'
                                    WHEN users_cumunication.mail = 1 AND COUNT(mail_chat.id) = 1 THEN 'E-Mail__BUSY.png'
                                    WHEN users_cumunication.mail = 1 AND COUNT(mail_chat.id) > 1 THEN 'E-Mail__BUSY.png'
                                    END),'E-Mail_OFF_OFF.png') AS `icon`
                        FROM      users_cumunication
                        JOIN      users ON users.id = users_cumunication.user_id
                        LEFT JOIN mail_chat ON mail_chat.last_user_id = users_cumunication.user_id AND mail_chat.`status` = 2
                        WHERE     users.`id` = '$res_video[user_id]'");
        
        $r_mail = $db->getResultArray();
        $mail   = $r_mail[result][0];
        
        if($mail[count]==0){
            $mai = '<img src="media/images/icons/comunication/'.$mail[icon].'" height="14" width="14">';
        }elseif ($mail[count] == 1){
            $mai = '<img src="media/images/icons/comunication/'.$mail[icon].'" height="14" width="14">';
        }else{
            $mai = '<span><span style="position: absolute; font-size: 8px; margin-top: 2px; text-align: center; width: 12px; margin-left: 1px;color: #FFF;">'.$mail[count].'</span><img src="media/images/icons/comunication/'.$mail[icon].'" height="14" width="14"></span>';
        }
        
        $db->setQuery("SELECT   COUNT(fb_chat.id) AS `count`,
                                IFNULL((CASE
                                WHEN users_cumunication.messenger = 0 THEN 'FB_OFF.png'
                                WHEN users_cumunication.messenger = 1 AND COUNT(fb_chat.id) = 0 THEN 'FB_ON.png'
                                WHEN users_cumunication.messenger = 1 AND COUNT(fb_chat.id) = 1 THEN 'FB_BUSY.png'
                                WHEN users_cumunication.messenger = 1 AND COUNT(fb_chat.id) > 1 THEN 'FB_BUSY1.png'
                                END),'FB_OFF.png') AS `icon`
                        FROM      users_cumunication
                        JOIN      users ON users.id = users_cumunication.user_id
                        LEFT JOIN fb_chat ON fb_chat.last_user_id = users_cumunication.user_id AND fb_chat.`status` = 2
                        WHERE     users.`id` = '$res_video[user_id]'");
        
        $r_fbm1 = $db->getResultArray();
        $fbm1   = $r_fbm1[result][0];
        
        if($fbm1[count]==0){
            $fbm = '<img src="media/images/icons/comunication/'.$fbm1[icon].'" height="14" width="14">';
        }elseif ($fbm1[count] == 1){
            $fbm = '<img src="media/images/icons/comunication/'.$fbm1[icon].'" height="14" width="14">';
        }else{
            $fbm = '<span><span style="position: absolute; font-size: 8px; margin-top: 2px; text-align: center; width: 12px; margin-left: 1px;color: #FFF;">'.$fbm1[count].'</span><img src="media/images/icons/comunication/'.$fbm1[icon].'" height="14" width="14"></span>';
        }
        
        
        if($res_video[count]==0){
            $video = '<img src="media/images/icons/comunication/'.$res_video[icon].'" height="14" width="14">';
        }elseif ($res_video[count] == 1){
            $video = '<img src="media/images/icons/comunication/'.$res_video[icon].'" height="14" width="14">';
        }else{
            $video = '<span><span style="position: absolute; font-size: 8px; margin-top: 2px; text-align: center; width: 12px; margin-left: 1px;color: #FFF;">'.$res_video[count].'</span><img src="media/images/icons/comunication/'.$res_video[icon].'" height="14" width="14"></span>';
        }
        
        $db->setQuery(" SELECT   users.online,
                                  IFNULL(users_call_dndlocalon.dndlocalon_status,0) AS dnd,
                                  users.work_activities_id,
                                  work_activities.name AS activitie_name,
                                  work_activities.color AS activitie_color
                        FROM     `users`
                        LEFT JOIN work_activities ON work_activities.id = users.work_activities_id
                        LEFT JOIN users_call_dndlocalon ON users_call_dndlocalon.user_id = users.id
                        WHERE     users.`id` = '$res_video[user_id]'");
        
        $r_gela13 = $db->getResultArray();
        $gela13   = $r_gela13[result][0];
        
        
        $dnd_status = $gela13[dnd];
        $activitie_status = $gela13[work_activities_id] != 0 ? 1 : 0;
        $avtivitie_name = $gela13['activitie_name'];
        $avtivitie_color = $gela13['activitie_color'];
        
        
        if($dnd_status == 0) {
            $phone_icon = "<img alt='inner' style='margin-right:3px;' src=\"media/images/icons/flesh_free.png\" height='14' width='14'>";
        }elseif ($dnd_status == 1) {
            $phone_icon = "<img alt='inner' style='margin-right:3px;' src=\"media/images/icons/comunication/Phone_INSIDE.png\" height='14' width='14'>";
        }elseif ($dnd_status == 2) {
            $phone_icon = "<img alt='inner' style='margin-right:3px;' src=\"media/images/auricular.png\" height='14' width='14'>";
        }
        if(empty($res_video['name'])) $res_video['name']='უცნობი';
        if(empty($res_video['sender_avatar'])) $res_video['sender_avatar']='media/images/icons/users.png';
        if($checkState == 1){
            if($res_video['status']=='2'){
                
                $data['mini_call_ext'] .= "<tr style=\"border: 1px solid #E6E6E6;\">
                                                <td style=\"font-size: 11px;\">$res_video[operator]</td>
                                                <td style='width: 20px;'><img alt='inner' class='hovered_image'  src='$res_video[sender_avatar]'></td>
                                                <td style=\"font-size: 11px;\">$res_video[name]</td>
                                                <td class='td_center'>
                                                <div >$phone_icon $gela $fbm $mai  $video</div>
                                                </td>
                                            </tr>";
            }elseif($res_video['status']=='1'){
                $i++;
                $data['mini_call_queue'] .= "<tr>
                                                <td>$i</td>
                                                <td style=\"width: 20px;\"><img alt='inner' class='hovered_image'  src='$res_video[sender_avatar]'></td>
                                                <td colspan=\"2\" >$res_video[name]</td>
                                             </tr>";
            }
            $data['chat_count'] = 0;
        }else{
            if($res_video['status']=='1'){
                $i++;
                $data['call_queue'] .= "<tr style=\"cursor:pointer;\" id='open_my_chat' data-source='video' call_id='$res_video[sender_id]' chat_id='$res_video[id]'>
                                        <td>$i</td>
                                        <td><img alt='inner' class='hovered_image'  src='$res_video[sender_avatar]'></td>
                                        <td></td>
                                        <td colspan=\"3\" style=\"color: rgb(244, 56, 54);cursor:pointer;\" >$res_video[name]</td>
                                        <td>$res_video[time]</td>
                                        </tr>";
            }elseif($res_video['status']=='2'){
                $data['call_ext'] .= "<tr style=\"cursor: pointer;border: 1px solid #E6E6E6;\" data-source='video' id='open_my_chat' call_id='$res_video[sender_id]' chat_id='$res_video[id]'>
                                        <td style=\"font-size: 11px;\"><div style=\"width: 44px;\"></div></td>
                                        <td><div style=\"width: 47px;\"><img alt='inner' class='hovered_image'  src='$res_video[sender_avatar]'></div></td>
                                        <td style=\"font-size: 11px;\">$res_video[name]</td>
                                        <td class='td_center'>
                                        <div style='width: 90px;'>
                                        $phone_icon $gela $fbm $mai  $video
                                        </div>
                                        </td>
                                        <td style='font-size: 11px;' >$res_video[operator]</td>
                                        <td style=\"font-size: 11px;\"><div style=\"width: 45px;\">$res_video[time]</div></td>
                                        <td ><div style=\"width: 45px;\">$res_video[time]</div></td>
                                        </tr>";
             }
        }
    }
}else{
    
    $db->setQuery("SELECT `user`,`queue`,`unique_id`,`ext_number`,`ext_name`,`phone`,`time`,`status`,`activite_name`, `color` 
                   FROM  (SELECT  	 `user_info`.`name` AS `user`,
                                     `work_activities`.`name` as activite_name, 
                                     `work_activities`.`color` as color,
                                     `asterisk_in_call`.`queue` as `queue`,
                                     `asterisk_in_call`.`ext` AS `ext_number`,
                                     `asterisk_in_call`.`ext_name` AS `ext_name`,
                                     `asterisk_in_call`.`unique_id`,
                                     `asterisk_in_call`.`phone` AS `phone`,
                                      TIME_FORMAT(SEC_TO_TIME(now() - `date`), '%i:%s')  AS `time`,
                                      if(`asterisk_in_call`.`status`= 'dnd',if(`asterisk_in_call`.`peer_status`='ok',`asterisk_in_call`.`status`,    'unavailable'),`asterisk_in_call`.`status`) AS `status`
        				   FROM 	 `asterisk_in_call`
        				   LEFT JOIN `users` on `users`.`id` = `asterisk_in_call`.`user_id`
        				   LEFT JOIN `user_info` on `user_info`.`user_id` = `asterisk_in_call`.`user_id`
        				   LEFT JOIN `work_activities` on `work_activities`.`id` = `users`.`work_activities_id`
        				   WHERE 	 `asterisk_in_call`.`status` in ('busy','dialout','ringing','paused','onhold') 
        				   GROUP BY  `asterisk_in_call`.`ext_name`
					       UNION ALL
                           SELECT    `user_info`.`name` AS `user`,
							         `work_activities`.`name` as activite_name, 
                					 `work_activities`.`color` as color,
                					  '' AS `queue`,
                					 `asterisk_in_call`.`ext` AS `ext_number`,
                					 `asterisk_in_call`.`ext_name` AS `ext_name`,
                					 `asterisk_in_call`.`unique_id`,
                					  '' AS `phone`,
                					  '' AS `time`,
					                  if(`asterisk_in_call`.`status`= 'dnd',if(`asterisk_in_call`.`peer_status`='ok',`asterisk_in_call`.`status`,'unavailable'),`asterisk_in_call`.`status`) AS `status`
        				   FROM 	 `asterisk_in_call`
        				   LEFT JOIN `users` on `users`.`id` = `asterisk_in_call`.`user_id`
        				   LEFT JOIN `user_info` on `user_info`.`user_id` = `asterisk_in_call`.`user_id`
        				   LEFT JOIN `work_activities` on `work_activities`.`id` = `users`.`work_activities_id`
        				   WHERE 	 `asterisk_in_call`.`ext_name` NOT IN (SELECT    asterisk_in_call.ext_name 
																		   FROM      asterisk_in_call 
																		   WHERE     asterisk_in_call.`status` in ('busy','dialout','ringing','paused','onhold') 
                                                                           GROUP BY `asterisk_in_call`.`ext_name`) 
                           GROUP BY `asterisk_in_call`.`ext_name`) results
                   WHERE not isnull(`phone`)  
                   ORDER BY `user` DESC,`ext_name` ASC");
    
        $result = $db->getResultArray();
        
        $color['unavailable']   ="flesh_off.png";
        $color['unknown']       ="#dadada";
        $color['busy']          ="flesh_inc.png";//
        $color['dialout']       ="flesh_out.png";//
        $color['ringing']       ="flesh_ringing.png";//
        $color['not in use']    ="flesh_free.png";
        $color['paused']        ="#000000";//
        $color['onhold']        = "pause.png";//
        
        foreach ($result[result] as $res){
            
            $aval      = $res['status'];
            $qn        = $res['queue'];
            $phone     = $res['phone'];
            $aname     = $res['ext_name'];
            $clid      = $res['phone'];
            $icon      = $res['status'];
            $unique_id = $res['unique_id'];
            $name      = $res[user];
            
            $db->setQuery("SELECT     COUNT(chat.id) AS `count`,
                					  IFNULL((CASE
    											 WHEN users_cumunication.chat = 0 THEN 'CHAT_OFF_OFF.png'
                                                 WHEN users_cumunication.chat = 1 AND COUNT(chat.id) = 0 THEN 'CHAT_ON_1.png'
                                                 WHEN users_cumunication.chat = 1 AND COUNT(chat.id) = 1 THEN 'CHAT_BUSY1.png'
                                                 WHEN users_cumunication.chat = 1 AND COUNT(chat.id) > 1 THEN 'CHAT_BUSY.png'
            								  END),'CHAT_OFF_OFF.png') AS `icon`
                            FROM      users_cumunication
                            JOIN      users ON users.id = users_cumunication.user_id
                            JOIN      user_info ON users.id = user_info.user_id 
                            LEFT JOIN chat ON chat.last_user_id = users_cumunication.user_id AND chat.`status` = 2
                            WHERE     user_info.`name` = '$name'");
            
            $r_chat = $db->getResultArray();
            $chat = $r_chat[result][0];
            
            if($chat[count]==0){
                $gela = '<img src="media/images/icons/comunication/'.$chat[icon].'" height="14" width="14">';
            }elseif ($chat[count]==1){
                $gela = '<img src="media/images/icons/comunication/'.$chat[icon].'" height="14" width="14">';
            }else{
                $gela = '<span><span style="position: absolute; font-size: 8px; margin-top: 2px; text-align: center; width: 12px; margin-left: 1px;color: #FFF;">'.$chat[count].'</span><img src="media/images/icons/comunication/'.$chat[icon].'" height="14" width="14"></span>';
            }
    
            $db->setQuery(" SELECT   COUNT(site_chat.id) AS `count`,
                IFNULL((CASE
                                            WHEN users_cumunication.site = 0 THEN 'OFF-my.png'
                                            WHEN users_cumunication.site = 1 AND COUNT(site_chat.id) = 0 THEN 'ON-my.png'
                                            WHEN users_cumunication.site = 1 AND COUNT(site_chat.id) = 1 THEN 'Busy - my.png'
                                            WHEN users_cumunication.site = 1 AND COUNT(site_chat.id) > 1 THEN 'busy- multi.png'
                                        END),'OFF-my.png') AS `icon`
                FROM      users_cumunication
                JOIN      users ON users.id = users_cumunication.user_id
                JOIN      user_info ON users.id = user_info.user_id
                LEFT JOIN site_chat ON site_chat.last_user_id = users_cumunication.user_id AND site_chat.`status` = 2
                WHERE     user_info.`name` = '$name'");
            
            $r_site = $db->getResultArray();
            $site  = $r_site[result][0];
            
            if($site[count]==0){
                $vib = '<img src="media/images/icons/comunication/'.$site[icon].'" height="14" width="14">';
            }elseif ($site[count] == 1){
                $vib = '<img src="media/images/icons/comunication/'.$site[icon].'" height="14" width="14">';
            }else{
                $vib = '<span><span style="position: absolute; font-size: 8px; margin-top: 2px; text-align: center; width: 12px; margin-left: 1px;color: #FFF;">'.$site[count].'</span><img src="media/images/icons/comunication/'.$site[icon].'" height="14" width="14"></span>';
            }
            
    
            $db->setQuery("SELECT     COUNT(mail_chat.id) AS `count`,
                            		  IFNULL((CASE
    											  WHEN users_cumunication.mail = 0 THEN 'E-Mail_OFF_OFF.png'
    											  WHEN users_cumunication.mail = 1 AND COUNT(mail_chat.id) = 0 THEN 'E-Mail_ON.png'
                                                  WHEN users_cumunication.mail = 1 AND COUNT(mail_chat.id) = 1 THEN 'E-Mail__BUSY.png'
                            					  WHEN users_cumunication.mail = 1 AND COUNT(mail_chat.id) > 1 THEN 'E-Mail__BUSY.png'
                            				  END),'E-Mail_OFF_OFF.png') AS `icon`
                            FROM      users_cumunication
                            JOIN      users ON users.id = users_cumunication.user_id
                            JOIN      user_info ON users.id = user_info.user_id
                            LEFT JOIN mail_chat ON mail_chat.last_user_id = users_cumunication.user_id AND mail_chat.`status` = 2
                            WHERE     user_info.`name` = '$name'");
            
            $r_mail = $db->getResultArray();
            $mail   = $r_mail[result][0];
            
            if($mail[count]==0){
                $mai = '<img src="media/images/icons/comunication/'.$mail[icon].'" height="14" width="14">';
            }elseif ($mail[count] == 1){
                $mai = '<img src="media/images/icons/comunication/'.$mail[icon].'" height="14" width="14">';
            }else{
                $mai = '<span><span style="position: absolute; font-size: 8px; margin-top: 2px; text-align: center; width: 12px; margin-left: 1px;color: #FFF;">'.$mail[count].'</span><img src="media/images/icons/comunication/'.$mail[icon].'" height="14" width="14"></span>';
            }
                
            $db->setQuery(" SELECT   COUNT(fb_chat.id) AS `count`,
                					 IFNULL((CASE
    											WHEN users_cumunication.messenger = 0 THEN 'FB_OFF.png'
    											WHEN users_cumunication.messenger = 1 AND COUNT(fb_chat.id) = 0 THEN 'FB_ON.png'
                                                WHEN users_cumunication.messenger = 1 AND COUNT(fb_chat.id) = 1 THEN 'FB_BUSY.png'
                								WHEN users_cumunication.messenger = 1 AND COUNT(fb_chat.id) > 1 THEN 'FB_BUSY1.png'
                							 END),'FB_OFF.png') AS `icon`
                            FROM      users_cumunication
                            JOIN      users ON users.id = users_cumunication.user_id
                            JOIN      user_info ON users.id = user_info.user_id
                            LEFT JOIN fb_chat ON fb_chat.last_user_id = users_cumunication.user_id AND fb_chat.`status` = 2
                            WHERE     user_info.`name` = '$name'");
            
            $r_fbm1 = $db->getResultArray();
            $fbm1   = $r_fbm1[result][0];
            
            if($fbm1[count]==0){
                $fbm = '<img src="media/images/icons/comunication/'.$fbm1[icon].'" height="14" width="14">';
            }elseif ($fbm1[count] == 1){
                $fbm = '<img src="media/images/icons/comunication/'.$fbm1[icon].'" height="14" width="14">';
            }else{
                $fbm = '<span><span style="position: absolute; font-size: 8px; margin-top: 2px; text-align: center; width: 12px; margin-left: 1px;color: #FFF;">'.$fbm1[count].'</span><img src="media/images/icons/comunication/'.$fbm1[icon].'" height="14" width="14"></span>';
            }
                
                
//                 $db->setQuery(" SELECT    COUNT(videocall.id) AS `count`,
//                                 		  IFNULL((CASE
//             											WHEN users_cumunication.video = 0 THEN 'VIDEO_OFF_OFF.png'
//             											WHEN users_cumunication.video = 1 AND COUNT(videocall.id) = 0 THEN 'VIDEO_ON.png'
//                                                         WHEN users_cumunication.video = 1 AND COUNT(videocall.id) = 1 THEN 'VIDEO_BUSY.png'
//                                 						WHEN users_cumunication.video = 1 AND COUNT(videocall.id) > 1 THEN 'VIDEO_BUSY.png'
//                                 				   END),'VIDEO_OFF_OFF.png') AS `icon`
//                                 FROM      users_cumunication
//                                 JOIN      users ON users.id = users_cumunication.user_id
//                                 JOIN      user_info ON users.id = user_info.user_id
//                                 LEFT JOIN videocall ON videocall.last_user_id = users_cumunication.user_id AND videocall.`status` = 2
//                                 WHERE     user_info.`name` = '$name'");
                
//                 $r_video1 = $db->getResultArray();
//                 $video1   = $r_video1[result][0];
                
//                 if($video1[count]==0){
//                     $video = '<img src="media/images/icons/comunication/'.$video1[icon].'" height="14" width="14">';
//                 }elseif ($video1[count] == 1){
//                     $video = '<img src="media/images/icons/comunication/'.$video1[icon].'" height="14" width="14">';
//                 }else{
//                     $video = '<span><span style="position: absolute; font-size: 8px; margin-top: 2px; text-align: center; width: 12px; margin-left: 1px;color: #FFF;">'.$video1[count].'</span><img src="media/images/icons/comunication/'.$video1[icon].'" height="14" width="14"></span>';
//                 }
                
            $db->setQuery("SELECT    users.online,
                                     IFNULL(users_call_dndlocalon.dndlocalon_status,0) AS dnd,
                                     users.work_activities_id,
                                     work_activities.name AS activitie_name,
                                     work_activities.color AS activitie_color
                           FROM     `users`
                           JOIN      user_info ON users.id = user_info.user_id 
                           LEFT JOIN work_activities ON work_activities.id = users.work_activities_id
                           LEFT JOIN users_call_dndlocalon ON users_call_dndlocalon.user_id = users.id                                                       
                           WHERE     user_info.`name` = '$name'");
            
            $r_gela = $db->getResultArray();
            $gela13 = $r_gela[result][0];
            
            $dnd_status = $gela13[dnd];
            $activitie_status = $gela13[work_activities_id] != 0 ? 1 : 0;
            $avtivitie_name = $gela13['activitie_name'];
            $avtivitie_color = $gela13['activitie_color'];
            

            if($dnd_status == 0) {
                $phone_icon = "<img alt='inner' style='margin-right:3px;' src=\"media/images/icons/$color[$aval]\" height='14' width='14'>";
            }elseif ($dnd_status == 1) {
                $phone_icon = "<img alt='inner' style='margin-right:3px;' src=\"media/images/icons/comunication/Phone_INSIDE.png\" height='14' width='14'>";
            }elseif ($dnd_status == 2) {
                $phone_icon = "<img alt='inner' style='margin-right:3px;' src=\"media/images/auricular.png\" height='14' width='14'>";
            }
    
            if ($checkState == 1) {
                
                if($clid == '2004'){
                    $db->setQuery("SELECT   access_log.ip 
                                   FROM    `asterisk_incomming`
                                   JOIN     access_log ON access_log.asterisk_incomming_id = asterisk_incomming.id
                                   WHERE    asterisk_incomming.dst_extension = '$aname'
                                     AND    asterisk_incomming.source = '2004'
                                   ORDER BY asterisk_incomming.id DESC LIMIT 1");
                    
                    $r_gh = $db->getResultArray();
                    $gh   = $r_gh[result][0];
                    $phone = $gh[ip];
                }else{ 
                    $phone = $clid;
                } 
                
                $data['mini_call_ext'] .= " <tr style=\"border: 1px solid #E6E6E6;\">
                                                <td style=\"font-size: 11px; width: 90px;\">$aname</td>
                                                <td colspan=\"2\" style='cursor: pointer; font-size: 11px;' id='cid' class='open_dialog' extention='$aname' number='$clid' ipaddres='$phone'>$phone</td>
                                                <td class='td_center'>
                                                    <div style=\"width:90px\">$phone_icon $gela $fbm $vib $mai</div>    
                                                </td>
                                             </tr>";
            }else{
                $data['call_ext'] .= "<tr style=\"border: 1px solid #E6E6E6;\">
                                          <td style=\"font-size: 11px;\">$qn</td>
                                          <td style=\"font-size: 11px;\">$aname</td>";
            }

            
            $db->setQuery("SELECT `name` AS `name`
                           FROM   `abonent_pin`
                           WHERE  `pin` != '' AND phone = '$clid' AND phone != 'Anonymous'
                           LIMIT 1");

            $result_name = $db->getResultArray();
            $clid_name=$result_name[result][0][name];
            
            if($clid == '2004'){
                $db->setQuery("SELECT ip FROM `access_log` ORDER BY id DESC LIMIT 1");
                $gh    = $db->getResultArray();
                $phone = $gh[result][0][ip];
                
            }else{
                $phone = $clid;
            }
            
            if ($checkState != 1) {

                if($activitie_status == 1) {
                    $user_name = '<div class="livestate-action-circ" title="'.$avtivitie_name.'" style="background:'.$avtivitie_color.'"></div>'.$name;
                } else {
                    $user_name = $name;
                }
                
                $data['call_ext'] .= "  <td style=\"font-size: 11px;\">$user_name</td>
                                            <td class='td_center' >
                                                <div style='width: 90px;'>$phone_icon $gela $fbm $vib $mai</div>
                                            </td>
                                            <td style='cursor: pointer; font-size: 11px;' id='cid' class='open_dialog' extention='$aname' ab_pin='$clid_name' number='$clid' ipaddres='$phone'>$phone</td>
                                            <td style=\"font-size: 11px;\">$res[time]</td>
                                            <td style=\"color: rgb(244, 56, 54); font-size: 12px;\"><div style=\"width: 100px;\">$clid_name</div></td>
                                         </tr>";
            }
        }
    



//QUEUE details

   $db->setQuery("SELECT    `queue`,
                            `position`,
                            `number`,
                             TIME_FORMAT(SEC_TO_TIME(UNIX_TIMESTAMP(now()) - `date`), '%i:%s')  AS `wait_time`
                   FROM     `asterisk_in_queue`
                   WHERE     NOT ISNULL(`queue`)
                   ORDER BY `queue` , `position`");

    $result1 = $db->getResultArray();

    $position  = 1;
    foreach($result1[result] as $res1) {
         
        $callerNumber = $res1[number];

        
        $db->setQuery("SELECT `name` AS `name`
                       FROM   `abonent_pin`
                       WHERE  `pin` != '' AND phone = '$callerNumber' AND phone != 'Anonymous'
                       LIMIT 1");


        $result_name = $db->getResultArray();
        $callername  = $result_name[result][0][name];
        $qn          = $res1[queue];
        $pixel      = '15px';
        if($callerNumber == '2004'){
            $db->setQuery("SELECT ip FROM `access_log` ORDER BY id DESC LIMIT 1");
            $gh = $db->getResultArray();
            $phone = $gh[result][0][ip];
        }else{
            $phone = $callerNumber;
        }

        if ($checkState == 1) {
            $data['mini_call_queue'] .= "<tr>
                    <td>$position</td>
                    <td colspan=\"3\">$phone</td>
    		      </tr>";
        }else{
            $data['call_queue'] .= "<tr>
                    <td>$qn</td>
                    <td>$position</td>
                    <td>$phone</td>
                    <td colspan=\"3\" style=\"color: rgb(244, 56, 54);\">$callername</td>
                    <td>".$res1[wait_time]." წუთი </td>
                 </tr>";
        }

        $position++;

    }
}
// $db->setQuery("SELECT id, COUNT(id) AS `count`, sender_id AS `call_id` FROM `videocall` where status in(0,1)");
// $chatvd = $db->getResultArray();
// $data['video_chat_id'] = $chatvd[result][0][id];
// $data['video_chat_call_id'] = $chatvd[result][0][call_id];
// $data['video_chat_count'] = $chatvd[result][0][count];

$db->setQuery("SELECT id, COUNT(id) AS `count` FROM `fb_chat` where status in(1)");
$chatfb = $db->getResultArray();
$data['fb_chat_id'] = $chatfb[result][0][id];
$data['fb_chat_count'] = $chatfb[result][0][count];

$db->setQuery("SELECT id, COUNT(id) AS `count` FROM `mail_chat` where status in(1)");
$chatmail = $db->getResultArray();
$data['mail_chat_id'] = $chatmail[result][0][id];
$data['mail_chat_count'] = $chatmail[result][0][count];

$db->setQuery("SELECT id, COUNT(id) AS `count` FROM `site_chat` where status in(1)");
$chatsite = $db->getResultArray();
$data['site_chat_id'] = $chatsite[result][0][id];
$data['site_chat_count'] = $chatsite[result][0][count];

$db->setQuery("SELECT id, COUNT(id) AS `count` FROM `chat` where status in(1)");
$chat22 = $db->getResultArray();
$data['chat_id'] = $chat22[result][0][id];
$data['chat_count'] = $chat22[result][0][count];

echo json_encode($data);
?>

