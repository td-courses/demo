<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
require_once('../includes/classes/class.Mysqli.php');



$begintime = $time;
$inuse      = Array();
$dict_queue = Array();
$user		= $_SESSION['USERID'];



$color['unavailable'] = "flesh_off.png";
$color['unknown']     = "#dadada";
$color['busy']        = "flesh_inc.png";//
$color['dialout']     = "flesh_out.png";//
$color['ringing']     = "flesh_ringing.png";//
$color['not in use']  = "flesh_free.png";
$color['paused']      = "#000000";//
$color['onhold']      = "pause.png";//
$color['dnd']         = "flesh_dnd.png";

$data = '';

////////////////////////////---------------------------------------------------------------------
$data .= '<table id="flesh_table">';
$data .= "<thead>";
$data .= "<tr>";
$data .= '<th style="width: 35px;">თანამშრომელი</th>';
$data .= '<th style="width: 115px;">შიდა ნომერი</th>';
$data .= '<th style="width: 175px;">სტატუსი</th>';
$data .= '<th style="width: 95px;">რიგი</th>';
$data .= '<th style="width: 35px;">ნომერი</th>';
$data .= '<th style="width: 105px;">დრო</th>';
$data .= "</tr>\n";
$data .= "</thead><tbody>\n";

$mysqli = new dbClass();
$mysqli->setQuery("SELECT  `user`,`queue`,`ext_number`,`ext_name`,`phone`,`time`,`status`
                   FROM(SELECT     `user_info`.`name` AS `user`,
                                   `asterisk_in_call`.`queue` AS `queue`,
                                   `asterisk_in_call`.`ext` AS `ext_number`,
                                   `asterisk_in_call`.`ext_name` AS `ext_name`,
                                   `asterisk_in_call`.`phone` AS `phone`,
                                   `asterisk_in_call`.`time` AS `time`,
                                    IF (`asterisk_in_call`.`status` = 'dnd',IF (`asterisk_in_call`.`peer_status` = 'ok',`asterisk_in_call`.`status`,'unavailable'),`asterisk_in_call`.`status`) AS `status`
                         FROM      `asterisk_in_call`
                         LEFT JOIN `user_info` ON `user_info`.`user_id` = `asterisk_in_call`.`user_id`
                         WHERE     `asterisk_in_call`.`status` IN ('busy','dialout','ringing','paused','onhold')
                         GROUP BY  `asterisk_in_call`.`ext_name`
                         UNION ALL
                         SELECT     `user_info`.`name` AS `user`,
                                    `asterisk_in_call`.`queue` AS `queue`,
                                    `asterisk_in_call`.`ext` AS `ext_number`,
                                    `asterisk_in_call`.`ext_name` AS `ext_name`,
                                    '' AS `phone`,
                                    '' AS `time`,
                                    IF (`asterisk_in_call`.`status` = 'dnd',IF (`asterisk_in_call`.`peer_status` = 'ok',`asterisk_in_call`.`status`,'unavailable'),`asterisk_in_call`.`status`) AS `status`
                         FROM       `asterisk_in_call`
                         LEFT JOIN  `user_info` ON `user_info`.`user_id` = `asterisk_in_call`.`user_id` AND user_info.user_id != 0
                         WHERE      `asterisk_in_call`.`ext_name` NOT IN (SELECT asterisk_in_call.ext_name
                                                                          FROM asterisk_in_call
                                                                          WHERE asterisk_in_call.`status` IN ('busy','dialout','ringing','paused','onhold')
                                                                          GROUP BY `asterisk_in_call`.`ext_name`)
                        GROUP BY   `asterisk_in_call`.`ext_name`) results
                        WHERE NOT   isnull(`phone`)
                        ORDER BY   `user` DESC, `ext_name` ASC");

$arr=$mysqli->getResultArray();

foreach($arr['result']  as $res) {
    $status =$res['status'];
    $data .= '<tr  queue="'.$res['queue'].'" dep="'.$res[""].'" ext="'.$res['ext_number'].'" user="'.$res['user'].'" state="'.$color[$status].'" >';
    $data .= "<td>$res[user]</td>";
    $data .= "<td>$res[ext_name]</td>";
    $data .= '<td class="td_center"><img alt="inner" src="media/images/icons/'.$color[$status].'" height="14" width="14"></td>';
    $data  .= "<td>$res[queue]</td><td>$res[phone]</td><td>$res[time]</td>";
}

$data .= "</tr>";
$data .= "</tbody>";
$data .= "</table>\n";
$data .= "<div id='empty-data' style='display: none;'><span>ჩანაწერი ვერ მოიძებნა</span></div>";

///QUEUE details

$mysqli->setQuery("SELECT   `queue`,
                            `position`,
                            `number`,
                             TIME_FORMAT(SEC_TO_TIME(UNIX_TIMESTAMP(now()) - `date`), '%i:%s')  AS `wait_time`
                   FROM     `asterisk_in_queue`
                   WHERE     NOT ISNULL(`queue`)
                   ORDER BY `queue` , `position`");

$arr=$mysqli->getResultArray();





$data .= '<table id="tb" queue="'.$qn.'">';
$data .= "<thead>";
$data .= "<tr>";
$data .= "<th>რიგი</th>";
$data .= "<th>პოზიცია</th>";
$data .= "<th>ნომერი</th>";
$data .= "<th>ლოდინის დრო</th>";
$data .= "</tr>\n";
$data .= "</thead>\n";
$data .= "<tbody>\n";



foreach($arr['result'] as $res) {
    
    
    
    $data .= "<tr $odd>";
    $data .= "<td>$res[queue]</td><td>$res[position]</td>";
    $data .= "<td>".$res[number]."</td>";
    $data .= "<td>".$res[wait_time]." წუთი</td>";
    $data .= "</tr>";
}



$data .= "</tbody>\n";
$data .= "</table>\n";




echo $data;

?>

