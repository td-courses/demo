<?php
require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db     = new dbClass();

//----------------------------- ცვლადი

$agent	    = $_REQUEST['agent'];
$name	    = $_REQUEST['name'];
$queue	    = $_REQUEST['queuet'];
$start_time = $_REQUEST['start_time'];
$end_time 	= $_REQUEST['end_time'];
$all_call = 0;



if($_REQUEST['act'] =='check'){

		
//--------------------------- ნაპასუხები ზარები ოპერატორების მიხედვით

    $db->setQuery("SELECT COUNT(*) AS all_count
			       FROM   asterisk_incomming
                   JOIN   user_info ON asterisk_incomming.user_id = user_info.user_id
                   JOIN   incomming_call ON incomming_call.asterisk_incomming_id = asterisk_incomming.id
			       WHERE  DATE(`asterisk_incomming`.`call_datetime`) BETWEEN '$start_time' AND '$end_time'
                   AND    user_info.`name` IN($agent)
                   AND    asterisk_incomming.disconnect_cause IN(3,4)
			       AND    NOT ISNULL(asterisk_incomming.user_id)");
    $result = $db->getResultArray();
    global $all_call;
    $all_call = $result[result][0][all_count];

 	$db->setQuery("SELECT       user_info.id,
        						user_info.`name` as name,
        						COUNT(*) AS zarebi,
        						SUM(IF(ISNULL(incomming_call.`user_id`),1,0)) AS daumushavebuli,
        						SUM(IF(NOT ISNULL(incomming_call.`user_id`),1,0)) AS damushavebuli,
        						0 AS out_call,
        						0 AS gamavali_damushavebuli,
        						0 AS task_count,
        						SEC_TO_TIME(SUM(asterisk_incomming.duration)) AS saubris_dro,
        						SEC_TO_TIME(ROUND((SUM(asterisk_incomming.duration)/COUNT(*)),0)) AS sashualo,
        						SEC_TO_TIME(MAX(asterisk_incomming.duration)) AS max_time,
        						SEC_TO_TIME(MIN(asterisk_incomming.duration)) AS min_time,
        						asterisk_incomming.user_id as `user_id`
                    FROM        asterisk_incomming
                    JOIN 		user_info ON asterisk_incomming.user_id = user_info.user_id
                    JOIN 		incomming_call ON incomming_call.asterisk_incomming_id = asterisk_incomming.id
                    WHERE 	  	user_info.`name` IN($agent)
                    AND 	    DATE(`asterisk_incomming`.`call_datetime`) BETWEEN '$start_time' AND '$end_time'
                    AND         asterisk_incomming.dst_queue = $queue
                    AND         asterisk_incomming.disconnect_cause IN(3,4)
                    GROUP BY    asterisk_incomming.user_id");
 	$ress = $db->getResultArray();
 	foreach($ress[result] AS $row){
        global $all_call;
        
        $percent                 = round(($row['zarebi']/$all_call)*100,2);
    	$coeficienti             = round($row['damushavebuli']/$row['zarebi'],2);
        $gamavali_daumushavebeli = $row['out_call'] - $row['gamavali_damushavebuli'];
        $user_id                 = $row['user_id'];
    	
    	$data['page']['answer_call_by_queue'] .= '
                        <tr>
        					<td style="cursor:pointer;" id="name">'.$row['name'].'</td>
        					<td id="answear_dialog" style="cursor: pointer; text-decoration: underline;" user="'.$row['name'].'">'.$row['zarebi'].' ზარი</td>
        					<td>'.$row['damushavebuli'].' ზარი</td>
        					<td id="undone_dialog" style="cursor: pointer; text-decoration: underline;" user1="'.$row['name'].'">'.$row['daumushavebuli'].' ზარი</td>
        					<td>'.$percent.'%</td>
        					<td>'.$row['saubris_dro'].'</td>
        					<td>'.$row['sashualo'].'</td>
        					<td>'.$row['max_time'].'</td>
        					<td>'.$row['min_time'].'</td>
        					<td id="answear_dialog1" style="cursor: pointer; text-decoration: underline;" user2="'.$row['name'].'">'.$row['out_call'].'  ზარი</td>
        					<td>'.$row['gamavali_damushavebuli'].'ზარი</td>
        					<td id="undone_dialog1" style="cursor: pointer;" user3="'.$row['name'].'">'.$gamavali_daumushavebeli.'  ზარი</td>
    					</tr>';
    }

//------------------------------//
}else if($_REQUEST['act'] =='answear_dialog_table'){
    $data	= array('page' => array('answear_dialog' => ''));
	$count  = $_REQUEST['count'];
	$hidden = $_REQUEST['hidden'];
	
 	$db->setQuery(" SELECT asterisk_incomming.id,
                    	   asterisk_incomming.call_datetime,
                    	   asterisk_incomming.source,
                    	   asterisk_incomming.dst_queue,
                    	   user_info.`name`,
                    	   SEC_TO_TIME(asterisk_incomming.duration),
                           concat('<button class=\"download\" str=',`asterisk_incomming`.`file_name`,'>შემომავალი</button>')
                    FROM   asterisk_incomming
                    JOIN   user_info ON asterisk_incomming.user_id = user_info.user_id
                    JOIN   incomming_call ON incomming_call.asterisk_incomming_id = asterisk_incomming.id
                    WHERE  DATE(`asterisk_incomming`.`call_datetime`) BETWEEN '$start_time' AND '$end_time'
                    AND    user_info.`name` IN('$name')
                    AND    asterisk_incomming.dst_queue = $queue
                    AND    asterisk_incomming.disconnect_cause IN(3,4)
                    AND    NOT ISNULL(asterisk_incomming.user_id)");
 	
 	$data = $db->getList($count, $hidden);
 	
}elseif($_REQUEST['act'] =='undone_dialog_table'){
    $data	= array('page' => array('answear_dialog' => ''));
    $count  = $_REQUEST['count'];
    $hidden = $_REQUEST['hidden'];
    
    $db->setQuery("SELECT  asterisk_incomming.id,
                    	   asterisk_incomming.call_datetime,
                    	   asterisk_incomming.source,
                    	   asterisk_incomming.dst_queue,
                    	   user_info.`name`,
                    	   SEC_TO_TIME(asterisk_incomming.duration),
                           concat('<button class=\"download4\" str=',`asterisk_incomming`.`file_name`,'>დაუმ.შემომავ...</button>')
                    FROM   asterisk_incomming
                    JOIN   user_info ON asterisk_incomming.user_id = user_info.user_id
                    JOIN   incomming_call ON incomming_call.asterisk_incomming_id = asterisk_incomming.id
                    WHERE  DATE(`asterisk_incomming`.`call_datetime`) BETWEEN '$start_time' AND '$end_time'
                    AND    user_info.`name` IN('$name')
                    AND    asterisk_incomming.dst_queue = $queue
                    AND    asterisk_incomming.disconnect_cause IN(3,4)
                    AND    ISNULL(incomming_call.`user_id`)
                    AND    NOT ISNULL(asterisk_incomming.user_id)");
    
    $data = $db->getList($count, $hidden);
    
}else if($_REQUEST['act'] =='answear_dialog_table1'){
    
    $data	= array('page' => array('answear_dialog' => ''));
	$count  = $_REQUEST['count'];
	$hidden = $_REQUEST['hidden'];
	
 	$db->setQuery("");
	$data = $db->getList($count, $hidden);
	
	//------------------------------//
		
}elseif($_REQUEST['act'] =='undone_dialog'){
        $data['page']['answear_dialog'] = '
            <table class="display" id="example2" style="width: 100%;">
                <thead>
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width: 16%;">თარიღი</th>
                        <th style="width: 16%;">ადრესატი</th>
            			<th style="width: 16%;">ექსთენშენი</th>
            			<th style="width: 20%;">ოპერატორი</th>
                        <th style="width: 16%;">დრო</th>
                        <th style="width: 16%;">ქმედება</th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                        	<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
                        </th>
                        <th>
                        	<input type="text" name="search_number" value="ფილტრი" class="search_init" style="width: 80%;">
            			</th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>                            
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>
                        <th>
                            <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>
            			<th>
                            <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>
            		</tr>
                </thead>
            </table>';
    
    }else if($_REQUEST['act'] =='answear_dialog'){

	   $data['page']['answear_dialog'] = '
			<table class="display" id="example" style="width: 100%;">
                <thead>
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width: 16%;">თარიღი</th>
                        <th style="width: 16%;">წყარო</th>
                        <th style="width: 16%;">ადრესატი</th>
						<th style="width: 20%;">ოპერატორი</th>
                        <th style="width: 16%;">დრო</th>
                        <th style="width: 16%;">ქმედება</th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                        	<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
                        </th>
                        <th>
                        	<input type="text" name="search_number" value="ფილტრი" class="search_init" style="width: 80%;">
						</th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>                            
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>
                        <th>
                            <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>
						<th>
                            <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>
					</tr>
                </thead>
            </table>';
	}elseif($_REQUEST['act'] =='undone_dialog1'){
        $data['page']['answear_dialog'] = '
			<table class="display" id="example2" style="width: 100%;">
                <thead>
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width: 20%;">თარიღი</th>
                        <th style="width: 20%;">ადრესატი</th>
    					<th style="width: 20%;">ოპერატორი</th>
                        <th style="width: 20%;">დრო</th>
                        <th style="width: 20%;">ქმედება</th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                        	<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
                        </th>
                        <th>
                        	<input type="text" name="search_number" value="ფილტრი" class="search_init" style="width: 80%;">
    					</th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>                            
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80%;" />
                        </th>
                        <th>
                            <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80%;" />
                        </th>
    				</tr>
                </thead>
            </table>';
    }else if($_REQUEST['act'] =='answear_dialog1'){

	   $data['page']['answear_dialog'] = '
			<table class="display" id="example2_1" style="width: 100%;">
                <thead>
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width: 20%;">თარიღი</th>
                        <th style="width: 20%;">ადრესატი</th>
						<th style="width: 20%;">ოპერატორი</th>
                        <th style="width: 20%;">დრო</th>
                        <th style="width: 20%;">ქმედება</th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                        	<input type="text" name="search_id" value="ფილტრი" class="search_init"/>
                        </th>
                        <th>
                        	<input type="text" name="search_number" value="ფილტრი" class="search_init" style="width: 80%;">
						</th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>                            
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>
                        <th>
                            <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>
					</tr>
                </thead>
            </table>';
    }
    
echo json_encode($data);
?>