<?php

/* ******************************
 *	Request aJax actions
* ******************************
*/

require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db     = new dbClass();
$action 	= $_REQUEST['act'];
$error		= '';
$data		= array();

switch ($action) {
	case 'get_list' :
	    $start_time = $_REQUEST['start_time'];
	    $end_time 	= $_REQUEST['end_time'];
		$count 		= $_REQUEST['count'];
		
  		$db->setQuery("SELECT   sent_sms.id,
								sent_sms.date,
								user_info.name AS author,
								IFNULL(sent_sms.phone,sent_sms.personal_number),
								incomming_call.client_user_id,
								sent_sms.content,
								CASE
									WHEN status = 0 THEN 'სერვისის შეცდომა'
									WHEN status = 2 THEN 'გაიგზავნა'
									WHEN status = 1 THEN 'არ გაიგზავნა'
								END AS sent_status
					  FROM      sent_sms
					  JOIN      user_info ON sent_sms.user_id = user_info.user_id
					  LEFT JOIN incomming_call ON incomming_call.id = sent_sms.incomming_call_id
  		              WHERE DATE(sent_sms.date)>='$start_time' AND DATE(sent_sms.date)<='$end_time'
					  ORDER BY  sent_sms.id DESC");
	  
		$data = $db->getList($count, $hidden);
			
		break;		
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Request Functions
* ******************************
*/

?>
