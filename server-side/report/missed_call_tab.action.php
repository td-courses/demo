<?php
require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();
header('Content-Type: application/json');
$start = $_REQUEST['start'];
$end   = $_REQUEST['end'];
$agent = $_REQUEST['agent'];
$queuet = $_REQUEST['queuet'];

         $db->setQuery("SELECT   COUNT(*) AS `count`,
                    			'30 წამი' AS `cause`
                        FROM 	`asterisk_incomming`
                        WHERE	wait_time < 31
                        AND call_datetime >= '$start'
                        AND call_datetime <= '$end'
                        AND dst_queue in ($queuet)
                        AND disconnect_cause = '2'
                        UNION ALL
                        SELECT   COUNT(*) AS `count`,
                    			'31 - 60 წამი' AS `cause`
                        FROM 	`asterisk_incomming`
                        WHERE	wait_time > 30 AND wait_time <61
                        AND call_datetime >= '$start'
                        AND call_datetime <= '$end'
                        AND dst_queue in ($queuet)
                        AND disconnect_cause = '2'
                        UNION ALL
                        SELECT   COUNT(*) AS `count`,
                    			'61 -120 წამი' AS `cause`
                        FROM 	`asterisk_incomming`
                        WHERE	wait_time > 60 AND wait_time <121
                        AND call_datetime >= '$start'
                        AND call_datetime <= '$end'
                        AND dst_queue in ($queuet)
                        AND disconnect_cause = '2'
                        UNION ALL
                        SELECT   COUNT(*) AS `count`,
                    			'2 წუთზე მეტი' AS `cause`
                        FROM 	`asterisk_incomming`
                        WHERE	wait_time > 120
                        AND call_datetime >= '$start'
                        AND call_datetime <= '$end'
                        AND dst_queue in ($queuet)
                        AND disconnect_cause = '2'");


$row = array();
$rows = array();

$result = $db->getResultArray(MYSQLI_NUM);
foreach($result[result] AS $r) {
	$row[0] = $r[1].': '.$r[0];
	$row[1] = (float)$r[0];
	array_push($rows,$row);
}


echo json_encode($rows);

?>