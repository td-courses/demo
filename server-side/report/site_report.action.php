<?php
require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db     = new dbClass();
$start  	= $_REQUEST['start'];
$end    	= $_REQUEST['end'];
$count 		= $_REQUEST["count"];
$action 	= $_REQUEST['act'];
$departament= $_REQUEST['departament'];
$type       = $_REQUEST['type'];
$category   = $_REQUEST['category'];
$s_category = $_REQUEST['sub_category'];
$done 		= $_REQUEST['done']%4;
$name 		= $_REQUEST['name'];
$title 		= $_REQUEST['title'];
$source_info   = $_REQUEST['source_info'];
$text[0] 	= "შემოსული  ზარები საიტების მიხედვით";
$text[1] 	= "'$departament'- შემოსული ზარები  კატეგორიების მიხედვით";
$text[2] 	= "'$departament'- შემოსული ზარები ქვე-კატეგორიების  მიხედვით";
$text[3] 	= "'$departament'- შემოსული  ქვე-ქვე–კატეგორიის მიხედვით";

//------------------------------------------------query-------------------------------------------
switch ($done){
    case  1:
        $db->setQuery("SELECT    info_category.`name`,
                                 COUNT(*),
                				 ROUND(COUNT(*)/(SELECT COUNT(*)
												 FROM  `incoming_call_site`
												 JOIN   incomming_call ON incomming_call.id = incoming_call_site.incomming_call_id
												 JOIN   my_web_site ON incoming_call_site.site_id = my_web_site.id
												 JOIN   info_category ON info_category.id = incomming_call.cat_1
												 WHERE  DATE(incomming_call.date) BETWEEN '$start' AND '$end' AND my_web_site.`name` = '$_REQUEST[departament]')*100,2) AS `percent`
                        FROM    `incoming_call_site`
                        JOIN     incomming_call ON incomming_call.id = incoming_call_site.incomming_call_id
                        JOIN     my_web_site ON incoming_call_site.site_id = my_web_site.id
                        JOIN     info_category ON info_category.id = incomming_call.cat_1
                        WHERE    DATE(incomming_call.date) BETWEEN '$start' AND '$end' AND my_web_site.`name` = '$_REQUEST[departament]'
                        GROUP BY incomming_call.cat_1");
        $text[0]=$text[1];
        break;
    case  2:
        $db->setQuery(" SELECT   inf_cat.`name`,
                                 COUNT(*),
                        		 ROUND(COUNT(*)/(SELECT COUNT(*)
												 FROM  `incoming_call_site`
												 JOIN   incomming_call ON incomming_call.id = incoming_call_site.incomming_call_id
												 JOIN   my_web_site ON incoming_call_site.site_id = my_web_site.id
												 JOIN   info_category ON info_category.id = incomming_call.cat_1
												 JOIN   info_category AS inf_cat ON inf_cat.id = incomming_call.cat_1_1
												 WHERE  DATE(incomming_call.date) BETWEEN '$start' AND '$end' AND my_web_site.`name` = '$_REQUEST[departament]' AND info_category.`name` = '$_REQUEST[type]')*100,2) AS `percent`
                        FROM    `incoming_call_site`
                        JOIN     incomming_call ON incomming_call.id = incoming_call_site.incomming_call_id
                        JOIN     my_web_site ON incoming_call_site.site_id = my_web_site.id
                        JOIN     info_category ON info_category.id = incomming_call.cat_1
                        JOIN     info_category AS inf_cat ON inf_cat.id = incomming_call.cat_1_1
                        WHERE    DATE(incomming_call.date) BETWEEN '$start' AND '$end' AND my_web_site.`name` = '$_REQUEST[departament]' AND info_category.`name` = '$_REQUEST[type]'
                        GROUP BY inf_cat.`name`");
        $text[0]=$text[2];
        break;
    case  3:
        $db->setQuery("SELECT    IF(inf_cat1.`name`!='',inf_cat1.`name`,'არ აქვს კატეგორია') as type,
                                 COUNT(*),
                				 ROUND(COUNT(*)/(SELECT COUNT(*)
												 FROM  `incoming_call_site`
												 JOIN   incomming_call ON incomming_call.id = incoming_call_site.incomming_call_id
												 JOIN   my_web_site ON incoming_call_site.site_id = my_web_site.id
												 JOIN   info_category ON info_category.id = incomming_call.cat_1
												 JOIN   info_category AS inf_cat ON inf_cat.id = incomming_call.cat_1_1
												 LEFT JOIN info_category AS inf_cat1 ON inf_cat1.id = incomming_call.cat_1_1_1
												 WHERE  DATE(incomming_call.date) BETWEEN '$start' AND '$end' AND my_web_site.`name` = '$_REQUEST[departament]' AND info_category.`name` = '$_REQUEST[type]' AND inf_cat.`name` = '$_REQUEST[category]')*100,2) AS `percent`
                        FROM    `incoming_call_site`
                        JOIN     incomming_call ON incomming_call.id = incoming_call_site.incomming_call_id
                        JOIN     my_web_site ON incoming_call_site.site_id = my_web_site.id
                        JOIN     info_category ON info_category.id = incomming_call.cat_1
                        JOIN     info_category AS inf_cat ON inf_cat.id = incomming_call.cat_1_1
                        LEFT JOIN info_category AS inf_cat1 ON inf_cat1.id = incomming_call.cat_1_1_1
                        WHERE    DATE(incomming_call.date) BETWEEN '$start' AND '$end' AND my_web_site.`name` = '$_REQUEST[departament]' AND info_category.`name` = '$_REQUEST[type]' AND inf_cat.`name` = '$_REQUEST[category]'
                        GROUP BY inf_cat1.`name`");
        $text[0]=$text[3];
        break;
    default:
        $db->setQuery("SELECT   my_web_site.`name`,
                                COUNT(*),
                        		ROUND(COUNT(*)/(SELECT COUNT(*)
												FROM  `incoming_call_site`
												JOIN   incomming_call ON incomming_call.id = incoming_call_site.incomming_call_id
												JOIN   my_web_site ON incoming_call_site.site_id = my_web_site.id
												WHERE  DATE(incomming_call.date) BETWEEN '$start' AND '$end')*100,2) AS `percent`
                        FROM    `incoming_call_site`
                        JOIN     incomming_call ON incomming_call.id = incoming_call_site.incomming_call_id
                        JOIN     my_web_site ON incoming_call_site.site_id = my_web_site.id
                        WHERE    DATE(incomming_call.date) BETWEEN '$start' AND '$end'
                        GROUP BY incoming_call_site.site_id");
        
        break;
}
///----------------------------------------------act------------------------------------------
switch ($action) {
    case "get_list":
        $data = $db->getList($count, "no");
        echo json_encode($data); return 0;
        break;
    case 'get_category' :
        $rows = array();
        $result = $db->getResultArray(MYSQLI_NUM);
        foreach ($result[result] AS $r) {
            $row[0] = $r[0];
            $row[1] = (float) $r[1];
            $rows['data'][]=$row;
        }
        $rows['text']=$text[0];
        echo json_encode($rows);
        break;
    case 'get_in_page':
        
        if($_REQUEST[rid] == 'არ აქვს კატეგორია'){
            $rid = 'AND incomming_call.cat_1_1_1 = 999';
        }else{
            $rid = "AND cat_1_1_1.`name` = '$_REQUEST[rid]'";
        }
        
        
        $db->setQuery(" SELECT      IF(asterisk_incomming.disconnect_cause = 2, '', incomming_call.id),
                                    incomming_call.id,
                                    incomming_call.date,
                                    incomming_call.client_name,
                                    incomming_call.client_user_id,
                                    incomming_call.phone,
                                    incomming_call.client_mail,
                                    (SELECT GROUP_CONCAT(`name`)
                                    FROM  `incoming_call_site`
                                    JOIN   my_web_site ON my_web_site.id = incoming_call_site.site_id
                                    WHERE  incomming_call_id = incomming_call.id),
                                    IFNULL(user_info1.`name`,user_info.`name`) AS `user_name`,
                                    inc_status.`name`,
                                    TIME_FORMAT(SEC_TO_TIME(asterisk_incomming.duration),'%i:%s') AS `duration`,
                                    CASE
                                        WHEN (NOT ISNULL(incomming_call.`chat_id`) AND NOT ISNULL(incomming_call.`cat_1`) AND NOT ISNULL(incomming_call.`cat_1_1`))
                                            THEN concat(_utf8 '<button class=\'download7\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/Chat.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დამუშავებ.ჩატი</span></button>')
                                        WHEN (NOT ISNULL(incomming_call.`chat_id`) AND (ISNULL(incomming_call.`cat_1`) OR ISNULL(incomming_call.`cat_1_1`)))
                                            THEN concat(_utf8 '<button class=\'download8\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/Chat.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დაუმუშავე.ჩატი</span></button>')
                                        
                                        WHEN (NOT ISNULL(incomming_call.`mail_chat_id`) AND (ISNULL(incomming_call.`cat_1`) OR ISNULL(incomming_call.`cat_1_1`)))
                                            THEN concat(_utf8 '<button class=\'download15\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/E-MAIL.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დაუმუშა.მეილი</span></button>')
                                        WHEN (NOT ISNULL(incomming_call.`mail_chat_id`) AND NOT ISNULL(incomming_call.`cat_1`) AND NOT ISNULL(incomming_call.`cat_1_1`))
                                            THEN concat(_utf8 '<button class=\'download15\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/E-MAIL.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დამუშავ.მეილი</span></button>')
                                        
                                        WHEN (NOT ISNULL(incomming_call.`site_chat_id`) AND (ISNULL(incomming_call.`cat_1`) OR ISNULL(incomming_call.`cat_1_1`)))
                                            THEN concat(_utf8 '<button class=\'download10\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/site.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დაუმუშ.ვაიბერი</span></button>')
                                        WHEN (NOT ISNULL(incomming_call.`site_chat_id`) AND NOT ISNULL(incomming_call.`cat_1`) AND NOT ISNULL(incomming_call.`cat_1_1`))
                                            THEN concat(_utf8 '<button class=\'download10\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/site.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დამუშა.ვაიბერი</span></button>')
                                        
                                        WHEN (NOT ISNULL(incomming_call.`fb_chat_id`) AND (ISNULL(incomming_call.`cat_1`) OR ISNULL(incomming_call.`cat_1_1`)))
                                            THEN concat(_utf8 '<button class=\'download11\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/Messenger.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დაუმ.მესენჯერი</span></button>')
                                        WHEN (NOT ISNULL(incomming_call.`fb_chat_id`) AND NOT ISNULL(incomming_call.`cat_1`) AND NOT ISNULL(incomming_call.`cat_1_1`))
                                            THEN concat(_utf8 '<button class=\'download11\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/Messenger.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დამუ.მესენჯერი</span></button>')
                                        
                                        WHEN (NOT ISNULL(incomming_call.`messenger_chat_id`) AND (ISNULL(incomming_call.`cat_1`) OR ISNULL(incomming_call.`cat_1_1`)))
                                            THEN concat(_utf8 '<button class=\'download13\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/CB.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დაუმ.მესენჯერი</span></button>')
                                        WHEN (NOT ISNULL(incomming_call.`messenger_chat_id`) AND NOT ISNULL(incomming_call.`cat_1`) AND NOT ISNULL(incomming_call.`cat_1_1`))
                                            THEN concat(_utf8 '<button class=\'download13\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/CB.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დამუ.მესენჯერი</span></button>')
                                        
                                        WHEN (NOT ISNULL(incomming_call.`video_call_id`) AND (ISNULL(incomming_call.`cat_1`) OR ISNULL(incomming_call.`cat_1_1`)))
                                            THEN concat(_utf8 '<button class=\'download14\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/Video.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დაუმუ. ვიდეო</span></button>')
                                        WHEN (NOT ISNULL(incomming_call.`video_call_id`) AND NOT ISNULL(incomming_call.`cat_1`) AND NOT ISNULL(incomming_call.`cat_1_1`))
                                            THEN concat(_utf8 '<button class=\'download14\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/Video.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დამუშ. ვიდეო</span></button>')
                                        
                                        WHEN `asterisk_incomming`.`disconnect_cause` = 2
                                            THEN concat('<button class=\'download2\' str=',CONCAT(DATE_FORMAT(asterisk_incomming.call_datetime,'%Y/%m/%d/'),asterisk_incomming.file_name),'><span style=\"vertical-align: -1px; font-size: 10px;\">უპასუხო</span></button>')
                                        WHEN `asterisk_incomming`.`disconnect_cause` IN(3,4)
                                            THEN IF (isnull(incomming_call.`user_id`),concat('<button class=\'download4\' str=',CONCAT(DATE_FORMAT(asterisk_incomming.call_datetime,'%Y/%m/%d/'),asterisk_incomming.file_name),'><img style=\"float: left;\" src=\"media/images/icons/comunication/phone.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დაუმ.შემომავა.</span></button>'),IF(incomming_call.transfer=1, concat('<button class=\'download5\' str=',CONCAT(DATE_FORMAT(asterisk_incomming.call_datetime,'%Y/%m/%d/'),asterisk_incomming.file_name),'><span style=\"vertical-align: -1px; font-size: 10px;\">გადართული  </span></button>'),concat('<button class=\'download\' str=',CONCAT(DATE_FORMAT(asterisk_incomming.call_datetime,'%Y/%m/%d/'),asterisk_incomming.file_name),'><img style=\"float: left;\" src=\"media/images/icons/comunication/phone.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">შემომავ. ზარი</span></button>')))
                                        ELSE '<button style=\"height: 22px; background-color: #2dc100;\" class=\"download15\"\><span style=\"vertical-align: 1px; font-size: 13px;\">მანუალური</span></button>მანუალური'
                                    END AS `file_name`
                        FROM        incoming_call_site
                        JOIN        incomming_call ON incomming_call.id = incoming_call_site.incomming_call_id
                        JOIN        my_web_site ON incoming_call_site.site_id = my_web_site.id
                        LEFT JOIN   asterisk_incomming ON asterisk_incomming.id = incomming_call.asterisk_incomming_id
                        LEFT JOIN   user_info ON user_info.user_id = asterisk_incomming.user_id
                        LEFT JOIN   user_info AS `user_info1` ON user_info1.user_id = incomming_call.user_id
                        LEFT JOIN   inc_status ON inc_status.id = incomming_call.inc_status_id
                        JOIN        source ON incomming_call.source_id = source.id
                        JOIN	    info_category AS cat_1 ON incomming_call.cat_1 = cat_1.id
                        LEFT JOIN	info_category AS cat_1_1 ON incomming_call.cat_1_1 = cat_1_1.id
                        LEFT JOIN	info_category AS cat_1_1_1 ON incomming_call.cat_1_1_1 = cat_1_1_1.id
                        WHERE       DATE(incomming_call.date) BETWEEN '$start' AND '$end'
                        $rid
                        AND cat_1_1.`name` = '$_REQUEST[sub_category]'
                        AND cat_1.`name` = '$_REQUEST[category]'
                        AND my_web_site.name = '$_REQUEST[type]'");
                         
            $data = $db->getList($count, 0);
            echo json_encode($data); return 0;
            break;
    default :
        echo "Action Is Null!";
        break;
        
}



?>