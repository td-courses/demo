<?php
require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();
$done         = $_REQUEST['done'];
$start_time   = $_REQUEST['start'];
$end_time     = $_REQUEST['end'];
$procent      = $_REQUEST['procent'];
$number       = $_REQUEST['number'];
$day          = $_REQUEST['day'];
$week_day     = $_REQUEST['users'];
$users_query  = "";

if($week_day == 'all'){
    $check_wwek = "";
}else{
    $check_wwek = " AND WEEKDAY(FROM_UNIXTIME(sl.datetime)) = $week_day";
}
//------------------------------------------------query-------------------------------------------

    if($done == 1){
    	$db->setQuery(" SELECT 'SL-ფაქტიური' AS `status`,
                                sl_info.date AS `date`,
                        		ROUND(SUM(IF(sl_info.avg_duration<$number,1,0))/COUNT(*)*100,2) AS percent,
                        		COUNT(*) AS `num`
                        FROM (SELECT   ROUND(SUM(sl.duration)/COUNT(*)) AS avg_duration,
                                       DATE(FROM_UNIXTIME(sl.datetime)) AS `date`
                			  FROM    `dashboard_chat_durations` AS sl
                			  WHERE    sl.`source_id` = 2 AND sl.`status` = 1 AND DATE(FROM_UNIXTIME(sl.datetime)) BETWEEN '$start_time' AND '$end_time' $check_wwek
                			  GROUP BY DATE(FROM_UNIXTIME(sl.datetime)), sl.chat_id) AS `sl_info`");
    	$result = $db->getResultArray();
    	       
    	foreach ($result[result] AS $row) {
    	    $name_answer[]     = $row['status'];
    	    $percent_answer[] = (float)$row['percent'];
    	    $count_answer[] = (float)$row['num'];
    	    $date[] = $row['date'];
    	    $limit_number[] = (float)$number;
    	    $limit_percent[] = (float)$procent;
    	    $count_all[]     = (float)$row['num'];
    	}
    	
    	
    }elseif($done == 2){
    	$db->setQuery(" SELECT 'SL-ფაქტიური' AS `status`,
                                HOUR(sl_info.date) AS `hourCount`,
                                CONCAT(HOUR(sl_info.date), ':00') AS `hour`,
                        		ROUND(SUM(IF(sl_info.avg_duration<$number,1,0))/COUNT(*)*100,2) AS percent,
                        		COUNT(*) AS `total`
                        FROM (SELECT   ROUND(SUM(sl.duration)/COUNT(*)) AS avg_duration,
                                       FROM_UNIXTIME(sl.datetime) AS `date`
                			  FROM    `dashboard_chat_durations` AS sl
                			  WHERE    sl.`source_id` = 2 AND sl.`status` = 1 AND DATE(FROM_UNIXTIME(sl.datetime)) = '$day'
                              $check_wwek $users_query
                			  GROUP BY HOUR(FROM_UNIXTIME(sl.datetime)), sl.chat_id) AS `sl_info`
                        GROUP BY HOUR(sl_info.date)");
    	$result = $db->getResultArray(MYSQLI_NUM);

    	foreach($result[result] AS $res) {	    	    
    	    $myarray[$res[1]]   = $res[3];
    	    $myarray21[$res[1]] = $res[4];
    	    
    	}

    	
    	for($i = 0; $i <= 23; $i++) {
    	    if(array_key_exists($i,$myarray)){
    	        if(strlen($i) == 1){
    	           $date[] = '0'.$i.':00';
    	        }else{
    	           $date[] = $i.':00';
    	        }
    	        $percent_answer[] = (float)$myarray[$i];
    	        $count_answer[]   = (float)$myarray21[$i];
    	        $count_all[]      = (float)$myarray21[$i];
    	         
    	    }else{
    	        if(strlen($i) == 1){
    	           $date[] = '0'.$i.':00';
    	        }else{
    	           $date[] = $i.':00';
    	        }
    	        $percent_answer[] = 0;
    	        $count_answer[]   = 0;
    	        $count_all[]      = 0;
    	    }
    	    
    	    $name_answer[]   = 'SL-ფაქტიური';
    	    $limit_number[]  = (float)$number;
    	    $limit_percent[] = (float)$procent;
    	    
    	}
	
    }
    
    if($done == 3){
        $db->setQuery(" SELECT  CONCAT(HOUR(sl_info.date), IF(MINUTE(sl_info.date) >= 30, '.5', '')) *2 AS `hour`,
                                ROUND(SUM(IF(sl_info.avg_duration<$number,1,0))/COUNT(*)*100,2) AS percent,
                                sl_info.time AS `time`,
                        		COUNT(*) AS `total`
                        FROM (SELECT   ROUND(SUM(sl.duration)/COUNT(*)) AS avg_duration,
                                       FLOOR(sl.datetime / (30 * 60)) AS time,
                                       FROM_UNIXTIME(sl.datetime) AS `date`
                			  FROM    `dashboard_chat_durations` AS sl
                			  WHERE    sl.`source_id` = 2 AND sl.`status` = 1 AND DATE(FROM_UNIXTIME(sl.datetime)) = '$day'
                              $check_wwek $users_query
                			  GROUP BY time, sl.chat_id) AS `sl_info`
                        GROUP BY sl_info.time");
        
        $result = $db->getResultArray(MYSQLI_NUM);
        
        
        foreach($result[result] AS $res) {
            $my_array[$res[0]] = $res[1];
            $myarray1[$res[0]] = $res[3];
        }
        
        for($i = 0; $i <= 47; $i++) {
            if(array_key_exists($i,$my_array)){
                if(strlen($i/2) == 1){
                    if($i % 2 == 0) {
                        $date[] = (($i-1)/2+0.5).':00';
                    }else{
                        $date[] = (($i-1)/2).':30';
                    }
                }else{
                    if($i % 2 == 0) {
                        $date[] = (($i)/2).':00';
                    }else{
                        $date[] = (($i-1)/2).':30';
                    }
                }
                $percent_unanswer[] = (float)$my_array[$i];
                $count_answer[]     = (float)$myarray1[$i];
                $count_all[]        = (float)$myarray1[$i];
            }else{
                if(strlen(round(($i/2))) == 1){
                    if($i % 2 == 0) { 
                        $date[] = '0'.(($i-1)/2+0.5).':00';
                    }else{
                        $date[] = '0'.(($i-1)/2).':30';
                    }
                }else{
                    if($i % 2 == 0) {
                        $date[] = (($i)/2).':00';
                    }else{
                        $date[] = (($i-1)/2).':30';
                        
                    }
                }
                $percent_unanswer[] = 0;
                $count_answer[]     = 0;
                $count_all[]        = 0;
            }

            $name_unanswer[]     = 'SL-ფაქტიური';
            $limit_number[] = (float)$number;
            $limit_percent[] = (float)$procent;
             
        }
    }
    if($done == 4){

    }
 	

    $unit     = " %";
    
    $serie1[] = array('count_all' => $count_all, 'count_unanswer' => $count_unanswer, 'count_answer' => $count_answer, 'name_answer' => $name_answer[0], 'name_unanswer' => $name_unanswer[0], 'percent_answer' => $percent_answer, 'percent_unanswer' => $percent_unanswer, 'unit' => $unit, 'date' => $date, 'limit_number' => $limit_number, 'limit_percent' => $limit_percent, 'all_answer_input' => $all[result][0][num], 'all_procent_input' => $all[result][0][percent]);
    
    
    echo json_encode($serie1);

?>