<?php
require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();

//----------------------------- ცვლადი

$agent	= $_REQUEST['agent'];
$queue	= $_REQUEST['queuet'];
$start_time = $_REQUEST['start_time'];
$end_time 	= $_REQUEST['end_time'];
$day = (strtotime($end_time)) -  (strtotime($start_time));
$day_format = round(($day / (60*60*24)) + 1);


// ----------------------------------

if(isset($_REQUEST["request"]) && $_REQUEST["request"] == "call_filter") {

    //------------------------------------------------QUERY FUNCTIONS---------------------------------------

//get summary of all calls
    function get_sum_calls($start_date, $end_date, $queue, $agent) {
        global $db;
        $db->setQuery("SELECT COUNT(id) AS all_calls
                       FROM   asterisk_incomming
                       WHERE  DATE(call_datetime) BETWEEN '$start_date' AND '$end_date' AND
                              asterisk_incomming.dst_queue IN ($queue) AND 
                              asterisk_incomming.dst_extension IN ($agent)
                       AND    asterisk_incomming.disconnect_cause NOT IN(1,0)");

        $result =  $db->getResultArray();
        return     $result[result][0]["all_calls"];

    }

//get summary of all calls in some time
    function get_sum_calls_in_time($start_date, $end_date, $call_ans_in, $queue, $agent) {
        global $db;
        $db->setQuery("SELECT COUNT(wait_time) AS calls_in_time
                       FROM   asterisk_incomming
                       WHERE  DATE(call_datetime) BETWEEN '$start_date' AND '$end_date' AND
                              asterisk_incomming.dst_queue IN ($queue) AND 
                              asterisk_incomming.dst_extension IN ($agent) AND
                              wait_time < '$call_ans_in'
AND    asterisk_incomming.disconnect_cause NOT IN(1,0)");

        $result = $db->getResultArray();
        return $result[result][0]["calls_in_time"];

    }

//get summary of  partly all calls
    function get_sum_partly_calls($start_date, $end_date, $call_unans_after, $queue, $agent) {
        return get_sum_unanswered_calls_after_time($start_date, $end_date, $call_unans_after, $queue, $agent) + get_sum_answered_calls($start_date, $end_date, $queue, $agent);
    }

//get summary of answered calls
    function get_sum_answered_calls($start_date, $end_date, $queue, $agent) {
        global $db;
        $db->setQuery(" SELECT COUNT(id) as answered_calls
                        FROM   asterisk_incomming
                        WHERE  DATE(call_datetime) BETWEEN '$start_date' AND '$end_date' AND
                               asterisk_incomming.dst_queue IN ($queue) AND 
                               asterisk_incomming.dst_extension IN ($agent) AND 
                               disconnect_cause != '2'
AND    asterisk_incomming.disconnect_cause NOT IN(1,0)");

        $result =  $db->getResultArray();
        return  $result[result][0]["answered_calls"];

    }

//get summary of answered calls
    function get_sum_unanswered_calls($start_date, $end_date, $queue, $agent) {
        global $db;
        $db->setQuery("SELECT COUNT(id) as answered_calls
                       FROM   asterisk_incomming
                       WHERE  DATE(call_datetime) BETWEEN '$start_date' AND '$end_date' AND
                              asterisk_incomming.dst_queue IN ($queue) AND 
                              asterisk_incomming.dst_extension IN ($agent) AND  
                              disconnect_cause = '2'");

        $result = $db->getResultArray();
        return  $result[result][0]["answered_calls"];

    }

//get summary of answered calls in specific time
    function get_sum_answered_calls_in_time($start_date, $end_date, $call_ans_in, $queue, $agent) {
        global $db;
        $db->setQuery(" SELECT COUNT(wait_time) AS calls_in_time
                        FROM asterisk_incomming
                        WHERE DATE(call_datetime) BETWEEN '$start_date' AND '$end_date' AND
                              asterisk_incomming.dst_queue IN ($queue) AND 
                              asterisk_incomming.dst_extension IN ($agent) AND 
                              disconnect_cause != '2' AND 
                              wait_time < '$call_ans_in'
AND    asterisk_incomming.disconnect_cause NOT IN(1,0)");

        $result = $db->getResultArray();
        return  $result[result][0]["calls_in_time"];

    }

//get summary of unanswered calls ofter specific time
    function get_sum_unanswered_calls_after_time($start_date, $end_date, $call_unans_after, $queue, $agent) {
        global $db;
        $db->setQuery("SELECT COUNT(wait_time) AS calls_in_time
                       FROM   asterisk_incomming
                       WHERE  DATE(call_datetime) BETWEEN '$start_date' AND '$end_date' AND
                              asterisk_incomming.dst_queue IN ($queue) AND 
                              asterisk_incomming.dst_extension IN ($agent) AND 
                              disconnect_cause = '2' AND 
                              wait_time > '$call_unans_after'");

        $result = $db->getResultArray();
        return  $result[result][0]["calls_in_time"];

    }

    //get requested data
    $queue          = $_REQUEST["queue"];
    $agent          = $_REQUEST["agent"];
    $start_date     = $_REQUEST["startDate"];
    $end_date       = $_REQUEST["endDate"];
    $sl_type        = $_REQUEST["slType"];
    $call_ans_in    = $_REQUEST["callAnsIn"];
    $call_not_after = $_REQUEST["callNotAfter"];

    //SL answered calls / answered calls (1)
    if($sl_type == 1) {

        //define variables
        $calls_in_time = get_sum_answered_calls_in_time($start_date, $end_date, $call_ans_in, $queue, $agent);
        $call_answered = get_sum_answered_calls($start_date, $end_date, $queue, $agent);

        //calculate sl percent
        $sl_result = round(($calls_in_time / $call_answered) * 100, 2);

        //set result array
        $result = array("allCalls" => $call_answered, "percentValues" => $sl_result." / ".$call_ans_in);

        //SL answered calls / all calls (2)
    }else if($sl_type == 2){

        //define variables
        $all_calls = get_sum_calls($start_date, $end_date, $queue, $agent);
        $calls_in_time = get_sum_answered_calls_in_time($start_date, $end_date, $call_ans_in, $queue, $agent);

        //calculate sl percent
        $sl_result = round(($calls_in_time / $all_calls) * 100, 2);

        //set result array
        $result = array("allCalls" => $all_calls, "percentValues" => $sl_result." / ".$call_ans_in);

        //SL all calls / all calls (3)
    } else if($sl_type == 3) {

        //define variables
        $all_calls = get_sum_calls($start_date, $end_date, $queue, $agent);
        $all_calls_in_time = get_sum_calls_in_time($start_date, $end_date, $call_ans_in, $queue, $agent);

        //calculate sl percent
        $sl_result = round(($all_calls_in_time / $all_calls) * 100, 2);

        //set result array
        $result = array("allCalls" => $all_calls, "percentValues" => $sl_result." / ".$call_ans_in);

        //SL answered calls / partly all calls (4)
    } else if($sl_type == 4) {

        //define variables
        $calls_in_time = get_sum_answered_calls_in_time($start_date, $end_date, $call_ans_in, $queue, $agent);
        $calls_partly_all = get_sum_partly_calls($start_date, $end_date, $call_not_after, $queue, $agent);

        //calculate sl percent
        $sl_result = round(($calls_in_time / $calls_partly_all) * 100, 2);

        //set result array
        $result = array("allCalls" => $calls_partly_all, "percentValues" => $sl_result." / ".$call_ans_in);

    }

    //return data array
    echo json_encode($result);

}

if(isset($_REQUEST["request"]) && $_REQUEST["request"] == "diagrams") {

    if ($_REQUEST['act'] == 'all_calls_dialog_table') {
        $data = array('page' => array(
            'answear_dialog' => ''
        ));
        $count = $_REQUEST['count'];
        $hidden = $_REQUEST['hidden'];
        $db->setQuery("SELECT 	call_datetime,
                				call_datetime,
                				source,
                				dst_queue,
                				dst_extension,
                				SEC_TO_TIME(duration),
                				CONCAT('<p onclick=play(', '\'',DATE_FORMAT(DATE(call_datetime),'%Y/%m/%d/'), file_name, '\'',  ')>მოსმენა</p>', '<a download=\"audio.wav\" href=\"http://pbx.my.ge/records/', DATE_FORMAT(DATE(call_datetime),'%Y/%m/%d/'), file_name, '\" target=\"_blank\">ჩამოტვირთვა</a>') AS `file`
                        FROM 	`asterisk_incomming`
                        WHERE   DATE(`call_datetime`) >= '$start_time'
                        AND 	DATE(`call_datetime`) <= '$end_time'
                        AND 	dst_queue IN ($queue)
                        AND    asterisk_incomming.disconnect_cause NOT IN(1,0)");
        
        $data = $db->getList($count, $hidden);


    } else if ($_REQUEST['act'] == 'answear_dialog_table') {
        $data = array('page' => array(
            'answear_dialog' => ''
        ));
        $count = $_REQUEST['count'];
        $hidden = $_REQUEST['hidden'];
        $db->setQuery("SELECT 	call_datetime,
                    				call_datetime,
                    				source,
                    				dst_queue,
                    				dst_extension,
                    				SEC_TO_TIME(duration),
                    				CONCAT('<p onclick=play(', '\'',DATE_FORMAT(DATE(call_datetime),'%Y/%m/%d/'), file_name, '\'',  ')>მოსმენა</p>', '<a download=\"audio.wav\" href=\"http://pbx.my.ge/records/', DATE_FORMAT(DATE(call_datetime),'%Y/%m/%d/'), file_name, '\" target=\"_blank\">ჩამოტვირთვა</a>') AS `file`
	                        FROM 	`asterisk_incomming`
                            WHERE disconnect_cause != '2' 
                            AND 	DATE(`call_datetime`) >= '$start_time'
                            AND 	DATE(`call_datetime`) <= '$end_time'
                            AND 	dst_queue IN ($queue)
                            AND		dst_extension IN ($agent)
                            AND    asterisk_incomming.disconnect_cause NOT IN(1,0)");
        
        $data = $db->getList($count, $hidden);


    } elseif ($_REQUEST['act'] == 'done_dialog_table') {
        $data = array('page' => array(
            'answear_dialog' => ''
        ));
        $count = $_REQUEST['count'];
        $hidden = $_REQUEST['hidden'];


        $db->setQuery("SELECT  call_datetime,
                				call_datetime,
                				source,
                				dst_queue,
                				dst_extension,
                				SEC_TO_TIME(duration),
                				CONCAT('<p onclick=play(', '\'',DATE_FORMAT(DATE(call_datetime),'%Y/%m/%d/'), file_name, '\'',  ')>მოსმენა</p>', '<a download=\"audio.wav\" href=\"http://pbx.my.ge/records/', DATE_FORMAT(DATE(call_datetime),'%Y/%m/%d/'), file_name, '\" target=\"_blank\">ჩამოტვირთვა</a>') AS `file`
                        FROM `asterisk_incomming`
                        JOIN incomming_call ON asterisk_incomming.id = incomming_call.asterisk_incomming_id
                        WHERE NOT ISNULL(incomming_call.user_id)
                        AND dst_queue IN($queue)
                        AND dst_extension IN($agent)
                        AND disconnect_cause != '2'
                        AND DATE(`call_datetime`) >= '$start_time'
                        AND DATE(`call_datetime`) <= '$end_time'
                        AND    asterisk_incomming.disconnect_cause NOT IN(1,0)
						");
        $data = $db->getList($count, $hidden);

    } elseif ($_REQUEST['act'] == 'undone_dialog_table') {
        $data = array('page' => array(
            'answear_dialog' => ''
        ));
        $count = $_REQUEST['count'];
        $hidden = $_REQUEST['hidden'];


        $db->setQuery("SELECT  call_datetime,
                				call_datetime,
                				source,
                				dst_queue,
                				dst_extension,
                				SEC_TO_TIME(duration),
                				CONCAT('<p onclick=play(', '\'',DATE_FORMAT(DATE(call_datetime),'%Y/%m/%d/'), file_name, '\'',  ')>მოსმენა</p>', '<a download=\"audio.wav\" href=\"http://pbx.my.ge/records/', DATE_FORMAT(DATE(call_datetime),'%Y/%m/%d/'), file_name, '\" target=\"_blank\">ჩამოტვირთვა</a>') AS `file`
                        FROM `asterisk_incomming`
                        JOIN incomming_call ON asterisk_incomming.id = incomming_call.asterisk_incomming_id
                        WHERE (ISNULL(incomming_call.user_id) OR incomming_call.user_id = '' OR incomming_call.user_id = 0)
                        AND dst_queue IN($queue)
                        AND dst_extension IN($agent)
                        AND disconnect_cause != '2'
                        AND DATE(`call_datetime`) >= '$start_time'
                        AND DATE(`call_datetime`) <= '$end_time'
                        AND    asterisk_incomming.disconnect_cause NOT IN(1,0)");
        $data = $db->getList($count, $hidden);

    } elseif ($_REQUEST['act'] == 'incoming_all_dialog') {
        $data['page']['answear_dialog'] = '
															
            	
                <table id="table_right_menu">
                <tr>
                <td><img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
                </td>
                <td><img alt="log" src="media/images/icons/log.png" height="14" width="14">
                </td>
                <td id="show_copy_prit_exel" myvar="0"><img alt="link" src="media/images/icons/select.png" height="14" width="14">
                </td>
                </tr>
                </table>
                <table class="display" id="example1" >
                    <thead>
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width: 180px;">თარიღი</th>
                            <th style="width: 110px;">წყარო</th>
                            <th style="width: 100px;">ადრესატი</th>
							<th style="width: 100px;">ექსთენშენი</th>
                            <th style="width: 80px;">დრო</th>
                            <th style="width: 100px;">ქმედება</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                            	<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
                            </th>
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init" style="">
							</th>
                                                        
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80px;" />
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 70px;"/>
                            </th>
							<th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 70px;"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 70px;" />
                            </th>
							<th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80px;" />
                            </th>
                            
                        </tr>
                    </thead>
                </table>';

    } elseif ($_REQUEST['act'] == 'undone_dialog') {
        $data['page']['answear_dialog'] = '
															
            	
                <table id="table_right_menu">
                <tr>
                <td><img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
                </td>
                <td><img alt="log" src="media/images/icons/log.png" height="14" width="14">
                </td>
                <td id="show_copy_prit_exel" myvar="0"><img alt="link" src="media/images/icons/select.png" height="14" width="14">
                </td>
                </tr>
                </table>
                <table class="display" id="example5" >
                    <thead>
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width: 180px;">თარიღი</th>
                            <th style="width: 110px;">წყარო</th>
                            <th style="width: 100px;">ადრესატი</th>
							<th style="width: 100px;">ექსთენშენი</th>
                            <th style="width: 80px;">დრო</th>
                            <th style="width: 100px;">ქმედება</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                            	<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
                            </th>
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init" style="">
							</th>
                                                        
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80px;" />
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 70px;"/>
                            </th>
							<th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 70px;"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 70px;" />
                            </th>
							<th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80px;" />
                            </th>
                            
                        </tr>
                    </thead>
                </table>';

    } elseif ($_REQUEST['act'] == 'done_dialog') {
        $data['page']['answear_dialog'] = '
															
            	
                <table id="table_right_menu">
                <tr>
                <td><img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
                </td>
                <td><img alt="log" src="media/images/icons/log.png" height="14" width="14">
                </td>
                <td id="show_copy_prit_exel" myvar="0"><img alt="link" src="media/images/icons/select.png" height="14" width="14">
                </td>
                </tr>
                </table>
                <table class="display" id="example4" >
                    <thead>
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width: 180px;">თარიღი</th>
                            <th style="width: 110px;">წყარო</th>
                            <th style="width: 100px;">ადრესატი</th>
							<th style="width: 100px;">ექსთენშენი</th>
                            <th style="width: 80px;">დრო</th>
                            <th style="width: 100px;">ქმედება</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                            	<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
                            </th>
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init" style="">
							</th>
                                                        
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80px;" />
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 70px;"/>
                            </th>
							<th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 70px;"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 70px;" />
                            </th>
							<th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80px;" />
                            </th>
                            
                        </tr>
                    </thead>
                </table>';
    } else
        if ($_REQUEST['act'] == 'unanswear_dialog_table') {


            $data = array('page' => array(
                'answear_dialog' => ''
            ));
            $count = $_REQUEST['count'];
            $hidden = $_REQUEST['hidden'];
            $db->setQuery("SELECT 	call_datetime,
                            	    call_datetime,
                            	    CONCAT('<span class=\"show_this_phone\">',source,'</span>') AS `source`,
                            	    dst_queue,
	                                SEC_TO_TIME(wait_time)
                            FROM 	`asterisk_incomming`
                            WHERE	disconnect_cause = '2'  
                            AND DATE(call_datetime) >= '$start_time'
                            AND DATE(call_datetime) <= '$end_time'
							AND dst_queue IN($queue)");
            $data = $db->getList($count, $hidden);

        } else
            if ($_REQUEST['act'] == 'answear_dialog') {

                $data['page']['answear_dialog'] = '
															
				
				<table id="table_right_menu" >
                <tr>
                <td><img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
                </td>
                <td><img alt="log" src="media/images/icons/log.png" height="14" width="14">
                </td>
                <td id="show_copy_prit_exel" myvar="0"><img alt="link" src="media/images/icons/select.png" height="14" width="14">
                </td>
                </tr>
                </table>									
                <table class="display" id="example2">
                    <thead>
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width: 180px;">თარიღი</th>
                            <th style="width: 110px;">წყარო</th>
                            <th style="width: 110px;">ადრესატი</th>
							<th style="width: 100px;">ექსთენშენი</th>
                            <th style="width: 80px;">დრო</th>
                            <th style="width: 90px;">ქმედება</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                            	<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
                            </th>
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init" style="">
							</th>
                                                      
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80px;" />
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 70px;"/>
                            </th>
							<th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 70px;"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 70px;" />
                            </th>
							<th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80px;" />
                            </th>
                            
                        </tr>
                    </thead>
                </table>
        

	';


            } else
                if ($_REQUEST['act'] == 'unanswear_dialog') {

                    $data['page']['answear_dialog'] = '
				
				<table id="table_right_menu">
                <tr>
                <td><img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
                </td>
                <td><img alt="log" src="media/images/icons/log.png" height="14" width="14">
                </td>
                <td id="show_copy_prit_exel" myvar="0"><img alt="link" src="media/images/icons/select.png" height="14" width="14">
                </td>
                </tr>
                </table>			
                <table class="display" id="example3">
                    <thead>
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width: 200px;">თარიღი</th>
                            <th style="width: 120px;">წყარო</th>
                            <th style="width: 100px;">ადრესატი</th>
                            <th style="width: 120px;">ლოდინის დრო</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                            	<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
                            </th>
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init" style="">
							</th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 100px;"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80px;" />
                            </th>
							<th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 70px;" />
                            </th>
                        </tr>
                    </thead>
                </table>';


                } else {
                    $data = array('page' => array(
                        'answer_call' => '',
                        'technik_info' => '',
                        'report_info' => '',
                        'answer_call_info' => '',
                        'answer_call_by_queue' => '',
                        'disconnection_cause' => '',
                        'unanswer_call' => '',
                        'disconnection_cause_unanswer' => '',
                        'unanswered_calls_by_queue' => '',
                        'totals' => '',
                        'call_distribution_per_day' => '',
                        'call_distribution_per_hour' => '',
                        'call_distribution_per_day_of_week' => '',
                        'service_level' => ''
                    ));

                    $data['error'] = $error;

                    $db->setQuery("	SELECT  COUNT(*) AS `count`
                                    FROM 	`asterisk_incomming`
                                    WHERE   DATE(call_datetime) >= '$start_time'
                                            AND DATE(call_datetime) <= '$end_time'
                                            AND dst_queue IN ($queue)
                                            AND    asterisk_incomming.disconnect_cause NOT IN(1,0)");
                    
                    $row_all_calls = $db->getResultArray();
                    
                    $db->setQuery("	SELECT  COUNT(*) AS `count`,
                                            dst_queue
                                    FROM 	`asterisk_incomming`
                                    WHERE	disconnect_cause != '2' AND NOT ISNULL(disconnect_cause)
                                    AND DATE(call_datetime) >= '$start_time'
                                    AND DATE(call_datetime) <= '$end_time'
                                    AND dst_queue IN ($queue)
                                    AND dst_extension IN ($agent)
AND    asterisk_incomming.disconnect_cause NOT IN(1,0)");
                    
                    $row_answer = $db->getResultArray();
                    
                    $db->setQuery(" SELECT 	COUNT(*) AS `count`,
                                            ROUND((SUM(wait_time) / COUNT(*))) AS `sec`
                                    FROM 	`asterisk_incomming`
                                    WHERE	disconnect_cause = '2'
                                    AND DATE(call_datetime) >= '$start_time'
                                    AND DATE(call_datetime) <= '$end_time'
                                    AND dst_queue IN ($queue)");
                    
                    $row_abadon = $db->getResultArray();
                    
                    $db->setQuery(" SELECT SEC_TO_TIME(ROUND(sum(duration) / count(id))) AS `avarage`
                                    FROM asterisk_incomming
                                    where date(call_datetime) BETWEEN date('$start_time') and date('$end_time')
AND    asterisk_incomming.disconnect_cause NOT IN(1,0)");
                    
                    $row_avarage = $db->getResultArray();
                    //---------------------------------------- რეპორტ ინფო

                    $data['page']['report_info'] = '
    
                <tr>
                    <td>რიგი:</td>
                    <td>' . $queue . '</td>
                </tr>
                <tr>
                    <td>საწყისი თარიღი:</td>
                    <td>' . $start_time . '</td>
                </tr>
                <tr>
                    <td>დასრულების თარიღი:</td>
                    <td>' . $end_time . '</td>
                </tr>
                <tr>
                    <td>პერიოდი:</td>
                    <td>' . $day_format . ' დღე</td>
                </tr>
    
							';

                    //----------------------------------------------

                    if ($_REQUEST['act'] == 'tab_0') {

                        $db->setQuery("	SELECT COUNT(incomming_call.id) as `count`
                                        FROM 	incomming_call
                                        JOIN asterisk_incomming ON incomming_call.asterisk_incomming_id = asterisk_incomming.id
                                        WHERE DATE(incomming_call.date) >= '$start_time'
                                        AND DATE(incomming_call.date) <= '$end_time'
                                        AND asterisk_incomming.dst_queue IN ($queue) 
                                        AND asterisk_incomming.dst_extension IN ($agent)
                                        AND incomming_call.phone != '' AND NOT ISNULL(incomming_call.user_id)
AND    asterisk_incomming.disconnect_cause NOT IN(1,0)");

                        $row_done_blank = $db->getResultArray();
//------------------------------- ტექნიკური ინფორმაცია


                        $data['page']['technik_info'] = '
							
                    <td>ზარი</td>
                    <td>' . $row_all_calls[result][0][count] . '</td>
                    <td>' . $row_answer[result][0][count] . '</td>
                    <td>' . $row_abadon[result][0][count] . '</td>
                    <td>' . $row_done_blank[result][0][count] . '</td>
                    <td>' . ($row_answer[result][0][count] - $row_done_blank[result][0][count]) . '</td>
                    <td>' . $row_avarage[result][0]['avarage'] . '</td>
                    <td>' . round(((($row_answer[result][0][count]) / ($row_answer[result][0][count] + $row_abadon[result][0][count])) * 100), 2) . ' %</td>
                    <td>' . round(((($row_abadon[result][0][count]) / ($row_answer[result][0][count] + $row_abadon[result][0][count])) * 100), 2) . ' %</td>
                    <td>' . round(((($row_done_blank[result][0][count]) / ($row_answer[result][0][count])) * 100), 2) . ' %</td>
                    <td>' . round(((($row_answer[result][0][count] - $row_done_blank[result][0][count]) / ($row_answer[result][0][count])) * 100), 2) . ' %</td>
                
							';
// -----------------------------------------------------

                    }

                    if ($_REQUEST['act'] == 'tab_1') {
//------------------------------- ნაპასუხები ზარები რიგის მიხედვით

                        $db->setQuery("	SELECT   COUNT(*) AS `count`,
                                	             dst_queue
                                	    FROM    `asterisk_incomming`
                                	    WHERE	 disconnect_cause != '2'
                                	    AND      DATE(call_datetime) >= '$start_time'
                                	    AND      DATE(call_datetime) <= '$end_time'
                                	    AND      dst_queue IN ($queue)
                                	    AND      dst_extension IN ($agent)
AND    asterisk_incomming.disconnect_cause NOT IN(1,0)
        	                            GROUP BY dst_queue");
                        
                        $answer_que = $db->getResultArray();
                        
                        foreach($answer_que[result] AS $row_answer_que){
                            $data['page']['answer_call'] .= '
							<tr><td>' . $row_answer_que[dst_queue] . '</td><td>' . $row_answer_que[count] . ' ზარი</td><td>' . round(((($row_answer_que[count]) / ($row_answer[count])) * 100)) . ' %</td></tr>
							';
                        }

//-------------------------------------------------------

//------------------------------- მომსახურების დონე(Service Level)


                        $db->setQuery("	SELECT `wait_time`                                				
                                        FROM   `asterisk_incomming`
                                        WHERE	disconnect_cause != '2'  
                                        AND     DATE(call_datetime) >= '$start_time'
                                        AND     DATE(call_datetime) <= '$end_time' 
                                        AND     dst_queue IN ($queue) 
                                        AND     dst_extension in ($agent)
AND    asterisk_incomming.disconnect_cause NOT IN(1,0)");
                        $w15 = 0;
                        $w30 = 0;
                        $w45 = 0;
                        $w60 = 0;
                        $w75 = 0;
                        $w90 = 0;
                        $w91 = 0;

                        $res_service_level = $db->getResultArray();
                        foreach($res_service_level[result] AS $res_service_level_r) {

                            if ($res_service_level_r['wait_time'] < 15) {
                                $w15++;
                            }

                            if ($res_service_level_r['wait_time'] < 30) {
                                $w30++;
                            }

                            if ($res_service_level_r['wait_time'] < 45) {
                                $w45++;
                            }

                            if ($res_service_level_r['wait_time'] < 60) {
                                $w60++;
                            }

                            if ($res_service_level_r['wait_time'] < 75) {
                                $w75++;
                            }

                            if ($res_service_level_r['wait_time'] < 90) {
                                $w90++;
                            }

                            $w91++;

                        }

                        $d30 = $w30 - $w15;
                        $d45 = $w45 - $w30;
                        $d60 = $w60 - $w45;
                        $d75 = $w75 - $w60;
                        $d90 = $w90 - $w75;
                        $d91 = $w91 - $w90;


                        $p15 = round($w15 * 100 / $w91);
                        $p30 = round($w30 * 100 / $w91);
                        $p45 = round($w45 * 100 / $w91);
                        $p60 = round($w60 * 100 / $w91);
                        $p75 = round($w75 * 100 / $w91);
                        $p90 = round($w90 * 100 / $w91);


                        $data['page']['service_level'] = '
							
							<tr class="odd">
						 		<td>15 წამში</td>
					 			<td>' . $w15 . '</td>
					 			<td></td>
					 			<td>' . $p15 . '%</td>
					 		</tr>
				 			<tr>
						 		<td>30 წამში</td>
					 			<td>' . $w30 . '</td>
					 			<td>' . $d30 . '</td>
					 			<td>' . $p30 . '%</td>
					 		</tr>
				 			<tr class="odd">
						 		<td>45 წამში</td>
					 			<td>' . $w45 . '</td>
					 			<td>' . $d45 . '</td>
					 			<td>' . $p45 . '%</td>
					 		</tr>
				 			<tr>
						 		<td>60 წამში</td>
					 			<td>' . $w60 . '</td>
					 			<td>' . $d60 . '</td>
					 			<td>' . $p60 . '%</td>
					 		</tr>
				 			<tr class="odd">
						 		<td>75 წამში</td>
					 			<td>' . $w75 . '</td>
					 			<td>' . $d75 . '</td>
					 			<td>' . $p75 . '%</td>
					 		</tr>
					 		<tr>
						 		<td>90 წამში</td>
					 			<td>' . $w90 . '</td>
					 			<td>' . $d90 . '</td>
					 			<td>' . $p90 . '%</td>
					 		</tr>
					 		<tr class="odd">
						 		<td>90+ წამში</td>
					 			<td>' . $w91 . '</td>
					 			<td>' . $d91 . '</td>
					 			<td>100%</td>
					 		</tr>
							';

//-------------------------------------------------------


//------------------------------------ ნაპასუხები ზარები


                        $db->setQuery("	SELECT	ROUND((SUM(wait_time) / COUNT(*)),2) AS `hold`,
                                				ROUND((SUM(duration) / COUNT(*)),2) AS `sec`,
                                				ROUND((SUM(duration) / 60 ),2) AS `min`
										FROM   `asterisk_incomming`
                                        WHERE	disconnect_cause != '2'  
                                        AND     DATE(call_datetime) >= '$start_time'
                                        AND     DATE(call_datetime) <= '$end_time' 
                                        AND     dst_queue IN ($queue) 
                                        AND     dst_extension in ($agent)
AND    asterisk_incomming.disconnect_cause NOT IN(1,0)");
                        
                        $row_clock = $db->getResultArray();

                        $data['page']['answer_call_info'] = '

                   	<tr>
					<td>ნაპასუხები ზარები</td>
					<td>' . $row_answer[result][0][count] . ' ზარი</td>
					</tr>
					
					<tr>
					<td>საშ. ხანგძლივობა:</td>
					<td>' . $row_clock[result][0][sec] . ' წამი</td>
					</tr>
					<tr>
					<td>სულ საუბრის ხანგძლივობა:</td>
					<td>' . $row_clock[result][0][min] . ' წუთი</td>
					</tr>
					<tr>
					<td>ლოდინის საშ. ხანგძლივობა:</td>
					<td>' . $row_clock[result][0][hold] . ' წამი</td>
					</tr>

							';

//---------------------------------------------


//--------------------------- ნაპასუხები ზარები ოპერატორების მიხედვით

              $db->setQuery("SELECT 	dst_extension AS `agent`,
                        				COUNT(*) AS `num`,
                        				ROUND(((COUNT(*) / (
                        				SELECT COUNT(*)
                        				FROM 		`asterisk_incomming`
                        				WHERE		disconnect_cause != '2'  
                        				AND DATE(call_datetime) >= '$start_time'
                                        AND DATE(call_datetime) <= '$end_time' 
                                        AND dst_queue IN ($queue) 
                                        AND dst_extension in ($agent)
AND    asterisk_incomming.disconnect_cause NOT IN(1,0)
                        				)) * 100),2) AS `call_pr`,
                        				ROUND((sum(duration) / 60),2) AS `call_time`,
                        				ROUND(((SUM(duration) / (
                        				SELECT SUM(duration)
                        				FROM 		`asterisk_incomming`
                        				WHERE		disconnect_cause != '2' 
AND    asterisk_incomming.disconnect_cause NOT IN(1,0)
                        				AND DATE(call_datetime) >= '$start_time'
                                        AND DATE(call_datetime) <= '$end_time' 
                                        AND dst_queue IN ($queue) 
                                        AND dst_extension in ($agent)
                        				)) * 100),2) as `call_time_pr`,
                        				TIME_FORMAT(SEC_TO_TIME(sum(duration) / count(*)), '%i:%s') AS `avg_call_time`,
                        				SEC_TO_TIME(sum(wait_time)) AS `hold_time`,
                        				SEC_TO_TIME(SUM(wait_time) / COUNT(*)) AS `avg_hold_time`
                            FROM 		`asterisk_incomming`
                            WHERE		disconnect_cause != '2' 
                            AND DATE(call_datetime) >= '$start_time'
                            AND DATE(call_datetime) <= '$end_time' 
                            AND dst_queue IN ($queue) 
                            AND dst_extension in ($agent)
AND    asterisk_incomming.disconnect_cause NOT IN(1,0)
                            GROUP BY dst_extension");
              
              $ress = $db->getResultArray();
              foreach ($ress[result] AS $row) {
                    $data['page']['answer_call_by_queue'] .= '

                   	<tr>
					<td>' . $row[agent] . '</td>
					<td>' . $row[num] . '</td>
					<td>' . $row[call_pr] . ' %</td>
					<td>' . $row[call_time] . ' </td>
					<td>' . $row[call_time_pr] . ' %</td>
					<td>' . $row[avg_call_time] . ' წუთი</td>
					<td>' . $row[hold_time] . ' წამი</td>
					<td>' . $row[avg_hold_time] . ' წამი</td>
					</tr>';

               }

//----------------------------------------------------


//--------------------------- კავშირის გაწყვეტის მიზეზეი


                        $db->setQuery(" SELECT COUNT(*) AS `count`
                                        FROM 	`asterisk_incomming`
                                        WHERE	disconnect_cause != '2'  
                                        AND DATE(call_datetime) >= '$start_time'
                                        AND DATE(call_datetime) <= '$end_time' 
                                        AND dst_queue IN ($queue) 
                                        AND dst_extension in ($agent)
                                        AND disconnect_cause = '3'");
                        
                        $row_COMPLETECALLER = $db->getResultArray();
                        
                        $db->setQuery(" SELECT COUNT(*) AS `count`
                                        FROM 	`asterisk_incomming`
                                        WHERE	disconnect_cause != '2' 
                                        AND DATE(call_datetime) >= '$start_time'
                                        AND DATE(call_datetime) <= '$end_time'
                                        AND dst_queue IN ($queue)
                                        AND dst_extension in ($agent)
                                        AND disconnect_cause = '4'");
                        
                        $row_AGENT = $db->getResultArray();

                        $data['page']['disconnection_cause'] = '

                           <tr>
            					<td>ოპერატორმა გათიშა:</td>
            					<td>' . $row_AGENT[result][0][count] . ' ზარი</td>
            					<td>' . ROUND((($row_AGENT[result][0][count] / ($row_COMPLETECALLER[result][0][count] + $row_AGENT[result][0][count])) * 100), 2) . ' %</td>
            					</tr>
            					<tr>
            					<td>აბონენტმა გათიშა:</td>
            					<td>' . $row_COMPLETECALLER[result][0][count] . ' ზარი</td>
            					<td>' . ROUND((($row_COMPLETECALLER[result][0][count] / ($row_COMPLETECALLER[result][0][count] + $row_AGENT[result][0][count])) * 100), 2) . ' %</td>
        					</tr>
                            ';

//-----------------------------------------------
                    }

                    if ($_REQUEST['act'] == 'tab_2') {
//----------------------------------- უპასუხო ზარები


                        $data['page']['unanswer_call'] = '  <tr>
                                            					<td>უპასუხო ზარების რაოდენობა:</td>
                                            					<td>' . $row_abadon[result][0][count] . ' ზარი</td>
                                            				</tr>
                                            				<tr>
                                            					<td>ლოდინის საშ. დრო კავშირის გაწყვეტამდე:</td>
                                            					<td>' . $row_abadon[result][0][sec] . ' წამი</td>
                                            				</tr>
                                            				<tr>
                                            					<td>საშ. რიგში პოზიცია კავშირის გაწყვეტამდე:</td>
                                            					<td>1</td>
                                            				</tr>
                                            				<tr>
                                            					<td>საშ. საწყისი პოზიცია რიგში:</td>
                                            					<td>1</td>
                                        					</tr>';


//--------------------------------------------


//----------------------------------- კავშირის გაწყვეტის მიზეზი


                        $data['page']['disconnection_cause_unanswer'] = '

                  <tr> 
                  <td>აბონენტმა გათიშა</td>
			      <td>' . $row_abadon[result][0][count] . ' ზარი</td>
			      <td>' . round((($row_abadon[result][0][count] / $row_abadon[result][0][count]) * 100), 2) . ' %</td>
		        </tr>
			    <tr> 
                  <td>დრო ამოიწურა</td>
			      <td>0 ზარი</td>
			      <td>0 %</td>
		        </tr>

							';

//--------------------------------------------

//------------------------------ უპასუხო ზარები რიგის მიხედვით

                  $db->setQuery("	SELECT   COUNT(*) AS `count`,
                                             dst_queue AS `queue`
                            	    FROM 	`asterisk_incomming`
                            	    WHERE	 disconnect_cause = '2' 
                            	    AND      DATE(call_datetime) >= '$start_time'
                            	    AND      DATE(call_datetime) <= '$end_time'
                            	    AND      dst_queue IN ($queue)
                                    GROUP BY dst_queue");
                  
                 $Unanswered_Calls_by_Queue_r = $db->getResultArray();
                 foreach($Unanswered_Calls_by_Queue_r[result] AS $Unanswered_Calls_by_Queue) {
                    $data['page']['unanswered_calls_by_queue'] .= '
                            <tr><td>' . $Unanswered_Calls_by_Queue[queue] . '</td><td>' . $Unanswered_Calls_by_Queue[count] . ' ზარი</td><td>' . round((($Unanswered_Calls_by_Queue[count] / $Unanswered_Calls_by_Queue[count]) * 100), 2) . ' %</td></tr>

							';
                 }

//---------------------------------------------------
                    }

                    if ($_REQUEST['act'] == 'tab_3') {
//------------------------------------------- სულ

                        $data['page']['totals'] = '

                   	<tr> 
                  <td>ნაპასუხები ზარების რაოდენობა:</td>
		          <td>' . $row_answer[result][0][count] . ' ზარი</td>
	            </tr>
                <tr>
                  <td>უპასუხო ზარების რაოდენობა:</td>
                  <td>' . $row_abadon[result][0][count] . ' ზარი</td>
                </tr>
		        

							';

//------------------------------------------------


//-------------------------------- ზარის განაწილება დღეების მიხედვით

                  $db->setQuery("SELECT 	DATE(gg.call_datetime) AS `datetime`,
                            				COUNT(*) AS `answer_count`,
                            				ROUND((( COUNT(*) / (
                            									SELECT COUNT(*)
                            									FROM 		`asterisk_incomming`
                            									WHERE		disconnect_cause != '2' 
                            									AND DATE(call_datetime) >= '$start_time'
                                                        	    AND DATE(call_datetime) <= '$end_time'
                                                    	        AND dst_queue IN ($queue)
                                                    	        AND dst_extension in ($agent)
AND    asterisk_incomming.disconnect_cause NOT IN(1,0)
                            										)) *100),2) AS `call_answer_pr`,
                            				(
                            					SELECT COUNT(*)
                            					FROM 		`asterisk_incomming`
                            					WHERE		asterisk_incomming.disconnect_cause = '2' 
                            					AND DATE(gg.call_datetime) = DATE(asterisk_incomming.call_datetime)
            	                                AND dst_queue IN ($queue)
            	                                GROUP BY DATE(asterisk_incomming.call_datetime)
                            					
                            				) AS `unanswer_call`,
                            				(
                            					SELECT ROUND(((COUNT(*) / $row_abadon[count] ) * 100),2)
                            									
                            					FROM 		`asterisk_incomming`
                            					WHERE		asterisk_incomming.disconnect_cause = '2' 
                            					AND DATE(gg.call_datetime) = DATE(asterisk_incomming.call_datetime)
            	                                AND dst_queue IN ($queue)
            	                                GROUP BY DATE(asterisk_incomming.call_datetime)
                            					
                            				) AS `call_unanswer_pr`,
                            				TIME_FORMAT(SEC_TO_TIME((SUM(gg.duration) / COUNT(*))), '%i:%s') AS `avg_durat`,
                            				ROUND((SUM(gg.wait_time) / COUNT(*))) AS `avg_hold`
                                FROM 		`asterisk_incomming` as gg
                                WHERE		disconnect_cause != '2' 
                                AND DATE(gg.call_datetime) >= '$start_time'
                                AND DATE(gg.call_datetime) <= '$end_time' 
                                AND gg.dst_queue IN ($queue)
                                AND gg.dst_extension in ($agent)
AND    gg.disconnect_cause NOT IN(1,0)
                                GROUP BY DATE(gg.call_datetime)");

                  $res = $db->getResultArray();
                  foreach($res[result] AS $row) {
                            $data['page']['call_distribution_per_day'] .= '

                   	<tr class="odd">
					<td>' . $row[datetime] . '</td>
					<td>' . ($row[answer_count] + $row[unanswer_call]) . '</td>
					<td>' . $row[answer_count] . '</td>
					<td>' . $row[call_answer_pr] . ' %</td>
					<td>' . $row[unanswer_call] . '</td>
					<td>' . ($row[call_unanswer_pr] == '' ? 0 : $row[call_unanswer_pr]) . ' %</td>
					<td>' . $row[avg_durat] . ' წუთი</td>
					<td>' . $row[avg_hold] . ' წამი</td>
					</tr>

							';
                        }

//----------------------------------------------------


//-------------------------------- ზარის განაწილება საათების მიხედვით


                    $db->setQuery("
					SELECT 	HOUR(gg.call_datetime) AS `datetime`,
                				COUNT(*) AS `answer_count`,
                				ROUND((( COUNT(*) / (
                									SELECT COUNT(*)
                									FROM 		`asterisk_incomming`
                									WHERE		disconnect_cause != '2'  
                									AND DATE(call_datetime) >= '$start_time'
                                            	    AND DATE(call_datetime) <= '$end_time'
                                        	        AND dst_queue IN ($queue)
                                        	        AND dst_extension in ($agent)
AND    asterisk_incomming.disconnect_cause NOT IN(1,0)
                										)) *100),2) AS `call_answer_pr`,
                				(
                					SELECT COUNT(*)
                					FROM 		`asterisk_incomming`
                					WHERE		asterisk_incomming.disconnect_cause = '2' 
			                        AND DATE(asterisk_incomming.call_datetime) >= '$start_time'
                                    AND DATE(asterisk_incomming.call_datetime) <= '$end_time'
                					AND HOUR(gg.call_datetime) = HOUR(asterisk_incomming.call_datetime)
	                                AND dst_queue IN ($queue)
                					
                				) AS `unanswer_call`,
                				(
                					SELECT ROUND(((COUNT(*) / $row_abadon[count]) * 100),2)
                									
                					FROM 		`asterisk_incomming`
                					WHERE		asterisk_incomming.disconnect_cause = '2'  
			                        AND DATE(asterisk_incomming.call_datetime) >= '$start_time'
                                    AND DATE(asterisk_incomming.call_datetime) <= '$end_time'
                					AND HOUR(gg.call_datetime) = HOUR(asterisk_incomming.call_datetime)
			                        AND dst_queue IN ($queue)
                		
                				) AS `call_unanswer_pr`,
                				TIME_FORMAT(SEC_TO_TIME((SUM(gg.duration) / COUNT(*))), '%i:%s') AS `avg_durat`,
                				ROUND((SUM(gg.wait_time) / COUNT(*))) AS `avg_hold`
                    FROM 		`asterisk_incomming` as gg
                    WHERE		disconnect_cause != '2' 
                    AND DATE(gg.call_datetime) >= '$start_time'
                    AND DATE(gg.call_datetime) <= '$end_time' 
                    AND gg.dst_queue IN ($queue)
                    AND gg.dst_extension in ($agent)
AND    gg.disconnect_cause NOT IN(1,0)
                    GROUP BY HOUR(gg.call_datetime)
					");

                    $res124 = $db->getResultArray();
                    foreach($res124[result] AS $row) {
                            $data['page']['call_distribution_per_hour'] .= '
				<tr class="odd">
						<td>' . $row[datetime] . ':00</td>
						<td>' . ((($row[answer_count] != '') ? $row[answer_count] : "0") + (($row[unanswer_call] != '') ? $row[unanswer_call] : "0")) . '</td>
						<td>' . (($row[answer_count] != '') ? $row[answer_count] : "0") . '</td>
						<td>' . (($row[call_answer_pr] != '') ? $row[call_answer_pr] : "0") . ' %</td>
						<td>' . (($row[unanswer_call] != '') ? $row[unanswer_call] : "0") . '</td>
						<td>' . (($row[call_unanswer_pr] != '') ? $row[call_unanswer_pr] : "0") . '%</td>
						<td>' . (($row[avg_durat] != '') ? $row[avg_durat] : "0") . ' წამი</td>
						<td>' . (($row[avg_hold] != '') ? $row[avg_hold] : "0") . ' წამი</td>

						</tr>
				';
                        }

//-------------------------------------------------


//------------------------------ ზარის განაწილება კვირების მიხედვით


                        $db->setQuery("
    					SELECT 	CASE
    									WHEN DAYOFWEEK(gg.call_datetime) = 1 THEN 'კვირა'
    									WHEN DAYOFWEEK(gg.call_datetime) = 2 THEN 'ორშაბათი'
    									WHEN DAYOFWEEK(gg.call_datetime) = 3 THEN 'სამშაბათი'
    									WHEN DAYOFWEEK(gg.call_datetime) = 4 THEN 'ოთხშაბათი'
    									WHEN DAYOFWEEK(gg.call_datetime) = 5 THEN 'ხუთშაბათი'
    									WHEN DAYOFWEEK(gg.call_datetime) = 6 THEN 'პარასკევი'
    									WHEN DAYOFWEEK(gg.call_datetime) = 7 THEN 'შაბათი'
    							END AS `datetime`,
                				COUNT(*) AS `answer_count`,
                				ROUND((( COUNT(*) / (
                									SELECT COUNT(*)
                									FROM 		`asterisk_incomming`
                									WHERE		disconnect_cause != '2' 
                									AND DATE(call_datetime) >= '$start_time'
                                            	    AND DATE(call_datetime) <= '$end_time'
                                        	        AND dst_queue IN ($queue)
                                        	        AND dst_extension in ($agent)
AND    asterisk_incomming.disconnect_cause NOT IN(1,0)
                										)) *100),2) AS `call_answer_pr`,
                				(
                					SELECT COUNT(*)
                					FROM 		`asterisk_incomming`
                					WHERE		asterisk_incomming.disconnect_cause = '2' 
                                    AND DATE(asterisk_incomming.call_datetime) >= '$start_time'
                                    AND DATE(asterisk_incomming.call_datetime) <= '$end_time'
                					AND DAYOFWEEK(gg.call_datetime) = DAYOFWEEK(asterisk_incomming.call_datetime)
                                    AND dst_queue IN ($queue)
                				) AS `unanswer_call`,
                				(
                					SELECT ROUND(((COUNT(*) / $row_abadon[count]) * 100),2)
                									
                					FROM 		`asterisk_incomming`
                					WHERE		asterisk_incomming.disconnect_cause = '2' 
                                    AND DATE(asterisk_incomming.call_datetime) >= '$start_time'
                                    AND DATE(asterisk_incomming.call_datetime) <= '$end_time'
                					AND DAYOFWEEK(gg.call_datetime) = DAYOFWEEK(asterisk_incomming.call_datetime)
                                    AND dst_queue IN ($queue)
                				) AS `call_unanswer_pr`,
                				TIME_FORMAT(SEC_TO_TIME((SUM(gg.duration) / COUNT(*))), '%i:%s') AS `avg_durat`,
                				ROUND((SUM(gg.wait_time) / COUNT(*))) AS `avg_hold`
                    FROM 		`asterisk_incomming` as gg
                    WHERE		disconnect_cause != '2' 
                    AND DATE(gg.call_datetime) >= '$start_time'
                    AND DATE(gg.call_datetime) <= '$end_time' 
                    AND gg.dst_queue IN ($queue)
AND    gg.disconnect_cause NOT IN(1,0)
                    AND gg.dst_extension in ($agent)
                    GROUP BY DAYOFWEEK(gg.call_datetime)
					");

                    $res12 = $db->getResultArray();
                    foreach ($res12[result] AS $row) {
                            $data['page']['call_distribution_per_day_of_week'] .= '

                   	<tr class="odd">
					<td>' . $row[datetime] . '</td>
					<td>' . ((($row[answer_count] != '') ? $row[answer_count] : "0") + (($row[unanswer_call] != '') ? $row[unanswer_call] : "0")) . '</td>
					<td>' . (($row[answer_count] != '') ? $row[answer_count] : "0") . '</td>
					<td>' . (($row[call_answer_pr] != '') ? $row[call_answer_pr] : "0") . ' %</td>
					<td>' . (($row[unanswer_call] != '') ? $row[unanswer_call] : "0") . '</td>
					<td>' . (($row[call_unanswer_pr] != '') ? $row[call_unanswer_pr] : "0") . '%</td>
					<td>' . (($row[avg_durat] != '') ? $row[avg_durat] : "0") . ' წამი</td>
					<td>' . (($row[avg_hold] != '') ? $row[avg_hold] : "0") . ' წამი</td>

					</tr>
						';

                        }

//---------------------------------------------------
                    }
                }

    echo json_encode($data);
}

//get charts data
if(isset($_REQUEST["request"]) && $_REQUEST["request"] == "slDiagram") {

    $done         = $_REQUEST['done'];
    $start_time   = $_REQUEST['start'];
    $end_time     = $_REQUEST['end'];
    $procent      = $_REQUEST['procent'];
    $number       = $_REQUEST['number'];
    $day          = $_REQUEST['day'];
    $users        = $_REQUEST['users'];
    if($users == 0){
        $users_query = "";
    }else{
        $users_query = "AND user_id = '$users'";
    }

    if($done == 1){

        $db->setQuery("SELECT   'SL-ფაქტიური' AS `status`,
                                 DATE(asterisk_incomming.call_datetime) AS `date`,
                                 ROUND((SUM(IF(asterisk_incomming.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                 COUNT(asterisk_incomming.wait_time ) AS `num`
                       FROM     `asterisk_incomming`
                       WHERE     DATE(asterisk_incomming.call_datetime) BETWEEN '$start_time' AND '$end_time' AND asterisk_incomming.disconnect_cause != '2' AND    asterisk_incomming.disconnect_cause NOT IN(1,0) $users_query
                       GROUP BY  DATE(asterisk_incomming.call_datetime)");
        
        $result = $db->getResultArray();
        
        $db->setQuery(" SELECT    'SL-ფაქტიური' AS `status`,
                                  DATE(asterisk_incomming.call_datetime) AS `date`,
                                  ROUND((SUM(IF(asterisk_incomming.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                  COUNT(asterisk_incomming.wait_time) AS `num`
                        FROM      `asterisk_incomming`
                        WHERE     DATE(asterisk_incomming.call_datetime) BETWEEN '$start_time' AND '$end_time' AND asterisk_incomming.disconnect_cause != '2' AND    asterisk_incomming.disconnect_cause NOT IN(1,0)  $users_query
                        GROUP BY  DATE(asterisk_incomming.call_datetime)");
        
        $result1 = $db->getResultArray();
        
        $db->setQuery("SELECT    'SL-ფაქტიური' AS `status`,
                                  DATE(asterisk_incomming.call_datetime) AS `date`,
                                  ROUND((SUM(IF(asterisk_incomming.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                  COUNT(asterisk_incomming.wait_time ) AS `num`
                        FROM      `asterisk_incomming`
                        WHERE     DATE(asterisk_incomming.call_datetime) BETWEEN '$start_time' AND '$end_time' AND asterisk_incomming.disconnect_cause = '2'
                        GROUP BY  DATE(asterisk_incomming.call_datetime)");
        $result2 = $db->getResultArray();
        $all = mysql_fetch_assoc(mysql_query("SELECT      'SL-ფაქტიური' AS `status`,
                                                            DATE(asterisk_incomming.call_datetime) AS `date`,
                                                            ROUND((SUM(IF(asterisk_incomming.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                                            COUNT(asterisk_incomming.wait_time ) AS `num`
                                                FROM        `asterisk_incomming`
                                                WHERE       DATE(asterisk_incomming.call_datetime) BETWEEN '$start_time' AND '$end_time' AND asterisk_incomming.disconnect_cause != '2' AND asterisk_incomming.disconnect_cause NOT IN(1,0)  $users_query"));

        $all_res = $db->getResultArray();
        $all = $all_res[result][0];
        
        foreach($result[result] AS $row) {
            $name_answer[]     = $row['status'];
            $percent_answer[] = (float)$row['percent'];
            $count_answer[] = (float)$row['num'];
            $date[] = $row['date'];
            $limit_number[] = (float)$number;
            $limit_percent[] = (float)$procent;
        }

        foreach($result1[result] AS $row) {
            $name_unanswer[]     = $row['status'];
            $percent_unanswer[] = (float)$row['percent'];
            $count_unanswer[] = (float)$row['num'];
            $date[] = $row['date'];

        }

        foreach($result2[result] AS $row) {
            $unanswer[] = (float)$row['num'];
            $date[] = $row['date'];

        }


    }elseif($done == 2){
        $db->setQuery(" SELECT  'SL-ფაქტიური' AS `status`,
                                 HOUR(asterisk_incomming.call_datetime) AS `hourCount`,
                                 CONCAT(HOUR(asterisk_incomming.call_datetime), ':00') AS `hour`,
                                 ROUND((SUM(IF(asterisk_incomming.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                 COUNT(asterisk_incomming.wait_time) as `total`
                        FROM     `asterisk_incomming`
                        WHERE    DATE(asterisk_incomming.call_datetime) = '$day' AND    asterisk_incomming.disconnect_cause NOT IN(1,0) AND asterisk_incomming.disconnect_cause != '2'  $users_query
                        GROUP BY HOUR(asterisk_incomming.call_datetime)");
        
        $result = $db->getResultArray();
        
        $db->setQuery(" SELECT  'SL-ფაქტიური' AS `status`,
                                 HOUR(asterisk_incomming.call_datetime) AS `hourCount`,
                                 CONCAT(HOUR(asterisk_incomming.call_datetime), ':00') AS `hour`,
                                 ROUND((SUM(IF(asterisk_incomming.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                 COUNT(asterisk_incomming.wait_time) as `total`
                        FROM     `asterisk_incomming`
                        WHERE    DATE(asterisk_incomming.call_datetime) = '$day'  AND asterisk_incomming.disconnect_cause = '2' 
                        GROUP BY HOUR(asterisk_incomming.call_datetime)");
        $result1 = $db->getResultArray();

        foreach ($result[result] AS $res) {
            $myarray[$res[hourCount]] = $res[hour];
            $myarray1[$res[hourCount]] = $res[percent];
        }

        foreach ($result1[result] AS $res1) {
            $myarray2[$res1[hourCount]] = $res1[hour];
            $myarray12[$res1[hourCount]] = $res1[percent];
        }

        for($i = 0; $i <= 23; $i++) {
            if(array_key_exists($i,$myarray)){
                if(strlen($i) == 1){
                    $date[] = '0'.$i.':00';
                }else{
                    $date[] = $i.':00';
                }
                $percent_answer[] = (float)$myarray[$i];
                $count_answer[] = (float)$myarray1[$i];
            }else{
                if(strlen($i) == 1){
                    $date[] = '0'.$i.':00';
                }else{
                    $date[] = $i.':00';
                }
                $percent_answer[] = 0;
                $count_answer[] = 0;
            }

            if(array_key_exists($i,$myarray2)){
                $unhour[] = (float)$myarray12[$i];
            }else{
                $unhour[] = 0;
            }

            $name_answer[]     = 'SL-ფაქტიური';
            $limit_number[] = (float)$number;
            $limit_percent[] = (float)$procent;

        }

    }

    if($done == 3){
        $db->setQuery(" SELECT  CONCAT(HOUR(asterisk_incomming.call_datetime), IF(MINUTE(asterisk_incomming.call_datetime) >= 30, '.5', '')) *2 AS `hour`,
                                ROUND((SUM(IF(asterisk_incomming.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                FLOOR(UNIX_TIMESTAMP(asterisk_incomming.call_datetime) / (30 * 60)) AS time,
                                COUNT(asterisk_incomming.wait_time) as `total`
                        FROM    `asterisk_incomming`
                        WHERE    DATE(asterisk_incomming.call_datetime) = '$day' AND    asterisk_incomming.disconnect_cause NOT IN(1,0) AND asterisk_incomming.disconnect_cause != '2'  $users_query
                        GROUP BY time");
        $result = $db->getResultArray();
        
        $db->setQuery(" SELECT  CONCAT(HOUR(asterisk_incomming.call_datetime), IF(MINUTE(asterisk_incomming.call_datetime) >= 30, '.5', '')) *2 AS `hour`,
                                ROUND((SUM(IF(asterisk_incomming.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                FLOOR(UNIX_TIMESTAMP(asterisk_incomming.call_datetime) / (30 * 60)) AS time,
                                COUNT(asterisk_incomming.wait_time) as `total`
                        FROM    `asterisk_incomming`
                        WHERE   DATE(asterisk_incomming.call_datetime) = '$day' AND asterisk_incomming.disconnect_cause = '2'  $users_query
                        GROUP BY  time");
        $result1 = $db->getResultArray();
        
        foreach($result[result] AS $res){
            $my_array[$res[0]] = $res[1];
            $myarray1[$res[0]] = $res[3];
        }

        foreach($result[result] AS $res){
            $my_array2[$res1[0]] = $res1[1];
            $myarray12[$res1[0]] = $res1[3];
        }

        for($i = 0; $i <= 47; $i++) {
            if(array_key_exists($i,$my_array)){
                if(strlen($i/2) == 1){
                    if($i % 2 == 0) {
                        $date[] = (($i-1)/2+0.5).':00';
                    }else{
                        $date[] = (($i-1)/2).':30';
                    }
                }else{
                    if($i % 2 == 0) {
                        $date[] = (($i)/2).':00';
                    }else{
                        $date[] = (($i-1)/2).':30';
                    }
                }
                $percent_unanswer[] = (float)$my_array[$i];
                $count_answer[] = (float)$myarray1[$i];
            }else{
                if(strlen(round(($i/2))) == 1){
                    if($i % 2 == 0) {
                        $date[] = '0'.(($i-1)/2+0.5).':00';
                    }else{
                        $date[] = '0'.(($i-1)/2).':30';
                    }
                }else{
                    if($i % 2 == 0) {
                        $date[] = (($i)/2).':00';
                    }else{
                        $date[] = (($i-1)/2).':30';

                    }
                }
                $percent_unanswer[] = 0;
                $count_answer[] = 0;
            }

            if(array_key_exists($i,$my_array2)){
                $unmin[] = (float)$myarray12[$i];
            }else{
                $unmin[] = 0;
            }

            $name_unanswer[]     = 'SL-ფაქტიური';
            $limit_number[] = (float)$number;
            $limit_percent[] = (float)$procent;

        }
    }
    
    if($done == 4){
//         mysql_query("SELECT   persons.`name`,
//                               ext
//                      FROM    `users`
//                      JOIN     persons ON users.person_id = persons.id
//                      WHERE    NOT ISNULL(ext)
//                      ORDER BY ext ASC");
    }


    $unit     = " %";

    $serie1[] = array('count_unanswer' => $count_unanswer, 'count_answer' => $count_answer, 'name_answer' => $name_answer[0], 'name_unanswer' => $name_unanswer[0], 'percent_answer' => $percent_answer, 'percent_unanswer' => $percent_unanswer, 'unit' => $unit, 'date' => $date, 'limit_number' => $limit_number, 'limit_percent' => $limit_percent,'unanswer' => $unanswer, 'unhour' => $unhour, 'unmin' => $unmin, 'all_answer' => $all[result][0][num], 'all_procent' => $all[result][0][percent] );


    echo json_encode($serie1);

}

?>