<?php
require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();
$start = $_REQUEST['start'];
$end   = $_REQUEST['end'];
$agent = $_REQUEST['agent'];
$queuet = $_REQUEST['queuet'];

$db->setQuery("SELECT   COUNT(*) AS `count`,
            			'ნაპასუხები' AS `cause`
                FROM 	`asterisk_incomming`
                WHERE	disconnect_cause IN(3,4)
                AND DATE(call_datetime) >= '$start'
                AND DATE(call_datetime) <= '$end' 
                AND dst_queue IN ($queuet) 
                AND dst_extension in ($agent)
                UNION ALL
                SELECT 	COUNT(*) AS `count`,
                		'უპასუხო' AS `cause`
                FROM 	`asterisk_incomming`
                WHERE	disconnect_cause = '2'
                AND DATE(call_datetime) >= '$start'
                AND DATE(call_datetime) <= '$end' 
                AND dst_queue IN ($queuet) ");

$result = $db->getResultArray();
$row = array();
$rows = array();
foreach($result[result] AS $r) {
    $row[0] = $r[cause].': '.$r[count];
    $row[1] = (float)$r[count];
	array_push($rows,$row);
}


echo json_encode($rows);

?>