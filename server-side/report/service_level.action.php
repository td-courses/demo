<?php
require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();

$done         = $_REQUEST['done'];
$start_time   = $_REQUEST['start'];
$end_time     = $_REQUEST['end'];
$procent      = $_REQUEST['procent'];
$number       = $_REQUEST['number'];
$day          = $_REQUEST['day'];
$users        = $_REQUEST['users'];

$users_query = "";

if($users == 'all'){
    $check_wwek = "";
}else{
    $check_wwek = " AND WEEKDAY(asterisk_incomming.call_datetime) = $users";
}
//------------------------------------------------query-------------------------------------------

    if($done == 1){
    	$db->setQuery("	SELECT   'SL-ფაქტიური' AS `status`,
                	              DATE(asterisk_incomming.call_datetime) AS `date`,
                	              ROUND((SUM(IF(asterisk_incomming.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                  COUNT(asterisk_incomming.wait_time ) AS `num`
                	    FROM     `asterisk_incomming`
                	    WHERE     DATE(asterisk_incomming.call_datetime) BETWEEN '$start_time' AND '$end_time' AND asterisk_incomming.disconnect_cause IN(3,4) $check_wwek  $users_query
                	    GROUP BY  DATE(asterisk_incomming.call_datetime)");
    	$result = $db->getResultArray();
    	$db->setQuery(" SELECT    'SL-ფაქტიური' AS `status`,
                	               DATE(asterisk_incomming.call_datetime) AS `date`,
                	               ROUND((SUM(IF(asterisk_incomming.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                   COUNT(asterisk_incomming.wait_time) AS `num`
                	     FROM     `asterisk_incomming`
                	     WHERE     DATE(asterisk_incomming.call_datetime) BETWEEN '$start_time' AND '$end_time' AND asterisk_incomming.disconnect_cause = '2' $check_wwek $users_query
                	     GROUP BY  DATE(asterisk_incomming.call_datetime)");
    	$result1 = $db->getResultArray();
    	$db->setQuery(" SELECT    COUNT(asterisk_incomming.wait_time) AS `num`
                	    FROM     `asterisk_incomming`
                	    WHERE     DATE(asterisk_incomming.call_datetime) BETWEEN '$start_time' AND '$end_time' AND    asterisk_incomming.disconnect_cause NOT IN(1,0) $check_wwek $users_query
                	    GROUP BY  DATE(asterisk_incomming.call_datetime)");
    	$result2 = $db->getResultArray();
    	$db->setQuery(" SELECT 'SL-ფაქტიური' AS `status`,
                        	    DATE(asterisk_incomming.call_datetime) AS `date`,
                        	    ROUND((SUM(IF(asterisk_incomming.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                        	    COUNT(asterisk_incomming.wait_time ) AS `num`
                	    FROM   `asterisk_incomming`
                	    WHERE   DATE(asterisk_incomming.call_datetime) BETWEEN '$start_time' AND '$end_time' AND asterisk_incomming.disconnect_cause IN(3,4)  $users_query");
    	$all = $db->getResultArray();
    	
    	foreach ($result[result] AS $row) {
    	    $name_answer[]     = $row['status'];
    	    $percent_answer[] = (float)$row['percent'];
    	    $count_answer[] = (float)$row['num'];
    	    $date[] = $row['date'];
    	    $limit_number[] = (float)$number;
    	    $limit_percent[] = (float)$procent;
    	}
    	
    	foreach ($result1[result] AS $row) {
    	    $name_unanswer[]     = (($row['status']=='')?'SL-ფაქტიური':$row['status']);
    	    $percent_unanswer[] = (($row['status']=='')?0:(float)$row['percent']);
    	    $count_unanswer[] = (($row['status']=='')?0:(float)$row['num']);
    	    $date[] = $row['date'];
    	
    	}
    	
    	foreach ($result2[result] AS $row) {
             $count_all[] = (float)$row['num'];
        }
    }elseif($done == 2){
	$db->setQuery(" SELECT  'SL-ფაქტიური' AS `status`,
        	                 HOUR(asterisk_incomming.call_datetime) AS `hourCount`,
            	             CONCAT(HOUR(asterisk_incomming.call_datetime), ':00') AS `hour`,
            	             ROUND((SUM(IF(asterisk_incomming.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                             COUNT(asterisk_incomming.wait_time) as `total`
            	    FROM     `asterisk_incomming`
            	    WHERE    DATE(asterisk_incomming.call_datetime) = '$day' AND asterisk_incomming.disconnect_cause IN(3,4) $check_wwek $users_query
            	    GROUP BY HOUR(asterisk_incomming.call_datetime)");
	$result = $db->getResultArray();
	$db->setQuery("SELECT   'SL-ფაქტიური' AS `status`,
                    	     HOUR(asterisk_incomming.call_datetime) AS `hourCount`,
                    	     CONCAT(HOUR(asterisk_incomming.call_datetime), ':00') AS `hour`,
                    	     ROUND((SUM(IF(asterisk_incomming.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                    	     COUNT(asterisk_incomming.wait_time) as `total`
            	    FROM    `asterisk_incomming`
            	    WHERE    DATE(asterisk_incomming.call_datetime) = '$day' AND asterisk_incomming.disconnect_cause = '2' $check_wwek $users_query
            	    GROUP BY HOUR(asterisk_incomming.call_datetime)");
	$result1 = $db->getResultArray();
	$db->setQuery("SELECT    'SL-ფაქტიური' AS `status`,
                    	      HOUR(asterisk_incomming.call_datetime) AS `hourCount`,
                    	      CONCAT(HOUR(asterisk_incomming.call_datetime), ':00') AS `hour`,
                    	      ROUND((SUM(IF(asterisk_incomming.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                    	      COUNT(asterisk_incomming.wait_time) as `total`
            	    FROM     `asterisk_incomming`
            	    WHERE     DATE(asterisk_incomming.call_datetime) = '$day' $check_wwek $users_query
            	    GROUP BY  HOUR(asterisk_incomming.call_datetime)");
	$result2 = $db->getResultArray();

	foreach ($result[result] AS $res) {	    	    
	    $myarray[$res[hourCount]] = $res[percent];
	    $myarray21[$res[hourCount]] = $res[total];
	}
	
	foreach ($result1[result] AS $res1) {	
	    $myarray1[$res1[hourCount]] = $res1[percent];
	    $myarray12[$res1[hourCount]] = $res1[total];
	}
	
	foreach ($result2[result] AS $res2) {

	    $myarray2[$res2[hourCount]] = $res2[total];
	}
	
	for($i = 0; $i <= 23; $i++) {
	    if(array_key_exists($i,$myarray)){
	        if(strlen($i) == 1){
	           $date[] = '0'.$i.':00';
	        }else{
	           $date[] = $i.':00';
	        }
	        $percent_answer[] = (float)$myarray[$i];
	        $count_answer[] = (float)$myarray21[$i];
	    }else{
	        if(strlen($i) == 1){
	           $date[] = '0'.$i.':00';
	        }else{
	           $date[] = $i.':00';
	        }
	        $percent_answer[] = 0;
	        $count_answer[] = 0;
	    }
	    
	    if(array_key_exists($i,$myarray1)){
	        $count_unanswer[] = (float)$myarray12[$i];
	    }else{
	        $count_unanswer[] = 0;
	    }
	    
	    if(array_key_exists($i,$myarray2)){
	        $count_all[] = (float)$myarray2[$i];
	    }else{
	        $count_all[] = 0;
	    }
	    
	    $name_answer[]     = 'SL-ფაქტიური';
	    $limit_number[] = (float)$number;
	    $limit_percent[] = (float)$procent;
	    
	}
	
    }
    
    if($done == 3){
        $db->setQuery(" SELECT  CONCAT(HOUR(asterisk_incomming.call_datetime), IF(MINUTE(asterisk_incomming.call_datetime) >= 30, '.5', '')) *2 AS `hour`,
                                ROUND((SUM(IF(asterisk_incomming.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                FLOOR(UNIX_TIMESTAMP(asterisk_incomming.call_datetime) / (30 * 60)) AS time,
                                COUNT(asterisk_incomming.wait_time) as `total`
                        FROM    `asterisk_incomming`
                        WHERE   DATE(asterisk_incomming.call_datetime) = '$day' AND asterisk_incomming.disconnect_cause IN(3,4) $users_query $users_query
                        GROUP BY  time");
        $result = $db->getResultArray();
        $db->setQuery("SELECT  CONCAT(HOUR(asterisk_incomming.call_datetime), IF(MINUTE(asterisk_incomming.call_datetime) >= 30, '.5', '')) *2 AS `hour`,
                                ROUND((SUM(IF(asterisk_incomming.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                FLOOR(UNIX_TIMESTAMP(asterisk_incomming.call_datetime) / (30 * 60)) AS time,
                                COUNT(asterisk_incomming.wait_time) as `total`
                        FROM    `asterisk_incomming`
                        WHERE   DATE(asterisk_incomming.call_datetime) = '$day' AND asterisk_incomming.disconnect_cause = '2' $users_query $users_query
                        GROUP BY  time");
        $result1 = $db->getResultArray();
        $db->setQuery("SELECT  CONCAT(HOUR(asterisk_incomming.call_datetime), IF(MINUTE(asterisk_incomming.call_datetime) >= 30, '.5', '')) *2 AS `hour`,
                                ROUND((SUM(IF(asterisk_incomming.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                FLOOR(UNIX_TIMESTAMP(asterisk_incomming.call_datetime) / (30 * 60)) AS time,
                                COUNT(asterisk_incomming.wait_time) as `total`
                        FROM    `asterisk_incomming`
                        WHERE   DATE(asterisk_incomming.call_datetime) = '$day' $users_query $users_query
                        GROUP BY  time");
        $result2 = $db->getResultArray();
        
        foreach($result[result] AS $res) {
            $my_array[$res[hour]] = $res[percent];
            $myarray1[$res[hour]] = $res[total];
        }
        
        foreach($result1[result] AS $res1) {
            $my_array1[$res1[hour]] = $res1[percent];
            $myarray12[$res1[hour]] = $res1[total];
        }
        
        foreach($result2[result] AS $res2){
            $myarray2[$res2[hour]] = $res2[total];
        }
        
        for($i = 0; $i <= 47; $i++) {
            if(array_key_exists($i,$my_array)){
                if(strlen($i/2) == 1){
                    if($i % 2 == 0) {
                        $date[] = (($i-1)/2+0.5).':00';
                    }else{
                        $date[] = (($i-1)/2).':30';
                    }
                }else{
                    if($i % 2 == 0) {
                        $date[] = (($i)/2).':00';
                    }else{
                        $date[] = (($i-1)/2).':30';
                    }
                }
                $percent_unanswer[] = (float)$my_array[$i];
                $count_answer[] = (float)$myarray1[$i];
            }else{
                if(strlen(round(($i/2))) == 1){
                    if($i % 2 == 0) { 
                        $date[] = '0'.(($i-1)/2+0.5).':00';
                    }else{
                        $date[] = '0'.(($i-1)/2).':30';
                    }
                }else{
                    if($i % 2 == 0) {
                        $date[] = (($i)/2).':00';
                    }else{
                        $date[] = (($i-1)/2).':30';
                        
                    }
                }
                $percent_unanswer[] = 0;
                $count_answer[] = 0;
            }
             
            if(array_key_exists($i,$my_array1)){
                $count_unanswer[] = (float)$myarray12[$i];
            }else{
                $count_unanswer[] = 0;
            }
            
            if(array_key_exists($i,$myarray2)){
                $count_all[] = (float)$myarray2[$i];
            }else{
                $count_all[] = 0;
            }
             
            $name_unanswer[]     = 'SL-ფაქტიური';
            $limit_number[] = (float)$number;
            $limit_percent[] = (float)$procent;
             
        }
    }
    if($done == 4){

    }
 	

    $unit     = " %";
    
    $serie1[] = array('count_all' => $count_all, 'count_unanswer' => $count_unanswer, 'count_answer' => $count_answer, 'name_answer' => $name_answer[0], 'name_unanswer' => $name_unanswer[0], 'percent_answer' => $percent_answer, 'percent_unanswer' => $percent_unanswer, 'unit' => $unit, 'date' => $date, 'limit_number' => $limit_number, 'limit_percent' => $limit_percent, 'all_answer_input' => $all[result][0][num], 'all_procent_input' => $all[result][0][percent]);
    
    
    echo json_encode($serie1);

?>