<?php

require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();
header('Content-Type: application/json');

    $start_time = $_REQUEST['start'];
    $end_time   = $_REQUEST['end'];
    $agent      = $_REQUEST['agent'];
    $queue      = $_REQUEST['queuet'];
    
    $quantity   = array();
    $cause      = array();
    $cause1     = array();
    $name       = array();
    $agentt     = array();
    $call_count = array();
    $name[]     = '';
    
    //--ნაპასუხები ზარები ოპერატორების მიხედვით
    $db->setQuery("SELECT     user_info.`name` AS `agent`,
            			      COUNT(mail_chat.id) AS `num`
                   FROM 	 `mail_chat`
                   JOIN       incomming_call ON incomming_call.mail_chat_id = mail_chat.id
                   LEFT JOIN  user_info ON incomming_call.user_id=user_info.user_id
                   WHERE	 `mail_chat`.`first_datetime` >= '$start_time'
                   AND       `mail_chat`.`first_datetime` <= '$end_time' 
                   AND       `incomming_call`.`user_id` in ($agent)
                   AND       `status` >2
                   GROUP BY  `incomming_call`.`user_id`");
    
    $ress = $db->getResultArray();
    foreach($ress[result] AS $row1){
        $call_count[] = (float)$row1[num];
    	$agentt[]	  = $row1[agent];
    }
    
    $db->setQuery(" SELECT   CASE
                                WHEN DAYOFWEEK(first_datetime) = 1 THEN 'კვირა'
                                WHEN DAYOFWEEK(first_datetime) = 2 THEN 'ორშაბათი'
                                WHEN DAYOFWEEK(first_datetime) = 3 THEN 'სამშაბათი'
                                WHEN DAYOFWEEK(first_datetime) = 4 THEN 'ოთხშაბათი'
                                WHEN DAYOFWEEK(first_datetime) = 5 THEN 'ხუთშაბათი'
                                WHEN DAYOFWEEK(first_datetime) = 6 THEN 'პარასკევი'
                                WHEN DAYOFWEEK(first_datetime) = 7 THEN 'შაბათი'
                            END AS `datetime`,
                            COUNT(*) AS answer_count
                    FROM  (  SELECT   ROUND(SUM(duration)/COUNT(*)) AS wait_time,
                             dashboard_chat_durations.chat_id
                            FROM    `dashboard_chat_durations`
                            JOIN     mail_chat ON mail_chat.id = dashboard_chat_durations.chat_id
                            JOIN     incomming_call ON incomming_call.mail_chat_id = mail_chat.id
                            WHERE    dashboard_chat_durations.`status` = 1 AND dashboard_chat_durations.source_id = 6
                            AND      mail_chat.`status` > 2 AND incomming_call.user_id IN($agent) AND mail_chat.first_datetime BETWEEN '$start_time' AND '$end_time'
                            GROUP BY chat_id) AS `avg`
                            JOIN     mail_chat ON mail_chat.id = avg.chat_id
                    GROUP BY datetime");
    
    $res = $db->getResultArray();
    
    foreach($res[result] AS $row){
        $answer_count1[] = (float)$row[answer_count];
        $datetime1[]	 = $row[datetime];
    }
    	
    //-- ნაპასუხები ზარები დღეების მიხედვით	
    
    $db->setQuery(" SELECT  DATE(first_datetime) AS `datetime`,
                            COUNT(*) AS answer_count
                    FROM  ( SELECT   ROUND(SUM(duration)/COUNT(*)) AS wait_time,
                    dashboard_chat_durations.chat_id
                    FROM    `dashboard_chat_durations`
                    JOIN     mail_chat ON mail_chat.id = dashboard_chat_durations.chat_id
                    JOIN       incomming_call ON incomming_call.mail_chat_id = mail_chat.id
                    WHERE    dashboard_chat_durations.`status` = 1 AND dashboard_chat_durations.source_id = 6
                    AND      mail_chat.`status` > 2 AND incomming_call.user_id IN($agent) AND mail_chat.first_datetime BETWEEN '$start_time' AND '$end_time'
                    GROUP BY chat_id) AS `avg`
                    JOIN mail_chat ON mail_chat.id = avg.chat_id
                    GROUP BY DATE(first_datetime)");
    
    $res = $db->getResultArray();
    
    foreach($res[result] AS $row){
        $answer_count2[] = (float)$row[answer_count];
        $datetime2[]	 = $row[datetime];
    }
    
    
    //-- უპასუხო ზარები საათების მიხედვით	
    
    $db->setQuery(" SELECT  DATE(first_datetime) AS `datetime`,
                            COUNT(*) AS answer_count
                    FROM  ( SELECT   ROUND(SUM(duration)/COUNT(*)) AS wait_time,
                    dashboard_chat_durations.chat_id
                    FROM    `dashboard_chat_durations`
                    JOIN     mail_chat ON mail_chat.id = dashboard_chat_durations.chat_id
                    JOIN       incomming_call ON incomming_call.mail_chat_id = mail_chat.id
                    WHERE    dashboard_chat_durations.`status` = 1 AND dashboard_chat_durations.source_id = 6
                    AND      mail_chat.`status` > 2 AND incomming_call.user_id IN($agent) AND mail_chat.first_datetime BETWEEN '$start_time' AND '$end_time'
                    GROUP BY chat_id) AS `avg`
                    JOIN mail_chat ON mail_chat.id = avg.chat_id
                    GROUP BY HOUR(first_datetime)");
    
    $res = $db->getResultArray();
    
    foreach($res[result] AS $row){
        
        $answer_count[] = (float)$row[answer_count];
        $datetime[] 	= $row[times];
    }
    
    
    
    		
    //--მომსახურების დონე(Service Level)
    $db->setQuery("SELECT   ROUND(SUM(duration)/COUNT(*)) AS wait_time
                   FROM    `dashboard_chat_durations`
                   JOIN     mail_chat ON mail_chat.id = dashboard_chat_durations.chat_id
                   JOIN       incomming_call ON incomming_call.mail_chat_id = mail_chat.id
                   WHERE    dashboard_chat_durations.`status` = 1 AND dashboard_chat_durations.source_id = 6
                   AND      mail_chat.`status` > 2 AND incomming_call.user_id IN($agent) AND mail_chat.first_datetime BETWEEN '$start_time' AND '$end_time'
                   GROUP BY mail_chat_id");
    
    $sl = $db->getResultArray();
    
    $w15 = 0;
    $w30 = 0;
    $w45 = 0;
    $w60 = 0;
    $w75 = 0;
    $w90 = 0;
    $w91 = 0;
    
    foreach($sl[result] AS $res_service_level_r) {
        
        if ($res_service_level_r['wait_time'] < 15) {$w15++;}
        
        if ($res_service_level_r['wait_time'] < 30){$w30++;}
        
        if ($res_service_level_r['wait_time'] < 45){$w45++;}
        
        if ($res_service_level_r['wait_time'] < 60){$w60++;}
        
        if ($res_service_level_r['wait_time'] < 75){$w75++;}
        
        if ($res_service_level_r['wait_time'] < 90){$w90++;}
        
        $w91++;
        
    }
    
    $d30 = $w30 - $w15;
    $d45 = $w45 - $w30;
    $d60 = $w60 - $w45;
    $d75 = $w75 - $w60;
    $d90 = $w90 - $w75;
    $d91 = $w91 - $w90;
    
    $mas         = array($w15,$d30,$d45,$d60,$d75,$d90,$d91);
    $call_second = array('15 წამში','30 წამში','45წამში','60 წამში','75 წამში','90 წამში','90+წამში');			
    							
    $unit[]   = "  ჩატი";
    $series[] = array('name' => $name, 'unit' => $unit, 'quantity' => $quantity, 'cause' => $cause);
    $series[] = array('name' => $name, 'unit' => $unit, 'call_count' => $call_count, 'agent' => $agentt);
    $series[] = array('name' => $name, 'unit' => $unit, 'answer_count' => $answer_count, 'datetime' => $datetime);
    $series[] = array('name' => $name, 'unit' => $unit, 'answer_count1' => $answer_count1, 'datetime1' => $datetime1);
    $series[] = array('name' => $name, 'unit' => $unit, 'answer_count2' => $answer_count2, 'datetime2' => $datetime2);
    $series[] = array('name' => $name, 'unit' => $unit, 'answer_count3' => $answer_count3, 'cause1' => $cause1);
    $series[] = array('name' => $name, 'unit' => $unit, 'count1' => $count1, 'queue1' => $queue1);
    $series[] = array('name' => $name, 'unit' => $unit, 'count2' => $count2, 'queue2' => $queue2);
    $series[] = array('name' => $name, 'unit' => $unit, 'unanswer_call' => $unanswer_call, 'times' => $times);
    $series[] = array('name' => $name, 'unit' => $unit, 'unanswer_count1' => $unanswer_count1, 'times2' => $times2);
    $series[] = array('name' => $name, 'unit' => $unit, 'unanswer_count2' => $unanswer_call2, 'date1' => $date1);
    $series[] = array('name' => $name, 'unit' => $unit, 'mas' => $mas, 'call_second' => $call_second);

echo json_encode($series);

?>