<?php
// require_once('../../includes/classes/core.php');
include('../../includes/classes/class.Mysqli.php');
$mysqli = new dbClass();
$action	= $_REQUEST['act'];
$error	= '';
$data	= array();
$tableName = 'person';
switch ($action) {
	case 'get_columns':
		
		$columnCount = 		$_REQUEST['count'];
		$columnNames[] = 	$_REQUEST['names'];
		$operators[] = 		$_REQUEST['operators'];
		$selectors[] = 		$_REQUEST['selectors'];
		$query = "SHOW COLUMNS FROM $tableName";
		$mysqli->setQuery($query,$tableName);
		$res = $mysqli->getResultArray();
		
		$i = 0;
		$columns = array();
		foreach($res['result'] AS $item)
		{
			$columns[$i] = $item['Field'];
			$i++;
		}
		
		
		$dat = array();
		$a = 0;
		for($j = 0;$j<$columnCount;$j++)
		{
			if($columns[$j] == 'id')
			{
				continue;
			}
			else
			{
				
				if($operators[0][$a] == 1) $op = true; else $op = false; //  TRANSFORMS 0 OR 1 TO True or False FOR OPERATORS
				
				
				if($res['data_type'][$j] == 'date')
				{
					$g = array('field'=>$columns[$j],'title'=>$columnNames[0][$a],'format'=>"{0:yyyy-MM-dd}",'filterable'=>array('cell'=>array('showOperators'=>$op,'template'=>betweenFilter)));
				}
				else if($selectors[0][$a] != '0') // GETTING SELECTORS WHERE VALUES ARE TABLE NAMES
				{
					$g = array('field'=>$columns[$j],'title'=>$columnNames[0][$a],'filterable'=>array('cell'=>array('showOperators'=>$op)),'values'=>getSelectors($selectors[0][$a]));
				}
				else
				{
					$g = array('field'=>$columns[$j],'title'=>$columnNames[0][$a],'filterable'=>array('cell'=>array('showOperators'=>$op)));
				}
				$a++;
			}
			array_push($dat,$g);
			
		}
		
		array_push($dat,array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px'));
		
		$new_data = array();
		//{"id":"id","fields":[{"id":{"editable":true,"type":"number"}},{"reg_date":{"editable":true,"type":"number"}},{"name":{"editable":true,"type":"number"}},{"surname":{"editable":true,"type":"number"}},{"age":{"editable":true,"type":"number"}}]}
		for($j=0;$j<$columnCount;$j++)
		{
			if($res['data_type'][$j] == 'date')
			{
				$new_data[$columns[$j]] = array('editable'=>false,'type'=>$res['data_type'][$j]);
			}
			else
			{
				$new_data[$columns[$j]] = array('editable'=>true,'type'=>$res['data_type'][$j]);
			}
		}
		
		
		$filtArr = array('id'=>'id','fields'=>$new_data);
		
		
		
		$kendoData = array('columnss'=>$dat,'modelss'=>$filtArr);

		
		//$dat = array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px');
		
		
		$data = $kendoData;
		//$data = '[{"gg":"sd","ads":"213123"}]';
		
		break;
	case 'change_theme':
		
		$themeID = $_REQUEST['theme_id'];
		$user_id = $_SESSION['USERID'];
		
		$query = "
		
		UPDATE users SET kendo_template='$themeID' WHERE id='$user_id'
		
		";
		$mysqli->setQuery($query);
		$mysqli -> execQuery();	

		break;
	case 'get_total':
		$mysqli->setQuery("SELECT COUNT(*) AS cc FROM person WHERE actived='1'");
		$rowCount = $mysqli->getResultArray();
		
		$data = array('totales'=>$rowCount[result][0][cc]);

		break;
	case 'get_list' :
		$columnCount = 		$_REQUEST['count'];
		$json_data = 		json_decode($_REQUEST[add],true);
		$itemPerPage = 		$json_data[pageSize];
		$skip = 			$json_data[skip];
		
		$query = "
		
		SHOW COLUMNS FROM $tableName
		
		";
		$mysqli->setQuery($query,$tableName);
		$cols = $mysqli->getResultArray();
		
		$i = 0;
		$columns = array();
		foreach($cols['result'] AS $item)
		{
			$columns[$i] = $item['Field'];
			$i++;
		}
		
		$filtersValue = array();
		$f = 0;
		$colList = '';
		for($j=0;$j<$columnCount;$j++)
		{
			if($j == $columnCount-1)
				$colList .= "$tableName".".".$columns[$j]." AS ".$columns[$j];
			else $colList .= "$tableName".".".$columns[$j]." AS ".$columns[$j].", ";
			foreach($json_data['filter']['filters'] as $item)
			{
				if($columns[$j] == $item['field'])
				{
					$filtersValue[$j]['operator'] = $item['operator'];
					$filtersValue[$j]['value'] = $item['value'];
				}
			}
		}
		
		//$arr = array('name'=>$nameFilterVal,'surname'=>$surnameFilterVal,'error'=>$err);
		//$data = var_dump($arr);
$counter = 0;

foreach($filtersValue as $item)
{
	if($item['value'] != '')
		$counter++;
}



if ($counter > 0) {
	$where = "";

	$k = 0;
	for($k=0;$k<$columnCount;$k++)
	{
		if(!empty($filtersValue[$k]['value']))
		{
			if($filtersValue[$k]['operator'] == 'eq' or $filtersValue[$k]['operator'] == 'startswith')
			{
				$parameters = $tableName.".".$columns[$k]." LIKE '%".$filtersValue[$k]['value']."%'";
				$where = addWhere($where, $parameters);
			}
			else if($filtersValue[$k]['operator'] == 'starts')
			{
				$parameters = $tableName.".".$columns[$k]." >= '".$filtersValue[$k]['value']."'";
				$where = addWhere($where, $parameters);
			}
			else if($filtersValue[$k]['operator'] == 'ends')
			{
				$parameters = $tableName.".".$columns[$k]." <= '".$filtersValue[$k]['value']."'";
				$where = addWhere($where, $parameters);
			}
			else if($filtersValue[$k]['operator'] == 'start')
			{
				$date = new DateTime($filtersValue[$k]['value']); //// DATE FORMATING AND ADDING 1 DAY TO JS UTC TIME
				$date->modify('+1 day'); //// DATE FORMATING AND ADDING 1 DAY TO JS UTC TIME
				$result = $date->format('Y-m-d'); //// DATE FORMATING AND ADDING 1 DAY TO JS UTC TIME


				$parameters = $tableName.".".$columns[$k]." = '".$result."'";
				$where = addWhere($where, $parameters);
			}
		}
	}

	$sql = "
					SELECT	$colList
				    FROM    `person`";
	if ($where) $sql .= " WHERE actived='1' AND $where LIMIT   $skip,$itemPerPage";
	$mysqli->setQuery($sql,'person');
	$result = $mysqli->getResultArray();
	$mysqli->setQuery("SELECT COUNT(*) AS cc FROM person WHERE actived='1' AND $where");
	$rowCount = $mysqli->getResultArray();
}
else
{
	$sql = "  
					SELECT	$colList
				    FROM    `person`
					
					WHERE actived='1' LIMIT $skip,$itemPerPage
					";

	$mysqli->setQuery($sql,'person');
	$result = $mysqli->getResultArray();
	$mysqli->setQuery("SELECT COUNT(*) AS cc FROM person WHERE actived='1'");
	$rowCount = $mysqli->getResultArray();
}


		$arr = array();
		$i=0;

		foreach($result[result] as $red)
		{
			$arr[$i]['id'] = $red[id];
			$arr[$i]['reg_date'] = $red[reg_date];
			$arr[$i]['name'] = $red[name];
			$arr[$i]['surname'] = $red[surname];
			$arr[$i]['age'] = intval($red[age]);
			$arr[$i]['sex_id'] = $red[sex_id];

			$arr[$i]['DATAAAAAAAA'] = $result.', '.$filtersValue[2]['value'].', '.$filtersValue[3]['value'].', '.$filtersValue[4]['value'].', '.$filtersValue[5]['value'];
			$arr[$i]['counter'] = $counter;
			$arr[$i]['sql'] = $sql;
			$arr[$i]['SKOD'] = $colList;
			$i++;
		}	
		
		$data = array('data'=>$arr,'total'=>$rowCount['result'][0]['cc']);
		//$data = var_dump($result);

		break;
	case 'save_priority':
		
		$json_data = json_decode($_REQUEST[add],true);
		
		$person_id = $json_data[models][0]['personID'];
		$name = $json_data[models][0]['personName'];
		$surname = $json_data[models][0]['personSurname'];
		$age = $json_data[models][0]['Age'];
		$sex_id = $json_data[models][0]['sexID'];
		if($name != '' or $surname != '' or $age != '' or $sex_id != '')
		{
			if(!CheckpersonExist($name, $surname, $age, $sex_id, $cat_id, $subcat_id))
			{
				Saveperson($person_id, $name, $surname, $age, $sex_id, $cat_id, $subcat_id);
			}
		}
		
		

		break;
	case 'disable':
		$json_data = json_decode($_REQUEST[add],true);
		$person_id = $json_data[models][0]['personID'];
		Disableperson($person_id);
	default:
		$error = 'Action is Null';
}



echo json_encode($data);


/* ******************************
 *	Category Functions
* ******************************
*/

function addWhere($where, $add, $and = true) {
	if ($where) {
		if ($and) $where .= " AND $add";
		else $where .= " OR $add";
	}
	else $where = $add;
	return $where;
}

function getSelectors($table)
{
	GLOBAL $mysqli;
	$query = "
				SELECT `name`,`id` 
				FROM   $table 
				ORDER BY id ASC
	";
	
	$mysqli -> setQuery($query);
	$result = $mysqli -> getResultArray();
	
	$selectorData = array();
	
	foreach($result['result'] AS $option)
	{
		array_push($selectorData,array('text'=>$option['name'],'value'=>$option['id']));
	}
	
	return $selectorData;
}

function Addperson($name, $surname, $age, $sex_id, $cat_id, $subcat_id)
{
	GLOBAL $mysqli;
	$user_id	= $_SESSION['USERID'];
	$reg_date = date("Y-m-d");
	$query = "INSERT INTO  `person`
							  (`name`,`surname`,`age`,`sex_id`,`image`,`reg_date`,`cat_id`,`subcat_id`,`emailCount`,`user_id`)
					VALUES 	  
							  ('$name','$surname','$age','$sex_id','','$reg_date','$cat_id','$subcat_id','0','$user_id')";
							  $mysqli -> setQuery($query);
							  $mysqli -> execQuery(); 

}
function Saveperson($person_id, $name, $surname, $age, $sex_id, $cat_id, $subcat_id)
{
	GLOBAL $mysqli;
	$user_id	= $_SESSION['USERID'];
	$query = "	UPDATE `person`
					SET    `name` ='$name',
						   `surname` = '$surname',
						   `age` = '$age',
						   `user_id` = '$user_id',
						   `sex_id` = '$sex_id'
					WHERE  `id`      = $person_id";
					$mysqli -> setQuery($query);
					$mysqli -> execQuery(); 
}

function Disableperson($person_id)
{
	GLOBAL $mysqli;
	$query = "	UPDATE person SET `actived`=0 WHERE id='$person_id'";
					$mysqli -> setQuery($query);
					$mysqli -> execQuery(); 
	
}

function CheckpersonExist($name, $surname, $age, $sex_id, $cat_id, $subcat_id)
{
	GLOBAL $mysqli;
	$query = "	SELECT `id`
											FROM   `person`
											WHERE  `name` = '$name' and `surname` = '$surname' and `age` = '$age' and `sex_id` = '$sex_id' and `subcat_id` = '$subcat_id' and `cat_id` = '$cat_id'";
	$mysqli -> setQuery($query);
	$result = $mysqli -> getResultArray();

	$res = $result['result'][0];
	if($res['id'] != ''){
		return true;
	}
	return false;
}
?>
