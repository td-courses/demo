<?php
require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();

$action	= $_REQUEST['act'];
$error	= '';
$data	= array();

switch ($action) {
	case 'get_add_page':
		$page		= GetPage();
		$data		= array('page'	=> $page);

		break;
	case 'get_edit_page':
		$departmetn_id		= $_REQUEST['id'];
	       $page		= GetPage(Getdepartment($departmetn_id));
           $data		= array('page'	=> $page);

		break;
	case 'get_list' :
		$count	= $_REQUEST['count'];
		$hidden	= $_REQUEST['hidden'];
		 
		$db->setQuery(" SELECT 	department.id,
								department.`name`
					    FROM 	department
					    WHERE 	department.actived=1");
		
		

		$data = $db->getList($count,$hidden,1);
		
		break;
	case 'save_department':
		$department_id 		= $_REQUEST['id'];
		$department_name    = $_REQUEST['name'];
		
	
		
		if($department_name != ''){
			if(!CheckdepartmentExist($department_name, $department_id)){
				if ($department_id == '') {
					Adddepartment( $department_id, $department_name);
				}else {
					Savedepartment($department_id, $department_name);
				}
								
			} else {
				$error = '"' . $department_name . '" უკვე არის სიაში!';
				
			}
		}
		
		break;
	case 'disable':
		$department_id	= $_REQUEST['id'];
		Disabledepartment($department_id);

		break;
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Category Functions
* ******************************
*/

function Adddepartment($department_id, $department_name){
    global $db;
	$user_id	= $_SESSION['USERID'];
	$db->setQuery("INSERT INTO 	 `department`
								(`name`,`user_id`)
					VALUES 		('$department_name', '$user_id')");
	
	$db->execQuery();
	
}

function Savedepartment($department_id, $department_name){
    global $db;
	$user_id	= $_SESSION['USERID'];
	$db->setQuery("	UPDATE `department`
					SET     `name` = '$department_name',
							`user_id` ='$user_id'
					WHERE	`id` = $department_id");
	$db->execQuery();
}

function Disabledepartment($department_id){
    global $db;
	$db->setQuery("	UPDATE `department`
					SET    `actived` = 0
					WHERE  `id` = $department_id");
	$db->execQuery();
}

function CheckdepartmentExist($department_name){
    global $db;
	$db->setQuery("SELECT `id`
				   FROM   `department`
				   WHERE  `name` = '$department_name' && `actived` = 1");
	
	$res=$db->getResultArray();
	if($res[result][0]['id'] != ''){
		return true;
	}
	return false;
}


function Getdepartment($department_id){
    global $db;
	$db->setQuery("SELECT  `id`,
						   `name`
				   FROM    `department`
				   WHERE   `id` = $department_id" );
	
	$res=$db->getResultArray();

	return $res[result][0];
}

function GetPage($res = '')
{
	$data = '
	<div id="dialog-form">
	    <fieldset>
	    	<legend>ძირითადი ინფორმაცია</legend>

	    	<table class="dialog-form-table">
				<tr>
					<td style="width: 170px;"><label for="CallType">დასახელება</label></td>
					<td>
						<input type="text" id="name" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="' . $res['name'] . '" />
					</td>
				</tr>

			</table>
			<!-- ID -->
			<input type="hidden" id="department_id" value="' . $res['id'] . '" />
        </fieldset>
    </div>
    ';
	return $data;
}

?>
