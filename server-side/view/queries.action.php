<?php
include('../../includes/classes/class.Mysqli.php');
global $db;
$db     = new dbClass();
$action	= $_REQUEST['act'];
$error	= '';
$data	= array();
 
switch ($action) {
	case 'get_add_page':
		$page		= GetPage($res='');
		$data		= array('page'	=> $page);

		break;
	case 'get_edit_page':
		$departmetn_id		= $_REQUEST['id'];
	       $page		= GetPage(Getdepartment($departmetn_id));
           $data		= array('page'	=> $page);

		break;
	case 'get_list' :
		$count	= $_REQUEST['count'];
		$hidden	= $_REQUEST['hidden'];
		 
		$db->setQuery("SELECT 	queries.id,
                                queries.id,
                                queries.datetime,
                                user_info.`name`,
								queries.`quest`,
                                queries.`answer`
					    FROM 	queries
                        LEFT JOIN user_info ON user_info.user_id = queries.user_id
					    WHERE 	queries.actived=1");

		$data = $db->getList($count, $hidden,1);

		break;
	case 'get_list_log' :
	    $count	= $_REQUEST['count'];
	    $hidden	= $_REQUEST['hidden'];
	    	
	   $db->setQuery("SELECT 	`logs`.`id`,
                				`logs`.`row_id`,
                				`logs`.`date`,
                				IF(`logs`.`event` = 1,'დამატება',IF(`logs`.actived = 0,'წაშლა','განახლება')) AS `act`,
                				`user_info`.`name`,
								CASE 
										WHEN `logs`.`collumn` = 'name' then 'დასახელება'
								END AS `colum`,
                				`logs`.`old_value`,
                				`logs`.`new_value`
                        FROM    `logs`
                        JOIN    `users` ON `logs`.user_id = users.id
                        JOIN    `user_info` ON users.id = user_info.user_id
                        WHERE   `logs`.`table` = 'queries'");
	
	    $data = $db->getList($count, $hidden);
	
	    break;
	case 'save_department':
		$department_id = $_REQUEST['id'];
		
		$quest  = htmlspecialchars($_REQUEST['quest'], ENT_QUOTES);
		$answer = htmlspecialchars($_REQUEST['answer'], ENT_QUOTES);
		
		if($quest != ''){
			if ($department_id == ''){
				Adddepartment($department_id, $quest, $answer);
			}else{
				Savedepartment($department_id, $quest, $answer);
			}
		}
		
		break;
	case 'disable':
		$department_id	= $_REQUEST['id'];
		Disabledepartment($department_id);

		break;
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Category Functions
* ******************************
*/

function Adddepartment($department_id, $quest, $answer){
    global $db;
	$user_id	= $_SESSION['USERID'];
	$site	    = $_REQUEST['site'];
	$source_id	= $_REQUEST['source_id'];
	$db->setQuery("INSERT INTO `queries`
							  (datetime, `user_id`, `quest`, `answer`)
					    VALUES 		
                              (NOW(), '$user_id', '$quest', '$answer')");
	
	$db->execQuery();
	$queries_id = $db->getLastId();
	
	$site_array = explode(",",$site);
	$db->setQuery("DELETE FROM queries_site WHERE queries_id = '$queries_id'");
	$db->execQuery();
	
	foreach($site_array AS $site_id){
	    $db->setQuery("INSERT INTO queries_site (queries_id, site_id) values ('$queries_id', $site_id)");
	    $db->execQuery();
	}
	
	$source_array = explode(",",$source_id);
	$db->setQuery("DELETE FROM queries_source WHERE queries_id = '$queries_id'");
	$db->execQuery();
	
	foreach($source_array AS $source_id){
	    $db->setQuery("INSERT INTO queries_source (queries_id, source_id) values ('$queries_id', $source_id)");
	    $db->execQuery();
	}
	
	$db->setQuery("INSERT INTO `queries_history`
                              (`user_id`, `datetime`, `queries_id`, `status`, `actived`) 
                         VALUES 
                              ('$user_id', NOW(), '$queries_id', 1, 1)");
	$db->execQuery();
}

function Savedepartment($department_id, $quest, $answer){
    global $db;
	$user_id	= $_SESSION['USERID'];
	$site	    = $_REQUEST['site'];
	$source_id	= $_REQUEST['source_id'];
	
	$db->setQuery("	UPDATE `queries`
					   SET `datetime` = NOW(),
                           `user_id` = '$user_id',
                           `quest`  = '$quest',
	                       `answer` = '$answer'
					WHERE  `id`     = '$department_id'");
	$db->execQuery();
	
	$site_array = explode(",",$site);
	$db->setQuery("DELETE FROM queries_site WHERE queries_id = '$department_id'");
	$db->execQuery();
	
	foreach($site_array AS $site_id){
	    $db->setQuery("INSERT INTO queries_site (queries_id, site_id) values ('$department_id', $site_id)");
	    $db->execQuery();
	}
	
	$source_array = explode(",",$source_id);
	$db->setQuery("DELETE FROM queries_source WHERE queries_id = '$department_id'");
	$db->execQuery();
	
	foreach($source_array AS $source_id){
	    $db->setQuery("INSERT INTO queries_source (queries_id, source_id) values ('$department_id', $source_id)");
	    $db->execQuery();
	}
}

function Disabledepartment($department_id){
    global $db;
    $db->setQuery("UPDATE `queries`
					  SET `actived` = 0
				   WHERE  `id`      = $department_id");
	
	$db->execQuery();
}

function CheckdepartmentExist($department_name){
    global $db;
    $db->setQuery("SELECT `id`
				   FROM   `queries`
				   WHERE  `name` = '$department_name' && `actived` = 1");
    
    $res = $db->getResultArray();
	if($res[result][0]['id'] != ''){
		return true;
	}
	return false;
}

function web_site($id){
    global $db;
    $data = '';
    
    $db->setQuery("SELECT `id`, `name`
				   FROM   `my_web_site`
				   WHERE   actived = 1");
    
    $res = $db->getResultArray();
    foreach ($res[result] AS $value){
        $db->setQuery("SELECT id
                       FROM  `queries_site`
                       WHERE  queries_site.queries_id = '$id' AND site_id = '$value[id]'");
        $check = $db->getNumRow();
        
        if($check>0){
            $data .= '<option value="' . $value['id'] . '" selected="selected">' . $value['name'] . '</option>';
        } else {
            $data .= '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
        }
        
    }
    
    return $data;
}

function get_source($id){
    global $db;
    $data = '';
    
    $db->setQuery("SELECT `id`,
                          `name`
				   FROM   `source`
				   WHERE   actived = 1");
    
    $res = $db->getResultArray();
    foreach ($res[result] AS $value){
        $db->setQuery("SELECT id
                       FROM  `queries_source`
                       WHERE  queries_source.queries_id = '$id' AND source_id = '$value[id]'");
        $check = $db->getNumRow();
        
        if($check>0){
            $data .= '<option value="' . $value['id'] . '" selected="selected">' . $value['name'] . '</option>';
        } else {
            $data .= '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
        }
        
    }
    
    return $data;
}

function Getdepartment($department_id){
    global $db;
	$db->setQuery(" SELECT  `id`,
							`quest`,
                            `answer`
					FROM    `queries`
					WHERE   `id` = $department_id" );

	$req = $db->getResultArray();
	return $req[result][0];
}

function GetPage($res = ''){
    
	$data = '
	<div id="dialog-form">
	    <fieldset>
	    	<legend>ძირითადი ინფორმაცია</legend>

	    	<table class="dialog-form-table">
                <tr>
					<td style="width: 170px;"><label for="CallType">საიტი</label></td>
				</tr>
                <tr>
					<td>
						<select style="height: 18px; width: 1175px;" id="my_site" class="idls object" multiple>'.web_site($res['id']).'</select>
					</td>
				</tr>
                <tr style="height:10px;"></tr>
				<tr>
					<td style="width: 170px;"><label for="CallType">წყარო</label></td>
				</tr>
                <tr>
					<td>
						<select style="height: 18px; width: 1175px;" id="source_id" class="idls object" multiple>'.get_source($res['id']).'</select>
					</td>
				</tr>
				<tr style="height:10px;"></tr>
                <tr style="height:0;">
					<td style="width: 70px;"><label for="quest">კითხვა</label></td>
				</tr>
				<tr style="height:0;">
					<td>
						<textarea type="text" id="quest" style="width:1170px; resize: vertical;">' . $res['quest'] . '</textarea>
					</td>
				</tr>
                <tr style="height:10;"></tr>
				<tr style="height:0;">
					<td style="width: 70px;"><label for="answer">პასუხი</label></td>
				</tr>
                <tr style="height:0;">
					<td style="width: 1177px;">
						<textarea type="text" id="answer" style="width:850px; resize: vertical;height: 300px;">' . $res['answer'] . '</textarea>
					</td>
				</tr>
			</table>
			<!-- ID -->
			<input type="hidden" id="department_id" value="' . $res['id'] . '" />
        </fieldset>
    </div>
    ';
	return $data;
}

?>
