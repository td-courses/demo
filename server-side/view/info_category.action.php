<?php
/* ******************************
 *	Category aJax actions
 * ******************************
*/
include('../../includes/classes/class.Mysqli.php');
global $db;
$db     = new dbClass();

$action	= $_REQUEST['act'];
$error	= '';
$data	= array();
$par_id 		= $_REQUEST['par_id'];
switch ($action) {
    case 'get_add_page':
        $page		= GetPage();
        $data		= array('page'	=> $page);

        break;
    case 'get_edit_page':
        $cat_id		= $_REQUEST['id'];
        $page		= GetPage(GetCategory($cat_id));

        $data		= array('page'	=> $page);

        break;
    case 'get_list' :
        $data['page'] = '';
        $db->setQuery("SELECT id,CONCAT(`name`,'<img my_id=',id,' style=\"margin-left: 10px;margin-right: 10px;\" class=\"cat_img cat_edit\" alt=\"img\" src=\"media/images/icons/edit.svg\"><img my_id=',id,' class=\"cat_img cat_delete\" alt=\"img\" src=\"media/images/icons/delete.svg\">') AS `name` FROM `info_category` WHERE actived = 1 AND parent_id = 0");
        
        $result = $db->getResultArray();
        foreach ($result[result] AS $cat_res){
            $data['page'] .= '<li>'.$cat_res[name];
            $db->setQuery("SELECT id,CONCAT(`name`,'<img my_id=',id,' style=\"margin-left: 10px;margin-right: 10px;\" class=\"cat_img cat_edit\" alt=\"img\" src=\"media/images/icons/edit.svg\"><img my_id=',id,' class=\"cat_img cat_delete\" alt=\"img\" src=\"media/images/icons/delete.svg\">') AS `name` FROM `info_category` WHERE actived = 1 AND parent_id = '$cat_res[id]'");
            $result_sub = $db->getResultArray();
            $ul_close = 0;
            $i=0;
            foreach ($result_sub[result] AS $cat1_res){
                $i++;
                if($i==1){
                    $data['page'] .= '<ul>';
                }
                $data['page'] .= '<li>'.$cat1_res[name];
                $db->setQuery("SELECT id,CONCAT(`name`,'<img my_id=',id,' style=\"margin-left: 10px;margin-right: 10px;\" class=\"cat_img cat_edit\" alt=\"img\" src=\"media/images/icons/edit.svg\"><img my_id=',id,' class=\"cat_img cat_delete\" alt=\"img\" src=\"media/images/icons/delete.svg\">') AS `name` FROM `info_category` WHERE actived = 1 AND parent_id = '$cat1_res[id]'");
                $result_sub_sub = $db->getResultArray();
                $ul_close1 = 0;
                $i1=0;
                foreach ($result_sub_sub[result] AS $cat2_res){
                    $i1++;
                    if($i1==1){
                        $data['page'] .= '<ul>';
                    }
                    $data['page'] .= '<li>'.$cat2_res[name].'</li>';
                    $ul_close1 = 1;
                }
                if($ul_close1==1){
                    $data['page'] .= '</ul>';
                }
                $data['page'] .= '</li>';
                $ul_close = 1;
            }
            if($ul_close==1){
                $data['page'] .= '</ul>';
            }
            $data['page'] .= '</li>';
        }
        break;

    case 'save_category':
        $cat_id 		      = $_REQUEST['id'];
        $category_value	      = $_REQUEST['category_value'];
        $category_select	  = $_REQUEST['category_select'];
        $sub_category_1_value = $_REQUEST['sub_category_1_value'];
        $sub_category_1_select= $_REQUEST['sub_category_1_select'];
        $sub_category_2_value = $_REQUEST['sub_category_2_value'];

        if($cat_id == ''){
            AddCategory($category_value, $category_select, $sub_category_1_value,  $sub_category_1_select, $sub_category_2_value);
        }else{
            SaveCategory($cat_id, $category_value, $category_select, $sub_category_1_value, $sub_category_1_select, $sub_category_2_value);
        }
        break;
    case 'disable':
        $cat_id	= $_REQUEST['id'];
        DisableCategory($cat_id);

        break;
    case 'get_sub_cat':
        $data['page'] = get_sub_cat($_REQUEST['category_id']);

        break;
    default:
        $error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Category Functions
 * ******************************
 */

function AddCategory($category_value, $category_select, $sub_category_1_value, $sub_category_1_select, $sub_category_2_value){
    global $db;
    $user_id = $_SESSION['USERID'];
    if($category_select == 0){
        $db->setQuery("INSERT INTO `info_category`
                                  (`user_id`, `name`, `parent_id`, `client_id`)
                            VALUES
                                  ('$user_id', '$category_value', '0', '1')");
        $db->execQuery();
    }else if($sub_category_1_select == 0){
        $db->setQuery("INSERT INTO `info_category`
                                  (`user_id`, `name`, `parent_id`, `client_id`)
                            VALUES
                                  ('$user_id', '$sub_category_1_value', '$category_select', '1')");
        
        $db->execQuery();
    }else{
        $db->setQuery("INSERT INTO `info_category`
                                  (`user_id`, `name`, `parent_id`, `client_id`)
                            VALUES
                                  ('$user_id', '$sub_category_2_value', '$sub_category_1_select', '1')");
        
        $db->execQuery();
    }

}

function SaveCategory($cat_id, $category_value, $category_select, $sub_category_1_value, $sub_category_1_select, $sub_category_2_value){
    global $db;
    $user_id	= $_SESSION['USERID'];
    if($category_value != ""){
        $db->setQuery("  UPDATE `info_category` SET
                                `user_id` = '$user_id',
                                `name` = '$category_value',
                                `parent_id`	= 0
                         WHERE  `id` = $cat_id");
        $db->execQuery();
    }else if($sub_category_1_value != ""){
        $db->setQuery("  UPDATE `info_category` SET
                                `user_id` = '$user_id',
                                `name` = '$sub_category_1_value',
                                `parent_id`	= $category_select
                         WHERE  `id` = $cat_id");
        $db->execQuery();
    }else{
        $db->setQuery("  UPDATE `info_category` SET
            	                `user_id` = '$user_id',
            				    `name` = '$sub_category_2_value',
            				    `parent_id`	= $sub_category_1_select
        				 WHERE  `id` = $cat_id");
        $db->execQuery();
    }

}

function DisableCategory($cat_id){
    global $db;
    
    $db->setQuery("UPDATE `info_category`
				      SET `actived` = 0
				   WHERE  `id` = $cat_id");
    
    $db->execQuery();
}

function CheckCategoryExist($cat_name, $par_id){
    global $db;
    $db->setQuery("SELECT `id`
				   FROM   `info_category`
				   WHERE  `name` = '$cat_name' && `parent_id` = $par_id && `actived` = 1");
    
    $res = $db->getResultArray();
    if($res[result][0]['id'] != ''){
        return true;
    }
    return false;
}
function Category($id){
    global $db;
    
    $data = '';
    $db->setQuery("SELECT `id`,
                          `name`
				   FROM   `info_category`
				   WHERE   actived = 1 AND parent_id = 0");
    
    $req = $db->getResultArray();


    $data .= '<option value="0" selected="selected">----</option>';
    
    foreach ($req[result] AS $res){
        if($res['id'] == $id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }

    return $data;
}

function get_sub_cat($id){
    global $db;
    
    $data = '';
    $db->setQuery("SELECT `id`, 
                          `name`
				   FROM   `info_category`
				   WHERE actived = 1 AND parent_id = '$id'");

    $req = $db->getResultArray();
    $data .= '<option value="0" selected="selected">----</option>';
    foreach ($req[result] AS $res){
        if($res['id'] == $id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }

    return $data;
}

function Category1($grand_parent, $parent){
    global $db;
    $data = '';
    $db->setQuery("SELECT `id`, 
                          `name`
				   FROM   `info_category`
				   WHERE  actived = 1 AND parent_id = '$grand_parent'");
    
    $req = $db->getResultArray();

    $data .= '<option value="0" selected="selected">----</option>';
    foreach ($req[result] AS $res){
        if($res['id'] == $parent){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }

    return $data;
}

function sub_category_list($parent){
    global $db;
    $data = '';
    $db->setQuery("SELECT `id`, 
                          `name`
				   FROM   `info_category`
				   WHERE   actived = 1 AND parent_id = '$parent'");

    $req = $db->getResultArray();
    $data .= '<option value="0" selected="selected">----</option>';
    foreach ($req[result] AS $res){
        if($res['id'] == $parent){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }

    return $data;
}

function GetCategory($cat_id){
    global $db;
    $db->setQuery("SELECT `id`,
                          `name`,
                          `parent_id` as parent,
                          (SELECT `parent_id` FROM `info_category` WHERE `id` = parent) as grand_parent
                    FROM  `info_category`
                    WHERE `id` = $cat_id" );
    
    $res = $db->getResultArray();

    return $res[result][0];
}

function GetPage($res = ''){
    
    $hd = '';
    $ch1 = '';
    $ch2 = '';
    $hdd1 = '';
    $hdd2 = '';
    $hdd3 = '';
    $hdd4 = '';
    $hdd5 = '';
    $hdd6 = '';
    $hdd7 = '';
    $category_value         = $res['name'];
    $sub_category_value     = $res['id'];
    $category_select        = $res['parent'];
    $sub_category_1_select  = $res['grand_parent'];
    $sub_category_1_value   = $res['name'];
    $sub_category_2_value   = $res['name'];
    $hidden_class = 'style="display:none;"';
    if($res['id'] == ''){
        $hd = $hidden_class;
    }else{
        if($category_select == 0){
            $category_select = 0;
            $sub_category_1_select = 0;
            $sub_category_1_value = '';
            $sub_category_2_value = '';
            $hdd1 = '';
            $hdd2 = $hidden_class;
            $hdd3 = $hidden_class;
            $hdd4 = $hidden_class;
            $hdd5 = $hidden_class;
            $hdd6 = $hidden_class;
            $hdd7 = $hidden_class;
            $ch1 = '';
            $ch2 = '';
        }else if($category_select != 0 && $sub_category_1_select == 0){
            $category_value = '';
            $sub_category_2_value = '';
            $hdd1 = $hidden_class;
            $hdd2 = '';
            $hdd3 = '';
            $hdd4 = $hidden_class;
            $hdd5 = $hidden_class;
            $hdd6 = $hidden_class;
            $hdd7 = '';
            $ch1 = 'checked';
            $ch2 = '';
        }else if($category_select != 0 && $sub_category_1_select != 0){
            $category_select = $res['grand_parent'];
            $sub_category_value = $res['parent'];
            $category_value = '';
            $sub_category_1_value = '';
            $hdd1 = $hidden_class;
            $hdd2 = '';
            $hdd3 = $hidden_class;
            $hdd4 = '';
            $hdd5 = '';
            $hdd6 = '';
            $hdd7 = '';
            $ch1 = 'checked disabled';
            $ch2 = 'checked';
        }

    }

    $data = '
	<div id="dialog-form">
	    <fieldset>
	    	
	    	<table class="dialog-form-table">
				<tr>
					<td style="width: 170px;"><label for="category">მთავარი კატეგორია</label></td>
					<td>
						<input '.$hdd1.' type="text" id="category_value" class="idls large" data-index="'. $res['id'] .'" value="' . $category_value . '" />
						<select '.$hd.' '.$hdd2.' id="category_select" class="idls large">' . Category($category_select)  . '</select>
					</td>
				    <td style="width: 170px;"><input '.$ch1.' style="margin-left: 15px;" type="checkbox" id="show_sub_category_1_value" /></td>
				</tr>
				<tr id="first_tr" '.$hd.' '.$hdd7.'>
					<td style="width: 170px;"><label for="sub_category_1">ქვე კატეგორია 1</label></td>
					<td>
						<input '.$hdd3.' type="text" id="sub_category_1_value" class="idls large" value="' . $sub_category_1_value . '" />
						<select '.$hdd4.' id="sub_category_1_select" class="idls large">' . Category1($category_select, $sub_category_value)  . '</select>
					</td>
				    <td style="width: 170px;"><input '.$ch2.' style="margin-left: 15px;" type="checkbox" id="show_sub_category_2_value" /></td>
				</tr>
				<tr id="second_tr" '.$hd.' '.$hdd5.'>
					<td style="width: 170px;"><label for="sub_category_2">ქვე კატეგორია 2</label></td>
					<td>
					    <input '.$hdd6.' type="text" id="sub_category_2_value" class="idls large" value="' . $sub_category_2_value . '" />
					</td>
				</tr>
			</table>
			<!-- ID -->
			<input type="hidden" id="cat_id" value="' . $res['id'] . '" />
        </fieldset>
    </div>
    ';
    return $data;
}

?>
