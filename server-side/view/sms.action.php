<?php
// require_once('../../includes/classes/core.php');
include('../../includes/classes/class.Mysqli.php');
$mysqli = new dbClass();
$action	= $_REQUEST['act'];
$error	= '';
$data	= array();
 
switch ($action) {
	case 'get_add_page':
		$page		= GetPage();
		$data		= array('page'	=> $page);

		break;
	case 'get_edit_page':
		$departmetn_id		= $_REQUEST['id'];
	       $page		= GetPage(Getdepartment($departmetn_id));
           $data		= array('page'	=> $page);

		break;
	case 'get_list' :
		$count	= $_REQUEST['count'];
		$hidden	= $_REQUEST['hidden'];
		 
		$query = "SELECT 	sms.id,
		                                sms.id,
										sms.`name`,
		                                sms.message
							    FROM 	sms
								WHERE 	sms.actived=1";
		$mysqli->setQuery($query);
		$data = $mysqli->getList($count,$hidden,1);						

		break;
	case 'get_list_log' :
	    $count	= $_REQUEST['count'];
	    $hidden	= $_REQUEST['hidden'];
	    	
	    $query = "SELECT 	`logs`.`id`,
                        				`logs`.`row_id`,
                        				`logs`.`date`,
                        				IF(`logs`.`event` = 1,'დამატება',IF(`logs`.actived = 0,'წაშლა','განახლება')) AS `act`,
                        				`user_info`.`name`,
										CASE 
												WHEN `logs`.`collumn` = 'name' then 'დასახელება'
										END AS `colum`,
                        				`logs`.`old_value`,
                        				`logs`.`new_value`
                                FROM    `logs`
                                JOIN    `users` ON `logs`.user_id = users.id
                                JOIN    `user_info` ON users.id = user_info.user_id
								WHERE   `logs`.`table` = 'sms'";
								
		$mysqli->setQuery($query);
		$data = $mysqli->getList($count,$hidden,1);	
	    
	
	    break;
	case 'save_department':
		$department_id 		= $_REQUEST['id'];
		$department_name    = $_REQUEST['name'];
		$message            = htmlspecialchars($_REQUEST['message'], ENT_QUOTES);
		
		if($department_name != ''){
            if ($department_id == '') {

            	if(!CheckdepartmentExist($department_name)) {
                    Adddepartment( $department_id, $department_name, $message);
				} else {
                    $error = "მსგავსი დასახელების შაბლონი უკვე არსებობს";
				}

            }else {
                Savedepartment($department_id, $department_name, $message);
            }
		}
		
		break;
	case 'disable':
		$department_id	= $_REQUEST['id'];
		Disabledepartment($department_id);

		break;
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Category Functions
* ******************************
*/

function Adddepartment($department_id, $department_name, $message)
{
	GLOBAL $mysqli;
	$user_id	= $_SESSION['USERID'];
	$query = "INSERT INTO 	 `sms`
								(`name`,`user_id`, `message`)
					VALUES 		('$department_name', '$user_id', '$message')";
	$mysqli->setQuery($query);
	$mysqli->execQuery();				
}

function Savedepartment($department_id, $department_name, $message)
{
	GLOBAL $mysqli;
	$user_id	= $_SESSION['USERID'];
	$query = "	UPDATE `sms`
					SET     `name` = '$department_name',
	                        `message`='$message',
							`user_id` ='$user_id'
					WHERE	`id` = $department_id";
	
	$mysqli->setQuery($query);
	$mysqli->execQuery();	
}

function Disabledepartment($department_id)
{
	GLOBAL $mysqli;
	$query = "	UPDATE `sms`
					SET    `actived` = 0
					WHERE  `id` = $department_id";

	$mysqli->setQuery($query);
	$mysqli->execQuery();				
}

function CheckdepartmentExist($department_name)
{
	GLOBAL $mysqli;
	$query = "	SELECT `id`
											FROM   `sms`
											WHERE  `name` = '$department_name' && `actived` = 1";

	$mysqli -> setQuery($query);
	$result = $mysqli -> getResultArray();

	$res = $result['result'][0];

	if($res['id'] != ''){
		return true;
	}
	return false;
}


function Getdepartment($department_id)
{
	GLOBAL $mysqli;

	$query = "	SELECT  `id`,
													`name`,
	                                                `message`
											FROM    `sms`
											WHERE   `id` = $department_id";
	$mysqli -> setQuery($query);
	$result = $mysqli -> getResultArray();

	$res = $result['result'][0];
	return $res;
}

function GetPage($res = '')
{
	$data = '
	<div id="dialog-form">
	    <fieldset>
	    	<legend>ძირითადი ინფორმაცია</legend>

	    	<table class="dialog-form-table" style="width: 100%;">
				<tr>
					<td style="width: 80px;"><label for="CallType">სახელი</label></td>
					<td>
						<input style="width: 100%;" type="text" id="name" value="'.$res[name].'">
					</td>
				</tr>
				<tr>
					<td style="width: 80px;"><label for="CallType">შინაარსი</label></td>
					<td>	
						<textarea maxlength="150" style="width:100%; resize: vertical;height: 165px;" id="content" name="call_content" cols="300" rows="10">'.$res[message].'</textarea>
					</td>
			   </tr>	
				<tr>
					<td style="width: 80px;"></td>
					<td>
						<input style="width: 50px;" type="text" id="simbol_caunt" value="0/150">
					</td>
				</tr>
			</table>
			<!-- ID -->
			<input type="hidden" id="department_id" value="' . $res['id'] . '" />
        </fieldset>
    </div>
    ';
	return $data;
}

?>
