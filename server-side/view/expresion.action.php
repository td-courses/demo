<?php
include('../../includes/classes/class.Mysqli.php');
require_once '../../includes/PHPExcel/IOFactory.php';

global $db;

$db     = new dbClass();

$action    = $_REQUEST['act'];
$error    = '';
$user = '';
$data    = array();
$imported_data = array();
$req = array();
$tmp_id = '';
// $req_file = $_FILES['excel'];
// $req_id = $_POST['data_id'];


switch ($action) {
    case 'get_add_page':
        $tmp_id = $_REQUEST['tmp_id'];
        $page        = GetModal($res, GetTmp($tmp_id));
        $data        = array('page'    => $page);

        break;
    case 'export_data':
        $req = array("id" => $_POST['id']);

        exportData($req['id']);

        break;
    case 'import_data':
        $req = array("file" => $_FILES['excel'], "id" => $_POST['data_id'], "tmp_id" => $_POST['tmp_id']);

        $imported_data = $db->getExcelImport($req['file']);

        importData($imported_data, $req['id'], $req['tmp_id']);


        break;
    case 'get_edit_page':
        $selected_id = $_REQUEST['id'];
        $page            = GetModal(GetTable($selected_id), GetTmp($tmp_id));
        $data            = array('page' => $page);

        break;
    case 'getExpresions':
        $count    = $_REQUEST['count'];
        $hidden    = $_REQUEST['hidden'];


        $db->setQuery(" SELECT 	users.id,
                                    users.`username`,
                                    user_info.`name`
                            FROM 	users 
                            INNER JOIN user_info ON users.id = user_info.user_id
                            WHERE 	users.actived = 1");

        $data = $db->getList($count, $hidden, 1);
        break;
    case 'users_import':
        $count    = $_REQUEST['count'];
        $hidden    = $_REQUEST['hidden'];
        $data_id = $_REQUEST['data_id'];
        $tmp_id = $_REQUEST['tmp_id'];

        if ($data_id == '' && $tmp_id !== '') {
            $db->setQuery("SELECT 	`id` as iid,
            `name`,
            `currentJob`,
            `price`
        FROM 	users_import
        WHERE tmp = '$tmp_id'");

            $data = $db->getList($count, $hidden, 1);
        } else {
            $db->setQuery(" SELECT 	`id` as iid,
            `name`,
            `currentJob`,
            `price`
    FROM 	users_import
    WHERE data_id = '$data_id'");

            $data = $db->getList($count, $hidden, 1);
        }
        break;
    case 'add_user':
        $nickname = $_REQUEST['nickname'];
        $name = $_REQUEST['name'];
        $tmp_id = $_REQUEST['tmp_id'];

        add_nickname($nickname);

        $user_id = get_user($nickname);
        $user =   $user_id;
        add_name($name, $user_id);

        update_imports($user_id, $tmp_id);

        break;
    default:
        $error = 'Action is Null';
}

$data['error'] = $error;
$data['id'] = $req['id'];
$data['file'] = $req['file'];
// $data['user'] = $user;
$data['tmp_id'] = $tmp_id;

echo json_encode($data);

function exportData($id)
{
    global $db;

    $query = "SELECT 
                *
                FROM 
                users_import
                WHERE data_id = '$id'";
    $db->setQuery($query);

    $db->getExcelExport();
}

function add_nickname($nickname)
{
    global $db;

    $db->setQuery("INSERT INTO 	`users`
                                   (`username`, `actived`)
                             VALUES 		
                                   ('$nickname', '1')");
    $db->execQuery();
}

function add_name($name, $user_id)
{

    global $db;

    $db->setQuery("INSERT INTO 	`user_info`
            (`user_id`, `name`)
      VALUES 		
            ('$user_id','$name')");
    $db->execQuery();
}
function update_imports($user_id, $tmp_id)
{
    global $db;

    $db->setQuery("	UPDATE `users_import`
					   SET `data_id` = '$user_id'
					WHERE  `tmp`      = '$tmp_id'");

    $db->execQuery();
}

function get_user($nickname)
{
    global $db;

    $db->setQuery(" SELECT id, username
    FROM 	users
    WHERE username = '$nickname'
    ");

    $data =  $db->getResultArray();

    $id = $data[result][0][id];
    return $id;
}


function GetTable($selected_id)
{
    $res = $selected_id;

    return $res;
}

function GetTmp($tmp_id)
{
    $tmp = $tmp_id;

    return $tmp;
}

function importData($imported_data, $id, $tmp_id)
{
    global $db;

    if ($id == '') {
        foreach ($imported_data as $row) {

            $db->setQuery(" INSERT INTO users_import
                             (`name`, `currentJob`, `price`, `tmp`)
                            VALUES ('$row[A]', '$row[B]', '$row[C]', '$tmp_id')
                            ");
            $db->execQuery();
        }
    } else {
        foreach ($imported_data as $row) {

            $db->setQuery(" INSERT INTO users_import
                             (`name`, `currentJob`, `price`, `data_id`)
                            VALUES ('$row[A]', '$row[B]', '$row[C]', '$id')
                            ");
            $db->execQuery();
        }
    }
}


function GetModal($res = '', $tmp_id)
{
    if ($res == '') {
        $data .= '
        <fieldset style="text-align: center; background-color: #e6F2F8; border:1px solid #A3D0E4; border-bottom: none">
        <label for="nickname">მეტსახელი</label>
        <input type="text" name="nickname" id="nickname">
        <label for="name">სახელი/გვარი</label>
        <input type="text" name="name" id="name">
        </fieldset>
        ';
    }


    $data .= '
    
    <input type="hidden" id="data_id" value="' . $res . '" />
    <table class="display" id="usersImport">
        <thead >
            <tr id="datatable_header">
                <th>ID</th>
                <th style="width: 50%;">მეტსახელი</th>
                <th style="width: 50%;">სახელი</th>
                <th>ნომერი</th>
            </tr>
        </thead>
        <thead>
            <tr class="search_header">
                <th class="colum_hidden">

                </th>
                <th>
                    <input type="text" name="search_category"  class="search_init" />
                </th>
                <th>
                    <input type="text" name="search_category"  class="search_init" />
                </th>
                <th>
                <input type="text" name="search_category"  class="search_init" />

                </th>
            </tr>
        </thead>
    </table>
    <div>
        <form action="server-side/view/expresion.action.php?act=export_data" method="POST" style="float: left;">
            <button id="export_button">ექსპორტი</button>
            <input type="hidden" name="id" value="' . $res . '" />
        </form>

        <form id="ExcelUploadForm" action="server-side/view/expresion.action.php?act=import_data" method="POST" enctype="multipart/form-data" style="float: right">
            <input type="file" name="excel" id="ExcelFile" style="display: none">
            <input type="hidden" name="data_id" value="' . $res . '" />
            <input type="hidden" name="tmp_id" value="' . $tmp_id . '" />
            <label for="ExcelFile" id="ExcelImport">იმპორტი</label>
            <button  name="import_button" id="import_button" style="display: none;">იმპორტი</button>
        </form>
    
            <div class="percent">0%</div>
        <div id="uploadStatus"></div>
    </div>

';

    return $data;
}
