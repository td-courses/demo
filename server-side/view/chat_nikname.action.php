<?php
require_once('../../includes/classes/class.Mysqli.php');
global $db;

$db     = new dbClass();
$action	= $_REQUEST['act'];
$error	= '';
$data	= array();

switch ($action) {
	case 'get_add_page':
		$page		= GetPage();
		$data		= array('page'	=> $page);

		break;
	case 'get_edit_page':
		$problem_id		= $_REQUEST['id'];
		$page		= GetPage(Getproblem($problem_id));
		$data		= array('page'	=> $page);

		break;
	case 'get_list' :
		$count	= $_REQUEST['count'];
		$hidden	= $_REQUEST['hidden'];
			
		$db->setQuery(" SELECT 	chat_nikname.id,
                                user_info.`name`,
								chat_nikname.`name`
					    FROM 	chat_nikname
                        JOIN    user_info ON user_info.user_id = chat_nikname.crystal_users_id
					    WHERE 	chat_nikname.actived=1");
		
		$data = $db->getList($count,$hidden,1);

		break;
	case 'save_problem':
		$id 	= $_REQUEST['id'];
		$name   = $_REQUEST['name'];
        $crystal_users_id = $_REQUEST['crystal_users_id'];


		if($name != ''){
		    if(!CheckproblemExist($name, $id)){
				if ($id == '') {
					Addproblem( $id, $name, $crystal_users_id);
				}else {
					Saveproblem($id, $name, $crystal_users_id);
				}

			} else {
				$error = '"' . $name . '" უკვე არის სიაში!';

			}
		}

		break;
	case 'disable':
		$problem_id	= $_REQUEST['id'];
		Disableproblem($problem_id);

		break;
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Category Functions
* ******************************
*/

function Addproblem($id, $name, $crystal_users_id){
	global $db;
	$db->setQuery("INSERT INTO 	`chat_nikname`
							   (`crystal_users_id`,`name`)
					    VALUES 		
                               ('$crystal_users_id','$name')");
	$db->execQuery();
}

function Saveproblem($id, $name, $crystal_users_id){
    global $db;
    $db->setQuery("	UPDATE `chat_nikname`
					SET     `crystal_users_id`='$crystal_users_id',
							`name` = '$name'
					WHERE	`id` = $id");
    $db->execQuery();
}

function Disableproblem($id){
    global $db;
    $db->setQuery("	UPDATE `chat_nikname`
					SET    `actived` = 0
					WHERE  `id` = $id");
    $db->execQuery();
}

function CheckproblemExist($name){
    global $db;
    $db->setQuery("	SELECT `id`
					FROM   `chat_nikname`
					WHERE  `name` = '$name' && `actived` = 1");
    
    $res = $db->getResultArray();
    
	if($res[result][0]['id'] != ''){
		return true;
	}
	return false;
}


function Getproblem($id){
    global $db;
    $db->setQuery("	SELECT  `id`,
                            `crystal_users_id`,
							`name`
					FROM    `chat_nikname`
					WHERE   `id` = $id" );
    
    $res = $db->getResultArray();

	return $res[result][0];
}

function GetCry($id){
    global $db;
    
    $data = '';
    $db->setQuery("SELECT users.id,
                          user_info.`name`
                   FROM   users
                   JOIN   user_info ON users.id = user_info.user_id
                   WHERE  users.actived = 1");
    
    $res = $db->getResultArray();

    $data .= '<option value="0" selected="selected">----</option>';
    foreach ($res[result] AS $res){
        if($res['id'] == $id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }

    return $data;
}

function GetPage($res = ''){
    
	$data = '
        	<div id="dialog-form">
        	    <fieldset>
        	    	<legend>ძირითადი ინფორმაცია</legend>
        
        	    	<table class="dialog-form-table">
        	           <tr>
        					<td style="width: 170px;"><label for="CallType">ოპერატორის სახელი</label></td>
        					<td>
        						<select id="crystal_users_id">'.GetCry($res[crystal_users_id]).'</select>
        					</td>
        				</tr>
        				<tr>
        					<td style="width: 170px;"><label for="CallType">ზედმეტსახელი</label></td>
        					<td>
        						<input type="text" id="name" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="' . $res['name'] . '" />
        					</td>
        				</tr>
        
        			</table>
        			<!-- ID -->
        			<input type="hidden" id="id" value="' . $res['id'] . '" />
                </fieldset>
            </div>';
	return $data;
}

?>
