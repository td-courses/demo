<?php
// require_once('../../includes/classes/core.php');
include('../../includes/classes/class.Mysqli.php');
$mysqli = new dbClass();
$action	= $_REQUEST['act'];
$error	= '';
$data	= array();

switch ($action) {
	case 'get_add_page':
		$page		= GetPage();
		$data		= array('page'	=> $page);

		break;
	case 'get_edit_page':
		$id	 = $_REQUEST['id'];
		$page		 = GetPage(Get_work($id));
		$data		 = array('page'	=> $page);

		break;
	
	case 'get_list_free_day' :
	    $count	= $_REQUEST['count'];
	    $hidden	= $_REQUEST['hidden'];
	    
	    $query = "  SELECT work_week_day_graphic.id,
            			   week_day.`name`,
            			   CONCAT(work_week_day_graphic.start_time,' - ',work_week_day_graphic.end_time)
                    FROM   work_week_day_graphic
                    JOIN   week_day ON week_day.id = work_week_day_graphic.week_day_id
                    WHERE  work_week_day_graphic.actived = 1";
	    
	    $mysqli->setQuery($query);
		$data = $mysqli->getList($count,$hidden,1);
	    break;
	case 'get_list_week_day' :
	    $count	= $_REQUEST['count'];
	    $hidden	= $_REQUEST['hidden'];
	    
	    $query = "  SELECT 	`holidays`.`id`,
            				 CONCAT(IF(holidays.year=0,'',CONCAT(holidays.`year`,'-')), IF(`holidays`.`month`<10,concat('0',`holidays`.`month`),`holidays`.`month`), '-', IF(`holidays`.`date`<10,concat('0',`holidays`.`date`),`holidays`.`date`)),
            				`holidays`.`name`,
            				`holidays_category`.`name`
                    FROM    `holidays`
                    JOIN    `holidays_category` ON `holidays`.holidays_category_id = `holidays_category`.`id`
                    WHERE   `holidays`.`actived` = 1";
	    
		$mysqli->setQuery($query);
		$data = $mysqli->getList($count,$hidden,1);
	    
	    break;
	case 'disable':
		$comment_id	= $_REQUEST['id'];
		$source     = $_REQUEST['source'];
		Disablecomment($comment_id,$source);

		break;
	case 'save_work':
	    $id	         = $_REQUEST['id'];
	    $week_day_id = $_REQUEST['week_day_id'];
	    $start	     = $_REQUEST['start'];
	    $end	     = $_REQUEST['end'];
	    $user	     = $_SESSION['USERID'];
	    
	    if ($id=='') {
	        $mysqli->setQuery("INSERT INTO `work_week_day_graphic`
                                          (`user_id`, `week_day_id`, `start_time`, `end_time`, `actived`) 
                                    VALUES 
                                          ('$user', '$week_day_id', '$start', '$end', 1)");
	        $mysqli->execQuery();
	    }else{
	        $mysqli->setQuery("UPDATE `work_week_day_graphic` 
                                  SET `user_id`     = '$user',
                                      `week_day_id` = '$week_day_id',
                                      `start_time`  = '$start',
                                      `end_time`    = '$end'
                               WHERE  `id`          = '$id'");
	        $mysqli->execQuery();
	    }
	    break;
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Category Functions
* ******************************
*/
function Disablecomment($id,$source){
    global $mysqli;
    if ($source==1){
		$query = "UPDATE work_week_day_graphic SET actived = 0 WHERE id = $id";
		$mysqli -> setQuery($query);
	    $mysqli -> execQuery(); 
    }elseif ($source==2){
		$query = "UPDATE holidays SET actived = 0 WHERE id = $id";
		$mysqli -> setQuery($query);
	    $mysqli -> execQuery(); 
    }
	
}

function get_week_day($id) {
    global $mysqli;
    
    $mysqli->setQuery("SELECT id, name FROM week_day");
    
    $data = $mysqli->getSelect($id);
    
    return $data;
    
}

function Get_work($id){
    global $mysqli;
    $mysqli->setQuery("SELECT work_week_day_graphic.id,
                			  work_week_day_graphic.`week_day_id`,
                			  work_week_day_graphic.start_time,
                			  work_week_day_graphic.end_time
                        FROM  work_week_day_graphic
                        WHERE work_week_day_graphic.actived = 1 AND id = '$id'");
    
    $res = $mysqli->getResultArray();
    
    return $res[result][0];
}

function GetPage($res = ''){
    $data = '<div id="dialog-form">
        	    <fieldset>
        	    	<legend>ძირითადი ინფორმაცია</legend>
                
        	    	<table class="dialog-form-table-holidays">
        					<input type="hidden" id="holidays_hidden_id" value="'.$res["id"].'">
        				<tr>
                            <td class="pad-bottom" colspan="2">
                                <label for="holidayCategory">დღე</label>
                                <select style="width: 228px;" name="week_day" id="week_day_id" data-select="jquery-ui-select">'.get_week_day($res["week_day_id"]).'</select>
                            </td>
        				</tr>
                        <tr style="height:10px;"></tr>
                        <tr>
                            <td style="width: 120px;" class="children-full-width pad-bottom">
                                <label for="holidayName">დასაწყისი</label>
                                <input style="width: 100px;" type="text" name="start" id="start" value="'.$res["start_time"].'">
                            </td>
                            <td class="children-full-width pad-bottom">
                                <label for="holidayName">დასასრული</label>
                                <input style="width: 100px;" type="text" name="end" id="end" value="'.$res["end_time"].'">
                            </td>
                        </tr>
        			</table>
        			<!-- ID -->
        			<input type="hidden" id="hidde_id" value="' . $res['id'] . '" />
                </fieldset>
            </div>';
    return $data;
}
?>
