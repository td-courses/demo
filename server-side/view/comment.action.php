<?php
require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db     = new dbClass();

$action	= $_REQUEST['act'];
$check	= $_REQUEST['check'];
$error	= '';
$data	= array();

switch ($action) {
	case 'get_add_page':
	    if ($check == 1) {
	        $page		= GetPage();
	        $data		= array('page'	=> $page);
	    }else {
	        $page		= GetPage1();
	        $data		= array('page'	=> $page);
	    }
		

		break;
	case 'get_edit_page':
		$comment_id	 = $_REQUEST['id'];
		if ($check == 1) {
		    $page		 = GetPage(Getcomment($comment_id));
		    $data		 = array('page'	=> $page);
		}else {
		    $page		 = GetPage1(Getcomment1($comment_id));
		    $data		 = array('page'	=> $page);
		}
		

		break;
	case 'get_list' :
		$count	= $_REQUEST['count'];
		$hidden	= $_REQUEST['hidden'];
			
		$db->setQuery("SELECT 	comment.id,
							   (SELECT GROUP_CONCAT(my_web_site.`name`) 
                				FROM comment_site 
                				JOIN my_web_site ON my_web_site.id = comment_site.site_id
                				WHERE comment_id = comment.id),
                			   (SELECT GROUP_CONCAT(`name`) 
                				FROM   comment_source 
                				JOIN   source ON source.id = comment_source.source_id
                                WHERE  comment_id = comment.id),
                                comment.`comment`
				       FROM 	comment
				       WHERE 	comment.actived=1");
		
		$data = $db->getList($count,$hidden,1);

		break;
	case 'get_list1' :
	    $count	= $_REQUEST['count'];
	    $hidden	= $_REQUEST['hidden'];
	    	
	    $db->setQuery("SELECT 	comment_chat.id,
                               (SELECT GROUP_CONCAT(my_web_site.`name`) 
                				FROM comment_chat_site 
                				JOIN my_web_site ON my_web_site.id = comment_chat_site.site_id
                				WHERE comment_id = comment_chat.id),
                			   (SELECT GROUP_CONCAT(`name`) 
                				FROM   comment_chat_source 
                				JOIN   source ON source.id = comment_chat_source.source_id
                                WHERE  comment_id = comment_chat.id),
								comment_chat.`comment`
					   FROM 	comment_chat
					   WHERE 	comment_chat.actived=1");
	    
	    $data = $db->getList($count,$hidden,1);
	
	    break;
	case 'save_priority':
		$comment_id = $_REQUEST['id'];
		$comment      = htmlspecialchars($_REQUEST['comment'], ENT_QUOTES);
		if ($comment_id == '') {
			Addcomment( $comment_id, $comment);
		}else {
			Savecomment($comment_id, $comment);
		}				
			
		break;
	case 'save_priority1':
	    $comment_id = $_REQUEST['id'];
	    $comment      = htmlspecialchars($_REQUEST['comment'], ENT_QUOTES);
	    if ($comment_id == '') {
            Addcomment1( $comment_id, $comment);
        }else {
            Savecomment1($comment_id, $comment);
        }
        
	    break;
	case 'disable':
		$comment_id	= $_REQUEST['id'];
		Disablecomment($comment_id);

		break;
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Category Functions
* ******************************
*/

function Addcomment($comment_id, $comment){
    global $db;
	$user_id	= $_SESSION['USERID'];
	
	$site	    = $_REQUEST['site'];
	$source_id	= $_REQUEST['source_id'];
	
	$db->setQuery("INSERT INTO  `comment`
							  (`user_id`,`comment`)
					VALUES 	  
							  ('$user_id','$comment')");
	
	$db->execQuery();
	
	$comment_id = $db->getLastId();
	
	
	$site_array = explode(",",$site);
	$db->setQuery("DELETE FROM comment_site WHERE comment_id = '$comment_id'");
	$db->execQuery();
	
	foreach($site_array AS $site_id){
	    $db->setQuery("INSERT INTO comment_site (comment_id, site_id) values ('$comment_id', $site_id)");
	    $db->execQuery();
	}
	
	$source_array = explode(",",$source_id);
	$db->setQuery("DELETE FROM comment_source WHERE comment_id = '$comment_id'");
	$db->execQuery();
	
	foreach($source_array AS $source_id){
	    $db->setQuery("INSERT INTO comment_source (comment_id, source_id) values ('$comment_id', $source_id)");
	    $db->execQuery();
	}

}

function Savecomment($comment_id, $comment){
    global $db;
	$user_id	= $_SESSION['USERID'];
	
	$site	    = $_REQUEST['site'];
	$source_id	= $_REQUEST['source_id'];
	
	$db->setQuery("	UPDATE `comment`
					   SET `user_id` ='$user_id',
						   `comment` ='$comment'
					WHERE  `id`      = $comment_id");
	$db->execQuery();
	
	$site_array = explode(",",$site);
	$db->setQuery("DELETE FROM comment_site WHERE comment_id = '$comment_id'");
	$db->execQuery();
	
	foreach($site_array AS $site_id){
	    $db->setQuery("INSERT INTO comment_site (comment_id, site_id) values ('$comment_id', $site_id)");
	    $db->execQuery();
	}
	
	$source_array = explode(",",$source_id);
	$db->setQuery("DELETE FROM comment_source WHERE comment_id = '$comment_id'");
	$db->execQuery();
	
	foreach($source_array AS $source_id){
	    $db->setQuery("INSERT INTO comment_source (comment_id, source_id) values ('$comment_id', $source_id)");
	    $db->execQuery();
	}
}

function Addcomment1($comment_id, $comment){
    global $db;
    $user_id	= $_SESSION['USERID'];
    
    $site	    = $_REQUEST['site'];
    $source_id	= $_REQUEST['source_id'];
    
    $db->setQuery("INSERT INTO  `comment_chat`
                               (`user_id`,`comment`)
                        VALUES
                               ('$user_id','$comment')");
    $db->execQuery();
    
    $comment_id = $db->getLastId();
    
    
    $site_array = explode(",",$site);
    $db->setQuery("DELETE FROM comment_chat_site WHERE comment_id = '$comment_id'");
    $db->execQuery();
    
    foreach($site_array AS $site_id){
        $db->setQuery("INSERT INTO comment_chat_site (comment_id, site_id) values ('$comment_id', $site_id)");
        $db->execQuery();
    }
    
    $source_array = explode(",",$source_id);
    $db->setQuery("DELETE FROM comment_chat_source WHERE comment_id = '$comment_id'");
    $db->execQuery();
    
    foreach($source_array AS $source_id){
        $db->setQuery("INSERT INTO comment_chat_source (comment_id, source_id) values ('$comment_id', $source_id)");
        $db->execQuery();
    }
}

function Savecomment1($comment_id, $comment){
    global $db;
    $user_id	= $_SESSION['USERID'];
    $site	    = $_REQUEST['site'];
    $source_id	= $_REQUEST['source_id'];
    
    $db->setQuery("	UPDATE `comment_chat`
                       SET `user_id` ='$user_id',
                           `comment` ='$comment'
                    WHERE  `id`      = $comment_id");
    $db->execQuery();
    
    $site_array = explode(",",$site);
    $db->setQuery("DELETE FROM comment_chat_site WHERE comment_id = '$comment_id'");
    $db->execQuery();
    
    foreach($site_array AS $site_id){
        $db->setQuery("INSERT INTO comment_chat_site (comment_id, site_id) values ('$comment_id', $site_id)");
        $db->execQuery();
    }
    
    $source_array = explode(",",$source_id);
    $db->setQuery("DELETE FROM comment_chat_source WHERE comment_id = '$comment_id'");
    $db->execQuery();
    
    foreach($source_array AS $source_id){
        $db->setQuery("INSERT INTO comment_chat_source (comment_id, source_id) values ('$comment_id', $source_id)");
        $db->execQuery();
    }
}

function Disablecomment($comment_id){
    global $db;
    if($_REQUEST[check]==1){
        $db->setQuery("	UPDATE `comment`
                           SET `actived` = 0
                        WHERE  `id` = $comment_id");
    }else{
        $db->setQuery("	UPDATE `comment_chat`
                           SET `actived` = 0
                        WHERE  `id` = $comment_id");
    }
    
    $db->execQuery();
}

function CheckcommentExist($comment){
    global $db;
    $db->setQuery("	SELECT `id`
					FROM   `comment`
					WHERE  `comment` = '$comment' && `actived` = 1");
    
    $res = $db->getResultArray();
    
	if($res[result][0]['id'] != ''){
		return true;
	}
	return false;
}

function web_site($id){
    global $db;
    $data = '';
    
    $db->setQuery("SELECT `id`, `name`
				   FROM   `my_web_site`
				   WHERE   actived = 1 ");
    
    $res = $db->getResultArray();
    foreach ($res[result] AS $value){
        $db->setQuery("SELECT id
                       FROM  `comment_site`
                       WHERE  comment_site.comment_id = '$id' AND site_id = '$value[id]'");
        $check = $db->getNumRow();
        
        if($check>0){
            $data .= '<option value="' . $value['id'] . '" selected="selected">' . $value['name'] . '</option>';
        } else {
            $data .= '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
        }
        
    }
    
    return $data;
}

function get_source($id){
    global $db;
    $data = '';
    
    $db->setQuery("SELECT `id`,
                          `name`
				   FROM   `source`
				   WHERE   actived = 1 AND id = 1");
    
    $res = $db->getResultArray();
    foreach ($res[result] AS $value){
        $db->setQuery("SELECT id
                       FROM  `comment_source`
                       WHERE  comment_source.comment_id = '$id' AND source_id = '$value[id]'");
        $check = $db->getNumRow();
        
        if($check>0){
            $data .= '<option value="' . $value['id'] . '" selected="selected">' . $value['name'] . '</option>';
        } else {
            $data .= '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
        }
        
    }
    
    return $data;
}

function web_site1($id){
    global $db;
    $data = '';
    
    $db->setQuery("SELECT `id`, `name`
				   FROM   `my_web_site`
				   WHERE   actived = 1");
    
    $res = $db->getResultArray();
    foreach ($res[result] AS $value){
        $db->setQuery("SELECT id
                       FROM  `comment_chat_site`
                       WHERE  comment_chat_site.comment_id = '$id' AND site_id = '$value[id]'");
        $check = $db->getNumRow();
        
        if($check>0){
            $data .= '<option value="' . $value['id'] . '" selected="selected">' . $value['name'] . '</option>';
        } else {
            $data .= '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
        }
        
    }
    
    return $data;
}

function get_source1($id){
    global $db;
    $data = '';
    
    $db->setQuery("SELECT `id`,
                          `name`
				   FROM   `source`
				   WHERE   actived = 1 AND id!=1");
    
    $res = $db->getResultArray();
    foreach ($res[result] AS $value){
        $db->setQuery(" SELECT id
                        FROM  `comment_chat_source`
                        WHERE  comment_chat_source.comment_id = '$id' AND source_id = '$value[id]'");
        $check = $db->getNumRow();
        
        if($check>0){
            $data .= '<option value="' . $value['id'] . '" selected="selected">' . $value['name'] . '</option>';
        } else {
            $data .= '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
        }
        
    }
    
    return $data;
}

function Getcomment($comment_id){
    global $db;
    $db->setQuery("	SELECT  `id`,
							`comment`
					FROM    `comment`
					WHERE   `id` = $comment_id" );
    
    $res = $db->getResultArray();

    return $res[result][0];
}

function GetPage($res = ''){
	$data = '
	<div id="dialog-form">
	    <fieldset>
	    	<legend>ძირითადი ინფორმაცია</legend>

	    	<table class="dialog-form-table">
				<tr>
					<td style="width: 170px;"><label for="CallType">საიტი</label></td>
				</tr>
				<tr>
					<td>
						<select style="height: 18px; width: 555px;" id="my_site" class="idls object" multiple>'.web_site($res['id']).'</select>
					</td>
				</tr>
                <tr style="height:10px;"></tr>
				<tr>
					<td style="width: 170px;"><label for="CallType">წყარო</label></td>
				</tr>
                <tr>
					<td>
						<select style="height: 18px; width: 555px;" id="source_id" class="idls object" multiple>'.get_source($res['id']).'</select>
					</td>
				</tr>
				<tr style="height:10px;"></tr>
				<tr>
					
					<td style="width: 170px;"><label for="CallType">კომენტარის შინაარში</label></td>
				</tr>
				<tr>
					
					<td colspan="6">
						<div id="content">	
						  <textarea  style="width: 550px; resize: vertical; height:100px" id="comment" class="idle" name="call_content" cols="300" rows="8">' . $res['comment'] . '</textarea>
						</div>		
					</td>
					
				</tr>

			</table>
			<!-- ID -->
			<input type="hidden" id="comment_id" value="' . $res['id'] . '" />
        </fieldset>
	

    </div>';
	
	return $data;
}


function Getcomment1($comment_id){
    global $db;
    $db->setQuery("	SELECT  `id`,
                            `comment`
                    FROM    `comment_chat`
                    WHERE   `id` = $comment_id" );
    
    $res = $db->getResultArray();
    
    return $res[result][0];
}

function GetPage1($res = ''){
    $data = '
	<div id="dialog-form">
	    <fieldset>
	    	<legend>ძირითადი ინფორმაცია</legend>
        
	    	<table class="dialog-form-table">
				<tr>
					<td style="width: 170px;"><label for="CallType">საიტი</label></td>
				</tr>
				<tr>
					<td>
						<select style="height: 18px; width: 555px;" id="my_site1" class="idls object" multiple>'.web_site1($res['id']).'</select>
					</td>
				</tr>
                <tr style="height:10px;"></tr>
				<tr>
					<td style="width: 170px;"><label for="CallType">წყარო</label></td>
				</tr>
                <tr>
					<td>
						<select style="height: 18px; width: 555px;" id="source_id1" class="idls object" multiple>'.get_source1($res['id']).'</select>
					</td>
				</tr>
				<tr style="height:10px;"></tr>
				<tr>
						    
					<td style="width: 170px;"><label for="CallType">საუბრის შინაარში</label></td>
				</tr>
				<tr>
						    
					<td colspan="6">
						<div id="content">
						  <textarea  style="width: 550px; resize: vertical; height:100px" id="comment1" class="idle" name="call_content" cols="300" rows="8">' . $res['comment'] . '</textarea>
						</div>
					</td>
						      
				</tr>
						      
			</table>
			<!-- ID -->
			<input type="hidden" id="comment_id1" value="' . $res['id'] . '" />
        </fieldset>
			    
			    
    </div>';
    
    return $data;
}

?>
