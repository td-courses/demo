<?php
// require_once('../../includes/classes/core.php');
include('../../includes/classes/class.Mysqli.php');
$mysqli = new dbClass();
$action	= $_REQUEST['act'];
$error	= '';
$data	= array();

switch ($action) {
	case 'get_add_page':
		$page	= GetPage();
		$data	= array('page'	=> $page);

		break;
	case 'get_edit_page':
		$id	    = $_REQUEST['id'];
		$page	= GetPage(Getfree_work($id));
		$data	= array('page'	=> $page);

		break;
		
	case 'save_free_work':
	    $id	             = $_REQUEST['free_work_id'];
	    $holidayCategory = $_REQUEST['holidayCategory'];
	    $connect	     = $_REQUEST['connect'];
	    $holidaYear	     = $_REQUEST['holidaYear'];
	    $holidayMonth	 = $_REQUEST['holidayMonth'];
	    $holidayDate	 = $_REQUEST['holidayDate'];
	    $holidayName	 = $_REQUEST['holidayName'];
	    $user	         = $_SESSION['USERID'];
	    
	    if ($id=='') {
	        $mysqli->setQuery("INSERT INTO `holidays`
                                          (`holidays_category_id`, `creeping`, `user_id`, `year`, `month`, `date`, `name`, `actived`) 
                                    VALUES 
                                          ('$holidayCategory', '$connect', '$user', '$holidaYear', '$holidayMonth', '$holidayDate', '$holidayName', 1);");
	        $mysqli->execQuery();
	    }else{
    	    $mysqli->setQuery("UPDATE `holidays` 
                                  SET `holidays_category_id` = '$holidayCategory',
                                      `creeping`             = '$connect',
                                      `user_id`              = '$user',
                                      `year`                 = '$holidaYear',
                                      `month`                = '$holidayMonth',
                                      `date`                 = '$holidayDate',
                                      `name`                 = '$holidayName'
                               WHERE  `id`                   = '$id'");
    	    $mysqli->execQuery();
	    }
	    break;
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Category Functions
* ******************************
*/
function get_holiday_category_list($id) {
    global $mysqli;
    
    $mysqli->setQuery("SELECT id, name FROM holidays_category");
    
    $data = $mysqli->getSelect($id);
    
    return $data;
    
}

// returns simple year list
function get_simple_years_list($selected_year) {
    
    $list = "<option value='0'>-------------</option>";
    
    for($i = 17; $i <= 50; $i++) {
        
        $year = "20".$i;
        
        if($selected_year == $year) {
            $list .= "<option value='".$year."' selected>".$year."</option>";
        } else {
            $list .= "<option value='".$year."'>".$year."</option>";
        }
        
    }
    
    return $list;
    
}

// returns simple month list
function get_simple_month_list($selected_month) {
    
    $list = "";
    $month_array = array("0","იანვარი", "თებერვალი", "მარტი", "აპრილი", "მაისი", "ივნისი", "ივლისი", "აგვისტო", "სექტემბერი", "ოქტომბერი", "ნოემბერი", "დეკემბერი");
    
    for($i = 1; $i <= 13; $i++) {
        
        if($selected_month == $i) {
            $list .= "<option value='".$i."' selected>".$month_array[$i]."</option>";
        } else {
            $list .= "<option value='".$i."'>".$month_array[$i]."</option>";
        }
        
    }
    
    return $list;
    
}

// returns simple day list
function get_simple_day_list($selected_day) {
    
    $list = "";
    $day = "";
    
    for($i = 1; $i <= 31; $i++) {
        
        if($i < 10) {
            $day = "0".$i;
        } else {
            $day = $i;
        }
        
        if($selected_day == $i) {
            $list .= "<option value='".$day."' selected>".$day."</option>";
        } else {
            $list .= "<option value='".$day."'>".$day."</option>";
        }
        
    }
    
    return $list;
    
}

function Getfree_work($id){
    global $mysqli;
    $mysqli->setQuery("SELECT 	  holidays.id,
    							  holidays.creeping,
    							  holidays.year,
    							  holidays.month,
    							  holidays.date,
                                  holidays.name,
    							  holidays_category.name AS holiday_category,
    							  holidays.holidays_category_id AS category
    				    FROM 	  holidays
    				    LEFT JOIN holidays_category ON holidays_category.id = holidays.holidays_category_id
                        WHERE 	  holidays.actived=1 AND holidays.id = '$id'");
    
    $res = $mysqli->getResultArray();
    
    return $res[result][0];
}

function GetPage($res = ''){
    $data = '<div id="dialog-form">
	    <fieldset>
	    	<legend>ძირითადი ინფორმაცია</legend>

	    	<table class="dialog-form-table-holidays">
					<input type="hidden" id="holidays_hidden_id" value="'.$res["id"].'">
				<tr>
                    <td class="pad-bottom" colspan="2">
                        <label for="holidayCategory">კატეგორია</label>
                        <select name="holiday_category" id="holidayCategory" data-select="jquery-ui-select">'.get_holiday_category_list($res["category"]).'</select>
                    </td>
										<td>
                        <label for="holidayCreeping">მცოცავი</label>
                        <input type="checkbox" name="holiday_creeping" id="holidayCreeping" value="1" '.($res["creeping"] ? 'checked' : '').'/>
                    </td>
                </tr>
                <tr>
                    <td class="children-full-width pad-bottom" style="width: 33%; padding-right: 10px;">
                        <label for="holidaYear">წელი</label>
                        <select style="width: 100px;" name="holiday_year" id="holidaYear" data-select="jquery-ui-select" '.($res["creeping"] ? '' : 'disabled').'>
                            '.get_simple_years_list($res["year"]).'
                        </select>
                    </td>
                    <td class="children-full-width pad-bottom" style="width: 33%; padding-right: 10px;">
                        <label for="holidayMonth">თვე</label>
                        <select name="holiday_month" id="holidayMonth" data-select="jquery-ui-select">
                            '.get_simple_month_list($res["month"]).'
                        </select>
                    </td>
                    <td class="children-full-width pad-bottom" style="width: 33%;">
                        <label for="holidayDate">რიცხვი</label>
                        <select style="width: 65px;" name="holiday_date" id="holidayDate" data-select="jquery-ui-select">
                            '.get_simple_day_list($res["date"]).'
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="children-full-width pad-bottom" style="width: 100%;" colspan="3">
                        <label for="holidayName">დასახელება</label>
                        <input type="text" name="holiday_name" id="holidayName" value="'.$res["name"].'">
                    </td>
                </tr>
			</table>
			<!-- ID -->
			<input type="hidden" id="free_work_id" value="' . $res['id'] . '" />
        </fieldset>
    </div>';
    return $data;
}

?>
