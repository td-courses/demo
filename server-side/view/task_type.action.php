<?php
require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db         = new dbClass();
$action		= $_REQUEST['act'];
$error		= '';
$data		= array();
$user_id	= $_SESSION['USERID'];

switch ($action) {
	case 'get_add_page':
		$page		= GetPage();
		$data		= array('page'	=> $page);

		break;
	case 'get_edit_page':
		$task_id		= $_REQUEST['id'];
		$page		= GetPage(Gettask_type($task_id));
		$data		= array('page'	=> $page);

		break;
	case 'get_list' :
		$count	= $_REQUEST['count'];
		$hidden	= $_REQUEST['hidden'];
			
		$db->setQuery(" SELECT 	task_type.id,
								task_type.`name`
					    FROM 	task_type
					    WHERE 	task_type.actived=1");
		
		$data = $db->getList($count,$hidden,0);

		break;
	case 'save_task':
		$task_id 		= $_REQUEST['id'];
		$task_name    = $_REQUEST['name'];



		if($task_name != ''){
			if(!Checktask_typeExist($task_name)){
				Savetask_type($task_id, $task_name);
			}else{
				$error = '"' . $task_name . '" უკვე არის სიაში!';
            }
		}

		break;
	case 'disable':
		$task_id	= $_REQUEST['id'];
		Disabletask_type($task_id);

		break;
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Category Functions
* ******************************
*/

function Savetask_type($task_id, $task_name, $user_id){
    global $db;
    $db->setQuery("UPDATE `task_type`
    				SET `name`    = '$task_name',
    					`user_id` = '$user_id'
    			 WHERE	`id`      = $task_id");
    $db->execQuery();
}



function Checktask_typeExist($task_name){
    global $db;
    $db->setQuery("	SELECT `id`
					FROM   `task_type`
					WHERE  `name` = '$task_name' && `actived` = 1");
    $res = $db->getResultArray();
    
	if($res[result][0]['id'] != ''){
		return true;
	}
	return false;
}


function Gettask_type($task_id){
    global $db;
    $db->setQuery("	SELECT  `id`,
							`name`
					FROM    `task_type`
					WHERE   `id` = $task_id" );
    
    $res = $db->getResultArray();

	return $res[result][0];
}

function GetPage($res = '')
{
	$data = '
	<div id="dialog-form">
	    <fieldset>
	    	<legend>ძირითადი ინფორმაცია</legend>

	    	<table class="dialog-form-table">
				<tr>
					<td style="width: 170px;"><label for="CallType">დასახელება</label></td>
					<td>
						<input type="text" id="name" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="' . $res['name'] . '" />
					</td>
				</tr>

			</table>
			<!-- ID -->
			<input type="hidden" id="task_id" value="' . $res['id'] . '" />
        </fieldset>
    </div>
    ';
	return $data;
}

?>
