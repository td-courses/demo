<?php
require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db     = new dbClass();
$action	= $_REQUEST['act'];
$error	= '';
$data	= array();

switch ($action) {
	case 'get_add_page':
		$page		= GetPage();
		$data		= array('page'	=> $page);

		break;
	case 'get_edit_page':
		$comment_id	 = $_REQUEST['id'];
		$page		 = GetPage(Getcomment($comment_id));
		$data		 = array('page'	=> $page);

		break;
	case 'get_list' :
		$count	= $_REQUEST['count'];
		$hidden	= $_REQUEST['hidden'];
			
		$db->setQuery("SELECT  id,
                              `name`,
                               (SELECT GROUP_CONCAT(my_web_site.`name`) 
                				FROM chat_comment_site 
                				JOIN my_web_site ON my_web_site.id = chat_comment_site.site_id
                				WHERE comment_id = chat_comment1.id),
                			   (SELECT GROUP_CONCAT(`name`) 
                				FROM   chat_comment_source 
                				JOIN   source ON source.id = chat_comment_source.source_id
                				WHERE  comment_id = chat_comment1.id),
							  `comment`
					   FROM    chat_comment1
					   WHERE   actived=1");

		$data = $db->getList($count,$hidden,1);

		break;
	case 'save_priority':
		$comment_id   = $_REQUEST['id'];
		
		$comment      = htmlspecialchars($_REQUEST['comment'], ENT_QUOTES);
		$comment_name = htmlspecialchars($_REQUEST['comment_name'], ENT_QUOTES);
		
		$comment = str_replace('W20W','+',$comment);
		$comment_name = str_replace('W20W','+',$comment_name);

		if($comment != ''){
			if ($comment_id == '') {
			    Addcomment( $comment_id, $comment_name, $comment);
			}else {
			    Savecomment($comment_id, $comment_name, $comment);
			}				
			
		}
		
		break;
	case 'disable':
		$comment_id	= $_REQUEST['id'];
		Disablecomment($comment_id);

		break;
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Category Functions
* ******************************
*/

function Addcomment($comment_id, $comment_name, $comment){
    global $db;
	$user_id	= $_SESSION['USERID'];
	$site	    = $_REQUEST['site'];
	$source_id	= $_REQUEST['source_id'];
	$db->setQuery("INSERT INTO  `chat_comment1`
							   (`user_id`, `name`, `comment`)
					    VALUES 	  
							   ('$user_id', '$comment_name', '$comment')");
	$db->execQuery();
	$comment_id = $db->getLastId();
	
	
	$site_array = explode(",",$site);
	$db->setQuery("DELETE FROM chat_comment_site WHERE comment_id = '$comment_id'");
	$db->execQuery();
	
	foreach($site_array AS $site_id){
	    $db->setQuery("INSERT INTO chat_comment_site (comment_id, site_id) values ('$comment_id', $site_id)");
	    $db->execQuery();
	}
	
	$source_array = explode(",",$source_id);
	$db->setQuery("DELETE FROM chat_comment_source WHERE comment_id = '$comment_id'");
	$db->execQuery();
	
	foreach($source_array AS $source_id){
	    $db->setQuery("INSERT INTO chat_comment_source (comment_id, source_id) values ('$comment_id', $source_id)");
	    $db->execQuery();
	}
}

function Savecomment($comment_id, $comment_name, $comment){
    global $db;
	$user_id	= $_SESSION['USERID'];
	$site	    = $_REQUEST['site'];
	$source_id	= $_REQUEST['source_id'];
	
	$db->setQuery("	UPDATE `chat_comment1`
					   SET `user_id` = '$user_id',
                           `name`    = '$comment_name',
						   `comment` = '$comment'
					WHERE  `id`      =  $comment_id");
	$db->execQuery();
	
	$site_array = explode(",",$site);
	$db->setQuery("DELETE FROM chat_comment_site WHERE comment_id = '$comment_id'");
	$db->execQuery();
	
	foreach($site_array AS $site_id){
	    $db->setQuery("INSERT INTO chat_comment_site (comment_id, site_id) values ('$comment_id', $site_id)");
	    $db->execQuery();
	}
	
	$source_array = explode(",",$source_id);
	$db->setQuery("DELETE FROM chat_comment_source WHERE comment_id = '$comment_id'");
	$db->execQuery();
	
	foreach($source_array AS $source_id){
	    $db->setQuery("INSERT INTO chat_comment_source (comment_id, source_id) values ('$comment_id', $source_id)");
	    $db->execQuery();
	}
}

function Disablecomment($comment_id){
    global $db;
    
    $db->setQuery("UPDATE `chat_comment1`
					  SET `actived` = 0
				   WHERE  `id`      = $comment_id");
    
    $db->execQuery();
}

function CheckcommentExist($comment){
    global $db;
    
	$db->setQuery("SELECT `id`
				   FROM   `chat_comment1`
				   WHERE  `comment` = '$comment' AND `actived` = 1");
	
	$res = $db->getResultArray();
	
	if($res[result][0][id] != ''){
		return true;
	}
	return false;
}

function web_site($id){
    global $db;
    $data = '';
    
    $db->setQuery("SELECT `id`, `name`
				   FROM   `my_web_site`
				   WHERE   actived = 1");
    
    $res = $db->getResultArray();
    foreach ($res[result] AS $value){
        $db->setQuery("SELECT id
                       FROM  `chat_comment_site`
                       WHERE  chat_comment_site.comment_id = '$id' AND site_id = '$value[id]'");
        $check = $db->getNumRow();
        
        if($check>0){
            $data .= '<option value="' . $value['id'] . '" selected="selected">' . $value['name'] . '</option>';
        } else {
            $data .= '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
        }
        
    }
    
    return $data;
}

function get_source($id){
    global $db;
    $data = '';
    
    $db->setQuery("SELECT `id`, 
                          `name`
				   FROM   `source`
				   WHERE   actived = 1 AND id != 1");
    
    $res = $db->getResultArray();
    foreach ($res[result] AS $value){
        $db->setQuery("SELECT id
                       FROM  `chat_comment_source`
                       WHERE  chat_comment_source.comment_id = '$id' AND source_id = '$value[id]'");
        $check = $db->getNumRow();
        
        if($check>0){
            $data .= '<option value="' . $value['id'] . '" selected="selected">' . $value['name'] . '</option>';
        } else {
            $data .= '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
        }
        
    }
    
    return $data;
}

function Getcomment($comment_id){
    global $db;
	$db->setQuery(" SELECT  `id`,
							`comment`,
                            `name`
					FROM    `chat_comment1`
					WHERE   `id` = $comment_id");

	$res = $db->getResultArray();
	
	return $res[result][0];
}

function GetPage($res = ''){
	$data = '
	<div id="dialog-form">
	    <fieldset>
	    	<legend>ძირითადი ინფორმაცია</legend>
            <table class="dialog-form-table">
                <tr>
					<td style="width: 170px;"><label for="CallType">დასახელება</label></td>
				</tr>
				<tr>
					
					<td>
						<textarea  style="width: 550px; height:30px; resize: none;" id="comment_name" class="idle" name="call_content" cols="300" rows="8">' . $res['name'] . '</textarea>
					</td>
				</tr>
                <tr style="height:10px;"></tr>
                <tr>
					<td style="width: 170px;"><label for="CallType">საიტი</label></td>
				</tr>
				<tr>
					<td>
						<select style="height: 18px; width: 555px;" id="my_site" class="idls object" multiple>'.web_site($res['id']).'</select>
					</td>
				</tr>
                <tr style="height:10px;"></tr>
				<tr>
					<td style="width: 170px;"><label for="CallType">წყარო</label></td>
				</tr>
                <tr>
					<td>
						<select style="height: 18px; width: 555px;" id="source_id" class="idls object" multiple>'.get_source($res['id']).'</select>
					</td>
				</tr>
				<tr style="height:10px;"></tr>
				<tr>
					<td style="width: 170px;"><label for="CallType">ტექსტი</label></td>
				</tr>
				<tr>
					<td>
						<div style="position:relative;">
						<div id="emoji_wrapper"><span id="emoji_close" code="0" >დახურვა</span>'.get_emoji().'</div>
							<img id="dialog_emojis" src="https://crm.my.ge/myge_live_chat/img/smiling-emoticon-square-face.png" width="20px" height="20px" alt="">
						<textarea  style="width: 550px; height:300px; resize: none;" id="comment" class="idle" name="call_content" cols="300" rows="8">' . $res['comment'] . '</textarea>
						</div>
					</td>
				</tr>
            </table>
			<!-- ID -->
			<input type="hidden" id="comment_id" value="' . $res['id'] . '" />
        </fieldset>
	</div>
					
    ';
	return $data;
}
function get_emoji(){
	global $db;
	$db->setQuery("SELECT `emojis`.`code` FROM `emojis` WHERE `emojis`.actived = 1");

    $result  = $db->getResultArray();
    $emojis = "";
    foreach ($result[result] AS $arr) {
        $emojis .= "<span code= '&#".$arr[code]."' >&#".$arr[code]."</span>";
    }
    $data = array("emojis" => $emojis);
    return $emojis;
}
?>
