<?php
require_once('../../includes/classes/class.Mysqli.php');
global $db;

$db     = new dbClass();
$action	= $_REQUEST['act'];
$error	= '';
$data	= array();

switch ($action) {
	case 'get_setting':
		$db->setQuery("SELECT   `ip_count`,
                                `welcome_on_off`,
                                `welcome_text`,
                                `input_on_off`,
                                `input_name`,
                                `input_mail`,
                                `input_phone`,
                                `sound_on_off`,
                                `color`,
                                `text_color`,
                                `send_file`,
                                `send_mail`,
                                `send_sound`,
                                `send_close`,
                                `taimer`
                        FROM    `chat_setting`;");
		
		$res = $db->getResultArray();
		
		$data = array(  'ip_count'	     => $res[result][0][ip_count],
                        'welcome_on_off' => $res[result][0][welcome_on_off],
                        'welcome_text'	 => $res[result][0][welcome_text],
                        'input_on_off'	 => $res[result][0][input_on_off],
                        'input_name'	 => $res[result][0][input_name],
                        'input_mail'	 => $res[result][0][input_mail],
                        'input_phone'	 => $res[result][0][input_phone],
                        'sound_on_off'	 => $res[result][0][sound_on_off],
                        'color_id'       => $res[result][0][color],
                        'text_color'     => $res[result][0][text_color],
                        'send_file'      => $res[result][0][send_file],
                        'send_mail'      => $res[result][0][send_mail],
                        'send_sound'     => $res[result][0][send_sound],
                        'send_close'     => $res[result][0][send_close],
                        'taimer'         => $res[result][0][taimer]);
		
        break;
	case 'save_setting':
	    $db->setQuery("UPDATE `chat_setting` 
                          SET `ip_count`       = '$_REQUEST[ip_count]',
                              `welcome_on_off` = '$_REQUEST[welcome_on_off]',
                              `welcome_text`   = '$_REQUEST[welcome_text]',
                              `input_on_off`   = '$_REQUEST[input_on_off]',
                              `input_name`     = '$_REQUEST[input_name]',
                              `input_mail`     = '$_REQUEST[input_mail]', 
                              `input_phone`    = '$_REQUEST[input_phone]',
                              `sound_on_off`   = '$_REQUEST[sound_on_off]',
                              `color`          = '$_REQUEST[color_id]',
                              `text_color`     = '$_REQUEST[text_color]',
                              `send_file`      = '$_REQUEST[send_file]',
                              `send_mail`      = '$_REQUEST[send_mail]',
                              `send_sound`     = '$_REQUEST[send_sound]',
                              `send_close`     = '$_REQUEST[send_close]',
                              `taimer`         = '$_REQUEST[taimer]'");
	    
	    $db->execQuery();
	    
        break;
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);
?>
