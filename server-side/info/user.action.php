<?php 
/* ******************************
 *	Workers aJax actions
 * ******************************
 */ 
// include('../../core.php');
include('../../includes/classes/class.Mysqli.php');
$mysqli = new dbClass();
$action 	= $_REQUEST['act'];
$user_id	= $_SESSION['USERID'];
$error 		= '';
$data 		= array();

switch ($action) {
	case 'get_add_page':
		$page		= GetPage();
		$data		= array('page'	=> $page);

		break;
	case 'get_edit_page':
	    $per_id		= $_REQUEST['id'];
		$page		= GetPage(GetWorker($per_id));

        $data		= array('page'	=> $page);

	    break;
	case 'get_list':
	    $count = $_REQUEST['count'];
	    $hidden = $_REQUEST['hidden'];
		$query = "	SELECT 	`users`.`id`,
							`user_info`.`name`,
							'',
							`extention`.`extention`,
							`user_info`.`mobile_phone`,
							`position`.`person_position`,
							`user_info`.`address`				
					FROM   	`user_info` 
					left JOIN `position` ON `user_info`.`position_id` = `position`.`id`
					JOIN 	`users` ON `user_info`.`user_id` = `users`.`id`
					left join	extention ON `users`.extension_id = extention.id
					WHERE  	`users`.`actived` = 1";

		$mysqli->setQuery($query);
		$data = $mysqli->getList($count,$hidden,1);

		break;
		
		case 'get_list_archive':
	    $count = $_REQUEST['count'];
	    $hidden = $_REQUEST['hidden'];
		$query = "	SELECT 	`users`.`id`,
							`user_info`.`name`,
							'',
							`extention`.`extention`,
							`user_info`.`mobile_phone`,
							`position`.`person_position`,
							`user_info`.`address`				
					FROM   	`user_info` 
					left JOIN `position` ON `user_info`.`position_id` = `position`.`id`
					JOIN 	`users` ON `user_info`.`user_id` = `users`.`id`
					left join	extention ON `users`.extension_id = extention.id
					WHERE  	`users`.`actived` = 0";

		$mysqli->setQuery($query);
		$data = $mysqli->getList($count,$hidden,1);

        break;

    case 'save_pers':
		$persons_id 		= $_REQUEST['id'];
    	$name 				= htmlspecialchars($_REQUEST['n'], ENT_QUOTES);
		$tin 				= $_REQUEST['t'];
		$position 			= $_REQUEST['p'];
		$address 			= htmlspecialchars($_REQUEST['a'], ENT_QUOTES);
		$image				= $_REQUEST['img'];
		$password			= $_REQUEST['pas'];
		$home_number		= $_REQUEST['h_n'];
		$mobile_number		= $_REQUEST['m_n'];
		$comment			= $_REQUEST['comm'];
		$user				= $_REQUEST['user'];
		$userpassword		= md5($_REQUEST['userp']);
		$group_permission	= $_REQUEST['gp'];
		$dep_id             = $_REQUEST['dep_id'];
		$service_center_id  = $_REQUEST['service_center_id'];
		$branch_id          = $_REQUEST['branch_id'];

		$CheckUser 			= CheckUser($user);


		if(empty($persons_id)){
			if($CheckUser){
				AddWorker($user_id, $name, $tin, $position, $address, $image, $password, $home_number, $mobile_number, $comment,  $user, $userpassword, $group_permission, $dep_id, $service_center_id, $branch_id);
			}else{
				$error = "მომხმარებელი ასეთი სახელით  უკვე არსებობს\nაირჩიეთ სხვა მომხმარებლის სახელი";
			}
		}else{
			SaveWorker($persons_id, $user_id, $name, $tin, $position, $address, $image, $password, $home_number, $mobile_number, $comment,  $user, $userpassword, $group_permission, $dep_id, $service_center_id, $branch_id);
		}


        break;
    case 'disable':
		$per_id = $_REQUEST['id'];
		DisableWorker($per_id);

		break;
		case 'enable':
		$per_id = $_REQUEST['id'];
		EnableWorker($per_id);

        break;
	case 'delete_image':
		$file_name		= $_REQUEST['file_name'];
		DeleteImage($file_name);

		break;
	case 'view_img':
	    $page		= GetIMG($_REQUEST[id]);
	    $data		= array('page'	=> $page);
	     
	    break;
    case 'GetServiceCenter':
        $page		= GetServiceCenter('',$_REQUEST[branch_id]);
        $data		= array('page'	=> $page);
    
        break;
	case 'clear':
		$file_list = $_REQUEST['file'];
		ClearProduct();
		if (!empty($file_list)) {
			$file_list = ClearFiles(json_decode($file_list));
		}
		$data = array('file_list' => json_encode($file_list));

    default:
       $error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Workers Functions
 * ******************************
 */
 
function GetIMG($id){
	global $mysqli;
	$query = "SELECT `image` FROM `user_info` WHERE `user_id` = $id ";

	$mysqli -> setQuery($query);
	$result = $mysqli -> getResultArray();

	$res = $result['result'][0];	

    if (empty($res['image'])) {
        $image = '0.jpg';
    }else{
        $image = $res['image'];
    }
    $data = '<div id="dialog-form">
	           <fieldset>
                <img style="margin: auto;display: block;" width="350" height="350"  src="media/uploads/file/'.$image.'">
               </fieldset>
             </div>
            ';

    return $data;
}
function CheckUser($user){
	global $mysqli;
	$query = "SELECT `username`
						FROM   `users`
						WHERE  `username` = '$user'";

	$mysqli -> setQuery($query);
	$result = $mysqli -> getResultArray();						

	if($result['count'] > 0){
		return false;
	}

	return true;
}



function ClearProduct() { 
	global $mysqli;
	$query = "SELECT	`id`,
    							`name`
						FROM `persons`";
	
	$mysqli -> setQuery($query);
	$result = $mysqli -> getResultArray();

	$req = $result['result'];	

	foreach( $req as $res){
		$name = htmlspecialchars($res[name], ENT_QUOTES);

		GLOBAL $log;
		$log->setUpdateLogBefore('persons', $res[id]);

		$query = "	UPDATE
		`persons`
		SET
		`name`	= '$name'
		WHERE
		`id`	= '$res[id]'";

		$mysqli -> setQuery($query);
		$mysqli -> execQuery();	

		$log->setUpdateLogAfter('persons', $res[id]);
	}
}

function AddWorker($user_id, $name, $tin, $position, $address, $image, $password, $home_number, $mobile_number, $comment,  $user, $userpassword, $group_permission, $dep_id, $service_center_id, $branch_id)
{
    $task_send_mail	= $_REQUEST['task_send_mail'];
    $user_mail		= $_REQUEST['user_mail'];
    $site    		= $_REQUEST['site'];
    
	global $mysqli;
    if($user != '' && $userpassword !='' && $group_permission !=''){
        $ext			= $_REQUEST['ext'];
        if(strlen($_REQUEST['userp']) == 32){
    
        }else{
            $query="INSERT	INTO `users`
                        (`username`,`password`,`group_id`,`extension_id`)
                        VALUES
						('$user','$userpassword','$group_permission','$ext')";
						$mysqli -> setQuery($query);
						$mysqli -> execQuery(); 
        }
    }
    
	$persons_id = $mysqli->getLastId();
	
	$user_id = $_SESSION["USERID"];
	
	$query = "INSERT INTO `user_info`
					(`user_id`, `name`, `tin`, `position_id`, `address`, `image`, `home_phone`, `mobile_phone`, `comment`, `dep_id`, `service_center_id`, `branch_id`, `mail`, `send_mail`)
				VALUES
				    ($persons_id, '$name', '$tin', $position, '$address', '$image', '$home_number', '$mobile_number', '$comment', '$dep_id', '$service_center_id', '$branch_id', '$user_mail', '$task_send_mail')";

	$mysqli -> setQuery($query);
	$mysqli -> execQuery();
	
	$site_array = explode(",",$site);
	$mysqli->setQuery("DELETE FROM users_site WHERE user_id = '$persons_id'");
	$mysqli->execQuery();
	
	foreach($site_array AS $site_id){
	    $mysqli->setQuery("INSERT INTO users_site (user_id, site_id) values ('$persons_id', $site_id)");
	    $mysqli->execQuery();
	}
	

}

function SaveWorker($persons_id, $user_id, $name, $tin, $position, $address, $image, $password, $home_number, $mobile_number, $comment, $user, $userpassword, $group_permission, $dep_id, $service_center_id, $branch_id)
{	global $mysqli;
	$user_id 		= $_SESSION['USERID'];
	
	$task_send_mail	= $_REQUEST['task_send_mail'];
	$user_mail		= $_REQUEST['user_mail'];
	$site    		= $_REQUEST['site'];
	
	$group_id 		= checkgroup($user_id);

	
	$query = "UPDATE `user_info` 
                 SET `user_id`			 = '$persons_id',
					 `name`				 = '$name',
					 `tin`				 = '$tin',
					 `position_id`		 = '$position',
					 `dep_id`	    	 = '$dep_id',
					 `address`			 = '$address',
					 `image`			 = '$image',
				 	 `home_phone`		 = '$home_number',
					 `mobile_phone`  	 = '$mobile_number',
					 `comment`			 = '$comment',
					 `service_center_id` = '$service_center_id',
					 `branch_id`		 = '$branch_id',
                     `mail`              = '$user_mail',
                     `send_mail`         = '$task_send_mail'
			 WHERE   `user_id`           = $persons_id";
			$mysqli -> setQuery($query);
			$mysqli -> execQuery();
				
			$site_array = explode(",",$site);
			$mysqli->setQuery("DELETE FROM users_site WHERE user_id = '$persons_id'");
			$mysqli->execQuery();
			
			foreach($site_array AS $site_id){
			    $mysqli->setQuery("INSERT INTO users_site (user_id, site_id) values ('$persons_id', $site_id)");
			    $mysqli->execQuery();
			}

	if( $user!= '' && $userpassword!='' && $group_permission!=''){
		$ext			= $_REQUEST['ext'];
		if(strlen($_REQUEST['userp']) == 32){
			$query = "	UPDATE	`users` SET
									`users`.`username` = '$user',
									`users`.`group_id` = '$group_permission',
									`users`.`extension_id` = '$ext'
							WHERE	`users`.`id` = '$persons_id' AND `users`.actived = 1";
							$mysqli -> setQuery($query);
							$mysqli -> execQuery(); 
		}else{
		$query = "	UPDATE	`users` SET
								`users`.`username` = '$user',
								`users`.`password` = '$userpassword',
								`users`.`group_id` = '$group_permission',
								`users`.`extension_id` = '$ext'
						WHERE	`users`.`id` = '$persons_id'	&& `users`.actived = 1";
						$mysqli -> setQuery($query);
						$mysqli -> execQuery(); 
		}
	}else{
		$query = "	UPDATE	`users` SET
								`users`.`extension_id` = '$ext'
						WHERE	`users`.`id` = '$persons_id' AND `users`.actived = 1";
						$mysqli -> setQuery($query);
						$mysqli -> execQuery(); 
	}
	
}

function DisableWorker($per_id)
{	global $mysqli;
	$user_id 		= $_SESSION['USERID'];
	$group_id 		= checkgroup($user_id);

	if ($user_id != 2) {
		$query = "UPDATE `users` SET
							`actived` = 0
					WHERE  `users`.`id` = '$per_id'";
					$mysqli -> setQuery($query);
					$mysqli -> execQuery(); 

		$query = "SELECT id 
												FROM user_status_log
												WHERE `user_id` = '$user_id'";
												// echo ">>". $res['id'];
												$mysqli -> setQuery($query);
		$result = $mysqli -> getResultArray();

		$res = $result['result'][0];
		if($res['id']==null){
			
			$query = "INSERT INTO `user_status_log` (`user_id`,`disable_time`,`enable_time`)
							VALUES ('$user_id', NOW(),'')
								";
			$mysqli -> setQuery($query);
			$mysqli -> execQuery(); 						
		}
		
		else{
			$query = "UPDATE `user_status_log` SET
						`disable_time` = NOW()
						WHERE  `user_id` = '$user_id'";
						$mysqli -> setQuery($query);
						$mysqli -> execQuery(); 
		}
		
	} else {
		global $error;
		$error = 'თქვენ არ გაქვთ თანამშრომლის წაშლის უფლება';
	}
}

function EnableWorker($per_id)
{	global $mysqli;
	$user_id 		= $_SESSION['USERID'];
	$group_id 		= checkgroup($user_id);

	if ($user_id != 2) {
		$query = "UPDATE `users` SET
							`actived` = 1
					WHERE  `users`.`id` = '$per_id'";
					$mysqli -> setQuery($query);
					$mysqli -> execQuery(); 

		$query = "SELECT id 
														FROM user_status_log
														WHERE `user_id` = '$user_id'";
														// echo ">>". $res['id'];
														$mysqli -> setQuery($query);
														$result = $mysqli -> getResultArray();

														$res = $result['result'][0];
				if($res['id']==null){
					
					$query = "INSERT INTO `user_status_log` (`user_id`,`disable_time`,`enable_time`)
									VALUES ('$user_id','', NOW())
										";	
					$mysqli -> setQuery($query);
					$mysqli -> execQuery(); 
				}
				
				else{
					$query = "UPDATE `user_status_log` SET
								`enable_time` = NOW()
								WHERE  `user_id` = '$user_id'";
								$mysqli -> setQuery($query);
								$mysqli -> execQuery(); 
				}



	} else {
		global $error;
		$error = 'თქვენ არ გაქვთ თანამშრომლის აღდგენის უფლება';
	}



}


function GetPosition($id)
{
	global $mysqli;
	$data = '';
    $query = "SELECT 	`id`,
    						   	`person_position`
						FROM 	`position`
						WHERE 	`actived` = '1'";
						$mysqli -> setQuery($query);
						$data = $mysqli->getSelect($id,"person_position");
	

	return $data;
}

function GetDepart($id){
	global $mysqli;
    $data = '';
    $query = "SELECT 	`id`,
    						   	`name`
						FROM 	`department`
						WHERE 	`actived` = '1'";
    $mysqli -> setQuery($query);
	$data = $mysqli -> getSelect();

	
    
    return $data;
}

function GetWorker($per_id)
{
	global $mysqli;
    $query = "	SELECT	`users`.`id` as `id`,
                                    				`user_info`.`name` as `name`,
                                    				`user_info`.`tin` as `tin`,
                                    				`user_info`.`position_id` as `position`,
                                                    `user_info`.`dep_id` as `dep_id`,
                                    				`user_info`.`address` as `address`,
                                                    `user_info`.branch_id,
                                                    `user_info`.service_center_id,
                                    				`user_info`.`image`,
                                                    -- `file`.`id` as `image_id`,
                                    				`users`.`username` as `username`,
                                    				`users`.`password` as `user_password`,
                                    				`users`.`group_id` as `group_id`,
                                    				`users`.`extension_id` as `ext`,
                                    				`user_info`.`home_phone` as `home_number`,
                                    				`user_info`.`mobile_phone` as `mobile_number`,
                                    				`user_info`.`comment` as `comment`,
                                                    `user_info`.mail,
                                                    `user_info`.send_mail
                                            FROM	`user_info`
                                            LEFT JOIN	`users` ON `users`.`id` = `user_info`.`user_id`
                                            -- LEFT JOIN	`file` ON `users`.`id` = `file`.`user_id`
											WHERE	`user_info`.`user_id` = '$per_id'";
											
	$mysqli -> setQuery($query);
	$result = $mysqli -> getResultArray();

	$res = $result['result'][0];
											
	return $res;
}

function DeleteImage($file_name)
{
	global $mysqli;
	$query = "UPDATE
				`file`
				SET
				`actived`			= 0
				WHERE
				`rand_name`			= '$file_name' ";
				$mysqli -> setQuery($query);
				$mysqli -> execQuery();	
}

function GetGroupPermission( $group_id, $check){
	global $mysqli;
	$data  = '';
	$where = "actived = 1";
	
	if ($check != '') {
	    $where = "actived IN(1,0)";
	}
	$query = "SELECT	`group`.id as `id`,
								`group`.`name` as `name`
						FROM	`group`
						WHERE   actived = 1";
	
    $mysqli -> setQuery($query);
    $data = $mysqli -> getSelect($group_id);
						


	return $data;
}

function GetServiceCenter( $id,$branch_id ){
	global $mysqli;
	$data = '';
	$query = "SELECT	`id`,
								`name`
						FROM	`service_center`
						WHERE   `actived` = 1 AND parent_id = 0 AND branch_id = $branch_id";
						    $mysqli -> setQuery($query);
							$data = $mysqli -> getSelect();
						


	return $data;
}

function GetBranch( $id ){
	global $mysqli;
	$data = '';
	$query = "SELECT	`id`,
								`name`
						FROM	 `department`
	                    WHERE   `actived` = 1";
$mysqli -> setQuery($query);

	$data = $mysqli->getSelect($id);

	return $data;
}

function web_site($id){
    global $mysqli;
    $data = '';
    
    $mysqli->setQuery("SELECT `id`, 
                              `name`
				       FROM   `my_web_site`
				       WHERE   actived = 1");
    
    $res = $mysqli->getResultArray();
    foreach ($res[result] AS $value){
        $mysqli->setQuery("SELECT id
                           FROM  `users_site`
                           WHERE  users_site.user_id = '$id' AND site_id = '$value[id]'");
        $check = $mysqli->getNumRow();
        
        if($check>0){
            $data .= '<option value="' . $value['id'] . '" selected="selected">' . $value['name'] . '</option>';
        }else{
            $data .= '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
        }
        
    }
    
    return $data;
}

function checkgroup($user){
	global $mysqli;
	$query = " SELECT users.group_id           
										   FROM   users          
										   WHERE  users.id = $user";
										   
										   $mysqli -> setQuery($query);
										   $result = $mysqli -> getResultArray();
								   
										   $res = $result['result'][0];										   

	 return $res['group_id']; 
}

function GetPage($res = '')
{
    global $mysqli;
    $image = $res['image'];
	if(empty($image)){
		$image = '0.jpg';
	}else{
	    $disable_img = 'disabled';
	}
	$query = "SELECT extention.id, extention.extention FROM extention";
	$mysqli -> setQuery($query);
	
	$checked = "";
	if ($res[send_mail] == 1) {
        $checked = "checked";
    }
	$data = '
	<div id="dialog-form">
	    <fieldset>
	    	<legend>ძირითადი ინფორმაცია</legend>

	    	<table class="dialog-form-table" id="info_table">
				<tr>
					<td class="label_td">
						<label for="name">სახელი, გვარი</label>
					</td>
					<td>
						<input type="text" id="name" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="' . $res['name'] . '" />
					</td>
				</tr>
				
				<tr>
					<td class="label_td">
						<label for="tin">პირადი ნომერი</label>
					</td>
					<td>
						<input type="text" id="tin" class="idle user_id" onblur="this.className=\'idle user_id\'" onfocus="this.className=\'activeField user_id\'" value="' . $res['tin'] . '" />
					</td>
				</tr>
				<tr>
					<td class="label_td">
						<label for="position">თანამდებობა</label>
					</td>
					<td>
						<select id="position" class="idls">' . GetPosition($res['position']) . '</select>
					</td>
				</tr>
                
				<tr>
					<td class="label_td">
						<label for="branch_id">განყოფილება</label>
					</td>
					<td>
						<select id="branch_id" class="idls" >' . GetBranch($res['branch_id']) . '</select>
					</td>
				</tr>
                <tr>
					<td class="label_td">
						<label for="address">დავალებაზე გაიგზავნოს მეილი</label>
                    </td>
					<td>
						<input style="margin-left: -103px;" type="checkbox" name="language" id="task_send_mail" value="1" autocomplete="off" '.$checked.'>
					</td>
				</tr>
                <tr>
					<td class="label_td">
						<label for="address">მეილი
					</label></td>
					<td>
						<input type="text" id="user_mail" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="' . $res['mail'] . '" />
					</td>
				</tr>
                <tr>
					<td class="label_td">
						<label for="address">საიტი</label>
                    </td>
					<td>
                        <select id="site_ids" class="idls" multiple>' . web_site($res['id']) . '</select>
					</td>
				</tr>
				<tr>
					<td class="label_td">
						<label for="address">მისამართი
					</label></td>
					<td>
						<input type="text" id="address" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="' . $res['address'] . '" />
					</td>
				</tr>
				<tr>
					<td class="label_td">
						<label for="home_number">სახლის ტელ: </label>
					</td>
					<td>
						<input type="text" id="home_number" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="' . $res['home_number'] . '" />
					</td>
				</tr>
				<tr>
					<td class="label_td">
						<label for="mobile_number">მობილური ტელ: </label>
					</td>
					<td>
						<input type="text" id="mobile_number" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="' . $res['mobile_number'] . '" />
					</td>
				</tr>
				<tr>
					<td class="label_td">
						<label for="comment">შენიშვნა: </label>
					</td>
					<td valign="top">
						<textarea id="comment" class="idle"  style="width: 230px !important;resize: vertical;">' . $res['comment'] . '</textarea>
					</td>
				</tr>
			</table>
			<!-- ID -->
			<div id="accordion">
			  <h3>მომხმარებელი</h3>
			  <div>
				<div>
					<div style="display: inline;"><label for="user" style="float:left;">მომხმარებელი :</label>
						<input type="text" id="user" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="' . $res['username'] . '" style="display: inline; margin-left: 36px;"/>
					</div>
				</div>
				<div style=" margin-top: 10px; ">
					<div style="display: inline;"><label for="user_password" style="float:left;">პაროლი :</label>
						<input type="password" id="user_password" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="' . $res['user_password'] . '" style="display: inline; margin-left: 72px;"/>
					</div>
				</div>
                <div style=" margin-top: 10px; ">
					<div style="display: inline;"><label for="user_password" style="float:left;">ჯგუფი :</label>
						<select style="display: inline; margin-left: 86px; width: 165px;" id="group_permission">' . GetGroupPermission( $res['group_id'], $res[id]) . '</select>
					</div>
				</div>
			 </div>
			</div>
        </fieldset>
 	    <fieldset>
	    	<legend>თანამშრომლის სურათი</legend>

	    	<table class="dialog-form-table" width="100%">
	    		<tr>
					<td id="img_colum">
						<img style="margin-left: 5px;" width="105" height="105" id="upload_img" img="'.$image.'" src="media/uploads/file/'.$image.'" />
					</td>
				</tr>
				<tr>
					<td style="padding-left: 30px;">
						<span>
							<a href="#" onclick="view_image('.$res[image_id].')" class="complate">View</a> | <a href="#" id="delete_image" image_id="'.$res[image_id].'" class="delete">Delete</a>
						</span>
					</td>
				</tr>
				</tr>
					<td style="padding-left: 5px;">
						<div style="margin-top:10px; width: 127px; margin-left: -5px;" class="file-uploader">
							<input id="choose_file" type="file" name="choose_file" class="input" style="display: none;">
							<button id="choose_button'.$disable_img.'" class="center" >აირჩიეთ ფაილი</button>
						</div>
					</td>
				</tr>
			</table>
        </fieldset>
		<input type="hidden" id="pers_id" value="' . $res['id'] . '" />
		<input type="hidden" id="is_user" value="'; 
		 $query = "SELECT id+1 AS `id` FROM users ORDER BY id DESC LIMIT 1" ;
		$result = $mysqli -> getResultArray();

		$incUs = $result['result'][0];
		$data .= $incUs[0];
		$data .= '" />
    </div>
    ';
	return $data;
}




?>