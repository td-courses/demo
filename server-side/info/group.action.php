<?php
/* ******************************
 *	Workers aJax actions
 * ******************************
 */

include('../../includes/classes/class.Mysqli.php');

$mysqli = new dbClass();



$action 	= $_REQUEST['act'];
$user_id	= $_SESSION['USERID'];
$error 		= '';
$data 		= array();

switch ($action) {
	case 'get_add_page':
		$page		= GetGroupPage();
		$data		= array('page'	=> $page);
		
		break;
	case 'get_edit_page':
	    $group_id		= $_REQUEST['id'];
		$page		    = GetGroupPage(GetGroup($group_id));
        
        $data		= array('page'	=> $page);
        
	    break;
	case 'get_list':
	    $count = $_REQUEST['count'];
	    $hidden = $_REQUEST['hidden'];
		$query = "SELECT `id`, `name` FROM `group` WHERE actived = 1";
		$mysqli->setQuery($query);
		$data = $mysqli->getList($count,$hidden,1);

        break;
	case 'get_pages_list':
		$count    = $_REQUEST['count'];
		$hidden   = $_REQUEST['hidden'];
		$group_id = $_REQUEST['group_id'];
		

		if(!empty($group_id)){
			$query = "SELECT  menu_detail.page_id,
                            				menu_detail.title,				
                            				IFNULL(group_permission.page_id,0) AS `check` 
                                    FROM    menu_detail
                                    LEFT JOIN group_permission ON menu_detail.page_id = group_permission.page_id AND group_permission.group_id = '$group_id'
                                    WHERE		menu_detail.url NOT IN ('#') AND menu_detail.page_id != 14 GROUP BY page_id";
		}else {
			$query = "SELECT  menu_detail.page_id,
                            				menu_detail.title,				
                            				IFNULL(group_permission.page_id,0) AS `check` 
                                    FROM    menu_detail
                                    LEFT JOIN group_permission ON menu_detail.page_id = group_permission.page_id AND group_permission.group_id = '$group_id'
                                    WHERE		menu_detail.url NOT IN ('#') AND menu_detail.page_id != 14 GROUP BY page_id";
		}								
		
		// $mysqli->setQuery($query);
		// $data = $mysqli->getList($count,$hidden,1,);

		$dataa    = array("aaData" => array());
        $result  = $mysqli->mysqli->query($query);
        
        if($result){
            
            while($aRow = $result->fetch_array(MYSQLI_NUM)){
                $row = array();
                for ($i = 0 ; $i < $count ; $i++){
                    /* General output */
                    $row[] = $aRow[$i];
					
						$check = "";
						if($aRow[2] != 0){
							$check.="checked";
						}
                        if($i == ($count - 1)){
                            $row[] = '<div class="callapp_checkbox">
                                          <input type="checkbox" id="callapp_checkbox1_'.$aRow[$hidden].'" name="check1_'.$aRow[$hidden].'" value="'.$aRow[$hidden].'" class="check1" '.$check.' />
                                          <label for="callapp_checkbox1_'.$aRow[$hidden].'"></label>
                                      </div>';
                        }
                    
                }
                $dataa['aaData'][] = $row;
                
			}
		}
			$data = $dataa;
						
		break;
	case 'save_group':
		$group_name		= $_REQUEST['nam'];
		$group_pages	= json_decode(stripslashes($_REQUEST['pag']));
		$group_id       = $_REQUEST['group_id'];	
		$wellcome_page_id = $_REQUEST['wellcome_page_id'];
		if(empty($group_id)){
			SaveGroup($group_name, $group_pages,$wellcome_page_id);
		}else{
			ClearForUpdate($group_id);
			UpdateGroup($group_id, $group_pages, $group_name,$wellcome_page_id);
		}
  		
		break;        
    case 'disable':
		$group_id = $_REQUEST['id'];
		DisableGroup($group_id);
				
        break;           
    default:
       $error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Workers Functions
 * ******************************
 */


function SaveGroup($group_name, $group_pages,$wellcome_page_id){
	global $mysqli;
	$query = "INSERT	INTO `group`
						(`group`.`name`)
				VALUES
						('$group_name')";
	
	$mysqli->setQuery($query);
	$mysqli->execQuery();

	$group_id = $mysqli->getLastId();
	
	$query = "INSERT INTO `group_wellcome_page` (`group_id`, `page_id`)
						VALUES ($group_id,$wellcome_page_id)";	

	$mysqli->setQuery($query);
	$mysqli->execQuery();
	
	$parrentaray = array();
	foreach($group_pages as $group_page) {
		$query = "INSERT	INTO `group_permission`
						(`group_permission`.`group_id`, `group_permission`.`page_id`)
					VALUES
						('$group_id','$group_page')";

		$mysqli -> setQuery($query);
		$mysqli -> execQuery(); 				

		
		$query = "	SELECT		`menu_detail`.`parent` as `parent_id`
												FROM		`pages`
												LEFT JOIN	`menu_detail` ON `menu_detail`.`page_id` = `pages`.`id`
												LEFT JOIN	`menu_detail` as `menu_detail1` ON `menu_detail1`.`id` =  `menu_detail`.`parent`
												WHERE		`pages`.`id` = '$group_page' AND `menu_detail`.`parent` != 0 ";
		
		$mysqli -> setQuery($query);
		$result = $mysqli -> getResultArray();
		foreach($result['result'] as $res){
			if( !in_array($res['parent_id'], $parrentaray) ){
				array_push($parrentaray, $res['parent_id']);		
			}
		}
	}	
	$query = "	SELECT	`pages`.`id` as `id`
											FROM	`pages`
											WHERE	`pages`.`name` = 'logout'";

	$mysqli -> setQuery($query);
	$result = $mysqli -> getResultArray();
	
	$res = $result['result'][0];



	$query = "INSERT	INTO `group_permission`
					(`group_permission`.`group_id`, `group_permission`.`page_id`)
				VALUES
					('$group_id','$res[id]')";
				
	$mysqli -> setQuery($query);
	$mysqli -> execQuery(); 		

	
	foreach($parrentaray as $parrent) {
		$query = "INSERT	INTO `group_permission`
						(`group_permission`.`group_id`, `group_permission`.`page_id`)
					VALUES
						('$group_id','$parrent')";
		$mysqli -> setQuery($query);
		$mysqli -> execQuery(); 

	}
		
}

function UpdateGroup($group_id, $group_pages, $group_name,$wellcome_page_id){
	global $mysqli;

	$query = "UPDATE  `group`
					SET  `name` = '$group_name'
			      WHERE  `id`  = $group_id
	";

	$mysqli -> setQuery($query);
	$mysqli -> execQuery(); 	
	
	$query = "UPDATE `group_wellcome_page`
	SET `page_id` = $wellcome_page_id
	WHERE `group_id` = $group_id";

	$mysqli->setQuery($query);
	$mysqli->execQuery();


	$parrentaray = array();
	foreach($group_pages as $group_page) {
		$query = "INSERT	INTO `group_permission`
		(`group_permission`.`group_id`, `group_permission`.`page_id`)
		VALUES
		('$group_id','$group_page')";

		$mysqli -> setQuery($query);
		$mysqli -> execQuery(); 
	
	
		$query = "	SELECT		`menu_detail`.`parent` as `parent_id`
				FROM		`pages`
				LEFT JOIN	`menu_detail` ON `menu_detail`.`page_id` = `pages`.`id`
				LEFT JOIN	`menu_detail` as `menu_detail1` ON `menu_detail1`.`id` =  `menu_detail`.`parent`
				WHERE		`pages`.`id` = '$group_page' AND `menu_detail`.`parent` != 0 ";

		$mysqli -> setQuery($query);
		$result = $mysqli -> getResultArray();

		$res = $result['result'][0];	

		if( !in_array($res['parent_id'], $parrentaray) ){
			array_push($parrentaray, $res['parent_id']);
		}
	}
	$query = "	SELECT	`pages`.`id` as `id`
											FROM	`pages`
											WHERE	`pages`.`name` = 'logout'";

	$mysqli -> setQuery($query);
	$result = $mysqli -> getResultArray();

	$res = $result['result'][0];	

	$query = "INSERT INTO `group_permission`
	(`group_permission`.`group_id`, `group_permission`.`page_id`)
	VALUES
	('$group_id','$res[id]')";
	
	$mysqli -> setQuery($query);
	$mysqli -> execQuery(); 


	$query = "SELECT id 
	                             FROM `group_permission`
								 WHERE group_id = '$group_id' AND page_id = 14";
								 
	$mysqli -> setQuery($query);
	$result = $mysqli -> getResultArray();							 

	if($result['count'] < 1){
		$query = "INSERT INTO `group_permission`
        		(`group_permission`.`group_id`, `group_permission`.`page_id`)
        		VALUES
				('$group_id','14')";
				
		$mysqli -> setQuery($query);
		$mysqli -> execQuery(); 		
				
	}
	
	foreach($parrentaray as $parrent) {
		$query = "INSERT	INTO `group_permission`
		(`group_permission`.`group_id`, `group_permission`.`page_id`)
		VALUES
		('$group_id','$parrent')";

		$mysqli -> setQuery($query);
		$mysqli -> execQuery();
	
	}
	
	$parrentaray1 = array();
	foreach($parrentaray as $parrent) {
	
	    $query = "	SELECT		`menu_detail`.`parent` as `parent_id`
	        FROM		`pages`
	        LEFT JOIN	`menu_detail` ON `menu_detail`.`page_id` = `pages`.`id`
	        LEFT JOIN	`menu_detail` as `menu_detail1` ON `menu_detail1`.`id` =  `menu_detail`.`parent`
			WHERE		`pages`.`id` = '$parrent' AND `menu_detail`.`parent` != 0 ";
			
			$mysqli -> setQuery($query);
			$result = $mysqli -> getResultArray();
	
			$res = $result['result'][0];

	    if( !in_array($res['parent_id'], $parrentaray1) ){
	        array_push($parrentaray1, $res['parent_id']);
	    }
	}
	
	foreach($parrentaray1 as $parrent1) {
	    $query = "SELECT id
                        	        FROM `group_permission`
									WHERE group_id = '$group_id' AND page_id = '$parrent1'";
		
		$mysqli -> setQuery($query);
		$mysqli -> execQuery();
		
	    if(mysql_num_rows($main_checker) < 1){
    	    $query = "INSERT	INTO `group_permission`
                	    (`group_permission`.`group_id`, `group_permission`.`page_id`)
                	    VALUES
						('$group_id','$parrent1')";
						
						$mysqli -> setQuery($query);
						$mysqli -> execQuery();			
	    }
	
	}
	
}



function DisableGroup($group_id)
{
	global $mysqli;
   $query = "UPDATE `group`
                SET `actived` = 0 
             WHERE  `id`      = '$group_id'";
	$mysqli -> setQuery($query);
	$mysqli -> execQuery();			 
}	


// function GetGroupNameById($group_id){
// 	global $mysqli;
// 	$query = "SELECT   `name`
// 							     FROM   `group`
// 								WHERE   `id` = $group_id";
// 	if($group_id !='')	{						
// 		$mysqli -> setQuery($query);
// 		$result = $mysqli -> getResultArray();
// 	}
// 	$res = $result['result'][0];
// 	return $res['name'];
// }


function ClearForUpdate($group_id){
	global $mysqli;
	$query = "DELETE FROM group_permission
					   WHERE group_id = $group_id";
	$mysqli -> setQuery($query);
	$mysqli -> execQuery();					   
}

function GetGroupPage($res = ''){
	
	$data = '
	<div id="dialog-form">
	    
 	    <fieldset>
	    <legend>ჯგუფი</legend>
    	    <table>
        	    <tr>
            	    <td style="width: 98px;">
            	       <label style="padding-top: 6px;">ჯგუფის სახელი :</label>
            	    </td>
            	    <td style="height: 21px;">
            	       <input style="width: 290px;" type="text" id="group_name" class="idle" onblur="this.className=\'idle\'" value="'.$res['name'].'"/>
            	    </td>
        	    </tr>
    	    </table>
		</fieldset>	
 	    <fieldset>
			<legend>გვერდები</legend>
			<div id = "choose" style="margin-bottom:10px">
				<span style="margin-right:50px;">მთავარი გვერდი </span>
				<select id="wellcome_page">
						'.get_pages($res["wellcome_page"]).'
				</select>
			</div>										
            <div id="dynamic" style="overflow: auto; height: 400px;">
                <table class="display" id="pages" style="width: 100%;">
                    <thead>
                        <tr style=" white-space: no-wrap;" id="datatable_header">
                            <th >ID</th> 
                            <th style="width: 100%;">გვერდის სახელი</th>
                            <th style="width: 30px;">#</th>   
                        </tr>
                    </thead>
					<thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                            	<input type="text" name="search_id" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input type="text" name="search_address" value="ფილტრი" class="search_init" style="width: 99%"/>
                            </th>
                            <th>
                            	<div class="callapp_checkbox">
                                    <input type="checkbox" id="check-all-group" name="check-all-group" />
                                    <label for="check-all-group"></label>
                                </div>
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </fieldset>						
    </div>

	<input type="hidden" id="group_id" value="' . $res['id'] . '" />
			
    ';
	return $data;
}

function get_pages($id){
	global $mysqli;
	
	$query = "SELECT page_id, title
						FROM menu_detail
						WHERE parent = 0 AND url ='' AND id NOT IN (68,69,71,1)";
	$mysqli -> setQuery($query);
	$data = $mysqli->getSelect($id,"title");
	

	return $data;
}

function GetGroup($group_id){
	global $mysqli;
	$query = "SELECT `group`.`id`,
												`group`.`name`,
												`group_wellcome_page`.`page_id` AS `wellcome_page`
												FROM   `group`
												LEFT JOIN `group_wellcome_page` ON `group_wellcome_page`.group_id = `group`.`id`
												WHERE `group`.`id` = $group_id";
	$mysqli -> setQuery($query);
	$result = $mysqli -> getResultArray();

	$res = $result['result'][0];											

	return $res;
}

?>