<?php
require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();
$action	= $_REQUEST['act'];
$error	= '';
$data	= array();



$rand_file	= $_REQUEST['rand_file'];
$file		= $_REQUEST['file_name'];
$mail_id    = $_REQUEST['mail_id'];



switch ($action) {
	case 'send_mail':
	    $out_id     = $_REQUEST['incomming_id'];
	    
		$page = GetPage($out_id, GetSMS($mail_id));
		$data = array('page' => $page);

		break;
	case 'send_mail_shablon':
	    $id = $_REQUEST['mail_id'];
	    $page = GetShablon($id);
	    $data = array('page'	=> $page);
	    
	    break;
	case 'get_list' :
		$count	= $_REQUEST['count'];
		$hidden	= $_REQUEST['hidden'];
		$inc_id	= $_REQUEST['inc_id'];
		
		$db->setQuery(" SELECT sent_mail.id,
							   sent_mail.date,
							   sent_mail.address,
							   sent_mail.`subject`,
							   IF(sent_mail.`status`=2,'გაგზავნილი','არ გაიგზავნა')
						FROM   sent_mail
						WHERE  sent_mail.incomming_call_id = '$inc_id'");

		$data = $db->getList($count, $hidden);

		break;
	case 'get_list_shablon' :
	    $count	 = $_REQUEST['count'];
	    $hidden	 = $_REQUEST['hidden'];
	    $site = $_REQUEST['site'];
	    $filter_site = '';
	   
	    
	    if ($site != 'null' && $site != ''){
	        $filter_site = "AND chat_comment1.id IN(SELECT   chat_comment_site.comment_id 
                                            FROM     chat_comment_site
                                            WHERE    chat_comment_site.site_id IN($site)
                                            GROUP BY chat_comment_site.comment_id)";
		}
		
		$filter_source = "AND chat_comment1.id IN(SELECT   chat_comment_source.comment_id
											FROM     chat_comment_source
											WHERE    chat_comment_source.source_id IN(7)
											GROUP BY chat_comment_source.comment_id)";
	    
	    $db->setQuery("SELECT id, chat_comment1.`name`,`chat_comment1`.`comment`, CONCAT('<button class=\"choose_shablon\">არჩევა</button>')
		FROM  `chat_comment1`
		WHERE `chat_comment1`.actived=1  $filter_site $filter_source
	   ");
	    
	    
	    $data = $db->getList($count, $hidden);
	    
	    break;
	case 'disable':
		$source_id	= $_REQUEST['id'];
		Disablesource($source_id);
		
        break;
		
		
	case 'delete_mail_file':
		
		$file_id 	= $_REQUEST['file_id'];
		$inccall_id = $_REQUEST['inccall_id'];
		
		$db->setQuery("UPDATE send_mail_detail
        			   JOIN   sent_mail ON sent_mail.id = send_mail_detail.sent_mail_id
        			      SET send_mail_detail.actived = 0
        			   WHERE  file_id = $file_id AND sent_mail.incomming_call_id = $inccall_id  AND `status` = 1");
		
		$db->execQuery();
		
		$db->setIncrement("	SELECT    file.`name`,
			    		    		  file.`rand_name`,
			    		    		  file.`id`
	    		    		FROM 	 `sent_mail`
	    		    		LEFT JOIN send_mail_detail ON send_mail_detail.sent_mail_id=sent_mail.id
	    		    		LEFT JOIN file ON file.id=send_mail_detail.file_id
	    		    		WHERE    `sent_mail`.incomming_call_id = $inccall_id AND sent_mail.`status`=1 AND send_mail_detail.actived=1");
		
		$increm = $db->getResultArray();
		$data1 = '';
	
		foreach($increm[result] AS $increm_row)	{	
			$data1 .=' <tr style="border-bottom: 1px solid #85b1de;">
					          <td style="width:110px; display:block;word-wrap:break-word;">'.$increm_row['rand_name'].'</td>													 
					          <td style=" width: 18px;"><button type="button" value="media/uploads/file/'.$increm_row['name'].'" style="cursor:pointer; border:none; margin-top:5%; display:block; height:16px;   margin-right: 0px; width:16px; background:none;background-image:url(\'media/images/get.png\');" id="download_name1" value="'.$increm_row[rand_name].'"> </td>
					          <td style=" width: 18px;"><button file_id="'. $increm_row[id] .'" inccall_id="'. $inccall_id .'"  type="button" value="'.$increm_row['id'].'" style="cursor:pointer; border:none; margin-top:5%; display:block; height:16px; width:16px;   margin-right: 0px; background:none; background-image:url(\'media/images/x.png\');" id="delete_mail_file"></button></td>
				          </tr>';
		}
	
		$data = array('deletedfiles' => $data1);
	
		break;
		
	case 'add_signature':
	    
		$signature_id = $_REQUEST['signature_id'];
		$db->setQuery("SELECT 	 `signature`,
								 `file`.`rand_name` as `image`
					   FROM      `signature`
					   LEFT JOIN `file` ON `signature`.`id` = `file`.`signature_id` AND 	`file`.actived = 1
					   WHERE 	 `signature`.`id` = '$signature_id'");
		
		$res    = $db->getResultArray();
		$page   = $res[result][0]['signature'];
		$path   = "../../media/uploads/file/".$res[result][0]['image'];
		$im     = file_get_contents($path);
		$imdata = base64_encode($im);  
		
		if($res[result][0]['image'] != ''){
			$page .= '<br><br><img src="data:image/jpg;base64,'.$imdata.'">';
		}
		
		$data = array('page' => $page);
		
		break;
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Category Functions
* ******************************
*/

function Addsource($source_name, $content){
		
    $user    = $_SESSION['USERID'];
	mysql_query("INSERT INTO 	 	`mail`
									(`subject`, `body`, `user_id`, `actived`)
							VALUES 		
									('$source_name', '$content', '$user', '1')");
	
}

function Savesource($source_id, $source_name, $content){
	
	$user_id	= $_SESSION['USERID'];
	mysql_query("	UPDATE `mail`
					SET    `subject` = '$source_name',
						   `body`='$content'
					WHERE  `id` = $source_id");
	
}

function Disablesource($source_id){
	
	mysql_query("	UPDATE `mail`
					SET    `actived` = 0
					WHERE  `id` = $source_id");
	
}

function Getsource($source_id){
	$res = mysql_fetch_assoc(mysql_query("	SELECT  id,
                                    				address,
	                                                cc_address,
	                                                bcc_address,
                                    				`subject`,
                                    				body
                                            FROM `sent_mail`
                                            WHERE actived=1 AND `status`=2 AND id=$source_id" ));

	return $res;
}

function GetSMS($id){
    global $db;
    $db->setQuery("SELECT sent_mail.id,
                          date,
                          address,
                          cc_address,
                          bcc_address,
                         `subject`,
                          body                                                
                   FROM  `sent_mail`                                            
                   WHERE  sent_mail.id = '$id'");
    
    $req = $db->getResultArray();
    return $req[result];
}

function get_mail_list($id=0){
    global $db;
	$data = '<option value="0">signature</option>';

	$db->setQuery(" SELECT `id`,
					       `name`
					FROM   `mail_account`
					where   actived = 1");

	$data = $db->getSelect($id);

	return $data;
}

function GetPage($out_id, $res){
	global $db;
	if($res==''){
	    $db->setQuery("INSERT INTO sent_mail (user_id) VALUES ('1')");
	    $db->execQuery();
	    $increment = $db->getLastId();
	    $db->setQuery("DELETE FROM sent_mail WHERE `id` = $increment");
	    $db->execQuery();
	}else{
		$increment = $res['id'];
	}
    $db->setQuery("SELECT `file`.file_date, 
                          `file`.`name`,
                          `rand_name`,
                          `file`.id
                   FROM   `file`
                   WHERE   mail_id = '$increment' 
                   AND    `file`.actived = 1");
    
    $file = $db->getResultArray();
	$data = '
	<div id="dialog-form">
	    <fieldset class="shablon_scroll" style="height: auto;">
	    	<table class="dialog-form-table">
                <tr>
					<td style="width: 90px; "><label for="d_number">From:</label></td>
					<td>
						<select id="from_mail_id" style="width:489px">
							'.get_mail_list().'
						</select>
					</td>
				</tr>
				<tr>
					<td style="width: 90px; "><label for="d_number">ადრესატი:</label></td>
					<td>
						<input type="text" check style="width: 484px !important;"id="mail_address" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="' . $res['address'] . '" />
					</td>
				</tr>
				<tr>
					<td style="width: 90px;"><label for="d_number">CC:</label></td>
					<td>
						<input type="text" style="width: 484px !important;" id="mail_address1" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="' . $res['cc_address'] . '" />
					</td>
				</tr>
				<tr>
					<td style="width: 90px;"><label for="d_number">Bcc:</label></td>
					<td>
						<input type="text" style="width: 484px !important;" id="mail_address2" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="' . $res['bcc_address'] . '" />
					</td>
				</tr>
				<tr>
					<td style="width: 90px;"><label for="d_number">სათაური:</label></td>
					<td>
						<input type="text" style="width: 484px !important;" id="mail_text" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="' . $res['subject'] . '" />
					</td>
				</tr>
			</table>
			<table class="dialog-form-table">
				<tr>
					<td colspan="2">	
						<div id="input" contenteditable="true" style="width:400px; height:200px">' . $res['body'] . '</div>
					</td>
				</tr>
			</table>
			<div style="margin-top: 15px;">
                    <div style="width: 100%; border:1px solid #CCC;float: left;">    	            
    	                   <div style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;width: 180px;float:left;">თარიღი</div>
                    	   <div style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;width: 189px;float:left;">დასახელება</div>
                    	   <div style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;width: 160px;float:left;">ჩამოტვირთვა</div>
                           <div style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;width: 20px;float:left;">-</div>
    	                   <div style="text-align: center;vertical-align: middle;float: left;width: 595px;"><button id="choose_button_mail" style="cursor: pointer;background: none;border: none;width: 100%;height: 25px;padding: 0;margin: 0;">აირჩიეთ ფაილი</button><input style="display:none;" type="file" name="choose_mail_file" id="choose_mail_file" value=""></div>
                           <div id="paste_files1">';
	                       foreach($file[result] AS $file_body){
                        	    $data .= '<div id="first_div">'.$file_body[file_date].'</div>
                                	      <div id="two_div">'.$file_body[name].'</div>
                                	      <div id="tree_div" onclick="download_file(\''.$file_body[rand_name].'\',\''.$file_body[name].'\')">ჩამოტვირთვა</div>
                        				  <div id="for_div" onclick="delete_file1(\''.$file_body[id].'\')">-</div>
                        				  <input type="hidden" class="attachment_address" value="'.$file_body[rand_name].'">';
                           }
                           $data .='</div>
					</div>
					
	            </div>
			
			<fieldset style="display: inline-flex; width: 100%; margin-left: -11px; float: left;">
			<table class="dialog-form-table">
			<tr>
					<td style="width: 69px;">
						<button id="email_shablob" class="center">შაბლონი</button>
					</td>		
					<td>
						<div class="file-uploader">
							
							<input id="hidden_inc" type="text" value="'. $res['name'] .'" style="display: none;">
						</div>
				    </td>
					<td style="width: 69px;">
						
					</td>
					<td style="width: 69px;">
					</td>
					<td style="width: 69px;">
					</td>
					<td style="width: 69px;">
					</td>
					<td style="width: 69px;">
					</td>
					<td style="width: 69px;">
					</td>
					<td style="width: 69px;">
						<button id="send_email" class="center">გაგზავნა</button>
					</td>
				</tr>
			</table>
			</fieldset>
			<!-- ID -->
			<input type="hidden" id="source_id" value="'.$res[id].'" />
			<input type="hidden" id="hidden_increment" value="'.$increment.'" />
        </fieldset>
    </div>
    ';
	return $data;
}

function GetShablon($id) {
    global $db;
    $db->setQuery("SELECT 	`chat_comment1`.`id`,
							`chat_comment1`.`name` AS `subject`,
							`chat_comment1`.`comment` AS `body`
					FROM    `chat_comment1`
					JOIN 	`chat_comment_site` ON chat_comment_site.comment_id =  chat_comment1.id
					JOIN 	`mail_account` ON mail_account.site_id = chat_comment_site.site_id
					WHERE 	`chat_comment1`.`actived` = 1 AND mail_account.id = '$id' ");
    
    $res = $db->getResultArray();
    foreach($res[result] AS $req){
        $tbody .= '<tr>
                    <td>'.$req[id].'</td>
                    <td>'.$req[subject].'</td>
                    <td><span onclick="pase_body(\'body_'.$req[id].'\',\''.$req[subject].'\')">არჩევა</span> <div id="body_'.$req[id].'" style="display:none;">'.$req[body].'</div></td>
                   </tr>';
    }
    $data = '<div id="dialog-form">
        	    <fieldset class="shablon_scroll" style="height: 400px; overflow-y: scroll;">
                    <table class="display" id="example_shablon_table" style="width: 100%;">
                        <thead>
                            <tr>
                                <th style="width: 100px;">№</th>
                                <th style="width: 50%;">სათაური</th>
                                <th style="width: 50%;">შინაარსი</th>
                                <th style="width: 100px;">ქმედება</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr class="search_header">
                                <th class="colum_hidden">
                                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                                </th>
                                <th>
                                    <input style="width: 90%;" type="text" name="search_category" value="ფილტრი" class="search_init"/>
                                </th>
                                <th>
                                    <input style="width: 90%;" type="text" name="search_category" value="ფილტრი" class="search_init"/>
                                </th>
                                <th>
                                    
                                </th>
                            </tr>
                        </thead>
       
                    </table>
                </fieldset>
             </div>';
    return $data;
}

?>