<?php

/* ******************************
 *	Request aJax actions
* ******************************
*/

require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();

include('../../includes/classes/wsdl.class.php');

$action = $_REQUEST['act'];
$error	= '';
$data	= array();
//incomming
$incom_id			 = $_REQUEST['id'];
$call_date			 = $_REQUEST['call_date'];
$hidden_user		 = $_REQUEST['hidden_user'];
$hidden_inc			 = $_REQUEST['hidden_inc'];
$imchat			     = $_REQUEST['imchat'];
$chat_id             = $_REQUEST['chat_id'];
$ipaddres            = $_REQUEST['ipaddres'];
$ii			         = $_REQUEST['ii'];
$phone				 = $_REQUEST['phone'];
// file
$rand_file			= $_REQUEST['rand_file'];
$file				= $_REQUEST['file_name'];

switch ($action) {
	case 'get_add_page':
		$number		= $_REQUEST['number'];
		$ipaddres   = $_REQUEST['ipaddres'];
		$ab_pin     = $_REQUEST['ab_pin'];

		$page		= GetPage(getlastphone($number), $number,'',$ipaddres,$ab_pin);
		$data		= array('page'	=> $page);

		break;
	case 'disable':
		$incom_id = $_REQUEST['id'];
		
		mysql_query("UPDATE `incomming_call`
						SET `noted`= 2
					  WHERE `id`   = $incom_id");
		break;
	case 'get_signature':
	    $site_id = $_REQUEST['site_id'];
	    
	    $db->setQuery(" SELECT signature 
                        FROM  `signature`
                        WHERE  account_id = '$site_id' AND actived = 1
                        LIMIT 1");
	    
	    $res = $db->getResultArray();
	    
	    $data = array('signature' => $res[result][0][signature]);
	    
	    break;
		
	case 'save_user_comunication':
	    $user	             = $_SESSION['USERID'];
	    $web_chat_checkbox   = $_REQUEST['web_chat_checkbox'];
	    $site_chat_checkbox = $_REQUEST['site_chat_checkbox'];
	    $messanger_checkbox  = $_REQUEST['messanger_checkbox'];
	    $mail_chat_checkbox  = $_REQUEST['mail_chat_checkbox'];
	    $video_call_checkbox = $_REQUEST['video_call_checkbox'];
	    
	    $db->setQuery("SELECT id, user_id, chat, site, messenger, mail, cf, video 
                       FROM   users_cumunication
                       WHERE  user_id = '$user'");
	    
	    $check = $db->getNumRow();
	    $req   = $db->getResultArray();
	    $row   = $req[result][0];
	    if ($check == 0) {
	        if ($web_chat_checkbox != 1) {
	            $web_chat_checkbox = 0;
	        }
	        
	        if ($site_chat_checkbox != 1){
	            $site_chat_checkbox=0;
	        }
	        
	        if ($messanger_checkbox != 1){
	            $messanger_checkbox = 0;
	        }
	        
	        if ($mail_chat_checkbox != 1){
	            $mail_chat_checkbox = 0;
	        }
	        
	        if ($video_call_checkbox != 1){
	            $video_call_checkbox=0;
	        }
	        
	        $db->setQuery("INSERT INTO `users_cumunication` 
                                    (`user_id`, `chat`, `site`, `messenger`, `mail`, `video`) 
                              VALUES 
                                    ('$user', '$web_chat_checkbox', '$site_chat_checkbox', '$messanger_checkbox', '$mail_chat_checkbox', '$video_call_checkbox');");
	        
	        $db->execQuery();
	    }else{
	        if ($web_chat_checkbox != 1) {
	            $web_chat_checkbox = 0;
	        }
	        
	        if ($site_chat_checkbox != 1){
	            $site_chat_checkbox=0;
	        }
	        
	        if ($messanger_checkbox != 1){
	            $messanger_checkbox = 0;
	        }
	        
	        if ($mail_chat_checkbox != 1){
	            $mail_chat_checkbox = 0;
	        }
	        
	        if ($video_call_checkbox != 1){
	            $video_call_checkbox=0;
	        }
	        
	        $db->setQuery("UPDATE `users_cumunication`
                              SET `chat`      = '$web_chat_checkbox',
                    			  `site`     = '$site_chat_checkbox',
                    			  `messenger` = '$messanger_checkbox',
                    			  `mail`      = '$mail_chat_checkbox',
                    			  `video`     = '$video_call_checkbox'
                           WHERE  `id`        = '$row[id]'");
	        $db->execQuery();
	    }
	    break;
	    
	case 'check_user_comunication':
	    $user = $_SESSION['USERID'];
	    
	    $db->setQuery("SELECT chat, site, messenger, mail, cf, video
	                   FROM   users_cumunication
	                   WHERE  user_id = '$user'");
	    
	    $check = $db->getNumRow();
	    $req   = $db->getResultArray();
	    $row   = $req[result][0];
	    if ($check == 0) {
	        $data = array('web_chat_checkbox'   => 0,
        	              'site_chat_checkbox' => 0,
        	              'messanger_checkbox'  => 0,
        	              'mail_chat_checkbox'  => 0,
        	              'video_call_checkbox' => 0);
	    }else{
	        $data = array('web_chat_checkbox'   => $row[chat],
            	          'site_chat_checkbox' => $row[site],
            	          'messanger_checkbox'  => $row[messenger],
            	          'mail_chat_checkbox'  => $row[mail],
            	          'video_call_checkbox' => $row[video]);
	    }
	    break;
	case 'load_dnd_data':
	    
	    $user_id = $_SESSION["USERID"];
	    $get_options = $_REQUEST["getOptions"];
	    
	    $options = $get_options == "true" ? get_activitie_options() : "";
	    $data = array('options' => $options);
	    
	    break;
	case 'focusin':
	    $id = $_REQUEST["id"];
	    $db->setQuery("UPDATE chat SET writes = 1 WHERE id = '$id'");
	    $db->execQuery();
	    break;
	case 'get_client_conversation_count':
	    $user = $_SESSION['USERID'];
	    $db->setQuery("SELECT   queries.id, 
					            queries_history.id AS `queries_id`
                      FROM      queries
                      LEFT JOIN queries_history ON queries_history.queries_id = queries.id AND queries_history.user_id = '$user'
                      WHERE     ISNULL(queries_history.id) AND queries.actived = 1");
	    $res = $db->getResultArray();
	    foreach ($res[result] AS $req){
	        $db->setQuery("INSERT INTO `queries_history`
                    	              (`user_id`, `datetime`, `queries_id`, `status`, `actived`)
                    	        VALUES
                    	              ('$user', NOW(), '$req[id]', 1, 1)");
	        $db->execQuery();
	    }
	    
	    $data = array('count' => 0);
	    
	    break;
	case 'focusout':
	    $id = $_REQUEST["id"];
	    $db->setQuery("UPDATE chat SET writes = 0 WHERE id = '$id'");
	    $db->execQuery();
	    break;
	case 'get_resp_user':
	    
	    $extention = $_REQUEST['extention'];
	    
	    $user_name = mysql_fetch_array(mysql_query("SELECT `id`, `name`
                                                     FROM   crystal_users
                                                     WHERE  last_extension = '$extention'
	                                                 AND    logged = 1"));
	    $data = array('id' => $user_name[id], 'name' => $user_name[name]);
	    break;
	    
	case 'get_edit_page':
	    $imby = $_REQUEST['imby'];
	    $page		= GetPage(Getincomming($incom_id,$chat_id),'', $imby , '', '');

		$data		= array('page'	=> $page);

		break;
	case 'chat_on_off':
	    $_SESSION['chat_off_on'] = $_REQUEST['value'];
	    $user	= $_SESSION['USERID'];
	    if($_REQUEST['value'] == 2){
	        mysql_query("UPDATE `crystal_users` SET `online`='1' WHERE (`id`='$user');");
	    }else{
	        mysql_query("UPDATE `crystal_users` SET `online`='0' WHERE (`id`='$user');");
	    }

	    break;
	case 'save_micr':
	    $user	= $_SESSION['USERID'];
	    $status = $_REQUEST['activeStatus'];
	    $db->setQuery("SELECT * FROM `user_mute_check` WHERE user_id = $user");
	    $check = $db->getNumRow();
	    if($check == 0){
	        $db->setQuery("INSERT INTO user_mute_check SET user_id='$user', `muted`='$status' ");
	        $db->execQuery();
	    }else{
	        $db->setQuery("UPDATE `user_mute_check` SET `muted`='$status' WHERE `user_id`='$user';");
	        $db->execQuery();
	    }
	    
	    break;
	case 'logout':

		$c_date	= date('Y-m-d H:i:s');
		$user	= $_SESSION['USERID'];

		$rResult=mysql_fetch_array(mysql_query("SELECT  MIN(ABS(UNIX_TIMESTAMP(person_work_graphic.`end`)-UNIX_TIMESTAMP(NOW()))) AS end_date
												FROM person_work_graphic
												WHERE person_work_graphic.person_id='$user'"));

		if ($rResult['end_date']<15 && $rResult['end_date']!= null){
			$data['logout']	= 1;
			$data['time']	= $rResult['end_date'];
			mysql_query("UPDATE `crystal_users` SET `online`='0' WHERE (`id`='$user');");
		}else{
			$data['logout']	= 0;
			$data['time']	= $rResult['end_date'];

		}

		break;
	case 'get_list':
		$count        = $_REQUEST['count'];
		$hidden       = $_REQUEST['hidden'];
		$fillter      = $_REQUEST['fillter'];
		$tab_id       = $_REQUEST['tab_id'];
		$operator_id  = $_REQUEST['operator_id'];
		$start        = $_REQUEST['start'];
		$end          = $_REQUEST['end'];
		$user_info_id = $_REQUEST['user_info_id'];
		$user	      = $_SESSION['USERID'];
		$user_filt    = '';
		if ($user_info_id == 1) {
		    $user_filt = "AND incomming_call.user_id = $user";
		}
		
		if ($tab_id > 0) {
		    $filt = " AND incomming_call.inc_status_id = '$tab_id'";
		}else{
		    $filt = '';
		}
		
		if ($operator_id > 0) {
		    $filt1 = "AND IF(ISNULL(incomming_call.asterisk_incomming_id),incomming_call.user_id,asterisk_incomming.user_id) = '$operator_id'";
		}else{
		    $filt1 = '';
		}
		 
		$filter_1  = $_REQUEST['filter_1'];
		$filter_2  = $_REQUEST['filter_2'];
		$filter_3  = $_REQUEST['filter_3'];
		$filter_4  = $_REQUEST['filter_4'];
		$filter_5  = $_REQUEST['filter_5'];
		
		if ($filter_1 != 1) {
		    $filter_1 = 0;
		}
		
		if ($filter_2 != 2) {
		    $filter_2 = 0;
		}
		
		if ($filter_3 != 3) {
		    $filter_3 = 0;
		}
		
		if ($filter_4 != 4) {
		    $filter_4 = 0;
		}
		
		if ($filter_5 != 5) {
		    $filter_5 = 0;
		}
		
		$filt_status = '';
		if ($filter_1 > 0 || $filter_2 > 0 || $filter_3 > 0 || $filter_4 > 0 || $filter_5 > 0) {
    		 $filt_status = "AND (CASE
            							WHEN ISNULL(incomming_call.`cat_1`) AND ISNULL(incomming_call.`cat_1_1`) AND IFNULL(`asterisk_incomming`.`disconnect_cause`,100) NOT IN(0,1,2) THEN 2
            							WHEN (NOT ISNULL(incomming_call.`cat_1`) OR NOT ISNULL(incomming_call.`cat_1_1`)) AND IFNULL(`asterisk_incomming`.`disconnect_cause`,100) NOT IN(0,1,2) THEN 1
            							WHEN asterisk_incomming.disconnect_cause = 2 THEN 3
            							WHEN asterisk_incomming.disconnect_cause = 1 THEN 4
            							WHEN asterisk_incomming.disconnect_cause = 0 THEN 5
            					  END) IN ($filter_1, $filter_2, $filter_3, $filter_4, $filter_5)";
		}
		
		
		$filter_6  = $_REQUEST['filter_6'];
		$filter_7  = $_REQUEST['filter_7'];
		$filter_8  = $_REQUEST['filter_8'];
		$filter_9  = $_REQUEST['filter_9'];
		$filter_10 = $_REQUEST['filter_10'];
		$filter_11 = $_REQUEST['filter_11'];
		
		if ($filter_6 == 6) {
		    $filter_6 = 1;
		}else{
		    $filter_6 = 0;
		}
		
		if ($filter_7 == 7) {
		    $filter_7 = 4;
		}else{
		    $filter_7 = 0;
		}
		
		if ($filter_8 == 8) {
		    $filter_8 = 9;
		}else{
		    $filter_8 = 0;
		}
		
		if ($filter_9 == 9) {
		    $filter_9 = 6;
		}else{
		    $filter_9 = 0;
		}
		
		if ($filter_10 == 10) {
		    $filter_10 = 11;
		}else{
		    $filter_10 = 0;
		}
		
		if ($filter_11 == 11) {
		    $filter_11 = 7;
		}else{
		    $filter_11 = 0;
		}
		
		if ($filter_6 > 0 || $filter_7 > 0 || $filter_8 > 0 || $filter_6 > 0 || $filter_9 > 0 || $filter_10 > 0 || $filter_11 > 0) {
		  $filter_source = "AND IFNULL(incomming_call.source_id,11) IN($filter_6, $filter_7, $filter_8, $filter_9, $filter_10, $filter_11)";
		}
		$db->setQuery("SELECT       IF(asterisk_incomming.disconnect_cause = 2, '', incomming_call.id),
                                    incomming_call.id,
                                    incomming_call.date,
                                    incomming_call.client_name,
                                    incomming_call.client_user_id,
                                    IFNULL(asterisk_incomming.source,incomming_call.phone) AS `phone`,
                                    incomming_call.client_mail,
                                   (SELECT GROUP_CONCAT(`name`) 
                                    FROM  `incoming_call_site`
                                    JOIN   my_web_site ON my_web_site.id = incoming_call_site.site_id
                                    WHERE  incomming_call_id = incomming_call.id),
                                    IFNULL(user_info1.`name`,user_info.`name`) AS `user_name`,
                                    inc_status.`name`,
                                    IF(asterisk_incomming.disconnect_cause = 2, TIME_FORMAT(SEC_TO_TIME(asterisk_incomming.wait_time),'%i:%s'), TIME_FORMAT(SEC_TO_TIME(asterisk_incomming.duration),'%i:%s')) AS `duration`,
                                    CASE
                                        WHEN (NOT ISNULL(incomming_call.`chat_id`) AND NOT ISNULL(incomming_call.`cat_1`) AND NOT ISNULL(incomming_call.`cat_1_1`))
                                            THEN concat(_utf8 '<button class=\'download7\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/Chat.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დამუშავებ.ჩატი</span></button>')
                                        WHEN (NOT ISNULL(incomming_call.`chat_id`) AND (ISNULL(incomming_call.`cat_1`) OR ISNULL(incomming_call.`cat_1_1`)))
                                            THEN concat(_utf8 '<button class=\'download8\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/Chat.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დაუმუშავე.ჩატი</span></button>')
                                        
                                        WHEN (NOT ISNULL(incomming_call.`mail_chat_id`) AND (ISNULL(incomming_call.`cat_1`) OR ISNULL(incomming_call.`cat_1_1`)))
                                            THEN concat(_utf8 '<button class=\'download15\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/E-MAIL.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დაუმუშა.მეილი</span></button>')
                                        WHEN (NOT ISNULL(incomming_call.`mail_chat_id`) AND NOT ISNULL(incomming_call.`cat_1`) AND NOT ISNULL(incomming_call.`cat_1_1`))
                                            THEN concat(_utf8 '<button class=\'download15\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/E-MAIL.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დამუშავ.მეილი</span></button>')
                                        
                                        WHEN (NOT ISNULL(incomming_call.`site_chat_id`) AND (ISNULL(incomming_call.`cat_1`) OR ISNULL(incomming_call.`cat_1_1`)))
                                            THEN concat(_utf8 '<button class=\'download10\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/site.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დაუმუშ.მესენჯერი</span></button>')
                                        WHEN (NOT ISNULL(incomming_call.`site_chat_id`) AND NOT ISNULL(incomming_call.`cat_1`) AND NOT ISNULL(incomming_call.`cat_1_1`))
                                            THEN concat(_utf8 '<button class=\'download10\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/site.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დამუშა.მესენჯერი</span></button>')
                                        
                                        WHEN (NOT ISNULL(incomming_call.`fb_chat_id`) AND (ISNULL(incomming_call.`cat_1`) OR ISNULL(incomming_call.`cat_1_1`)))
                                            THEN concat(_utf8 '<button class=\'download11\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/Messenger.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დაუმ.მესენჯერი</span></button>')
                                        WHEN (NOT ISNULL(incomming_call.`fb_chat_id`) AND NOT ISNULL(incomming_call.`cat_1`) AND NOT ISNULL(incomming_call.`cat_1_1`))
                                            THEN concat(_utf8 '<button class=\'download11\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/Messenger.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დამუ.მესენჯერი</span></button>')
                                        
                                        WHEN (NOT ISNULL(incomming_call.`messenger_chat_id`) AND (ISNULL(incomming_call.`cat_1`) OR ISNULL(incomming_call.`cat_1_1`)))
                                            THEN concat(_utf8 '<button class=\'download13\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/CB.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დაუმ.მესენჯერი</span></button>')
                                        WHEN (NOT ISNULL(incomming_call.`messenger_chat_id`) AND NOT ISNULL(incomming_call.`cat_1`) AND NOT ISNULL(incomming_call.`cat_1_1`))
                                            THEN concat(_utf8 '<button class=\'download13\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/CB.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დამუ.მესენჯერი</span></button>')
                                        
                                        WHEN `asterisk_incomming`.`disconnect_cause` = 2
                                            THEN concat('<button class=\'download2\' str=',CONCAT(DATE_FORMAT(asterisk_incomming.call_datetime,'%Y/%m/%d/'),asterisk_incomming.file_name),'><span style=\"vertical-align: -1px; font-size: 10px;\">უპასუხო</span></button>')
                                        WHEN `asterisk_incomming`.`disconnect_cause` = 0
                                            THEN concat('<button style=\'background-color: #ff9800;\' class=\'download2\' str=',CONCAT(DATE_FORMAT(asterisk_incomming.call_datetime,'%Y/%m/%d/'),asterisk_incomming.file_name),'><span style=\"vertical-align: -1px; font-size: 10px;\">რიგშია</span></button>')
                                        WHEN `asterisk_incomming`.`disconnect_cause` = 1
                                            THEN concat('<button style=\'background-color: #ff9800;\' class=\'download2\' str=',CONCAT(DATE_FORMAT(asterisk_incomming.call_datetime,'%Y/%m/%d/'),asterisk_incomming.file_name),'><span style=\"vertical-align: -1px; font-size: 10px;\">საუბრობს</span></button>')
                                        WHEN `asterisk_incomming`.`disconnect_cause` IN(3,4)
                                            THEN IF (isnull(incomming_call.`user_id`),concat('<button class=\'download4\' str=',CONCAT(DATE_FORMAT(asterisk_incomming.call_datetime,'%Y/%m/%d/'),asterisk_incomming.file_name),'><img style=\"float: left;\" src=\"media/images/icons/comunication/phone.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დაუმ.შემომავა.</span></button>'),IF(incomming_call.transfer=1, concat('<button class=\'download5\' str=',CONCAT(DATE_FORMAT(asterisk_incomming.call_datetime,'%Y/%m/%d/'),asterisk_incomming.file_name),'><span style=\"vertical-align: -1px; font-size: 10px;\">გადართული  </span></button>'),concat('<button class=\'download\' str=',CONCAT(DATE_FORMAT(asterisk_incomming.call_datetime,'%Y/%m/%d/'),asterisk_incomming.file_name),'><img style=\"float: left;\" src=\"media/images/icons/comunication/phone.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">შემომავ. ზარი</span></button>')))
                                        ELSE '<button style=\"height: 22px; background-color: #2dc100;\" class=\"download15\"\><span style=\"vertical-align: 1px; font-size: 13px;\">მანუალური</span></button>'
                                    END AS `file_name`
                        FROM        incomming_call
                        LEFT JOIN   asterisk_incomming ON asterisk_incomming.id = incomming_call.asterisk_incomming_id
                        LEFT JOIN   user_info ON user_info.user_id = asterisk_incomming.user_id
                        LEFT JOIN   user_info AS `user_info1` ON user_info1.user_id = incomming_call.user_id
                        LEFT JOIN   inc_status ON inc_status.id = incomming_call.inc_status_id
                        JOIN        source ON incomming_call.source_id = source.id
                        LEFT JOIN   my_web_site ON incomming_call.web_site_id = my_web_site.id 
                        WHERE       DATE(incomming_call.date) BETWEEN '$start' AND '$end' $filt $filt1 $filt_status $filter_source $user_filt");
		
        $data = $db->getList($count, $hidden);

		break;
	case 'get_list_history' :
	    $count        = $_REQUEST['count'];
	    $hidden       = $_REQUEST['hidden'];

	    $start_check  = $_REQUEST['start_check'];
	    $end_check    = $_REQUEST['end_check'];
	    $phone        = $_REQUEST['phone'];
	    $s_u_user_id  = $_REQUEST['s_u_user_id'];

	    $filt = '';
	    if($phone == '' && $s_u_user_id == ''){
	        $filt="AND inc.client_user_id = 'fddhgdhgd'";
	    }

	    if ($phone != '' && $s_u_user_id != '') {
	        $filt="AND inc.client_user_id = '$s_u_user_id' OR inc.phone = '$phone'";
	    }

	    if ($phone != '' && $s_u_user_id == '') {
	        $filt="AND inc.phone = '$phone'";
	    }

	    if ($phone == '' && $s_u_user_id != '') {
	        $filt="AND inc.client_user_id = '$s_u_user_id'";
	    }

	    $db->setQuery(" SELECT    `inc`.`id` AS `task_id`,
                        		  `inc`.`date` AS `date`,
                        		   IF(`inc`.client_user_id = '000000', '', `inc`.client_user_id) AS `pin`,
                        		   IF(`inc`.`phone` = '2004', `inc`.`ipaddres`, `inc`.`phone`) AS `phone`,
                        	       user_info.`name`,
                        		  `inc`.`call_content`,
                                   IF(inc.inc_status_id=0,'დაუმუშავებელი',inc_status.`name`) AS `status`
                        FROM      `incomming_call` AS `inc`
                        LEFT JOIN  user_info ON `inc`.`user_id` = user_info.user_id
                        LEFT JOIN  inc_status  ON inc.inc_status_id = inc_status.id
                        WHERE      DATE(inc.date) BETWEEN '$start_check' AND '$end_check' $filt");

	    $data = $db->getList($count, $hidden);

	    break;
	case 'get_list_client_order' :
	    $count                         = $_REQUEST['count'];
	    $hidden                        = $_REQUEST['hidden'];
	    $history_user_id               = $_REQUEST['history_user_id'];
	    $incomming_call_id             = $_REQUEST['incomming_call_id'];
	    $client_history_filter_date    = $_REQUEST['client_history_filter_date'];
	    $client_history_filter_activie = $_REQUEST['client_history_filter_activie'];
	    
	    $db->setQuery("SELECT  id,
                			   production_id,
                			   title,
                			   create_date,
                			   prom_color,
                			   '' AS `moqmedeba`,
                			   block_date,
                			   '' AS `ar_vici`,
                			   '' AS `ar_vici1`,
                			   '' AS `ar_vici1`
                        FROM  `my_order`
                        WHERE  my_user_id = $history_user_id");
	    
	    $data = $db->getList($count, $hidden);
	    
	    break;
	case 'get_list_client_transaction' :
	    $count                                  = $_REQUEST['count'];
	    $hidden                                 = $_REQUEST['hidden'];
	    
	    $history_user_id                        = $_REQUEST['history_user_id'];
	    $incomming_call_id                      = $_REQUEST['incomming_call_id'];
	    $client_history_filter_site             = $_REQUEST['client_history_filter_site'];
	    $client_history_filter_group            = $_REQUEST['client_history_filter_group'];
	    $client_history_filter_transaction_type = $_REQUEST['client_history_filter_transaction_type'];
	    
	    $db->setQuery("SELECT  id,
                			   site_name,
                              `comment`,
                			  `name`,
                			  `product_id`,
                			  `money_before`,
                			  `money_after`,
                			  `quantity`,
                			  `expense`,
                			  `insert_date`
                        FROM  `my_transaction`
                        WHERE  my_user_id = $history_user_id");
	    
	    $data = $db->getList($count, $hidden);
	    
	    break;
	case 'get_user_log' :
	    $count	= $_REQUEST['count'];
	    $hidden	= $_REQUEST['hidden'];
	    $incomming_call_id = $_REQUEST['incomming_call_id'];
	    $db->setQuery("SELECT `logs`.id,
                			  `logs`.date,
                			   IF(`logs`.`event`=1,'რედაქტირება','დამატება'),
                			   user_info.`name`,
                			    CASE
                					WHEN collumn = 'phone' THEN	'ტელეფონი'
                					WHEN collumn = 'cat_1' THEN 'კატეგორია'
                					WHEN collumn = 'cat_1_1' THEN 'ქვე კატეგორია1'
                					WHEN collumn = 'cat_1_1_1' THEN 'ქვე კატეგორია2'
                					WHEN collumn = 'call_content' THEN 'საუბრის შინაარსი'
                					WHEN collumn = 'inc_status' THEN 'რეაგირება'
                					WHEN collumn = 'client_user_id' THEN 'User ID'
                					WHEN collumn = 'client_pid' THEN 'საიდ. კოდი / პ.ნ'
                					WHEN collumn = 'client_name' THEN 'სახელი,გვარი / დასახელება'
									WHEN collumn = 'client_birth' THEN 'დაბადების თარიღი'
                					WHEN collumn = 'client_mail' THEN 'ელ. ფოსტა'
                					WHEN collumn = 'client_satus' THEN 'სტატუსი'
                					WHEN collumn = 'client_sex' THEN 'სქესი'
									WHEN collumn = 'source' THEN 'ინფორმაციის წყარო'
									WHEN collumn = 'sites' THEN 'საიტი'
                				END AS `collumt`,
                			   `logs`.new_value,
                			   `logs`.old_value
                        FROM   `logs`
                        LEFT JOIN   user_info ON user_info.user_id = `logs`.user_id
                        WHERE  `table` = 'incomming_call' AND row_id = '$incomming_call_id' AND new_value != '' ");
	    
	    $data = $db->getList($count, $hidden);
	    
	    break;
	case 'get_list_news' :
	    $count        = $_REQUEST['count'];
	    $hidden       = $_REQUEST['hidden'];
	    
	    $db->setQuery(" SELECT action.id,
                    	       action.start_date,
                    	       action.end_date,
                    	       action.`name`,
                              (SELECT GROUP_CONCAT(my_web_site.`name`) 
                               FROM  `action_site`
                               JOIN   my_web_site ON action_site.site_id = my_web_site.id
                               WHERE  action_site.action_id = action.id) AS `site`,
                    	       action.content
            	        FROM   action
            	        WHERE  action.actived=1 AND action.end_date >= DATE(CURDATE())");
	    
	    $data = $db->getList($count, $hidden,1);
	    
	    break;
	case 'get_list_crm' :
	    $count        = $_REQUEST['count'];
	    $hidden       = $_REQUEST['hidden'];
	    
	    $start_crm  = $_REQUEST['start_crm'];
	    $end_crm    = $_REQUEST['end_crm'];
	    $phone      = $_REQUEST['phone'];
	    $pin        = $_REQUEST['pin'];
	    
	    $db->setQuery("SELECT 	   ar.id,
                                   ard.set_time,
                                   arb.number_8,
                                   arb.number_2,
            					   arb.number_3,
            					   arb.phone_number,
            					   arb.number_5,
                                   ar.comment_main,
                                   CONCAT(IFNULL(tr.`name`,''),IF(ISNULL(tr.`name`),'','/'),IFNULL(add_page_tree.`name`,'')) AS `status`
                        FROM 	   autocall_request ar 
                        left JOIN  autocall_request_details ard ON ar.id = ard.request_id
                        LEFT JOIN  autocall_request_base arb ON arb.request_id = ard.id
                        LEFT JOIN  add_page_tree ON arb.tree_id = add_page_tree.id
                        LEFT JOIN  add_page_tree AS `tr` ON add_page_tree.parent_id = tr.id
                        WHERE      ar.call_type_id = 4 AND ar.actived=1 AND ard.actived = 1 
						AND       ((arb.number_2 = '$pin' AND arb.number_2 != '')  OR (arb.phone_number = '$phone' AND arb.phone_number != ''))
                        AND        DATE(ard.set_time) BETWEEN '$start_crm' AND '$end_crm'");
	    
	    $data = $db->getList($count, $hidden);
	    
	    break;
	    
	case 'get_list_quest' :
	    $count = 		$_REQUEST['count'];
	    $hidden = 		$_REQUEST['hidden'];
	    
	    $source_id   = $_REQUEST['source_id'];
	    $site        = $_REQUEST['site'];
	    
	    $filter_site   = "";
	    $filter_source = "";
	    if ($site != 'null' && $site != '') {
	        $filter_site = "AND queries.id IN(SELECT   queries_site.queries_id
                                    	      FROM     queries_site
                                    	      WHERE    queries_site.site_id IN($site)
                                    	      GROUP BY queries_site.queries_id)";
	    }
	    
	    if ($source_id != 0) {
	        $filter_source = "AND queries.id IN(SELECT   queries_source.queries_id
                                    	        FROM     queries_source
                                    	        WHERE    queries_source.source_id IN($source_id)
                                    	        GROUP BY queries_source.queries_id)";
	    }
	    
	    
	    $db->setQuery(" SELECT 	queries.id,
                                queries.datetime,
                                user_info.`name`,
								queries.`quest`,
                                queries.`answer`
					    FROM 	queries
                        LEFT JOIN user_info ON user_info.user_id = queries.user_id
					    WHERE 	queries.actived=1 $filter_site $filter_source");
	    
	    $data = $db->getList($count, $hidden);
	    
	    break;
	    
	case 'get_list_mail':
	    $count             = $_REQUEST['count'];
	    $hidden            = $_REQUEST['hidden'];
	    $incomming_call_id = $_REQUEST['incomming_id'];
	    
	        $db->setQuery(" SELECT  id,
                    	            `date`,
                    	            CONCAT('to: ',`address`,'<br>cc: ',cc_address,'<br> bcc: ',bcc_address) as address,
                    	            `body`,
                    	            if(`status`=3,'გასაგზავნია',IF(`status`=2,'გაგზავნილია',''))
            	            FROM   `sent_mail`
            	            WHERE   incomming_call_id = '$incomming_call_id' AND `status` != 1");
	    
	    $data = $db->getList($count, $hidden);
	    break;
	case 'save_incomming':
	    $incom_id     = $_REQUEST['id'];
	    $id           = $_REQUEST['hidde_incomming_call_id'];
		$user		  = $_SESSION['USERID'];
		$hidden_user  = $_REQUEST['hidden_user'];
		
		if($id == ''){
		    Addincomming($incom_id);
		    
    	}else{
    	    Saveincomming($incom_id);
		}
		
		break;
	case 'move_incomming':
		$note = $_REQUEST['note'];

		note_incomming();

		break;
	case 'close_chat':
		$last_request_res = mysql_fetch_assoc(mysql_query(
			"SELECT `last_request_datetime`,
					`status`,
					TIME_TO_SEC(TIMEDIFF(NOW(),last_request_datetime)) as timedifference,
					who_rejected
				from chat 
				where `id` = '$_REQUEST[chat_id]';"));
		$timedifference = $last_request_res['timedifference'];
		$who_rejected = $last_request_res['who_rejected'];
		$query_addition ='';
		if($timedifference >= 10 && $who_rejected == ''){
			$query_addition .= ", `who_rejected` = 0";
		} else if ($timedifference < 10 && $who_rejected == ''){
			$query_addition .= ", `who_rejected` = 1";
		}
	    mysql_query("UPDATE `chat` SET
			        `status`='4'$query_addition
			        WHERE  `id`='$_REQUEST[chat_id]';");

	    break;
	case 'black_list_add':

		num_block_incomming();

		break;
	case 'num_check':
	    
		$phone = $_REQUEST['phone'];

		$check_num=mysql_num_rows(mysql_query("SELECT phone
											   FROM black_list
											   WHERE phone='$phone' AND actived=1
											   LIMIT 1"));
		if ($check_num==0 && $phone != '2004') {
			$check=1;
		}else {
			$check=0;
		}
	$data = array('check' => $check);

		break;
		
	case 'black_list_add_chat':
	    $user              = $_SESSION['USERID'];
	    $source_id         = $_REQUEST['source_id'];
	    $chat_block_reason = $_REQUEST['chat_block_reason'];
	    $id                = $_REQUEST['id'];
	    
	    if($source_id == 6){
	        $db->setQuery("SELECT fb_chat_id FROM incomming_call WHERE id = '$id'");
	        $fb = $db->getResultArray();
	        $db->setQuery("SELECT sender_id, sender_name FROM fb_chat WHERE id = '$fb[result][0][fb_chat_id]'");
	        $sender = $db->getResultArray();
	        $db->setQuery("SELECT id FROM `fb_chat_black_list` WHERE sender_id = '$sender[result][0][sender_id]' AND actived = 1");
	        $check_block = $db->getResultArray();
	        if($check_block==0){
	            mysql_query("INSERT INTO `fb_chat_black_list` 
                                        (`user_id`, `datetime`, `sender_id`, `name`, `reason`) 
                                  VALUES 
	                                    ('$user', NOW(), '$sender[result][0][sender_id]', '$sender[result][0][sender_name]', '$chat_block_reason');");
	            
	            mysql_query("UPDATE fb_chat SET status = 3 WHERE id = $fb[result][0][fb_chat_id]");
	        }else{
	            global $error;
	            $error = 'მომხმარებელი უკვე დაბლოკილია';
	        }
	    }else if($source_id == 7){
	        $db->setQuery("SELECT mail_chat_id FROM incomming_call WHERE id = '$id'");
	        $mail = $db->getResultArray();
	        $db->setQuery("SELECT sender_id FROM mail_chat WHERE id = '$mail[result][0][mail_chat_id]'");
	        $sender = $db->getResultArray();
	        $db->setQuery("SELECT id FROM `mail_chat_black_list` WHERE sender_id = '$sender[result][0][sender_id]' AND actived = 1");
	        $check_block = $db->getNumRow();
	        if($check_block==0){
	            mysql_query("INSERT INTO `mail_chat_black_list`
                    	                (`user_id`, `datetime`, `sender_id`, `reason`)
                    	          VALUES
	                                    ('$user', NOW(), '$sender[0][sender_id][sender_id]', '$chat_block_reason');");
	            
	            mysql_query("UPDATE mail_chat SET status = 3 WHERE id = $mail[0][sender_id][mail_chat_id]");
	        }else{
	            global $error;
	            $error = 'მომხმარებელი უკვე დაბლოკილია';
	        }
	    }else if($source_id == 9){
	        $db->setQuery("SELECT site_chat_id FROM incomming_call WHERE id = '$id'");
	        $site = $db->getResultArray();
	        $db->setQuery("SELECT sender_id, sender_name FROM site_chat WHERE id = '$site[result][0][site_chat_id]'");
	        $sender = $db->getResultArray();
	        $db->setQuery("SELECT id FROM `site_chat_black_list` WHERE sender_id = '$sender[result][0][sender_id]' AND actived = 1");
	        $check_block = $db->getNumRow();
	        if($check_block==0){
	            $db->setQuery("INSERT INTO `site_chat_black_list`
                    	                (`user_id`, `datetime`, `sender_id`, `name`, `reason`)
                    	          VALUES
	                                    ('$user', NOW(), '$sender[result][0][sender_id]', '$sender[result][0][sender_name]', '$chat_block_reason');");
	            $db->execQuery();
	            $db->setQuery("UPDATE site_chat SET status = 3 WHERE id = $site[result][0][site_chat_id]");
	            $db->execQuery();
	        }else{
	            global $error;
	            $error = 'მომხმარებელი უკვე დაბლოკილია';
	        }
	    }
	    
	    $data = array('check' => $check);
	    
	    break;
	case 'ipaddr_check':

// 	    $ipaddres		= $_REQUEST['ipaddres'];
// 	    $ipaddr_block_comment = $_REQUEST['ipaddr_block_comment'];
// 	    mysql_close();
// 	    mysql_connect('212.72.155.176','root','Gl-1114');
// 	    mysql_select_db('webcall');
// 	    $check_num=mysql_num_rows(mysql_query(" SELECT phone
//                                     	        FROM blocked_ip
//                                     	        WHERE ip='$ipaddres'
//                                     	        LIMIT 1"));
// 	    if ($check_num==0 && $ipaddres != '0') {
// 	        mysql_query("INSERT INTO `blocked_ip`
// 	                     (`clients_id`, `ip`, `comment`, `status`)
//                          VALUES
//                          ('7', '$ipaddres', '$ipaddr_block_comment', '1');");
// 	        $check=1;
// 	    }else {
// 	        $check=0;
// 	    }
// 	    $data = array('check' => $check);

	    break;

	case 'delete_file':
	    $delete_id = $_REQUEST['delete_id'];
	    $edit_id   = $_REQUEST['edit_id'];
	    
		$db->setQuery("DELETE FROM file WHERE id = $delete_id");
		$db->execQuery();
		$db->setQuery("SELECT  `name`,
							   `rand_name`,
							   `id`,
                               `file_date`
					   FROM    `file`
					   WHERE   `incomming_call_id` = $edit_id");
		
		$increm = $db->getResultArray();
		$data1 = '';

		foreach($increm[result] AS $increm_row)	{
		    $data1 .='<tr style="border-bottom: 1px solid #CCC;">
                        <td style="border-right: 1px solid #CCC;height: 20px;vertical-align: middle;word-wrap:break-word;">'.$increm_row[file_date].'</td>
        				<td style="border-left: 1px solid #CCC;height: 20px;vertical-align: middle;word-wrap:break-word;">'.$increm_row[name].'</td>
        				<td style="border-left: 1px solid #CCC;height: 20px;vertical-align: middle;"><button type="button" value="https://crm.my.ge/media/uploads/file/'.$increm_row[rand_name].'" style="cursor:pointer; border:none; height:16px; width:16px; background:none;background-image:url(\'media/images/get.png\');" id="download" ></button><input type="text" style="display:none;" id="download_name" value="'.$increm_row[rand_name].'"> </td>
        	          	<td style="height: 20px;vertical-align: middle;"><button type="button" value="'.$increm_row[id].'" style="cursor:pointer; border:none;  height:16px; width:16px; background:none; background-image:url(\'media/images/x.png\');" id="delete"></button></td>
        			  </tr>';
		}

		$data = array('page' => $data1);

		break;

	case 'up_now':
		$user    = $_SESSION['USERID'];
		$edit_id = $_REQUEST['edit_id'];

		if($rand_file != ''){
			$db->setQuery("INSERT INTO 	`file`
                    				   (`user_id`, `file_date`, `incomming_call_id`, `name`, `rand_name`)
                    			VALUES
                    				   ('$user',NOW(), '$edit_id', '$file', '$rand_file');");
			$db->execQuery();
		}

		$db->setQuery("	SELECT  `name`,
								`rand_name`,
								`id`,
                                `file_date`
						FROM 	`file`
						WHERE   `incomming_call_id` = $edit_id");
		
		$increm = $db->getResultArray();
		$data1 = '';

		foreach($increm[result] AS $increm_row)	{
           $data1 .='<tr style="border-bottom: 1px solid #CCC;">
                        <td style="border-right: 1px solid #CCC;height: 20px;vertical-align: middle;word-wrap:break-word;">'.$increm_row[file_date].'</td>
        				<td style="border-left: 1px solid #CCC;height: 20px;vertical-align: middle;word-wrap:break-word;">'.$increm_row[name].'</td>
        				<td style="border-left: 1px solid #CCC;height: 20px;vertical-align: middle;"><button type="button" value="https://crm.my.ge/media/uploads/file/'.$increm_row[rand_name].'" style="cursor:pointer; border:none; height:16px; width:16px; background:none;background-image:url(\'media/images/get.png\');" id="download" ></button><input type="text" style="display:none;" id="download_name" value="'.$increm_row[rand_name].'"> </td>
        	          	<td style="height: 20px;vertical-align: middle;"><button type="button" value="'.$increm_row[id].'" style="cursor:pointer; border:none;  height:16px; width:16px; background:none; background-image:url(\'media/images/x.png\');" id="delete"></button></td>
        			  </tr>';
		}
		
		$data = array('page' => $data1);

		break;

	case 'cat_2':
	    $page		= get_cat_1_1($_REQUEST['cat_id'],'');
	    $data		= array('page'	=> $page);
	    
	    break;
	case 'cat_3':
	    $page		= get_cat_1_1_1($_REQUEST['cat_id'],'');
	    $data		= array('page'	=> $page);
	    
	case 'block_ip':
	    if($_REQUEST[ip] != ''){
	    mysql_query("INSERT INTO `blocked`
                    (`user_id`, `datetime`,`ip`,`comment`)
                    VALUES
					('$_SESSION[USERID]',NOW(),'$_REQUEST[ip]','$_REQUEST[chat_comment]');");
		$last_request_res = mysql_fetch_assoc(mysql_query(
			"SELECT `last_request_datetime`,
					`status`,
					TIME_TO_SEC(TIMEDIFF(NOW(),last_request_datetime)) as timedifference,
					who_rejected
				from chat 
				where `id` = '$_REQUEST[chat_id]';"));
		$timedifference = $last_request_res['timedifference'];
		$who_rejected = $last_request_res['who_rejected'];
		$query_addition ='';
		if($timedifference >= 10 && $who_rejected == ''){
			$query_addition .= ", `who_rejected` = 0";
		} else if ($timedifference < 10 && $who_rejected == ''){
			$query_addition .= ", `who_rejected` = 1";
		}
	    mysql_query("UPDATE `chat` SET
        	                `status`='3'$query_addition
        	         WHERE  `id`='$_REQUEST[chat_id]';");
	    }else{
	        $error = 'IP მისამართი არასწორია!';
	    }
		break;
	case 'block_pin':
	    if($_REQUEST[pin] != ''){
	    mysql_query("INSERT INTO `blocked`
                    (`user_id`, `datetime`,`pin`,`comment`)
                    VALUES
					('$_SESSION[USERID]',NOW(),'$_REQUEST[pin]','$_REQUEST[chat_comment]');");
		$last_request_res = mysql_fetch_assoc(mysql_query(
			"SELECT `last_request_datetime`,
					`status`,
					TIME_TO_SEC(TIMEDIFF(NOW(),last_request_datetime)) as timedifference,
					who_rejected
				from chat 
				where `id` = '$_REQUEST[chat_id]';"));
		$timedifference = $last_request_res['timedifference'];
		$who_rejected = $last_request_res['who_rejected'];
		$query_addition ='';
		if($timedifference >= 10 && $who_rejected == ''){
			$query_addition .= ", `who_rejected` = 0";
		} else if ($timedifference < 10 && $who_rejected == ''){
			$query_addition .= ", `who_rejected` = 1";
		}
	    mysql_query("UPDATE `chat` SET
        	                `status`='3'$query_addition
        	         WHERE  `id`='$_REQUEST[chat_id]';");
	    }else{
	        $error = 'IP მისამართი არასწორია!';
	    }
	    break;
    case 'chat_end':
        if($_REQUEST[chat_id] != '' && $_REQUEST[chat_id] != '0'){
			$last_request_res = mysql_fetch_assoc(mysql_query(
				"SELECT `last_request_datetime`,
						`status`,
						TIME_TO_SEC(TIMEDIFF(NOW(),last_request_datetime)) as timedifference,
						who_rejected
					from chat 
					where `id` = '$_REQUEST[chat_id]';"));
			$timedifference = $last_request_res['timedifference'];
			$who_rejected = $last_request_res['who_rejected'];
			$query_addition ='';
			if($timedifference >= 10 && $who_rejected == ''){
				$query_addition .= ", `who_rejected` = 0";
			} else if ($timedifference < 10 && $who_rejected == ''){
				$query_addition .= ", `who_rejected` = 1";
			}
            mysql_query("UPDATE `chat` SET
                                `status`='3'$query_addition
                         WHERE  `id`='$_REQUEST[chat_id]'");
        }else{
            $error = 'შეცდომა: ჩატი ვერ დაიხურა!';
        }
        break;
        
    case 'save_comment'  :
        $comment            = $_REQUEST['comment'];
        $incomming_call_id  = $_REQUEST['incomming_call_id'];
        $user_id	        = $_SESSION['USERID'];
        $db->setQuery(" INSERT INTO `chat_comment`
                                   (`user_id`, `incomming_call_id`, `parent_id`, `comment`, `datetime`, `active`)
                             VALUES
                                   ('$user_id', '$incomming_call_id', '0', '$comment', NOW(),'1');");
        $db->execQuery();
        
        break;
        
    case 'update_comment' :
        $comment = $_REQUEST['comment'];
        $id      = $_REQUEST['id'];
        
        $db->setQuery("UPDATE `chat_comment`
                          SET `comment` = '$comment'
                       WHERE  `id`      = $id");
        $db->execQuery();
        break;
        
    case 'delete_comment'  :
        
        $comment_id=$_REQUEST['comment_id'];
        
        $db->setQuery("UPDATE `chat_comment`
                          SET `active` = 0
                       WHERE  `id`     = $comment_id");
        $db->execQuery();
        break;
        
    case 'get_add_question':
        $id   = $_REQUEST['incomming_call_id'];
        $page = Getquestion($id);
        $data = array('page'	=> $page);
        break;
        
    case 'get_list_sms':
        $count        = $_REQUEST['count'];
        $hidden       = $_REQUEST['hidden'];
        $incomming_id = $_REQUEST['incomming_id'];
        
        if ($incomming_id != '') {
            $db->setQuery(" SELECT id,
                                   date,
                                   phone,
                                  `content`,
                                   if(`status`=1,'გასაგზავნია',IF(`status`=2,'გაგზავნილია',''))
                            FROM  `sent_sms`
                            WHERE  incomming_call_id = '$incomming_id'");
            
            $data = $db->getList($count, $hidden);
        }else{
            $data = '';
        }
        break;
        
	case 'get_all_chat':
    	if($_REQUEST['source']=="mail"){
    		$db->setQuery("SELECT  	IF(m.user_id='0','აუტომოპასუხე',(IFNULL(u.name,IF(c.sender_name='', c.sender_id, c.sender_name)))) as name,
                					'',
                					'',
                					'',
                					m.text AS `message_client`,
                					m.text AS `message_operator`,
                					m.datetime AS message_datetime,
                					'',
                					m.id,
									c.sender_id,
                					'',
                                    m.subject AS `subject`,
                                    IFNULL(m.user_id,0) AS `check`
                		FROM mail_messages as m
                		JOIN mail_chat as c on m.mail_chat_id=c.id
                		LEFT JOIN user_info as u on m.user_id=u.user_id
                		WHERE m.mail_chat_id =  $_REQUEST[chat_id]
                		ORDER BY m.id ASC");
    		
    	}elseif($_REQUEST['source']=="fbm"){
    	    $db->setQuery(" SELECT    IF(m.user_id='0','აუტომოპასუხე',(IFNULL(u.name,c.sender_name))) as name,
    								  '',
    								  '',
    								  '',
    								  m.text AS `message_client`,
    								  m.text AS `message_operator`,
    								  m.datetime AS message_datetime,
    								  '',
    								  m.id,
    								  '',
    								 (SELECT url from fb_attachments WHERE messages_id=m.id LIMIT 1) as media,
                                      IFNULL(m.user_id,0) AS `check`,
                                      (SELECT type from fb_attachments WHERE messages_id=m.id LIMIT 1) as media_type
    					    FROM      fb_messages as m
    						JOIN      fb_chat as c on m.fb_chat_id=c.id
    						LEFT JOIN user_info as u on m.user_id=u.user_id
    						WHERE     m.fb_chat_id =  $_REQUEST[chat_id]
    						ORDER BY  m.id ASC");
    		
    	}elseif($_REQUEST['source']=="site"){		
     	    $db->setQuery("SELECT    IF(m.user_id='0','აუტომოპასუხე',(IFNULL(u.name,c.sender_name))) as name,
    								  '',
    								  '',
    								  '',
    								  m.text AS `message_client`,
    								  m.text AS `message_operator`,
    								  m.datetime AS message_datetime,
    								  '',
    								  m.id,
    								  '',
    								 m.media as media,
                                      IFNULL(m.user_id,0) AS `check`
    					    FROM      site_messages as m
    						JOIN      site_chat as c on m.site_chat_id=c.id
    						LEFT JOIN user_info as u on m.user_id=u.user_id
    						WHERE     m.site_chat_id =  $_REQUEST[chat_id]
    						ORDER BY  m.id ASC");
    	}elseif($_REQUEST['source']=="video"){
    	    $res = $db->setQuery("");
    	}else{
    	    $db->setQuery("UPDATE `chat` 
                              SET `answer_date`    = NOW(),
                                  `answer_user_id` = '$_SESSION[USERID]'
                           WHERE  `id`             = '$_REQUEST[chat_id]'
                           AND     status          = 1");
    	    
    	    $db->execQuery();
            $name           = '';
            $os             = '';
            $bro            = '';
            $ip             = '';
            $sms            = '';
            $chat_detail_id = 0;
            
            $db->setQuery(" SELECT     chat.`name`,
                                       chat.`device`,
                                       chat.`browser`,
                                       chat.`ip`,
                                       chat_details.message_client,
                                       chat_details.message_operator,
                                       chat_details.message_datetime,
                                       IF(ISNULL(chat_nikname.`name`),'საიტის ოპერატორი',chat_nikname.name) AS `op_name`,
                                       chat_details.id,
    								   chat.pin
                            FROM      `chat`
                            LEFT JOIN `chat_details` ON chat.id = chat_details.chat_id AND chat_details.message_operator != 'daixura'
                            LEFT JOIN `chat_nikname` ON chat_nikname.crystal_users_id = chat_details.operator_user_id AND chat_nikname.actived = 1
    						WHERE     `chat`.id = $_REQUEST[chat_id]
                            ORDER BY   chat_details.message_datetime ASC");
    	}
    	
    	$res = $db->getResultArray();
    	
    	foreach($res[result] AS $req){
            $subject = '';
    		if($_REQUEST['source']=="mail"){
    		    $subject_text = str_replace('Re:','',$req[subject]);
    		    if ($subject_text!=''){
    		        $subject = 'თემა: '.$subject_text;
    		    }
    		    $req['message_client'] = preg_replace('/<base[^>]+href[^>]+>/', '', $req[message_client]);
    			$db->setQuery("SELECT `name`, SUBSTRING_INDEX(patch,  'htdocs/',-1) AS `patch`, `check_mesage` FROM `mail_attachments` WHERE messages_id=$req[id]");
    			$fq = $db->getResultArray();
    			foreach($fq[result] AS $file){
    			    if ($file[check_mesage] == 1) {
    			        $req['message_operator'] = $req[message_operator]. "<a href='$file[patch]' target='_blank'>$file[name]</a><br>";
    			    }else{
        				$file[patch] = str_replace('/var/www/html/','',$file[patch]);	
        				$req['message_client'] = preg_replace('/(?<=cid:'.$file[name].').*?(?=>)/', $file[patch].'"',  $req['message_client']);//preg_grep('/(?<=cid:).*?(?=@)/', array($req['4'],'123'));
        				$req[message_client] = str_replace('cid:'.$file[name], 'https://crm.my.ge/', $req['message_client']);							
        				$req[message_client] = $req[message_client]. "<a href='https://crm.my.ge/$file[patch]' target='_blank'>$file[name]</a><br>";
    			    }
    				
    			} 
    		}
    		
    		$gif_url = preg_grep('/(?<=http).*?(?=.gif)/', array($req[message_client],'123'));
    		if($gif_url[0])
    		    $req[message_client] = "<a href='$gif_url[0]' target='_blank'><img style='border-radius: 10%;max-height: 200px;' src='$gif_url[0]'></a>";
    
    		    if ($req[media_type] == 'fallback') {
    		        if(!empty($req['media'])) $req[message_client] = $req[message_client]. "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='https://crm.my.ge/media/images/icons/file_icons.png'></a>";
    		    }elseif ($req[media_type] == 'file'){
    		        if(!empty($req['media'])) $req[message_client] = $req[message_client]. "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='https://crm.my.ge/media/images/icons/file_icons.png'></a>";
    		    }elseif ($req[media_type] == 'video'){
    		        if(!empty($req['media'])) $req[message_client] = $req[message_client]. "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='https://crm.my.ge/media/images/icons/file_icons.png'></a>";
    		    }else{
    		        if(!empty($req['media'])) $req[message_client] = $req[message_client]. "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 200px;' src='$req[media]'></a>";
    		    }
    			
    			
			$name=$req[name];
			$email = $req[sender_id];

    		if($name=='') $name='უცნობი';
    		$os  = $req[device];
    		$bro = $req[browser];
    		$ip  = $req[ip];
    		$pin = $req[pin];
    			
    			
            $color='color:#2681DC;';
    		if ($req[check] > 0 && $_REQUEST['source'] != 'chat') {
    		    $color = '';
    		}
    		if($req[message_client]=='' && $_REQUEST['source'] == 'chat'){
    		    $sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                                <div style="float: right!important; background: rgba(80, 181, 0, 0.24) none repeat scroll 0 0; border-radius: 15px;padding: 7px 15px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word;box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);">
                    	            <p style="text-align: right; font-weight: bold; font-size: 14px;">'.$req[op_name].'</p>
                                    <p style="overflow-x: auto; padding-top: 7px; line-height: 20x; font-size: 14px;">'.$req[message_operator].'</p>
                                    <div style="text-align: right; padding-top: 7px;">
                                        <p style="color: #878787; width: 100%;">'.$req[message_datetime].'</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
            }else{
                if ($_REQUEST['source'] == 'chat') {
                    $sms .= '<tr style="height: 17px;">
                	            <td colspan="2" style=" word-break: break-word;">
                                    <div style="background: rgba(33, 33, 33, 0.04) none repeat scroll 0 0;border-radius: 15px;padding: 7px 15px;color: #212121; max-width: auto; max-width: 85%; float: left; overflow-wrap: break-word;box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);">
                        	            <p style="font-weight: bold;font-size: 14px;">'.$name.'</p>
                                        <p style="overflow-x: auto; padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 14px;">'.$req[message_client].'</p>
                                        <div>
                                            <p style="width: 100%; color: #878787;">'.$req[message_datetime].'</p>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr style="height:10px;"></tr>';
                }else{
                    if ($req[check] > 0) {
                        $sms .= '<tr style="height: 17px;">
                    	            <td colspan="2" style=" word-break: break-word;">
                                        <div style="float: right!important; background: rgba(80, 181, 0, 0.24) none repeat scroll 0 0; border-radius: 15px;padding: 7px 15px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word;box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);">
                            	            <p style="text-align: right; font-weight: bold; font-size: 14px;">'.$name.'</p>
                                            <p style="overflow-x: auto; padding-top: 7px; line-height: 20x; font-size: 14px;">'.$req[message_operator].'</p>
                                            <div style="text-align: right; padding-top: 7px;">
                                                <p style="color: #878787; width: 100%;">'.$req[message_datetime].'</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height:10px;"></tr>';
                    }else{
                        $sms .= '<tr style="height: 17px;">
                    	            <td colspan="2" style=" word-break: break-word;">
                                        <div style="background: rgba(33, 33, 33, 0.04) none repeat scroll 0 0;border-radius: 15px;padding: 7px 15px;color: #212121; max-width: auto; max-width: 85%; float: left; overflow-wrap: break-word;box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);">
                            	            <p style="font-weight: bold;font-size: 14px;">'.$name.'</p>
											<p style="overflow-x: auto; font-weight: bold; font-size: 14px;">'.$subject.'</p>
											<p style="overflow-x: auto; font-weight: 100; font-size: 13px;">'.$email.'</p>
                                            <p style="overflow-x: auto; padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 14px;">'.$req[message_client].'</p>
                                            <div>
                                                <p style="width: 100%; color: #878787;">'.$req[message_datetime].'</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height:10px;"></tr>';
                    }
                }
            }
        }
        
		$sms =  str_replace('https://e.mail.ru/', '',  $sms);
		$sms =  str_replace('https://touch.mail.ru/cgi-bin/', '',  $sms);
		
		if($_REQUEST['source']=="fbm"){
			$db->setQuery("SELECT `id` FROM `fb_messages` WHERE  `fb_chat_id` = '$_REQUEST[chat_id]' ORDER BY id DESC LIMIT 1");
			
			$ge = $db->getResultArray();
			$chat_detail_id = $ge[result][0][id];
		}elseif($_REQUEST['source']=="site"){
		    $db->setQuery("SELECT `id` FROM `site_messages` WHERE  `site_chat_id` = '$_REQUEST[chat_id]' ORDER BY id DESC LIMIT 1");
			
		    $ge = $db->getResultArray();
		    $chat_detail_id = $ge[result][0][id];
		}elseif($_REQUEST['source']=="mail"){
		    $db->setQuery("SELECT `id` FROM `mail_messages` WHERE  `mail_chat_id` = '$_REQUEST[chat_id]' ORDER BY id DESC LIMIT 1");
			
		    $ge = $db->getResultArray();
		    $chat_detail_id = $ge[result][0][id];
		}elseif($_REQUEST['source']=="video"){
		    $chat_detail_id = 0;
		}else{
		    $db->setQuery("SELECT `chat_details`.`id`
                           FROM   `chat_details`
                           WHERE  `chat_details`.`chat_id` = '$_REQUEST[chat_id]' AND chat_details.operator_user_id != '$_SESSION[USERID]' ORDER BY chat_details.id DESC LIMIT 1");
		    
		    $ge = $db->getResultArray();
		    $chat_detail_id = $ge[result][0][id];
		}
        $data = array('name' => $name, 'os' => $os, 'bro' => $bro, 'pin' => $pin, 'ip' => $ip, 'sms' => $sms, 'chat_detail_id'=>$chat_detail_id);
        break;
	case 'get_history':
		if($_REQUEST['source']=='site'){
// 			$res = mysql_query("SELECT   `text`,
// 										  IFNULL(u.`name`,`sender_name`) as name,
// 										  m.datetime as 'datetime',
// 										  m.media,
//                                           m.user_id,
//                                           c.first_datetime,
//                                           c.id AS `cat_id`
// 								FROM     `site_chat` as c
// 								JOIN     `site_messages` as m ON m.site_chat_id=c.id
// 								LEFT JOIN crystal_users as u on u.id=m.user_id 
// 								WHERE     c.sender_id in (SELECT sender_id FROM site_chat WHERE id='".$_REQUEST['chat_id']."') AND `status` in (1,2,3)");
// 			$imID = 0;
// 			while ($req = mysql_fetch_array($res)){
// 				if(!empty($ge['media'])) $ge[text] = $ge[text]. "<a href='$ge[media]' target='_blank'><img style='border-radius: 10%;max-height: 200px;' src='$ge[media]'></a>";
// 				if($imID != $req[cat_id]){
// 				    $sms .= '<tr style="height: 20px;"><td colspan=2 style="font-weight:bold;text-align: center;">------'.$req[first_datetime].'-------</td></tr>';
// 				}
// 				if($req['user_id']==''){
// 				    $sms .= '<tr style="height: 17px;">
//                     	            <td colspan="2" style=" word-break: break-word;">
//                                         <div style="background: rgba(33, 33, 33, 0.04) none repeat scroll 0 0;border-radius: 15px;padding: 7px 15px;color: #212121; max-width: auto; max-width: 85%; float: left; overflow-wrap: break-word;box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);">
//                             	            <p style="font-weight: bold;font-size: 14px;">'.$req[name].'</p>
//                                             <p style="padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 14px;">'.$req[text].'</p>
//                                             <div>
//                                                 <p style="width: 100%; color: #878787;">'.$req['datetime'].'</p>
//                                             </div>
//                                         </div>
//                                     </td>
//                                 </tr>
//                                 <tr style="height:10px;"></tr>';
// 				}else{
// 				    $sms .= '<tr style="height: 17px;">
//                     	            <td colspan="2" style=" word-break: break-word;">
//                                         <div style="float: right!important; background: rgba(80, 181, 0, 0.24) none repeat scroll 0 0; border-radius: 15px;padding: 7px 15px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word;box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);">
//                             	            <p style="text-align: right; font-weight: bold; font-size: 14px;">'.$req[name].'</p>
//                                             <p style="padding-top: 7px; line-height: 20x; font-size: 14px;">'.$req[text].'</p>
//                                             <div style="text-align: right; padding-top: 7px;">
//                                                 <p style="color: #878787; width: 100%;">'.$req['datetime'].'</p>
//                                             </div>
//                                         </div>
//                                     </td>
//                                 </tr>
//                                 <tr style="height:10px;"></tr>';
// 				}
// 				$imID = $req[cat_id];
// 			}
// 			$data = array('name' => $name, 'sms' => $sms);
		}elseif ($_REQUEST['source']=='fbm'){
		    $db->setQuery(" SELECT   `text`,
    								  IFNULL(u.`name`,`sender_name`) as name,
    								  m.datetime as 'datetime',
    								  (SELECT url from fb_attachments WHERE messages_id=m.id LIMIT 1) AS `media`,
                                      m.user_id,
                                      c.first_datetime,
                                      c.id AS `cat_id`,
                                      (SELECT type from fb_attachments WHERE messages_id=m.id LIMIT 1) as media_type
    						FROM     `fb_chat` as c
    						JOIN     `fb_messages` as m ON m.fb_chat_id=c.id
    						LEFT JOIN user_info as u on m.user_id=u.user_id
    						WHERE     c.sender_id in (SELECT sender_id FROM fb_chat WHERE id='".$_REQUEST['chat_id']."') AND `status` in (1,2,3)
                            ORDER BY m.datetime ASC");
		    $imID = 0;
		    $res = $db->getResultArray();
		    foreach($res[result] AS $req){
		        if ($req[media_type] == 'fallback') {
		            if(!empty($req['media'])) $req[text] = $req[text]. "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='https://crm.my.ge/media/images/icons/file_icons.png'></a>";
		        }elseif ($req[media_type] == 'file'){
		            if(!empty($req['media'])) $req[text] = $req[text]. "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='https://crm.my.ge/media/images/icons/file_icons.png'></a>";
		        }elseif ($req[media_type] == 'video'){
		            if(!empty($req['media'])) $req[text] = $req[text]. "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='https://crm.my.ge/media/images/icons/file_icons.png'></a>";
		        }else{
		            if(!empty($req['media'])) $req[text] = $req[text]. "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 200px;' src='$req[media]'></a>";
		        }
		        if($imID != $req[cat_id]){
		            $sms .= '<tr style="height: 20px;"><td colspan=2 style="font-weight:bold;text-align: center;">------'.$req[first_datetime].'-------</td></tr>';
		        }
		        
		        if ($req[user_id]=='') {
		            $sms .= '<tr style="height: 17px;">
                    	            <td colspan="2" style=" word-break: break-word;">
                                        <div style="background: rgba(33, 33, 33, 0.04) none repeat scroll 0 0;border-radius: 15px;padding: 7px 15px;color: #212121; max-width: auto; max-width: 85%; float: left; overflow-wrap: break-word;box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);">
                            	            <p style="font-weight: bold;font-size: 14px;">'.$req[name].'</p>
                                            <p style="overflow-x: auto; padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 14px;">'.$req[text].'</p>
                                            <div>
                                                <p style="width: 100%; color: #878787;">'.$req['datetime'].'</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height:10px;"></tr>';
		        }else{
		            $sms .= '<tr style="height: 17px;">
                    	            <td colspan="2" style=" word-break: break-word;">
                                        <div style="float: right!important; background: rgba(80, 181, 0, 0.24) none repeat scroll 0 0; border-radius: 15px;padding: 7px 15px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word;box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);">
                            	            <p style="text-align: right; font-weight: bold; font-size: 14px;">'.$req[name].'</p>
                                            <p style="overflow-x: auto; padding-top: 7px; line-height: 20x; font-size: 14px;">'.$req[text].'</p>
                                            <div style="text-align: right; padding-top: 7px;">
                                                <p style="color: #878787; width: 100%;">'.$req['datetime'].'</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height:10px;"></tr>';
		          //$sms .= '<tr style="height: 17px;"><td colspan=2 style="font-weight:bold;color:#2681DC; word-break: break-word;">'.$req['name'].'</td></tr><tr style="height: 17px;"><td colspan=2 >'.$req[text].'</td></tr><tr><td colspan=2 style="border-bottom:2px dotted #DBDBDB;text-align: right;"><p>'.$req[time].'</p><time class="timeago" datetime="'.$req[time].'"></time></td></tr>';
		        }
		        
		        $imID = $req[cat_id];
		    }
		    $data = array('name' => $name, 'sms' => $sms);
		}elseif($_REQUEST['source']=="mail"){
		    $db->setQuery("SELECT  	IF(m.user_id='0','აუტომოპასუხე',(IFNULL(u.name, IF(c.sender_name='', c.sender_id, c.sender_name)))) as name,
                        		        m.text,
                        		        m.text,
                        		        m.datetime,
                        		        m.id,
                        		        m.subject AS `subject`,
                                        m.user_id,
                                        c.first_datetime,
										c.sender_id AS `sender_id`,
                                        c.id AS `cat_id`
            		        FROM        mail_messages as m
            		        JOIN        mail_chat as c on m.mail_chat_id=c.id
            		        LEFT JOIN   user_info as u on m.user_id=u.user_id
            		        WHERE       c.sender_id = (SELECT mail_chat.sender_id FROM mail_chat WHERE id='".$_REQUEST['chat_id']."') AND `status` in (1,2,3)
                            ORDER BY m.datetime ASC");
		    $imID = 0;
		    $res = $db->getResultArray();
		    foreach($res[result] AS $req){
		        $name=$req[0];
		        $subject_text = str_replace('Re:','',$req[subject]);
	            if ($subject_text!='') {
	                $subject = 'თემა: '.$subject_text;
	            }
		            
		        $db->setQuery("SELECT `name`, SUBSTRING_INDEX(patch,  'htdocs/',-1) AS `patch`, `check_mesage` FROM `mail_attachments` WHERE messages_id=$req[id]");
		        $fq = $db->getResultArray();
		        foreach($fq[result] AS $file){
		            if ($file[check_mesage] == 1) {
		                $req['text']      = $req[text]. "<a href='$file[patch]' target='_blank'>$file[name]</a><br>";
		            }else{
    	                $file[patch] = str_replace('/var/www/html/','',$file[patch]);
    	                $req['text']    = preg_replace('/(?<=cid:'.$file[name].').*?(?=>)/', $file[patch].'"',  $req['text']);//preg_grep('/(?<=cid:).*?(?=@)/', array($req['4'],'123'));
    	                $req['text']      = str_replace('cid:'.$file[name], 'https://crm.my.ge/', $req['text']);
    	                $req['text']      = $req[text]. "<a href='https://crm.my.ge/$file[patch]' target='_blank'>$file[name]</a><br>";
		            }
	            }
	            if($imID != $req[cat_id]){
	                $sms .= '<tr style="height: 20px;"><td colspan=2 style="font-weight:bold;text-align: center;">------'.$req[first_datetime].'-------</td></tr>';
	            }
		        if ($req[user_id] == '') {
		            $sms .= '<tr style="height: 17px;">
                    	            <td colspan="2" style=" word-break: break-word;">
                                        <div style="background: rgba(33, 33, 33, 0.04) none repeat scroll 0 0;border-radius: 15px;padding: 7px 15px;color: #212121; max-width: auto; max-width: 85%; float: left; overflow-wrap: break-word;box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);">
                            	            <p style="font-weight: bold;font-size: 14px;">'.$req[name].'</p>
                                            <p style="overflow-x: auto; font-weight: bold;font-size: 14px;">'.$subject.'</p>
                                            
											
											<p style="overflow-x: auto; padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 14px;">'.$req[text].'</p>
                                            <div>
                                                <p style="width: 100%; color: #878787;">'.$req['datetime'].'</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height:10px;"></tr>';
		        }else{
		            $sms .= '<tr style="height: 17px;">
                    	            <td colspan="2" style=" word-break: break-word;">
                                        <div style="float: right!important; background: rgba(80, 181, 0, 0.24) none repeat scroll 0 0; border-radius: 15px;padding: 7px 15px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word;box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);">
                            	            <p style="text-align: right; font-weight: bold; font-size: 14px;">'.$req[name].'</p>
                                            <p style="overflow-x: auto; padding-top: 7px; line-height: 20x; font-size: 14px;">'.$req[text].'</p>
                                            <div style="text-align: right; padding-top: 7px;">
                                                <p style="color: #878787; width: 100%;">'.$req['datetime'].'</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height:10px;"></tr>';
		        }
		        $imID = $req[cat_id];
	            //$sms .= '<tr style="height: 17px;"><td colspan=2 style="font-weight:bold;color:#2681DC; word-break: break-word;">'.$name.'</td></tr>'.$subject.'<tr style="height: 17px;"><td colspan=2 style=" word-break: break-word;">'.$req[1].'</td></tr><tr><td colspan=2 style="border-bottom:2px dotted #DBDBDB;text-align: right;"><p>'.$req[3].'</p><time class="timeago" datetime="'.$req[3].'"></time></td></tr>';
	        }
	        
	        $sms =  str_replace('https://e.mail.ru/', '',  $sms);
	        $sms =  str_replace('https://touch.mail.ru/cgi-bin/', '',  $sms);
	        
	        $data = array('name' => $name, 'sms' => $sms);
		}else{ 
			$name='';
			$os='';
			$bro='';
			$ip='';
			$sms='';
			$db->setQuery("SELECT  chat.`name`,
									chat.`device`,
									chat.`browser`,
									chat.`ip`,
									chat_details.message_client,
									chat_details.message_operator,
									chat_details.message_datetime,
									IF(ISNULL(chat_nikname.`name`),'ოპერატორი',chat_nikname.name) AS `name`,
									chat.join_date,
									chat.id
							FROM `chat`
							LEFT JOIN `chat_details` ON chat.id = chat_details.chat_id AND chat_details.message_operator != 'daixura'
							LEFT JOIN `chat_nikname` ON chat_nikname.crystal_users_id = chat_details.operator_user_id AND chat_nikname.actived = 1
							WHERE `chat`.ip = '$_REQUEST[ip]'
                            ORDER BY chat_details.message_datetime ASC");
			$imID = 0;
			$res = $db->getResultArray(MYSQLI_NUM);
			foreach($res[result] AS $req){
				$name=$req[0];
				$os=$req[1];
				$bro=$req[2];
				$ip=$req[3];
				if($imID != $req[9]){
				    $sms .= '<tr style="height: 20px;"><td colspan=2 style="font-weight:bold;text-align: center;">------'.$req[8].'-------</td></tr>';
				}
				if($req[4]==''){
				    $sms .= '<tr style="height: 17px;">
                    	            <td colspan="2" style=" word-break: break-word;">
                                        <div style="float: right!important; background: rgba(80, 181, 0, 0.24) none repeat scroll 0 0; border-radius: 15px;padding: 7px 15px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word;box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);">
                            	            <p style="text-align: right; font-weight: bold; font-size: 14px;">'.$req[0].'</p>
                                            <p style="overflow-x: auto; padding-top: 7px; line-height: 20x; font-size: 14px;">'.$req[5].'</p>
                                            <div style="text-align: right; padding-top: 7px;">
                                                <p style="color: #878787; width: 100%;">'.$req[6].'</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height:10px;"></tr>';
					//$sms .= '<tr style="height: 17px;"><td colspan=2 style="font-weight:bold; word-break: break-word;">'.$req[7].'</td></tr><tr style="height: 17px;"><td colspan=2 >'.$req[5].'</td></tr><tr><td colspan=2 style="border-bottom:2px dotted #DBDBDB;text-align: right;"><p>'.$req[6].'</p><time class="timeago" datetime="'.$req[6].'"></time></td></tr>';
				}else{
					//$sms .= '<tr style="height: 17px; background: #e9e9e9;"><td colspan=2 style="font-weight:bold;color:#2681DC; word-break: break-word;">'.$name.'</td></tr><tr style="height: 17px;"><td colspan=2 >'.$req[4].'</td></tr><tr><td colspan=2 style="border-bottom:2px dotted #DBDBDB;text-align: right;"><p>'.$req[6].'</p><time class="timeago" datetime="'.$req[6].'"></time></td></tr>';
					
					$sms .= '<tr style="height: 17px;">
                	            <td colspan="2" style=" word-break: break-word;">
                                    <div style="background: rgba(33, 33, 33, 0.04) none repeat scroll 0 0;border-radius: 15px;padding: 7px 15px;color: #212121; max-width: auto; max-width: 85%; float: left; overflow-wrap: break-word;box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);">
                        	            <p style="overflow-x: auto; font-weight: bold;font-size: 14px;">'.$name.'</p>
                                        <p style="padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 14px;">'.$req[4].'</p>
                                        <div>
                                            <p style="width: 100%; color: #878787;">'.$req[6].'</p>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr style="height:10px;"></tr>';
				}
				$imID = $req[9];
			}
			$data = array('name' => $name, 'os' => $os, 'bro' => $bro, 'ip' => $ip, 'sms' => $sms);
		}
		break;

	case 'check_user_dnd_on_local':
	    $user_id = $_SESSION["USERID"];
	    $db->setQuery("SELECT dndlocalon_status FROM `users_call_dndlocalon` where user_id = '$user_id'");
	    if ($db->getNumRow() == 0) {
	        $db->setQuery("SELECT muted FROM `user_mute_check` where user_id = '$user_id'");
	        if ($db->getNumRow() == 0) {
	            $muted = 0;
	        }else{
	            $row1  = $db->getResultArray();
	            $muted = $row1[result][0][muted];
	        }
	        
	        $db->setQuery("SELECT    COUNT(*) AS `unrid`
        	               FROM      queries
	                       LEFT JOIN queries_history ON queries_history.queries_id = queries.id AND queries_history.user_id = '$user_id'
        	               WHERE     ISNULL(queries_history.id) AND queries.actived = 1");
	        $res_queries = $db->getResultArray();
	        
	        $data = array("status" => 0, 'muted' => $muted, 'queries_unrid_count' => $res_queries[result][0][unrid]);
	        
	    }else{
	        $db->setQuery("SELECT muted FROM `user_mute_check` where user_id = '$user_id'");
	        if ($db->getNumRow() == 0) {
	            $muted = 0;
	        }else{
	            $row1  = $db->getResultArray();
	            $muted = $row1[result][0][muted];
	        }
	        $db->setQuery("SELECT dndlocalon_status FROM `users_call_dndlocalon` where user_id = '$user_id'");
	        $row=$db->getResultArray();
	        
	        $db->setQuery("SELECT    COUNT(*) AS `unrid`
        	               FROM      queries
	                       LEFT JOIN queries_history ON queries_history.queries_id = queries.id AND queries_history.user_id = '$user_id'
        	               WHERE     ISNULL(queries_history.id) AND queries.actived = 1");
	        $res_queries = $db->getResultArray();
	        
	        $data = array("status" => $row[result][0][dndlocalon_status], 'muted' => $muted, 'queries_unrid_count' => $res_queries[result][0][unrid]);
	    }
	    
	    
	    
	    
	    
	    break;
	case 'change_incall_dnd_status':

			$user_id 	  	= $_SESSION["USERID"];
			$active_status 	= $_REQUEST["activeStatus"];
			

			$db->setQuery("SELECT dndlocalon_status FROM `users_call_dndlocalon` where user_id = '$user_id'");
			if ($db->getNumRow() == 0) {
			    $db->setQuery("INSERT INTO `users_call_dndlocalon` 
                                        (`user_id`, `dndlocalon_status`) 
                                   VALUES 
                                        ('$user_id', '$active_status')");
			    $db->execQuery();
			}else{
			    $db->setQuery("UPDATE users_call_dndlocalon
    							 SET dndlocalon_status = '$active_status'
    						  WHERE  user_id           = '$user_id'");
			    $db->execQuery();
			}
			$data = array("active" => true);
			break;
	case 'change_incall_activitie_status':

			$user_id 	  	= $_SESSION["USERID"];
			$seesion_id		= session_id();
			$activitie_id 	= $_REQUEST["activitieId"];
			$type			= $_REQUEST["type"];
			$data 			= array();

			// define activitie statuses
			$crystal_users_activitie_id = $type == "on" ? $activitie_id : 0;

			$db->setQuery("UPDATE users
						      SET work_activities_id = '$crystal_users_activitie_id'
						   WHERE  id                 = '$user_id'");
			$db->execQuery();

			if($type == "on") {

				// insert new activitie data
			    $db->setQuery("INSERT INTO work_activities_log 
                                         (`user_id`, `session_id`, `work_activities_id`, `start_datetime`)
							        VALUES 
                                         ('$user_id', '$seesion_id', '$activitie_id', NOW())");
			    $db->execQuery();

				// define dnd status
				$db->setQuery("SELECT dnd FROM work_activities WHERE id = '$activitie_id' AND actived = 1");
				$dnd_status_assoc = $db->getResultArray();

				$dnd_status = $dnd_status_assoc[result][0]["dnd"];

				$db->setQuery("UPDATE users
							      SET saved_dnd = dnd,
								      dnd       = '$dnd_status'
							    WHERE id        = '$user_id'");
				$db->execQuery();

			} else if($type == "off") {

				$db->setQuery("UPDATE work_activities_log
							      SET end_datetime = NOW()

							   WHERE  session_id         = '$seesion_id'
							   AND    work_activities_id = '$activitie_id'
							   AND    ISNULL(end_datetime)");
				$db->execQuery();

				$db->setQuery("UPDATE users
							      SET dnd = saved_dnd
							   WHERE  id = '$user_id'");
				$db->execQuery();
			}

			// define online and dnd status
			$db->setQuery("SELECT online, dnd from users WHERE id = '$user_id'");
			
			$online_status_assoc = $db->getResultArray();
			$online_status       = $online_status_assoc[result][0]["online"];
			$dnd_status          = $online_status_assoc[result][0]["dnd"];
			
			// define responce data
			$data["onlineStatus"] = (int)$online_status;
			$data["dndStatus"] = (int)$dnd_status;

			break;
	case 'check_activitie_status':

			$user_id          = $_SESSION["USERID"];
			
			$activitie_data   = get_activitie_status_data($user_id);
			$activitie_id     = $activitie_data["id"];
			$activitie_status = $activitie_data["status"];
			$seconds_passed   = get_activitie_seconds_passed($user_id, $activitie_id);

			$data = array(
				"activitieId" => $activitie_id,
				"activitieStatus" => $activitie_status,
				"secondsPassed" => $seconds_passed
			);

			break;
	case 'check_activitie_status_off':
	
		$user_id = $_SESSION["USERID"];
		$activitie_data = get_activitie_status_data($user_id);
		$activitie_id = $activitie_data["id"];
		$activitie_status = $activitie_data["status"];
		$seconds_passed = get_activitie_seconds_passed_off($user_id, $activitie_id);

		$data = array(
			
			"secondsPassed" => $seconds_passed
		);

	break;			

	case 'open_new_sms_dialog':

			$type = $_REQUEST["type"];


			if($_REQUEST['crm']==1){
				$crm_base_id=$_REQUEST['crm_base_id'];
				$crm_phone=mysql_fetch_assoc(mysql_query("SELECT  * FROM autocall_request_base WHERE id='$crm_base_id'"));
				$page		= get_new_sms_dialog_crm($type,$crm_phone);
				$data		= array('page'	=> $page, 'type' => $type);
			}else{
				$page		= get_new_sms_dialog($type);
				$data		= array('page'	=> $page, 'type' => $type);
			}


			break;
	case 'get_shablon':	
	
			$data		 = array('shablon' => getShablon());
			break;

	case 'send_new_sms':
	
			$send_type = $_REQUEST["sendType"];
			$addressee = $_REQUEST["addressee"];
			$sms_text = $_REQUEST["smsText"];
	
			$json = array(
				'Number' => $addressee,
				'Message' => $sms_text
			);
	
			$json = json_encode($json);
	
			$opts = array('http' =>
				array(
					'method'  => 'POST',
					'header'  => 'Authorization: Basic EE9F474A-B34C-4502-A352-8DC8DA53E647',
					'header'  => 'Content-type:application/json; charset=utf-8',
					'content' => $json
				)
			);
	
			$context  = stream_context_create($opts);
			$result = file_get_contents('http://192.168.40.19:8089/api/Smses/Send', false, $context);
	
			$data = array('response' => $result);
	
			break;
	case 'get_sms_template_dialog':
			
		$data = array(
			"html" => sms_template_dialog()
		);
			
		break;
	case 'phone_directory':

		$count = 		$_REQUEST['count'];

		$db->setQuery("SELECT id,
							  id, 
							  name, 
							  CONCAT('<button class=\"download_shablon\" sms_id=\"', id ,'\" data-message=\"', message ,'\" >არჩევა</button>')
						FROM  sms
						WHERE actived = 1");

		$data = $db->getList($count, $hidden);

		break;
		
	case 'get_phone_dir_list':
	    
	    $count  = $_REQUEST['count'];
	    $hidden = $_REQUEST['hidden'];
	    $type   = $_REQUEST['type'];
	    
	    
	    $db->setQuery("SELECT 	 persons_phone.id,
                    	         persons_phone.name,
                    	         person_position AS position,
                    	         IF($type=1, persons_phone.phone_number, persons_phone.mail) AS number
            	       FROM      persons_phone
            	       LEFT JOIN position ON position.id=persons_phone.position_id
            	       WHERE     persons_phone.actived = 1");
	    
	    $res = $db->getResultArray(MYSQLI_NUM);
	    
	    $data = array("aaData"	=> array());
	    
	    foreach ($res[result] AS $aRow){
	        $row = array();
	        for ($i = 0 ; $i < $count ; $i++){
	            /* General output */
	            $row[] = $aRow[$i];
	            if($i == ($count - 1)){
	                $row[] = '<div class="callapp_checkbox">
                                  <input type="checkbox" id="pdl_checkbox_'.$aRow[$hidden].'" name="check_'.$aRow[$hidden].'" value="'.$aRow[3].'" class="pdl-check" />
                                  <label for="pdl_checkbox_'.$aRow[$hidden].'"></label>
                              </div>';
	            }
	        }
	        $data['aaData'][] = $row;
	    }
	    
	    break;
	    
	case 'phone_dir_list':
	    
	    //data variables
	    $type = $_REQUEST['type'];
	    $html = phoneDirList($type);
	    $data = array('html'	=> $html);
	    
	    break;
	    
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;
echo json_encode($data);


/* ******************************
 *	Request Functions
* ******************************
*/
function phoneDirList($type) {
    if($type == 1){
        $address = 'ტელ. ნომერი';
    }else{
        $address = 'მეილი';
    }
    return '<table class="display" id="phoneDirectoryList" style="width: 100%;">
                <thead>
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width: 27%;">სახელი</th>
                        <th style="width: 28%;">თანამდებობა</th>
                        <th style="width: 42%;">'.$address.'</th>
                        <th style="width: 3%;">#</th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                           <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <div class="callapp_checkbox">
                              <input type="checkbox" id="pdl_check_all" name="pdl_check_all" />
                              <label for="pdl_check_all"></label>
                            </div>
                        </th>
                    </tr>
                </thead>
            </table>
            <input type="hidden" id="view_type" value="'.$type.'" />';
    
}
function checkgroup($user){
	$res = mysql_fetch_assoc(mysql_query("  SELECT crystal_users.group_id
											FROM    crystal_users
											WHERE  crystal_users.id = $user"));
	return $res['group_id'];

}

function Addincomming($incom_id){
    global $db;
    
    $ab_person_name      = $_REQUEST['s_u_name'];
    $ext                 = $_SESSION['EXTENSION'];
    $incom_id	         = $_REQUEST['id'];
    $user		         = $_SESSION['USERID'];
    $c_date		         = date('Y-m-d H:i:s');
    
    $call_date			 = $_REQUEST['call_date'];
    $hidden_user		 = $_REQUEST['hidden_user'];
    $hidden_inc			 = $_REQUEST['hidden_inc'];
    $imchat			     = $_REQUEST['imchat'];
    $chat_id             = $_REQUEST['chat_id'];
    $ipaddres            = $_REQUEST['ipaddres'];
    $ii			         = $_REQUEST['ii'];
    $phone				 = $_REQUEST['phone'];
    $incomming_cat_1	 = $_REQUEST['incomming_cat_1'];
    $incomming_cat_1_1	 = $_REQUEST['incomming_cat_1_1'];
    $incomming_cat_1_1_1 = $_REQUEST['incomming_cat_1_1_1'];
    $source			     = $_REQUEST['source'];
    $my_site			 = $_REQUEST['site_id'];
    $s_u_mail			 = $_REQUEST['s_u_mail'];
    $client_sex			 = $_REQUEST['client_sex'];
    $client_birth_year	 = $_REQUEST['client_birth_year'];
    $s_u_status			 = $_REQUEST['s_u_status'];
    $inc_status			 = $_REQUEST['inc_status'];
    $call_content		 = htmlspecialchars($_REQUEST['call_content'], ENT_QUOTES);
    $source_id			 = $_REQUEST['source_id'];
    $personal_pin		 = $_REQUEST['s_u_user_id'];
    $personal_id		 = $_REQUEST['s_u_pid'];
    $lid        		 = $_REQUEST['lid'];
    $lid_comment         = $_REQUEST['lid_comment'];
    $processing_start_date  = $_REQUEST['processing_start_date'];
    
    $db->setQuery("INSERT INTO `incomming_call`
                           SET `id`             = '$incom_id',
                               `date`           = NOW(),
                               `processing_start_date` = '$processing_start_date',
                               `processing_end_date`   = NOW(),
                               `user_id`        = '$user',
                               `extension`      = '$ext',
                               `phone`          = '$phone',
                               `client_user_id` = '$personal_pin',
                               `client_pid`     = '$personal_id',
                               `client_name`    = '$ab_person_name',
                               `client_mail`    = '$s_u_mail',
                               `client_satus`   = '$s_u_status',
                               `client_sex`     = '$client_sex',
                               `client_birth_year` = '$client_birth_year',
                               `cat_1`          = '$incomming_cat_1',
                               `cat_1_1`        = '$incomming_cat_1_1',
                               `cat_1_1_1`      = '$incomming_cat_1_1_1',
                               `inc_status_id`  = '$inc_status',
                               `source_id`      = '$source_id',
                               `lid`            = '$lid',
                               `lid_comment`    = '$lid_comment',
                               `call_content`   = '$call_content'");
    $db->execQuery();
    
    $site_array = explode(",",$my_site);
    $db->setQuery("DELETE FROM incoming_call_site WHERE incomming_call_id = '$incom_id'");
    $db->execQuery();
    
    foreach($site_array AS $site_id){
        $db->setQuery("INSERT INTO incoming_call_site (incomming_call_id, site_id) values ('$incom_id', $site_id)");
        $db->execQuery();
    }
    
    if ($lid == 1) {
        $db->setQuery("SELECT *
                       FROM   outgoing_call
                       WHERE  incomming_call_id = '$incom_id' AND outgoing_type_id = 4");
        
        $check = $db->getNumRow();
        if ($check == 0) {
            $db->setQuery("INSERT INTO `outgoing_call`
                                   SET `user_id`           = '$user',
                                       `date`              =  NOW(),
                                       `start_date`        =  NOW(),
                                       `incomming_call_id` = '$incom_id',
                                       `phone`             = '$phone',
                                       `outgoing_type_id`  = '4',
                                       `status_id`         = '1',
                                       `lid_comment`       = '$lid_comment'
                                       `actived`           = '1'");
            $db->execQuery();
        }
    }
    
}

function Saveincomming($incom_id){
	global $error;
	global $db;
	$ab_person_name      = $_REQUEST['s_u_name'];
	$ext                 = $_SESSION['EXTENSION'];
	$incom_id	         = $_REQUEST['id'];
	$user		         = $_SESSION['USERID'];
	$c_date		         = date('Y-m-d H:i:s');
	
	$call_date			 = $_REQUEST['call_date'];
	$hidden_user		 = $_REQUEST['hidden_user'];
	$hidden_inc			 = $_REQUEST['hidden_inc'];
	$imchat			     = $_REQUEST['imchat'];
	$chat_id             = $_REQUEST['chat_id'];
	$ipaddres            = $_REQUEST['ipaddres'];
	$ii			         = $_REQUEST['ii'];
	$phone				 = $_REQUEST['phone'];
	$incomming_cat_1	 = $_REQUEST['incomming_cat_1'];
	$incomming_cat_1_1	 = $_REQUEST['incomming_cat_1_1'];
	$incomming_cat_1_1_1 = $_REQUEST['incomming_cat_1_1_1'];
	$source			    = $_REQUEST['source'];
	$my_site			= $_REQUEST['site_id'];
	$s_u_mail			= $_REQUEST['s_u_mail'];
	$client_sex			= $_REQUEST['client_sex'];
	$client_birth_year	= $_REQUEST['client_birth_year'];
	$s_u_status			= $_REQUEST['s_u_status'];
	$inc_status			= $_REQUEST['inc_status'];
	$call_content		= htmlspecialchars($_REQUEST['call_content'], ENT_QUOTES);
	$source_id			= $_REQUEST['source_id'];
	$personal_pin		= $_REQUEST['s_u_user_id'];
	$personal_id		= $_REQUEST['s_u_pid'];
	$lid        		= $_REQUEST['lid'];
	$lid_comment        = $_REQUEST['lid_comment'];
	
	//if ($ext=0 || $_REQUEST[imchat] == '1'){
	//if ($hidden_user == $user || $_REQUEST['imchat'] == '1' || $hidden_user == '' || $user == 1){

	        if ($phone != '' && $phone != '2004' && ($personal_pin != '' || $ab_person_name != '')){
	            $db->setQuery("SELECT phone,
                	                  id,
                	                  name
                	           FROM   abonent_pin
                	           WHERE  abonent_pin.phone = '$phone'
                	           LIMIT 1");

	            $check_ab_pin = $db->getResultArray();
	            $ab_inf_id = $check_ab_pin[result][0][id];
	            if ($check_ab_pin[result][0][phone] == '' && $check_ab_pin[result][0][name] == '') {

	                $db->setQuery("INSERT INTO `abonent_pin`
	                                          (`user_id`, `datetime`, `phone`, `pin`, `name`)
	                                    VALUES
	                                          ('$user', NOW(), '$phone', '$personal_pin', '$ab_person_name')");
	                $db->execQuery();
	                
	            }else{
                    $db->setQuery("UPDATE `abonent_pin`
                                      SET `user_id`  = $user,
                                          `datetime` = NOW(),
                                          `pin`      = '$personal_pin',
                                          `name`     = '$ab_person_name'
                                   WHERE  `id`       = '$ab_inf_id'");
	                
	                $db->execQuery();
	                
	            }
	        }

	        if($_REQUEST['ii']==1 && $_REQUEST['source']=='site'){
	            $db->setQuery("SELECT last_user_id FROM site_chat WHERE id = '$chat_id'");
	            $check_user = $db->getResultArray();
	            
	            if ($user == $check_user[result][0][last_user_id]) {
	                $user_update = "`user_id` = '$user',";
	            }else{
	                if ($check_user[result][0][last_user_id] == '') {
	                    $user_update = "`user_id` = '$user',";
	                }else{
	                    $user_update = "";
	                }
	                
	            }
	        }elseif($_REQUEST['ii']==1 && $_REQUEST['source']=='fbm'){
	            $db->setQuery("SELECT last_user_id FROM fb_chat WHERE id = '$chat_id'");
	            $check_user = $db->getResultArray();
	            
	            if ($user == $check_user[result][0][last_user_id]) {
	                $user_update = "`user_id` = '$user',";
	            }else{
	                if ($check_user[result][0][last_user_id] == '') {
	                    $user_update = "`user_id` = '$user',";
	                }else{
	                    $user_update = "";
	                }
	            }
	        }elseif($_REQUEST['ii']==1 && $_REQUEST['source']=='mail'){
	            $db->setQuery("SELECT last_user_id FROM mail_chat WHERE id = '$chat_id'");
	            $check_user = $db->getResultArray();
	            $last_user_id = $check_user[result][0][last_user_id];
	            
	            if ($user == $last_user_id) {
	                $user_update = "`user_id` = '$user',";
	            }else{
	                if ($last_user_id == '') {
	                    $user_update = "`user_id` = '$user',";
	                }else{
	                    $user_update = "";
	                }
	                
	            }
	        }elseif($_REQUEST['ii']==1 && $_REQUEST['source']=='chat'){
	            $db->setQuery("SELECT last_user_id FROM chat WHERE id = '$chat_id'");
	            $check_user = $db->getResultArray();
	            if ($user == $check_user[result][0][last_user_id]) {
	                $user_update = "`user_id` = '$user',";
	            }else{
	                if ($check_user[result][0][last_user_id] == '') {
	                    $user_update = "`user_id` = '$user',";
	                }else{
	                    $user_update = "";
	                }
	                
	            }
	        }else{
	            if($_REQUEST['ii']==1 && $phone != ''){
	               $user_update = "`user_id` = '$user',";
	            }
	        }
	        
	        
	        if($_REQUEST['source']=='site'){
	            if($user != 64 && $user != 1){
	               $db->setQuery("UPDATE site_chat SET last_user_id = '$user', `status` = 2 WHERE id = '$chat_id' AND `status` < 2");
	               $db->execQuery();
	            }
	        }elseif($_REQUEST['source']=='fbm'){
	            if($user != 64 && $user != 1){
	               $db->setQuery("UPDATE fb_chat SET last_user_id = '$user', `status` = 2 WHERE id = '$chat_id' AND `status` < 2");
	               $db->execQuery();
	            }
	        }elseif($_REQUEST['source']=='mail'){
	            if($user != 64 && $user != 1){
	               $db->setQuery("UPDATE mail_chat SET last_user_id = '$user', `status` = 2 WHERE id = '$chat_id' AND `status` < 2");
	               $db->execQuery();
	            }
	        }elseif($_REQUEST['source']=='chat'){
                if($user != 64 && $user != 1){
                   $db->setQuery("UPDATE chat SET last_user_id = '$user', `status` = 2 WHERE id = '$chat_id' AND `status` < 2");
	               $db->execQuery();
	            }
	        }
	        
			$db->setQuery("UPDATE `incomming_call`
                              SET `user_id`           = '$user',
                                  `extension`         = '$ext',
                                  `phone`             = '$phone',
                                  `client_user_id`    = '$personal_pin',
                                  `client_pid`        = '$personal_id',
                                  `client_name`       = '$ab_person_name',
                                  `client_mail`       = '$s_u_mail',
                                  `client_satus`      = '$s_u_status',
                                  `client_sex`        = '$client_sex',
                                  `client_birth_year` = '$client_birth_year',
                                  `cat_1`             = '$incomming_cat_1',
                                  `cat_1_1`           = '$incomming_cat_1_1',
                                  `cat_1_1_1`         = '$incomming_cat_1_1_1',
                                  `inc_status_id`     = '$inc_status',
                                  `source_id`         = '$source_id',
                                  `lid`               = '$lid',
                                  `lid_comment`       = '$lid_comment',
                                  `call_content`      = '$call_content'
                           WHERE  `id`                = '$incom_id'");
			
			$db->execQuery();
			
			$db->setQuery("UPDATE `incomming_call`
			                  SET  processing_end_date = NOW()
			               WHERE  `id` = '$incom_id' AND NOT ISNULL(asterisk_incomming_id) AND ISNULL(processing_end_date)");
			$db->execQuery();
			
			$ischange = $_REQUEST['ischange'];

			if($ischange == "true"){
					
				$site_array = explode(",",$my_site);

				$db->setQuery("SELECT GROUP_CONCAT(`name`) AS `arr` FROM my_web_site WHERE id in (
					SELECT site_id FROM incoming_call_site WHERE incomming_call_id = '$incom_id'
				)
					");
					$sites = $db->getResultArray();
					$arr = $sites["result"][0]['arr'];
					// var_dump($sites);
				$db->setQuery("INSERT INTO `logs`
										SET 
										`event` = 1,
										`date` = NOW(),
										old_value = '$arr',
										`user_id` = '$user',
										row_id = '$incom_id',
										`table`= 'incomming_call',
										`collumn` = 'sites';
					");$db->execQuery();
				$db->setQuery("DELETE FROM incoming_call_site WHERE incomming_call_id = '$incom_id'");
				$db->execQuery();
				
				foreach ($site_array AS $site_id){
					$db->setQuery("INSERT INTO incoming_call_site (incomming_call_id, site_id) values ('$incom_id', $site_id)");
					$db->execQuery();
				}
			}
			
			if ($lid == 1) {
			    $db->setQuery("SELECT * 
                               FROM   outgoing_call
                               WHERE  incomming_call_id = '$incom_id' AND outgoing_type_id = 4");
			    
			    $check = $db->getNumRow();
			    if ($check == 0) {
			        $db->setQuery("INSERT INTO `outgoing_call`
                    			           SET `user_id`           = '$user',
                        			           `date`              =  NOW(),
                        			           `start_date`        =  NOW(),
                                               `incomming_call_id` = '$incom_id',
                        			           `phone`             = '$phone',
                        			           `outgoing_type_id`  = '4',
                        			           `status_id`         = '1',
                                               `lid_comment`       = '$lid_comment',
                        			           `actived`           = '1'");
			        $db->execQuery();
			    }
			}
			
// 		}else{
// 			$error='თქვენ არ გაქვთ შეცვლის უფლება';
// 		}
		if(($chat_id != '' || $chat_id != 0)){
			
			$source              	= $_REQUEST['source'];
		
			if($_REQUEST['ii']==1 && $source=='site'){
			   $db->setQuery("UPDATE `site_chat` SET `status`='3' WHERE (`id`='$chat_id')");
			   $db->execQuery();
			   
			   $db->setQuery("UPDATE `incomming_call`
			                     SET  processing_end_date = NOW()
			                  WHERE  `id` = '$incom_id' AND (ISNULL(processing_end_date) OR processing_end_date='')");
			   $db->execQuery();
			   
			}elseif($_REQUEST['ii']==1 && $source=='fbm'){
			    $db->setQuery("UPDATE `fb_chat` SET `status`='3' WHERE (`id`='$chat_id')");
			    $db->execQuery();
			    
			    $db->setQuery("UPDATE `incomming_call`
            			          SET  processing_end_date = NOW()
            			       WHERE  `id` = '$incom_id' AND (ISNULL(processing_end_date) OR processing_end_date='')");
			    $db->execQuery();
			    
			}elseif($_REQUEST['ii']==1 && $source=='mail'){
			    $db->setQuery("UPDATE `mail_chat` SET `status`='3' WHERE (`id`='$chat_id')");
			    $db->execQuery();
			    
			    $db->setQuery("UPDATE `incomming_call`
			                     SET   processing_end_date = NOW()
			                   WHERE  `id` = '$incom_id' AND (ISNULL(processing_end_date) OR processing_end_date='')");
			    $db->execQuery();
			    
			}else{
			    if($_REQUEST['ii']==1 && $phone !='2004'){
    				$db->setQuery("SELECT `last_request_datetime`,
            							  `status`,
            							   TIME_TO_SEC(TIMEDIFF(NOW(),last_request_datetime)) as timedifference,
            							   who_rejected
            						from   chat 
            						where `id` = '$chat_id';");
    				$last_request_res = $db->getResultArray();
    				$timedifference   = $last_request_res[result][0]['timedifference'];
    				$who_rejected     = $last_request_res[result][0]['who_rejected'];
    				$query_addition   ='';
    				
    				if($timedifference >= 10 && $who_rejected == ''){
    					$query_addition .= ", `who_rejected` = 0";
    				}else if($timedifference < 10 && $who_rejected == ''){
    					$query_addition .= ", `who_rejected` = 1";
    				}
    				
    				$db->setQuery("UPDATE `chat` SET
    									  `status` = '4' 
    				                       $query_addition
    							   WHERE  `id`     = '$chat_id';");
    				$db->execQuery();
    				
    				$db->setQuery("UPDATE `incomming_call`
    				                  SET  processing_end_date = NOW()
    				               WHERE  `id` = '$incom_id' AND (ISNULL(processing_end_date) OR processing_end_date='')");
    				$db->execQuery();
    			}
			}
		}
// 	}else{
// 		$error='თქვენ არ გაქვთ არჩეული ექსთენშენი, გთხოვთ გახვიდეთ სისტემიდან და აირიოთ ';
// 	}

}

function note_incomming(){
	global $error;
	$incom_id	= $_REQUEST['id'];
	$user		= $_SESSION['USERID'];
	$group		= checkgroup($user);

//      $inc_user = mysql_fetch_array(mysql_query("SELECT user_id 
//                                                 FROM   incomming_call
//                                                 WHERE  id = $incom_id"));

//      if ($group==2 || $group==3) {
	    
		mysql_query("UPDATE `incomming_call`
					    SET `noted`              = '1',
                            `monitoring_user_id` =  $user
							 WHERE `id`          = '$incom_id'");

// 	}elseif ($group==1 && $inc_user[user_id] == $user){
	    
// 	    mysql_query("UPDATE `incomming_call`
//             	        SET `noted`              ='1',
//             	            `monitoring_user_id` = $user
//             	     WHERE  `id`                 = '$incom_id'");
// 	}else{
// 	     $error='ეს ფუნქცია თქვენთვის შეზღუდულია';
//  	}

}

function num_block_incomming(){
	global $error;
	$phone			= $_REQUEST['phone'];
	$block_comment	= $_REQUEST['block_comment'];
	$user			= $_SESSION['USERID'];
	$group			= checkgroup($user);
	$c_date			= date('Y-m-d H:i:s');

	if ($group==2 || $group == 1 || $group == 3){
		mysql_query("INSERT INTO `black_list`
						(`user_id`,`date`,`phone`,`comment`)
					VALUES
						('$user','$c_date', '$phone','$block_comment');");

	}else{
		$error='ეს ფუნქცია თქვენთვის შეზღუდულია';
	}
}

function get_comment($hidden_id){
    global $db;
    $data = "";
    $db->setQuery("SELECT   `id`,
                            `user_id`,
                            `incomming_call_id`,
                            `comment`,
                            `datetime`
                   FROM     `chat_comment`
                   WHERE     parent_id = 0 AND incomming_call_id = $hidden_id AND active = 1
                   ORDER BY `datetime` DESC");
    
    $req = $db->getResultArray();
    foreach ($req[result] AS $res){
        $data .='<div style="margin-top: 30px;">
                        <input type=hidden id="hidden_comment_id_'.$res['id'].'" value="'.$res['id'].'"/>
                        <span style="color: #369; font-weight: bold;">'. get_user($res['user_id']) .'</span> '. $res['datetime'] .' <br><br>
                        <span style="border: 1px #D7DBDD solid; padding: 7px; border-radius:50px; background-color:#D7DBDD; "> '.$res['comment'].'</span>
                        <img id="edit_comment" my_id="'. $res['id'] .'" style="cursor:pointer; margin-left: 10px;margin-right: 10px;" class="cat_img cat_edit" alt="img" src="media/images/icons/edit.svg">
                        <img id="delete_comment" my_id="'. $res['id'] .'" style="cursor:pointer" class="cat_img cat_delete" alt="img" src="media/images/icons/delete.svg">
                        <br/><br/><span id="get_answer" class="'. $res['id'] .'"  style="color: #369; font-size:15px;     font-size: 11px;
                        margin-left: 10px;  ">პასუხის გაცემა</span>
                 </div>
                 <div style="margin-left: 50px; margin-top: 10px;">
                    '.get_answers($res['id'],$hidden_id).'
                 </div>';
    }
    
    
    return $data;
}
function web_site($id){
    global $db;
	$data = '';
	
	$db->setQuery("SELECT `id`, `name`
				   FROM   `my_web_site`
				   WHERE   actived = 1");
	
	$res = $db->getResultArray();
	foreach ($res[result] AS $value){
	    $db->setQuery("SELECT id
	                   FROM  `incoming_call_site`
	                   WHERE incoming_call_site.incomming_call_id = '$id' AND site_id = '$value[id]'");
	    $check = $db->getNumRow();
	    
	    if($check>0){
	        $data .= '<option value="' . $value['id'] . '" selected="selected">' . $value['name'] . '</option>';
	    } else {
	        $data .= '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
	    }
	    
	}
	
	return $data;
}

function get_search_site($id){
    global $db;
    $data = '';
    
    $db->setQuery("SELECT `name` AS `id`, `name`
				   FROM   `my_web_site`
				   WHERE   actived = 1");
    
    $data = $db->getSelect($id);
    
    return $data;
}

function get_search_transaction_type($id){
    global $db;
    $data = '';
    
    $db->setQuery("SELECT `name` AS `id`, `name`
				   FROM   `transaction_type`
				   WHERE   actived = 1");
    
    $data = $db->getSelect($id);
    
    return $data;
}


function Getclientstatus($id){
    
    if($id == 1){
        $data .= '<option value="0">----</option>
                  <option value="1" selected="selected">აქტიური</option>
                  <option value="2">პასიური</option>';
    }elseif($id ==2){
        $data .= '<option value="0">----</option>
                  <option value="1">აქტიური</option>
                  <option value="2" selected="selected">პასიური</option>';
    }else{
        $data .= '<option value="0">----</option>
                  <option value="1">აქტიური</option>
                  <option value="2">პასიური</option>';
    }
    
    return $data;
}

function get_s_u_sex($id){
    
    if($id == 1){
        $data .= '<option value="0">----</option>
                  <option value="1" selected="selected">მდედრობითი</option>
                  <option value="2">მამრობითი</option>';
    }elseif($id ==2){
        $data .= '<option value="0">----</option>
                  <option value="1">მდედრობითი</option>
                  <option value="2" selected="selected">მამრობითი</option>';
    }else{
        $data .= '<option value="0">----</option>
                  <option value="1">მდედრობითი</option>
                  <option value="2">მამრობითი</option>';
    }
    
    return $data;
}

function Getincstatus($object_id){
    global $db;
	$data = '';
	$db->setQuery("SELECT `id`,
                          `name`
				   FROM   `inc_status`
				   WHERE  `actived` = 1");

    $data = $db->getSelect($object_id);

	return $data;
}

function Getsource($object_id){
    global $db;
	$data = '';
	$db->setQuery("SELECT 	`id`,
                            `name`
				    FROM 	`source`
				    WHERE 	 actived=1");


	$data = $db->getSelect($object_id);

	return $data;
}

function get_transfer_ext($ext, $check){
    $data = '';
    if ($check == 1) {
        $req = mysql_query("SELECT `id`,
                                   `name`
                            FROM   `crystal_users`
                            WHERE  `group_id` IN(1,2,3) AND `actived` = 1");
        
        $data .= '<option value="0" selected="selected">----</option>';
        while( $res = mysql_fetch_assoc($req)){
            if($res['id'] == $ext){
                $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
            }else{
                $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
            }
        }
    }else{
        $req = mysql_query("SELECT   extention_login.extention 
                            FROM    `extention_login` 
                            JOIN     crystal_users ON crystal_users.last_extension = extention_login.extention
                            WHERE    extention_login.id < 11 AND crystal_users.logged = 1
                            ORDER BY extention_login.extention ASC");
        
        
        $data .= '<option value="0" selected="selected">----</option>';
        while( $res = mysql_fetch_assoc($req)){
            if($res['extention'] == $ext){
                $data .= '<option value="' . $res['extention'] . '" selected="selected">' . $res['extention'] . '</option>';
            }else{
                $data .= '<option value="' . $res['extention'] . '">' . $res['extention'] . '</option>';
            }
        }
    }
    return $data;
}

function Getcategory($category_id){

	$data = '';
	$req = mysql_query("SELECT `id`, `name`
						FROM `category`
						WHERE actived=1 && parent_id=0 ");


	$data .= '<option value="0" selected="selected">----</option>';
	while( $res = mysql_fetch_assoc($req)){
		if($res['id'] == $category_id){
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		}else{
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function get_cat_1($id){
    global $db;
    
    $db->setQuery("SELECT `id`,
                          `name`
                   FROM   `info_category`
                   WHERE   actived = 1 AND `parent_id` = 0");
    
    $req = $db->getResultArray();
    $data .= '<option value="0" selected="selected">----</option>';
    foreach ($req[result] AS $res){
        if($res['id'] == $id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    
    return $data;
    
}

function get_cat_1_1($id,$child_id){
    global $db;
    
    $db->setQuery("SELECT  `id`,
                           `name`
                   FROM    `info_category`
                   WHERE    actived = 1 AND `parent_id` = '$id' AND `parent_id` != 0");
    
    $req = $db->getResultArray();
    
    $data .= '<option value="0" selected="selected">----</option>';
    $i = 0;
    foreach($req[result] AS $res){
        if($res['id'] == $child_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
        $i = 1;
    }
    if($i == 0 && $id > 0){
        $data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
    }
    
    return $data;
}

function get_cat_1_1_1($id,$child_id){
    global $db;
    $db->setQuery("SELECT `id`,
                          `name`
                   FROM   `info_category`
                   WHERE   actived = 1 AND `parent_id` = '$id' AND `parent_id` != 0");
    
    $req = $db->getResultArray();
    
    $data .= '<option value="0" selected="selected">----</option>';
    $i = 0;
    
    foreach($req[result] AS $res){
        if($res['id'] == $child_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
        $i = 1;
    }
    
    if($i == 0 && $id > 0){
        $data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
    }
    
    return $data;
}

function Getdepartment($task_department_id){
	$data = '';
	$req = mysql_query("SELECT `id`, `name`
					    FROM `department`
					    WHERE actived=1 ");


	$data .= '<option value="0" selected="selected">----</option>';
	while( $res = mysql_fetch_assoc($req)){
		if($res['id'] == $task_department_id){
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		}else{
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function Getpriority($priority_id){
	$data = '';
	$req = mysql_query("SELECT `id`, `name`
						FROM `priority`
						WHERE actived=1 ");

	$data .= '<option value="0" selected="selected">----</option>';
	while( $res = mysql_fetch_assoc($req)){
		if($res['id'] == $priority_id){
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		}else{
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function Gettask_type($task_type_id){

	$data = '';
	$req = mysql_query("SELECT `id`, `name`
					    FROM `task_type`
					    WHERE actived=1 ");

	$data .= '<option value="0" selected="selected">----</option>';
	while( $res = mysql_fetch_assoc($req)){
		if($res['id'] == $task_type_id){
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		}else{
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function Getpersons($persons_id){

	$data = '';
	$req = mysql_query("SELECT `id`, `name`
                        FROM   `crystal_users`
                        WHERE   actived=1 AND id IN(88103)
                        UNION ALL
                        SELECT `id`, `name`
                        FROM   `crystal_users`
                        WHERE   actived=1 AND id IN(1505)
                        UNION ALL
                        SELECT `id`, `name`
                        FROM   `crystal_users`
                        WHERE   actived=1 AND id IN(502)
                        UNION ALL
	                    SELECT `id`, `name`
                        FROM   `crystal_users`
                        WHERE   actived=1 AND id IN(88110)
                        UNION ALL
                        SELECT `id`, `name`
                        FROM   `crystal_users`
                        WHERE   actived=1 AND id NOT IN(88103, 1505, 502, 88110)");

	$data .= '<option value="0" selected="selected">----</option>';
	while( $res = mysql_fetch_assoc($req)){
		if($res['id'] == $persons_id){
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		}else{
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function Get_control($persons_id){
	$data = '';
	$req = mysql_query("SELECT `id`, `name`
                        FROM   `crystal_users`
                        WHERE   actived=1 AND id IN(88103)
                        UNION ALL
                        SELECT `id`, `name`
                        FROM   `crystal_users`
                        WHERE   actived=1 AND id IN(1505)
                        UNION ALL
                        SELECT `id`, `name`
                        FROM   `crystal_users`
                        WHERE   actived=1 AND id IN(502)
                        UNION ALL
	                    SELECT `id`, `name`
                        FROM   `crystal_users`
                        WHERE   actived=1 AND id IN(88110)
                        UNION ALL
                        SELECT `id`, `name`
                        FROM   `crystal_users`
                        WHERE   actived=1 AND id NOT IN(88103, 1505, 502, 88110)");

	$data .= '<option value="0" selected="selected">----</option>';
	while( $res = mysql_fetch_assoc($req)){
		if($res['id'] == $persons_id){
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		}else{
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function getlastphone($phone){
    global $db;
    if($phone == ''){
        $res = '';
    }else{
        $db->setQuery("SELECT    	incomming_call.id AS id,
                                    incomming_call.`date` AS call_date,
            						incomming_call.`date` AS record_date,
            						incomming_call.phone AS `phone`,
                                    incomming_call.ipaddres,
            						incomming_call.asterisk_incomming_id,
            						user_info.`name` AS operator,
            						IF(ISNULL(asterisk_incomming.user_id),chat_user_name.`name`,user_info.`name`) AS operator,
            						IF(ISNULL(asterisk_incomming.user_id),chat.answer_user_id,asterisk_incomming.user_id ) as `user_id`,
            						incomming_call.inc_status_id AS `inc_status`,
            						incomming_call.cat_1,
            						incomming_call.cat_1_1,
                                    incomming_call.cat_1_1_1,
            						incomming_call.call_content AS call_content,
            						incomming_call.source_id,
            						incomming_call.client_user_id AS personal_pin,
            						incomming_call.client_pid AS personal_id,
            						incomming_call.web_site_id,
            						incomming_call.client_name,
            						incomming_call.client_satus,
            						incomming_call.client_sex,
                                    incomming_call.client_mail,
            						incomming_call.chat_id,
                                    incomming_call.lid,
                                    incomming_call.lid_comment,
                                    asterisk_incomming.source AS `ast_source`,
                                    chat.ip,
            						site_chat_id,
            						fb_chat_id,
            						mail_chat_id,
                                    messenger_chat_id,
                                    video_call_id
                        FROM 	    incomming_call
                        LEFT JOIN   asterisk_incomming on asterisk_incomming.id = incomming_call.asterisk_incomming_id
                        LEFT JOIN   users ON users.id=asterisk_incomming.user_id
                        LEFT JOIN   user_info ON users.id = user_info.user_id
                        LEFT JOIN   chat on incomming_call.chat_id = chat.id
                        LEFT JOIN   users AS chat_user ON chat_user.id=chat.answer_user_id
                        LEFT JOIN   user_info AS chat_user_name ON chat_user.id=chat_user_name.user_id
                        WHERE      	incomming_call.phone = '$phone'
                        ORDER BY incomming_call.id DESC
                        LIMIT 1");
        $res = $db->getResultArray();
    }
    return $res[result][0];
}


function Getincomming($incom_id,$chat_id){
	global $db;
	if($chat_id == ''){
	    $wh = "incomming_call.id =$incom_id";
	}else{
		if($_REQUEST['source']=='site'){
			$wh = "incomming_call.site_chat_id = $chat_id";
		}elseif($_REQUEST['source']=='fbm'){
			$wh = "incomming_call.fb_chat_id = $chat_id";
		}elseif($_REQUEST['source']=='mail'){
			$wh = "incomming_call.mail_chat_id = $chat_id";
		}elseif($_REQUEST['source']=='video'){
		    $wh = "incomming_call.video_call_id = $chat_id";
		}else{
			$wh = "incomming_call.chat_id = $chat_id";
		}
		
	}
	
    $db->setQuery("SELECT    	incomming_call.id AS id,
                                incomming_call.`date` AS call_date,
        						incomming_call.`date` AS record_date,
        						incomming_call.phone AS `phone`,
                                incomming_call.ipaddres,
        						incomming_call.asterisk_incomming_id,
                                IF(NOT ISNULL(incomming_call.asterisk_incomming_id),
                                    CASE 
                    					WHEN FROM_UNIXTIME(UNIX_TIMESTAMP(asterisk_incomming.call_datetime)+wait_time+duration) <= incomming_call.processing_start_date 
                    						THEN SEC_TO_TIME(UNIX_TIMESTAMP(processing_end_date)-UNIX_TIMESTAMP(processing_start_date))
                    					WHEN FROM_UNIXTIME(UNIX_TIMESTAMP(asterisk_incomming.call_datetime)+wait_time+duration) > incomming_call.processing_start_date
                    						AND FROM_UNIXTIME(UNIX_TIMESTAMP(asterisk_incomming.call_datetime)+wait_time+duration) >= incomming_call.processing_end_date
                    						THEN '00:00:00'
                    					WHEN FROM_UNIXTIME(UNIX_TIMESTAMP(asterisk_incomming.call_datetime)+wait_time+duration) > incomming_call.processing_start_date
                    						AND FROM_UNIXTIME(UNIX_TIMESTAMP(asterisk_incomming.call_datetime)+wait_time+duration) < incomming_call.processing_end_date 
                    						THEN SEC_TO_TIME(UNIX_TIMESTAMP(incomming_call.processing_end_date) - (UNIX_TIMESTAMP(asterisk_incomming.call_datetime)+wait_time+duration))
                    			    END,
                                '00:00:00') AS `processed_time`,
        						user_info.`name` AS operator,
        						IF(ISNULL(asterisk_incomming.user_id),chat_user_name.`name`,user_info.`name`) AS operator,
        						IF(ISNULL(asterisk_incomming.user_id),chat.answer_user_id,asterisk_incomming.user_id ) as `user_id`,
        						incomming_call.inc_status_id AS `inc_status`,
        						incomming_call.cat_1,
        						incomming_call.cat_1_1,
                                incomming_call.cat_1_1_1,
        						incomming_call.call_content AS call_content,
        						incomming_call.source_id,
        						incomming_call.client_user_id AS personal_pin,
        						incomming_call.client_pid AS personal_id,
        						incomming_call.web_site_id,
        						incomming_call.client_name,
        						incomming_call.client_satus,
        						incomming_call.client_sex,
                                incomming_call.client_mail,
                                incomming_call.client_birth_year,
        						incomming_call.chat_id,
                                incomming_call.lid,
								incomming_call.note_comm,
                                incomming_call.lid_comment,
								incomming_call.call_content,
                                asterisk_incomming.source AS `ast_source`,
                                SEC_TO_TIME(asterisk_incomming.duration) AS `duration`,
			                    SEC_TO_TIME(asterisk_incomming.wait_time) AS `wait_time`,
                                chat.ip,
        						site_chat_id,
        						fb_chat_id,
        						mail_chat_id,
                                messenger_chat_id,
                                video_call_id
                    FROM 	    incomming_call
                    LEFT JOIN   asterisk_incomming on asterisk_incomming.id = incomming_call.asterisk_incomming_id
                    LEFT JOIN   users ON users.id=asterisk_incomming.user_id
                    LEFT JOIN   user_info ON users.id = user_info.user_id
                    LEFT JOIN   chat on incomming_call.chat_id = chat.id
                    LEFT JOIN   users AS chat_user ON chat_user.id=chat.answer_user_id
                    LEFT JOIN   user_info AS chat_user_name ON chat_user.id=chat_user_name.user_id
                    WHERE      	$wh ");
    
    $req = $db->getResultArray();
	$res = $req[result][0];
	return $res;
}

function GetPage($res='', $number, $imby, $ipaddres, $ab_pin){
    global $db;
    if ($res[id] == '') {
        $db->setQuery("INSERT INTO incomming_call
                               SET user_id = ''");
        $db->execQuery();
        $hidde_inc_id = $db->getLastId();
        $db->setQuery("DELETE FROM incomming_call WHERE id = $hidde_inc_id");
        $db->execQuery();
        $processing_start_date = date('Y-m-d H:i:s');
    }else{
        $hidde_inc_id = $res[id];
        $db->setQuery("UPDATE incomming_call
	                      SET processing_start_date = NOW()
                       WHERE (ISNULL(processing_end_date) OR processing_end_date = '') 
                       AND    id = '$hidde_inc_id'");
        $db->execQuery();
    }
    
    $db->setQuery("SELECT    COUNT(*) AS `unrid`
                   FROM      queries
                   LEFT JOIN queries_history ON queries_history.queries_id = queries.id AND queries_history.user_id = '$_SESSION[USERID]'
                   WHERE     ISNULL(queries_history.id) AND queries.actived = 1");
    $res_queries = $db->getResultArray();
    
    if ($res_queries[result][0][unrid]>0) {
        $queries_display = "";
        $queries_count   = $res_queries[result][0][unrid];
    }else{
        $queries_display = "display:none";
        $queries_count   = '';
    }
    
    $phone = $_REQUEST['phone'];
	$num   = 0;
	$lid_check = '';
	if($res[phone]==""){if ($number ==''){$num = $phone;}else{$num=$number;}}else{ $num=$res[phone];}
    $lid_comment_hidde = "display:none;";
	if ($res[id] != '') {$dis='disabled="disabled"'; }else{$dis='';}
	if ($res[lid] == 1) {$lid_check='checked'; $lid_comment_hidde = '';}
	if ($ab_pin == '' || $res[mail_chat_id] > 0) {
	    $ab_pin = $res[personal_pin];
	}
	
    $person_pin = $ab_pin;
	
	$crm_count = 0;
	if ($person_pin != '' || $num != '') {
	    
	    $db->setQuery("SELECT 	  COUNT(*) AS `count`  
                       FROM 	  autocall_request ar 
                       LEFT JOIN  autocall_request_details ard ON ar.id = ard.request_id
                       LEFT JOIN  autocall_request_base arb ON arb.request_id = ard.id
                       WHERE      ar.call_type_id = 4 AND ar.actived=1 AND ard.actived = 1 
                       AND       ((arb.number_2 = '$person_pin' AND arb.number_2 != '')  OR (arb.phone_number = '$num' AND arb.phone_number != ''))
                       AND        DATE(ard.set_time) >=CURDATE() - INTERVAL 7 DAY AND DATE(ard.set_time) <= CURDATE()");
	    
	    $check_crm_count = $db->getResultArray();
	    $crm_count       = $check_crm_count[result][0][count];
	}
	
	if ($crm_count==0) {
	    $crm_display = 'display:none;';
	}else{
	    $crm_display = '';
	}
	$person_name = $res['client_name'];
	$person_user = $res['personal_pin'];
	
	// if ($res['person_name'] == '') {

	//     $db->setQuery(" SELECT   `name` AS `name`,
    //                              `pin`  AS `user_id`
    //                     FROM     `abonent_pin`
    //                     WHERE   (`pin` != '' OR `name` != '') AND phone = '$num' AND phone != 'Anonymous'
    //                     ORDER BY  id DESC
    //                     LIMIT 1");
	    
    //     $pers_info   = $db->getResultArray();
	//     $person_name = $pers_info[result][0][name];
	//     $person_user = $pers_info[result][0][user_id];
	// }

	$db->setQuery("SELECT  `comment`
				   FROM    `black_list`
				   WHERE    phone = '$num' AND phone != 'Anonymous' AND `comment` != ''
				   ORDER BY id DESC
				   LIMIT 1");
	
	$req                 = $db->getResultArray();
	$last_blocked_reason = $req[result][0];
	
	$db->setQuery("SELECT  `id`,
                           `name`,
						   `rand_name`,
                           `file_date`
				   FROM    `file`
				   WHERE   `incomming_call_id` = '$res[id]'");
	
	$increm = $db->getResultArray();
	
	$db->setQuery(" SELECT     IF(ISNULL(user_info.`name`),'ოპერატორი',user_info.`name`) AS `name`,
                               IFNULL(extention.extention,0) AS last_extension
                    FROM      `users`
                    LEFT JOIN  extention ON users.extension_id = extention.extention
                    LEFT JOIN `user_info` ON `user_info`.`user_id` = users.id
                    WHERE      users.`id` = '$_SESSION[USERID]'");
	
	$req     = $db->getResultArray();
	$getuser = $req[result][0];
	
	if($number==''){
		if($res['chat_id'] !=0 && $res['chat_id'] != ''){
				$imchat=1;
		}else{
				$imchat=0;
		}
	}else{
		$imchat=0;
	}
	
	if($res['asterisk_incomming_id'] !='null' && $res['asterisk_incomming_id'] != null){
	    $chatcom='display:block;'; $callcom='display:none;';
	}else{
	    $chatcom='display:none;'; $callcom='display:block;';
	}
	
	if($_REQUEST[chat_id]=='' || $_REQUEST[site_chat_id]!=''){
		$mychat_id = 0;
	}else{
		$mychat_id = $_REQUEST[chat_id];
	}
	
	$transfer_resps = 0;
	if($res['chat_id']!='' || $res['chat_id']!=0){$mychat_id = $res['chat_id']; $transfer_resps = 0;}
	$transfer_resps = 0;
	$chat_source = $_REQUEST["source"];	
	if($res['site_chat_id']){$chat_source ='site'; $mychat_id =$res['site_chat_id']; $transfer_resps =0;}
	if($res['fb_chat_id']){$chat_source ='fbm'; $mychat_id =$res['fb_chat_id']; $transfer_resps = 0;}
	if($res['mail_chat_id']){$chat_source ='mail';$mychat_id =$res['mail_chat_id']; $transfer_resps = 0;}
	if($res['video_call_id']){$chat_source ='video'; $mychat_id =$res['video_call_id']; $transfer_resps = 0;};
	
	$sms = '';
	$chat_disable = '';
	$chat_select = '';
	$border = 'border-bottom: 2px solid #333 !important;';
	if($res[chat_id] > 0){
	    $db->setQuery(" SELECT  chat.`name`,
                                chat.`device`,
                                chat.`browser`,
                                chat.`ip`,
                                chat_details.message_client,
                                chat_details.message_operator,
                                chat_details.message_datetime,
                                IF(ISNULL(chat_nikname.`name`),'ოპერატორი',chat_nikname.name) AS `op_name`,
                                chat_details.id
                        FROM `chat`
                        LEFT JOIN `chat_details` ON chat.id = chat_details.chat_id AND chat_details.message_operator != 'daixura'
                        LEFT JOIN `chat_nikname` ON chat_nikname.crystal_users_id = chat_details.operator_user_id AND chat_nikname.actived = 1
                        WHERE `chat`.id = $res[chat_id]
	                    ORDER BY chat_details.message_datetime");
	    
	    $res1           = $db->getResultArray();
	    $chat_detail_id = 0;
	    
	    $db->setQuery("SELECT  `chat_details`.`id`
            	       FROM    `chat_details`
            	       WHERE   `chat_details`.`chat_id` = '$res[chat_id]' AND chat_details.operator_user_id != '$_SESSION[USERID]' 
					   ORDER BY chat_details.id DESC LIMIT 1");
	    
        $req            = $db->getResultArray();
        $ge             = $req[result][0];
        $chat_detail_id = $ge[id];
        
	    foreach($res1[result] AS $req){
	        if (strpos($req[message_client], 'შემოუერთდა ჩატს.') === FALSE) {
	            $name = $req[op_name];
	        }else{
	            $name = '';
	        }
	        if($req[message_client]==''){
	            $sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                                <div style="float: right!important; background: rgba(80, 181, 0, 0.24) none repeat scroll 0 0; border-radius: 15px;padding: 7px 15px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word;box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);">
                    	            <p style="text-align: right; font-weight: bold; font-size: 14px;">'.$req[op_name].'</p>
                                    <p style="padding-bottom: 7px; padding-top: 7px; line-height: 20px; font-size: 14px;">'.$req[message_operator].'</p>
                                    <div style="text-align: right;">
                                        <p style="color: #878787; width: 100%;">'.$req[message_datetime].'</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
	        }else{
	             $sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                                <div style="background: rgba(33, 33, 33, 0.04) none repeat scroll 0 0;border-radius: 15px;padding: 7px 15px;color: #212121; max-width: auto; max-width: 85%; float: left; overflow-wrap: break-word;box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);">
                    	            <p style="font-weight: bold;font-size: 14px;">'.$name.'</p>
                                    <p style="padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 14px;">'.$req[message_client].'</p>
                                    <div>
                                        <p style="width: 100%; color: #878787;">'.$req[message_datetime].'</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
	        }
	    }
	    
	    if($imby != '1'){
    	    $chat_disable = 'disabled';
    	    $chat_select = 'border-bottom: 2px solid rgb(51, 51, 51) !important;';
    	    $border = 'display:none;';
		}
		
		$db->setQuery(" SELECT	IF(who_rejected = 1, 'ოპერატორმა გათიშა', 'აბონენტმა გათიშა') AS rejecter
						FROM   `chat`
						WHERE  `answer_user_id` != 0 AND `status` > 2 
						AND     id               = '$res[chat_id]'");
		
		$rejecter_res = $db->getResultArray();
		$rejecter     = $rejecter_res[result][0]['rejecter'];
		
		$sms .= '<tr style="height: 17px;"><td colspan=2 style="font-weight:bold;color:#2681DC; word-break: break-word;">'.$rejecter.'</td></tr>';
	}elseif($res[site_chat_id] > 0){
	    $db->setQuery("SELECT 
										site_chat.last_user_id,
										site_messages.datetime,
										site_chat.sender_name AS `name`,
										site_messages.text,
										site_messages.media,
										site_messages.id,
										IFNULL((SELECT `name` from user_info WHERE user_info.user_id=site_messages.user_id),'') as operator
								FROM `site_chat`
								LEFT JOIN `site_messages` ON site_chat.id = site_messages.site_chat_id# AND chat_details.message_operator != 'daixura'
								WHERE `site_chat`.id = $res[site_chat_id]
	                            ORDER BY site_messages.datetime");
	    $chat_detail_id = 0;
	    $res1 = $db->getResultArray();
	    foreach ($res1[result] AS $req){
	        if($req[operator]==''){
	            //$sms .= '<tr style="height: 17px;"><td colspan=2 style="font-weight:bold; word-break: break-word;">'.$req[name].'</td></tr><tr style="height: 17px;"><td colspan=2 style=" word-break: break-word;">'.$req[text].'</td></tr><tr><td colspan=2 style="border-bottom:2px dotted #DBDBDB;text-align: right;"><time class="timeago" datetime="'.$req['datetime'].'"></time></td></tr>';
	            $sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                                <div style="background: rgba(33, 33, 33, 0.04) none repeat scroll 0 0;border-radius: 15px;padding: 7px 15px;color: #212121; max-width: auto; max-width: 85%; float: left; overflow-wrap: break-word;box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);">
                    	            <p style="font-weight: bold;font-size: 14px;">'.$req[name].'</p>
                                    <p style="padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 14px;">'.$req[text].'</p>
                                    <div>
                                        <p style="width: 100%; color: #878787;">'.$req['datetime'].'</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
	        }else{
	            //$sms .= '<tr style="height: 17px;"><td colspan=2 style="font-weight:bold;color:#2681DC; word-break: break-word;">'.$req[operator].'</td></tr><tr style="height: 17px;"><td colspan=2 style=" word-break: break-word;">'.$req[text].'</td></tr><tr><td colspan=2 style="border-bottom:2px dotted #DBDBDB;text-align: right;"><time class="timeago" datetime="'.$req['datetime'].'"></time></td></tr>';
	            
	            $sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                                <div style="float: right!important; background: rgba(80, 181, 0, 0.24) none repeat scroll 0 0; border-radius: 15px;padding: 7px 15px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word;box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);">
                    	            <p style="text-align: right; font-weight: bold; font-size: 14px;">'.$req[operator].'</p>
                                    <p style="padding-bottom: 7px; padding-top: 7px; line-height: 20px; font-size: 14px;">'.$req[text].'</p>
                                    <div style="text-align: right;">
                                        <p style="color: #878787; width: 100%;">'.$req['datetime'].'</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
	        }
			$chat_detail_id=$req['id'];
	    }
	    if($imby != '1'){
    	    $chat_disable = 'disabled';
    	    $chat_select = 'border-bottom: 2px solid rgb(51, 51, 51) !important;';
    	    $border = 'display:none;';
		}
    }if($res[fb_chat_id] > 0){
	    $db->setQuery("SELECT     fb_chat.last_user_id,
								  fb_messages.datetime,
								  fb_chat.sender_name AS `name`,
								  fb_messages.text,
								 (SELECT url from fb_attachments WHERE messages_id=fb_messages.id LIMIT 1) as media,
								  fb_messages.id,
								  IFNULL((SELECT `name` from user_info WHERE user_info.user_id=fb_messages.user_id),'') as operator,
                                  (SELECT type from fb_attachments WHERE messages_id=fb_messages.id LIMIT 1) as media_type
					   FROM      `fb_chat`
					   LEFT JOIN `fb_messages` ON fb_chat.id = fb_messages.fb_chat_id# AND chat_details.message_operator != 'daixura'
					   WHERE     `fb_chat`.id = $res[fb_chat_id]
	                   ORDER BY   fb_messages.datetime");
	    
	    $res1           = $db->getResultArray();
	    $chat_detail_id = 0;
	    
	    foreach($res1[result] AS $req){
	        if ($req[media_type] == 'fallback') {
	            if(!empty($req['media'])) $req[text] = $req[text]. "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='https://crm.my.ge/media/images/icons/file_icons.png'></a>";
	        }elseif ($req[media_type] == 'file'){
	            if(!empty($req['media'])) $req[text] = $req[text]. "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='https://crm.my.ge/media/images/icons/file_icons.png'></a>";
	        }elseif ($req[media_type] == 'video'){
	            if(!empty($req['media'])) $req[text] = $req[text]. "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='https://crm.my.ge/media/images/icons/file_icons.png'></a>";
	        }else{
	            if(!empty($req['media'])) $req[text] = $req[text]. "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 200px;' src='$req[media]'></a>";
	        }
	         
	        if($req[operator]==''){
	            $sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                                <div style="background: rgba(33, 33, 33, 0.04) none repeat scroll 0 0;border-radius: 15px;padding: 7px 15px;color: #212121; max-width: auto; max-width: 85%; float: left; overflow-wrap: break-word;box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);">
                    	            <p style="font-weight: bold;font-size: 14px;">'.$req[name].'</p>
                                    <p style="padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 14px;">'.$req[text].'</p>
                                    <div>
                                        <p style="width: 100%; color: #878787;">'.$req['datetime'].'</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
	        }else{
	            $sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                                <div style="float: right!important; background: rgba(80, 181, 0, 0.24) none repeat scroll 0 0; border-radius: 15px;padding: 7px 15px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word;box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);">
                    	            <p style="text-align: right; font-weight: bold; font-size: 14px;">'.$req[operator].'</p>
                                    <p style="padding-bottom: 7px; padding-top: 7px; line-height: 20px; font-size: 14px;">'.$req[text].'</p>
                                    <div style="text-align: right;">
                                        <p style="color: #878787; width: 100%;">'.$req['datetime'].'</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
	        }
			$chat_detail_id=$req['id'];
	    }
	    if($imby != '1'){
    	    $chat_disable = 'disabled';
    	    $chat_select  = 'border-bottom: 2px solid rgb(51, 51, 51) !important;';
    	    $border       = 'display:none;';
		}
	}	
	if($res[mail_chat_id] > 0){
	    
	    $db->setQuery("SELECT      mail_chat.last_user_id,
								   mail_messages.datetime,
								   IF(mail_chat.sender_name = '', mail_chat.sender_id, mail_chat.sender_name) AS `name`,
								   mail_messages.text,
                                   mail_messages.id,
								   IFNULL((SELECT `name` from user_info WHERE user_info.user_id = mail_messages.user_id),'') as operator,
                                   mail_messages.subject
						FROM      `mail_chat`
						LEFT JOIN `mail_messages` ON mail_chat.id = mail_messages.mail_chat_id
						WHERE     `mail_chat`.id = $res[mail_chat_id]
	                    ORDER BY   mail_messages.datetime");
	    
	    $res1           = $db->getResultArray();
	    $chat_detail_id = 0;	    
	    foreach($res1[result] AS $req){
	        
			$db->setQuery("SELECT `name`, SUBSTRING_INDEX(patch,  'htdocs/',-1) AS `patch`, `check_mesage` FROM `mail_attachments` WHERE messages_id=$req[id]");
			$fq          = $db->getResultArray();
            $req['text'] = preg_replace('/<base[^>]+href[^>]+>/', '', $req['text']);
            
			foreach($fq[result] AS $file){
			    if ($file[check_mesage] == 1) {
			        $req['text']      = $req[text]. "<a href='$file[patch]' target='_blank'>$file[name]</a><br>";
			    }else{
					$req['text'] = preg_replace('/(?<=cid:'.$file[name].').*?(?=>)/', $file[patch].'"',  $req['text']);//preg_grep('/(?<=cid:).*?(?=@)/', array($req['4'],'123'));
					$req['text'] = str_replace('cid:'.$file[name], 'https://crm.my.ge/', $req['text']);
					$file[patch]    = str_replace('/var/www/html/','',$file[patch]);
					$req['text']     = $req['text']. "<a href='https://crm.my.ge/$file[patch]'>$file[name]</a><br>";
			    }
			} 
			
			$subject = '';
			if($req[subject] != ''){
			    $subject = 'თემა: '.$req[subject];
			}
			
	        if($req[operator]==''){
	            $sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                                <div style="background: rgba(33, 33, 33, 0.04) none repeat scroll 0 0;border-radius: 15px;padding: 7px 15px;color: #212121; max-width: auto; max-width: 85%; float: left; overflow-wrap: break-word;box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);">
                    	            <p style="font-weight: bold; font-size: 14px;">'.$req[name].'</p>
                                    <p style="font-weight: bold; font-size: 14px;">'.$subject.'</p>
                                    <p style="padding-bottom: 7px; line-height: 20px;font-size: 14px;">'.$req[text].'</p>
                                    <div style="padding-top: 7px;">
                                        <p style="width: 100%; color: #878787;">'.$req['datetime'].'</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
	        }else{
	            $sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                                <div style="float: right!important; background: rgba(80, 181, 0, 0.24) none repeat scroll 0 0; border-radius: 15px;padding: 7px 15px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word;box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);">
                    	            <p style="text-align: right; font-weight: bold; font-size: 14px;">'.$req[operator].'</p>
                                    <p style="padding-top: 7px; line-height: 20x; font-size: 14px;">'.$req[text].'</p>
                                    <div style="text-align: right; padding-top: 7px;">
                                        <p style="color: #878787; width: 100%;">'.$req['datetime'].'</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
	        }
			$chat_detail_id=$req['id'];
	    }
	    
	    $db->setQuery("SELECT     mail_chat.sender_id AS `name`
	                   FROM      `mail_chat`
	                   LEFT JOIN `mail_messages` ON mail_chat.id = mail_messages.mail_chat_id
	                   WHERE     `mail_chat`.id = $res[mail_chat_id]");
	    
	    $mail_info = $db->getResultArray();
	    
	    if ($res[client_mail] == '') {
			$mail_name = $mail_info[result][0][name];
		}else{
			$mail_name = $res[client_mail];
		}
	    
	    if($imby != '1'){
    	    $chat_disable = 'disabled';
    	    $chat_select = 'border-bottom: 2px solid rgb(51, 51, 51) !important;';
    	    $border = 'display:none;';
		}
    }
	if ($res[client_mail] == '') {
		$mail_name = $mail_info[result][0][name];
	}else{
		$mail_name = $res[client_mail];
	}
	if($res[video_call_id] > 0){
// 	    $user	    = $_SESSION['USERID'];
// 	    $res_status = mysql_fetch_array(mysql_query("SELECT `status`
//                                         	         FROM   `videocall`
//                                         	         WHERE   id = $res[video_call_id]"));
	    
// 	    if ($res_status[status] == 1) {
// 	        mysql_query("UPDATE videocall 
//                             SET status       = 2,
//                                 last_user_id = '$user'
//                          WHERE  id           = $res[video_call_id]");
// 	    }
	}
	
	$display_standart_amount_action = "block";
    $display_in_out_amount_action   = "none";
	
    $data  .= '
	<div id="dialog-form">
		<input type="hidden" value="'.$imchat.'" id="imchat">
		<input type="hidden" value="'.$res[fb_chat_id].'" id="fb_chat">
		<input type="hidden" value="'.$res[site_chat_id].'" id="site_chat">
        <input type="hidden" value="'.$res['mail_chat_id'].'" id="mail_chat">
        <input type="hidden" value="'.$res['video_call_id'].'" id="video_chat_id">
	    <input type="hidden" value="'.$_SESSION[USERID].'" id="chat_user_id" chat_user_name="'.$getuser[name].'" ext="'.$getuser[last_extension].'">
	    <fieldset style="width: 210px; min-height: 705px;float: left;margin-right: 3px;" id="chatistema">
			<div style="float:left;width: 100%;">
    	    	<table class="dialog-form-table" style="margin-bottom:20px;" width="100%" id="main_call_chat">
            		<tr style="height: 10px;text-align: left;font-weight: bold;border-bottom: 1px solid #666;">
    	            	<td colspan="2" ><span style="display:block;font-size: 12px;">ზარი</span></td>
    	        	</tr>
    	        	<tr style="height: 27px;text-align: left;border-bottom: 1px solid #666;">
    	            	<td>'.$getuser[last_extension].'</td>
                		<td style="text-align: right;"><img id="chat_call_live" src="media/images/icons/flesh_free.png" alt="ლაივი" title="მიმდინარე ჩატი" width="15px" style="border:0 !important;padding: 0;"></td>
    	        	</tr>
    	        	<tr id="chat_call_queue" style="height: 27px;text-align: left;border-bottom: 1px solid #666;display:none;">
    	            	<td style="cursor:pointer;" id="chat_call_number" class="open_dialog" check_save_chat="1" extention="" number="" ipaddres=""></td>
                    	<td id="chat_call_duration" style="text-align: right;"></td>
    	        	</tr>
    	    	</table>';
	
        		$db->setQuery("SELECT id FROM `chat` WHERE `status` = 1");
        		$req = $db->getNumRow();
    		
$data  .= '
    			<div style="max-height: 620px;" id="aloo">
    				<div id="incoming_chat_tabs" style="border:none; padding: 0em; border: 1px solid #fff; background: #fff;">
    					<ul style="padding: .0em .0em 0; border: 0px solid #A3D0E4;">						
    						<li class="incoming_chat_tab_chat"><a href="#incoming_chat_tab_chat"><img style="margin: 0 -6.5px;" src="media/images/icons/comunication/Chat.png" height="21" width="21"></a><span class="chat fabtext" ></span></li>
                            <li class="incoming_chat_tab_fbm"><a href="#incoming_chat_tab_fbm"><img style="margin: 0 -6.5px;" src="media/images/icons/comunication/Messenger.png" height="21" width="21"></a><span class="fbm_chat fabtext" ></span></li>
                            <li class="incoming_chat_tab_site"><a href="#incoming_chat_tab_site"><img style="margin: 0 -6.5px;" src="media/images/icons/comunication/ON-my.png" height="21" width="21"></a><span class="site_chat fabtext" ></span></li>						
    						<li class="incoming_chat_tab_mail"><a href="#incoming_chat_tab_mail"><img style="margin: 0 -6.5px;" src="media/images/icons/comunication/E-MAIL.png" height="21" width="21"></a><span class="mail_chat fabtext" ></span></li>
                            <li style="display:none;" class="incoming_chat_video"><a href="#incoming_chat_video"><img style="margin: 0 -6.5px;" src="media/images/icons/comunication/Video.png" height="21" width="21"></a><span class="video_chat fabtext" ></span></li>
                        </ul>
    					<div id="incoming_chat_tab_chat" style="max-height: 650px;">
                            <table class="dialog-form-table" width="100%">
                                <tr style="height:15px"></tr>
                	        	<tr style="height: 10px;text-align: left;font-weight: bold;border-bottom: 1px solid #666;">
                	            	<td><span style="display:block;font-size: 12px;">აქტიური ჩატი</span></td>
                	    		</tr>
                	    	</table>
                            <div id="incoming_chat_tab_chat1" style="height: 373px;overflow-y: auto;"></div>
    
                            <table class="dialog-form-table" width="100%">
                                <tr style="height:15px"></tr>
                	        	<tr style="height: 10px;text-align: left;font-weight: bold;border-bottom: 1px solid #666;">
                	            	<td><span style="display:block;font-size: 12px;">რიგშია</span></td>
                	    		</tr>
                	    	</table>
                            <div id="incoming_chat_tab_chat_queue" style="height: 230px;overflow-y: auto;"></div>
                            
                        </div>
    					<div id="incoming_chat_tab_site" style="max-height: 650px;">
                            <table class="dialog-form-table" width="100%">
                                <tr style="height:15px"></tr>
                	        	<tr style="height: 10px;text-align: left;font-weight: bold;border-bottom: 1px solid #666;">
                	            	<td><span style="display:block;font-size: 12px;">აქტიური ჩატი</span></td>
                	    		</tr>
                	    	</table>
                            <div id="incoming_chat_tab_site1" style="height: 373px; overflow-y: auto;"></div>
                            <table class="dialog-form-table" width="100%">
                                <tr style="height:15px"></tr>
                	        	<tr style="height: 10px;text-align: left;font-weight: bold;border-bottom: 1px solid #666;">
                	            	<td><span style="display:block;font-size: 12px;">რიგშია</span></td>
                	    		</tr>
                	    	</table>
                            <div id="incoming_chat_tab_site_queue" style="height: 230px; overflow-y: auto;"></div>
                        </div>
    					<div id="incoming_chat_tab_mail" style="max-height: 650px;">
                            <table class="dialog-form-table" width="100%">
                                <tr style="height:15px"></tr>
                	        	<tr style="height: 10px;text-align: left;font-weight: bold;border-bottom: 1px solid #666;">
                	            	<td><span style="display:block;font-size: 12px;">აქტიური ჩატი</span></td>
                	    		</tr>
                	    	</table>
                            <div id="incoming_chat_tab_mail1"  style="height: 373px; overflow-y: auto;"></div>
                            <table class="dialog-form-table" width="100%">
                                <tr style="height:15px"></tr>
                	        	<tr style="height: 10px;text-align: left;font-weight: bold;border-bottom: 1px solid #666;">
                	            	<td><span style="display:block;font-size: 12px;">რიგშია</span></td>
                	    		</tr>
                	    	</table>
                            <div id="incoming_chat_tab_mail_queue" style="height: 230px; overflow-y: auto;"></div>
                        </div>
    					<div id="incoming_chat_tab_fbm" style="max-height: 650px;">
                            <table class="dialog-form-table" width="100%">
                                <tr style="height:15px"></tr>
                	        	<tr style="height: 10px;text-align: left;font-weight: bold;border-bottom: 1px solid #666;">
                	            	<td><span style="display:block;font-size: 12px;">აქტიური ჩატი</span></td>
                	    		</tr>
                	    	</table>
                            <div id="incoming_chat_tab_fbm1" style="height: 373px; overflow-y: auto;"></div>
                            <table class="dialog-form-table" width="100%">
                                <tr style="height:15px"></tr>
                	        	<tr style="height: 10px;text-align: left;font-weight: bold;border-bottom: 1px solid #666;">
                	            	<td><span style="display:block;font-size: 12px;">რიგშია</span></td>
                	    		</tr>
                	    	</table>
                            <div id="incoming_chat_tab_fbm_queue" style="height: 230px; overflow-y: auto;"></div>
                        </div>
                        <div id="incoming_chat_video" style="max-height: 650px;">
                            <table class="dialog-form-table" width="100%">
                                <tr style="height:15px"></tr>
                	        	<tr style="height: 10px;text-align: left;font-weight: bold;border-bottom: 1px solid #666;">
                	            	<td><span style="display:block;font-size: 12px;">აქტიური ჩატი</span></td>
                	    		</tr>
                	    	</table>
                            <div id="incoming_chat_video1" style="height: 373px; overflow-y: auto;"></div>
                            <table class="dialog-form-table" width="100%">
                                <tr style="height:15px"></tr>
                	        	<tr style="height: 10px;text-align: left;font-weight: bold;border-bottom: 1px solid #666;">
                	            	<td><span style="display:block;font-size: 12px;">რიგშია</span></td>
                	    		</tr>
                	    	</table>
                            <div id="incoming_chat_video_queue" style="height: 230px; overflow-y: auto;"></div>
                        </div>
    				</div>				
    			</div>
    	    </div>
        </fieldset>
    	    <div style="width: 325px;float: left;min-height: 450px;" id="gare_div">
    	        <fieldset style="width: 306px;float: left;padding: 9px;padding-bottom: 5px;margin-bottom: 2px;">
    	           <table style="width:100%;margin-bottom:5px;" id="blocklogo" >
                        <tr>
                            <td>
                                <img id="chat_live" src="media/images/icons/chat.png" alt="ლაივი" title="მიმდინარე ჩატი" width="20px" style="border:0 !important;float:left;padding: 0; margin-right: 5px;cursor:pointer;padding-bottom: 2px;border-bottom: 2px solid #333 !important;">
                                <img id="chat_hist" src="media/images/icons/history.png" alt="ისტორია" title="ისტორია" width="20px" style="filter: brightness(0.1);border:0 !important;float:left;padding: 0; margin-right: 5px;cursor:pointer;padding-bottom: 2px;">
                    
                                <img id="chat_flag" src="media/images/icons/georgia.png" alt="ქვეყანა" title="ქვეყანა" width="20px" style="display:none;margin-left: 95px;border:0 !important;float:left;padding: 0; margin-right: 5px;padding-bottom: 2px;">
                                <img id="chat_browser" src="media/images/icons/chrome.png" alt="ბრაუზერი" title="ბრაუზერი" width="20px" style="display:none;border:0 !important;float:left;padding: 0; margin-right: 5px;padding-bottom: 2px;">
                    	        <img id="chat_device" src="media/images/icons/windows.png" alt="სისტემა" title="სისტემა" width="20px" style="display:none;border:0 !important;float:left;padding: 0; margin-right: 5px;padding-bottom: 2px;">
                    
                    	        <img id="user_block" ip="'.$res[ip].'" src="media/images/icons/block.png" alt="block" title="დაბლოკვა" width="20px" style="border:0 !important;float:right;padding: 0;cursor:pointer;">
                                <img id="chat_end" ip="'.$res[ip].'" src="media/images/icons/chat_end.png" alt="chat_end" title="ჩატის დახურვა" width="20px" style="border:0 !important;float:right;padding: 0;cursor:pointer;margin-right: 5px;display:none;">
                                <img id="choose_button5" src="media/images/icons/upload.png" alt="ატვირთვა" title="ატვირთვა" width="20px" style="border:0 !important;float:right;padding: 0;cursor:pointer;margin-right: 5px;">
                                <input id="choose_file1" name="choose_file" type="file" class="file" style="display: none;">
                            </td>
                        </tr>
                    </table>
    	        </fieldset>
                <fieldset style="width: auto;float: left;">
                    <span check_zoom="0" id="zomm_div" style="float: right; cursor: pointer;">>></span>
                    <div style="float:left;" id="chatcontent">
                        <div style="padding: 6px;background:#FFF;border: 2px solid #DBDBDB; height:430px; width: 288px;overflow: auto;" id="chat_scroll">
                    	   <table style="width: 100%;" id="log">
                            '.$sms.'
        	               </table>
                           <span id="span_seen" style="display:none; float:right; margin-right:5px;">&#10004; seen</span>
        	            </div>
                        <div id="email_attachment_files1">
                        </div>
                	    <div style="width: 300px;margin-top:5px;overflow: hidden;border: 2px solid #DBDBDB;position:relative; height: 162px;">
							<div id="emoji_wrapper"><span id="emoji_close" code="0" >დახურვა</span>'.get_emoji().'</div>
                            <button id="send_chat_program"></button>
							<img id="dialog_emojis" src="https://crm.my.ge/myge_live_chat/img/smiling-emoticon-square-face.png" width="20px" height="20px" alt="">
							<div class="progress" style="display:none;">
								<div class="progress-bar" ></div>
							</div>
							<div id="email_attachment_files"></div> 
							
							<div '.$chat_disable.' contenteditable="true" placeholder="შეიყვანეთ ტექსტი" id="send_message" style="border:0px;width:100%;height:120px;resize:none;overflow-y: auto;"></div>
                    		<input type="hidden" value="'.$mychat_id.'" id="chat_original_id">
                    		<input type="hidden" value="'.$chat_source.'" id="chat_source">
                            <input type="hidden" value="'.($chat_detail_id == ''?'0':$chat_detail_id).'" id="chat_detail_id">
                	    </div>
                    </div>
                </fieldset>
    	    </div>
            <div id="side_menu" style="border: solid 1px #CCC; float: right; width: 613px; margin-left: 2px; background: #EEE; color: #FFF; margin-bottom:2px;">
    			<span class="info" style="margin-left: 10px;display: block;padding: 10px 0px;  cursor: pointer; float:left;" onclick="show_right_side(\'info\')"><img style="border-bottom: 2px solid #333;filter: brightness(0.1);padding-bottom: 3px;" src="media/images/icons/info_blue.png" alt="24 ICON" height="20" width="20" title="ინფო"></span>
                <span class="client_history" style="margin-left: 10px;display: block;padding: 10px 0px;  cursor: pointer; float:left;" onclick="show_right_side(\'client_history\')"><img style="padding-bottom: 3px;" src="media/images/icons/client_history_blue.png" alt="24 ICON" height="20" width="20" title="ისტორია"></span>
    			<span class="task" style="margin-left: 15px;display: block;padding: 10px 0px;  cursor: pointer; float:left;" onclick="show_right_side(\'task\')"><img style="padding-bottom: 3px;" src="media/images/icons/task_blue.png" alt="24 ICON" height="20" width="20" title="დავალება"></span>
    			<span class="sms" style="margin-left: 15px;display: block;padding: 10px 0px;  cursor: pointer; float:left;" onclick="show_right_side(\'sms\')"><img style="padding-bottom: 3px;" src="media/images/icons/sms_blue.png" alt="24 ICON" height="20" width="20" title="ესემეს"></span>
                <span class="client_mail" style="margin-left: 15px;display: block;padding: 10px 0px;  cursor: pointer; float:left;" onclick="show_right_side(\'client_mail\')"><img style="padding-bottom: 3px;" src="media/images/icons/client_email_blue.png" alt="24 ICON" height="20" width="20" title="ელ. ფოსტა"></span>
                <span class="record" style="margin-left: 15px;display: block;padding: 10px 0px;  cursor: pointer; float:left;" onclick="show_right_side(\'record\')"><img style="padding-bottom: 3px;" src="media/images/icons/record_blue.png" alt="24 ICON" height="20" width="20" title="ჩანაწერი"></span>
    			<span class="file" style="margin-left: 15px;display: block;padding: 10px 0px;  cursor: pointer; float:left;" onclick="show_right_side(\'file\')"><img style="padding-bottom: 3px;" src="media/images/icons/file_blue.png" alt="24 ICON" height="20" width="20" title="ფაილები"></span>
                <span class="client_conversation" style=" margin-left: 15px;display: block;padding: 9px  0px;  cursor: pointer; float:left;" onclick="show_right_side(\'client_conversation\')"><img style="padding-bottom: 3px;" src="media/images/icons/client_conversation_blue.png" alt="24 ICON" height="20" width="20" title="შეკითხვები">
                    <span id="client_conversation_count_hint" style="padding: 3px 6px 3px 5px; float:right; text-align: center; font-size: 10px;background: rgb(244, 67, 54); margin-top: -5px; border-radius: 50%; '.$queries_display.'">'.$queries_count.'</span>
                </span>
                <span class="chat_question" style="margin-left: 15px;display: block;padding: 10px 0px;  cursor: pointer; float:left;" onclick="show_right_side(\'chat_question\')"><img style="padding-bottom: 3px;" src="media/images/icons/operator_comments_blue.png" alt="24 ICON" height="20" width="20" title="კომენტარები"></span>
                <span class="newsnews" style="margin-left: 15px;display: block;padding: 10px 0px;  cursor: pointer; float:left;" onclick="show_right_side(\'newsnews\')"><img style="padding-bottom: 3px;" src="media/images/icons/newspaper_blue.png" alt="24 ICON" height="20" width="20" title="სიახლეები"></span>
                <span class="user_logs" style="margin-left: 15px;display: block;padding: 10px 0px;  cursor: pointer; float:left;" onclick="show_right_side(\'user_logs\')"><img style="padding-bottom: 3px;" src="media/images/icons/user_logs_blue.png" alt="24 ICON" height="20" width="20" title="ლოგები"></span>
                <span class="crm" style="margin-left: 15px;display: none;padding: 11px 0px;  cursor: pointer; float:left;" onclick="show_right_side(\'crm\')">
                    <img style="padding-bottom: 3px;" src="media/images/icons/CRM.png" alt="24 ICON" height="18" width="18" title="CRM">
                    <span id="crm_count_hint" style="'.$crm_display.' padding: 1px 5px 2px 5px; text-align: center;font-size: 11px;background: rgb(244, 67, 54);width: 98px !important;border-radius: 50%;">'.$crm_count.'</span>
                </span>
                <span class="call_resume" style="margin-right: 5px; display: block; padding: 10px 0px; cursor: pointer; float:right;" onclick="show_right_side(\'call_resume\')"><img style="padding-bottom: 3px;" src="media/images/icons/resume_blue.png" alt="24 ICON" height="20" width="20" title="დეტალური ინფო"></span>
    		</div>
            <div style="width: 615px;float: right; overflow-y: auto;" id="right_side">
				<fieldset style="display:block; height: 669px; padding:5px" id="info">
                    <table width="100%" class="dialog-form-table">
                        <tr style="height:0px">
							<td style="width: 196px;"><label for="d_number">კატეგორია</label></td>
							<td style="width: 197px;"><label for="d_number">ქვე კატეგორია1</label></td>
							<td style="width: 197px;"><label for="d_number">ქვე კატეგორია2</label></td>
                        </tr>
						<tr style="height:0px">
                            <td><select  id="incomming_cat_1"     style="width: 190px; height: 18px; ">'.get_cat_1($res['cat_1']).'</select></td>
                            <td><select  id="incomming_cat_1_1"   style="width: 191px; height: 18px; ">'.get_cat_1_1($res['cat_1'],$res['cat_1_1']).'</select></td>
                            <td><select  id="incomming_cat_1_1_1" style="width: 191px; height: 18px; ">'.get_cat_1_1_1($res['cat_1_1'],$res['cat_1_1_1']).'</select></td>
                        </tr>
                        <tr style="height:5px"></tr>
                        <tr style="height:0px">
							<td colspan="3"><label for="d_number">საიტი</label></td>
						</tr>
						<tr style="height:0px">
                            <td colspan="3"><select style="height: 18px; width: 590px;" id="my_site" class="idls object" multiple>'.web_site($res['id']).'</select></td>
                        </tr>
                    </table>
                    <hr style="border: 2px solid #FFF; margin-left: -6px; width: 103%;">
                    <table width="100%" class="dialog-form-table">
                        <tr style="height:0px">
							<td style="width: 205px;"><label for="d_number">ტელეფონი</label></td>
							<td style="width: 205px;"><label for="d_number">User ID</label></td>
                            <td style="width: 205px;"><label for="d_number">ელ. ფოსტა</label></td>
						</tr>
						<tr style="height:0px">
                            <td>
								<input style="height: 18px; width: 160px;" type="text" id="phone" class="idle" value="' . $num. '"/>
                                <span id="search_phone" style="margin-top: -20px; margin-left: 170px;" class="search_button"><img style="padding: 0; border: solid 1px #009688;" src="media/images/icons/search_button.png" height="13" width="13"></span>
							</td>
							<td>
								<input style="height: 18px; width: 160px;" type="text" id="s_u_user_id" class="idle" value="'.$person_user.'"/>
                                <span id="search_user_id" style="margin-top: -20px; margin-left: 170px;" class="search_button"><img style="padding: 0; border: solid 1px #009688;" src="media/images/icons/search_button.png" height="13" width="13"></span>
							</td>
                            <td>
								<input style="height: 18px; width: 160px;" type="text" id="s_u_mail" class="idle" value="'.$mail_name.'"/>
                                <span id="search_mail" style="margin-top: -20px; margin-left: 170px;" class="search_button"><img style="padding: 0; border: solid 1px #009688;" src="media/images/icons/search_button.png" height="13" width="13"></span>
							</td>
                        </tr>
                        <tr style="height:5px"></tr>
                        <tr style="height:0px">
                            <td style="width: 205px;"><label for="d_number">საიდ. კოდი / პ.ნ</label></td>
							<td style="width: 205px;"><label for="d_number">სახელი,გვარი / დასახელება</label></td>
							<td style="width: 205px;"><label for="d_number">სქესი</label></td>
						</tr>
						<tr style="height:0px">
                            <td>
								<input style="height: 18px; width: 160px;" type="text" id="s_u_pid" class="idle" value="'.$res['personal_id'].'"/>
                                <span id="search_s_u_pid" style="margin-top: -20px; margin-left: 170px;" class="search_button"><img style="padding: 0; border: solid 1px #009688;" src="media/images/icons/search_button.png" height="13" width="13"></span>
							</td>
                            <td>
                                <input style="height: 18px; width: 183px;" type="text" id="s_u_name" class="idle" value="'.($person_name == '' ? $res['client_name'] : $person_name).'"/>
                                <span id="search_name" style="margin-top: -20px; margin-left: 170px; display:none;" class="search_button"><img style="padding: 0; border: solid 1px #009688;" src="media/images/icons/search_button.png" height="13" width="13"></span>
    						</td>
                            <td>
                                <select  id="client_sex" style="width: 190px; height: 18px; ">'.get_s_u_sex($res['client_sex']).'</select>
							</td>
                        </tr>
                        <tr style="height:5px"></tr>
                        <tr style="height:0px">
							<td><label for="d_number">სტატუსი</label></td>
							<td><label for="d_number">ინფორმაციის წყარო</label></td>
                            <td><label for="d_number">დაბადების წელი</label></td>
						</tr>
						<tr style="height:0px">
                            <td>
                                <select  id="s_u_status" style="width: 190px; height: 18px; ">'.Getclientstatus($res['client_satus']).'</select>
							</td>
                            <td>
                                <select  id="source_id" style="width: 190px; height: 18px; ">'.Getsource($res['source_id']).'</select>
							</td>
							<td>
                                <input style="height: 18px; width: 182px;" type="text" id="client_birth_year" class="idle" value="'.$res[client_birth_year].'"/>
							</td>
                        </tr>
                        <tr style="height:5px"></tr>
                        <tr style="height:0px">
                            <td><label for="d_number">რეაგირება</label></td>
							<td colspan="2"></td>
						</tr>
						<tr style="height:0px">
                            <td>
                                <select  id="inc_status" style="width: 190px; height: 18px; ">'.Getincstatus($res['inc_status']).'</select>
							</td>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td><input style="width: 18px; margin-left: 0px;" type="checkbox" name="language" id="lid" value="1" '.$lid_check.'/></td>
                                        <td style="width: 40px;">ლიდი</td>
                                        <td>
                							<textarea  style="'.$lid_comment_hidde.' resize: vertical;width: 314px; height: 28px;" id="lid_comment" class="idle" name="call_content">' . $res['lid_comment'] . '</textarea>
                						</td>
                                    </tr>
                                </table>
                            </td>
                            
                        </tr>
                    </table>
                    <table width="100%" class="dialog-form-table">
                        <hr style="border: 2px solid #FFF; margin-left: -6px; width: 103%;">
                        <tr style="height:0px">
							<td colspan="3" style="width: 180px;">საუბრის შინაარსი</td>
                        </tr>
					    <tr style="height:0px">
						    <td colspan="3">
    							<textarea  style="'.$callcom.' resize: vertical;width: 98%; height: 50px;" id="call_content" class="idle" name="call_content">' . $res['call_content'] . '</textarea>
    							<textarea  style="'.$chatcom.' resize: vertical;width: 98%; height: 50px;" id="call_content1" class="idle" name="call_content">' . $res['call_content'] . '</textarea>
							</td>
					    </tr>
					</table>
					<table style="'.($res[noted]=='1'?"":"display:none;").'">
                        <tr style="height:10px;"></tr>
						<tr style="height:0px">
							<td style="width: 180px;"><label for="note_comm">კომენტარი</label></td>
						</tr>
					    <tr style="height:0px">
						    <td>
							     <textarea  style="resize: vertical;width: 571px; margin-right: -22px;" id="note_comm" class="idle" name="note_comm">' . $res['note_comm'] . '</textarea>
							</td>
					    </tr>

					</table>
					<hr style="border: 2px solid #FFF;margin-left: -6px;width: 103%;">
					<div style="width:100%; box-sizing: border-box;" id="check_history" style="padding-top: 15px;">
						<div style="width:100%">
                            <legend>მომართვის ისტორია</legend>
            	            <div style="float: left; width: 100%;">
                            <table>
                                <tr style="height:0px;">
                                    <td style="width: 92px;"></td>
                                    <td style="width: 92px;"></td>
                                    <td style="width: 123px;"><label>User Id</label></td>
                                    <td><label>ტელეფონი</label></td>
                                    <td></td>
                                </tr>
                                <tr style="height:0px;">
                                    <td style="width: 92px;"><input class="callapp_filter_body_span_input date_input" type="text" id="start_check" style="width: 79px; height: 20px; margin-top: 20px;" value="'.date('Y-m-d', strtotime('-7 day')).'"></td>
                                    <td style="width: 92px;"><input class="callapp_filter_body_span_input date_input" type="text" id="end_check" style="width: 79px; height: 20px; margin-top: 20px;" value="'.date('Y-m-d').'"></td>
                                    <td><input class="callapp_filter_body_span_input" type="text" id="check_ab_user_id" style="width: 111px; height: 20px; margin-top: 20px;" value="'.$person_pin.'"></td>
                                    <td><input class="callapp_filter_body_span_input" type="text" id="check_ab_phone" style="width: 111px; height: 20px; margin-top: 20px;" value="'.$num.'"></td>
                                    <td><button id="search_ab_pin" style="margin-left: 10px; margin-top: 4px;">ძებნა</button></td>
                                </tr>
                            <table>

            	            </div>
                          <div>
                	            <table class="display" id="table_history" style="width: 100%">
                                    <thead style="font-size: 10px;">
                                        <tr id="datatable_header">
                                            <th>ID</th>
                                            <th style="width: 20%;">თარიღი</th>
                                            <th style="width: 11%;">User ID</th>
                        					<th style="width: 15%">ტელ.ნომერი</th>
                        					<th style="width: 18%;">ოპერატორი</th>
                                            <th style="width: 19%;">კომენტარი</th>
                                            <th style="width: 17%;">რეაგირება</th>
                                        </tr>
                                    </thead>
                                    <thead>
                                        <tr class="search_header">
                                            <th class="colum_hidden">
                                        	   <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                                            </th>
                                            <th>
                                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                                            </th>
                                            <th>
                                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                                            </th>
                                            <th>
                                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                                            </th>
                                            <th>
                                                <input type="text" name="search_phone" value="ფილტრი" class="search_init" />
                                            </th>
                                            <th>
                                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                                            </th>
                                            <th>
                                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                                            </th>
                                        </tr>
                      				</thead>
                  				</table>
                            </div>
						</div>
        	        </div>
				 </fieldset>
                 
                 <fieldset style="display:none; height: 656px;" id="newsnews">
                    <table class="display" id="table_news" >
                        <thead>
                            <tr id="datatable_header">
                                <th>ID</th>
                                <th style="width:15%;">დასაწყისი</th>
                    			<th style="width:15%;">დასასრული</th>
                    			<th style="width:15%;">დასა-<br>ხელება</th>
                                <th style="width:15%;">საიტი(ები)</th>
                    			<th style="width:40%;">შინაარსი</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr class="search_header">
                                <th class="colum_hidden">
                            	    <input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                </th>
                                <th>
                                	<input type="text" name="search_number" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                </th>
                                <th>
                                	<input type="text" name="search_number" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                </th>
                                <th>
                                    <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                </th>
                                <th>
                                    <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                </th>
                                <th>
                                    <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                </th>
                            </tr>
                        </thead>
                    </table>
                 </fieldset>
                 <fieldset style="display:none; height: 656px;" id="client_history">
                    <table>
                        <tr style="height:0px;">
                            <td colspan="2" style="width: 92px; text-align: center;">პერიოდი</td>
                            <td style="width: 123px;"><label>User Id</label></td>
                            <td></td>
                        </tr>
                        <tr style="height:0px;">
                            <td style="width: 92px;"><input class="callapp_filter_body_span_input date_input" type="text" id="date_from" style="width: 79px; height: 20px; margin-top: 20px;" value="'.date('Y-m-d', strtotime('-7 day')).'"></td>
                            <td style="width: 92px;"><input class="callapp_filter_body_span_input date_input" type="text" id="date_to" style="width: 79px; height: 20px; margin-top: 20px;" value="'.date('Y-m-d').'"></td>
                            <td><input class="callapp_filter_body_span_input" type="text" id="client_history_user_id" style="width: 111px; height: 20px; margin-top: 20px;" value="'.$person_pin.'"></td>
                            <td><button id="search_client_history" style="margin-left: 10px; margin-top: 4px;">ძებნა</button></td>
                        </tr>
                    </table>
                    <div style="height: 30px;">
                        <ul id="ul_tab" style="margin-top: 5px; display: block; margin-left: -50px;">
                            <li role="presentation" id="control_tab_id_4_0" onclick="get_sub_tab4(this.id,'.'\'control_tab_4_0\''.')"
                                style="color: black; border-bottom-width: 2px; border-bottom-color: rgb(11, 92, 154);">
                                <a style="color: black; text-decoration: none;" href="#">განცხადებები</a>
                            </li>
                            <li role="presentation" id="control_tab_id_4_1" onclick="get_sub_tab4(this.id,'.'\'control_tab_4_1\''.')"
                                style="border-bottom-width: 1px; border-bottom-color: gray;"><a href="#"
                                style="text-decoration: none; color: gray;">ტრანზაქციები</a>
                            </li>
                        </ul>
                    </div>
                    <div id="control_tab_4_0" class="ex_highlight_row">
                        <div id="dt_example">
                            <table>
                                <tr style="height:0px">
                                    <td>თარიღი</td>
                                    <td>მოქმედება</td>
                                </tr>
                                <tr style="height:0px">
                                    <td style="width: 200px;">
                                         <select  id="client_history_filter_date" style="width: 190px; height: 18px; ">
                                            <option value="0">ყველა</option>
                                            <option value="განთავსებული">განთავსებული</option>
                                            <option value="განახლებული">განახლებული</option>
                                            <option value="მოქმედების ვადა">მოქმედების ვადა</option>
                                          </select>
                                    </td>
                                    <td>
                                         <select  id="client_history_filter_activie" style="width: 190px; height: 18px; ">
                                            <option value="0">ყველა</option>
                                            <option value="განცხადების დამატება">განცხადების დამატება</option>
                                            <option value="რედაქტირება">რედაქტირება</option>
                                            <option value="წაშლა">წაშლა</option>
                                          </select>
                                    </td>
                                    <td>
                                        <td><button id="search_client_orders" style="margin-left: 10px;">ფილტრი</button></td>
                                    <td>
                                </tr>
                            </table>
                            <div style="width: 593px; overflow-x: scroll; margin-top: 10px; height: 465px; overflow-x: auto;">
                                <table class="display" id="example4_0" style="width: 1000px;">
                                    <thead>
                                        <tr style="font-size: 10px;" id="datatable_header">
                                            <th>ID</th>
                                            <th style="width: 11%;">ID</th>
                                            <th style="width: 11%;">დასახელება</th>
                                            <th style="width: 11%;">თარიღი</th>
                                            <th style="width: 11%;">პრომოუშენი</th>
                                            <th style="width: 11%;">მოქმედება</th>
                                            <th style="width: 11%;">დაბლოკვის<br>თარიღი</th>
                                            <th style="width: 11%;">დაბლოკვის<br>მიზეზი</th>
                                            <th style="width: 11%;">დაბლოკვის <br> რ-ბა</th>
                                            <th style="width: 13%;">გაგზავნილი<br>გაფრთხილ.</th>
                                        </tr>
                                    </thead>
                                    <thead>
                                        <tr class="search_header">
                                            <th class="colum_hidden">
                                                <input type="text" name="search_id" value="ფილტრი" class="search_init" style="font-size: 10px; height: 15px; width: 80%"/>
                                            </th>
                                            <th>
                                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                                       style="font-size: 10px; height: 15px; width: 80%"/>
                                            </th>
                                            <th>
                                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                                       style="font-size: 10px; height: 15px; width: 80%"/>
                                            </th>
                                            <th>
                                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                                       style="font-size: 10px; height: 15px; width: 80%"/>
                                            </th>
                                            <th>
                                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                                       style="font-size: 10px; height: 15px; width: 80%"/>
                                            </th>
                                            <th>
                                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                                       style="font-size: 10px; height: 15px; width: 80%"/>
                                            </th>
                                            <th>
                                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                                       style="font-size: 10px; height: 15px; width: 80%"/>
                                            </th>
                                            <th>
                                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                                       style="font-size: 10px; height: 15px; width: 80%"/>
                                            </th>
                                            <th>
                                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                                       style="font-size: 10px; height: 15px; width: 80%"/>
                                            </th>
                                            <th>
                                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                                       style="font-size: 10px; height: 15px; width: 80%"/>
                                            </th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div style="display: none" id="control_tab_4_1" class="ex_highlight_row">
                        <div id="dt_example">
                            <table style="width: 570px;">
                                <tr style="height:0px">
                                    <td style="width: 135px;">საიტი</td>
                                    <td style="width: 175px;">ჯგუფი</td>
                                    <td style="width: 198px;">ტრანზაქციის ტიპი</td>
                                    <td></td>
                                </tr>
                                <tr style="height:0px">
                                    <td style="width: 200px;">
                                         <select  id="client_history_filter_site" style="width: 190px; height: 18px;">"'.get_search_site('').'"</select>
                                    </td>
                                    <td>
                                         <select  id="client_history_filter_group" style="width: 190px; height: 18px; ">
                                            <option value="0">-----</option>
                                            <option value="ხარჯი">ხარჯი</option>
                                            <option value="ჩარიცხვა">ჩარიცხვა</option>
                                            <option value="შიდა">შიდა</option>
                                          </select>
                                    </td>
                                    <td>
                                         <select  id="client_history_filter_transaction_type" style="width: 190px; height: 18px; ">"'.get_search_transaction_type('').'"</select>
                                    </td>
                                    <td>
                                        <button id="search_client_transaction" style="margin-left: 10px;">ფილტრი</button>
                                    </td>
                                </tr>
                            </table>
                            <div style="width: 593px; overflow-x: scroll;  margin-top: 10px; height: 465px; overflow-x: auto;">
                                <table class="display" id="example4_1" style="width: 790px;">
                                    <thead>
                                        <tr style="font-size: 10px;" id="datatable_header">
                                            <th>ID</th>
                                            <th style="width:9%;">საიტი</th>
                                            <th style="width: 25%;">ტრანზაქციის<br>ტიპი</th>
                                            <th style="width: 9%;">ოპერატორი</th>
                                            <th style="width: 9%;">ID</th>
                                            <th style="width: 9%;">საწყისი<br>თანხა </th>
                                            <th style="width: 9%;">თანხა</th>
                                            <th style="width: 9%;">რაოდენობა</th>
                                            <th style="width: 9%;">საბოლოო<br>თანხა</th>
                                            <th style="width: 13%;">თარიღი</th>
                                        </tr>
                                    </thead>
                                    <thead>
                                        <tr class="search_header">
                                            <th class="colum_hidden">
                                                <input type="text" name="search_id" value="ფილტრი" class="search_init" style="font-size: 10px; height: 15px; width: 80%"/>
                                            </th>
                                            <th>
                                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                                       style="font-size: 10px; height: 15px; width: 80%"/>
                                            </th>
                                            <th>
                                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                                       style="font-size: 10px; height: 15px; width: 80%"/>
                                            </th>
                                            <th>
                                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                                       style="font-size: 10px; height: 15px; width: 80%"/>
                                            </th>
                                            <th>
                                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                                       style="font-size: 10px; height: 15px; width: 80%"/>
                                            </th>
                                            <th>
                                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                                       style="font-size: 10px; height: 15px; width: 80%"/>
                                            </th>
                                            <th>
                                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                                       style="font-size: 10px; height: 15px; width: 80%"/>
                                            </th>
                                            <th>
                                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                                       style="font-size: 10px; height: 15px; width: 80%"/>
                                            </th>
                                            <th>
                                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"
                                                       style="font-size: 10px; height: 15px; width: 80%"/>
                                            </th>
                                            <th>
                                                <input type="text" name="search_category" value="ფილტრი" class="search_init"
                                                       style="font-size: 10px; height: 15px; width: 80%"/>
                                            </th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                 </fieldset>
                 <fieldset style="display:none; height: 656px;" id="client_mail">
                    <div class="margin_top_10">
                        <div style="float:right;">
                             <a style="text-decoration: none; color: #019acc; font-family: Arial;" href="index.php?pg=102" target="_blank">ყველა მეილი</a> 
				             <a href="index.php?pg=102" target="_blank"><img style="margin-top: 4px;" src="media/images/icons/new-tab-button.png" width="11" height="9" alt=""></a>
				        </div>           
        	            <div id="button_area">
                            <button id="add_mail">ახალი E-mail</button>
                        </div>
                        <table class="display" id="table_mail" >
                            <thead>
                                <tr id="datatable_header">
                                    <th>ID</th>
                                    <th style="width: 20%;">თარიღი</th>
                                    <th style="width: 25%;">ადრესატი</th>
                                    <th style="width: 35%;">გზავნილი</th>
                                    <th style="width: 20%;">სტატუსი</th>
                                </tr>
                            </thead>
                            <thead>
                                <tr class="search_header">
                                    <th class="colum_hidden">
                                	    <input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                    	<input type="text" name="search_number" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                </tr>
                            </thead>
                        </table>
    	            </div>
                 </fieldset>
                 <fieldset style="display:none; height: 656px;" id="client_conversation">
                    <div>
        	           <table class="display" id="table_question" style="width:100%">
                            <thead>
                                <tr id="datatable_header">
                                    <th>ID</th>
                                    <th style="width: 20%;">თარიღი</th>
                                    <th style="width: 20%;">თანამშრომელი</th>
                                    <th style="width: 20%;">კითხვა</th>
                                    <th style="width: 40%;">პასუხი</th>
                                </tr>
                            </thead>
                            <thead>
                                <tr class="search_header">
                                    <th class="colum_hidden">
                                	    <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input style="width: 99%;" type="text" name="search_date" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input style="width: 99%;" type="text" name="search_date" value="ფილტრი" class="search_init" />
                                    </th>
                                </tr>
                            </thead>
                        </table>
        	         </div>
                 </fieldset>
                 <fieldset style="display:none; height: 656px;" id="chat_question">
                     <p>ახალი კომენატრი</p>
    	             <textarea id="add_ask" style="border: 1px solid #dbdbdb; resize:none; width:440px; height:50px; margin-top:5px; display:inline-block;" rows="3"></textarea > <button style="height: 50px; top: -20px;" id="add_comment">დამატება</button>
                     <div style="height: 512px; overflow-y: scroll; border: 1px solid #dbdbdb;">'.get_comment($hidde_inc_id).'</div>
                     <input type=hidden id="comment_incomming_id" value="'.$res['id'].'"/>
                 </fieldset>
                 <fieldset style="display:none; height: 656px;" id="user_logs">
                     <table class="display" id="user_log_table" style="width:100%">
                        <thead>
                            <tr style="font-size: 11px;" id="datatable_header">
                                <th>ID</th>
                                <th style="width:20%">თარიღი</th>
                                <th style="width:15%">ქმედება</th>
                                <th style="width:12%">მომხმა<br>რებელი</th>
                                <th style="width:15%">ველი</th>
                                <th style="width:19%">ახალი მნიშვნელობა</th>
                                <th style="width:19%">ძველი მნიშვნელობა</th>
                            </tr>
                        </thead>
                        <thead >
                            <tr class="search_header">
                                <th class="colum_hidden">
                                    <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                                </th>
                                <th>
                                    <input type="text" name="search_number" value="ფილტრი" class="search_init" />
                                </th>
                                <th>
                                    <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                                </th>
                                <th>
                                    <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                                </th>
                                <th>
                                    <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                                </th>
                                <th>
                                    <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                                </th>
                                <th>
                                    <input style="width:97%" type="text" name="search_date" value="ფილტრი" class="search_init" />
                                </th>
                            </tr>
                        </thead>
                    </table>
                 </fieldset>
                 <fieldset style="display:none; height: 656px;" id="call_resume">
                     <table width="100%" class="dialog-form-table">
                        <tr style="height:0px">
							<td style="width: 205px;"><label for="d_number">ოპერატორი</label></td>
							<td style="width: 205px;"><label for="d_number">თარიღი</label></td>
                            <td style="width: 205px;"><label for="d_number">მომართვის ნომერი</label></td>
						</tr>
						<tr style="height:0px">
                            <td>
								<input style="height: 18px; width: 160px;" type="text" id="my_operator_name" class="idle" value="'.$res[operator].'" disabled="disabled"/>
                            </td>
							<td>
								<input style="height: 18px; width: 160px;" type="text" id="call_datetime" class="idle" value="'.$res[call_date].'" disabled="disabled"/>
                            </td>
                            <td>
								<input style="height: 18px; width: 160px;" type="text" id="incomming_call_id" class="idle" value="'.$hidde_inc_id.'" disabled="disabled"/>
                            </td>
                        </tr>
                        <tr style="height:15px;"></tr>
                        <tr style="height:0px">
							<td style="width: 205px;"><label for="d_number">ლოდინის დრო</label></td>
							<td style="width: 205px;"><label for="d_number">საუბრის ხანგრძლივობა</label></td>
                            <td style="width: 205px;"><label for="d_number">დამუშავების ხანგრძლივობა</label></td>
						</tr>
						<tr style="height:0px">
                            <td>
								<input style="height: 18px; width: 160px;" type="text" id="call_wait_time" class="idle" value="'.$res[wait_time].'" disabled="disabled"/>
                            </td>
							<td>
								<input style="height: 18px; width: 160px;" type="text" id="call_duration" class="idle" value="'.$res[duration].'" disabled="disabled"/>
                            </td>
                            <td>
								<input style="height: 18px; width: 160px;" type="text" id=call_done_date" class="idle" value="'.$res[processed_time].'" disabled="disabled"/>
                            </td>
                        </tr>
                        <tr style="height:15px;"></tr>
                        <tr style="height:0px">
							<td colspan="3"><label for="d_number">საიტი</label></td>
						</tr>
						<tr style="height:0px">
                            <td colspan="3"><select style="height: 18px; width: 590px;" id="my_site_resume" class="idls object" disabled="disabled" multiple>'.web_site($res['id']).'</select></td>
                        </tr>
                    </table>
                 </fieldset>
				 <fieldset style="display:none; height: 656px;" id="task">
                    <div id="task_button_area" style="height: 30px;"> 
                        <button id="add_task_button">ახალი დავალება</button>
                    </div>
    	            <table class="display" id="task_table" style="width: 100%; margin-left:0;">
                        <thead>
                            <tr id="datatable_header">
                                <th>ID</th>
                                <th style="width: 20%">თარიღი</th>
                                <th style="width: 20%;">დასაწყისი</th>
                                <th style="width: 20%;">დასასრული</th>
                                <th style="width: 20%;">სტატუსი</th>
                                <th style="width: 20%;">პასუხისმგებელი</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr class="search_header">
                                <th class="colum_hidden">
                                    <input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                </th>
                                <th>
                                    <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                </th>
                                <th>
                                    <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                </th>
                                <th>
                                    <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                </th>
                                <th>
                                    <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                </th>
                                <th>
                                    <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                </th>
                            </tr>
                        </thead>
                    </table>
                </fieldset>
				<fieldset style="display:none; height: 656px;" id="sms">
					<div class="margin_top_10">
                    <div style="float:right;">
                         <a style="text-decoration: none; color: #019acc; font-family: Arial;" href="index.php?pg=68" target="_blank">ყველა ესემესი</a> 
			             <a href="index.php?pg=68" target="_blank"><img style="margin-top: 4px;" src="media/images/icons/new-tab-button.png" width="11" height="9" alt=""></a>
			        </div>         
					<div id="button_area">
						<button id="add_sms_phone" class="jquery-ui-button new-sms-button" data-type="phone">ტელეფონის ნომრით</button>
					</div>
					<table class="display" id="table_sms" >
						<thead>
							<tr id="datatable_header">
								<th>ID</th>
								<th style="width: 20%;">თარიღი</th>
								<th style="width: 15%;">ადრესატი</th>
								<th style="width: 50%;">ტექსტი</th>
								<th style="width: 15%;">სტატუსი</th>
							</tr>
						</thead>
						<thead>
							<tr class="search_header">
								<th class="colum_hidden">
								<input type="text" name="search_id" value="ფილტრი" class="search_init"/>
								</th>
								<th>
									<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
								</th>
								<th>
									<input type="text" name="search_date" value="ფილტრი" class="search_init"/>
								</th>
								<th>
									<input type="text" name="search_date" value="ფილტრი" class="search_init"/>
								</th>
								<th>
									<input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 97%;"/>
								</th>
							</tr>
						</thead>
					</table>
					</div>
				</fieldset>
                <fieldset style="display:none; height: 656px;" id="crm">
                    <div style="width:100%">
                            
            	            <div style="float: left; width: 100%;">
                            <table>
                                <tr style="height: 0px;">
                                    <td colspan="2" style="width: 145px; text-align: center;">პერიოდი</td>
                                    <td><label>User Id</label></td>
                                    <td>ტელეფონი</td>
                                    <td></td>
                                </tr>
                                <tr style="height: 0px;">
                                    <td style="width: 92px;"><input class="callapp_filter_body_span_input date_input" type="text" id="start_crm" style="width: 79px; height: 20px; margin-top: 20px;" value="'.date('Y-m-d', strtotime('-7 day')).'"></td>
                                    <td style="width: 92px;"><input class="callapp_filter_body_span_input date_input" type="text" id="end_crm" style="width: 79px; height: 20px; margin-top: 20px;" value="'.date('Y-m-d').'"></td>
                                    <td><input class="callapp_filter_body_span_input" type="text" id="check_ab_crm_pin" style="width: 111px; height: 20px; margin-top: 20px;" value="'.$person_pin.'"></td>
                                    <td><input class="callapp_filter_body_span_input" type="text" id="check_ab_crm_phone" style="width: 111px; height: 20px; margin-top: 20px;" value="'.$res[phone].'"></td>
                                    <td><button id="search_ab_crm_pin" style="margin-left: 10px; margin-top: 4px;">ძებნა</button></td>
                                </tr>
                            <table>

            	            </div>
                          <div>
                	            <table class="display" id="table_crm" style="width: 100%">
                                        <thead>
                                            <tr id="datatable_header">
                                                <th>ID</th>
                                                <th style="width: 15%;">Upload<br>date</th>
                                                <th style="width: 14%;">Prom-<br>date</th>
                            					<th style="width: 11%;">User Id</th>
                            					<th style="width: 12%;">User<br>name</th>
                                                <th style="width: 13%;">Mobile</th>
                                                <th style="width: 13%;">PID</th>
                                                <th style="width: 10%;">Com-<br>ment</th>
                                                <th style="width: 12%;">status</th>
                                            </tr>
                                        </thead>
                                        <thead>
                                            <tr class="search_header">
                                                <th class="colum_hidden">
                                            	   <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                                                </th>
                                                <th>
                                                    <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                                                </th>
                                                <th>
                                                    <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                                                </th>
                                                <th>
                                                    <input type="text" name="search_phone" value="ფილტრი" class="search_init" />
                                                </th>
                                                <th>
                                                    <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                                                </th>
                                                <th>
                                                    <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                                                </th>
                                                <th>
                                                    <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                                                </th>
                    	                        <th>
                                                    <input style="width: 97%;" type="text" name="search_category" value="ფილტრი" class="search_init" />
                                                </th>
                                                <th>
                                                    <input style="width: 96%;" type="text" name="search_category" value="ფილტრი" class="search_init" />
                                                </th>
                                            </tr>
                          				</thead>
                          				</thead>
                      				</table>
                                </div>
			    </fieldset>
                <fieldset style="display:none; height: 656px;" id="record" >
	               <div style="margin-top: 10px;">
                        <audio controls style="margin-left: 145px;" id="auau">
                          <source src="" type="audio/wav">
                          Your browser does not support the audio element.
                        </audio>
                   </div>
                   <fieldset style="display:block !important; margin-top: 10px;">
                        <legend>შემომავალი ზარი</legend>
        	            <table style="margin: auto;">
        	               <tr>
        	                   <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;">თარიღი</td>
                        	   <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;">ხანგძლივობა</td>
                               <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;">ოპერატორი</td>
                               <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;">ვინ გათიშა</td>
                        	   <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;">მოსმენა</td>
                    	    </tr>
        	                '.GetRecordingsSection($res).'
                	    </table>
    	            </fieldset>
    	      </fieldset>
              <fieldset style="display:none; height: 656px;" id="file">
                    <table style="float: right;text-align: center; border: 1px solid #ccc; width: 100%;">
					<tr>
						<td>
							<div class="file-uploader">
								<input id="choose_file" type="file" name="choose_file" class="input" style="display: none;">
								<button id="choose_button" style="width: 100%;" class="center">აირჩიეთ ფაილი</button>
								<input id="hidden_inc" type="text" value="" style="display: none;">
							</div>
						</td>
					</tr>
				</table>
			     <table style="float: right;border: 1px solid #ccc;width: 100%;margin-top: 2px;text-align: center;">
			          <tr>
			           <td colspan="4">მიმაგრებული ფაილი</td>
			          </tr>
				</table>
				<table id="file_div" style="float: right; border: 1px solid #ccc; width: 100%;margin-top: 2px; text-align: center;">';

		             foreach ($increm[result] AS $increm_row){
						$data .='
						        <tr style="border-bottom: 1px solid #CCC;">
                                  <td style="border-right: 1px solid #CCC;height: 20px;vertical-align: middle;word-wrap:break-word;">'.$increm_row[file_date].'</td>
						          <td style="border-right: 1px solid #CCC;height: 20px;vertical-align: middle;word-wrap:break-word;">'.$increm_row[name].'</td>
						          <td style="border-right: 1px solid #CCC;height: 20px;vertical-align: middle;"><button type="button" value="https://crm.my.ge/media/uploads/file/'.$increm_row[rand_name].'" style="cursor:pointer; border:none;height:16px; width:16px; background:none;background-image:url(\'media/images/get.png\');" id="download" ></button><input type="text" style="display:none;" id="download_name" value="'.$increm_row[rand_name].'"> </td>
						          <td style="height: 20px;vertical-align: middle;"><button type="button" value="'.$increm_row[id].'" style="cursor:pointer; border:none; height:16px; width:16px; background:none; background-image:url(\'media/images/x.png\');" id="delete"></button></td>
						        </tr>';
					}

    	 $data .= '
    	 		</table>
    		</fieldset>
            

	 		</div>
	 		<input id="hidden_user" type="text" value="'. $res['user_id'] .'" style="display: none;">
	 		<input id="note" type="text" value="'. $res['note'] .'" style="display: none;">
            <input id="ast_source" type="text" value="'. $res['ast_source'] .'" style="display: none;">
            <input id="hidde_incomming_call_id" type="text" value="'. $res['id'] .'" style="display: none;">
            <input id="processing_start_date" type="text" value="'.$processing_start_date.'" style="display: none;">
	 </div>';

	return $data;
}

function GetRecordingsSection($res){
    global $db;
    
	if ($res[phone]==''){
	    $num_row = 0;
	}else{
    	$db->setQuery(" SELECT    SEC_TO_TIME(asterisk_incomming.duration) AS `time`,
                				  CONCAT(DATE_FORMAT(asterisk_incomming.call_datetime,'%Y/%m/%d/'),asterisk_incomming.file_name)  AS `userfield`,
                				  asterisk_incomming.call_datetime,
                				  user_info.`name`,
                				  IF(asterisk_incomming.disconnect_cause = 4, 'ოპერატორმა', 'მომხმარებელმა') AS `completed`
                        FROM 	  asterisk_incomming
                        LEFT JOIN user_info ON user_info.user_id = asterisk_incomming.user_id
	                    WHERE	  asterisk_incomming.id ='$res[asterisk_incomming_id]'");
    	
    	$num_row = $db->getNumRow();
    	$req     = $db->getResultArray();
	}

	if ($num_row == 0){
		$data .= '<tr>
                      <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;" colspan=5>ჩანაწერი არ მოიძებნა</td>
        	      </tr>';
	}else{
        foreach($req[result] AS $res2){
    		$src = $res2['userfield'];
    		$data .= '
    		      <tr>
                    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$res2[call_datetime].'</td>
            	    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$res2[time].'</td>
                    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$res2[name].'</td>
                    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$res2[completed].'</td>
            	    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;cursor: pointer;" onclick="listen(\''.$src.'\')"><span style="color: #2a8ef2;">მოსმენა</span></td>
        	      </tr>';
    	}
	}
	
    return $data;
}


function get_activitie_options() {
    global $db;
	$options = '<li class="is-active" data-id="0" data-color="#bdc3c7">
					<span class="activitie-color-block" style="background: #bdc3c7;"></span>
					<span class="activitie-name">აირჩიეთ აქტივობა</span>
				</li>';
	$db->setQuery("SELECT id, name, color FROM work_activities WHERE actived = 1");
    $req=$db->getResultArray();
	foreach ($req[result] AS $res){

		$options .= '<li data-id="'.$res["id"].'" data-color="'.$res["color"].'">
						<span class="activitie-color-block" style="background:'.$res["color"].';"></span>
						<span class="activitie-name">'.$res["name"].'</span>
					</li>';

	}

	return $options;

}

function get_dnd_status($userid) {

	$query = mysql_query("SELECT dnd FROM crystal_users WHERE id = '$userid'");
	$res = mysql_fetch_assoc($query);

	return $res["dnd"];

}

function get_activitie_status_data($userid) {
    global $db;
	$db->setQuery("SELECT work_activities_id
	               FROM   users
	               WHERE  id = '$userid'");

	$res = $db->getResultArray();
	$activitie_id = (int) $res[result][0]["work_activities_id"];
	$activitie_status = $activitie_id == 0 ? 'off' : 'on';

	return array(
		"id" => $activitie_id,
		"status" => $activitie_status
	);

}

function get_activitie_seconds_passed($userid, $activitieid) {
    global $db;

	$db->setQuery("SELECT TIME_TO_SEC(TIMEDIFF(NOW(),start_datetime)) AS time_difference
				   FROM   work_activities_log
				   WHERE  user_id            = '$userid'
				   AND 	  work_activities_id = '$activitieid'
				   AND	  ISNULL(end_datetime) AND start_datetime > date(now())");

	$req = $db->getResultArray();
	$res = $req[result][0];

	

		$db->setQuery("SELECT    TIME_TO_SEC(DATE_FORMAT(work_shift.timeout,'%H:%i')) AS shift_timeout
					   FROM      work_graphic_rows
					   LEFT JOIN work_shift ON work_shift.id = work_graphic_rows.work_shift_id
					   WHERE     work_graphic_id =(SELECT id
        							               FROM   work_graphic
        						                   WHERE  project_id = '2' AND year = year(now()) 
                                                   AND    month = DATE_FORMAT(now(), '%m') 
                                                   AND    actived = 1 AND `operator_id` = '$userid')
					   AND `date` = CONCAT(DATE_FORMAT(now(), '%d'),'.',DATE_FORMAT(now(), '%m'),'.20',DATE_FORMAT(now(), '%y'))");

		$res_break_time = $db->getResultArray();
		$arr_break_time = $res_break_time[result][0];

		$db->setQuery("SELECT sum(TIME_TO_SEC(TIMEDIFF(end_datetime ,start_datetime)) ) AS time_difference
					   FROM   work_activities_log
					   WHERE  user_id = '$userid'
					   AND 	  work_activities_id IN (SELECT id FROM work_activities WHERE actived = 1)
					   AND    end_datetime > date(now()) AND start_datetime > date(now())");

		$res_used_time = $db->getResultArray();
		$arr_used_time = $res_used_time[result][0];

	// echo ">>>".$arr_break_time['shift_timeout'] - $arr_used_time["time_difference"];

	// if($arr_break_time['shift_timeout'] != 0){

			if($res['time_difference'] > 0){
				$result = $arr_break_time['shift_timeout'] - $arr_used_time["time_difference"] - $res["time_difference"];
			}
			else
				$result = $arr_break_time['shift_timeout'] - $arr_used_time["time_difference"];

	// }
	// else {
	// 	$result = "not_set";
	// }
	

	return $result;

}


function get_new_sms_dialog($type) {

	// define adressee input structures
	$adressee_inputs = $type == 'phone' ? '<div class="new-sms-row">
												<label for="smsAddresseeByPhone">ტელეფონის ნომერი</label>
												<div class="new-sms-input-holder">
													<input type="text" id="smsAddresseeByPhone">
												</div>
											</div>'
										: '<div class="new-sms-row grid">
												<div class="nsrg-col">
													<label for="smsAddresseeByPIN">PIN კოდი</label>
													<div class="new-sms-input-holder">
														<input type="text" id="smsAddresseeByPIN">
													</div>
												</div>
												<div class="nsrg-col">
													<label for="smsAddresseeByIDNum">პირადი ნომერი</label>
													<div class="new-sms-input-holder">
														<input type="text" id="smsAddresseeByIDNum">
													</div>
												</div>
											</div>';
	
	//new sms send dialog content
	return '<fieldset style="display:block;" class="new-sms-send-container">

				<!-- label for fieldset -->
				<legend>SMS</legend>

				<!-- row unit -->
				<div class="new-sms-row">
					<button id="smsTemplate" data-button="jquery-ui-button">შაბლონი</button>
				</div>

				<!-- row unit -->
				'.$adressee_inputs.'

				<!-- row unit -->
				<div class="new-sms-row">
					<label for="smsText">ტექსტი</label>
					<div class="new-sms-input-holder">
						<textarea id="newSmsText"></textarea>
					</div>
				</div>

				<!-- row unit -->
				<div class="new-sms-row">
					<input type="text" id="smsCharCounter" data-limit="150" value="0/150">
					<button id="sendNewSms" data-button="jquery-ui-button">გაგზავნა</button>
				</div>

			</fieldset>';

}

function getShablon(){
	global $db;
	$db->setQuery("SELECT 	sms.id,
        					sms.`name`,
        					sms.`message`
					FROM 	sms
					WHERE 	sms.actived=1 ");

	$data_count = $db->getNumRow();

	if($data_count > 0) {

		$data = '<table id="box-table-b1">
            		<tr class="odd">
            			<th style="width: 26px;">#</th>
            			<th style="width: 160px;">დასახელება</th>
            			<th>ქმედება</th>
            		</tr> ';

		$req=$db->getResultArray();
		foreach ($req[result] AS $res3){
        	$data .= '<tr class="odd">
            				<td>' . $res3[id] . '</td>
            				<td style="width: 30px !important;">' . $res3['name'] . '</td>
            				<td style="font-size: 10px !important;">
            					<button style="width: 45px;" class="download_shablon" sms_id="' . $res3['id'] . '" data-message="' . $res3['message'] . '">არჩევა</button>
            				</td>
        			   </tr>';

		}
		$data.='</table>';
    } else {
		$data = '<div class="empty-sms-shablon">
					<p>შაბლონთა ჩამონათვალი ცარიელია</p>
				 </div>';
	}
    return $data;
}


function sms_template_dialog() {

	return '
		<div class="sms-template-table-wrapper">
			<table class="display" id="sms_template_table" style="width: 100%;">
				<thead>
					<tr id="datatable_header">
						<th>ID</th>
						<th style="width: 10%;" >№</th>
						<th style="width:  70%;">დასახელება</th>
						<th style="width:  15%">ქმედება</th>
					</tr>
				</thead>
				<thead>
					<tr class="search_header">
						<th class="colum_hidden">
							<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
						</th>
						<th>
							<input type="text" name="search_number" value="" class="search_init" style="width:100%;"></th>
						<th>
							<input type="text" name="search_date" value="ფილტრი" class="search_init" style="width:100%;"/>
						</th>
						<th>
							
						</th>
					</tr>
				</thead>
			</table>
		<div>';

}

function Getquestion($id){
    $data = '
        <p>ახალი კომენატრი</p>
        <textarea id="add_ask" style="resize:none; width:440px; height:50px; margin-top:5px; display:inline-block;" rows="3"></textarea > <button style="height: 50px;  top: -20px;" id="add_comment">დამატება</button>
        <div style="height: 512px; overflow-y: auto; border: 1px solid #dbdbdb;">'.get_comment($id).'</div>
        <input type=hidden id="comment_incomming_id" value="'.incomming_call_id.'"/>
    ';
    return $data;
}

function get_user($id){
    global $db;
    $data   ='';
    $db->setQuery("SELECT `name`
                   FROM   `user_info`
                   WHERE   user_id = $id");
    
    $res    = $db->getResultArray();
    $data   = $res[result][0]['name'];
    return $data;
}

function get_answers($id,$hidden_id){
    global $db;
    $data= '';
    $db->setQuery("SELECT `id`,
                          `user_id`,
                          `incomming_call_id`,
                          `comment`,
                          `datetime`
                    FROM  `chat_comment`
                    WHERE  parent_id = $id AND incomming_call_id = $hidden_id AND active = 1");
    
    $req = $db->getResultArray();
    
    foreach($req[result] AS $res){
        $data .='<div>
                        <span style="color: #369; font-weight: bold;">'. get_user($res['user_id']) .'</span> '. $res['datetime'] .' <br><br>
                        <span style="border: 1px #D7DBDD solid; padding: 7px; border-radius:50px; background-color:#D7DBDD; ">'.$res['comment'].'</span>
                        <img id="edit_comment" my_id="'. $res['id'] .'" style="cursor:pointer; margin-left: 10px;margin-right: 10px;" class="cat_img cat_edit" alt="img" src="media/images/icons/edit.svg">
                        <img id="delete_comment" my_id="'. $res['id'] .'" style="cursor:pointer" class="cat_img cat_delete" alt="img" src="media/images/icons/delete.svg"><br/><br/>
                </div>';
    }
    
    return $data;
    
}

function get_emoji(){
	global $db;
	$db->setQuery("SELECT `emojis`.`code` FROM `emojis` WHERE `emojis`.actived = 1");

    $result  = $db->getResultArray();
    $emojis = "";
    foreach ($result[result] AS $arr) {
        $emojis .= "<span code= '&#".$arr[code]."' >&#".$arr[code]."</span>";
    }
    $data = array("emojis" => $emojis);
    return $emojis;
}

?>
