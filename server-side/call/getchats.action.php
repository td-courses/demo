<?php

/* ******************************
 *	Request aJax actions
* ******************************
*/
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();
$data = array();
$action  = $_REQUEST['act'];
$user_id = $_SESSION['USERID'];

switch ($action) {
	case 'getchats':
        $db->setQuery("SELECT   chat.`id`,
                                chat.`name` as `name`,
                                chat.`status`,
                                TIME_FORMAT(TIMEDIFF(NOW(),chat.last_user_message_date),'%H:%i:%s') AS tt,
                                '' as user_id1,
                                'chat' as `source`,
                                (SELECT operator_user_id FROM chat_details WHERE chat_id = chat.id ORDER BY id DESC LIMIT 1) as user_id,
                                `status`
                        FROM    `chat`
                        WHERE    chat.`status` IN(2,3) AND last_user_id = $user_id
                        UNION ALL
                        SELECT  chat.`id`,
                                chat.`name` as `name`,
                                chat.`status`,
                                TIME_FORMAT(TIMEDIFF(NOW(),chat.join_date),'%H:%i:%s') AS tt,
                                chat.last_user_id AS `last_chat_user`,
                                'chat' as `source`,
                                last_user_id as user_id,
                                `status`
                        FROM    `chat`
                        WHERE    chat.`status` = 1
                        UNION ALL
                        SELECT 	id,
                                sender_name as `name`,
                                `status`, 
                                TIME_FORMAT(TIMEDIFF(NOW(),first_datetime),'%H:%i:%s') as tt,
                                sender_id AS `sender_id`,
                                'site' AS `source`,
                                (SELECT user_id FROM site_messages as m WHERE m.site_chat_id=site_chat.id ORDER BY m.id DESC LIMIT 1) as user_id,
                                `status`
                        FROM `site_chat` 
                        WHERE `status` = 1
                        UNION ALL
                        SELECT 	id,
                                sender_name as `name`,
                                `status`, 
                                TIME_FORMAT(TIMEDIFF(NOW(),last_datetime),'%H:%i:%s') as tt,
                                sender_id AS `sender_id`,
                                'site' AS `source`,
                                (SELECT user_id FROM site_messages as m WHERE m.site_chat_id=site_chat.id ORDER BY m.id DESC LIMIT 1) as user_id,
                                `status`
                        FROM `site_chat` 
                        WHERE `status` = 2 AND last_user_id = $user_id
                        UNION ALL                                 
                        SELECT 	id,
                                sender_name as `name`,
                                `status`, 
                                TIME_FORMAT(TIMEDIFF(NOW(),first_datetime),'%H:%i:%s') as tt,
                                sender_id AS `sender_id`,
                                'fbm' AS `source`,
                                (SELECT user_id FROM fb_messages as m WHERE m.fb_chat_id=fb_chat.id ORDER BY m.id DESC LIMIT 1) as user_id,
                                `status`				
                        FROM `fb_chat` 
                        WHERE `status` = 1   
                        UNION ALL
                        SELECT 	id,
                                sender_name as `name`,
                                `status`, 
                                TIME_FORMAT(TIMEDIFF(NOW(),last_datetime),'%H:%i:%s') as tt,
                                sender_id AS `sender_id`,
                                'fbm' AS `source`,
                                (SELECT user_id FROM fb_messages as m WHERE m.fb_chat_id=fb_chat.id ORDER BY m.id DESC LIMIT 1) as user_id,
                                `status`				
                        FROM `fb_chat` 
                        WHERE `status` = 2 AND last_user_id = $user_id     
                        UNION ALL
                        SELECT 	`id`, 
                                `sender_name` as `name`, 
                                `status`,                             
                                TIME_FORMAT(TIMEDIFF(NOW(),first_datetime),'%H:%i:%s') AS `tt` ,    
                                sender_id AS `sender_id`,
                                'mail' AS `source`,
                                last_user_id as user_id,
                                `status`
                        FROM `mail_chat` as m           
                        WHERE `status` = 1
                        UNION ALL
                        SELECT 	`id`, 
                                `sender_name` as `name`, 
                                `status` AS `sender_id`,                             
                                TIME_FORMAT(TIMEDIFF(NOW(),last_datetime),'%H:%i:%s') AS `tt` ,    
                                sender_id,
                                'mail' AS `source`,
                                (SELECT user_id FROM mail_messages WHERE mail_chat_id = m.id ORDER BY id DESC LIMIT 1) as user_id,
                                `status`
                        FROM `mail_chat` as m           
                        WHERE `status` = 2 AND last_user_id = $user_id
                        UNION ALL
                        SELECT 	`id`, 
                                `sender_name` as `name`, 
                                `status`,                             
                                 TIME_FORMAT(TIMEDIFF(NOW(),first_datetime),'%H:%i:%s') AS `tt` ,    
                                 sender_id AS `sender_id`,
                                'video' AS `source`,
                                 last_user_id as user_id,
                                `status`
                        FROM    `videocall` as m           
                        WHERE   `status` = 1
                        UNION ALL
                        SELECT 	`id`, 
                                `sender_name` as `name`, 
                                `status`,                             
                                 TIME_FORMAT(TIMEDIFF(NOW(),first_datetime),'%H:%i:%s') AS `tt` ,    
                                 sender_id AS `sender_id`,
                                'video' AS `source`,
                                 last_user_id as user_id,
                                `status`
                        FROM    `videocall` as m           
                        WHERE   `status` = 2 AND last_user_id = $user_id");
        
        $res = $db->getResultArray();
        $i=0;
        $chat_color  = 0;
        $site_color = 0;
        $mail_color  = 0;
        $site_color  = 0;
        $fbm_color   = 0;
        
        $page=array('chat'=>'', 'chat_queue' => '', 'chat_color_status' => 0, 'site'=>'', 'site_queue'=>'', 'site_color_status' => 0, 'fbm'=>'', 'fbm_queue'=>'', 'fbm_color_status' => 0, 'mail'=>'', 'mail_queue'=>'', 'mail_color_status' => 0, 'site_chat'=>'', 'site_chat_queue'=>'', 'site_color_status' => 0, 'video'=>'', 'video_queue'=>'');
        foreach ($res[result] AS $result){
	        $i++;
	        if($result[name] == ''){
                $inc_name = 'სტუმარი '.$i;
            }else{
                $inc_name = $result[name];
            }
            
            if($result['user_id']){ 
                $ic='out_chat.png';
                $tab_bacgraund_status = 0;
            }else{  
                $ic='inc_chat.png';
                $tab_bacgraund_status = 1;
            }
            
            if($tab_bacgraund_status == 1 && $result['source'] == 'chat' && $result['status'] == 2){
                $chat_color = $tab_bacgraund_status;
            }elseif ($tab_bacgraund_status == 1 && $result['source'] == 'site'  && $result['status'] == 2){
                $site_color = $tab_bacgraund_status;
            }elseif ($tab_bacgraund_status == 1 && $result['source'] == 'mail'  && $result['status'] == 2){
                $mail_color = $tab_bacgraund_status;
            }elseif ($tab_bacgraund_status == 1 && $result['source'] == 'site_chat'  && $result['status'] == 2){
                $site_color = $tab_bacgraund_status;
            }elseif ($tab_bacgraund_status == 1 && $result['source'] == 'fbm'  && $result['status'] == 2){
                $fbm_color = $tab_bacgraund_status;
            }
            
            $color = get_color($i);
            $source = ($result['source']);
            $status = ($result['status']);
            
            
            if ($source != 'chat') {
                $new_source = $source.'_queue';
                
                if($source == 'video'){
                    $call_id = $result[sender_id];
                }
                $page[$new_source] = $page[$new_source].'';
                if ($status==1) {
                    
                    $page[$new_source] = $page[$new_source].'<div style=" word-break: break-word; background: '.$color.';margin-top:10px;border:1px solid #85b1de;overflow: hidden;padding: 5px 5px;cursor:pointer;" class="clickmetostart" chat_id="'.$result[id].'" call_id="'.$call_id.'" data-source="'.$result[source].'"  color="'.$color.'" title="'.$result[user_id1].'">
                                                                 <div style="display:block;width: 125px; font-size: 10px; float:left;font-weight:bold;">'.$inc_name.'</div>
                                                                 <div style="display:block;width: 55px; font-size: 10px; float:left;">'.($result[tt]).'</div>
                                                                 <img id="call_status_chat" src="media/images/icons/'.$ic.'" alt="chat" width="14px" style="border:0 !important;float:left;padding: 0;">
                                                             </div>';
                }else{
                    
                    $page[$source] = $page[$source].'<div style=" word-break: break-word; background: '.$color.';margin-top:10px;border:1px solid #85b1de;overflow: hidden;padding: 5px 5px;cursor:pointer;" class="clickmetostart" chat_id="'.$result[id].'" call_id="'.$call_id.'" data-source="'.$result[source].'"  color="'.$color.'" title="'.$result[user_id1].'">
                                                         <div style="display:block;width: 125px; font-size: 10px; float:left; font-weight:bold;">'.$inc_name.'</div>
                                                         <div style="display:block;width: 55px; font-size: 10px; float:left;">'.($result[tt]).'</div>
                                                         <img id="call_status_chat" src="media/images/icons/'.$ic.'" alt="chat" width="14px" style="border:0 !important;float:left;padding: 0;">
                                                     </div>';
                    
                    
                }
            }else{
                if ($status == 1) {
                    $new_source = $source.'_queue';
                    
                    $page[$new_source] = $page[$new_source].'<div style=" word-break: break-word; background: '.$color.';margin-top:10px;border:1px solid #85b1de;overflow: hidden;padding: 5px 5px;cursor:pointer;" class="clickmetostart" chat_id="'.$result[id].'" data-source="'.$result[source].'"  color="'.$color.'" title="'.$result[user_id1].'">
                                                                 <div style="display:block;width: 125px; font-size: 10px; float:left; font-weight:bold;">'.$inc_name.'</div>
                                                                 <div style="display:block;width: 55px;float:left; font-size: 10px;">'.($result[tt]).'</div>
                                                                 <img id="call_status_chat" src="media/images/icons/'.$ic.'" alt="chat" width="14px" style="border:0 !important;float:left;padding: 0;">
                                                             </div>';
                }else{
                    $page[$source] = $page[$source].'<div style=" word-break: break-word; background: '.$color.';margin-top:10px;border:1px solid #85b1de;overflow: hidden;padding: 5px 5px;cursor:pointer;" class="clickmetostart" chat_id="'.$result[id].'" data-source="'.$result[source].'"  color="'.$color.'" title="'.$result[user_id1].'">
                                                         <div style="display:block;width: 125px; float:left; font-size: 10px; font-weight:bold;">'.$inc_name.'</div>
                                                         <div style="display:block;width: 55px; font-size: 10px; float:left;">'.($result[tt]).'</div>
                                                         <img id="call_status_chat" src="media/images/icons/'.$ic.'" alt="chat" width="14px" style="border:0 !important;float:left;padding: 0;">
                                                     </div>';
                }
            }
            if($result[status]==3){
	            $page[$source] = $page[$source].
	                "<script>
	                    if($('#chat_original_id').val() == $result[id]){
    	                    if($('#gle').length < 1){
    	                    $('#log').append('<tr id=\"gle\" style=\"height: 17px;\"><td colspan=2 style=\"font-weight:bold;color:#2681DC; word-break: break-word;\"></td></tr><tr style=\"height: 17px;\"><td colspan=2 >მომხმარებელმა დატოვა ჩატი!</td></tr><tr><td colspan=2 style=\"border-bottom:2px dotted #DBDBDB;text-align: right;\"><time class=\"timeago\" datetime=\"\"></time></td></tr>');
    	                    $('#send_message').prop('disabled',true);
                            }
    	                }
	                </script>";
	        }
	    } 
	    
	    $page[chat_color_status] = $chat_color;
	    $page[site_color_status] = $site_color;
	    $page[fbm_color_status] = $fbm_color;
	    $page[site_color_status] = $site_color;
	    $page[mail_color_status] = $mail_color;
	    
         if($_REQUEST['source']=='site'){ 
            $db->setQuery("SELECT id FROM `site_chat` ORDER BY id DESC");
            
            $req1 = $db->getResultArray();
            
            $i=0;
            $first_id = 0;
            foreach ($req1[result] AS $result1){
                $i++;
                $first_id = $result1[id];
            }
            
            if($_REQUEST[chat_id] != 0){
                $ii = 0;
                $chat_detail_id = $_REQUEST[chat_detail_id] ;
                
                $db->setQuery("  SELECT   `m`.`id`,
                                          `text`,
                                           IFNULL(u.`name`,`sender_name`) as name,
                                          `datetime` as 'message_datetime',
                                           m.media
                                 FROM     `site_chat` as c
                                 JOIN     `site_messages` as m ON m.site_chat_id=c.id
                                 left JOIN user_info as u on u.user_id=m.user_id  
                                 WHERE     c.`id` = '$_REQUEST[chat_id]' AND  m.id>$chat_detail_id AND m.user_id=0
                                 ORDER BY  m.id asc
                                 LIMIT 1");

                $res = $db->getResultArray();
                $ge = $res[result][0];
                
                
                

                
                if(!empty($ge['text'])) 
                    $ge[1] = '<tr style="height: 17px;">
                	            <td colspan="2" style=" word-break: break-word;">
                                    <div style="background: rgba(33, 33, 33, 0.04) none repeat scroll 0 0;border-radius: 15px;padding: 7px 15px;color: #212121; max-width: auto; max-width: 85%; float: left; overflow-wrap: break-word;box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);">
                        	            <p style="font-weight: bold; font-size: 14px;">'.$ge[name].'</p>
                                        <p style="padding-bottom: 7px; line-height: 20px;font-size: 14px;">'.$ge['text'].'</p>
                                        <div style="padding-top: 7px;">
                                            <p style="width: 100%; color: #878787;">'.$ge[message_datetime].'</p>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr style="height:10px;"></tr>';
                    
                $ii = $ge[id];
                if (strpos($ge[text], 'შემოუერთდა ჩატს.') === FALSE) {
                    $name = $ge[name];
                }else{
                    $name = '';
                }
            }
         }elseif($_REQUEST['source']=='fbm'){
             
            $db->setQuery("SELECT   id 
                           FROM    `fb_chat` 
                           ORDER BY id DESC");
            
            $req1 = $db->getResultArray();
            
            $i=0;
            $first_id = 0;
            foreach ($req1[result] AS $result1){
                $i++;
                $first_id = $result1[id];
            }
            
            if($_REQUEST[chat_id] != 0){
                $ii = 0;
                $chat_detail_id = $_REQUEST[chat_detail_id];
                
                $db->setQuery("SELECT   `m`.`id`,
                                        `text`,
                                         IFNULL(u.`name`,`sender_name`) as name,
                                        `datetime` as 'message_datetime',
                                        (SELECT url from fb_attachments WHERE messages_id=m.id LIMIT 1) as media,
                                        (SELECT type from fb_attachments WHERE messages_id=m.id LIMIT 1) as media_type
                               FROM     `fb_chat` as c
                               JOIN     `fb_messages` as m ON m.fb_chat_id=c.id
                               LEFT JOIN user_info as u on u.user_id = m.user_id  
                               WHERE     c.`id` = '$_REQUEST[chat_id]' AND  m.id>$chat_detail_id AND ISNULL(m.user_id)
                               ORDER BY  m.id asc
                               LIMIT 1");
                
                $req = $db->getResultArray();
                $ge  = $req[result][0];
                //if(!empty($ge['media'])) $ge[text] = $ge[text]. "<a href='$ge[media]' target='blank'><img style='border-radius: 10%;max-height: 200px;' src='$ge[media]'></a>"; 
                if ($ge[media_type] == 'fallback') {
                    if(!empty($ge['media'])) $ge[text] = $req[text]. "<a href='$ge[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='https://crm.my.ge/media/images/icons/file_icons.png'></a>";
                }elseif ($ge[media_type] == 'file'){
                    if(!empty($ge['media'])) $ge[text] = $req[text]. "<a href='$ge[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='https://crm.my.ge/media/images/icons/file_icons.png'></a>";
                }elseif ($ge[media_type] == 'video'){
                    if(!empty($ge['media'])) $ge[text] = $req[text]. "<a href='$ge[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='https://crm.my.ge/media/images/icons/file_icons.png'></a>";
                }else{
                    if(!empty($ge['media'])) $ge[text] = $ge[text]. "<a href='$ge[media]' target='blank'><img style='border-radius: 10%;max-height: 200px;' src='$ge[media]'></a>";
                }
                if(!empty($ge['text'])) 
                    $ge[1] = '<tr style="height: 17px;">
                	            <td colspan="2" style=" word-break: break-word;">
                                    <div style="background: rgba(33, 33, 33, 0.04) none repeat scroll 0 0;border-radius: 15px;padding: 7px 15px;color: #212121; max-width: auto; max-width: 85%; float: left; overflow-wrap: break-word;box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);">
                        	            <p style="font-weight: bold; font-size: 14px;">'.$ge[name].'</p>
                                        <p style="overflow-x: auto; padding-bottom: 7px; line-height: 20px;font-size: 14px;">'.$ge['text'].'</p>
                                        <div style="padding-top: 7px;">
                                            <p style="width: 100%; color: #878787;">'.$ge[message_datetime].'</p>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr style="height:10px;"></tr>';
                $ii = $ge[id];
                if (strpos($ge[text], 'შემოუერთდა ჩატს.') === FALSE) {
                    $name = $ge[name];
                }else{
                    $name = '';
                }
            }
        }elseif($_REQUEST['source']=='mail'){
            $db->setQuery("SELECT id FROM `mail_chat` ORDER BY id DESC");
            $req1 = $db->getResultArray();
            $i=0;
            $first_id = 0;
            foreach ($req1[result] AS $result1){
                $i++;
                $first_id = $result1[id];
            }
            if($_REQUEST[chat_id] != 0){
                $ii = 0;
                $chat_detail_id = $_REQUEST[chat_detail_id] ;
                
                $db->setQuery("SELECT   `m`.`id`,
                                        `text`,
                                         IFNULL(u.`name`,`sender_name`) as name,
                                        `datetime` as 'message_datetime',
                                        `m`.subject
                               FROM     `mail_chat` as c
                               JOIN     `mail_messages` as m ON m.mail_chat_id=c.id
                               LEFT JOIN user_info as u on u.user_id = m.user_id 
                               WHERE     c.`id` = '$_REQUEST[chat_id]' AND  m.id>'$chat_detail_id' AND ISNULL(m.user_id)
                               ORDER BY  m.id ASC
                               LIMIT 1");
                
                $req = $db->getResultArray();
                $ge  = $req[result][0];
                $db->setQuery("SELECT `name`, SUBSTRING_INDEX(patch,  'htdocs/',-1) AS `patch` FROM `mail_attachments` WHERE messages_id='$ge[id]'");
                $fq  = $db->getResultArray();
                $ge['text'] = preg_replace('/<base[^>]+href[^>]+>/', '', $ge['text']);
                foreach($fq[result] AS $file){
                    
                    
                    $ge['text'] = preg_replace('/(?<=cid:'.$file[name].').*?(?=>)/', $file[patch].'"',  $ge['text']);//preg_grep('/(?<=cid:).*?(?=@)/', array($req['4'],'123'));
                    $ge['text'] = str_replace('cid:'.$file[name], 'https://crm.my.ge/', $ge['text']);
                    $file[patch]    = str_replace('/var/www/html/','',$file[patch]);						
                    $ge['text']     = $ge['text']. "<a href='https://crm.my.ge/$file[patch]'>$file[name]</a><br>"; 
                    
                } 
                $subject = '';
                if($ge[subject] != ''){
                    $subject = 'თემა: '.$ge[subject];
                }
                if(!empty($ge['text'])) 
                  $ge[1] = '<tr style="height: 17px;">
                	            <td colspan="2" style=" word-break: break-word;">
                                    <div style="background: rgba(33, 33, 33, 0.04) none repeat scroll 0 0;border-radius: 15px;padding: 7px 15px;color: #212121; max-width: auto; max-width: 85%; float: left; overflow-wrap: break-word;box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);">
                        	            <p style="font-weight: bold; font-size: 14px;">'.$ge[name].'</p>
                                        <p style="overflow-x: auto; font-weight: bold; font-size: 14px;">'.$subject.'</p>
                                        <p style="overflow-x: auto; padding-bottom: 7px; line-height: 20px;font-size: 14px;">'.$ge['text'].'</p>
                                        <div style="padding-top: 7px;">
                                            <p style="width: 100%; color: #878787;">'.$ge[message_datetime].'</p>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr style="height:10px;"></tr>';
                            
                $ii = $ge[id];
                if (strpos($ge[text], 'შემოუერთდა ჩატს.') === FALSE) {
                    $name = $ge[name];
                }else{
                    $name = '';
                }
            }
        }else{   
            $db->setQuery("SELECT id FROM `chat` WHERE `status` = 1 ORDER BY id DESC");
            $req1 = $db->getResultArray();
            $i=0;
            $first_id = 0;
            foreach($req1[result] AS $result1){
                $i++;
                $first_id = $result1[id];
            }
            
            
            if($_REQUEST[chat_id] != 0){
                $ii = 0;
                $db->setQuery(" SELECT    `chat_details`.`id`,
                                           chat_details.message_client,
                                          `chat`.`name`,
                                           chat_details.message_datetime,
                                           message_operator,
                                           chat_nikname.`name` AS oper_name,
                                           chat.seen
                                 FROM     `chat_details`
                                 JOIN     `chat` ON chat_details.chat_id = chat.id AND chat.status NOT IN(3,4)
                                 LEFT JOIN chat_nikname ON chat_details.operator_user_id = chat_nikname.crystal_users_id AND chat_nikname.actived = 1
                                 WHERE    `chat_details`.`chat_id` = '$_REQUEST[chat_id]' AND chat_details.operator_user_id != '$user_id' ORDER BY chat_details.id DESC LIMIT 1");
                
                $req = $db->getResultArray();
                $ge  = $req[result][0];
                
                $ii = $ge[id];
                if (strpos($ge[message_client], 'შემოუერთდა ჩატს.') === FALSE) {
                    $name = $ge[name];
                }else{
                    $name = '';
                }
                
                $ge[1] = '<tr style="height: 17px;">
                	            <td colspan="2" style=" word-break: break-word;">
                                    <div style="background: rgba(33, 33, 33, 0.04) none repeat scroll 0 0;border-radius: 15px;padding: 7px 15px;color: #212121; max-width: auto; max-width: 85%; float: left; overflow-wrap: break-word;box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);">
                        	            <p style="font-weight: bold; font-size: 14px;">'.$ge[name].'</p>
                                        <p style="overflow-x: auto; padding-bottom: 7px; line-height: 20px;font-size: 14px;">'.$ge['message_client'].'</p>
                                        <div style="padding-top: 7px;">
                                            <p style="width: 100%; color: #878787;">'.$ge[message_datetime].'</p>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr style="height:10px;"></tr>';
            }
        }
        
        $data = array('page'	=> $page, 'count' => $i, 'first_id' => $first_id, 'color'=>$color, 'chat_detail_id'=>$ii, 'sms'=>$ge[1], 'name'=>$name, 'message_datetime'=>$ge[message_datetime],'message_operator'=>$ge[message_operator],'oper_name'=>$ge[oper_name],'chat_seen'=>$ge[seen]);

		break;  
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;
echo json_encode($data);


/* ******************************
 *	Request Functions
* ******************************
*/
function get_color($i){
    if(strlen($i) == 2){
        $i = substr($i,1);
    }elseif(strlen($i) == 3){
        $i = substr($i,2);
    }else{
        $i = $i;
    }
    switch ($i) {
        case 1:
            return '#BBDAFF';
        break;
        case 2:
            return '#CEF0FF';
        break;
        case 3:
            return '#ACF3FD';
        break;
        case 4:
            return '#B5FFFC';
        break;
        case 5:
            return '#A5FEE3';
        break;
        case 6:
            return '#B5FFC8';
        break;
        case 7:
            return '#7CEB98';
        break;
        case 8:
            return '#99FD77';
        break;
        case 9:
            return '#95FF4F';
        break;
        case 10:
            return '#EDEF85';
        break;
        default:
            return '#FDFEFF';
    }
}
?>