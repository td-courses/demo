<?php
require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();
$action = $_REQUEST['act'];
$error = '';
$data = '';

switch ($action) {
    case 'get_add_page':
        $page = GetPage();
        $data = array('page' => $page);

        break;
    case 'get_edit_page':
        $source_id = $_REQUEST['id'];
        $page = GetPage(Getsource($source_id));
        $data = array('page' => $page);

        break;
    case 'get_list' :
        $count = $_REQUEST['count'];
        $hidden = $_REQUEST['hidden'];

        $db->setQuery("SELECT 	id,
                				date,
                				phone,
                				`content`,
                				if(`status`=2,'გასაგზავნია',IF(`status`=1,'გაგზავნილია',''))
                        FROM `sent_sms`
                        WHERE status = 1");

        $data = $db->getList($count, $hidden);

        break;
    case "send_sms_easy":
        $data = array('data' => easySmsSendPage());
        break;
    case "send_sms_complicated":
        $data = array('data' => complicatedSmsSendPage());
        break;
    case 'save_source':
        $source_id = $_REQUEST['id'];
        $source_name = $_REQUEST['name'];
        $content = $_REQUEST['content'];


        if ($source_name != '') {

            if ($source_id == '') {
                if (!ChecksourceExist($source_name, $source_id)) {
                    Addsource($source_name, $content);
                } else {
                    $error = '"' . $source_name . '" უკვე არის სიაში!';

                }
            } else {
                Savesource($source_id, $source_name, $content);
            }

        }

        break;
    case 'disable':
        $source_id = $_REQUEST['id'];
        Disablesource($source_id);

        break;
    case 'get_shablon':

        $data = array('shablon' => getShablon());

        break;
    case 'get_smsevent':

        $user_id = $_SESSION['USERID'];
        $test = $_REQUEST['test'];
        $sms_id = $_REQUEST['sms_id'];
        $res = mysql_query("SELECT  id
						   FROM  `sms_event`
						   WHERE `status` = 1");

        if (mysql_num_rows($res) > 0) {
            while ($row = mysql_fetch_assoc($res)) {
                mysql_query("UPDATE `sms_event` 
								SET 
									`user_id`='$user_id', 
									`sms_id`='$sms_id', 
									`content`='$test', 
									`date`= NOW(), 
									`status`='1'
							 WHERE (`id`='$row[id]');");

                $sms_event_event = $row[id];
                $data = array('insert' => $sms_event_event);
            }
        } else {
            mysql_query("INSERT INTO `sms_event`
								(`user_id`, `actived`, `sms_id`, `content`, `date`, `status`)
							VALUES
								('$user_id', 1, '$sms_id', '$test', NOW(), 1);");

            $sms_event_id = mysql_insert_id();
            $data = array('insert' => $sms_event_id);

        }


        break;
    case 'phone_directory':

        $html = getShablon();
        $data = array('html' => $html);

        break;
    case 'phone_dir_list':

        //data variables
        $html = phoneDirList();
        $data = array('html' => $html);

        break;
    case 'get_phone_dir_list':

        $count = $_REQUEST['count'];
        $hidden = $_REQUEST['hidden'];

        $rResult = mysql_query("SELECT 	persons_phone.id,
                                                persons_phone.name,
                                                person_position AS position,
                                                persons_phone.phone_number AS number
                                      FROM persons_phone
                                      LEFT JOIN position ON position.id=persons_phone.position_id");

        $data = array(
            "aaData" => array()
        );

        while ($aRow = mysql_fetch_array($rResult)) {
            $row = array();
            for ($i = 0; $i < $count; $i++) {
                /* General output */
                $row[] = $aRow[$i];
                if ($i == ($count - 1)) {
                    $row[] = '<div class="callapp_checkbox">
                                  <input type="checkbox" id="pdl_checkbox_' . $aRow[$hidden] . '" name="check_' . $aRow[$hidden] . '" value="' . $aRow['number'] . '" class="pdl-check" />
                                  <label for="pdl_checkbox_' . $aRow[$hidden] . '"></label>
                              </div>';
                }
            }
            $data['aaData'][] = $row;
        }

        break;
    case 'new_number_form':

        $html = newNumberInDirectoryForm();
        $data = array('html' => $html);

        break;
    case 'save_new_phone_in_directory':

        //data variables
        $name = $_REQUEST["name"];
        $position = $_REQUEST["position"];
        $number = $_REQUEST["number"];

        //check if similar data exist in database
//         if (phone_num_exists($number)) {
//             $data_return = "exists";

//             //if not exists
//         } else {

//             //check if data was saved successfully
//             if (save_phone_in_dir($name, $position, $number)) {
//                 $data_return = "saved";
//             } else {
//                 $data_return = "error";
//             }
//         }

//         //send result to client side
//         $data = array('data' => $data_return);

        break;

    case 'get_complicated_sms_list' :

        $sms_send_event_id = $_REQUEST["sms_send_event_id"];

        $rResult = mysql_query("SELECT CONCAT('d',hard_sms_event_detail.id) AS sms_name,
									   hard_sms_event_detail.id,
									   hard_sms_event_detail.phone,
									   IF(hard_sms_event_detail.`status`=2,'გაგზავნილი',IF(hard_sms_event_detail.`status`=1,'გასაგზავნია','არ გაიგზავნა')) AS sms_status
								FROM   hard_sms_event_detail
								WHERE hard_sms_event_detail.sms_event_id='$sms_send_event_id'");
        $collector = "";

        while ($aRow = mysql_fetch_array($rResult)) {
            $row = array();
            for ($i = 0; $i < 4; $i++) {
                /* General output */
                $row[] = $aRow[$i];
            }
            $data['aaData'][] = $row;
        }

        break;

    default:
        $error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Category Functions
* ******************************
*/

function Addsource($source_name, $content)
{
    global $db;
    $user = $_SESSION['USERID'];
    $db->setQuery("INSERT INTO 	 	`sms`
									(`name`, `message`, `user_id`, `actived`)
							VALUES 		
									('$source_name', '$content', '$user', '1')");
    $db->execQuery();
    //GLOBAL $log;
    //$log->setInsertLog('template');
}

function Savesource($source_id, $source_name, $content)
{
    global $db;
    //GLOBAL $log;
    //$log->setUpdateLogAfter('template', $template_id);
    $user_id = $_SESSION['USERID'];
    $db->setQuery("	UPDATE `sms`
					SET    `name` = '$source_name',
						   `message`='$content'
					WHERE  `id` = $source_id");
    $db->execQuery();
    //$log->setInsertLog('template',$template_id);
}

function Disablesource($source_id)
{
    global $db;
    //GLOBAL $log;
    //$log->setUpdateLogAfter('template', $template_id);
    $db->setQuery("	UPDATE `sms`
					SET    `actived` = 0
					WHERE  `id` = $source_id");
    $db->execQuery();
    //$log->setInsertLog('template',$template_id);
}

function Getsource($source_id)
{
    global $db;
    $db->setQuery("	SELECT  sms_event.id,
							sms_shablon.`name`,
							sms_event.content
					FROM sms_event
					LEFT JOIN sms_shablon ON sms_shablon.id=sms_event.sms_id
					WHERE sms_event.actived=1 AND sms_event.id=$source_id");
    $res = $db->getResultArray();
    return $res[result][0];
}

function GetPage($res = '')
{
    global $db;
    if ($res[id] == '') {
        $hidde_div = "display: none;";
        $st = 'auto';
        $db->setQuery("INSERT INTO sms_event SET name = ''");
        $db->execQuery();
        $hidde = $db->getLastId();
        $db->setQuery("DELETE FROM sms_event WHERE id = $hidde");
        $db->execQuery();
        $button_hidde = '';
    } else {
        $hidde_div = '';
        $st = '438px';
        $hidde = $res[id];
        $button_hidde = 'display: none;';
    }
    $data = '
	<div id="dialog-form">
	 <div id="tabs_tabs" style="width: 99%; margin: 0 auto; height: auto;">
		<ul style="height: 22px;">
			<li><a style="height: 13px;" href="#tab-0">მარტივი დაგზავნა</a></li>
			<li><a style="height: 13px;" href="#tab-1">რთული დაგზავნა</a></li>
		</ul>
		<div id="tab-0">
	       <fieldset style="width: ' . $st . '; margin-left: -13px;">
			<legend>SMS</legend>
			    	<table class="dialog-form-table">
						<tr>
							<td style="width: 180px;"><label for="d_number">სახელი</label></td>
						</tr>
			    		<tr>
							<td style="width: 180px;">
								<input style="width: 160px;" type="text" id="sms_name" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="' . $res['name'] . '" disabled="disabled"/>
							</td>
							<td>
								
							</td>
							<td style="width: 69px; ' . $button_hidde . '">
								<button id="sms_shablon" class="center">შაბლონი</button>
							</td>
						</tr>
						<tr>
							<td style="width: 180px;"><label for="content">ტექსტი</label></td>
						</tr>
					
						<tr>
							
							<td colspan="6">	
								<textarea maxlength="150"  style="width: 406px; resize: vertical;" id="sms_text" class="idle" name="call_content" cols="300" rows="6" disabled="disabled">' . $res['content'] . '</textarea>
							</td>
						</tr>
						<tr>
							<td style="width: 85px;">
								<input style="width: 50px;" type="text" id="simbol_caunt" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="0/150"/>
							</td>
							<td>
							</td>
							
							<td style="width: 69px;">
							</td>
						</tr>
						<tr>
							<td style="' . $button_hidde . '">
								<button style="width: 87px;" id="send_sms" class="center">გაგზავნა</button>
							</td>
							<td style="' . $button_hidde . '">
								<input id="choose_file" type="file" name="choose_file" class="input" value="" style="display: none; "><button style="  width: 136px;" id="choose_button" value="">აირჩიეთ ფაილი</button>	
							</td>
							
							<td style="' . $button_hidde . '">
								<button style="width: 138px;" id="excel_template" class="center">ექსელის შაბლონი</button>
							</td>
						</tr>	
					</table>
		        </fieldset>
			   <fieldset style="width: 438px;   margin-top: 3px; margin-left: -13px; height: auto; ' . $hidde_div . ' ">
					<div id="dt_example" class="inner-table" style="width: 100%;">
			         <div id="container" style="width: 100%; margin-top: 22px;   height: 348px;">        	
			            <div id="dynamic">
			            	<h2 align="center"></h2>
			            	<table class="display" id="example_1">
			                    <thead >
			                        <tr style="height: 20px;" id="datatable_header">
			                            <th>ID</th>
			                            <th style="width: 20%;">#</th>
										<th style="width: 40%;">ადრესატი</th>
										<th style="width: 40%;">სტატუსი</th>
			                        </tr>
			                    </thead>
			                    <thead>
			                        <tr class="search_header">
			                            <th class="colum_hidden">
			                            <th>
			                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 50px; height: 15px;  padding: 0;   margin:  0 auto;"/>
			                            </th>
			                          	<th>
			                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="height: 15px;  padding: 0;  margin:  0 auto;"/>
			                            </th>
										<th>
			                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="height: 15px;  padding: 0;  margin:  0 auto;"/>
			                            </th>
										<th>
			                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="height: 15px;  padding: 0;  margin:  0 auto;"/>
			                            </th>
			                        </tr>
			                    </thead>
			                </table>
			            </div>
			        </div>
		        </fieldset>
				</div>
				<div style="margin-top: 15px;" id="tab-1">
			    
					<div id="dt_example" class="inner-table" style="width: 433px;">
			         <div id="container" style="width: 100%; margin-top: 22px;   height: auto;">        	
			            <div id="dynamic">
			            	<h2 align="center"></h2>
			            	<table class="display" id="example_2">
			                    <thead >
			                        <tr style="height: 20px;" id="datatable_header">
			                            <th>ID</th>
			                            <th style="width: 20%;">#</th>
										<th style="width: 40%;">ადრესატი</th>
										<th style="width: 40%;">სტატუსი</th>
			                        </tr>
			                    </thead>
			                    <thead>
			                        <tr class="search_header">
			                            <th class="colum_hidden">
			                            <th>
			                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 50px; height: 15px;  padding: 0;   margin:  0 auto;"/>
			                            </th>
			                          	<th>
			                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="height: 15px;  padding: 0;  margin:  0 auto;"/>
			                            </th>
										<th>
			                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="height: 15px;  padding: 0;  margin:  0 auto;"/>
			                            </th>
										<th>
			                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="height: 15px;  padding: 0;  margin:  0 auto;"/>
			                            </th>
			                        </tr>
			                    </thead>
			                </table>
			            </div>
			        </div>
		        
			   	   <table class="dialog-form-table">
						<tr>
							
			   				<td>
								<button style="width: 87px;" id="send_sms_hard" class="center">გაგზავნა</button>
							</td>
			   				<td>
								
							</td>
							<td>
							    <label for="hard_choose_file">click</label>
								<input id="hard_choose_file" type="file" name="choose_file" class="input" value="" style="display: none; "><button style="  width: 136px; margin-left:74px;" id="hard_choose_button" value="">აირჩიეთ ფაილი</button>	
							</td>
							
							<td>
								<button style="width: 138px;" id="hard_excel_template" class="center">ექსელის შაბლონი</button>
							</td>
						</tr>	
					</table>
		    </div>
		</div>
	</div>
</div>	
	<input id="sms_send_event_id" type="text" value="' . $hidde . '" style="display: none;">
    ';
    return $data;
}

function increment($table)
{

    $result = mysql_query("SHOW TABLE STATUS LIKE '$table'");
    $row = mysql_fetch_array($result);
    $increment = $row['Auto_increment'];
    $next_increment = $increment + 1;
    mysql_query("ALTER TABLE '$table' AUTO_INCREMENT=$next_increment");

    return $increment;
}

/*================================================================= NEW FUNCTIONS ============================================================*/

// easy mailing page structure
function easyMailingPage()
{

    return '<table class="display" id="easy_mailing_table" >
                <thead>
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width: 100%;">თარიღი</th>
                        <th style="width: 100%;">ადრესატი</th>
                        <th style="width: 100%;">ტექსტი</th>
                        <th style="width: 100%;">სტატუსი</th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                           <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_number" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                        </th>
                    </tr>
                </thead>
            </table>';

}

// easy mailing page structure

// complicate mailing page structure
function complicateMailingPage()
{

    return '<table class="display" id="complicated_mailing_table" >
                <thead>
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width: 100%;">თარიღი</th>
                        <th style="width: 100%;">ადრესატი</th>
                        <th style="width: 100%;">ტექსტი</th>
                        <th style="width: 100%;">სტატუსი</th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                           <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_number" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                        </th>
                    </tr>
                </thead>
            </table>';

}

// complicate mailing page structure

// easy sms sending dialog structure
function easySmsSendPage()
{

    return '<fieldset class="new-sms-send-container">

                <!-- label for fieldset -->
                <legend>SMS</legend>
                
                 <!-- row unit -->
                <div class="new-sms-row">
	                <button class="jquery-ui-button" id="smsTemplate">
	                    <span>შაბლონი</span>
                    </button>
	                <button style="display:none" class="jquery-ui-button" id="smsPhoneDir">
	                    <span>ცნობარი</span>
	                </button>
                </div>
	            
	            <!-- row unit -->
	            <div class="new-sms-row">
	                <label for="smsAddressee">ადრესატი</label>
	                <div class="new-sms-input-holder">
	                    <input type="text" id="smsAddressee">
                    </div>
                </div>
                
                <!-- row unit -->
                <div class="new-sms-row">
	                <label for="smsText">ტექსტი</label>
	                <div class="new-sms-input-holder">
	                    <textarea id="newSmsText" maxlength="150"></textarea>
                    </div>
                </div>
                
                <!-- row unit -->
                <div class="new-sms-row">
	                <input type="text" id="smsCharCounter" data-limit="150" value="0/150">
	                <button class="jquery-ui-button" id="sendNewSms">
	                    <span>გაგზავნა</span>
                    </button>
                </div>
	            
            </fieldset>';

}

// easy sms sending dialog structure


// complicate sms sending dialog structure
function complicatedSmsSendPage()
{

    return '<fieldset class="new-sms-send-container">

                <!-- label for fieldset -->
                <legend>SMS</legend>
                
                 <!-- row unit -->
                <div id="dt_example" class="inner-table" style="width: 100%;">
			         <div id="container" style="width: 100%; margin-top: 22px;   height: auto;">        	
			            <div id="dynamic">
			            	<h2 align="center"></h2>
			            	<table class="display" id="example_2">
			                    <thead >
			                        <tr style="height: 20px;" id="datatable_header">
			                            <th>ID</th>
			                            <th style="width: 20%;">#</th>
										<th style="width: 40%;">ადრესატი</th>
										<th style="width: 40%;">სტატუსი</th>
			                        </tr>
			                    </thead>
			                    <thead>
			                        <tr class="search_header">
			                            <th class="colum_hidden">
			                            <th>
			                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 50px; height: 15px;  padding: 0;   margin:  0 auto;"/>
			                            </th>
			                          	<th>
			                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="height: 15px;  padding: 0;  margin:  0 auto;"/>
			                            </th>
										<th>
			                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="height: 15px;  padding: 0;  margin:  0 auto;"/>
			                            </th>
										<th>
			                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="height: 15px;  padding: 0;  margin:  0 auto;"/>
			                            </th>
			                        </tr>
			                    </thead>
			                </table>
			            </div>
			        </div>
		        
			   	   <table class="dialog-form-table" style="width: 100%; table-layout: fixed;">
						<tr>
							
			   				<td>
								<button style="width: 87px; font-family: pvn; font-size: 12px; float: left;" id="send_sms_hard" class="center jquery-ui-button">გაგზავნა</button>
							</td>
			   				<td>
								
							</td>
							<td style="width: 150px;">
								<input id="hard_choose_file" type="file" name="choose_file" class="input" value="" style="display: none; "><button style="  width: 136px; font-family: pvn; font-size: 12px; float: right;" id="hard_choose_button" class="jquery-ui-button" value="">აირჩიეთ ფაილი</button>	
							</td>
							
							<td style="width: 145px;">
								<button style="width: 138px; font-family: pvn; font-size: 12px; float: right;" id="hard_excel_template" class="center jquery-ui-button">ექსელის შაბლონი</button>
							</td>
						</tr>	
					</table>
		    </div>
	            
            </fieldset>';

}

// complicate sms sending dialog structure


// sms sending shablon
function getShablon()
{
    global $db;
    $db->setQuery("SELECT 	sms.id,
        					sms.`name`,
        					sms.`message`
				    FROM 	sms
				    WHERE 	sms.actived=1 ");
    $req = $db->getResultArray();
    $data = '<table id="box-table-b1">
				<tr class="odd">
					<th style="width: 26px;">#</th>
					<th style="width: 160px;">ტექსტი</th>
					<th>ქმედება</th>
				</tr> ';

    foreach($req[result] AS $res3) {

        $data .= '<tr class="odd">
					<td>' . $res3[id] . '</td>
					<td style="width: 30px !important;">' . $res3['name'] . '</td>
					<td style="font-size: 10px !important;">
					    <button style="width: 45px;" class="download_shablon" sms_id="' . $res3['id'] . '" data-message="' . $res3['message'] . '">არჩევა</button>
                    </td>
				 </tr>';

    }
    $data .= '</table>';
    return $data;
}

// sms sending shablon


//returns structure of phone directory list
function phoneDirList()
{

    return '<table class="display" id="phoneDirectoryList" style="width: 100%;">
                <thead>
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width: 38%;">სახელი</th>
                        <th style="width: 37%;">თანამდებობა</th>
                        <th style="width: 23%;">ტელ. ნომერი</th>
                        <th style="width: 3%;">#</th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                           <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                        </th>             
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>            
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <div class="callapp_checkbox">
                              <input type="checkbox" id="pdl_check_all" name="pdl_check_all" />
                              <label for="pdl_check_all"></label>
                            </div>
                        </th>   
                    </tr>
                </thead>
            </table>';

}

//returns structure of phone directory list


//returns the form of adding new number in phone directory
function newNumberInDirectoryForm()
{

    return '
        <div id="dialog-form">
            <fieldset>
                <legend>ძირითადი ინფორმაცია</legend>
    
                <table class="dialog-form-table">
                    <tr>
                        <td style="width: 170px;"><label for="CallType">სახელი</label></td>
                        <td>
                            <input type="text" id="name" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 170px;"><label for="CallType">თანამდებობა</label></td>
                        <td>
                            <select name="position" id="position" data-select="chosen">
                                ' . getPositionList() . '
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 170px;"><label for="CallType">ტელ. ნომერი</label></td>
                        <td>
                            <input type="text" id="telNumber" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="" />
                        </td>
                    </tr>
    
                </table>
                
            </fieldset>
        </div>
    ';

}

//returns the form of adding new number in phone directory


//get position list
function getPositionList()
{
    global $db;
    $db->setQuery("	SELECT  `id`,
							`person_position`
					FROM    `position`");
    $query = $db->getResultArray();
    $options = "";

    foreach ($query[result] AS $row) {

        $options .= "<option value='" . $row['id'] . "'>" . $row['person_position'] . "</option>";
    }

    return $options;

}

//get position list

//check for phone number in phone directory
// function phone_num_exists($phone_number)
// {
//     global $db;
//     //define result variable
//     $result = false;

//     //query from database
//     $db->setQuery("SELECT phone_number
//                    FROM persons_phone
//                    WHERE phone_number = '$phone_number'");

//     //check for existing data length
//     $res_count = mysql_num_rows($q);

//     if ($res_count > 0) {
//         $result = true;
//     }

//     //return result
//     return $result;

// }

//save new phone number in directory
function save_phone_in_dir($_name, $_position, $_number)
{

    //define variables
    $user_id = $_SESSION['USERID'];
    $name = $_name;
    $position = $_position;
    $number = $_number;
    $query = "INSERT INTO persons_phone (user_id, datetime, name, position_id, phone_number, actived)
              VALUES ('" . $user_id . "', NOW(), '" . $name . "', '" . $position . "', '" . $number . "', '1')";

    //query to database
    if (mysql_query($query)) {
        $result = true;
    } else {
        $result = false;
    }

    return $result;

}

?>

