<?php
require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db     = new dbClass();
$data = array();

// Queue
$db->setQuery("SELECT  `number`
               FROM    `queue`
               WHERE   `actived` = 1");
$res = $db->getResultArray();                   
$data['queue'] =  '<option value="0">ყველა</option>';
foreach ($res[result] AS $req){
    $data['queue'] .=  '<option value="'.$req[number].'">'.$req[number].'</option>';
}

// Extension & User
$db->setQuery(" SELECT  extention.id,
                       `extention`,
                       `user_info`.`name`
                FROM   `extention`
                JOIN    users ON users.extension_id = extention.id
                JOIN    user_info ON user_info.user_id = users.id
                WHERE 	users.logged = 1 AND extention.actived = 1");

$res = $db->getResultArray();

$data['ext']  = '<option value="0">ყველა</option>';
$data['user'] = '<option value="0">ყველა</option>';
foreach($res[result] AS $req){
    $data['ext'] .=  '<option value="'.$req[id].'">'.$req[extention].'</option>';
    $data['user'] .=  '<option value="'.$req[name].'">'.$req[name].'</option>';
}

// State
$data['state'] =  '<option value="0">ყველა</option>';
$data['state'] .=  '<option value="flesh_off.png">მიუწდომელი</option>';
$data['state'] .=  '<option value="flesh_inc.png">დაკავებული</option>';
$data['state'] .=  '<option value="flesh_ringing.png">რეკავს</option>';
$data['state'] .=  '<option value="flesh_free.png">თავისუფალი</option>';

echo json_encode($data);
?>