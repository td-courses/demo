<?php

/* ******************************
 *	Request aJax actions
 * ******************************
 */

date_default_timezone_set('Asia/Tbilisi');
require_once('../../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();
$action 	= $_REQUEST['act'];
$error		= '';
$data		= array();
$status 	= '';

$id			= $_REQUEST['id'];

switch ($action) {
    case 'get_add_page':
        $res = '';
        $page = GetPage($res);
        $data = array('page' => $page);
        
        break;
    
    case 'get_list' :
        $count 				= $_REQUEST['count'];
        $hidden 			= $_REQUEST['hidden'];
        $start				= $_REQUEST['start'];
        $end				= $_REQUEST['end'];
        $incomming_call_id	= $_REQUEST['incomming_call_id'];
        
        $db->setQuery("SELECT    task.id,
                                 task.task_date,
                                 task.task_start_date,
                                 task.task_end_date,
                                 task_status.`name` AS `status_name`,
                                 user_info.`name`
                       FROM      task
                       LEFT JOIN task_status on task.task_status_id = task_status.id
                       LEFT JOIN user_info on user_info.user_id = task.task_recipient_id
                       WHERE     task.incomming_call_id != 0 AND task.incomming_call_id = '$incomming_call_id' AND task.actived = 1
                       UNION ALL
                       SELECT    outgoing_call.id,
                                 outgoing_call.date AS `task_date`,
                                 outgoing_call.out_start AS `task_start_date`,
                                 outgoing_call.out_end AS `task_end_date`,
                                `status`.`name`  AS `status_name`,
                                 user_info.`name`
                       FROM      outgoing_call
                       LEFT JOIN status on status.id = outgoing_call.status_id
                       LEFT JOIN user_info on user_info.user_id = outgoing_call.recepient_id
                       WHERE     NOT ISNULL(outgoing_call.incomming_call_id) AND outgoing_call.outgoing_type_id=1 AND outgoing_call.incomming_call_id = '$incomming_call_id' and outgoing_call.actived = 1");
        
            $data = $db->getList($count, $hidden);
            
            break;
    case 'save_task':
        $task_type             = $_REQUEST['task_type'];
        $task_status_id        = $_REQUEST['task_status_id'];
        $task_branch           = $_REQUEST['task_branch'];
        $task_recipient        = $_REQUEST['task_recipient'];
        $task_create_date      = $_REQUEST['task_create_date'];
        $task_start_date       = $_REQUEST['task_start_date'];
        $task_end_date         = $_REQUEST['task_end_date'];
        $task_description      = $_REQUEST['task_description'];
        $task_note             = $_REQUEST['task_note'];
        $incomming_call_id     = $_REQUEST['incomming_call_id'];
        $site                  = $_REQUEST['site'];
        
        $task_id = Addtask($task_type,$task_status_id,$task_branch,$task_recipient,$task_create_date,$task_start_date,$task_end_date,$task_description,$task_note,$incomming_call_id);

        
        if ($task_type == 2) {
            $db->setQuery("SELECT status.`name`
                           FROM   status
                           WHERE  actived = 1 AND id = '$task_status_id'");
            
            $res_status  = $db->getResultArray();
            $status_name = $res_status[result][0][name];
        }else{
            $db->setQuery("SELECT task_status.`name`
                           FROM   task_status
                           WHERE  actived = 1 AND id = '$task_status_id'");
            
            $res_status  = $db->getResultArray();
            $status_name = $res_status[result][0][name];
        }
        
//         $send  = 0;
//         if ($check>0) {
//             $send=1;
//         }
$db->setQuery("SELECT NOW() AS `time`;");
$res = $db->getResultArray();
        $data = array('task_id' => $task_id, 'task_type' => $task_type, 'status_name' => $status_name, 'time' => $res[result][0]['time']);
        break;
    case 'task_type_changed':
        $page = get_task_status($_REQUEST['cat_id']);
        $data = array('page'	=> $page);
        
        break;
    case 'task_branch_changed':
        $page = get_task_recipient($_REQUEST['cat_id'],$_REQUEST['position_id']);
        $data = array('page'	=> $page);
        
        break;
    case 'task_branch_changed_2':
        $page        = '<option value="0" >-------------</option>';
        $id	 		 = $_REQUEST['cat_id'];
        $position_id = $_REQUEST['position_id'];
        $where       = '';
        
        if($id != 0  && $position_id == 0){
            $where = "AND user_info.branch_id = '$id'";
        }else if ($id == 0 && $position_id != 0){
            $where = "AND user_info.position_id = '$position_id'";
        }else if ($id != 0 && $position_id != 0){
            $where = "AND user_info.branch_id = '$id'
            AND user_info.position_id = '$position_id'";
        }else if ($id == 0 && $position_id == 0){
            $where ="";
        }
        
        $db->setQuery("SELECT user_info.user_id, 
                              user_info.`name`
                       FROM   user_info
                       JOIN   users on user_info.user_id = users.id
                       WHERE  users.actived = 1
                       $where");
        
        $res = $db->getResultArray();
        foreach($res[result] AS $res){
            $page .= '<option value="' . $res['user_id'] . '">' . $res['name'] . '</option>';
        }
        
        $data		= array('page'	=> $page);
        
        break;
    case 'get_task_status_2':
        $page		= get_task_status_2($_REQUEST['cat_id'],'');
        $data		= array('page'	=> $page);
        
        break;
    default:
        $error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Request Functions
 * ******************************
 */
function get_select($id,$table,$row='name',$where='actived = 1'){
    global $db;
    $data = '<option value="0" >-------------</option>';
    
    $db->setQuery("SELECT `id`,
                          `$row`
                   FROM   `$table`
                   WHERE   $where");
    
    $req = $db->getResultArray();
    
    $data .= '';
    foreach  ($req[result] AS $res) {
        if ($res['id'] == $id) {
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res[$row] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res[$row] . '</option>';
        }
    }
    
    return $data;
}

function get_task_status($id){
    switch($id){
        case 1:
            $data = get_select('','task_status','name','actived = 1 and parent_id = 0');
            break;
        case 2:
            $data = get_select('','status','name','actived = 1');
            break;
        default:
            $data = "<option value='0'>-----------</option>";
    }
    return $data;
    
}

function get_user_select($id){
    global $db;
    $data = '<option value="0" >-------------</option>';
    
    $db->setQuery("SELECT users.`id`,
						  user_info.`name`
				   FROM   user_info
				   JOIN   users on users.id = user_info.user_id");
    
    $data = $db->getSelect($id);
    return $data;
}

function GetPosition($id){
    global $db;
    $data = '';
    $db->setQuery(" SELECT 	`id`,
						   	`person_position`
					FROM 	`position`
					WHERE 	`actived` = '1'");
    
    $data = '<option value="0" >-------------</option>';
    $data = $db->getSelect($id);
    
    return $data;
}

function get_task_recipient($id,$position_id){
    global $db;
    $where='';
    if($id != 0  && $position_id == 0){
        $where ="AND user_info.branch_id = '$id'";
        
    } else if ($id == 0 && $position_id != 0){
        $where ="AND user_info.position_id = '$position_id'";
        
    } else if ($id != 0 && $position_id != 0){
        $where ="AND user_info.branch_id = '$id'
        AND user_info.position_id = '$position_id'";
        
    } else if ($id == 0 && $position_id == 0){
        $where ="";
    }
    $data = '<option value="0" >-------------</option>';
    
    $users = array();
    $data .= '';
    $db->setQuery(" SELECT users.`id`,
                           user_info.`name`
                    FROM   user_info
                    JOIN   users ON users.id = user_info.user_id
                    WHERE  users.actived = 1
                    $where");
    
    $data = $db->getSelect($id);
        
    return $data;
}

function Addtask($task_type,$task_status_id,$task_branch,$task_recipient,$task_create_date,$task_start_date,$task_end_date,$task_description,$task_note,$incomming_call_id){
    global $db;
    
    $user		= $_SESSION['USERID'];
    $site       = $_REQUEST['site'];
    $site_array = explode(",",$site);
    
    $db->setQuery("DELETE FROM incoming_call_site WHERE incomming_call_id = '$incomming_call_id'");
    $db->execQuery();
    
    foreach($site_array AS $site_id){
        $db->setQuery("INSERT INTO incoming_call_site (incomming_call_id, site_id) values ('$incomming_call_id', $site_id)");
        $db->execQuery();
    }
    
    $task_status_id_2 = $_REQUEST['task_status_id_2'];
    
    
        switch($task_type){
            case 1:
                $db->setQuery("INSERT INTO `task`
                                          (`user_id`,`incomming_call_id`,`task_status_id`,`task_status_id_2`,`task_type_id`,`task_branch_id`,`task_recipient_id`,`task_date`,`task_start_date`,`task_end_date`,`task_description`,`task_note`)
                                    VALUES
                                          ('$user','$incomming_call_id','$task_status_id','$task_status_id_2','$task_type','$task_branch','$task_recipient','$task_create_date','$task_start_date','$task_end_date','$task_description','$task_note')");
                
                $db->execQuery();

                break;
            case 2:
                $db->setQuery("INSERT INTO `outgoing_call`
                                       SET `user_id`          = '$user',
                                           `date`             =  NOW(),
                                           `incomming_call_id`= '$incomming_call_id',
                                           `start_date`       =  NOW(),
                                           `out_start`        = '$task_start_date',
                                           `out_end`          = '$task_end_date',
                                           `recepient_id`     = '$task_recipient',
                                           `status_id`        = '$task_status_id',
                                           `branch_id`        = '$task_branch',
                                           `outgoing_comment` = '$task_note',
                                           `call_content`     = '$task_description',
                                           `actived`          = '1'");
                
                $db->execQuery();
                break;
        }
        
        return $db->getLastId();
}

function get_task_status_2($id,$child_id){
    global $db;
    $db->setQuery("SELECT  `id`,
                           `name`
                   FROM    `task_status`
                   WHERE    actived = 1 AND `parent_id` = '$id' AND `parent_id` != 0");
    
    $req = $db->getResultArray();
    $data .= '<option value="0" selected="selected">----</option>';
    $i = 0;
    foreach($req[result] AS $res){
        if($res['id'] == $child_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
        $i = 1;
    }
    if($i == 0 && $id > 0){
        $data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
    }
    
    return $data;
}

function GetPage($res=''){
    $data  .= '
	<!-- jQuery Dialog -->
	<div id="dialog-form">
        <fieldset>
            <p> ფორმირების თარიღი: '.date("Y-m-d H:i:s").' </p>
            <hr>
			<table id="task_info_table">
				<tr>
					<td style="width:230px;"><label for="task_type">დავალების ტიპი</label></td>
                    <td style="width:230px;"><label for="task_status_id">სტატუსი</label></td>
                    <td style="width:269px;"><label for="task_start_date">ქვე-სტატუსი</label></td>
				</tr>
				<tr>
					<td><select style="width: 210px;" data-select="chosen" id="task_type">'.get_select('','task_type').'</select></td>
					<td><select style="width: 210px;" data-select="chosen" id="task_status_id" readonly></select></td>
					<td><select style="width: 269px;" data-select="chosen" id="task_status_id_2">'.get_task_status_2('','').'</select></td>
				</tr>
                <tr style="height:10px;"></tr>
            </table>
            <table style="width:100%">
				<tr>
					<td style="width:230px;"><label for="task_recipient">დავალების მიმღები</label></td>
					<td style="width:230px;"><label for="task_branch">განყოფილება</label></td>
					<td style="width:269px;" colspan="2"><label style="text-align: center;" for="task_start_date">პერიოდი</label></td>
				</tr>
				<tr>
					<td><select style="width: 210px;" data-select="chosen" id="task_recipient">'.get_user_select('').'</select></td>
                    <td><select style="width: 210px;" data-select="chosen" id="task_branch" >'.get_select('','department').'</select></td>
					<td><input style="float: left; width: 120px;" id="task_start_date" type="text" value=""></td>
					<td><input style="float: left; width: 120px;" id="task_end_date" type="text" value=""></td>
				</tr>
                <tr style="height:10px;"></tr>
			</table>
			<table style="width:100%">
				<tr>
					<td><label for="task_description">კომენტარი</label></td>
				</tr>
				<tr>
					<td><textarea style="resize: vertical;width:97%" id="task_description"></textarea></td>
				</tr>
                <tr style="height:10px;"></tr>
				<tr>
					<td><label for="task_note">შედეგი</label></td>
				</tr>
				<tr>
					<td><textarea style="resize: vertical;width:97%" id="task_note"></textarea></td>
				</tr>
            </table>
            <input style="float: left;" id="task_create_date" type="hidden" value="'.(($res['task_date']=='')?date("Y-m-d H:i:s"):$res['task_date']).'">
		</fieldset>
	</div>';
    
    return $data;
}
?>