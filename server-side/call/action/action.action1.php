<?php

/* ******************************
 *	Request aJax actions
* ******************************
*/

require_once('../../../includes/classes/class.Mysqli.php');
global $db;
$db     = new dbClass();
$action 	= $_REQUEST['act'];
$error		= '';
$data		= array();

//action
$action_id			= $_REQUEST['id'];
$action_name		= $_REQUEST['action_name'];
$start_date			= $_REQUEST['start_date'];
$end_date			= $_REQUEST['end_date'];
$action_content	    = $_REQUEST['action_content'];

//task
$persons_id			    = $_REQUEST['persons_id'];
$task_type_id			= $_REQUEST['task_type_id'];
$priority_id			= $_REQUEST['priority_id'];
$comment 	        	= $_REQUEST['comment'];
$task_department_id 	= $_REQUEST['task_department_id'];
$hidden_inc				= $_REQUEST['hidden_inc'];
$edit_id				= $_REQUEST['edit_id'];
$delete_id				= $_REQUEST['delete_id'];



switch ($action) {
	case 'get_add_page':
		$number		= $_REQUEST['number'];
		$page		= GetPage($res='', $number);
		$data		= array('page'	=> $page);

		break;
	case 'disable':
		$db->setQuery("UPDATE `action`
						  SET `actived` = 0
					   WHERE  `id`      = $action_id ");
		$db->execQuery();
		
		$db->setQuery("UPDATE `action_history`
						  SET `actived`   = 0
					   WHERE  `action_id` = $action_id ");
		$db->execQuery();
		break;
	case 'get_edit_page':
		$page		= GetPage(Getaction($action_id));

		$data		= array('page'	=> $page);

		break;
	case 'get_list' :
		$count  = $_REQUEST['count'];
		$hidden = $_REQUEST['hidden'];
		$user	= $_SESSION['USERID'];
		$group	= checkgroup($user);
		
		$db->setQuery(" SELECT 	  action.id,
								  action.start_date,
								  action.end_date,
								  action.`name`,
                                 (SELECT GROUP_CONCAT(my_web_site.`name`) 
                                  FROM `action_site`
                                  JOIN  my_web_site ON action_site.site_id = my_web_site.id
                                  WHERE action_site.action_id = action.id) AS `site`,
								  action.content,
								  user_info.`name`,
                                  IF(action_history.id != '', '<div class=\"action-status read-action\">წაკითხული</div>', '<div class=\"action-status unread-action\">წაუკითხავი</div>') AS read_status
						FROM 	  action
						JOIN      user_info ON action.user_id=user_info.user_id
						LEFT JOIN action_history ON action_history.action_id = action.id AND action_history.user_id = '$user'
						WHERE 	  action.actived=1 AND action.end_date < NOW()");
	  
		$data = $db->getList($count, $hidden, 1);

		break;
	case 'save_action':
		
	
		if($action_id == ''){
			Addaction(  $action_name,  $start_date, $end_date, $action_content);
		}else {
			saveaction($action_id,  $action_name,  $start_date, $end_date, $action_content);
		}
		break;
	
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Request Functions
* ******************************
*/	

function checkgroup($user){
    global $db;
    $db->setQuery("SELECT users.group_id
                   FROM   users
                   WHERE  users.id = $user");
    
    $res = $db->getResultArray();
    
    return $res[result][0]['group_id'];
    
}

function saveaction($action_id,  $action_name,  $start_date, $end_date, $action_content){
	
	global $db;
	
	$user			= $_SESSION['USERID'];
	$h_start_date	= $_REQUEST['h_start_date'];
	$h_end_date	    = $_REQUEST['h_end_date'];
	
	$db->setQuery("UPDATE `action` 
                      SET `user_id`    = '$user',
						  `name`       = '$action_name',
						  `start_date` = '$start_date', 
						  `end_date`   = '$end_date', 
						  `content`    = '$action_content', 
						  `actived`    = '1' 
				   WHERE  `id`         = '$action_id'");
	
	$db->execQuery();
}       

function Getaction($action_id){
	global $db;
    $db->setQuery(" SELECT 	action.id,
    						action.`name` AS action_name,
    						action.start_date as start_date,
    						action.end_date as end_date,
    						action.content AS action_content
    				FROM 	action
    				WHERE 	action.id = $action_id");
	
    $res = $db->getResultArray();
	return $res[result][0];
}

function GetPage($res='', $number){
	
	$num = 0;
	if($res[phone]==""){
		$num=$number;
	}else{ 
		$num=$res[phone]; 
	}

	$data  .= '
	<!-- jQuery Dialog -->
    <div id="add-edit-goods-form" title="აქცია">
    	<!-- aJax -->
	</div>
	<div id="dialog-form">
		<div style="float: left; width: 580px;">	
			<fieldset >
		    	<legend>ინფო</legend>
				<fieldset float:left;">
			    	<table width="100%" class="dialog-form-table">
						<tr>
							<td>დასახელება</td>
							<td style="width:20px;></td>
							
							<td colspan "5">
								<input  type="text" id="action_name" class="idle" onblur="this.className=\'idle\'"  value="' . $res['action_name']. '"  />
							</td>
						</tr>
						<tr>
							<td style="width: 150px;"><label for="d_number">პერიოდი</label></td>
							<td>
								<input type="text" id="start_date" class="idle" onblur="this.className=\'idle\'" value="' . $res['start_date']. '" />
							</td>
							<td style="width: 150px;"><label for="d_number">-დან</label></td>
							<td>
								<input type="text" id="end_date" class="idle" onblur="this.className=\'idle\'"  value="' . $res['end_date']. '"  />
							</td>
							<td style="width: 150px;"><label for="d_number">-მდე</label></td>
						</tr>
					</table>
				</fieldset>
				<fieldset style="float: left; width: 400px;">
					<legend>აღწერა</legend>
			    		<table width="100%" class="dialog-form-table">
						<tr>
							<td colspan="5">
								<textarea  style="width: 530px; height: 100px; resize: none;" id="action_content" class="idle" name="content" cols="100" rows="2">' . $res['action_content'] . '</textarea>
							</td>
						</tr>		
						</table>
				</fieldset>	
			</div>
			<div style="float: right;  width: 360px;">
			</fieldset>
			<fieldset style="float: right;  width: 440px;">
				<legend>განყოფილებები</legend>
    			<div id="dt_example" class="inner-table">
    		        <div style="width:440px;" id="container" >        	
    		            <div id="dynamic">
    		            	<div id="button_area">
    		            		<button id="" style="display:none;">დამატება</button>
    	        			</div>
    		                <table class="" id="example6" style="width: 100%;">
    		                    <thead>
    								<tr  id="datatable_header">
    										
    		                           <th style="display:none">ID</th>
    									<th style="width:4%;">#</th>
    									<th style="width:25%; word-break:break-all;">ფილიალი</th>
    									<th style="width:20%; word-break:break-all;">მისამართი</th>
    								</tr>
    							</thead>
    							<thead>
    								<tr class="search_header">
    									<th class="colum_hidden">
                                			<input type="text" name="search_id" value="" class="search_init" style="width: 10px"/>
                                		</th>
    									<th>
    										<input style="width:100px;" type="text" name="search_overhead" value="ფილტრი" class="search_init" />
    									</th>
    									<th>
    										<input style="width:100px;" type="text" name="search_partner" value="ფილტრი" class="search_init" />
    									</th>
    								</tr>
    							</thead>
    		                </table>
    		            </div>
    		            <div class="spacer">
    		            </div>
    		        </div>
                </fieldset>
				<fieldset style="float: right;  width: 440px;">
				    <legend>აქციის პროდუქტები</legend>
					<div id="dt_example" style="width:440px;" class="display">       	
                        <div id="button_area">
                    		<button id="" style="display:none;">დამატება</button>
            			</div>
                        <table class="" id="example5" style="width: 100%;">
                            <thead>
        						<tr  id="datatable_header">
        								
                                   <th style="display:none">ID</th>
        							<th style="width:10%;">#</th>
        							<th style="width:25%;">ფილიალი</th>
        							<th style="width:15%;">თარიღი</th>
        							<th style="width:30%;">პროდუქტი</th>
        							<th style="width:20%;">თანხა</th>
        						</tr>
        					</thead>
        					<thead>
        						<tr class="search_header">
        							<th class="colum_hidden">
                            			<input type="text" name="search_id" value="" class="search_init" style="width: 80%"/>
                            		</th>
        							<th>
        								<input style="width:70px;" type="text" name="search_overhead" value="ფილტრი" class="search_init" style="width: 80%"/>
        							</th>
        							<th>
        								<input style="width:65px;" type="text" name="search_partner" value="ფილტრი" class="search_init" style="width: 80%"/>
        							</th>
        							<th>
        								<input style="width:100px;" type="text" name="search_overhead" value="ფილტრი" class="search_init" style="width: 80%"/>
        							</th>
        							<th>
        								<input style="width:30px;" type="text" name="search_partner" value="ფილტრი" class="search_init" style="width: 80%"/>
        							</th>
        							
        						</tr>
        					</thead>
        	            </table>
        		    </div>
                </fieldset>
				<input type="hidden" id="action_id" value="'.$_REQUEST['id'].'"/>
	  		</div>
    </div>';

	return $data;
}



?>