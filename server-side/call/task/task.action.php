<?php
// MySQL Connect Link
require_once('../../../includes/classes/class.Mysqli.php');
global $db;
$db = new  dbClass();
ini_set('display_errors', 'On'); 
error_reporting(E_ERROR);
date_default_timezone_set('Asia/Tbilisi');
// Main Strings
$action                     = $_REQUEST['act'];
$error                      = '';
$data                       = array();
$user_id	                = $_SESSION['USERID'];
$open_number                = $_REQUEST['open_number'];
$queue                      = $_REQUEST['queue'];
$status                     = $_REQUEST['status'];
$task_status                = $_REQUEST['task_status'];
 
// Incomming Call Dialog Strings
$hidden_id                  = $_REQUEST['id'];
$incomming_id               = $_REQUEST['incomming_id'];


$ws_customer_id             = $_REQUEST['ws_customer_id'];
$ws_loan_id                 = $_REQUEST['ws_loan_id'];
$incomming_cat_1            = $_REQUEST['incomming_cat_1'];
$incomming_cat_1_1          = $_REQUEST['incomming_cat_1_1'];
$incomming_cat_1_1_1        = $_REQUEST['incomming_cat_1_1_1'];
$incomming_comment          = htmlspecialchars($_REQUEST['incomming_comment'], ENT_QUOTES);
$info_comment               = htmlspecialchars($_REQUEST['info_comment'], ENT_QUOTES);
$info_requested_info        = $_REQUEST['info_requested_info'];
$info_given_info            = $_REQUEST['info_given_info'];

//ambition
$incomming_call_ambition_id = $_REQUEST['incomming_call_ambition_id'];
$save_ambition              = $_REQUEST['save_ambition'];
if($save_ambition == 'true'){
    $ambition_number               = $_REQUEST["ambition_number"];
    $ambition_registration_date    = $_REQUEST["ambition_registration_date"];
    $ambition_loan_number          = $_REQUEST["ambition_loan_number"];
    $ambition_pid                  = $_REQUEST["ambition_pid"];
    $ambition_mobile               = $_REQUEST["ambition_mobile"];
    $ambition_product              = $_REQUEST["ambition_product"];
    $ambition_answer_need          = $_REQUEST["ambition_answer_need"];
    $ambition_answer_source        = $_REQUEST["ambition_answer_source"];
    $ambition_ambition             = $_REQUEST["ambition_ambition"];
    $ambition_date                 = $_REQUEST["ambition_date"];
    $ambition_author               = $_REQUEST["ambition_author"];
    $ambition_cause                = $_REQUEST["ambition_cause"];
    $ambition_owner                = $_REQUEST["ambition_owner"];
    $ambition_receiver             = $_REQUEST["ambition_receiver"];
    $ambition_client_email         = $_REQUEST["ambition_client_email"];
    $ambition_client_address       = $_REQUEST["ambition_client_address"];
    $ambition_description          = $_REQUEST["ambition_description"];
    $ambition_name                 = $_REQUEST["ambition_name"];
    $ambition_mail_sent_status     = $_REQUEST["ambition_mail_sent_status"];
}
//personal and contact info 
    $info_pid              = $_REQUEST["info_pid"];
    $info_client_number    = $_REQUEST["info_client_number"];
    $info_doc_number       = $_REQUEST["info_doc_number"];
    $info_born_date        = $_REQUEST["info_born_date"];
    $info_age              = $_REQUEST["info_age"];
    $info_name             = $_REQUEST["info_name"];
    $info_mobile           = $_REQUEST["info_mobile"];
    $info_other_mobile     = $_REQUEST["info_other_mobile"];
    $info_juristic_address = $_REQUEST["info_juristic_address"];
    $info_address          = $_REQUEST["info_address"];
    $info_email            = $_REQUEST["info_email"];


//arrow
    $by_arrow       = "false";
    $by_arrow 		= $_REQUEST['by_arrow'];
    $confirm_update = $_REQUEST['confirm_update'];
    $confirm_click  = $_REQUEST['confirm_click'];
$type_id = $_REQUEST['type_id'];

switch ($action) {
	case 'get_add_page':
		$page		= GetPage('','');
		$data		= array('page'	=> $page);

		break;
	case 'get_edit_page':
		$page		= GetPage(Getincomming($hidden_id));
		$data		= array('page'	=> $page);

		break;
	case 'get_list':
        $count = 		$_REQUEST['count'];
		$hidden = 		$_REQUEST['hidden'];
		$operator = $_REQUEST['operator'];
		if($operator == 0 || $operator == ''){
		    $operator_fillter = '';
		}else{
		    $operator_fillter = "AND outgoing_campaign_detail.responsible_person_id = $operator";
		}
		
	  	$rResult = mysql_query("SELECT  outgoing_campaign_detail.`id`,
	  	                                outgoing_campaign_detail.`id`,
	  	                                outgoing_campaign.create_date,
	  	                                phone_base_detail.`phone1`,
                        				phone_base_detail.`phone2`,
                        				CONCAT(phone_base_detail.`firstname`,' ',phone_base_detail.`lastname`),
                        				phone_base_detail.`pid`,
	  	                                scenario.`name`,
	  	                                user_info.`name`
                                FROM `outgoing_campaign`
                                JOIN outgoing_campaign_detail ON outgoing_campaign.id = outgoing_campaign_detail.outgoing_campaign_id
                                JOIN phone_base_detail ON outgoing_campaign_detail.phone_base_detail_id = phone_base_detail.id
	  	                        JOIN scenario ON outgoing_campaign.scenario_id = scenario.id
	  	                        LEFT JOIN users ON outgoing_campaign_detail.responsible_person_id = users.id
                                LEFT JOIN user_info ON users.id = user_info.user_id
                                WHERE outgoing_campaign_detail.actived = 1 AND  outgoing_campaign_detail.`status` = $status $operator_fillter");
		$data = array(
				"aaData"	=> array()
		);

		while ( $aRow = mysql_fetch_array( $rResult ) )
		{
			$row = array();
			for ( $i = 0 ; $i < $count ; $i++ )
			{
				/* General output */
				$row[] = $aRow[$i];
				if($i == ($count - 1)){
				    $row[] = '<div class="callapp_checkbox">
                                  <input type="checkbox" id="callapp_checkbox_'.$aRow[$hidden].'" name="check_'.$aRow[$hidden].'" value="'.$aRow[$hidden].'" class="check" />
                                  <label for="callapp_checkbox_'.$aRow[$hidden].'"></label>
                              </div>';
				}
			}
			$data['aaData'][] = $row;
		}
	
	    break;
    case 'save_incomming':
        $id			               = $_REQUEST['id'];
        $task_id			       = $_REQUEST['task_id'];
        $incomming_call_id         = $_REQUEST['incomming_call_id'];
        $task_status               = $_REQUEST['task_status'];
        $task_status_2             = $_REQUEST['task_status_2'];
        $task_branch               = $_REQUEST['task_branch'];
        $task_date                 = $_REQUEST['task_date'];
        $task_start_date           = $_REQUEST['task_start_date'];
        $task_end_date             = $_REQUEST['task_end_date'];
        $task_description          = $_REQUEST['task_description'];
        $task_position             = $_REQUEST['task_position'];
        $task_recipient            = $_REQUEST['task_recipient'];
        $task_note                 = $_REQUEST['task_note'];
        $site                      = $_REQUEST['site'];
        $finis_colum               = '';
        $finis_date                = '';
        $finish                    = '';

        
        if ($task_status == 74 || $task_status == 77) {
            $finis_colum = 'finish_date,';
            $finis_date = 'NOW(),';
            
            $finish = 'finish_date=NOW(),';
        }
        if($id == ''){
            //insert task
            $db->setQuery("INSERT INTO `task`
            		        (`id`,
                            `user_id`,
                            `incomming_call_id`,
                            `task_recipient_id`,
                            `task_date`,
                            `task_start_date`,
                            `task_end_date`,
                            $finis_colum
                            `task_description`,
                            `task_type_id`,
                            `task_note`,
                            `task_status_id`,
                            `task_status_id_2`,
                            `task_branch_id`,
                            `cat_1`,
                            `cat_1_1`,
                            `cat_1_1_1`,
                            `info_comment`)
            		    VALUES
                            ('$task_id',
                            '$user_id',
                            '$incomming_call_id',
                            '$task_recipient',
                            NOW(),
                            $finis_date
                            '$task_start_date',
                            '$task_end_date',
                            '$task_description',
                            '1',
                            '$task_note',
                            '$task_status',
                            '$task_status_2',
                            '$task_branch',
                            '$incomming_cat_1',
                            '$incomming_cat_1_1',
                            '$incomming_cat_1_1_1',
                            '$info_comment');");
            $db->execQuery();
            
            $site_array = explode(",",$site);
            $db->setQuery("DELETE FROM task_site WHERE task_id = '$task_id'");
            $db->execQuery();
            
            foreach ($site_array AS $site_id){
                $db->setQuery("INSERT INTO task_site (task_id, site_id) values ('$task_id', $site_id)");
                $db->execQuery();
            }
            
            
            
            $db->setQuery("SELECT task_status.`name`
                           FROM   task_status
                           WHERE  actived = 1 AND id = '$task_status'");
            
            $res_status  = $db->getResultArray();
            $status_name = $res_status[result][0][name];
            
            $db->setQuery("SELECT NOW() AS `time`;");
            $res = $db->getResultArray();

        $data = array('task_id' => $task_id, 'type' => 1, 'status_name' => get_task_status($task_status), 'time' => $res[result][0]['time']);

		} else {
            $db->setQuery("SELECT NOW() AS `time`;");
            $res = $db->getResultArray();
            $data = array('task_id' => $task_id, 'type' => 2, 'status_name' => get_task_status($task_status), 'time' => $res[result][0]['time']);
            //update task
            $db->setQuery("SELECT task_recipient_id 
                             FROM task 
                            WHERE id = '$id'");
            
            $old_recipient_req = $db->getResultArray();
            
            $old_recipient = $old_recipient_req[result][0]['task_recipient_id'];
            
            // if($old_recipient != $task_recipient){
                
            //     mysql_query("UPDATE `task_history`
			// 		            SET `actived` = 0
			// 		         WHERE  `task_id` = '$id'");
                
                
            // }
            $db->setQuery("SELECT `task_status_id_2` FROM `task` WHERE `id` = $id ");
            $res = $db->getResultArray();
            if ($res[result][0]['task_status_id_2'] == 34 && $task_status_2 !=34){
                $process_time = "`process_time` = NOW(),";
            }else{
                $process_time = "";
            }
            
            $db->setQuery(" SELECT user_id,
                                   task_start_date,
                    			   task_end_date,
                    			   task_status_id,
                    			   task_status_id_2,
                    			   task_branch_id,
                    			   task_recipient_id,
                    			   task_description,
                    			   info_comment
                            FROM  `task`
                            WHERE  id = $id AND actived = 1");
            
            $task_info   = $db->getResultArray();
            $tsk         = $task_info[result][0];
            $former_wiev = '';
            
            if ($tsk[task_start_date] != $task_start_date || $tsk[task_end_date] != $task_end_date || $tsk[task_status_id] != $task_status || $tsk[task_status_id_2] != $task_status_2 || $tsk[task_branch_id] != $task_branch || $tsk[task_recipient_id] != $task_recipient || $tsk[task_description] != $task_description || $tsk[info_comment] != $info_comment) {
                if ($tsk[user_id] == $user_id) {
                    $former_wiev = "`former_wiev` = 0,";
                }else{
                    $former_wiev = "`former_wiev` = 1,";
                }
                
            }
            
            $db->setQuery("SELECT task_recipient_id, user_info.`name`
                            FROM task 
                            left join user_info ON user_info.user_id = task_recipient_id
                            WHERE task.id = $id");

            $req = $db->getResultArray();
	


            $db->setQuery("UPDATE `task` 
                              SET  task_recipient_id = '$task_recipient', 
                                   task_start_date   = '$task_start_date',
                                   task_end_date     = '$task_end_date',
                                   $finish
                                   task_description  = '$task_description',
                                   task_note         = '$task_note',
                                   task_status_id    = '$task_status',
                                   task_status_id_2  = '$task_status_2',
                                   task_position_id  = '$task_position',
                                   task_branch_id    = '$task_branch',
                                  `cat_1`            = '$incomming_cat_1',
                                  `cat_1_1`          = '$incomming_cat_1_1',
                                  `cat_1_1_1`        = '$incomming_cat_1_1_1',
                                   $process_time
                                   $former_wiev
                                  `info_comment`     = '$info_comment'
                            WHERE  id                = '$id'");
            $db->execQuery();
            
            $site_array = explode(",",$site);
            $db->setQuery("DELETE FROM task_site WHERE task_id = '$id'");
            $db->execQuery();
            
            foreach ($site_array AS $site_id){
                $db->setQuery("INSERT INTO task_site (task_id, site_id) values ('$id', $site_id)");
                $db->execQuery();
            }
        }
        break;
	case 'task_status_cat':
        $page		= getStatusTask_2($_REQUEST['cat_id'],'');
        $data		= array('page'	=> $page);

        break;
    case 'task_branch_changed':
        $page		= get_task_recipient($_REQUEST['cat_id']);
        $data		= array('page'	=> $page);
    case 'disable':
        $id      = $_REQUEST['id'];
        $user_id = $_SESSION['USERID'];
        mysql_query("UPDATE task set actived = 0 where `id` = '$id' AND task_recipient_id != '$user_id'");
        
        $check=mysql_num_rows(mysql_query("SELECT *
                                           FROM   task
                                           WHERE  id = $id AND task_recipient_id = '$user_id'"));
        if ($check == 0) {
            mysql_query("UPDATE `task_history`
                            SET `actived` = 0
                         WHERE  `task_id` = '$id'");
        }
        
        break;
    case 'view':
	    $user 						= $_SESSION['USERID'];
	    $id   						= $_REQUEST['id'];
	    $check						= mysql_num_rows(mysql_query("SELECT * FROM task_history WHERE task_id = $id AND `user_id` = $user"));
	    if($check == 0){
	        mysql_query("INSERT INTO `task_history` 
                            (`user_id`, `datetime`, `task_id`, `status`, `actived`) 
                        VALUES
                            ('$user', NOW(), '$id', 1, 1)");
	    } else {
	        global $error;
	        $error = 'მოცემულ სიახლეს უკვე გაეცანით';
	    }
        break;
    case 'cat_2':
        $page		= get_cat_1_1($_REQUEST['cat_id'],'');
        $data		= array('page'	=> $page);
    
        break;
    case 'cat_3':
        $page		= get_cat_1_1_1($_REQUEST['cat_id'],'');
        $data		= array('page'	=> $page);

    break;
    //chat comment cases
    case 'save_comment'  :
        $comment            =   $_REQUEST['comment'];
        $incomming_call_id  =   $_REQUEST['incomming_call_id'];
        if($incomming_call_id==''){
            $incomming_call_id= 0;
        }
        $task_id            =   $_REQUEST['task_id'];
        $db->setQuery(" INSERT INTO `chat_comment`
                                   (`user_id`, `incomming_call_id`, `task_id`, `parent_id`, `comment`, `datetime`, `active`)
                             VALUES
                                   ('$user_id', '$incomming_call_id','$task_id', '0', '$comment', NOW(),'1');");
        $db->execQuery();
        
        $page		= "";
        $data		= array('page'	=> $page);
        break;
    case 'update_comment' :
        $comment    = $_REQUEST['comment'];
        $id         = $_REQUEST['id'];
        
        $db->setQuery("UPDATE `chat_comment`
                          SET `comment`    = '$comment'
                       WHERE  `id`        = $id");
        $db->execQuery();
        break;
    case 'delete_comment'  :
           $comment_id=$_REQUEST['comment_id'];
            
           $db->setQuery("UPDATE `chat_comment`
                             SET `active` = 0
                          WHERE  `id` = $comment_id");
           $db->execQuery();
        break;
    case 'get_add_question':
        $incomming_id   = $_REQUEST['incomming_call_id'];
        $task_id        = $_REQUEST['task_id'];
        $page		= Getquestion($incomming_id,$task_id);
        $data		= array('page'	=> $page);

        break;
    //chat comment cases end
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Request Functions
* ******************************
*/

function get_task_recipient($id){
    global $db;
	$where = '';
	if ($id != 0){
		$where ="AND user_info.branch_id = '$id'";
    }
	
	$data .= '';
	
	$db->setQuery(" SELECT users.`id`,
						   user_info.`name`
					FROM   user_info
					JOIN   users ON users.id = user_info.user_id
					WHERE  users.actived = 1
					$where");
	
	$req = $db->getResultArray();
	
	foreach($req[result] AS $res){
		$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
	}

	return $data;
}

function get_cat_1($id){
    global $db;
    
    $db->setQuery("SELECT `id`,
                          `name`
                   FROM   `info_category`
                   WHERE   actived = 1 AND `parent_id` = 0");
    
    $req = $db->getResultArray();
    $data .= '<option value="0" selected="selected">----</option>';
    foreach ($req[result] AS $res){
        if($res['id'] == $id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    
    return $data;
    
}

function get_cat_1_1($id,$child_id){
    global $db;
    
    $db->setQuery("SELECT `id`,
                          `name`
                   FROM   `info_category`
                   WHERE   actived = 1 AND `parent_id` = '$id' AND `parent_id` != 0");
    
    $req = $db->getResultArray();
    
    $data .= '<option value="0" selected="selected">----</option>';
    $i = 0;
    foreach($req[result] AS $res){
        if($res['id'] == $child_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
        $i = 1;
    }
    if($i == 0 && $id > 0){
        $data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
    }
    
    return $data;
}

function get_cat_1_1_1($id,$child_id){
    global $db;
    $db->setQuery("SELECT `id`,
                          `name`
                   FROM   `info_category`
                   WHERE   actived = 1 AND `parent_id` = '$id' AND `parent_id` != 0");
    
    $req = $db->getResultArray();
    
    $data .= '<option value="0" selected="selected">----</option>';
    $i = 0;
    
    foreach($req[result] AS $res){
        if($res['id'] == $child_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
        $i = 1;
    }
    
    if($i == 0 && $id > 0){
        $data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
    }
    
    return $data;
}

function web_site($id, $check_site){
    global $db;
    $data = '';
    
    $db->setQuery("SELECT `id`, `name`
				   FROM   `my_web_site`
				   WHERE   actived = 1");
    
    $res = $db->getResultArray();
    $i = 0;
    foreach ($res[result] AS $value){
        if ($check_site == 2){
            $db->setQuery("SELECT id
                           FROM  `outgoing_call_site`
                           WHERE  outgoing_call_site.outgoing_call_id = '$id' AND site_id = '$value[id]'");
        }elseif ($check_site ==3){
            $db->setQuery("SELECT id
                           FROM  `incoming_call_site`
                           WHERE  incoming_call_site.incomming_call_id = '$id' AND site_id = '$value[id]'");
        }else{
            $db->setQuery("SELECT id
                           FROM  `task_site`
                           WHERE  task_site.task_id = '$id' AND site_id = '$value[id]'");
        }
        
        $check = $db->getNumRow();
        
        if($check>0){
            $data .= '<option value="' . $value['id'] . '" selected="selected">' . $value['name'] . '</option>';
        } else {
            $data .= '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
        }
        
    }
    
    return $data;
}

function Getclientstatus($id){
    
    if($id == 1){
        $data .= '<option value="0">----</option>
                  <option value="1" selected="selected">აქტიური</option>
                  <option value="2">პასიური</option>';
    }elseif($id ==2){
        $data .= '<option value="0">----</option>
                  <option value="1">აქტიური</option>
                  <option value="2" selected="selected">პასიური</option>';
    }else{
        $data .= '<option value="0">----</option>
                  <option value="1">აქტიური</option>
                  <option value="2">პასიური</option>';
    }
    
    return $data;
}

function get_s_u_sex($id){
    
    if($id == 1){
        $data .= '<option value="0">----</option>
                  <option value="1" selected="selected">მდედრობითი</option>
                  <option value="2">მამრობითი</option>';
    }elseif($id ==2){
        $data .= '<option value="0">----</option>
                  <option value="1">მდედრობითი</option>
                  <option value="2" selected="selected">მამრობითი</option>';
    }else{
        $data .= '<option value="0">----</option>
                  <option value="1">მდედრობითი</option>
                  <option value="2">მამრობითი</option>';
    }
    
    return $data;
}

function getStatusTask($id){
    global $db;
    $db->setQuery("SELECT  `id`,
                           `name`
                   FROM    `task_status`
                   WHERE   `actived` = 1 AND id != 0 and parent_id = 0");
    
    $data = $db->getSelect($id);
    return $data;
}

function getStatusTask_2($id, $child_id){
    global $db;
    $db->setQuery("SELECT  `id`,
                           `name`
                   FROM    `task_status`
                   WHERE   `actived` = 1 AND id != 0 and parent_id != 0 and `parent_id` = '$id'");
    
    $data = $db->getSelect($child_id);
    return $data;
}

function get_user_select($id){
    global $db;
    $db->setQuery("SELECT  users.`id`,
						   user_info.`name`
				   FROM    user_info
				   JOIN    users on users.id = user_info.user_id");

    $data = $data = $db->getSelect($id);
	return $data;
}

function getUsers($id){
    global $db;
    $db->setQuery("SELECT `users`.`id`,
                          `user_info`.`name`
                   FROM   `users`
                   JOIN   `user_info` ON `users`.`id` = `user_info`.`user_id`
                   WHERE  `users`.`actived` = 1");

    $data = $db->getSelect($id);
    return $data;
}


//chat question functions
function get_user($id){
    $data   ='';
    global $db;
    $db->setQuery(" SELECT`username`
                    FROM `users`
                    WHERE id = $id AND actived = 1");
    
    $res    = $db->getResultArray();
    $data   = $res[result][0]['username'];
    return $data;
}

function get_comment($incomming_id,$task_id, $outgoing_id){
    global $db;
    $where ='';
    if($incomming_id !='' && $incomming_id != 0){
        $where = "AND incomming_call_id = '$incomming_id'";
    }elseif ($outgoing_id != '' && $outgoing_id == 0){
        $where = "AND outgoing_id = '$outgoing_id'";
    }else {
        $where = "AND task_id = '$task_id'";
    }
    $data="";
    $db->setQuery(" SELECT   `id`,
                             `user_id`,
                             `incomming_call_id`,
                             `comment`,
                             `datetime`
                    FROM     `chat_comment`
                    WHERE     parent_id = 0  AND active = 1  $where
                    ORDER BY `datetime` DESC");
    $req = $db->getResultArray();
    foreach($req[result] AS $res){
            
        $data .='<div style="margin-top: 30px;">
                    <input type=hidden id="hidden_comment_id_'.$res['id'].'" value="'.$res['id'].'"/>
                    <span style="color: #369; font-weight: bold;">'. get_user($res['user_id']) .'</span> '. $res['datetime'] .' <br><br>
                    <span style="border: 1px #D7DBDD solid; padding: 7px; border-radius:50px; background-color:#D7DBDD; "> '.$res['comment'].'</span>
                    <img id="edit_comment" my_id="'. $res['id'] .'" style="cursor:pointer; margin-left: 10px;margin-right: 10px;" class="cat_img cat_edit" alt="img" src="media/images/icons/edit.svg">
                    <img id="delete_comment" my_id="'. $res['id'] .'" style="cursor:pointer" class="cat_img cat_delete" alt="img" src="media/images/icons/delete.svg">
                    <br/><br/><span id="get_answer" class="'. $res['id'] .'"  style="color: #369; font-size:15px;     font-size: 11px;
                    margin-left: 10px;  ">პასუხის გაცემა</span>
                </div>
        <div style="margin-left: 50px; margin-top: 10px;">
            '.get_answers($res['id']).'
        </div>';
    }


    return $data;
}
function Getquestion($incomming_id,$task_id){
    $data = '
        <p>ახალი კომენატრი</p>
        <textarea id="add_ask" style="resize:none; width:480px; height:50px; margin-top:5px; display:inline-block;" rows="3"></textarea > <button style="top: -20px; height: 50px;" id="add_comment">დამატება</button>
            '.get_comment($incomming_id,$task_id).'
    ';
    return $data;
}
function get_answers($id){
    global $db;
    $data= '';
    
    $db->setQuery("SELECT `id`,
                          `user_id`,
                          `comment`,
                          `datetime`
                   FROM   `chat_comment`
                   WHERE   parent_id = '$id' AND active = 1");
    $req = $db->getResultArray();
    
    foreach($req[result] AS $res){
       $data .='<div>
                    <span style="color: #369; font-weight: bold;">'. get_user($res['user_id']) .'</span> '. $res['datetime'] .' <br><br>
                    <span style="border: 1px #D7DBDD solid; padding: 7px; border-radius:50px; background-color:#D7DBDD; ">'.$res['comment'].'</span>
                    <img id="edit_comment" my_id="'. $res['id'] .'" style="cursor:pointer; margin-left: 10px;margin-right: 10px;" class="cat_img cat_edit" alt="img" src="media/images/icons/edit.svg">
                    <img id="delete_comment" my_id="'. $res['id'] .'" style="cursor:pointer" class="cat_img cat_delete" alt="img" src="media/images/icons/delete.svg"><br/><br/>
                </div>';
    }
    return $data;
}
function GetPosition($id){
    global $db;
	$data = '';
    $db->setQuery(" SELECT `id`,
						   `person_position`
					FROM   `position`
					WHERE  `actived` = '1'");

	$data = $db->getSelect($id);

	return $data;
}
function get_select($id,$table,$row='name',$where='actived = 1'){
    global $db;
    $data = '<option value="0" >-------------</option>';
    
    $db->setQuery("SELECT `id`,
                          `$row`
                   FROM   `$table`
                   WHERE   $where");
    
    $data = $db->getSelect($id);
    
    return $data;
}
function GetUserGroup($user_id){
    global $db;
    $db->setQuery("SELECT `group_id`
                   FROM   `users`
                   WHERE  `id` = $user_id");
    
    $req = $db->getResultArray();
    
    return $req[result][0][group_id];
}

function Getincstatus($object_id){
    global $db;
    $data = '';
    $db->setQuery("SELECT `id`,
                          `name`
				   FROM   `inc_status`
				   WHERE  `actived` = 1");
    
    $data = $db->getSelect($object_id);
    
    return $data;
}

function Getsource($object_id){
    global $db;
    $data = '';
    $db->setQuery("SELECT 	`id`,
                            `name`
				    FROM 	`source`
				    WHERE 	 actived=1");
    
    
    $data = $db->getSelect($object_id);
    
    return $data;
}

function Getincomming($hidden_id){
    global $db;
    $db->setQuery(" SELECT IFNULL(task.incomming_call_id,0) AS `incomming_call_id`,
                           IFNULL(task.outgoing_id,0) AS `outgoing_id`
                    from   task
                    where `task`.`actived` = 1  
        AND   `task`.`id` =  '$hidden_id'");
    
    
    $res = $db->getResultArray();
    $phone_table ='task';
    $colum_list  = "";
    if($res[result][0]['incomming_call_id'] != 0 && $res[result][0]['incomming_call_id'] != ''){
        $phone_table = 'incomming_call';
        
        $colum_list  = "$phone_table.source_id,
                	    $phone_table.client_user_id AS personal_pin,
                	    $phone_table.client_pid AS personal_id,
                	    $phone_table.web_site_id,
                	    $phone_table.client_name,
                	    $phone_table.client_satus,
                	    $phone_table.client_sex,
                	    $phone_table.client_mail,";
        
    }elseif ($res[result][0]['outgoing_id'] != 0 && $res[result][0]['outgoing_id'] != ''){
        $phone_table = 'outgoing_call';
        
        $colum_list  = "$phone_table.source_id,
                        $phone_table.client_user_id AS personal_pin,
                        $phone_table.client_pid AS personal_id,
                        $phone_table.web_site_id,
                        $phone_table.client_name,
                        $phone_table.client_satus,
                        $phone_table.client_sex,
                        $phone_table.client_mail,";
    }

	$db->setQuery("SELECT 	  task.id,
                              former_user.`name` as task_former_user,
                              CASE
                                WHEN NOT ISNULL(task.outgoing_id) THEN task.outgoing_id
                                WHEN NOT ISNULL(task.incomming_call_id) && task.incomming_call_id!=0 THEN task.incomming_call_id
                                WHEN (ISNULL(task.incomming_call_id) || task.incomming_call_id=0) && ISNULL(task.outgoing_id) THEN task.id
                              END AS from_site,
                              CASE
                                WHEN NOT ISNULL(task.outgoing_id) THEN 2
                                WHEN NOT ISNULL(task.incomming_call_id) && task.incomming_call_id!=0 THEN 3
                                WHEN (ISNULL(task.incomming_call_id) || task.incomming_call_id=0) && ISNULL(task.outgoing_id) THEN 1
                              END AS from_site_check,
                              task.incomming_call_id,
                              task.task_recipient_id,
                              task.task_date,
                              task.task_start_date,
                              task.task_end_date,
                              task.task_branch_id,
                              task.task_description,
                              task.task_note,
                              task.task_position_id,
                              task.task_status_id,
                              task.task_status_id_2,
                              incomming_call.id as incomming_call_id,
                              task.outgoing_id,
                              task.`user_id` AS task_user_id,
                              $phone_table.phone,
                    	      incomming_call.inc_status_id AS `inc_status`,
                    	      $phone_table.cat_1,
                    	      $phone_table.cat_1_1,
                    	      $phone_table.cat_1_1_1,
                    	      incomming_call.call_content AS call_content,
	                          $colum_list
                              incomming_call.chat_id,
                    	      incomming_call.lid,
                              incomming_call.asterisk_incomming_id
                    FROM 	 `task`
                    LEFT JOIN incomming_call on incomming_call.id = task.incomming_call_id
                    LEFT JOIN outgoing_call on task.outgoing_id = outgoing_call.id
                    LEFT JOIN user_info as former_user on former_user.`user_id` = task.`user_id`
                    WHERE 	 `task`.`actived` = 1 AND task.task_type_id = 1 AND `task`.`id` =  $hidden_id");
	
	$res = $db->getResultArray();
	return $res[result][0];
}

function GetPage($res){
    global $db;
    $user_id    = $_SESSION['USERID'];
    
    if ($res[task_user_id] == $user_id && $res[id] != '') {
        $db->setQuery("UPDATE task SET former_wiev = 0 WHERE id = '$res[id]'");
        $db->execQuery();
    }
    $user_gr    = GetUserGroup($user_id);
    $dis        = '';
    if($res == ''){
        $db->setQuery("INSERT INTO task
                               SET user_id = ''");
        $db->execQuery();
        $increment_id = $db->getLastId();
        $increment = $increment_id;
        $db->setQuery("DELETE FROM task WHERE id = $increment_id");
        $db->execQuery();
        
    } else {
        $increment = $res['id'];
    }
    
    $user_id	                = $_SESSION['USERID'];
    if($res == ''){
        $data .= "<script>
                    setTimeout(() => {
                        $('#view').hide();
                    }, 50);
                </script>";
        $wiev_status=0;
    } else if($res['task_recipient_id'] != $user_id && $user_id!=2){
        $data .= "<script>
                    setTimeout(() => {
                        $('#view').hide();
                    }, 50);
                </script>";
        $wiev_status=0;
    }else if($res['task_recipient_id'] == $user_id){
        $wiev_status=1;
        $dis='disabled="disabled"';
    }
    if ($res[from_site_check] == 2 || $res[from_site_check] == 3) {
        $disable_site = 'disabled="disabled"';
    }else{
        $disable_site = '';
    }
    if ($res[lid] == 1) {$lid_check='checked';}
	$data  .= '
         <div id="dialog-form">
            <fieldset style="width: 320px;  float: left; height: 430px;" id="left_side_fieldset">
               <legend>ძირითადი ინფორმაცია</legend>
               <table class="dialog-form-table">
                   <tr style="height:0px;">
                       <td>დამფორმირებელი :</td>
                       <td>'.$res['task_former_user'].'</td>
                       <td></td>
                   </tr>
                   <tr style="height:5px;"></tr>
                   <tr style="height:0px;">
                       <td>ტელეფონის ნომერი :</td>
                       <td>'.$res['phone'].'</td>
                       <td></td>
                   </tr>
                   <tr style="height:10px;"></tr>
        	       <tr style="height:0px;">
                       <td><label for="task_id">მომართვის №</label></td>
                       <td><label for="task_date">შექმნის თარიღი</label></td>
        		   </tr>
                   <tr style="height:0px;">
                       <td><input id="task_id" type="text" value="'.$increment.'" readonly></td>
                       <td><input id="task_date" type="text" value="'.(($res['task_date']=='')?date("Y-m-d H:i:s"):$res['task_date']).'" readonly></td>
        		   </tr>
                   <tr style="height:5px;"></tr>
        		   <tr style="height:0px;">
                        <td><label>პერიოდი</label></td>
        		   </tr>
                   <tr style="height:0px;">
                        <td><input id="task_start_date" style="width:110px;display: inline;" type="text" value="'.$res['task_start_date'].'" '.$dis.'><span style="vertical-align: unset;"> -დან</span></td>
                        <td><input id="task_end_date"   style="width:110px;display: inline;" type="text" value="'.$res['task_end_date'].'" '.$dis.'><span style="vertical-align: unset;"> -მდე</span></td>
        		   </tr>
                   <tr style="height:5px;"></tr>
                   <tr style="height:0px;">
                        <td><label for="task_status">სტატუსი</label></td>
                       <td><label for="task_status_2">სტატუსი 2</label></td>
        		   </tr>
                   <tr style="height:0px;">
                        <td><select id="task_status" style="width: 100%;" data-select="chosen">'.getStatusTask($res['task_status_id']).'</select></td>
                        <td><select id="task_status_2" style="width: 100%;" data-select="chosen">'.getStatusTask_2($res['task_status_id'],$res['task_status_id_2']).'</select></td>
        		   </tr>
                   <tr style="height:5px;"></tr>
                   <tr style="height:0px;">
                        <td><label for="task_branch">განყოფილება</label></td>
                        <td><label for="task_recipient">დავალების მიმღები</label></td>
        		   </tr>
                   <tr style="height:0px;">
                        <td><select id="task_branch" style="width: 100%;" data-select="chosen">'.get_select($res['task_branch_id'],'department').'</select></td>
                        <td><select id="task_recipient" style="width: 100%;" data-select="chosen">'.get_user_select($res['task_recipient_id']).'</select></td>
                   </tr>
                   <tr style="height:5px;"></tr>
        	       <tr style="height:0px;">
                       <td colspan="2"><label for="task_description">კომენტარი</label></td>
        	       </tr>
                   <tr style="height:0px;">
                       <td colspan="2"><textarea '.($user_gr == 1 || $res[task_user_id] = $user_id ? '' : 'readonly').' style="height:69px;" id="task_description" >'.$res['task_description'].'</textarea></td>
                   </tr>
                   <tr style="height:5px;"></tr>
        	       <tr style="height:0px;">
                       <td colspan="2"><label for="task_note">შედეგი</label></td>
        	       </tr>
                   <tr style="height:0px;">
                       <td colspan="2"><textarea style="height:69px;" id="task_note" >'.$res['task_note'].'</textarea></td>
                   </tr>
               </table>
            </fieldset>
            <div id="side_menu" style="font-size: 10px; float: left; height: 449px; width: 65px; margin-left: 4px; background: #272727; color: #FFF;margin-top: 6px;">
               <spam class="info"           style="display: block;padding: 10px 5px;  cursor: pointer;" onclick="show_right_side(\'info\')">            <img style="padding-left: 16px;padding-bottom: 5px;" src="media/images/icons/info.png"      alt="24 ICON" height="24" width="24"><div style="text-align: center;">ინფო</div></spam>
               <spam class="record"         style="display: block;padding: 10px 5px;  cursor: pointer;" onclick="show_right_side(\'record\')">          <img style="padding-left: 16px;padding-bottom: 5px;" src="media/images/icons/record.png"    alt="24 ICON" height="24" width="24"><div style="text-align: center;">ჩანაწერი</div></spam>
               <spam class="chat_question"  style="display: block;padding: 10px 5px;  cursor: pointer;" onclick="show_right_side(\'chat_question\')">   <img style="padding-left: 16px;padding-bottom: 5px;" src="media/images/icons/call_chat.png" alt="24 ICON" height="24" width="24"><div style="text-align: center;">კომენტარი</div></spam>
               <spam class="file"           style="display: block;padding: 10px 5px;  cursor: pointer;" onclick="show_right_side(\'file\')">            <img style="padding-left: 16px;padding-bottom: 5px;" src="media/images/icons/file.png"      alt="24 ICON" height="24" width="24"><div style="text-align: center;">ფაილი</div></spam>
            </div>
            <div id="right_side" style="margin-top: 6px;">
                <fieldset style="display:none; height: 424px;" id="info">
                    <table width="100%" class="dialog-form-table">
                        <tr style="height:0px">
							<td style="width: 196px;"><label for="d_number">კატეგორია</label></td>
							<td style="width: 197px;"><label for="d_number">ქვე კატეგორია1</label></td>
							<td style="width: 197px;"><label for="d_number">ქვე კატეგორია2</label></td>
                        </tr>
						<tr style="height:0px">
                            <td><select  id="incomming_cat_1"     style="width: 190px; height: 18px; ">'.get_cat_1($res['cat_1']).'</select></td>
                            <td><select  id="incomming_cat_1_1"   style="width: 191px; height: 18px; ">'.get_cat_1_1($res['cat_1'],$res['cat_1_1']).'</select></td>
                            <td><select  id="incomming_cat_1_1_1" style="width: 191px; height: 18px; ">'.get_cat_1_1_1($res['cat_1_1'],$res['cat_1_1_1']).'</select></td>
                        </tr>
                        <tr style="height:5px"></tr>
                        <tr style="height:0px">
							<td colspan="3"><label for="d_number">საიტი</label></td>
						</tr>
						<tr style="height:0px">
                            <td colspan="3"><select style="height: 18px; width: 590px;" id="my_site" class="idls object" multiple '.$disable_site.'>'.web_site($res['from_site'], $res[from_site_check]).'</select></td>
                        </tr>
                    </table>
                    <hr style="border: 2px solid #FFF; margin-left: -6px; width: 103%;">
                    <table width="100%" class="dialog-form-table">
                        <tr style="height:0px">
							<td style="width: 205px;"><label for="d_number">ტელეფონი</label></td>
							<td style="width: 205px;"><label for="d_number">User ID</label></td>
                            <td style="width: 205px;"><label for="d_number">ელ. ფოსტა</label></td>
						</tr>
						<tr style="height:0px">
                            <td>
								<input style="height: 18px; width: 183px;" type="text" id="phone" class="idle" value="' . $res[phone]. '" disabled="disable"/>
                                <span id="search_phone" style="margin-top: -20px; margin-left: 170px; display:none;" class="search_button"><img style="padding: 0; border: solid 1px #009688;" src="media/images/icons/search_button.png" height="13" width="13"></span>
							</td>
							<td>
								<input style="height: 18px; width: 183px;" type="text" id="s_u_user_id" class="idle" value="'.$res['personal_pin'].'" disabled="disable"/>
                                <span id="search_user_id" style="margin-top: -20px; margin-left: 170px; display:none;" class="search_button"><img style="padding: 0; border: solid 1px #009688;" src="media/images/icons/search_button.png" height="13" width="13"></span>
							</td>
                            <td>
								<input style="height: 18px; width: 183px;" type="text" id="s_u_mail" class="idle" value="'.$res[client_mail].'" disabled="disable"/>
                                <span id="search_mail" style="margin-top: -20px; margin-left: 170px; display:none;" class="search_button"><img style="padding: 0; border: solid 1px #009688;" src="media/images/icons/search_button.png" height="13" width="13"></span>
							</td>
                        </tr>
                        <tr style="height:5px"></tr>
                        <tr style="height:0px">
                            <td style="width: 205px;"><label for="d_number">საიდ. კოდი / პ.ნ</label></td>
							<td style="width: 205px;"><label for="d_number">სახელი,გვარი / დასახელება</label></td>
							<td style="width: 205px;"><label for="d_number">სქესი</label></td>
						</tr>
						<tr style="height:0px">
                            <td>
								<input style="height: 18px; width: 183px;" type="text" id="s_u_pid" class="idle" value="'.$res['personal_id'].'" disabled="disable"/>
                                <span id="search_s_u_pid" style="margin-top: -20px; margin-left: 170px; display:none;" class="search_button"><img style="padding: 0; border: solid 1px #009688;" src="media/images/icons/search_button.png" height="13" width="13"></span>
							</td>
                            <td>
                                <input style="height: 18px; width: 183px;" type="text" id="s_u_name" class="idle" value="'.$res[client_name].'" disabled="disable"/>
                                <span id="search_name" style="margin-top: -20px; margin-left: 170px; display:none;" class="search_button"><img style="padding: 0; border: solid 1px #009688;" src="media/images/icons/search_button.png" height="13" width="13"></span>
    						</td>
                            <td>
                                <select  id="client_sex" style="width: 190px; height: 18px;" disabled="disable">'.get_s_u_sex($res['client_satus']).'</select>
							</td>
                        </tr>
                        <tr style="height:5px"></tr>
                        <tr style="height:0px">
							<td><label for="d_number">სტატუსი</label></td>
							<td><label for="d_number">ინფორმაციის წყარო</label></td>
                            <td><label for="d_number">რეაგირება</label></td>
						</tr>
						<tr style="height:0px">
                            <td>
                                <select  id="s_u_status" style="width: 190px; height: 18px;" disabled="disable">'.Getclientstatus($res['client_satus']).'</select>
							</td>
                            <td>
                                <select  id="source_id" style="width: 190px; height: 18px;" disabled="disable">'.Getsource($res['source_id']).'</select>
							</td>
							<td>
                                <select  id="inc_status" style="width: 190px; height: 18px;" disabled="disable">'.Getincstatus($res['inc_status']).'</select>
							</td>
                        </tr>
                        <tr style="height:5px"></tr>
                        <tr style="height:0px">
							<td colspan="3"></td>
						</tr>
						<tr style="height:0px">
                            <td colspan="3">
                                <table>
                                    <tr>
                                        <td><input style="width: 18px; margin-left: 0px;" type="checkbox" name="language" id="lid" value="1" '.$lid_check.' disabled="disable"/></td>
                                        <td style="width: 70px;">ლიდი</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table width="100%" class="dialog-form-table">
                        <hr style="border: 2px solid #FFF; margin-left: -6px; width: 103%;">
                        <tr style="height:0px">
							<td colspan="3" style="width: 180px;">საუბრის შინაარსი</td>
                        </tr>
					    <tr style="height:0px">
						    <td colspan="3">
    							<textarea  style="resize: vertical;width: 98%; height: 50px;" id="call_content" class="idle" name="call_content" disabled="disable">' . $res['call_content'] . '</textarea>
    						</td>
					    </tr>
					</table>
					<table style="'.($res[noted]=='1'?"":"display:none;").'">
                        <tr style="height:10px;"></tr>
						<tr style="height:0px">
							<td style="width: 180px;"><label for="note_comm">კომენტარი</label></td>
						</tr>
					    <tr style="height:0px">
						    <td>
							     <textarea  style="resize: vertical;width: 571px; margin-right: -22px;" id="note_comm" class="idle" name="note_comm">' . $res['note_comm'] . '</textarea>
							</td>
					    </tr>

					</table>
                    <input type="hidden" value="'.$res['incomming_call_id'].'"              id="incomming_call_id">
                    <input type="hidden" value="'.$res['outgoing_id'].'"              id="outgoing_id">
                    <input type="hidden" value="'.$res['incomming_call_id'].'"              id="incomming_id">
                    <input type="hidden" value="'.$res['user_id'].'"                        id="hidden_user_id">
                    <input type="hidden" value="'.$res['id'].'"                             id="hidden_id">
                </fieldset>
                <fieldset style="display:none; height: 424px;  overflow-y: auto;" id="chat_question">
                    <p>ახალი კომენატრი</p>
                    <textarea id="add_ask" style="resize:none; width:480px; height:50px; margin-top:5px; display:inline-block;" rows="3"></textarea style="top: -20px; height: 50px;"> <button style="height:50px; top: -20px;" id="add_comment">დამატება</button>
                        '.get_comment($res['incomming_call_id'],$res['id'], $res['outgoing_id']).'
                    <input type=hidden id="comment_incomming_id" value="'.(($res['id']=='')?$increment:$res['id']).'"/>
                </fieldset>
                <fieldset style="display:none;" id="record">
                    <legend>ჩანაწერები</legend>
                        '.show_record($res).'
                </fieldset>
                <fieldset style="display:none; height: 424px;" id="file">
                    <legend>ფაილი</legend>
                        '.show_file($res).'
                </fieldset>
            </div>
        </div>';
            
        return $data;
}

function GetSmsSendPage() {
    $data = '
        <div id="dialog-form">
            <fieldset style="width: 299px;">
					<legend>SMS</legend>
			    	<table class="dialog-form-table">
						<tr>
							<td><label for="d_number">ადრესატი</label></td>
						</tr>
			    		<tr>
							<td>
								<span id="errmsg" style="color: red; display: none;">მხოლოდ რიცხვი</span>
								<input type="text" id="sms_phone"  value="">
							</td>
							<td>
								<button id="copy_phone">Copy</button>
							</td>
							<td>
								<button id="sms_shablon">შაბლონი</button>
							</td>
						</tr>
						<tr>
							<td><label for="content">ტექსტი</label></td>
						</tr>
					
						<tr>
							
							<td colspan="6">	
								<textarea maxlength="150" style="width: 298px; resize: vertical;" id="sms_text" name="call_content" cols="300" rows="4"></textarea>
							</td>
						</tr>
						<tr>
							<td>
								<input style="width: 50px;" type="text" id="simbol_caunt" value="0/150">
							</td>
							<td>
								
							</td>
							
							<td>
								<button id="send_sms">გაგზავნა</button>
							</td>
						</tr>	
					</table>
		        </fieldset>
        </div>';
    return $data;
}

function GetMailSendPage(){
    $data = '
            <div id="dialog-form">
        	    <fieldset style="height: auto;">
        	    	<table class="dialog-form-table">
        				
        				<tr>
        					<td style="width: 90px; "><label for="d_number">ადრესატი:</label></td>
        					<td>
        						<input type="text" style="width: 490px !important;"id="mail_address" value="" />
        					</td>
        				</tr>
        				<tr>
        					<td style="width: 90px;"><label for="d_number">CC:</label></td>
        					<td>
        						<input type="text" style="width: 490px !important;" id="mail_address1" value="" />
        					</td>
        				</tr>
        				<tr>
        					<td style="width: 90px;"><label for="d_number">Bcc:</label></td>
        					<td>
        						<input type="text" style="width: 490px !important;" id="mail_address2" value="" />
        					</td>
        				</tr>
        				<tr>
        					<td style="width: 90px;"><label for="d_number">სათაური:</label></td>
        					<td>
        						<input type="text" style="width: 490px !important;" id="mail_text" value="" />
        					</td>
        				</tr>
        			</table>
        			<table class="dialog-form-table">
        				<tr>
        					<td>	
        						<textarea id="input" style="width:551px; height:200px"></textarea>
        					</td>
        			   </tr>
        			</table>
			    </fieldset>
		    </div>';
    return $data;
}

function show_record($res){
    global $db;
    $db->setQuery(" SELECT   `datetime`,
                              TIME_FORMAT(SEC_TO_TIME(duration),'%i:%s') AS `duration`,
                              CONCAT(DATE_FORMAT(asterisk_incomming.call_datetime, '%Y/%m/%d/'),`file_name`) AS file_name,
                              user_info.`name`,
                              if(asterisk_incomming.disconnect_cause = '4', 'ოპერატორმა', 'აბონენტმა') as completed
                    FROM     `asterisk_incomming`
                    LEFT JOIN user_info ON user_info.user_id = asterisk_incomming.user_id
                    WHERE     asterisk_incomming.id = '$res[asterisk_incomming_id]'");
    
    $record_incomming = $db->getResultArray();
    
    foreach ($record_incomming[result] AS $record_res_incomming) {
        $str_record_incomming .= '<tr>
                                      <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$record_res_incomming['datetime'].'</td>
                            	      <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$record_res_incomming['duration'].'</td>
                                      <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$record_res_incomming['name'].'</td>
                                      <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$record_res_incomming['completed'].'</td>
                            	      <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;cursor: pointer;" onclick="listen(\''.$record_res_incomming['file_name'].'\')"><span>მოსმენა</span></td>
                            	  </tr>';
    }
    
    $phone = $res['phone'];
    if(strlen($res['phone']) > 1 && substr($phone,0,1) == '0'){
        $phone = substr($phone,1);
    }
    
    if(strlen($phone) > 1){
        $ph = "CAST(`phone` AS SIGNED) LIKE '%$phone%'";
    }else{
        $ph = "CAST(`phone` AS SIGNED) LIKE '%sdfgsdfge%'";
    }
    $db->setQuery(" SELECT   `call_datetime`,
                              TIME_FORMAT(SEC_TO_TIME(duration),'%i:%s') AS `duration`,
                              CONCAT(DATE_FORMAT(asterisk_outgoing.call_datetime, '%Y/%m/%d/'),`file_name`) AS file_name,
                              user_info.name
                    FROM     `asterisk_outgoing`
                    LEFT JOIN user_info ON user_info.user_id = asterisk_outgoing.user_id
                    WHERE   $ph");
   
    $record_outgoing = $db->getResultArray();
    foreach($record_outgoing[result] AS $record_res_outgoing) {
        $str_record_outgoing .= '<tr>
                                    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$record_res_outgoing[call_datetime].'</td>
                            	    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$record_res_outgoing[duration].'</td>
                                    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$record_res_outgoing['name'].'</td>
                                    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;cursor: pointer;" onclick="listen(\''.$record_res_outgoing[file_name].'\')"><span>მოსმენა</span></td>
                                  </tr>';
    }
    
    if($str_record_outgoing == ''){
        $str_record_outgoing = '<tr>
                                    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;" colspan=4>ჩანაწერი არ მოიძებნა</td>
                        	      </tr>';
    }
    if($str_record_incomming == ''){
        $str_record_incomming = '<tr>
                                   <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;" colspan=5>ჩანაწერი არ მოიძებნა</td>
                        	      </tr>';
    }
    
    $data = '  <div style="margin-top: 10px;">
                    <audio controls style="margin-left: 165px;">
                      <source src="" type="audio/wav">
                      Your browser does not support the audio element.
                    </audio>
               </div>
               <fieldset style="display:block !important; margin-top: 10px;" class="nothide">
                    <legend>შემომავალი ზარი</legend>
                    <div style="overflow-y:scroll;height:130px;">
                        <table style="margin: auto; width: 96%;">
                            <tr>
                                <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">თარიღი</td>
                                <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">ხანგძლივობა</td>
                                <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">ოპერატორი</td>
                                <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">ვინ გათიშა</td>
                                <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">მოსმენა</td>
                             </tr>
                            '.$str_record_incomming.'
                        </table>
                    </div>
                </fieldset>
	            <fieldset style="display:block !important; margin-top: 10px;" class="nothide">
                    <legend>გამავალი ზარი</legend>
                    <div style="overflow-y:scroll;height:117px;">
                        <table style="margin: auto; width: 96%;">
                            <tr>
                                <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">თარიღი</td>
                                <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">ხანგძლივობა</td>
                                <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">ვინ გათიშა</td>
                                <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">მოსმენა</td>
                            </tr>
                            '.$str_record_outgoing.'
                        </table>
                    </div>
	            </fieldset>';
    return $data;
}
function show_file($res){
    global $db;
    if ($res['incomming_call_id'] != '' && $res['incomming_call_id'] != 0) {
        $where = "AND `incomming_call_id` = '$res[incomming_call_id]'";
    }elseif ($res['outgoing_id'] != '' && $res['outgoing_id'] != 0){
        $where = "AND `outgoing_id` = '$res[outgoing_id]'";
    }else{
        $where = "AND `task_id` = '$res[id]'";
    }
    
    $db->setQuery("SELECT `name`,
                          `rand_name`,
                          `file_date`,
                          `id`
                   FROM   `file`
                   WHERE  `actived` = 1 $where");
    
    $file_incomming = $db->getResultArray();
    foreach($file_incomming[result] AS $file_res_incomming) {
        $str_file_incomming .= '<div style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;width: 180px;float:left;">'.$file_res_incomming[file_date].'</div>
                            	<div style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;width: 189px;float:left;">'.$file_res_incomming[name].'</div>
                            	<div style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;cursor: pointer;width: 160px;float:left;"><a   download href="media/uploads/file/'.$file_res_incomming['rand_name'].'">ჩამოტვირთვა</a></div>
                            	<div style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;cursor: pointer;width: 20px;float:left;" onclick="delete_file(\''.$file_res_incomming[id].'\')">-</div>';
    }
    $data = '<div style="margin-top: 15px;">
                    <div style="width: 100%; border:1px solid #CCC;float: left;">    	            
    	                   <div style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;width: 180px;float:left;">თარიღი</div>
                    	   <div style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;width: 189px;float:left;">დასახელება</div>
                    	   <div style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;width: 160px;float:left;">ჩამოტვირთვა</div>
                           <div style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;width: 20px;float:left;">-</div>
    	                   <div style="text-align: center;vertical-align: middle;float: left;width: 595px;"><button id="upload_file" style="cursor: pointer;background: none;border: none;width: 100%;height: 25px;padding: 0;margin: 0;">აირჩიეთ ფაილი</button><input style="display:none;" type="file" name="file_name" id="file_name"></div>
                           <div id="paste_files">
                           '.$str_file_incomming.'
                           </div>
            	    </div>
	            </div>';
    return $data;
}
function get_task_status($id){
    global $db;
    $db->setQuery("SELECT `name` FROM `task_status` WHERE id = '$id' ");
    $res = $db->getResultArray();

    return $res[result][0]['name'];

}


?>