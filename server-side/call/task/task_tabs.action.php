<?php

    // MySQL Connect Link
    require_once('../../../includes/classes/class.Mysqli.php');
    global $db;
    $db = new dbClass();
    $action        = $_REQUEST['act'];
    $user_id       = $_SESSION['USERID'];
    $data = array();
    ////

    switch($action){

        case 'get_tabs' :
        
            $page		= get_tabs(get_parent_status());
            $data		= array('page'	=> $page);
            break;

        case 'get_list' :
            $count      = $_REQUEST['count'];
            $hidden     = $_REQUEST['hidden'];
            $parent     = $_REQUEST['parent'];
            $child      = $_REQUEST['child'];
            $user_id    = $_SESSION['USERID'];
            
            $and        = '';
            if ($child != 'undefined') {
                $and = "and task.task_status_id_2 = '$child'";
            }
            
            $db->setQuery(" SELECT group_id
                            FROM   users
                            WHERE  id = '$user_id' AND actived = 1");
            
            $group = $db->getResultArray();
            
            if ($group[result][0][group_id] == 1) {
                $filt_resp = '';
            }else{
                $filt_resp = ' AND (task.task_recipient_id = '.$user_id.' OR task.user_id = '.$user_id.')';
            }
            $start_date = $_REQUEST['start_date'];
            $end_date   = $_REQUEST['end_date'];
            
            $db->setQuery("SELECT    `task`.`id`,
                					  IF($user_id = task.user_id && task.former_wiev = 1, CONCAT('<div style=\"background: yellow; text-align: center;\">',`task`.`id`,'</div>'), CONCAT('<div style=\"text-align: center;\">',task.id,'<div>')),
                					 `task`.`task_date`,
                					 `task`.`task_start_date`,
                					  CASE
                						 WHEN  `task`.`task_end_date` < IFNULL(`task`.finish_date,NOW()) THEN  CONCAT('<div style=\"background: #F44336;\">',`task`.`task_end_date`,'<div>')
                						 WHEN `task`.`task_end_date` >= IFNULL(`task`.finish_date,NOW()) THEN `task`.`task_end_date`
                					  END AS `task_end_date`,
                					 `user_info`.`name`,
                					 `recipient_user`.`name`,
                					 `task_status`.`name`,
                					 `task_status_2`.`name`,
                					 `department`.`name`,
                					 `task`.`task_description`,
                					  IF(NOT ISNULL(task_history.id), '<div class=read-action>გაცნობილი</div>', '<div class=unread-action>წაუკითხავი</div>') AS read_status,
                					 `task`.`id`
                            FROM      task
                            LEFT JOIN user_info as recipient_user on recipient_user.user_id = task.task_recipient_id
                            LEFT JOIN user_info on user_info.user_id = task.user_id
                            LEFT JOIN task_status on task_status.id = task.task_status_id
                            LEFT JOIN task_status as task_status_2 on task_status_2.id = task.task_status_id_2
                            LEFT JOIN task_history on task.id = task_history.task_id AND task_history.actived = 1
                            LEFT JOIN department on department.id = task.task_branch_id
                            LEFT JOIN incomming_call on task.incomming_call_id = incomming_call.id
                            LEFT JOIN outgoing_call on task.outgoing_id = outgoing_call.id
                            
                            where task.task_type_id = 1 and task.task_status_id = '$parent' $and
                            AND task.actived = 1 AND DATE(task.task_date) >= '$start_date' AND DATE(task.task_date) <= '$end_date'
                            $filt_resp");
            
            $data = $db->getList($count, $hidden,1);
            break;
        case "get_quantity":
            $sqlres_outgoing    = get_status();
            $result             = array();
            foreach($sqlres_outgoing AS $arr){
                
                if($arr['parent_id'] == 0){
                    $result["".$arr['id'].""]=get_quantity(get_task_status(),$arr['id']);
                } else {
                    $result["".$arr['id'].""]= get_quantity2(get_task_status_1(),$arr['id']);
                }
            }
            $data	= array('quantity'	=> $result);
        break;


        default : echo ("error");


    }

    echo json_encode($data);

    function get_tabs($res){
        
        $datafull="";
        $data2='';
        $data='<div id="tab_0" class="main_tab" name="0"><ul>';
        foreach ($res as $arr) {
            if($arr['parent_id']==0){
                $data   .=' <li id="t_'.$arr['id'].'" name="'.$arr['id'].'" >
                                <a>'.$arr['name'].'</a>
                                <input value="0" id="input_'.$arr['id'].'" class="caunt_imput" style="margin: 0; margin-top: 4px; margin-left: 5px;" disabled="disabled">
                            </li>';
                $data2  .='</ul>
                                </div><div id="tab_'.$arr['id'].'" class="tab_'.$arr['id'].'" name="'.$arr['id'].'">
                            <ul>';
                $child_req = get_child_status($arr['id']);
                foreach($child_req AS $child_res){
                    $data2.=get_child_tabs($child_res,$child_res['parent_id']);
                }
            }
        }
        $data.='</ul></div>';
        $data2.='</ul></div>';
        $datafull .=  $data;
        $datafull .=  $data2;
        return $datafull;
    }


    function get_child_tabs($arr,$id){
        $data='';
        if($arr['parent_id']==$id && $arr['parent_id']!=0 )
            $data.=' <li id="t_'.$arr['id'].'" name="'.$arr['id'].'" id_select="'.$id.'"  class="child">
                            <a>'.$arr['name'].'</a><input value="0" id="input_'.$arr['id'].'" class="caunt_imput" style="margin: 0; margin-top: 4px; margin-left: 5px;" disabled="disabled">
                    </li>';
            
        return $data;
    }


    function get_status(){
        global $db;
        
        $db->setQuery(" SELECT *
                        From `task_status`
                        where actived=1");
        
        $res = $db->getResultArray();
        return $res[result];
    }
    function get_parent_status(){
        global $db;
        $db->setQuery("SELECT *
                       From `task_status`
                       where actived=1 and parent_id = 0");
        $res = $db->getResultArray();
        return $res[result];
    }
    function get_child_status($id){
        global $db;
        $db->setQuery(" SELECT *
                        From `task_status`
                        where actived=1 and parent_id = '$id'");
        $res = $db->getResultArray();
        return $res[result];
    }

    function get_task_status(){
        global $db;
        $start_date = $_REQUEST['start_date'];
        $end_date   = $_REQUEST['end_date'];
        $db->setQuery("SELECT   `task`.`task_status_id` AS `id`,  count(`task`.`task_status_id`) as `quantity`
                       FROM     `task`
                       WHERE    `task`.`actived` = 1 and task_type_id = 1 AND DATE(task_date) >= '$start_date' AND DATE(task_date) <= '$end_date'
                       GROUP BY `task`.`task_status_id`");
        
        $res = $db->getResultArray();
        return $res[result];
    }

    function get_task_status_1(){
        global $db;
        $start_date = $_REQUEST['start_date'];
        $end_date   = $_REQUEST['end_date'];

        $db->setQuery(" SELECT `task`.`task_status_id_2` AS `id`, count(`task`.`task_status_id_2`) as `quantity`
                        FROM `task`
                        WHERE   `task`.`actived`=1 and task_type_id = 1 AND DATE(task_date) >= '$start_date' AND DATE(task_date) <= '$end_date'
                        GROUP BY `task`.`task_status_id_2`");
        
        $res=$db->getResultArray();
        return $res[result];
    }

    function get_quantity($res,$id){
        foreach($res AS $arr){ 
            if($arr['id']==$id){
                $data=$arr['quantity'];
            }
        }
        return $data;
    }
    

    function get_quantity2($res,$id){
        foreach($res AS $arr){ 
            if($arr['id']==$id){
                $data=$arr['quantity'];
            }
        }
        
        return $data;
    }

?>