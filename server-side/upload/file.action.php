<?php
/* ******************************
 *	File Upload aJax actions
 * ******************************
 */
require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db     = new dbClass();
$action = $_REQUEST['act'];
$error	= '';
$data	= array();
$user   = $_SESSION['USERID'];
switch ($action) {
    case 'file_upload':
        $element	   = $_REQUEST['button_id'];
        $file_name	   = $_REQUEST['file_name'];
        $type		   = $_REQUEST['file_type'];
        $path		   = $_REQUEST['path'];
        $rand_name     = $file_name . '.' . $type;
        $original_name = $_REQUEST['file_name_original'];
        $path		   = $path . $file_name . '.' . $type;
        $table_id      = $_REQUEST['table_id'];
        $table_name    = $_REQUEST['table_name'];
        
        if (! empty ( $_FILES [$element] ['error'] )) {
            switch ($_FILES [$element] ['error']) {
                case '1' :
                    $error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
                    break;
                case '2' :
                    $error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
                    break;
                case '3' :
                    $error = 'The uploaded file was only partially uploaded';
                    break;
                case '4' :
                    $error = 'No file was uploaded.';
                    break;
                case '6' :
                    $error = 'Missing a temporary folder';
                    break;
                case '7' :
                    $error = 'Failed to write file to disk';
                    break;
                case '8' :
                    $error = 'File upload stopped by extension';
                    break;
                case '999' :
                default :
                    $error = 'No error code avaiable';
            }
        } elseif (empty ( $_FILES [$element] ['tmp_name'] ) || $_FILES [$element] ['tmp_name'] == 'none') {
            $error = 'No file was uploaded..';
        } else {
            if (file_exists($path)) {
                unlink($path);
            }
            move_uploaded_file ( $_FILES [$element] ['tmp_name'], $path);
            $db->setQuery("INSERT INTO `file` (`user_id`, `".$table_name."_id`, `name`, `rand_name`, `file_date`) VALUES ('$user', '$table_id', '$original_name', '$rand_name', NOW())");
            $db->execQuery();
            $db->setQuery("SELECT  `name`,
                    			   `rand_name`,
                    			   `file_date`,
                                   `id`
            			    FROM   `file`
            			    WHERE  `".$table_name."_id` = $table_id AND `actived` = 1");
            $str_file_table = array();
            $file_tbale = $db->getResultArray();
            foreach($file_tbale[result] AS $file_res_table) {
                $str_file_table[] = array('file_date' => $file_res_table[file_date], 'name' => $file_res_table[name], 'rand_name' => $file_res_table[rand_name],'id' => $file_res_table[id]);
            }
            $data		= array('page'	=> $str_file_table);
            @unlink ( $_FILES [$element] );
        }
        
        break;
	case 'upload_file':
		$element	= 'choose_file';
		$file_name	= $_REQUEST['file_name'];
		$type		= $_REQUEST['type'];
		$path		= $_REQUEST['path'];
		$path		= $path . $file_name . '.' . $type;


		if (! empty ( $_FILES [$element] ['error'] )) {
			switch ($_FILES [$element] ['error']) {
				case '1' :
					$error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
					break;
				case '2' :
					$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
					break;
				case '3' :
					$error = 'The uploaded file was only partially uploaded';
					break;
				case '4' :
					$error = 'No file was uploaded.';
					break;
				case '6' :
					$error = 'Missing a temporary folder';
					break;
				case '7' :
					$error = 'Failed to write file to disk';
					break;
				case '8' :
					$error = 'File upload stopped by extension';
					break;
				case '999' :
				default :
					$error = 'No error code avaiable';
			}
		} elseif (empty ( $_FILES [$element] ['tmp_name'] ) || $_FILES [$element] ['tmp_name'] == 'none') {
			$error = 'No file was uploaded..';
		} else {


            $info = pathinfo($_FILES[$element]['name']);
            $i = 0;
            $path='';
            do {
                $image_name = $info['filename'] . ($i ? "($i)" : ""). "." . $info['extension'] ;
                $i++;
                $path = '../../media/uploads/file/' . $image_name;
            } while(file_exists($path));
            if(move_uploaded_file($_FILES[$element]['tmp_name'], $path))
            {
                $data['file_name']=$image_name;
            }
            @unlink ($_FILES [$element]);


		}

		break;
		
		case 'upload_filee':
			$element	= 'choose_filee';
			$file_name	= $_REQUEST['file_name'];
			$type		= $_REQUEST['type'];
			$path		= $_REQUEST['path'];
			$path		= $path . $file_name . '.' . $type;
		
			if (! empty ( $_FILES [$element] ['error'] )) {
				switch ($_FILES [$element] ['error']) {
					case '1' :
						$error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
						break;
					case '2' :
						$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
						break;
					case '3' :
						$error = 'The uploaded file was only partially uploaded';
						break;
					case '4' :
						$error = 'No file was uploaded.';
						break;
					case '6' :
						$error = 'Missing a temporary folder';
						break;
					case '7' :
						$error = 'Failed to write file to disk';
						break;
					case '8' :
						$error = 'File upload stopped by extension';
						break;
					case '999' :
					default :
						$error = 'No error code avaiable';
				}
			} elseif (empty ( $_FILES [$element] ['tmp_name'] ) || $_FILES [$element] ['tmp_name'] == 'none') {
				$error = 'No file was uploaded..';
			} else {
				if (file_exists($path)) {
					unlink($path);
				}
				move_uploaded_file ( $_FILES [$element] ['tmp_name'], $path);
		
				// for security reason, we force to remove all uploaded file
				@unlink ( $_FILES [$element] );
			}
		
			break;
		case 'upload_file_mail':
		    $element	   = $_REQUEST['button_id'];
		    $file_name	   = $_REQUEST['file_name'];
		    $type		   = $_REQUEST['file_type'];
		    $path		   = $_REQUEST['path'];
		    $rand_name     = $file_name . '.' . $type;
		    $original_name = $_REQUEST['file_name_original'];
		    $path		   = $path . $file_name . '.' . $type;
		    $table_id      = $_REQUEST['table_id'];
		    $table_name    = $_REQUEST['table_name'];
		    
		    if (! empty ( $_FILES [$element] ['error'] )) {
		        switch ($_FILES [$element] ['error']) {
		            case '1' :
		                $error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
		                break;
		            case '2' :
		                $error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
		                break;
		            case '3' :
		                $error = 'The uploaded file was only partially uploaded';
		                break;
		            case '4' :
		                $error = 'No file was uploaded.';
		                break;
		            case '6' :
		                $error = 'Missing a temporary folder';
		                break;
		            case '7' :
		                $error = 'Failed to write file to disk';
		                break;
		            case '8' :
		                $error = 'File upload stopped by extension';
		                break;
		            case '999' :
		            default :
		                $error = 'No error code avaiable';
		        }
		    } elseif (empty ( $_FILES [$element] ['tmp_name'] ) || $_FILES [$element] ['tmp_name'] == 'none') {
		        $error = 'No file was uploaded..';
		    } else {

                $info = pathinfo($_FILES[$element]['name']);
                $i = 0;
                $path='';
                do {
                    $image_name = $info['filename'] . ($i ? "($i)" : ""). "." . $info['extension'] ;
                    $i++;
                    $path = '../../mailmyge/file_attachment/' . $image_name;
                } while(file_exists($path));
                if(move_uploaded_file($_FILES[$element]['tmp_name'], $path))
                {
                    $data['file_name']=$image_name;
                }
                @unlink ($_FILES [$element]);

		        $db->setQuery("INSERT INTO `file` (`user_id`, `mail_id`, `name`, `rand_name`, `file_date`) VALUES ('$user', '$table_id', '$original_name', '$image_name', NOW())");
		        $db->execQuery();
		        $db->setQuery("SELECT  `name`,
                        			   `rand_name`,
                        			   `file_date`,
                                       `id`
                			    FROM   `file`
                			    WHERE  `mail_id` = $table_id AND `actived` = 1");
		        $str_file_table = array();
		        $file_tbale = $db->getResultArray();
		        foreach($file_tbale[result] AS $file_res_table) {
		            $str_file_table[] = array('file_date' => $file_res_table[file_date], 'name' => $file_res_table[name], 'rand_name' => $file_res_table[rand_name],'id' => $file_res_table[id]);
		        }
		        $data ['page']= $str_file_table;

		    }
		    break;
    case 'delete_file':
		$file_name	= $_REQUEST['file_name'];
		$path		= $_REQUEST['path'];
		$path		= $path . $file_name;
		
		if (file_exists($path)) {
			unlink($path);
		}
		
        break;
    case 'delete_file1':
        $file_id    = $_REQUEST['file_id'];
        $table_name = $_REQUEST['table_name'];
        $path		= "../../mailmyge/file/";
        
        $db->setQuery("SELECT `rand_name`,
			                  `".$table_name."_id`
                       FROM   `file` WHERE `id` = $file_id");
        $file_res = $db->getResultArray();
        
        $db->setQuery("UPDATE `file` SET `actived`= 0 WHERE `id` = $file_id");
        $db->execQuery();
        
        $path		= $path . $file_res[result][0][rand_name];
        
        if (file_exists($path)) {
            unlink($path);
        }
        
        $karochera = $file_res[result][0][$table_name.'_id'];
        
        $db->setQuery(" SELECT `name`,
                			   `rand_name`,
                			   `file_date`,
                               `id`
        			    FROM   `file`
        			    WHERE  `".$table_name."_id` = '$karochera' AND `actived` = 1");
        
        $file_tbale = $db->getResultArray();
        $str_file_table = array();
        
        foreach($file_tbale[result] AS $file_res_table) {
            $str_file_table[] = array('file_date' => $file_res_table[file_date],'name' => $file_res_table[name],'rand_name' => $file_res_table[rand_name],'id' => $file_res_table[id]);
        }
        
        $data		= array('page'	=> $str_file_table);
        
        break;
    case 'get_file_list':
		$path		= $_REQUEST['path'];		
		$file_list	= directoryToArray($path, false);
		$data		= array('file_list' => json_encode($file_list));
		
        break;
    default:
       $error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	File Upload Functions
 * ******************************
 */

function directoryToArray($directory, $recursive) {
	$array_items = array();
	if ($handle = opendir($directory)) {
		while (false !== ($file = readdir($handle))) {
			if ($file != "." && $file != "..") {
				if (is_dir($directory. "/" . $file)) {
					if($recursive) {
						$array_items = array_merge($array_items, directoryToArray($directory. "/" . $file, $recursive));
					}
					$file = $file;
					$array_items[] = preg_replace("/\/\//si", "/", $file);
				} else {
					$file = $file;
					$array_items[] = preg_replace("/\/\//si", "/", $file);
				}
			}
		}
		closedir($handle);
	}
	return $array_items;
}

?>