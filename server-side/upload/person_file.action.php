<?php
/* ******************************
 *	File Upload aJax actions
 * ******************************
 */
require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db     = new dbClass();
$action = $_REQUEST['act'];
$error	= '';
$data	= array();
$user   = $_SESSION['USERID'];
switch ($action) {
    case 'file_upload':
        $element	   = $_REQUEST['button_id'];
        $file_name	   = $_REQUEST['file_name'];
        $type		   = $_REQUEST['file_type'];
        $path		   = $_REQUEST['path'];
        $rand_name     = $file_name . '.' . $type;
        $original_name = $_REQUEST['file_name_original'];
        $path		   = $path . $file_name . '.' . $type;
        $table_id      = $_REQUEST['table_id'];
        $table_name    = $_REQUEST['table_name'];
        $user          = $_REQUEST['person_id'];
		
        if (! empty ( $_FILES [$element] ['error'] )) {
            switch ($_FILES [$element] ['error']) {
                case '1' :
                    $error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
                    break;
                case '2' :
                    $error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
                    break;
                case '3' :
                    $error = 'The uploaded file was only partially uploaded';
                    break;
                case '4' :
                    $error = 'No file was uploaded.';
                    break;
                case '6' :
                    $error = 'Missing a temporary folder';
                    break;
                case '7' :
                    $error = 'Failed to write file to disk';
                    break;
                case '8' :
                    $error = 'File upload stopped by extension';
                    break;
                case '999' :
                default :
                    $error = 'No error code avaiable';
            }
        } elseif (empty ( $_FILES [$element] ['tmp_name'] ) || $_FILES [$element] ['tmp_name'] == 'none') {
            $error = 'No file was uploaded..';
        } else {
            if (file_exists($path)) {
                unlink($path);
            }
            move_uploaded_file ( $_FILES [$element] ['tmp_name'], $path);
            
			//$error = 'User: '.$user.' File Name: '.$rand_name.' File org Name: '.$original_name;
			
			$db->setQuery("UPDATE `person` SET `image`='$rand_name' WHERE id='$user'");
			$db->execQuery();
            
        }
        
        break;
		
		
    case 'delete_file':
		$file_name	= $_REQUEST['file_name'];
		$path		= $_REQUEST['path'];
		$path		= $path . $file_name;
		
		if (file_exists($path)) {
			unlink($path);
		}
		
        break;
    
    default:
       $error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


?>